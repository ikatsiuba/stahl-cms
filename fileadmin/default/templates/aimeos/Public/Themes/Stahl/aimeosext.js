/**
 * Catalog filter actions
 */

AimeosCatalogFilter.setupAttributeItemSubmit = function() {
	$(".catalog-filter-attribute .checkbox-list button").click(function() {
		$(this).closest('ul').find('input:checked').prop("checked", true);
		AimeosCatalogFilter.submit($(this));
	});

	$(".catalog-filter-attribute .slider-list button").click(function() {
		 $("#slider-min").prop( "disabled", false );
		 $("#slider-max").prop( "disabled", false );
		 $("#slider").prop( "disabled", false );
         $("#slider").attr("checked-attr", "checked");
		AimeosCatalogFilter.submit($(this));
	});


	$(".catalog-filter-attribute .checkbox-list li.attr-item").on("click", ".attr-name, .attr-count", function(ev) {
		var input = $("input", ev.delegateTarget);
		if(input.prop("disabled")==false){
			input.prop("checked") ? input.prop("checked", false) : input.prop("checked", true);
		}
	});

	$(".catalog-filter-attribute .radio-list li.attr-item").on("change", ":radio", function(ev) {
		changeRadioFilter($(this));
	});
	$(".catalog-filter-attribute .radio-list li.attr-item").on("click", ".attr-name, .attr-count", function(ev) {
		changeRadioFilter($(this));
	});

	$(".catalog-filter-attribute").on("change", "select.attr-item", function(ev) {
		if($(this).val() == '') {
			$(this).removeAttr('name');
		}
		else {
			var attrId = $(this).find('option:selected').data('id');
			$(this).attr('name','ai[f_attrid]['+attrId+']');
		}
		AimeosCatalogFilter.submit($(this));

	});

    $(".catalog-filter-search .standardbutton, #webcode_overview .tx-solr-submit-blue").on("click",function(ev) {
        AimeosCatalogFilter.searchSubmit($(this));
        return false;
    });

    $(".catalog-filter-search .value").keypress(function (e) {
          if (e.which == 13) {
            AimeosCatalogFilter.searchSubmit($(this));
            return false;
          }
        });


	function changeRadioFilter(th) {
		var li_item = $(th).closest('li.attr-item');
		if($(th).closest('ul').find('input').prop("disabled")==false){
			$(th).closest('ul').find('input').prop("checked", false);
		}
		if($(li_item).find('input').prop("disabled")==false){
			$(li_item).find('input').prop("checked", true);
		}
		AimeosCatalogFilter.submit($(th));
	}

	function checkBoxChange(){
		var len=$('#product-compare input[name="ai[products][]"]:checked, #product-compare input[name="ai[products][]"][type=hidden]').length;
		$('#product-compare button[type=submit]').find('span.selected-item').text(len);
		$('#product-compare button[type=submit]').prop('disabled', len<2);
	}
	checkBoxChange();
	$('#product-compare input[type=checkbox]').unbind().on('change','',function(){
		checkBoxChange();
	});

}

AimeosCatalogFilter.setupAttributeListsToggle = function() {

	$(".catalog-filter-attribute .attribute-lists .attr-list").hide();
	$(".catalog-filter-attribute .attribute-lists ul li input:checked").prop('checked',true);

	$(".catalog-filter-attribute fieldset:not(.subfilter)").on("click", "legend", function(ev) {
		$(".attr-list", ev.delegateTarget).slideToggle();
	});
	//$(".attribute-lists .stahl-global-filter").trigger('click');
	var ie = AimeosCatalogFilter.detectIE();
    AimeosCatalogFilter.split_number = 12;
    if (!ie) {
        AimeosCatalogFilter.split_number = 4;
    }
    AimeosCatalogFilter.line_width = $(".slider_line").width()-2;
    if($('.slider_line').length>0){
	    // mark 1
	    var mark_1 = Draggable.create(".value_mark.i_1", {
	        type:"x",
	        edgeResistance:1,
	        bounds:".slider_wrapper",
	        lockAxis:true,
	        throwProps:true,
	        onDrag: AimeosCatalogFilter.synchronizeValueMark1
	    });
	    TweenLite.to($(".value_mark.i_1"), 0, {x: 0/*Math.round(($(".value_mark.i_1 .box").text()*($(".slider_line").width())-2)/mx*100)/100*/});
    AimeosCatalogFilter.updateSliderValue();
    }
}

AimeosCatalogFilter.setupAttributeToggle = function() {}

AimeosCatalogFilter.submit = function(elem) {
	if(AimeosCatalogFilter.isAjax)
		 return;

	AimeosCatalogFilter.isAjax=true;
	$(".catalog-list,.catalog-filter-attribute").fadeTo(1000, 0.5);
	var form = elem.parents(".catalog-filter");
	$.post(form.find('[name="ajaxurl"]').val(), form.serialize(), function(data) {
			var doc = document.createElement("html");
			doc.innerHTML = data;
			$(".listtable").html($(".listtable", doc).html());
			$(".catalog-filter-attribute").html($(".catalog-filter-attribute", doc).html());
			$(".catalog-list,.catalog-filter-attribute").fadeTo(1000, 1);
			AimeosCatalogFilter.setupAttributeItemSubmit();
			AimeosCatalogFilter.setupAttributeListsToggle();
			AimeosCatalogFilter.isAjax=false;
	});

	return false;
}

AimeosCatalogFilter.searchSubmit = function(elem) {
	var element = $(elem);
	var form = element.parents('form');
	var url = $('#searchurl');
	form.attr('action',url.val());
	form.find('[name="ai[d_prodid]"]').attr('name','ai[f_catid]').val('0');
	form.find('[name="ai[f_catid]"]').val('0');
	form.submit();
	return false;
}

AimeosCatalogFilter.detectIE = function () {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }
    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    return false;
}

AimeosCatalogFilter.synchronizeValueMark1 = function () {
    var position = parseInt($('.value_mark.i_1').css('transform').split(',')[AimeosCatalogFilter.split_number]);
    var value = (position / ($(".slider_line").width()-1)) * mx;

    value = Math.round(value*100)/100;
    if(position==1){
        value = sliderArrayValues[0];
    }
    $(".value_mark.i_1 .box").text(value);
    AimeosCatalogFilter.updateSliderValue();
};

AimeosCatalogFilter.updateSliderValue = function () {
	var value = $(".value_mark.i_1 .box").text();
    $("#slider").val(value);
    if(sliderArrayValues){
        var min_array = [], max_array = [], ln=sliderArrayValues.length;

        for (var i = 0; i < ln; i++) {
            if(parseInt(sliderArrayValues[i])<=value){
                min_array.push(sliderArrayValues[i]);
            }else{
                max_array.push(sliderArrayValues[i]);
            }
        }
    }
    if(min_array.length==0){
        min_array.push(sliderArrayValues[0]);
    }
    if(max_array.length==0){
        max_array.push(sliderArrayValues[sliderArrayValues.length-1]);
    }
    $("#slider-min").val(min_array.toString());
    $("#slider-max").val(max_array.toString());
}

AimeosCatalogDetail.setupFirstImageZoom = function(){}

AimeosCatalogFilter.pager = function(elem, url) {
	if(AimeosCatalogFilter.isAjaxPager)
		 return;

	AimeosCatalogFilter.isAjaxPager = true;
	var form = $(".catalog-filter");
	$(".catalog-list-pagination").fadeTo(1000, 0.5);
	$.post(url, form.serialize(), function(data) {
			var doc = document.createElement("html");
			doc.innerHTML = data;
			$(".listtable .content-row").append($(".listtable .content-row", doc).html());
			$(".catalog-list-pagination").html($(".catalog-list-pagination", doc).html());
			$(".catalog-list-pagination").fadeTo(1000, 1);
			AimeosCatalogFilter.setupAttributeItemSubmit();
			AimeosCatalogFilter.isAjaxPager = false;
	});

	return false;
}

AimeosWebcodeFilter = {

	pager : function(elem, url) {
		if(AimeosWebcodeFilter.isAjaxPager)
			 return;

		AimeosWebcodeFilter.isAjaxPager = true;
		var form = $(".catalog-filter");
		$(".catalog-list-pagination").fadeTo(1000, 0.5);
		$.post(url, form.serialize(), function(data) {
				var doc = document.createElement("html");
				doc.innerHTML = data;
				$(".webcode .table_details tbody").append($(".webcode .table_details tbody", doc).html());
				$(".catalog-list-pagination").html($(".catalog-list-pagination", doc).html());
				$(".catalog-list-pagination").fadeTo(1000, 1);
				AimeosWebcodeFilter.isAjaxPager = false;
		});

		return false;
    }

}

AimeosDownloadsFilter = {

		/**
		 * Submits the form when clicking on filter attribute names or counts
		 */
		setupAttributeItemSubmit: function(filter) {

			$(filter+" button.mediabutton").click(function() {
				AimeosDownloadsFilter.submit($(this));
			});


			$(filter+" .value").keypress(function (e) {
				  if (e.which == 13) {
					  AimeosDownloadsFilter.submit($(this));
				    return false;
				  }
				});

			$(filter+" select.download_select").unbind().on("change", "", function(ev) {
				var elem=$(this);
				var selectors = elem.parents(".media-catalog-filter").find("select.download_select");
				if(selectors.length>0){
					index = selectors.index(elem);
					for( var i=index+1;i<selectors.length;i++ ){
						$(selectors[i]).prop('disabled', true);
					}
				}
				AimeosDownloadsFilter.submit($(this));
			});

			$(filter+" button.clear").click(function() {
				var elem=$(this);
				var selectors = elem.parents(".media-catalog-filter").find("select");
				if(selectors.length>0){
					index = selectors.index(elem);
					for( var i=index+1;i<selectors.length;i++ ){
						$(selectors[i]).prop('disabled', true);
					}
				}
				$(filter+" .value").val('');
				AimeosDownloadsFilter.submit($(this));
			});


			$(filter+" select.download_select_attribute").unbind().on("change", "", function(ev) {
				AimeosDownloadsFilter.submit($(this));
			});

			$(filter+" .attribute-checkbox input").unbind().on("click", "", function(ev) {
				var input = $("input", ev.delegateTarget);
				input.prop("checked") ? input.prop("checked", false) : input.prop("checked", true);
				AimeosDownloadsFilter.submit($(this));
			});
		},
		submit: function(elem) {

			if(AimeosDownloadsFilter.isAjax){
				 return;
			}

			AimeosDownloadsFilter.isAjax=true;
			var form = elem.parents(".media-catalog-filter"),medialist=form.find('[name="ai\[f_listtype\]"]').val() ;
			console.log(medialist, form.attr('action'));
			$.post(form.attr('action'), form.serialize(), function(data) {
					var doc = document.createElement("html");
					doc.innerHTML = data;
					$(".medialist-"+medialist).html($(".medialist-"+medialist, doc).html());
					AimeosDownloadsFilter.setupAttributeItemSubmit(".medialist-"+medialist);
					AimeosDownloadsFilter.isAjax=false;
			});

			return false;
		},

		/**
		 * Initializes the locale selector actions
		 */
		init: function() {
			this.setupAttributeItemSubmit(".media-catalog-filter");
			//$(".catalog-filter-attribute .radio-list li.attr-item input:checked").prop('checked',false);
			//$(".catalog-filter-attribute .radio-list li.attr-item").removeClass('checked');
			$(".media-catalog-filter select.selecter").val('');//.trigger("change");
			$(".media-catalog-filter .value").val('');
		}
};

AimeosCatalogFilter.setupSearchAutocompletion =  function() {
	var aimeosInputComplete = $(".catalog-filter-search .value");
// console.warn(aimeosInputComplete)

	aimeosInputComplete.autocomplete({
		minLength : 3,
		delay : 100,
		appendTo : ".catalog-filter-search",
		source : function(req, add) {
			var nameTerm = {};
			nameTerm[aimeosInputComplete.attr("name")] = req.term;

		$.getJSON(aimeosInputComplete.data("url"), nameTerm, function(data) {
				var suggestions = [];

				$.each(data, function(idx, val) {
					suggestions.push({href:val.id, label: val.name, url: val.url});
				});

				add(suggestions);
			});
		},
		select : function(ev, ui) {
			location.href=ui.item.url;
			return false;
		}
	});

	var topInputComplete = $("#webcode_overview .value");

    topInputComplete.autocomplete({
        minLength : 3,
        delay : 100,
        appendTo : "#webcode_overview",
        source : function(req, add) {
            var nameTerm = {};
            nameTerm[topInputComplete.attr("name")] = req.term;

        $.getJSON(topInputComplete.data("url"), nameTerm, function(data) {
                var suggestions = [];

                $.each(data, function(idx, val) {
                    suggestions.push({href:val.id, label: val.name, url: val.url});
                });

                add(suggestions);
            });
        },
        select : function(ev, ui) {
            location.href=ui.item.url;
            return false;
        }
    });
};

jQuery(document).ready(function() {
    jQuery('#product-compare .pagination li a').click(function(){
    	var iname = 'ai[products]';
    	var $form = jQuery('#product-compare');
		var $a = jQuery(this);
		var href = $a.attr('href');
		var products = [];
        $form.find('input[name="'+iname+'[]"]').each(function(){
        	var $i = jQuery(this);
        	if($i.prop('checked') || $i.attr('type') == 'hidden'){
                products.push($i.val());
			}
		});
        console.log(products);
        if(products.length) {
        	var params = {}; params[iname] = products;
        	var data_href = $a.attr('data-href');
        	if(!data_href){
                $a.attr('data-href',href);
                data_href = href;
			}
            data_href = data_href + ( data_href.match(/[\?]/g) ? '&' : '?' ) + jQuery.param(params);
    		$a.attr('href',data_href);
        };
	});
});

jQuery(document).ready(function($) {
	AimeosDownloadsFilter.init();
});