$(document).ready(function (e) {
	/*	if ($(".aimeos.catalog-filter").length > 0) {
			$(".aimeos.catalog-filter").wrap("<div class='module product_filter'><div>");
		}*/

	// plugins conflict is fix , for accordion on the Download page
	var downloadPageFilters = $('#content .panel-group.volle_breite div[id*="accordion3-"]');
		$('header form[id*="tx-solr-search-form-pi-results"] input[type="text"]').bind('keydown', 'keypress', function(e){
			var code = (e.keyCode ? e.keyCode : e.which);
			var timeCount = (jQuery(this).hasClass('tx-solr-q'))? 1550 : 1500 ;

			if (downloadPageFilters.length > 2) {
				setTimeout(function() {
					var checkCloningParent = downloadPageFilters.parent().find('div[child*="accordion3-"].active').parent().find('.select_box_1 div[class="selecter  closed"]');

					if (downloadPageFilters.parent().find('div[child*="accordion3-"]').hasClass('active') && checkCloningParent.length > 1 ) {
					console.warn( timeCount , code, jQuery(e.target).val() ); //[0]  1700); // [1] 890);
						for (var i = 0; i < checkCloningParent.parent().length; i++) {
							checkCloningParent.parent().eq(i).children('div[class="selecter  closed"]').not(':eq(0)').remove();
						}
					};
				}, timeCount);
			};
            // $('header #tx-solr-search-form-pi-results_2 input[type="text"]').removeClass('ui-autocomplete-input')
		});
	panel();

	/*** panel-body download***/
	function panel() {
		
		$('.panel .panel-body').each(function() {
			if($(this).find( ".media-list" ).length > 0) {
				$(this).css('background-color', '#ededed');
			}
		});
	}

	function check_flyout() {
		$(".headline-row > .flyout_container > ul > li").each(function (index, element) {
			if ($(this).children(".flyout_container").length === 0) {
				$(this).addClass("hide_arrow");
			}
		});

		$(".headline-row > .flyout_container > ul > li > .flyout_container > ul > li").each(function (index, element) {
			if ($(this).children(".flyout_container").length === 0) {
				$(this).addClass("hide_arrow");
			}
		});

		$(".headline-row > .flyout_container > ul > li > .flyout_container > ul > li > .flyout_container > ul > li").each(function (index, element) {
			if ($(this).children(".flyout_container").length === 0) {
				$(this).addClass("hide_arrow");
			}
		});

	}
	check_flyout();

    // Produce on page global/produkte/produkte/beleuchten/
    var hyperlinkAttr = $('.product_filter .catalog-filter .direct_flyout hyperlink');
    hyperlinkAttr.css('cursor', 'pointer');
    hyperlinkAttr.on('click', function(){
        window.open($(this)[0].attributes[0].value);
    });


    $(document).on("click", ".product_series.module  .pagination .center a, .product_item_table.module  .pagination .center a", function (e) {
		e.preventDefault();
		$(this).fadeOut();
	});

	//click on item redirect
	$(document).on("click", ".product_series.module  .list_item", function () {
		var href = $(this).find("a").attr('href');
		window.location.href = href;
	});
	$(document).on("click", ".product_item_table .detail .checkbox_div", function (e) {
		e.stopPropagation();
	});


	$(document).on("click", ".product_item_table .detail", function () {
		var href = $(this).find("a").attr('href');
		window.location.href = href;
	});





	/*** button comapre info ***/
	/*	$(document).on("click", "#product-compare > button", function()
		{
			console.log("click");
			$("#product-compare > button[type=submit] + div").fadeIn();
		});
		
		$(document).on("change", "#product-compare button[disabled]", function()
		{
			console.log("chanse");
		});*/



	var add_content_acessories_bar = false;
	$(document).on("click", ".product_hero_extended .info_bar li[detail='accessories']", function () {
		if ($(this).hasClass("active") && (!add_content_acessories_bar)) {
			add_content_acessories_bar = true;
			$(".add_content.accessories ul li:first-child").click();
		}
	});



	/* if ($(".product_detail.module").length > 0) {

	     $(".product_detail.module .images > a > div > img").each(function(index, element) {
	         var url = $(this).attr("src");
	         $(this).parents("a").attr("href", url).addClass("html5lightboxafter");
	         $(".html5lightboxafter").html5lightbox();
	     });
	 }*/


	$(".product_detail .html5lightbox").each(function (index, element) {
		$(this).attr("data-width", "");
		$(this).attr("data-height", "");
		$(".html5lightbox").html5lightbox();
	});

	/************** FILTER ****************/
	var dropdown_wait;
	$(document).on("click", ".catalog-filter-attribute .attribute-lists fieldset > legend", function () {
		if (!$(this).hasClass("active")) {
			$(".catalog-filter-attribute legend").removeClass("active");

			$(this).addClass("active");
			$(".catalog-filter-attribute legend:not(.active) + ul.attr-list, .catalog-filter-attribute legend:not(.active) + div + ul.attr-list").hide();

			var filter_width = $(".catalog-filter .catalog-filter-attribute .attribute-lists").width();

			$(this).next("ul").css("width", filter_width);
			$(this).next(".attribute-selected").hide();
			if ( $(this).hasClass("active") && ! $(".attr-list.slider-list").parent().children("legend").hasClass("active")) {
				$(".catalog-filter-attribute .attr-list.slider-list").hide();
			};
		} else {
			$(this).removeClass("active");
			$(this).next(".attribute-selected").show();
		};
	});

	function check_visibility(elem) {
		if (!$(elem).next("ul").is(':visible')) {
			$(elem).removeClass("active");
			clearInterval(dropdown_wait);

		}
	}



	var category_filter = false;
	var global_filter_complete = false;
	var filter_animation = false;




	function detect_inactive() {
		$(".stahl-global-filter > fieldset, .stahl-category-filter > fieldset").each(function (index, element) {
			if ($(this).css("display") === "none") {
				$(this).addClass("inactive");
				$(this).remove();
			}
		});

		console.log($(".stahl-category-filter > fieldset:not(.inactive)").length);
		if ($(".stahl-category-filter > fieldset:not(.inactive)").length > 4 && ($(".add_filter").length === 0)) {
			$("<div class='add_filter'><div class='blue_bar'></div><div class='center'>"+$('#filterlabel').html()+"</div></div>").insertAfter(".stahl-category-filter");
		} else if ($(".add_filter").length > 0) {
			$(".add_filter").remove();
		}

		$(".stahl-category-filter > fieldset:not(.inactive)").each(function (index, element) {
			if (index > 3) {
				$(this).addClass("more_filter");
			}
		});


		check_more_filter_shouldbe_active();
	}
	detect_inactive();

	if ($(".stahl-global-filter").length > 0) {
		$(document).ajaxComplete(function () {
			check_fieldests();
			detect_inactive();
			console.log("filter AJAX function");
		});
	}





	// main filter mechanik
	$(document).on("click", ".attribute-lists .stahl-global-filter", function () {
		if (($(".catalog-filter-attribute").is(":animated")) && !filter_animation) {
			filter_animation = true;
			console.log("change global");
			if (!category_filter) {
				global_filter_complete = true;
				var wait = setInterval(function () {
					console.log("wait");
					if ((!$(".catalog-filter-attribute").is(":animated")) && (parseInt($(".catalog-filter-attribute").css("opacity")) === 1)) {
						clearInterval(wait);
						refresh_li_check();

						$(".attribute-lists > .stahl-global-filter fieldset").each(function (index, element) {
							if ($(this).is(":visible") && (!$(this).hasClass("subfilter"))) {
								if ($(this).children(".attribute-selected").length === 0) {
									console.log($(this).attr("class"));
									global_filter_complete = false;
								}
							}
						});

						filter_animation = false;

						if (global_filter_complete) {
							console.log("complet");
							$(".stahl-global-filter").addClass("complete");
							$(".catalog-filter-attribute").addClass("global_complete");
							/*$(".stahl-category-filter > .attr-list").remove();*/
							if ($(".stahl-category-filter > fieldset").length > 4 && ($(".add_filter").length === 0)) {
								$("<div class='add_filter'><div class='blue_bar'></div><div class='center'>"+$('#filterlabel').html()+"</div></div>").insertAfter(".stahl-category-filter");
							}
							$(".stahl-category-filter").fadeIn(function () {
								$(".stahl-category-filter").addClass("active");
							});
							category_filter = true;
						}
					}
				}, 10);

			} else {
				/*$(".stahl-category-filter").fadeOut().removeClass("active");*/
				category_filter = false;
			}
		}

	});

	$(document).on("click", ".attribute-lists .stahl-category-filter.active", function () {
		choice_extra_filter = false;
		if (($(".catalog-filter-attribute").is(":animated")) && !filter_animation) {
			if ($(this).children("fieldset").hasClass("more_filter")) {
				$(".catalog-filter-attribute").addClass("extra_filter_choice");
			}
			filter_animation = true;
			console.log("change category");
			var wait = setInterval(function () {
				console.log("wait");
				if ((!$(".catalog-filter-attribute").is(":animated")) && (parseInt($(".catalog-filter-attribute").css("opacity")) === 1)) {
					clearInterval(wait);
					refresh_li_check();

					$(".stahl-category-filter").addClass("active");
					if ($(".stahl-category-filter > fieldset").length > 4 && ($(".add_filter").length === 0)) {
						$("<div class='add_filter'><div class='blue_bar'></div><div class='center'>"+$('#filterlabel').html()+"</div></div>").insertAfter(".stahl-category-filter");
					}
					if ($(".catalog-filter-attribute").hasClass("extra_filter_choice")) {
						$(".add_filter > .center").click();
					}
					$(".attribute-lists > .stahl-category-filter fieldset").each(function (index, element) {
						if ($(this).is(":visible") && (!$(this).hasClass("subfilter"))) {
							if ($(this).children(".attribute-selected").length === 0) {
								console.log($(this).attr("class"));
								global_filter_complete = false;
							}
						}
					});
					filter_animation = false;
				}
			}, 10);


		}

	});

	// add extra filter
	$(document).on("click", ".add_filter > .center", function () {
		if (!$(this).hasClass("active")) {
			$(this).addClass("active");
			$(this).text($('#filtercancellabel').html());
			$(".stahl-category-filter > fieldset.more_filter").each(function (index, element) {
				$(this).addClass("more_filter");
				$(this).slideDown();
			});
		} else {
			$(this).removeClass("active");
			$(this).text($('#filterlabel').html());
			$(".stahl-category-filter > .more_filter").slideUp();
		}


	});

	$(".catalog-filter-search > button.standardbutton, .media-filter-search > button.mediabutton").text("");
	//$(".catalog-filter-search > input").attr("placeholder", "Produktname oder Webcode");

	/*    $(".stahl-category-filter > .attr-list").remove();*/
	$(".stahl-category-filter").addClass("active");

	//clear selected filter 
	$(document).on("click", ".attribute-lists a.filter_clear", function () {
		var parent = $(this).parents(".attribute-selected");
		var list = parent.next("ul.attr-list");
		list.find("li").find("input").attr("checked", false);
		$(this).parents(".attribute-selected").remove();
	});

	$(document).on("click", ".attribute-lists fieldset ul li > label", function () {
		if ($(this).prev("input").prop("checked")) {
			$(this).prev("input").prop("checked", false);
		} else {
			$(this).prev("input").prop("checked", true);
		}


	});

	$(document).on("click", ".catalog-filter-attribute .attribute-lists fieldset ul li input", function () {
		if (!$(this).hasClass("attr-name")) {
			if ($(this).prop("checked")) {
				$(this).closest("li").addClass("checked");
			} else {
				$(this).closest("li").removeClass("checked");
			}
		}
	});

	function refresh_li_check() {
		// console.log("refresh");
		$(".catalog-filter-attribute .attribute-lists ul li").each(function (index, element) {
			if ($(this).children("input").attr("checked")) {
				$(this).addClass("checked");

			} else {
				$(this).removeClass("checked");
			}
		});


		$(".catalog-filter-attribute li").removeClass("option_disabled");
		$(".catalog-filter-attribute input.attr-item").each(function (index, element) {
			if ($(this).attr("disabled") === "disabled") {
				$(this).parent("li").addClass("option_disabled");
			}

		});
	}

	function check_more_filter_shouldbe_active() {

		if ($(".stahl-category-filter fieldset.checked.more_filter").length > 0) {
			if ($(".add_filter").length > 0 && !($(".add_filter").hasClass("active"))) {
				$(".add_filter").click();
				/*				$(".add_filter").addClass("more_filter");
								$(".add_filter").slideDown();*/
			}

		}
	}

	function check_fieldests() {
		$(".headline-row").each(function (index, element) {
			if ($(this).children("a").length === 0 && $(this).children("p").length === 0 && $(this).children(":header").length === 0) {
				$(this).remove();
			} else if ($(this).children("a").length === 0 && $(this).children("p").length === 0 && $(this).children(":header").length > 0) {
				$(this).addClass("only-headline");
			} else if ($(this).children("a").length === 0 && $(this).children("p").length > 0 && $(this).children(":header").length === 0) {
				$(this).addClass("only-p");
			} else if ($(this).children("a").length > 0 && $(this).children("p").length === 0 && $(this).children(":header").length === 0) {
				$(this).addClass("only-cta");
			} else if ($(this).children("a").length === 0 && $(this).children("p").length > 0 && $(this).children(":header").length > 0) {
				$(this).addClass("with-p");
			} else if ($(this).children("a").length > 0 && $(this).children("p").length === 0 && $(this).children(":header").length > 0) {
				$(this).addClass("with-cta");
			}
		});

		var attributeLists = $(".catalog-filter-attribute .slider-list");
        attributeLists.children("div").children("fieldset").removeClass("checked");
        attributeLists.find("ul li").each(function (index, element) {
			if ($(this).children("input").attr("checked")) {
				$(this).closest("fieldset").addClass("checked");
			}
		});

		if (attributeLists.find("#slider").attr("value") > attributeLists.find("#slider-min").attr("value")) {
            attributeLists.parents("fieldset").addClass("checked");
		};
	}

	refresh_li_check();
	check_fieldests();
	/************** FILTER END ****************/


	/*** QUICK FIX ***/
	$(".teaserboxen_normal_drei_raster.direct_flyout.module .row").addClass("sameheight");
	$(".product_category_headline_tabs.module .row").addClass("sameheight");
	$(".product_category_headline_tabs.module .row .headline_tab").addClass("setheight");



	/*** MOBILE Anpassungen ***/
	var mobile = false;
	var click_option = "click";
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		mobile = true;
		if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
			click_option = "touchend";
		}

	}


	var last_flyout_item = "";
	var acctualy_level = 0;
	var flyout_listener = false;
	var prev_category = "";
	var first_li = "";

	$(document).on(click_option, ".flyout_container li a", function (e) {
		if (mobile) {
			e.preventDefault();
			if ($(this).is(last_flyout_item)) {
				var href = $(this).attr("href");
				window.location.href = href;
			}


			if ($(window).width() < 767) {
				if ($(this).next("div").hasClass("flyout_container")) {
					acctualy_level++;
					prev_category = $(this).text();
					first_li = $(this).next(".flyout_container").find("li:eq(0)");
					if (!$(first_li).hasClass("go_back")) {
						$("<li class='go_back'>" + prev_category + "</li>").insertBefore(first_li);
					}
					$(".fylout_icon + .flyout_container").addClass("left_" + acctualy_level);
				} else {
					var href = $(this).attr("href");
					window.location.href = href;
				}

			}
			last_flyout_item = "";
			/*            last_flyout_item = $(this);*/
		}
	});

	$(document).on(click_option, ".flyout_container li.go_back", function () {
		$(".fylout_icon + .flyout_container").removeClass("left_" + acctualy_level);
		acctualy_level--;
	});



	$(document).on(click_option, ".flyout_container li a", function (e) {

	});


	if ($(".catalog-filter-search").length > 0 && $(".flyout_container").length > 0) {
		$(".direct_flyout.module .headline-row h2").addClass("short_h2");
	}
	if ($(".catalog-filter-search").length > 0 && $(".filter_module").length > 0) {
		$(".filter_module .headline-row h2").addClass("short_h2");
	}



	$(document).on("click", "ul.ui-autocomplete > li", function () {
		$(".catalog-filter-search button[type=submit]").click();
	});


	/*	console.log($("#c148").children().length);
		if($("#c148").children().length === 0)
		{
			$("#c148").hide();
		}*/





	if ($(".product_category_headline_tabs.module + .catalog-filter").length > 0) {
		var elem_parent = $(".product_category_headline_tabs.module").parent(".frame.default");
		$("<div class='frame default search_place'></div").insertAfter(elem_parent);
		$(".product_category_headline_tabs.module + .catalog-filter").appendTo(".frame.default.search_place");
	}
	/************** DOWNLOAD  ****************/
	/* $(window).on("load", function() {
	    $("th.pwrk-jobboard-col-select > select").selecter();
	});*/
	$(window).on("load", function () {

	});

	/*	$(document).on("change" , ".product_download select", function()
		{
			console.log("change");
			setTimeout(function()
			{
				$(".product_download select.selecter").selecter();
			},4000);
		});*/


	if ($(".aimeos.media-list").length > 0) {
		$(".aimeos.media-list").each(function (index, element) {
			var download_media = $(this).parents("#accordion3");
			if (!($(download_media).hasClass("download-media"))) {
				$(download_media).addClass("download-media");
			}
		});

	}


	if ($(".module.product_filter").length > 0) {
		if ($(".module.product_filter .filter_module").length > 0 && $(".stahl-global-filter").children().length === 0) {
			$(".module.product_filter .module").last().addClass("nobottom");
			if(!($(".module.product_filter .filter_module").hasClass("nobottom")))
				{
					$(".module.product_filter .filter_module").addClass("half_bottom")
				}
		} else {
			$(".module.product_filter .module:not(.filter_module)").last().addClass("nobottom");
		}
	}





	var last_filter_parent = 0;


	$(document).on("change", ".aimeos.media-list select", function () {
		last_filter_parent = $(this).parents(".frame.default").attr("id");
	});

	$(document).on("click", ".download-media button.clear", function () {
		last_filter_parent = $(this).parents(".frame.default").attr("id");
		var scroll_parent = $(this).closest(".panel.panel-default");
		scrollto(scroll_parent);
	});
	
	$(document).on("click", ".download-media .media-filter-search", function () {
		last_filter_parent = $(this).parents(".frame.default").attr("id");
	});


	if ($(".media-catalog-filter").length > 0) {
		$(document).ajaxComplete(function () {
			if (!mobile) {
				$("#" + last_filter_parent + " .product_download select.selecter").selecter();
				$(".media-filter-search > button.mediabutton").text("");
				$(".product_download select.selecter").hide();
			}
		});
	}
	if ($(".stahl-global-filter").length > 0) {
		$("ul.attr-list").each(function (index, element) {
			if ($(this).children("button").length > 0) {
				$(this).children("button").insertAfter($(this).children("li:last"));
			}
		});
		$(document).ajaxComplete(function () {
			console.log("ajax done");
			$("ul.attr-list").each(function (index, element) {
				if ($(this).children("button").length > 0) {
					$(this).children("button").insertAfter($(this).children("li:last"));
				}
			});
		});
	}

	$(document).on("click", ".product_download .checkbox_div", function () {
		$(this).find("input").click();
	});


	$(".icons_area").each(function (index, element) {
		if (!($(this).find(".icon").length > 0)) {
			$(this).hide();
		}
	});


	function products_comapre_table_setheight() {
		/*		if ($(".products_compare_table").length > 0 && $(window).width() < 768) {*/
		var height = 0;
		var max_height = 0;
		var children_counter = $(".products_compare_table .left_column > div").length;

		for (i = 1; i <= children_counter; i++) {
			console.log("forr");
			height = 0;
			max_height = 0;

			$(".products_compare_table .left_column > div").eq(i).css("height", "auto");
			max_height = $(".products_compare_table .left_column > div").eq(i).outerHeight();

			$(".products_compare_table .owl-carousel .product_item").each(function (index, element) {
				$(this).children("div").eq(i).css("height", "auto");
				height = $(this).children("div").eq(i).outerHeight();
				if (height > max_height) {
					max_height = height;
				}
			});

			//set height
			$(".products_compare_table .left_column > div").eq(i).css("height", max_height);
			$(".products_compare_table .owl-carousel .product_item").each(function (index, element) {
				$(this).children("div").eq(i).css("height", max_height);
			});

		}


	}
	$(window).on("load", function () {
		products_comapre_table_setheight();
	});
	$(window).resize(function (e) {
		products_comapre_table_setheight();
	});



	var tool_tip_change_pos = $("span.move-alt");
	$(tool_tip_change_pos).appendTo("body");

	var tool_tip_remove = $("span.delete-alt");
	$(tool_tip_remove).appendTo("body");


	/*** COMPARE PRODUCTS TOOL TIPS ***/
	$(document).on('mouseenter', '.product_item .change_pos', function () {
		var elem_offset = $(this).offset();
		$(tool_tip_change_pos).css("top", elem_offset.top).css("left", elem_offset.left);
		$(tool_tip_change_pos).show();
	});

	$(document).on('mouseleave', '.product_item .change_pos', function () {
		$(tool_tip_change_pos).hide();
	});

	$(document).on('mouseenter', '.product_item .remove', function () {
		var elem_offset = $(this).offset();
		$(tool_tip_remove).css("top", elem_offset.top).css("left", elem_offset.left);
		$(tool_tip_remove).show();
	});

	$(document).on('mouseleave', '.product_item .remove', function () {
		$(tool_tip_remove).hide();
	});


	/*** VIDEO YOUTUBE THUMBNAILS ***/
	function processURL(url) {
		var id;
		if (url.indexOf('youtube.com') > -1) {
			id = url.split('/')[3].split('v=')[1].split('&')[0];
			return processYouTube(id);
		} else if (url.indexOf('youtu.be') > -1) {
			id = url.split('/')[3];
			return processYouTube(id);
		} else {
			throw new Error('Unrecognised URL');
		}

		function processYouTube(id) {
			if (!id) {
				throw new Error('Unsupported YouTube URL');
			}

			return ('http://i2.ytimg.com/vi/' + id + '/hqdefault.jpg');
		}
	}


	
	
	

	$(".add_content.multimedia .video").each(function () {
		var video_link = $(this).parent("a.html5lightbox").attr("href");
		var thumbnails = processURL(video_link);
		$(this).children("img").attr("src", thumbnails);

	});

});
