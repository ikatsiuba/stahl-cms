################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            location {
                title = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.location
                config {
                    backend_layout {
                        colCount = 6
                        rowCount = 5
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = Header 
                                        colPos = 0
                                        colspan = 6
                                    }
                                }
                            }
                            2 {
                                columns {
                                    1 {
                                        name = GeoLocation or contact person
                                        colPos = 1
                                        colspan = 2
                                    }
									2 {
                                        name = Content
                                        colPos = 2
                                        colspan = 4
                                    }
														
                                }
                            }
							3 {
                                columns {
                                    1 {
                                        name = Call Back
                                        colPos = 3
                                        colspan = 6
                                    }
                                }
                            }
							4 {
                                columns {
                                    1 {
                                        name = Content Bottom
                                        colPos = 4
                                        colspan = 6
                                    }
                                }
                            }
                            5 {
                                columns {
                                    1 {
                                        name = Sticky: Top
                                        colPos = 10
                                        colspan = 2
                                    }
                                    2 {
                                        name = Sticky: Center
                                        colPos = 11
                                        colspan = 2
                                    }
                                    3 {
                                        name = Sticky: Bottom
                                        colPos = 12
                                        colspan = 2
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:bootstrap_package/Resources/Public/Images/BackendLayouts/default_location.gif
            }
        }
    }
}