page.includeCSS {
	file1 = fileadmin/default/css/fonts/LT-WebFonts/demo.css
  	file2 = fileadmin/default/css/owl-carousel/owl.carousel.min.css 
	file3 =  fileadmin/default/css/owl-carousel/owl.theme.default.min.css 	
	file4 = fileadmin/default/css/style.css 	
	file5 = fileadmin/default/css/module.css
	file6 =  fileadmin/default/css/responsive.css
	file7 =  fileadmin/default/css/responsive-tables.css
	file8 = fileadmin/default/css/develop.css

}
page.includeCSSLibs.googlewebfont >