lib.stdheader.10.setCurrent.htmlSpecialChars = 0

##############
### CONFIG ###
##############
[globalVar = GP:L = 0]
page.headerData.51 = HMENU
page.headerData.51 {
  special = language
  #Sprach IDs eintragen
  special.value = 1
  1 = TMENU
  1 {
    NO = 1
    NO {
      stdWrap.cObject = TEXT
      stdWrap.cObject {
        value = en 
      }
      linkWrap = <link rel="alternate" hreflang="|
      doNotLinkIt = 1
      after.cObject = TEXT
      after.cObject {
        stdWrap {
          wrap = " href="|" />
          typolink {
            parameter.data = page:uid
            additionalParams = &L=1
            returnLast = url
            #inkl baseurl
            forceAbsoluteUrl = 1
            #z.b. tt_news
            addQueryString = 1
            addQueryString{
                exclude = L,id,cHash,no_cache
                method = GET
            }
            useCacheHash = 0
            no_cache = 0
          }
        }
      }
    }
    CUR = 1
    CUR {
      doNotShowLink = 1
    }
  }
}
[global]
[globalVar = GP:L = 1]
page.headerData.51 = HMENU
page.headerData.51 {
  special = language
  #Sprach IDs eintragen
  special.value = 0
  1 = TMENU
  1 {
    NO = 1
    NO {
      stdWrap.cObject = TEXT
      stdWrap.cObject {
        value = de 
      }
      linkWrap = <link rel="alternate" hreflang="|
      doNotLinkIt = 1
      after.cObject = TEXT
      after.cObject {
        stdWrap {
          wrap = " href="|" />
          typolink {
            parameter.data = page:uid
            additionalParams = &L=0
            returnLast = url
            #inkl baseurl
            forceAbsoluteUrl = 1
            #z.b. tt_news
            addQueryString = 1
            addQueryString{
                exclude = L,id,cHash,no_cache
                method = GET
            }
            useCacheHash = 0
            no_cache = 0
          }
        }
      }
    }
    CUR = 1
    CUR {
      doNotShowLink = 1
    }
  }
}
[global]


config {
    # default language
    defaultGetVars.L = 0
    uniqueLinkVars = 1
    sys_language_uid = 0
    htmlTag_langKey = de
    metaCharset = utf-8
    language = de
    locale_all = de_DE.utf-8
    
    sys_language_mode = strict
    sys_language_overlay = 0

    linkVars = L
    prefixLocalAnchors = all
    extTarget = _blank
    htmlTag_setParams = lang="de" dir="ltr" class="no-js"
    # cat=bootstrap package: advanced/150/110; type=boolean; label=No Cache
    no_cache = 0
    # cat=bootstrap package: advanced/150/120; type=options[Do not remove=0,Remove=1,Move to external file=external]; label=Remove default JavaScript
    removeDefaultJS = 1
    # cat=bootstrap package: advanced/150/130; type=boolean; label=Compress JavaScript
    compressJs = 1
    # cat=bootstrap package: advanced/150/140; type=boolean; label=Compress Css
    compressCss = 1
    # cat=bootstrap package: advanced/150/150; type=boolean; label=Concatenate JavaScript
    concatenateJs = 1
    # cat=bootstrap package: advanced/150/160; type=boolean; label=Concatenate Css
    concatenateCss = 1
    # Seitentitel entfernen
    noPageTitle = 0
    # cache always expires at midnight 
    cache_clearAtMidnight = 1
}
[applicationContext = Development*]
config {
    no_cache = 1
    compressJs = 0
    compressCss = 0
    concatenateJs = 0
    concatenateCss = 0
}
[end]


[PIDupinRootline = 6] && [globalVar = TSFE:id != 490]
config.noPageTitle = 1
page {
    meta{
        description >
        robots >
        application-name >
        keywords >
    }
}
[end]

# English language, sys_language.uid = 1
[globalVar = GP:L = 1]
config.sys_language_uid = 1
config.language = en
config.locale_all = en_US.UTF-8
config.htmlTag_langKey = en
config.htmlTag_setParams = lang="en" dir="ltr" class="no-js"
[global]


/*
 Main.html (search)
*/
# German language
temp.language = TEXT
temp.language.value = de

# English language
[globalVar = GP:L = 1]
  temp.language.value = en
[global]
lib.language < temp.language