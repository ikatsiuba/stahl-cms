page.includeJS{
    jquery = fileadmin/default/js/jquery-2.2.4.min.js
    file1 = fileadmin/default/js/bootstrap.min.js
    file2 = https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBfOm1hcWucKdqdjkSya7KL2Keg6M0t3mQ
}

page.includeJSFooterlibs.jquery >
page.includeJSFooterlibs.bootstrap >

page.includeJSFooter {
    file9 = fileadmin/default/js/jquery.lazy.min.js
    file1 = fileadmin/default/js/html5lightbox/html5lightbox.js
    file11 = fileadmin/default/js/jscrollpane.min.js
    file2 = fileadmin/default/js/owl.carousel.min.js
    file3 = fileadmin/default/js/owl.carousel.js
    file4 = fileadmin/default/js/jquery.fs.selecter.min.js
    file5 = fileadmin/default/js/master.js
    file6 = fileadmin/default/js/develop.js
    file7 = fileadmin/default/js/responsive-tables.js
    file8 = fileadmin/default/templates/aimeos/Public/Themes/Stahl/products_catalog.js
}

[applicationContext = Production/Live]
page.includeJSFooter {
    file9 = fileadmin/default/js/leadinspektor.js
}
[end]

[PIDupinRootline = 6]
  page.includeJS {
      file22 = fileadmin/default/js/gsap/TweenLite.min.js
      file23 = fileadmin/default/js/gsap/Draggable.min.js
      file24 = fileadmin/default/js/gsap/CSSPlugin.min.js
  }
[end]