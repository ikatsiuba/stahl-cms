page.shortcutIcon = fileadmin/user_upload/gfx/favicon.ico


plugin.tx_news {
        settings {
                link {
                        skipControllerAndAction = 1
                }
        }
}

[globalVar = GP:tx_news_pi1|news > 0]
config.defaultGetVars {
        tx_news_pi1 {
                controller=News
                action=detail
        }
}
[global]

page.headerData.20 = TEXT
page.headerData.20.value (
   <script type="text/javascript" src="//fast.fonts.net/jsapi/9ee0f449-f9be-469a-9c11-c449220b25e0.js"></script>
)

page.20 = CONTENT
page.20 {
  stdWrap.required = 1
    table = sys_category
    select {
      uidInList= 5
        pidInList = root,-1
    selectFields = sys_category.*
    }
    renderObj = COA
    renderObj {
        10 = TEXT
        10.field = title
        10.wrap2  =  <h1> | </h1>
    }
}
page.config.contentObjectExceptionHandler = 0

lib.pageid = TEXT
lib.pageid.data =  getIndpEnv:TYPO3_REQUEST_URL

lib.pageidtitel = RECORDS
lib.pageidtitel {
    source.data = page:uid
    tables = pages
    conf.pages = TEXT
    conf.pages.field = title
	stdWrap.replacement {
	10 {
      search.char = 32
      replace = -
    }
	}
}

temp.LocationSearch = USER
temp.LocationSearch {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    extensionName = LocationSearch
    pluginName = Locations
	vendorName = Havasww
    view < plugin.tx_location_search.view
    persistence < plugin.tx_location_search.persistence
    settings < plugin.tx_location_search.settings
}
 
location = COA
location {
     15 < temp.LocationSearch
}

temp.LocationContact = USER
temp.LocationContact {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    extensionName = LocationSearch
    pluginName = Contactperson
	vendorName = Havasww
    view < plugin.tx_location_search.view
    persistence < plugin.tx_location_search.persistence
    settings < plugin.tx_location_search.settings
}


location_contact = COA
location_contact {
     15 < temp.LocationContact
}


footer_info = CONTENT
footer_info {
	  table = tt_content
	  select {
			pidInList = 400
			languageField = sys_language_uid
			orderBy = sorting
			where = colPos= 0
		}
}

kontaktformular = CONTENT
kontaktformular {
  table = tt_content
  slide = -1
  select {
  languageField = sys_language_uid
    orderBy = sorting
    where = colPos= 10
}
}


newsletter = CONTENT
newsletter {
	  table = tt_content
	  select {
			pidInList = 14
			languageField = sys_language_uid
			orderBy = sorting
			where = colPos= 0
		}
}
callback_form = CONTENT
callback_form {
  table = tt_content
	select {
	languageField = sys_language_uid
		pidInList = 2
		orderBy = sorting
		where = colPos= 0
}
}

sticky_contact = CONTENT
sticky_contact {
  table = tt_content
  slide = -1
	select {
	languageField = sys_language_uid
		orderBy = sorting
		where = colPos= 11
}
}
copyright = TEXT
copyright {
  data = date : U
  strftime = %Y
  noTrimWrap = |&copy; | R. STAHL AG|
}

####################
#### NAVIGATION ####
####################

navitopleft = COA
navitopleft {
    10 = HMENU
    10 {
      special = list
      special.value = 5,4
        1 = TMENU
        1 {
            wrap =   <ul class="nav navbar-nav navbar-main">   |  </ul>
            expAll = 1
            noBlur = 1
      
            NO = 1
            NO {
                ATagTitle.field =  title 
                ATagBeforeWrap = 1
                allWrap =  <li> | </li>
            }
      
      ACT < .NO
            ACT {
               ATagParams = class="active"
              allWrap = <li> | </li>
            }
        }
    }
}


navitopright = COA
navitopright {
    10 = HMENU
    10 {
      special = directory
      special.value = 13
      entryLevel = 2
        1 = TMENU
        1 {
           wrap =  <ul class="nav navbar-nav navbar-main"> |  </ul>
           expAll = 1
           noBlur = 1
           NO = 1
           NO {
				ATagTitle.field =  title
				ATagBeforeWrap = 1
				allWrap =  <li class="{field:subtitle}"> | </li>
				allWrap.insertData = 1
               }
      
			ACT < .NO
			ACT {
				ATagParams = class="active"
				}
			}
		}
}

############################
#### NAVIGATION MOBIL ####
############################
product_nav_mobile = COA
product_nav_mobile {

    20 = HMENU
    20 {
      special = directory
	  special.value = 5

	  1 = TMENU
	  1 {
		expAll = 1
		NO{
			allWrap = <li>|</li>
			ATagTitle.field =  title
		}
		ACT = 1
		ACT{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li class="dropdown">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="dropdown active">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		#wrap = <ul class="nav nav-pills produkte">|</ul>
		
		 # NO + Übersetzung nicht vorhanden
		USERDEF1 < .NO
		USERDEF1 {
					wrapItemAndSub = <li class="block_language" > | </li>
					doNotLinkIt = 1
				}
	    }
	  2 < .1
	  2 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <ul class="dropdown-menu">|</ul>
	    } 
    }
    wrap = <ul class="nav nav-pills produkte_mobile">|</ul>
}
corporate_nav_en_mobile = HMENU
corporate_nav_en_mobile {
	  special = list
	  special.value = 11,23,24,598
	  1 = TMENU
	  1 {
		expAll = 1
		NO{
			allWrap = <li>|</li>
			ATagTitle.field =  title
		}
		ACT = 1
		ACT{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.languageField =  title
		}
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li class="dropdown">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="dropdown active">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		
		
		 # NO + Übersetzung nicht vorhanden
		USERDEF1 < .NO
		USERDEF1 {
					wrapItemAndSub = <li class="block_language" > | </li>
					doNotLinkIt = 1
				}
	  }
	  2 < .1
	  2 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <ul class="dropdown-menu">|</ul>
	  }  
	  wrap = <ul class="nav nav-pills unternehmen_mobile">|</ul>
}
corporate_nav_mobile = HMENU
corporate_nav_mobile {
	  special = list
	  special.value = 11,23,24,598,25
	  1 = TMENU
	  1 {
		expAll = 1
		NO{
			allWrap = <li>|</li>
			ATagTitle.field =  title
		}
		ACT = 1
		ACT{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.languageField =  title
		}
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li class="dropdown">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="dropdown active">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		
		
		 # NO + Übersetzung nicht vorhanden
		USERDEF1 < .NO
		USERDEF1 {
					wrapItemAndSub = <li class="block_language" > | </li>
					doNotLinkIt = 1
				}
	  }
	  2 < .1
	  2 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <ul class="dropdown-menu">|</ul>
	  } 
	  wrap = <ul class="nav nav-pills unternehmen_mobile">|</ul>
}

############################
#### NAVIGATION DESKTOP ####
############################
corporate_nav = HMENU
corporate_nav {
	  special = list
	  special.value = 11,23,24,598,25
	  1 = TMENU
	  1 {
		expAll = 1
		NO{
			allWrap = <li>|</li>
			ATagTitle.field =  title
		}
		ACT = 1
		ACT{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.languageField =  title
		}
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li class="dropdown">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="dropdown active">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		wrap = <ul class="nav nav-pills unternehmen">|</ul>
		
		 # NO + Übersetzung nicht vorhanden
		USERDEF1 < .NO
		USERDEF1 {
					wrapItemAndSub = <li class="block_language" > | </li>
					doNotLinkIt = 1
				}
	  }
	  2 < .1
	  2 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <span class="wrapper-dropdown-menu"><ul class="dropdown-menu second-level scroll-pane">|</ul></span>
	  }
	  3 < .1
	  3 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <ul class="third-level">|</ul>
	  }
	  4 < .1
	  4 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <ul class="fourth-level">|</ul>
	  }
}
##EN
corporate_nav_en = HMENU
corporate_nav_en {
	  special = list
	  special.value = 11,23,24,598
	  1 = TMENU
	  1 {
		expAll = 1
		NO{
			allWrap = <li>|</li>
			ATagTitle.field =  title
		}
		ACT = 1
		ACT{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.languageField =  title
		}
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li class="dropdown">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="dropdown active">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		wrap = <ul class="nav nav-pills unternehmen">|</ul>
		
		 # NO + Übersetzung nicht vorhanden
		USERDEF1 < .NO
		USERDEF1 {
					wrapItemAndSub = <li class="block_language" > | </li>
					doNotLinkIt = 1
				}
	  }
	  2 < .1
	  2 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <span class="wrapper-dropdown-menu"><ul class="dropdown-menu second-level scroll-pane">|</ul></span>
	  }
	  3 < .1
	  3 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <ul class="third-level">|</ul>
	  }
	  4 < .1
	  4 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <ul class="fourth-level">|</ul>
	  }  
}


temp.productMenu = USER
temp.productMenu {
    userFunc = Aimeos\Aimeosext\Hooks\GoogleSitemap\Sitemap->printProductMenu
}



product_nav = COA
product_nav {

    20 = HMENU
    20 {
      special = directory
	  special.value = 5
	  excludeUidList = 6
	  1 = TMENU
	  1 {
		expAll = 1
		NO{
			allWrap = <li>|</li>
			ATagTitle.field =  title
		}
		ACT = 1
		ACT{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li class="dropdown">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="dropdown active">|</li>
			ATagTitle.field =  title
			ATagParams = class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true"
		}
		#wrap = <ul class="nav nav-pills produkte">|</ul>
		
		 # NO + Übersetzung nicht vorhanden
		USERDEF1 < .NO
		USERDEF1 {
					wrapItemAndSub = <li class="block_language" > | </li>
					doNotLinkIt = 1
				}
	    }
	  2 < .1
	  2 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <span class="wrapper-dropdown-menu"><ul class="dropdown-menu second-level scroll-pane">|</ul></span>
	    }
	  3 < .1
	  3 {
		IFSUB = 1
		IFSUB{
			wrapItemAndSub = <li>|</li>
		}
		ACTIFSUB = 1
		ACTIFSUB{
			wrapItemAndSub = <li class="active">|</li>
			ATagTitle.field =  title
		}
		wrap = <ul class="third-level">|</ul>
	    }	      
      4 < .1
      4 {
    	IFSUB = 1
    	IFSUB{
    		wrapItemAndSub = <li>|</li>
    	}
    	ACTIFSUB = 1
    	ACTIFSUB{
    		wrapItemAndSub = <li class="active">|</li>
    		ATagTitle.field =  title
    	}
    	wrap = <ul class="fourth-level">|</ul>
    	}	      
    }
    wrap = <ul class="nav nav-pills produkte">|</ul>
}
product_nav {
     10 < temp.productMenu
}


footer_nav = COA
footer_nav {
  10 = HMENU
  10 {
     special = directory
     special.value = 16
     1 = TMENU
     1 {
        wrap =  <ul class="nav-footer"> |  </ul>
        expAll = 1
        noBlur = 1
        NO = 1
        NO {
           ATagTitle.field =  title
           ATagBeforeWrap = 1
           allWrap =  <li> | </li>
           }
	   ACT < .NO
          ACT {
              ATagParams = class="active"
            }
        }
    }
}
language_switch = COA_INT
language_switch {
10 = HMENU
10{ 
	special = language
	special {
		value = 0,1
		#normalWhenNoLanguage = 0
	}	
	wrap = <ul class="langnavi">|</ul>
	1 = TMENU
	1 {
	noBlur = 1
	NO = 1
	NO {
		linkWrap = <li class="xinactive"> | </li>
		doNotLinkIt = 1  
		stdWrap{
			override = DE || EN
			typolink{
				parameter.data = page:uid
				additionalParams = &L=0 || &L=1 
				
				#additionalParams.data = &L=0 || &L=1  || &ai[controller]&ai[action]&ai[d_prodid]&ai[l_pos] || &ai[controller]&ai[action]&ai[d_prodid]&ai[l_pos]&ai[d_parent] 
				
				addQueryString = 1
				addQueryString{
					exclude = L,id,cHash,no_cache
					method = GET
				}
				useCacheHash = 0
				no_cache = 0
			}
		}
	}
	ACT <.NO
	ACT = 1
	ACT.linkWrap = <li class="active">|</li>
	# NO + Übersetzung nicht vorhanden

	USERDEF1 < .NO
	 USERDEF1 {
        doNotLinkIt = 1
        stdWrap {
          typolink {
            # Fallback-Zielseite (hier Startseite)
            parameter = 1
            additionalParams = &L=1
            ATagParams = class="lang-switch"
          }
        }
      }
	# ACT + Übersetzung nicht vorhanden
	USERDEF2 < .ACT
	USERDEF2.doNotLinkIt = 1

		}
	}
}

[applicationContext = Production/Live] && [globalVar = GP:L = 0]
page.headerData.100 = TEXT
page.headerData.100.value (
<script type="text/javascript">
/*<![CDATA[*/
/*TS_inlineFooter*/
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-7121365-1', 'auto');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

/*]]>*/
</script>
)
[global]
[applicationContext = Production/Live] && [globalVar = GP:L = 1]
page.headerData.100 = TEXT
page.headerData.100.value (
<script type="text/javascript">
/*<![CDATA[*/
/*TS_inlineFooter*/
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-7121365-3', 'auto');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

/*]]>*/
</script>
)
[global]
[applicationContext = Production/Staging] && [globalVar = GP:L = 0]
page.headerData.100 = TEXT
page.headerData.100.value (
<script type="text/javascript">
/*<![CDATA[*/
/*TS_inlineFooter*/
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-7121365-21', 'auto');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

/*]]>*/
</script>
)
[global]
[applicationContext = Production/Staging] && [globalVar = GP:L = 1]
page.headerData.100 = TEXT
page.headerData.100.value (
<script type="text/javascript">
/*<![CDATA[*/
/*TS_inlineFooter*/
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-7121365-22', 'auto');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

/*]]>*/
</script>
)
[global]
[applicationContext = Development*]
    page {
        meta {
            robots   = noindex,nofollow
        }
    }
# Content object one:
page.1 = TEXT
page.1.value = <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NRR9XK2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
page.headerData.110 = TEXT
page.headerData.110.value (
<script type="text/javascript">
/*<![CDATA[*/
/*TS_inlineFooter*/
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NRR9XK2');

/*]]>*/
</script>
)
[global]
[applicationContext = Production/PreProd] && [globalVar = GP:L = 0]
page.1 = TEXT
page.1.value = <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KTK6PJ2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
page.headerData.110 = TEXT
page.headerData.110.value (
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KTK6PJ2');</script>
)
[global]
[applicationContext = Production/PreProd] && [globalVar = GP:L = 1]
page.1 = TEXT
page.1.value = <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KTK6PJ2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
page.headerData.110 = TEXT
page.headerData.110.value (
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KZKJ6VM');</script>
)
[global]
[applicationContext = Production/Live]
siteimproveanalytics = TEXT
siteimproveanalytics.value(
<script type="text/javascript">
/*<![CDATA[*/
(function() {
var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;
sz.src = '//siteimproveanalytics.com/js/siteanalyze_6037133.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);
})();
/*]]>*/
</script>
)
[global]
avaya_chat_de = TEXT
avaya_chat_de.value(
	<li class="sharing chat chat_de" id="avayaWebChatScript" lang-attr="DE" script-src-attr="https://awcs.stahl.de:443/WebChatApp/AvayaWebChat.js">
		<div><span class="icon-googleplus"></span></div>
		<div class="layout_sticky_more chat_more"><span>Chat Starten</span></div>
		<div class="layout_sticky_content chat_de"></div>
	</li>
)

avaya_chat_en = TEXT
avaya_chat_en.value(
	<li class="sharing chat chat_en" id="avayaWebChatScript" lang-attr="EN" script-src-attr="https://awcs.stahl.de:443/WebChatApp/AvayaWebChat.js">
	<div><span class="icon-googleplus"></span></div>
		<div class="layout_sticky_more chat_more"><span>Chat Start</span></div>
		<div class="layout_sticky_content chat_en"></div>
	</li>
)
