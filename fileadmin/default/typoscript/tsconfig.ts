<INCLUDE_TYPOSCRIPT: source="../fileadmin/default/templates/bootstrap/Private/WebLayout/BackendLayouts" extensions="txt">
TCEFORM.tt_content.section_frame {
     
     addItems.22 = Inner Content
     addItems.23 = Modul
	 addItems.24 = NEWS Change
	 
}

tx_powermail.flexForm.type.addFieldOptions.jhcaptcharecaptcha = reCAPTCHA (jh_captcha)

RTE.classes {
	small {
        name = Small
    }
	medium {
        name = Medium
    }
	more_space {
		name = Großer Abstand
	}
}
RTE.default.proc {
	allowedClasses (
        table, table-striped, table-bordered, table-hover, table-condensed,
        small, medium, more_space,
        active, success, warning, danger, info,
        list-unstyled, list-inline,
        btn, btn-default, btn-primary, btn-success, btn-warning, btn-block,
        visible-lg, visible-md, visible-print, visible-sm, visible-xs
    )
}
RTE.default.proc.entryHTMLparser_db {
    tags {
        p.fixAttrib.class.list      = ,small, medium, more_space
	}
}
RTE.default {
    contentCSS = fileadmin/default/templates/bootstrap/Public/Css/RTE/rte.css
}

tx_news.templateLayouts {
	0 = Default List View
	1 = TE-02: Newsbox L
	2 = TE-02: Newsbox horizontal HOME
	3 = TE-02: Newsbox horizontal 
	4 = TE-03: Newsbox vertikal
	5 = List View
	6 = Date Menu
	7 = Categories Blue
	8 = Categories Gray
}