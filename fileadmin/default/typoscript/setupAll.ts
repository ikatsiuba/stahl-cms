<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/page/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/page/config/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/page/includejs/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/page/includecss/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/solr/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/powermail/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/aimeos/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/aimeosext/setup.ts">