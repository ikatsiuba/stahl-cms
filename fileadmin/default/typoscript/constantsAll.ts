<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/bootstrap/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/news/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/powermail/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/calendarize/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:fileadmin/default/typoscript/extension/jh_captcha/constants.ts">

page.meta.viewport = width=device-width, initial-scale=1, maximum-scale=1
plugin.tx_felogin_pi1.storagePid = 0
config.admPanel = 0