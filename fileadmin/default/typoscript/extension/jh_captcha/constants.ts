plugin.tx_jhcaptcha {
    settings {
        reCaptcha {
            siteKey = 6Ld0kxEUAAAAABjJvjOdcbnDwTT6wIQGRL0vB9ME
            secretKey = 6Ld0kxEUAAAAAH754-wB51JqzjY5KtiTG8ObxgTA
			size = compact
			lang = config.language
        }
    }
} 	

# German language, sys_language.uid = 0
[globalVar = GP:L = 0]
plugin.tx_jhcaptcha {
    settings {
        reCaptcha {
			lang = de
		}
	}
}
[global]
# English language, sys_language.uid = 1
[globalVar = GP:L = 1]
plugin.tx_jhcaptcha {
    settings {
        reCaptcha {
			lang = en
		}
	}
}
[global]
