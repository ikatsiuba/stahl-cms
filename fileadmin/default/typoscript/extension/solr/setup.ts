config.index_enable = 1
plugin {
    tx_solr {
        index {
            enableFileIndexing = 1
            queue {
                pages {
                    fields {
                        pageHierarchy_stringM = SOLR_MULTIVALUE
                        pageHierarchy_stringM {
                            value = {leveltitle:3}/
                            insertData = 1
                        }
                        productHierarchy_stringM = SOLR_MULTIVALUE
                        productHierarchy_stringM {
                            value = 
                        }
                    }
                }
            }
            fieldProcessingInstructions {
                pageHierarchy_stringM = pathToHierarchy
                productHierarchy_stringM = pathToHierarchy
            }
        }
        solr {
            host = localhost
            port = 8983
        }
        search {
                results {
			resultsHighlighting = 1
                        resultsHighlighting {
				highlightFields = content
				fragmentSize = 180
				fragmentSeparator = [...]

				wrap = <span class="results-highlight">|</span>
			}
			siteHighlighting = 1
                }
		sorting = 1
		sorting {
			defaultOrder = asc

			options >
                        options {
				relevance {
					field = relevance
					label = Relevance
				}

				title {
					field = sortTitle
					label = Title
				}

				type {
					field = type
					label = Type
				}
			}
		}
		lastSearches = 1
		lastSearches {
			limit = 10
			mode = global
		}
		frequentSearches = 0
		frequentSearches {
			limit = 10
                }
                faceting = 1
                faceting {
                    minimumCount = 2
                    facets {
			type {
                            label = ContentType
                            field = type
                            type = options
			}

                        pageHierarchy {
                            field = pageHierarchy_stringM
                            label = Scopes

                            type = hierarchy
                            hierarchy = HMENU
                            hierarchy {
                                1 = TMENU
                                1 {
                                    NO = 1
                                    NO {
                                        wrapItemAndSub = <li>|</li>
                                    }
                                }
                            }
                        }

                        productHierarchy {
                            field = productHierarchy_stringM
                            label = ProductCategories

                            type = hierarchy
                            hierarchy = HMENU
                            hierarchy {
                                1 = TMENU
                                1 {
                                    NO = 1
                                    NO {
                                        wrapItemAndSub = <li>|</li>
                                    }
                                }
                            }
                        }
                    }
                }
        }
        statistics = 1
    }
    tx_solrfluid {

	view {
		 layoutRootPaths.10 = fileadmin/default/templates/solr/Private/Layouts/
		 partialRootPaths.10 = fileadmin/default/templates/solr/Private/Partials/
		 templateRootPaths.10 = fileadmin/default/templates/solr/Private/Templates/

		# important! else linkbuilding will fail
		pluginNamespace = tx_solr
	}
    }
}

[globalVar = GP:L = 0]
plugin {
    tx_solr {
        solr {
            path = /solr/core_de/
	}
        search {
            sorting {
                options {
                    relevance {
                        label = Relevanz
                    }
                    title {
                        label = Alphabetisch
                    }   
                    type >                
		}
            }
        }
    }
}
[end]
[globalVar = GP:L = 1]
plugin {
    tx_solr {
        solr {
            path = /solr/core_en/
	}
    }
}
[end]
[applicationContext = Production/Live]
plugin {
    tx_solr {
        solr {
            host = web59.serverdienst.net
            username = admin
            password = stahl2017
            port = 8983
	}
    }
}
[end]
[applicationContext = Production/PreProd]
plugin {
    tx_solr {
        solr {
            host = web59.serverdienst.net
            username = admin
            password = stahl2017
            port = 8984
	}
    }
}
[end]
page.includeCSS {
	search = fileadmin/default/templates/solr/Public/Css/results.css
}