plugin.tx_news {
	view {
		 # cat=plugin.tx_news/file; type=string; label=Path to template root (FE)
		templateRootPath = fileadmin/default/templates/news/Private/Templates/
		 # cat=plugin.tx_news/file; type=string; label=Path to template partials (FE)
		partialRootPath = fileadmin/default/templates/news/Private/Partials/
		 # cat=plugin.tx_news/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = fileadmin/default/templates/news/Private/Layouts/
	}		 
}