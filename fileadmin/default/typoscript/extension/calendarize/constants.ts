plugin.tx_calendarize{
 	view {	
		# cat=calendarize/file; type=string; label=Path to template root (FE)
 		templateRootPath = fileadmin/default/templates/calendarize/Private/Templates/
 		# cat=calendarize/file; type=string; label=Path to template partials (FE)
 		partialRootPath = fileadmin/default/templates/calendarize/Private/Partials/
 		# cat=calendarize/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = fileadmin/default/templates/calendarize/Private/Layouts/
 	}
}