plugin.bootstrap_package {
	settings {
		less {
			link-hover-color = darken(@link-color, 20%)
		}
	}
}
page {
	fluidtemplate {
		# cat=bootstrap package: advanced/100/100; type=string; label=Layout Root Path: Path to layouts
		layoutRootPath = fileadmin/default/templates/bootstrap/Private/Layouts/Page/
		# cat=bootstrap package: advanced/100/110; type=string; label=Partial Root Path: Path to partials
		partialRootPath = fileadmin/default/templates/bootstrap/Private/Partials/Page/
		# cat=bootstrap package: advanced/100/120; type=string; label=Template Root Path: Path to templates
		templateRootPath = fileadmin/default/templates/bootstrap/Private/Templates/Page/
	}
}

plugin.bootstrap_package_contentelements {
	view {
		# cat=bootstrap package: advanced/140/layoutRootPath; type=string; label=Layout Root Path: Path to layouts
		layoutRootPath = fileadmin/default/templates/bootstrap/Private/Layouts/ContentElements/
		# cat=bootstrap package: advanced/140/partialRootPath; type=string; label=Partial Root Path: Path to partials
		partialRootPath = fileadmin/default/templates/bootstrap/Private/Partials/ContentElements/
		# cat=bootstrap package: advanced/140/templateRootPath; type=string; label=Template Root Path: Path to templates
		templateRootPath = fileadmin/default/templates/bootstrap/Private/Templates/ContentElements/
	}
}