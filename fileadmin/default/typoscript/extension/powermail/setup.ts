plugin.tx_powermail.view {
    partialRootPath >
    partialRootPaths {
        10 = fileadmin/default/templates/powermail/Private/Partials/
        20 = fileadmin/default/templates/jh_captcha/Private/Powermail/Partials/Jhcaptcharecaptcha
    }
}
page.includeJS{
	g_recaptcha_init = fileadmin/default/js/grecaptcha.js
}

page.includeJS{
	g_recaptcha = https://www.google.com/recaptcha/api.js?hl=de-DE&onload=CaptchaCallback&render=explicit
}
# English language, sys_language.uid = 1
[globalVar = GP:L = 1]
page.includeJS{
	g_recaptcha = https://www.google.com/recaptcha/api.js?hl=en-EN&onload=CaptchaCallback&render=explicit
}
[global]