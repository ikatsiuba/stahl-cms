plugin.tx_powermail {
 	view {
 		# cat=powermail_main/file; type=string; label= Path to template root (FE)
 		templateRootPath = fileadmin/default/templates/powermail/Private/Templates/
 
 		# cat=powermail_main/file; type=string; label= Path to template partials (FE)
 		partialRootPath = fileadmin/default/templates/powermail/Private/Partials/
 
 		# cat=powermail_main/file; type=string; label= Path to template layouts (FE)
 		layoutRootPath = fileadmin/default/templates/powermail/Private/Layouts/
 	}

	settings {
		styles{
			bootstrap{
				fieldWrappingClasses = input-container
			}
		}
	}
}