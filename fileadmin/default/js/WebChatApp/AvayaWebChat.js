/*!
* 
* AvayaWebChat.js
* Copyright 2016 Avaya Inc. All Rights Reserved. 
* Usage of this source is bound to the terms described 
* in https://support.avaya.com/licenseinfo
* Avaya - Confidential & Proprietary. 
*/

function enableAvayaWebChat(customlanguage){

    // get the port number and server by creating a a href with the javascript src.
    var parser = document.createElement('a');
    parser.href = document.getElementById('avayaWebChatScript').src;

    // set local variables with the correct port and server ip retrieved earlier from the scripts src
    var Host=parser.hostname; // server ip
    var Portno=parser.port; // get the port
    var Protocol=parser.protocol; // get the protocol

    // verify if user added custom styles or not
    if(customlanguage != ""){
        language = customlanguage;
    }else{
        language = "DE";
    }
       
    /* (DID) DETAILED INTEGRATOR DOCUMENATION
     * Now the code starts that loads the first html and javascript code that makes up web chat. This means
     * when going to chat.html (which is loaded from the code below). You will find he code and the style sheets
     * that make up the chat startup and the chat conversation dialog.
     */

    // Create the actual iframe and add necessary properties. After append it to view
    var iframe = document.createElement('iframe');
    iframe.src = "/fileadmin/default/js/WebChatApp/chat_" + language + ".html";
    iframe.id ="iframe";
    /* (DID) DETAILED INTEGRATOR DOCUMENATION
     * as this is the iframe of the chat startup dialog, this is the starting point to move the location of the
     * dialog to another location on the screen. In addition the main size is defined (be sure to change the
     * values below accordingly.
     */
    iframe.width = 550;
    iframe.height = 420;
    iframe.frameBorder = 0;
    iframe.style.position = "fixed";
    iframe.style.right = "68px";
    iframe.style.top = "50%";
	iframe.style.marginTop = "-105px";
	iframe.style.display = "none";
    document.body.appendChild(iframe);

}