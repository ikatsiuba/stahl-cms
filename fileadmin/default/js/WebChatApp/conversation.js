/*******************************************************************************\
*                                                                               *
*  WebChat Integration Sample © 2012 Avaya, All rights reserved.            *
*                                                                               *
\*******************************************************************************/




/*****************************************************************************\
* global variables
\*****************************************************************************/

// scaling options
var m_MaxMessageSize = 1024;
var m_BackendPollLatency = 5000;
var m_MaxFailedRequest = 12;

var m_TopicID = "";
var m_NickName = "Nick";
var m_IQ = "";



/*****************************************************************************\
* main application initializer
\*****************************************************************************/

/**
 * setup the chat window and start the ajax application
 */
function initializeWebchat(maxMessageSize, backendPollLatency, maxFailedRequest)
{
	// setup the globals
	if (typeof(maxMessageSize) == 'number') {
		m_MaxMessageSize = maxMessageSize;
	}
	if (typeof(backendPollLatency) == 'number') {
		m_BackendPollLatency = backendPollLatency;
	}
	if (typeof(maxFailedRequest) == 'number') {
		m_MaxFailedRequest = maxFailedRequest;
	}

	// we need to handle some events so set the handlers
    window.onresize = resizeControls;		// dynamic sizing of controls is required
    window.onunload = closeConversation;	// catch page close to do clean up tasks
	window.onbeforeunload = confirmExit;	// therby we can ask the user to confirm

	// calculate and set the sizing initially
	resizeControls();

	var openerDOM = opener.window.document;
	if (!openerDOM) {
		alert(ChatI18N_SysPopup_MissingParent);
		return;
	}
    var topicID = openerDOM.getElementById("ChatLauncherTopicID").value;
    if (topicID && topicID.length > 0) {
    	m_TopicID = comergo_jsgenerics_util.trim(topicID);
    } else {
    	alert(ChatI18N_SysPopup_MissingTopic);
    	return;
    }
    var nick = openerDOM.getElementById("ChatLauncherNickName").value;
    if (nick && nick.length > 0) {
    	m_NickName = comergo_jsgenerics_util.trim(nick);
    }
    var iq = openerDOM.getElementById("ChatLauncherEmail").value;
    if (iq && iq.length > 0) {
    	m_IQ = comergo_jsgenerics_util.trim(iq);
    }
    var iq1 = openerDOM.getElementById("ChatLauncherNumber").value;
    if (iq1 && iq1.length > 0) {
    	m_IQ1 = comergo_jsgenerics_util.trim(iq1);
    }

	
    // looks nice if the control lights up when active
    var inputControl = document.getElementById("ChatSendInput");
    inputControl.onfocus = function(){this.style.border = '2px solid #FFA500';};
    inputControl.onblur = function(){this.style.border = '1px solid #C0C0C0';};

    // chat some state messages
    writeMessageToScreen('<span class="ChatSystemMessage">' + ChatI18N_SysMsg_Connecting.replace(/\{Topic\}/, m_TopicID) + '</span><br />');
    // Show Initial Question in Conversation page
	writeMessageToScreen('<span class="ChatRecordOutSender">' + m_NickName + '</span>');
	writeMessageToScreen('<span class="ChatRecordOutText">' + "Email -> " + m_IQ + "    Telefonnummer -> " + m_IQ1 + '</span><br />');

	try {
		var uri = "WebChatBackend";
		var data = "Operation=startConversation&TopicID=" + encodeURIComponent(m_TopicID) + "&Nick=" + encodeURIComponent(m_NickName) + "&Message=" + encodeURIComponent(m_IQ);
		comergo_jsgenerics_ajax.post(uri, data, onConnected, onConnectFailed);
    } catch (error) {
	    alert(ChatI18N_SysPopup_ErrorStarting + ' ' + error);
    }
}

function onConnected(response) {
	try {
	    // chat some state messages
	    writeMessageToScreen('<span class="ChatSystemMessage">' + ChatI18N_SysMsg_Connected.replace(/\{Topic\}/, m_TopicID) + '</span><br />');
	} catch (error) {}
	try {
	    // enabled message send button -> now the user can chat
		document.getElementById("ChatSendButton").disabled = false;
	} catch (error) {}
    // trigger chat message transfer
	window.setTimeout('comergo_jsgenerics_ajax.post("WebChatBackend", "Operation=receive", updateChatDisplay, updateChatDisplayError)', m_BackendPollLatency);
}

function onConnectFailed(http) {
    // chat some state messages
    writeMessageToScreen('<span class="ChatSystemMessage">' + ChatI18N_SysMsg_ConnectError.replace(/\{Topic\}/, m_TopicID) + '</span><br />');
}



/*****************************************************************************\
* message in and out section
\*****************************************************************************/

/**
 * function that is invoked by the send button
 *
 * it updates the display and submits the message to the server
 */
function send()
{
	var inputControl = document.getElementById("ChatSendInput");
	var message = inputControl.value;
	if (message.length > 0) {
		if (message.length > m_MaxMessageSize) {
			message = message.substr(0, m_MaxMessageSize);
		}
		document.getElementById("ChatSendInput").value = "";
		var uri = "WebChatBackend";
		var data = "Operation=send&Message=" + encodeURIComponent(message);
		comergo_jsgenerics_ajax.post(uri, data, onSendSuccess, onSendFailed);
		escapedMessage = comergo_jsgenerics_util.escapeHTML(message);
		writeMessageToScreen('<div class="received"><span class="ChatRecordOutSender">' + m_NickName + '</span><span class="ChatRecordOutText">' + escapedMessage + '</span></div>');
    }
    document.getElementById("ChatSendInput").focus();
}


function onSendSuccess(response) {
}

function onSendFailed(http) {
    // chat some state messages
    writeMessageToScreen('<span class="ChatSystemMessage">' + ChatI18N_SysMsg_SendError.replace(/\{Topic\}/, m_TopicID) + '</span><br />');
}


/**
 * recurring chat message transfer function
 *
 * the function is triggered once and triggers itself further
 * Messages and states are requested from the server and written
 * to the display
 *
 * @param response
 */
function updateChatDisplay(response) {
	try {
		// cut out state message and write it to screen
		var messages = response.replace(/<span id="ChatDisplayState">.*?<\/span>/, '');
		if (messages && messages.length > 0) {
			writeMessageToScreen(messages);
		}
		var state = response.replace(/<div.*<\/div>/, '');
		if (state && state.length > 0) {
			state = state.replace(/<span id="ChatDisplayState">/, '');
			state = state.replace(/<\/span>/, '');
			if (state === 'active') {
				state = ChatI18N_ChatState_Active;
			} else if (state === 'composing') {
				state = ChatI18N_ChatState_Composing;
			} else {
				state = ChatI18N_ChatState_Gone;
			}
			document.getElementById("ChatDisplayState").innerHTML = state;
		}
	} catch (error) {}
	// re trigger update
	window.setTimeout('comergo_jsgenerics_ajax.post("WebChatBackend", "Operation=receive", updateChatDisplay, updateChatDisplayError)', m_BackendPollLatency);
}

function updateChatDisplayError(http) {
	m_MaxFailedRequest--;
	if (m_MaxFailedRequest == 0) {
		closeConversation();
		return;
	}
	
	try {
	    // chat some state messages
	    writeMessageToScreen('<span class="ChatSystemMessage">' + ChatI18N_SysMsg_ReceiveError + '</span><br />');
		// re trigger update
	} catch (error) {}
	window.setTimeout('comergo_jsgenerics_ajax.post("WebChatBackend", "Operation=receive", updateChatDisplay, updateChatDisplayError)', m_BackendPollLatency);
}




/*****************************************************************************\
*
\*****************************************************************************/

/**
 * we need to resize controls because it is not possible to do this with standard HTML
 * and this is what this function simply does
 */
function resizeControls()
{
	var w = 630;
	var h = 460;

	// first we query the window size because this is browser dependent
    if (typeof(window.innerWidth) == 'number') {
		// Non-IE
        w = window.innerWidth;
        h = window.innerHeight;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
		// IE 6+ in 'standards compliant mode'
        w = document.documentElement.clientWidth;
        h = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
		// IE 4 compatible
        w = document.body.clientWidth;
        h = document.body.clientHeight;
    }

	// now the size of the control can be set
    var height = h - 270;
    if (height > 0) {
    	try { // the DOM may not be loaded already
        	// the height of the display must be set because the height can't be given
        	// with standard html
        	var ChatMessageHistory = document.getElementById("ChatMessageHistory");
            ChatMessageHistory.style.height = (h - 270) + 'px';
            // form controls behave strange with standard html so it
            // is required so set their width, too
            if (w > 140) {
    			var ChatSendInput = document.getElementById("ChatSendInput");
    			ChatSendInput.style.width = (w - 36) + 'px';
    			var ChatSendButton = document.getElementById("ChatSendButton");
    			ChatSendButton.style.width = (w - 36) + 'px';
            };
    	} catch (e) {}
    };
}


/**
 * Writes a message to the ChatDisplay, the message is in common a snippet of html
 * This is a convenience method that takes care about some further
 * tasks when writing a message e.g. scrolling
 *
 * @param message a snippet of html
 */
function writeMessageToScreen(message)
{
    var tb = document.getElementById("ChatMessageHistory");
    tb.innerHTML += message;
	tb.scrollTop = tb.scrollHeight;
}


/**
 * event handler that enables the browsers close confirmation
 *
 * @returns {String} a confirmation message
 */
function confirmExit()
{
	return ChatI18N_SysPopup_ConfirmExit;
}


/**
 * event handler that triggers application clean up
 */
function closeConversation()
{
	comergo_jsgenerics_ajax.post("WebChatBackend", "Operation=closeConversation", onClosedConversation, onClosedConversation);
}


/**
 *
 */
function onClosedConversation()
{
	try {
	    // enabled message send button -> now the user can chat
		document.getElementById("ChatSendButton").disabled = true;
	} catch (error) {}
	try {
	    // chat the new state
	    writeMessageToScreen('<span class="ChatSystemMessage">' + ChatI18N_SysMsg_Closed + '</span><br />');
	} catch (error) {}
}




/*****************************************************************************\
*
\*****************************************************************************/

function checkEnter(e, el)
{
    var characterCode;// literal character code will be stored in this variable

    if (e && e.which) {
        characterCode = e.which;
    } else {
        e = event;
        characterCode = e.keyCode;
    }
    if (characterCode == 13) {
        send();
        return false;
    } else {
        //check we are adding a char and size <= maxsize
        switch (e.keyCode) {
			case 37: // left
				return true;
			case 38: // up
				return true;
			case 39: // right
				return true;
			case 40: // down
				return true;
			case 8: // backspace
				return true;
			case 46: // delete
				return true;
			case 27: // escape
				el.value='';
				return true;
		}

		return (el.value.length < m_MaxMessageSize);
    }
}

function pasteEvent()
{
	try { // catch Mozilla's exceptions
		// Get the Source Element...
		var objTxtBox = window.event.srcElement;
		// Get the pasted value from the clip board..
		var pasteData = window.clipboardData.getData("Text");
		if (pasteData.length + objTxtBox.value.length > m_MaxMessageSize) {
			if (objTxtBox.value.length >= m_MaxMessageSize) {
				// do not paste: Cancel default behavior, ie., The Pasting Functionality..
				event.returnValue = false;
			} else {
				var len = m_MaxMessageSize - objTxtBox.value.length;
				pasteData = pasteData.substr(0, len);

				if (objTxtBox.createTextRange) {
					objTxtBox.focus(objTxtBox.caretPos);
					objTxtBox.caretPos = document.selection.createRange().duplicate();
					if (objTxtBox.caretPos.text.length > 0) {
						objTxtBox.caretPos.text = objTxtBox.caretPos.text;
					} else {
						objTxtBox.caretPos.text = pasteData;
					}
				} else {
					objTxtBox.value += pasteData;
				}
				// Already pasted: Cancel default behavior, ie., The Pasting Functionality..
				event.returnValue = false;
			}
		}
	} catch(error) {
		;
	}
}

function beforePasteEvent()
{
		try { // catch Mozilla's exceptions
		event.returnValue = false;
	} catch(error) {
		;
	}
}

