$(document).ready(function () {
    /***************Mail*****************/
    $(".tx_locationsearch_direct_contact .show_callback_form").click(function () {
        var text = $(this).parents(".tx-location-search").find(".email a").html()
        console.log(text);
        $("#powermail_field_receivermail").val(text);
    });





    /*******teaserboxen_volle_breite_tabs slider R.H*******/

    var checkByUrl = $(document)[0].URL.split('/');

    if ( $('body').hasClass('page-5') || ( checkByUrl[3] == "global" && ( checkByUrl[4] == "produkte" || checkByUrl[4] == "products" ) ) ) {

        $('#custom-tab-slide .tab-content').addClass('carousel-inner').find('.tab-pane').addClass('item');
        $('#custom-tab-slide.carousel').carousel({interval: false});
        $('#custom-tab-slide .carousel-inner > .tab-pane').css('padding', '0');

        function fullSizeEvent() {
            $('#custom-tab-slide .tab-pane').on('click', function (e) {
                var eleId = $('#custom-tab-slide .item.active').attr('id');
                $('#custom-tab-slide #myTab ').find('a[href*="' + eleId + '"]').click();
            });
        };

        function ipadSizeEvent() {
            $('#custom-tab-slide .tab-pane').on('touchend', function (e) {
                var eleId = $('#custom-tab-slide .item.active').attr('id');
                $('#custom-tab-slide #myTab ').find('a[href*="' + eleId + '"]').click();
            });
        };

        function mobileSizeEvent() {
            $('#custom-tab-slide .tab-content ').on('touchend', function () {
                var elementChange = $('#custom-tab-slide #myTab');

                elementChange.find('.tab').removeClass('active');
                setTimeout(function () {
                    var eleId = $('#custom-tab-slide .item.active').attr('id');

                    elementChange.find('a[href*="' + eleId + '"]').parent('.tab').addClass('active');
                    elementChange.find('.tab_slider').css({"width": elementChange.find('a[href*="' + eleId + '"]').width() + 7 + "px", "left": elementChange.find('.active a').offset().left + 15 + "px"});

                    if (elementChange.find('.tab').eq(0).offset().top == elementChange.find('.tab').last().offset().top) {
                        elementChange.find('.tab_slider').css({"top": (elementChange.find('.tab').height() - elementChange.find('.tab_slider').height() / 2) + "px"});
                    } else {
                        if (elementChange.find('.tab').eq(0).offset().top != elementChange.find('.active').offset().top) {
                            elementChange.find('.tab_slider').css({"top": (elementChange.height() - elementChange.find('.tab_slider').height()) + "px"});
                        } else {
                            elementChange.find('.tab_slider').css({"top": (elementChange.find('.tab').height() - elementChange.find('.tab_slider').height()) + "px"});
                        };
                    };
                }, 100);
            });
        };



        if ($('#custom-tab-slide ').width() > 1170) {
            if ($('#custom-tab-slide ').width() < 1400) ipadSizeEvent();
            else fullSizeEvent();
        } else mobileSizeEvent();


        $(window).on('resize', function () {
            if ($('#custom-tab-slide ').width() > 1170) {
                if ($('#custom-tab-slide ').width() < 1400) ipadSizeEvent();
                else fullSizeEvent();
            } else mobileSizeEvent();
        });


    };

    /***CHAT close**/
    if ($('#iframe').css('display') == 'none') {
        $(".sharing.chat").removeClass("gray");
    }
    $(".hasEvents").click(function (e) {
        e.stopPropagation();

    });



    $('a[href$=".pdf"]').attr('target', '_blank');
    dateitypen();
    /*** panel-body download***/
    function dateitypen() {
        $('.results-list .results-entry h3 a[href$=".pdf"]').each(function () {
            $href = $(this).attr('href');
            console.log($href);
            $(this).parent().before("<span class='typ_logo_pdf'><a href=" + $href + " target='_blank'>pdf</a></span>");
        });
    }


    //Selecter INIT
    if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
        $(".selecter").selecter({
            cover: true
        });

        $("select").each(function () {
            if ($(this).find("option:selected").val() === "") {
                $(this).next(".selecter").find(".selecter-selected").addClass("placeholder");
            }
        });

        $('<div class="arrow"></div>').prependTo(".selecter-options");
        var i = $("div.selecter").length;
        $("div.selecter").each(function () {
            $(this).css("z-index", i);
            i--;
        });
        $('.selecter').on('change', function () {
            var selecter = $(this).next(".selecter");
            if (selecter.find(".selecter-options span").eq(0).html() === selecter.find(".selecter-selected").html()) {
                selecter.find(".selecter-selected").addClass("placeholder");
            } else {
                selecter.find(".selecter-selected").removeClass("placeholder");
            }
        });
    }

    $('.selecter').on('change', function () {
        var selecter = $(this).next(".selecter");
        if (selecter.find(".selecter-options span").eq(0).html() === selecter.find(".selecter-selected").html()) {
            selecter.find(".selecter-selected").addClass("placeholder");
        } else {
            selecter.find(".selecter-selected").removeClass("placeholder");
        }
    });


    $(".tx_locationsearch_open").click(function (e) {
        if ($(document).scrollTop() > 0) {
            $('html, body').animate({
                scrollTop: 0
            }, 600);
        }
        e.preventDefault();
        if ($(window).width() > 991) {
            $(".tx_locationsearch_locations").slideToggle('fast', function ()
            {
                $(".tx_locationsearch_locations .tab_navigation ul li.active").click();
            });
        } else {
            $(".tx_locationsearch_locations").toggleClass("mobile_slide");
            set_tab_bar_width();
        }
    });

    $(".expert_mail_newsletter").click(function (e) {
        e.preventDefault();
        $(".expert_mail_newsletter_content").fadeIn('fast');
    });
    $(".expert_mail_newsletter_registration_close").click(function (e) {
        $(".expert_mail_newsletter_content").fadeOut('fast');
    });
    $(".fame-shbg").click(function (e) {
        $(".expert_mail_newsletter_content").slideToggle('fast');
    });

    $(".powermail_fieldwrap_optionalbestellen").click(function (e) {
        $(".expert_mail_newsletter_content .powermail_fieldset_23").toggleClass('active');
        $(".powermail-text-only").toggleClass('active');
    });
    $(".powermail_fieldwrap_uid1816").click(function (e) {
        $(".expert_mail_newsletter_content .powermail_fieldset_149").toggleClass('active');
        $(".powermail-text-only").toggleClass('active');
    });


    $(".more_newsletter").click(function (e) {
        $(".text_bild_module_content").toggleClass('active');
        $(".more_newsletter").toggleClass('active');
    });

    $(".chat_more").click(function (e) {
        $("#iframe").css("display", "block");
    });
        $(".sharing.chat").bind('click touchstart',function () {
            if ( $('body > iframe#iframe').length < 1 ){
                var chatAttrArr = [$(this).attr('lang-attr'), $(this).attr('script-src-attr')];
                $.getScript( chatAttrArr[1] ).done(function() {
                    //     https://awcs.stahl.de:443/WebChatApp/AvayaWebChat.js
                    enableAvayaWebChat(chatAttrArr[0]);
                    $('iframe#iframe').css('z-index', '991').show();
                });
            };

        });

    $(".in-page-share ul").on('click touchstart', 'li', function () {


        if ($(this).hasClass('chat')) {

            $("#iframe").css("display", "block");
        } else {
            $("#iframe").css("display", "none");
        }
        $('.in-page-share li').removeClass('gray');
        $('.in-page-share li').removeClass('active');
        $(this).addClass('active');
        $(".layout_sticky_more").removeClass("active");
        $(".layout_sticky_content").removeClass("active");
        $(this).find('.layout_sticky_more').addClass('active');
    });

    $(".in-page-share ul").on('click touchstart', '.active .layout_sticky_more', function (e) {
        e.stopPropagation();
        console.log($(this));
        $(this).closest("li").addClass("gray");
        $(".layout_sticky_more").removeClass("active");
        $(this).next().addClass("active");
    });


    $(".in-page-share ul").on('click touchstart', 'li.active', function (e) {
        e.stopPropagation();
        $(this).addClass("gray");
        $(".layout_sticky_more").removeClass("active");
        $(this).find(".layout_sticky_content").addClass("active");
    });


    $(".in-page-share ul").on('click touchstart', '.layout_sticky_content_close', function (e) {
        e.stopPropagation();
        $(this).closest("li").removeClass("gray");
        $(".layout_sticky_more").removeClass("active");
        $(".layout_sticky_content").removeClass("active");
        $(".in-page-share li").removeClass("active");
    });
    $(".in-page-share ul").on('click touchstart', '.layout_sticky_content', function (e) {
        e.stopPropagation();
    });

    $(document).on('mouseenter', '.in-page-share ul li', function () {
        $(this).addClass('active');
        $(this).find('.layout_sticky_more').addClass('active');

        // if($(this).hasClass('chat')){
        // $("#iframe").addClass('active');
        // }


    });

    $(document).on('mouseleave', '.in-page-share ul li', function () {
        $(".in-page-share ul li").removeClass("active");
        $(this).find('.layout_sticky_more').removeClass('active');
        if ($(this).hasClass('chat')) {
            $("#iframe").removeClass('active');
        }
    });


    /* RMA FORM START */
    $("#rma_form_select .rma_form").click(function () {
        $(".powermail_form.layout3").eq(1).parents(".frame").slideUp("fast");
        $(".powermail_form.layout3").eq(0).parents(".frame").slideDown("fast", function () {
            $("#rma_form_select .rma_form").addClass("active");
            $("#rma_form_select .reclamation_form").removeClass("active");
        });

    });
    $("#rma_form_select .reclamation_form").click(function () {
        $(".powermail_form.layout3").eq(0).parents(".frame").slideUp("fast");
        $(".powermail_form.layout3").eq(1).parents(".frame").slideDown("fast", function () {
            $("#rma_form_select .reclamation_form").addClass("active");
            $("#rma_form_select .rma_form").removeClass("active");
        });

    });
    $(".powermail_form.layout3 .powermail_fieldwrap_fehler select").selecter("disable");
    $(".powermail_form.layout3 .powermail_fieldwrap_fehler02 select").selecter("disable");

    if ($(".powermail_form.layout3 .powermail_fieldwrap_fehler select").val() !== "") {
        $(".powermail_form.layout3 .powermail_fieldwrap_fehler select").selecter("enable");
    }
    if ($(".powermail_form.layout3 .powermail_fieldwrap_fehlergruppe02 select").val() !== "") {
        $(".powermail_form.layout3 .powermail_fieldwrap_fehler02 select").selecter("enable");
    }

    $(".powermail_form.layout3 .powermail_fieldwrap_fehlergruppe").change(function () {
        fehlergruppe_select_change(".powermail_fieldwrap_fehlergruppe", ".powermail_fieldwrap_fehler");
    });
    $(".powermail_form.layout3 .powermail_fieldwrap_fehlergruppe02").change(function () {
        fehlergruppe_select_change(".powermail_fieldwrap_fehlergruppe02", ".powermail_fieldwrap_fehler02");
    });

    if ($(".powermail_form.layout3").eq(1).find(".powermail_field_error").length > 0) {
        $(".powermail_form.layout3").eq(0).parents(".frame").slideUp("fast");
        $(".powermail_form.layout3").eq(1).parents(".frame").slideDown("fast");
    }
    if ($(".powermail_fieldwrap_fehlergruppe select option:selected").val() !== "") {
        fehlergruppe_select_change(".powermail_fieldwrap_fehlergruppe", ".powermail_fieldwrap_fehler");
    }
    if ($(".powermail_fieldwrap_fehlergruppe02 select option:selected").val() !== "") {
        fehlergruppe_select_change(".powermail_fieldwrap_fehlergruppe02", ".powermail_fieldwrap_fehler02");
    }

    function fehlergruppe_select_change(select, target_select) {
        var selected = $(select).find("select option:selected").text();
        if (selected !== "") {
            selected = selected.replace("/", "");

            try {
                var options = "";
                $(".powermail_form.layout3 .powermail_fieldwrap_type_select label").each(function () {
                    var text = $.trim($(this).text()).replace("Fehler ", "");
                    if (text === selected) {
                        options = $(this).parents(".powermail_fieldwrap_type_select").find("select").html();
                    }
                });
                if (options !== "") {
                    $(".powermail_form.layout3 " + target_select + " select").html(options);
                    $(".powermail_form.layout3 " + target_select + " select").selecter("destroy");
                    $(".powermail_form.layout3 " + target_select + " div.selecter").remove();
                    $(".powermail_form.layout3 " + target_select + " select").selecter({
                        cover: true
                    });
                    $(".powermail_form.layout3 " + target_select + " select").selecter("enable");
                }
            } catch (e) {
                $(".powermail_form.layout3 " + target_select + " select").html('<option value="">Fehler</option>');
                $(".powermail_form.layout3 " + target_select + " select").selecter("destroy");
                $(".powermail_form.layout3 " + target_select + " select").selecter({
                    cover: true
                });
                $(".powermail_form.layout3 " + target_select + " select").selecter("disable");
            }
        }
    }
    /* RMA FORM END */
    /*  $( ".navitop_language .active" ).click(function(e) {
     $( ".langnavi li" ).toggleClass('active');
     });
     */

    changeHeadlineNews();

    function changeHeadlineNews() {
        var replace_news_header = document.getElementsByClassName("replace_news_header");
        var replace_news_subline = document.getElementsByClassName("replace_news_subline");


        for (var i = 0; i < replace_news_header.length; i++) {

            var parent = replace_news_header[i].parentElement;
            //console.log(parent);
            while (parent !== null && !parent.classList.contains("frame")) {
                parent = parent.parentElement;
                //console.log(parent);
            }
            if (parent !== null) {
                var headline = "";
                var subline = "";
                for (var j = 0; j < parent.children.length; j++) {
                    if (parent.children[j].tagName == "H5") {
                        headline = parent.children[j].innerHTML;
                    }
                    if (parent.children[j].tagName == "H6") {
                        subline = parent.children[j].innerHTML;
                    }
                }

                replace_news_header[i].innerHTML = headline;
                if (typeof replace_news_subline[i] !== "undefined") {
                    replace_news_subline[i].innerHTML = subline;
                }
            }
        }
    }

    /* calculate width for mobile tab bar */
    function set_tab_bar_width() {
        var width = 0;
        if ($(window).width() < 992) {
            $(".tx_locationsearch_locations .tab_navigation .content-container ul.no_default li").each(function (index, element) {

                if (($(".tx_locationsearch_locations .tab_navigation .content-container ul li").length - 1) > index) {
                    width += $(this).outerWidth(true);
                    width += 2;
                }
                if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                    $("body").addClass("apple_device");
                }
                $(".tx_locationsearch_locations .tab_navigation .content-container ul, .tab_navigation-line-slide").css("width", width);
            });


        }
    }
    /* Chat Time */
    chat_time();
});

$('iframe.aktie').load(function () {
    $("iframe").css("height", "1000px");
});
$(document).load(function () {
    $("iframe").css("height", "1400px");
});

/* function Chat_Time */
function chat_time() {
    var heute = new Date();
    var yyyy = heute.getFullYear(); //yields year
    var MM = heute.getMonth(); //yields month
    var dd = heute.getDate(); //yields day
  console.log(dd+'-'+MM+'-'+yyyy);
    Hours = heute.getHours() //yields hour
    show_chat = (8 < Hours && Hours < 12) || (12 < Hours && Hours < 16);
//    console.log(Hours);
    var xxx = dd + "." + (MM + 1) + "." + yyyy; // NOW 
     var feiertag = ['23.12.2017', '24.12.2017', '25.12.2017', '26.12.2017', '27.12.2017', '28.12.2017', '29.12.2017', '30.12.2017', '31.12.2017', '01.01.2018', '06.01.2018', '30.03.2018', '02.04.2018', '01.05.2018', '10.05.2018', '21.05.2018', '31.05.2018', '03.10.2018', '01.11.2018', '25.12.2018', '26.12.2018'];
   if ($.inArray(xxx, feiertag) > -1) {
        show_chat = false;
//        console.log('Feiertag in BW');
    } 
	else {
//        console.log('Werktag in BW');
        if (dd == 0 || dd == 6) {
            show_chat = false;
        }	
    }
	
	
    if (show_chat) {
		if(dd=5){
			if(Hours < 15){
				$('.chat_en').show();
				$('.chat_de').show();
			}
			else{
				$('.chat_en').show();
				$('.chat_de').hide();
			}
		}
		else{
			$('.chat_en').show();
			$('.chat_de').show();
		}
	}
	else {
			$('.chat_en').hide();
			$('.chat_de').hide();
		}
}