/*!
 *
 * AvayaWebChat.js
 * Copyright 2016 Avaya Inc. All Rights Reserved.
 * Usage of this source is bound to the terms described
 * in https://support.avaya.com/licenseinfo
 * Avaya - Confidential & Proprietary.
 */
;

function enableAvayaWebChat(w, r, n, o) {
    var x = document.createElement("a");
    x.href = document.getElementById("avayaWebChatScript").src;
    var v = x.hostname;
    var u = x.port;
    var p = x.protocol;
    if (n != "") {
        currentFontColor = n
    } else {
        currentFontColor = "#000005"
    }
    if (o != "") {
        currentBackgroundColor = o
    } else {
        currentBackgroundColor = "#60b2ea"
    }
    if (r != "") {
        currentText = r
    } else {
        currentText = "Start chat"
    }
    var s = document.createElement("iframe");
    s.src = "/fileadmin/default/js/WebChatDialog/chat.html?topicName=" + w + "&customFontColor=" + currentFontColor + "&customBackgroundColor=" + currentBackgroundColor + "&customText=" + currentText;
    s.id = "iframe";
    s.width = 350;
    s.height = 'auto';
    s.frameBorder = 0;
    s.style.position = "fixed";
    s.style.right = "67px";
    s.style.top = "50%";
	s.style.marginTop = "35px";
	s.style.display = "none";
    document.body.appendChild(s);
    var m = window.addEventListener ? "addEventListener" : "attachEvent";
    var q = window[m];
    var t = m == "attachEvent" ? "onmessage" : "message";
    q(t, function(a) {
        if (a.data == "close") {
            document.getElementById("iframe").style.height = "auto"
        } else {
            if (a.data == "open") {
                document.getElementById("iframe").style.height = "450px"
            }
        }
    }, false)
};