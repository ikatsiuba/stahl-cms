/*!
* 
* topic_model.js
* Copyright 2016 Avaya Inc. All Rights Reserved. 
* Usage of this source is bound to the terms described 
* in https://support.avaya.com/licenseinfo
* Avaya - Confidential & Proprietary. 
*/
(function(){var b=function(){function a(e,f){this.topicName=e;this.topicID=f}a.prototype={printMessage:function(){console.log(this.topicName+" "+this.topicID)}};return(a)};b.$inject=[];angular.module("WebChatClient").factory("Topic",b)}());