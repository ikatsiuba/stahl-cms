/*!
* 
* chat_messages.js
* Copyright 2016 Avaya Inc. All Rights Reserved. 
* Usage of this source is bound to the terms described 
* in https://support.avaya.com/licenseinfo
* Avaya - Confidential & Proprietary. 
*/
(function(){var b=function(){function d(h,c,i,j){this.type=h;this.message=c;this.chatSessionID=i;this.publishDate=j}d.prototype={printMessage:function(){console.log(this.message+" "+this.type+" "+this.publishDate)}};function a(c){}return(d)};b.$inject=[];angular.module("WebChatClient").factory("ChatMessage",b)}());