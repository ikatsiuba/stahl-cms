/*!
* 
* language_model.js
* Copyright 2016 Avaya Inc. All Rights Reserved. 
* Usage of this source is bound to the terms described 
* in https://support.avaya.com/licenseinfo
* Avaya - Confidential & Proprietary. 
*/
(function(){var b=function(){function a(d){this.languageJSON=d}return(a)};b.$inject=[];angular.module("WebChatClient").factory("Language",b)}());