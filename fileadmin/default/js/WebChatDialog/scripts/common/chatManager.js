/*!
 *
 * chatManager.js
 * Copyright 2016 Avaya Inc. All Rights Reserved.
 * Usage of this source is bound to the terms described
 * in https://support.avaya.com/licenseinfo
 * Avaya - Confidential & Proprietary.
 */
;
app.factory("ChatManagerFactory", ["$rootScope", function(M) {
    var A = window.location.hostname;
    var O = window.location.port;
    var G = window.location.protocol;
    var J = A;
    var R = O;
    var H = G;
    var C = "/WebChatDialog/";
    var E;
    var D;
    var z;
    var I;
    var N;
    var V;
    var Q = function(a) {
        N(M.languageJSON.succesMessage)
    };
    var P = function(a) {
        B(M.languageJSON.lostConnectionErrorMessage);
        N("LostConnection")
    };
    var L = function(a) {
        B(M.languageJSON.creatingConnectionErrorMessage);
        N("SocketError")
    };
    var y = function(b) {
        var a = jQuery.parseJSON(b.data);
        if (a.hasOwnProperty("MESSAGE")) {
            M.$broadcast("getMessages", a);
            V(a)
        } else {
            if (a.hasOwnProperty("STATE")) {
                M.$broadcast("setState", a);
                V(a)
            }
        }
    };
    return {
        init: function(d, c, a, b) {
            J = d;
            R = c;
            H = a;
            V = b
        },
        CheckTopic: function(c, b) {
            var a = H;
            var e = a.concat("//", J, ":", R, C, "configchat");
            var d = {
                topicName: c
            };
            $.ajax({
                url: e,
                type: "GET",
                beforeSend: function(f) {
                    f.setRequestHeader("X-User-Agent", "AvayaIPOCCWebChatClient")
                },
                data: d,
                success: function(g, f, i) {
                    var j = $.parseJSON(i.responseText);
                    var h = j.RESPONSE_CODE;
                    if (h == 0) {
                        console.log("topic is available");
                    }
                },
                error: function(j, f, g) {
                    var k;
                    var i;
                    var l = "";
                    try {
                        k = $.parseJSON(j.responseText);
                        i = k.RESPONSE_CODE;
                        if (i == 401) {
                            console.log("Unauthorized");
                        } else {
                            if (i == 1005) {
                                console.log("invalid topic name");
                            } else {
                                if (i == 441) {
                                    console.log("topic not available");
                                    l = "topicNotAvailable"
                                } else {
                                    if (i == 1006) {
                                        console.log("system busy");
                                        l = "systemBusy"
                                    } else {
                                        console.log("other error");
                                    }
                                }
                            }
                        }
                    } catch (h) {
                        //console.log(h);
                    }
                    z = b;
                    z(l)
                }
            })
        },
        emoticonEnabled: function(d) {
            var b = H;
            var c = b.concat("//", J, ":", R, C, "chatemoticonservlet");
            var a = {};
            $.ajaxSetup({
                cache: false
            });
            $.ajax({
                url: c,
                type: "GET",
                beforeSend: function(e) {
                    e.setRequestHeader("X-User-Agent", "AvayaIPOCCWebChatClient")
                },
                data: a,
                success: function(f, e, g) {
                    var h = $.parseJSON(g.responseText);
                    var i = h.EMOTICON_ENABLED;
                    I = d;
                    I(i)
                },
                error: function(i, e, f) {
                    var j;
                    var h;
                    var k = "";
                    try {
                        j = $.parseJSON(i.responseText);
                        h = j.RESPONSE_CODE;
                        if (h == 401) {
                            console.log("Unauthorized")
                        } else {
                            console.log("other error")
                        }
                    } catch (g) {
                        console.log(g)
                    }
                    I = d;
                    I(k)
                }
            })
        },
        StartChat: function(f, d, b) {
            var a = H;
            var e = a.concat("//", J, ":", R, C, "startchat");
            var c = {
                custUserName: f,
                topicName: d
            };
            N = b;
            D = "";
            $.ajax({
                url: e,
                type: "POST",
                beforeSend: function(g) {
                    g.setRequestHeader("X-User-Agent", "AvayaIPOCCWebChatClient")
                },
                data: c,
                success: function(h, g, i) {
                    D = h.JSESSIONID;
                    T(d)
                },
                error: function(k, g, h) {
                    var l;
                    var j;
                    try {
                        l = $.parseJSON(k.responseText);
                        j = l.RESPONSE_CODE;
                        if (j == 401) {
                            console.log("Unauthorized");
                            B(M.languageJSON.unauthorizedAccessErrorMessage)
                        } else {
                            if (j == 404) {
                                console.log("destination not found");
                                B(M.languageJSON.destinationNotFoundErrorMessage)
                            } else {
                                console.log("other error");
                                B(M.languageJSON.otherServerErrorMessage)
                            }
                        }
                    } catch (i) {
                        console.log(i);
                        B(M.languageJSON.otherServerErrorMessage)
                    }
                }
            })
        },
        EndChat: function(e, c) {
            var a = H;
            var d = a.concat("//", J, ":", R, C, "endchat");
            var b = {
                custUserName: e,
                topicName: c,
                jSessionId: D
            };
            D = "";
            $.ajax({
                url: d,
                type: "POST",
                beforeSend: function(f) {
                    f.setRequestHeader("X-User-Agent", "AvayaIPOCCWebChatClient")
                },
                data: b,
                success: function(g, f, h) {
                    D = g.JSESSIONID;
                    F();
                    M.$broadcast("closeChatSucces", D)
                },
                error: function(j, f, g) {
                    var k;
                    var i;
                    try {
                        k = $.parseJSON(j.responseText);
                        i = k.RESPONSE_CODE;
                        if (i == 401) {
                            console.log("Unauthorized");
                            B(M.languageJSON.unauthorizedAccessErrorMessage)
                        } else {
                            if (i == 404) {
                                console.log("destination not found");
                                B(M.languageJSON.destinationNotFoundErrorMessage)
                            } else {
                                if (i == 1000) {
                                    console.log("cannot exit chat");
                                    B(M.languageJSON.exitChatErrorMessage)
                                } else {
                                    console.log("other error");
                                    B(M.languageJSON.otherServerErrorMessage)
                                }
                            }
                        }
                    } catch (h) {
                        console.log(h);
                        B(M.languageJSON.otherServerErrorMessage)
                    }
                }
            })
        },
        sendMessage: function(d, c) {
            var b = c;
            b = S(b);
            var a = '{"MESSAGE":"' + b + '","JSESSIONID":"' + D + '"}';
            E.send(a);
            M.$broadcast("sendMessage", c, D)
        },
        sendState: function(a, b) {
            var c = '{"STATE":"' + b + '","JSESSIONID":"' + D + '"}';
            if (E != undefined) {
                E.send(c)
            }
        }
    };

    function S(d) {
        var a = d.length,
            b = [];
        while (a--) {
            var c = d[a].charCodeAt();
            if (c < 65 || c > 127 || (c > 90 && c < 97)) {
                b[a] = "&#" + c + ";"
            } else {
                b[a] = d[a]
            }
        }
        return b.join("")
    }

    function U(a) {
        return window.btoa(unescape(encodeURIComponent(a)))
    }

    function K(a) {
        return decodeURIComponent(escape(window.atob(a)))
    }

    function T(b) {
        var c = "wss:";
        if (H === "http:") {
            c = "ws:"
        }
        var a = c + "//" + J + ":" + R + "/WebChatDialog/wsconnect/" + D + "/" + b;
        E = new WebSocket(a);
        E.onopen = Q;
        E.onclose = P;
        E.onmessage = y;
        E.onerror = L
    }

    function B(a) {
        M.$broadcast("error", a)
    }

    function F() {
        E.close()
    }
}]);