/*!
* 
* factories.js
* Copyright 2016 Avaya Inc. All Rights Reserved. 
* Usage of this source is bound to the terms described 
* in https://support.avaya.com/licenseinfo
* Avaya - Confidential & Proprietary. 
*/
;app.factory("UserDetailsFactory",[function(){var b="";return{setUserName:function(a){b=a},getUserName:function(){return b}}}]);app.factory("ArhivedMessegesFactory",[function(){var b=[];return{addNewMessage:function(a){b.push(a)},getMessages:function(){return b}}}]);app.factory("TopicFactory",[function(){var b="";return{setTopicName:function(a){b=a},getTopicName:function(){return b}}}]);