/*!
* 
* commonFunctions.js
* Copyright 2016 Avaya Inc. All Rights Reserved. 
* Usage of this source is bound to the terms described 
* in https://support.avaya.com/licenseinfo
* Avaya - Confidential & Proprietary. 
*/
;var regexEmoticon=/:-?[()dpDPoOsS|]|:\'?-?[(]|\|;-?[)]|;-?[()]|OO|oo|>:-?[)]|<3|O:-?[)]|o:-?[)]|\^\^|[(]n[)]|[(]y[)]|&gt;:-?[)]|&lt;3/g;var emoticons={":D":"biggrin",":-D":"biggrin",":S":"confused",":-S":"confused","|;-)":"cool",";(":"cry",";-(":"cry",":'-(":"cry",":'(":"cry",OO:"eek",">:-)":"evil",">:)":"evil","&gt;:-)":"evil","&gt;:)":"evil","&lt;3":"like","<3":"like","^^":"lol","O:-)":"mrgreen","O:)":"mrgreen",":|":"neutral",":-|":"neutral",":P":"razz",":-P":"razz",":(":"sad",":-(":"sad",":)":"smile",":-)":"smile",":O":"surprised",":-O":"surprised",";)":"wink",";-)":"wink","(n)":"thumbdown","(y)":"thumbup"},url="images/emoticons/";function replaceTextWithEmoticon(c){var d=c.replace(regexEmoticon,function(a){return typeof emoticons[a]!="undefined"?'<img src="'+url+emoticons[a]+'.png"/>':a});return d}function allowDrop(b){b.preventDefault()}function drop(b){b.preventDefault();console.log("drop prevented")};