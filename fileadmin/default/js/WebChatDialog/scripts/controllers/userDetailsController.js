/*!
* userDetailsController.js
* Copyright 2016 Avaya Inc. All Rights Reserved. 
* Usage of this source is bound to the terms described 
* in https://support.avaya.com/licenseinfo
* Avaya - Confidential & Proprietary. 
*/
;app.controller("UserDetailsController",["$scope","$rootScope","UserDetailsFactory",function(j,g,f){j.IPOCC_status;j.username=f.getUserName();var i=new Date();j.displayDate=h(i);g.$on("loadedJSON",function(a,b){j.IPOCC_status=g.languageJSON.ipoccStatusAvailableText;j.$apply()});g.$on("error",function(a,b){j.IPOCC_status=g.languageJSON.ipoccStatusUnavailableText;j.$apply()});g.$on("agentGone",function(a,b){j.IPOCC_status=g.languageJSON.ipoccStatusUnavailableText;j.$apply()});function h(b){var c=b.getHours().toString();c=c.length>1?c:"0"+c;var a=b.getMinutes().toString();a=a.length>1?a:"0"+a;return c+":"+a}}]);