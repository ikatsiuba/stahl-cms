/*!
* 
* closeChatController.js
* Copyright 2016 Avaya Inc. All Rights Reserved. 
* Usage of this source is bound to the terms described 
* in https://support.avaya.com/licenseinfo
* Avaya - Confidential & Proprietary. 
*/
;app.controller("CloseChatController",["$scope","$rootScope","ChatMessage","UserDetailsFactory","TopicFactory","ChatManagerFactory",function(k,h,j,g,i,l){k.closeChat=function(){l.sendState(g.getUserName(),"gone");l.EndChat(g.getUserName(),i.getTopicName());window.close()};h.$on("closeChatSucces",function(a,b){window.close()});h.$on("error",function(a,b){console.log("Error message is comming from server")})}]);