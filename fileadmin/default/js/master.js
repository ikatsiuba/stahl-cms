/**
 * jQuery Unveil
 * A very lightweight jQuery plugin to lazy load images
 * http://luis-almeida.github.com/unveil
 *
 * Licensed under the MIT license.
 * Copyright 2013 Luís Almeida
 * https://github.com/luis-almeida
 */

/*

;(function($) {

  $.fn.unveil = function(threshold, callback) {

    var $w = $(window),
        th = threshold || 0,
        retina = window.devicePixelRatio > 1,
        attrib = retina? "data-src-retina" : "data-src",
        images = this,
        imagesLazy = document.querySelectorAll('.lazy'),
        loaded;

    this.one("unveil", function() {

        // console.warn(this);
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        // if (!imagesLazy) this.setAttribute("src", source);
        if (typeof callback === "function") callback.call(this);
      }
    });

    function unveil() {
      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.is(":hidden")) return;

        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();

        return eb >= wt - th && et <= wb + th;
      });

      loaded = inview.trigger("unveil");
      images = images.not(loaded);
    }

    $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);

    unveil();

    return this;

  };

})(window.jQuery || window.Zepto);

*/


/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/



;
(function ($, window, document, undefined) {
	$.fn.doubleTapToGo = function (params) {
		if (!('ontouchstart' in window) &&
			!navigator.msMaxTouchPoints &&
			!navigator.userAgent.toLowerCase().match(/windows phone os 7/i)) return false;

		this.each(function () {
			var curItem = false;

			$(this).on('click', function (e) {
				var item = $(this);
				if (item[0] != curItem[0]) {
					e.preventDefault();
					curItem = item;
				}
			});

			$(document).on('click touchstart MSPointerDown', function (e) {
				var resetItem = true,
					parents = $(e.target).parents();

				for (var i = 0; i < parents.length; i++)
					if (parents[i] == curItem[0])
						resetItem = false;

				if (resetItem)
					curItem = false;
			});
		});
		return this;
	};
})(jQuery, window, document);




jQuery.fn.extend({
	propAttr: $.fn.prop || $.fn.attr
});

jQuery.curCSS = jQuery.css;

/*! Copyright 2012, Ben Lin (http://dreamerslab.com/)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 1.0.19
 *
 * Requires: jQuery >= 1.2.3
 */
(function (a) {
	if (typeof define === "function" && define.amd) {
		define(["jquery"], a);
	} else {
		a(jQuery);
	}
}(function (a) {
	a.fn.addBack = a.fn.addBack || a.fn.andSelf;
	a.fn.extend({
		actual: function (b, l) {
			if (!this[b]) {
				throw '$.actual => The jQuery method "' + b + '" you called does not exist';
			}
			var f = {
				absolute: false,
				clone: false,
				includeMargin: false,
				display: "block"
			};
			var i = a.extend(f, l);
			var e = this.eq(0);
			var h, j;
			if (i.clone === true) {
				h = function () {
					var m = "position: absolute !important; top: -1000 !important; ";
					e = e.clone().attr("style", m).appendTo("body");
				};
				j = function () {
					e.remove();
				};
			} else {
				var g = [];
				var d = "";
				var c;
				h = function () {
					c = e.parents().addBack().filter(":hidden");
					d += "visibility: hidden !important; display: " + i.display + " !important; ";
					if (i.absolute === true) {
						d += "position: absolute !important; ";
					}
					c.each(function () {
						var m = a(this);
						var n = m.attr("style");
						g.push(n);
						m.attr("style", n ? n + ";" + d : d);
					});
				};
				j = function () {
					c.each(function (m) {
						var o = a(this);
						var n = g[m];
						if (n === undefined) {
							o.removeAttr("style");
						} else {
							o.attr("style", n);
						}
					});
				};
			}
			h();
			var k = /(outer)/.test(b) ? e[b](i.includeMargin) : e[b]();
			j();
			return k;
		}
	});
}));

$(function() {
    // This fix for loading images on local version
            /*
    if (false) {
        var srcNew = 'http://stahl-cms.dev.havasww-digital.de/' ;
        var img = $('.body-bg img');
        console.warn('lazy')
        for (var i = 0; i < img.length; i++) {
            var dataSrcOne = img.eq(i).attr('data-src');
            var srcOne = img.eq(i).attr('src');
            if (img.eq(i).parent().css("background-image") == 'none') {
                let dataSrcOneCheck = (dataSrcOne == undefined)? srcOne: dataSrcOne;
                if (img.hasClass('lazy')) img.eq(i).attr('data-src', srcNew + dataSrcOneCheck);
					else img.eq(i).attr('src', srcNew + dataSrcOneCheck);
            };
            if (img.eq(i).parent().css("background-image") != 'none') {
                console.warn('lazy', 2)
                img.eq(i).parent().css("background-image", "url('" + srcNew + "/fileadmin" + img.eq(i).parent().css('background-image').split('/fileadmin')[1].split('g")')[0] + 'g');
                // For all elements
            };
        };
	};
            */
});
$(function() {
       $(".lazy").Lazy({
            beforeLoad: function(element) {
                // called before an elements gets handled
                lazy_teaserboxen_vollbild_func();
                background_height();
                home_slider();
                img_src_attr();
                teaserzeile_te_lazy_load();
                console.log(11)
            },
            afterLoad: function(element) {
                background_height();
                // img_src_attr();
				sameHeight_bilder_accordeon();
                // called after an element was successfully handled
            },
            onError: function(element) {
                // called whenever an element could not be handled
            },
            onFinishedAll: function() {
                // called once all elements was handled
            }
	   });
});
function teaserzeile_te_lazy_load() {
    if ($(".teaserzeile_te_07").length > 0 && $('header#topnavi').offset().top + $(window).height() >= $(".teaserzeile_te_07").offset().top ) {
        $(".teaserzeile_te_07 .row.sameheight .setheight:not(.blue)").each(function (index, element) {
            var img_data_src = $(this).children("img").attr("data-src");
            var checkSrc = (img_data_src != undefined)? img_data_src : $(this).children("img").attr("src") ;
            $(this).css("background-image", "url(" + checkSrc + ")");
        });
    }
};
function lazy_teaserboxen_vollbild_func() {
    var lazy_teaserboxen_vollbild_carousel = $('.teaserboxen_vollbild_drei_raster_carousel.module .owl-stage');
    if (lazy_teaserboxen_vollbild_carousel.length > 0 && $('header#topnavi').offset().top + $(window).height() >= lazy_teaserboxen_vollbild_carousel.offset().top) {
        for (var i = 0; i < lazy_teaserboxen_vollbild_carousel.find('img').length; i++) {
            var dataSrc = lazy_teaserboxen_vollbild_carousel.find('img').eq(i).attr('data-src');
            lazy_teaserboxen_vollbild_carousel.find('img').eq(i).attr('src', dataSrc);
        };
    };
};
function background_height() {
	if ( $('.background_height').length > 0) {
		var height = 0;
		$('.background_height').each(function () {
			$(this).find('.get_height').each(function (index, element) {
				height = $(this).css("height", "auto").actual("height");
				$(this).next('.set_height').height(height);
			});
		});
	}
};
function home_slider() {
	var home_slider_img = $('#home_slider .owl-item .lazy');
	if (home_slider_img.length > 0 && $('#home_slider').offset().top + $(window).height() >= $('header#topnavi').offset().top ){
		for (var i = 0; i < home_slider_img.length; i++) {
			home_slider_img.eq(i).attr('src', home_slider_img.attr('data-src'))
		}
	}
}
function img_src_attr() {
    var img = $('.body-bg img');
    for (var i = 0; i < img.length; i++) {
        var dataSrc = img.eq(i).attr('data-src');
			if (!img.eq(i).hasClass('lazy') && img.eq(i).parent().css("background-image") == 'none' && $('img').attr('data-src') != undefined) {
				img.eq(i).attr('src', dataSrc);
    console.warn('1');
				/* && img.eq(i).attr('src') == "/fileadmin/user_upload/gfx/default-lazy.png"*/
			}
            if (img.eq(i).hasClass('lazy') && img.eq(i).parent().css("background-image") == 'none' && $('img').attr('data-src') != undefined ) {
    console.warn('2');
                img.eq(i).attr('src', dataSrc);
            };
    };
};
function sameHeight_bilder_accordeon() {
    if ($(".product_cat").length > 0 || $(".bilder_accordion").length > 0 || $(".teaserboxen_normal_drei_raster.direct_flyout.module").length > 0 ) {
        var wrapper_arr = [],
			mathElem ,
			wrapper = $(".product_cat .lazy , .bilder_accordion .teaserboxen_normal_drei_raster_image img, .teaserboxen_normal_drei_raster.direct_flyout.module .teaserboxen_normal_drei_raster_image img");

		for (var i = 0; i < wrapper.length; i++) {
			wrapper_arr.push(wrapper[i].height);
			if (wrapper_arr.length == wrapper.length) {
				mathElem = Math.min.apply(null, wrapper_arr);
				if (parseFloat(wrapper[i].height) >= mathElem || parseFloat(wrapper[i].height) < mathElem) wrapper.eq(i).height(mathElem);
			}
		};
    }
};

$(document).ready(function () {

	$('form .powermail_submit.btn.btn-primary').on('click', function(e){
		let error = false;
		$(['input[required]:visible', 'textarea[required]:visible', 'select[required]:visible']).each(function(index, selector) {                
			$(selector).each(function(index, input) {
				error = error == true ? error : $(input).val() == '';
			});
		});

		$('.parent:visible').each(function(index, group){
			if(error == true) {
				error = error;
			}
			else {
				error = $('input:checked', group).length < minCheckedCheckboxes;                
			}
		});
		if(error == false) {
			error =! $('input[type=radio]:required').is(':checked');
		}
		if(error) {
			$('input[required]:visible, textarea[required]:visible, select[required]:visible').removeClass("parsley-error");
			$('input[required]:visible, textarea[required]:visible, select[required]:visible').filter(function() {
				return !this.value;
			}).addClass('parsley-error');
			}
			
			
		if( $(this).parents('form:first').find('.powermail_captcha').val().length < 1 ) {
			$(this).parents('form:first').find('.g-recaptcha').addClass('parsley-error');
			if($(this).parents('form:first').find('.parsley-error')){
					e.preventDefault();
				}
			}
        else{
            $(this).parents('form:first').find('.g-recaptcha').removeClass('parsley-error');	
        }  
	});
/*
	$("img").unveil();
    $("img").unveil(200, function() {
	  $(this).load(function() {
		this.style.opacity = 1;
	  });
	});*/
    function checkSize() {
        $('.teaserboxen_normal_drei_raster_image_content ').each(function(){
            if (jQuery(this).width() + ( parseInt(jQuery(this).css('padding-left')) * 2 ) !=  jQuery(this).parent('a').parent('div[class*="col-"]').width())  {
                jQuery(this).css('width', jQuery(this).parent('a').parent('div[class*="col-"]').width() + 'px')
                jQuery(this).find('h3').css({
                    'width': (jQuery(this).parent('a').parent('div[class*="col-"]').width() - ( parseInt(jQuery(this).css('padding-left')) * 2 ) )+ 'px',
                    'display': 'block'
                });
            };
        });
    };
    if (jQuery('body').is('.pagelevel-4.language-0.backendlayout-pagets__product')) {
        checkSize();

        jQuery(window).on('resize', function(){
            checkSize();
        });
    }
	/* COOKIE */

	function createCookie(name, value, h) {
		var expires;
		if (h) {
			var date = new Date();
			date.setTime(date.getTime() + (h * 1000 * 60 * 60 * 24 * 365));
			expires = "; expires=" + date.toGMTString();
		} else {
			expires = "";
		}
		document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = encodeURIComponent(name) + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) === ' ')
				c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) === 0)
				return decodeURIComponent(c.substring(nameEQ.length, c.length));
		}
		return null;
	}

	function eraseCookie(name) {
		createCookie(name, "", -1);
	}
	if (readCookie("cookie_info") === null) {
		$(".cookie").addClass("cookie_visible");
	} else {
		$(".cookie").removeClass("cookie_visible");
	}

	$(".secondaryContentSection ul li").click(function (e) {
		$(".secondaryContentSection ul li").removeClass('active');
		$(this).addClass('active');
	});


	$("#yes").click(function (e) {
		createCookie("cookie_info", 1, 1);
		$(".cookie").slideUp(function () {
			$(".cookie").removeClass("cookie_visible");
		});
	});

	function msieversion() {

		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");

		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return version number
		{
			return true;
		}

		return false;
	}

	var ie_browser = msieversion();
	if (ie_browser) {
		$("body").addClass("ie_browser");
	}

	var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));

	var sites = ["Investor Relations", "Karriere"];
	var chat_flag = false;

	function switch_chat_off() {
		sites.forEach(function (entry) {
			if (!chat_flag) {
				$("ul.breadcrumb_ul li").each(function (index, element) {
					var li = $(this).text();
					if (li.indexOf(entry) >= 0) {
						$("body").addClass("chat_off");
						chat_flag = true;
						return false;
					}
				});
			} else {
				return false;
			}
		});
		$(".in-page-share").fadeIn();
	}
	switch_chat_off();

	/*** print logo add ***/
	var print_logo = $(".navbar .logo_background a:first-child img").clone();
	$(print_logo).appendTo("body").addClass("print_logo");

	$(window).on("load", function () {
		sameHeight();
		// sameHeight_bilder_accordeon();
	});

	$('.tab').first().addClass('active');

	var hero_carousel = $("#home_slider");

	hero_carousel.owlCarousel({
		// lazyLoad: true,
		margin: 0,
		loop: true,
		autoWidth: false,
		items: 1,
		nav: true,
		navText: false,
		dots: true,
		autoHeight: true,
		autoplay:true,
		autoplayTimeout:8000,
		autoplayHoverPause:false
	});



	var teaserboxen_carousel = $("#teaserboxen_vollbild_drei_raster_carousel").owlCarousel({
		margin: 16,
		loop: true,
		autoWidth: true,
		items: 3,
		nav: true,
		navText: false,
		dots: true,
	});


	$(".teaserblock_mit_versatz_carousel").owlCarousel({
		margin: 0,
		loop: true,
		autoWidth: false,
		items: 1,
		nav: true,
		navText: false,
		dots: true,
	});

	$(window).on("load", function () {
		$(hero_carousel).trigger('refresh.owl.carousel');
		$(product_carousel).trigger('refresh.owl.carousel');
		// $("img").trigger("unveil");

		/*var one_time = true;
		var teaserboxen_carousel_data = "";

		if ($("#teaserboxen_vollbild_drei_raster_carousel .owl-item > .cat-item").length > 0) {
			teaserboxen_carousel_data = teaserboxen_carousel.data('owl.carousel');
			teaserboxen_carousel_data.settings.loop = false;
			teaserboxen_carousel_data.options.loop = false;
			teaserboxen_carousel.trigger('refresh.owl.carousel');
			one_time = false;
			console.log("loop na false");
		}

		$("#teaserboxen_vollbild_drei_raster_carousel .owl-item > .cat-item").each(function (index, element) {
			var owl_item = $(this).parent(".owl-item");
			var owl_item_pos = $(owl_item).index();
			console.log(owl_item_pos);
			$(teaserboxen_carousel).trigger('remove.owl.carousel', owl_item_pos);
		});

		if (!one_time) {
			teaserboxen_carousel_data.settings.loop = true;
			teaserboxen_carousel_data.options.loop = true;
			teaserboxen_carousel.trigger('refresh.owl.carousel');
		}*/

	});

	$(".teaserboxen_volle_breite_tabs .tab-content > .tab-pane").first().addClass("active in");


	function set_carousel_navi() {
		$(".owl-nav").each(function (index, element) {
			var this_prev = $(this).children(".owl-prev");
			var this_next = $(this).children(".owl-next");
			var point_counter = $(this).next(".owl-dots").children(".owl-dot").length;
			var margin_standard = $(this_prev).width();
			var margin_right = ((3.5 * point_counter) + 2);
			var margin_left = (((3.5 * point_counter) + 2) + margin_standard) * (-1);
			$(this_prev).css("margin-left", margin_left);
			$(this_next).css("margin-left", margin_right);

		});
	}
	set_carousel_navi();

	$(document).ajaxComplete(function () {
		set_carousel_navi();
	});

	/*** home search position fix ***/
	/*** DE ***/
	if ($("#c7207 .aimeos.catalog-filter").length > 0) {
		$("#c7207 .aimeos.catalog-filter").insertAfter("#c7207 + .bilder_accordion.module h2");
		$("#c7207").remove();
	}

	/*** EN ***/
	if ($("#c12098 .aimeos.catalog-filter").length > 0) {
		$("#c12098 .aimeos.catalog-filter").insertAfter("#c12098 + .bilder_accordion.module h2");
		$("#c12098").remove();
	}

	//*******************Modulescript********************//

	var beforePrint = function () {
		$("body").addClass("print_styles");
		$(window).resize();
		sameHeight();
		background_height();
		// sameHeight_bilder_accordeon();
		$(".teaserboxen_volle_breite_tabs ul li.active").click();
		set_carousel_navi();
		open_all_details();
		$(hero_carousel).trigger('refresh.owl.carousel');

	};
	var afterPrint = function () {
		refresh_print();
	};

	if (window.matchMedia) {
		var mediaQueryList = window.matchMedia('print');
		mediaQueryList.addListener(function (mql) {
			if (mql.matches) {
				beforePrint();
			} else {
				afterPrint();
			}
		});
	}


	window.onbeforeprint = beforePrint;
	window.onafterprint = afterPrint;

	var refresh_done = 0;

	function refresh_print() {
		if (refresh_done === 0) {
			refresh_done = 1;
			setTimeout(function () {
				refresh_done = 0;
				$("body").removeClass("print_styles");
				$(window).resize();
				sameHeight();
				background_height();
				// sameHeight_bilder_accordeon();
				$(".teaserboxen_volle_breite_tabs ul li.active").click();
				set_carousel_navi();
				open_all_details();
				$(hero_carousel).trigger('refresh.owl.carousel');
			}, 2000);
		}
	}

	function open_all_details() {
		$(".panel-collapse.collapse").css("display", "block");
		$(".panel-heading").addClass("active");
	}

	if ($("body").hasClass("kontaktformular")) {
		scrollto(".subcontent-wrap");
	}


	sameHeight();

	$(window).resize(function () {
		sameHeight();
		background_height();
		sameHeight_bilder_accordeon();
		$(".teaserboxen_volle_breite_tabs ul li.active").click();
		set_carousel_navi();
    });

	function sameHeight() {
		if ($(window).width() > 220) {
			$('.sameheight').each(function () {
				var maxHeight = 0;
				var maxHeightheadline = 0;
				var mobile_off = false;
				var save_parent = 0;
				if (!$(this).is(":visible")) {
					var parent = $(this).parent();
					var elem = $(this);
					var find_none = true;
					var max_parents_number = 20;
					var counter = 0;
					while (find_none) {
						counter++;
						if ($(elem).css("display") === "none") {
							$(elem).addClass("calc_height");
							save_parent = $(elem);
							find_none = false;
						} else {
							elem = $(elem).parent();
						}

						if (counter >= max_parents_number) {
							find_none = false;
						}
					}
				}

				$(this).find('.setheight').each(function () {
					if (($(this).hasClass("location_content_person") || $(this).hasClass("location_content_form")) && $(window).width() < 768) {
						mobile_off = true;
					} else {
						$(this).css("height", "auto");
						if ($(this).height() > maxHeight) {
							maxHeight = $(this).height();
						}
					}
				});

				$(this).find('.setheightheadline').each(function () {
					$(this).css("height", "auto");
					if ($(this).height() > maxHeightheadline) {
						maxHeightheadline = $(this).height();
					}

				});

				if (!mobile_off) {
					$(this).find('.setheight').each(function () {
						$(this).height(maxHeight);
					});
					$(this).find('.setheightheadline').each(function () {
						$(this).height(maxHeightheadline);
					});
				} else {
					$(this).find('.setheight').each(function () {
						$(this).css("height", "auto");
					});

				}

				if (!find_none) {
					$(save_parent).removeClass("calc_height");
				}
			});
		}
	};

	$(window).load(function (e) {
		background_height();
		sameHeight();
		$(".teaserboxen_volle_breite_tabs ul li.active").click();
		$(".news_feed ul, .product_download ul, .product_accessories ul, .tab_navigation ul").each(function (index, element) {
			var this_ul = $(this);
			if (!$(this_ul).hasClass("no_default")) {
				$(this_ul).children("li").first().click();
			}
		});

		/* IMAGE MAP */
		/* INIT */
		var image_map_offset = $(".image_map").offset();
		var image_map_height = $(".image_map").height();
		$(".image_map .image_map_point").each(function () {
			var details = $(this).find(".image_map_point_detail_wrapper");
			var details_width = details.width();
			var details_height = details.height();
			var offset_top = $(this).offset().top;
			var offset_left = $(this).offset().left;

			if ((offset_left + 50 + details_width) > $(window).width()) {
				$(this).addClass("detail_left");
			}
			if ((offset_top + 50 + details_height) > (image_map_offset.top + image_map_height)) {
				$(this).addClass("detail_top");
			}
		});
	});

	/** NAVIGATION **/
	$(window).scroll(function (e) {
		sticky_navi();
		sticky_side_menu();
		// if (lazy_teaserboxen_vollbild_carousel.length != 0) lazy_teaserboxen_vollbild_func();
		// if ( $(".teaserzeile_te_07").length > 0 ) teaserzeile_te_lazy_load();
    });

	var sticky = 0;

	function sticky_placeholder() {
		var height = $(".navbar .main_element").outerHeight(false);
		$(".sticky_placeholder").height(height);
	}
	sticky_placeholder();

	function sticky_navi() {
		var top_distance = 0;
		top_distance = $(window).scrollTop();
		sticky_start = 51;
		if (top_distance > sticky_start && sticky === 0) {
			/*		     $(".navbar .main_element, .ex_explorer_logo").addClass("sticky");
					     $(".sticky_placeholder").show();*/
			$("#move-to-top").fadeIn();
			/* $(".language_url").css("visibility", "hidden");*/
			sticky = 1;
		} else if (top_distance <= sticky_start && sticky && sticky) {
			/* $(".navbar .main_element, .ex_explorer_logo").removeClass("sticky");
			 $(".sticky_placeholder").hide();*/
			$("#move-to-top").hide();
			/* $(".language_url").css("visibility", "visible");*/
			sticky = 0;
		}
	}

	function change_main_category() {
		if ($(window).width() > 991){
			if ($(".navitop_left_side .navbar-main > li:last-child a").hasClass("active")) {
				$(".mainmenu .nav-pills.produkte").hide();
				$(".mainmenu .nav-pills.unternehmen").fadeIn();
			} else {
				$(".mainmenu .nav-pills.produkte").fadeIn();
				$(".mainmenu .nav-pills.unternehmen").hide();
			}
		} else{
			if ($(".navitop_left_side .navbar-main > li:last-child a").hasClass("active")) {
				$(".mainmenu .nav-pills.produkte_mobile").hide();
				$(".mainmenu .nav-pills.unternehmen_mobile").fadeIn();
			} else {
				$(".mainmenu .nav-pills.produkte_mobile").fadeIn();
				$(".mainmenu .nav-pills.unternehmen_mobile").hide();
			}
		}
	
	}
	change_main_category();

	$("nav.navbar-collapse .close_button").click(function (e) {
		if ($(".tx_locationsearch_locations").hasClass("mobile_slide")) {
			$(".tx_locationsearch_locations").removeClass("mobile_slide");
		}
		$(".navbar-collapse.flyout_1.collapse.in").removeClass("in");
		$(".mainmenu .navbar-toggle").addClass("collapsed");
		$(".meta_navi_mobile").fadeOut();
		if ($(".main_element").hasClass("sticky")) {
			$("#move-to-top").fadeIn();
		}
		$("body").removeClass("block_scrolling");
		set_distance_top("off");

	});

	/*		Start New Version Product Nav	*/


    // Main Drop Menu scrolling
    var productDropMenu = jQuery('header .mainmenu .wrapper-dropdown-menu');
    var heightElemArr = [];
    var jspPaneHeight ;

    function paddings_dropDown_menu() {
        productDropMenu.css({'padding-left': jQuery('.logo_background a img.img-responsive').offset().left - 20 + 'px', 'padding-right': jQuery(window).width() - ( jQuery('.langnavi').offset().left + jQuery('.langnavi').width() ) + 'px', });
    };

    function customnScroll() {
        jQuery(function() {
            jQuery('.scroll-pane').jScrollPane();
        });
    };

    jQuery(window).resize(function () {
        paddings_dropDown_menu();
        customnScroll();
    });
    customnScroll();
    paddings_dropDown_menu();

    productDropMenu.find('li')
        .on('mouseenter', function (e) {

            var heightWrapperDropMenu = productDropMenu.find('.second-level').height();
            var checkTargetElem = (jQuery(e.target).is('a'))? jQuery(e.target).parent('li') : jQuery(e.target) ;
            var shadowHeight = (jQuery(window).width() < 1200)? (checkTargetElem.parents('.second-level').height() * 0.25) : (checkTargetElem.parents('.second-level').height() * 0.3);
            var jspPaneTop = (-parseFloat(productDropMenu.find('.jspPane').css('top')));

            var heightElemSecondLevArr = [];
            var heightElemThirdLevArr = [];
            var heightElemFourthLevArr = [];
            var heightElemSecondLev = checkTargetElem.parent('.jspPane').children('li');
// console.error(jspPaneHeight, heightElemArr);
            jQuery('html > body').addClass('productDropMenu_open').find('.in-page-share').addClass('close');

            for (var i = 0; i < heightElemSecondLev.length;  i++ ) heightElemSecondLevArr.push(heightElemSecondLev.eq(i)[0].offsetHeight);

            if (heightElemArr.length <= 1) heightElemArr[0] = arraySum(heightElemSecondLevArr);

            if ( checkTargetElem.parent('ul').hasClass('jspPane') && checkTargetElem.children('ul').hasClass('third-level')) {
                var heightElemThirdLev = checkTargetElem.find('.third-level > li');
                var biggerHeight = ( jspPaneTop + heightElemArr[1] > heightElemArr[0] )? jspPaneTop + heightElemArr[1] : heightElemArr[0];

                if (heightWrapperDropMenu + shadowHeight - jspPaneTop >= 0) {
                    if (heightWrapperDropMenu < heightElemArr[0] && heightWrapperDropMenu < heightElemArr[1]) {
                        checkTargetElem.parent('.jspPane').css('height', biggerHeight + shadowHeight + 'px');
                        customnScroll();
                    }
                    if (heightWrapperDropMenu < heightElemArr[0]+ shadowHeight && heightWrapperDropMenu < heightElemArr[1]+ shadowHeight) {
                        checkTargetElem.parent('.jspPane').css('height', biggerHeight + shadowHeight + 'px');
                        customnScroll();
                    }
                };

                for (var i = 0; i < heightElemThirdLev.length;  i++ ) heightElemThirdLevArr.push(heightElemThirdLev.eq(i)[0].offsetHeight);

                checkTargetElem.addClass('mouseenter');
                checkTargetElem.parents('.wrapper-dropdown-menu').removeClass('fourth_visible second_visible').addClass('third_visible second_scroll');

                if (heightElemArr.length > 2) heightElemArr.pop();
                if (heightElemArr.length <= 2 && arraySum(heightElemThirdLevArr) != 0) heightElemArr[1] = arraySum(heightElemThirdLevArr);

                if (heightWrapperDropMenu < heightElemArr[0] + shadowHeight || heightWrapperDropMenu < heightElemArr[1] + shadowHeight) {
                    checkTargetElem.find('.third-level').children('li').eq(0).css('margin-top', jspPaneTop + 'px');
                } else {
                    checkTargetElem.find('.third-level').children('li').eq(0).css('margin-top', '0')
                };

            };

            if ( checkTargetElem.parent('ul').hasClass('third-level') && ! checkTargetElem.parent('ul').hasClass('fourth-level') ) {
                productDropMenu.removeClass('second_scroll').addClass('third_scroll');
                checkTargetElem.addClass('mouseenter');
            };

            if ( checkTargetElem.children('ul').hasClass('fourth-level') ) {
                var heightElemFourthLev = checkTargetElem.find('.fourth-level > li');
                topFourth = checkTargetElem[0].offsetTop;

                for (var i = 0; i < heightElemFourthLev.length;  i++ ) heightElemFourthLevArr.push(heightElemFourthLev.eq(i)[0].offsetHeight);
                heightElemArr[2] = arraySum(heightElemFourthLevArr);

                // var biggerHeight = ( jspPaneTop + heightElemArr[2] > heightElemArr[0] )? jspPaneTop + heightElemArr[2] : jspPaneTop + heightElemArr[2];
                var biggerHeight = ( jspPaneTop + heightElemArr[2] > heightElemArr[0] )? jspPaneTop + heightElemArr[2] : heightElemArr[0];

                checkTargetElem.parents('.wrapper-dropdown-menu').removeClass('second_visible third_visible').addClass('fourth_visible');

                if ( heightWrapperDropMenu < heightElemArr[0] + shadowHeight ) {
                    productDropMenu.find('.jspPane').css('height', biggerHeight + shadowHeight + 'px');
                    customnScroll();
                    checkTargetElem.find('.fourth-level').children('li').eq(0).css('margin-top', jspPaneTop + 'px');
                } else {
                    productDropMenu.find('.jspPane').css('height', biggerHeight + shadowHeight + 'px');
                    checkTargetElem.find('.fourth-level').children('li').eq(0).css('margin-top', '0')
                    customnScroll();
                };
            };

            if ( checkTargetElem.parent('ul').hasClass('fourth-level') && checkTargetElem.children('ul').length != 1 ) {
                productDropMenu.removeClass('third_scroll').addClass('fourth_scroll');
                // console.error('Fourth 1');
                checkTargetElem.addClass('mouseenter');
            };
            productDropMenu.children().bind('scroll', function(e) {
                var jspPaneTopScroll = (-parseFloat(productDropMenu.find('.jspPane').css('top')));

                if ( checkTargetElem.parents('.wrapper-dropdown-menu').hasClass('second_scroll') ) {
                    if (heightWrapperDropMenu < heightElemArr[0] && heightWrapperDropMenu > heightElemArr[1]) {
                        checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if (heightWrapperDropMenu < heightElemArr[0] && heightWrapperDropMenu < heightElemArr[1]) {
                        checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if (heightWrapperDropMenu > heightElemArr[0] && heightWrapperDropMenu > heightElemArr[1]) {
                        checkTargetElem.parents('.jspPane').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if (heightWrapperDropMenu > heightElemArr[0] && heightWrapperDropMenu < heightElemArr[1]) {
                        checkTargetElem.parents('.jspPane').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if (heightWrapperDropMenu < heightElemArr[0] + shadowHeight && heightWrapperDropMenu > heightElemArr[1] + shadowHeight) {
                        checkTargetElem.parents('.jspPane').children('li').eq(0).css('margin-top', '0px');
                        checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if (heightWrapperDropMenu < heightElemArr[0] + shadowHeight && heightWrapperDropMenu < heightElemArr[1] + shadowHeight) {
                        checkTargetElem.parents('.jspPane').children('li').eq(0).css('margin-top', '0px');
                        checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };

                };
                if ( checkTargetElem.parents('.wrapper-dropdown-menu').hasClass('third_scroll') ) {
                    if (heightWrapperDropMenu > heightElemArr[0]) {
                        if ( heightWrapperDropMenu < heightElemArr[1] && heightWrapperDropMenu < heightElemArr[2]) {
                            checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        };
                        if (heightWrapperDropMenu < heightElemArr[1] && heightWrapperDropMenu > heightElemArr[2]) {
                            checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        };
                        if (heightWrapperDropMenu > heightElemArr[1] && heightWrapperDropMenu > heightElemArr[2]) {
                            checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                            checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        };
                        if ( heightWrapperDropMenu > heightElemArr[1] && heightWrapperDropMenu < heightElemArr[2]) {
                            checkTargetElem.parents('.jspPane').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                            checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                            checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', '0px');
                        };
                    } else {
                        if ( heightWrapperDropMenu < heightElemArr[1] && heightWrapperDropMenu < heightElemArr[2]) {
                            checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        };
                        if (heightWrapperDropMenu < heightElemArr[1] && heightWrapperDropMenu > heightElemArr[2]) {
                            checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        };
                        if (heightWrapperDropMenu > heightElemArr[1] && heightWrapperDropMenu > heightElemArr[2]) {
                            checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                            checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        };
                        if ( heightWrapperDropMenu > heightElemArr[1] && heightWrapperDropMenu < heightElemArr[2]) {
                            checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                            checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        };
                    };
                };
                if ( checkTargetElem.parents('.wrapper-dropdown-menu').hasClass('fourth_scroll') ) {
                    if ( heightWrapperDropMenu > heightElemArr[0] ) {
                        checkTargetElem.parents('.jspPane').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if ( heightWrapperDropMenu > heightElemArr[1] && heightWrapperDropMenu < heightElemArr[2] ) {
                        checkTargetElem.parents('.jspPane').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if ( heightWrapperDropMenu > heightElemArr[1] && heightWrapperDropMenu > heightElemArr[2] ) {
                        checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if ( heightWrapperDropMenu < heightElemArr[1] && heightWrapperDropMenu < heightElemArr[2] ) {
                        checkTargetElem.parents('.jspPane').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                        checkTargetElem.parents('.third-level').children('li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                    if ( heightWrapperDropMenu < heightElemArr[1] && heightWrapperDropMenu > heightElemArr[2] ) {
                        checkTargetElem.find('.fourth-level > li').eq(0).css('margin-top', jspPaneTopScroll + 'px');
                    };
                };
            });

        })
        .on('mouseleave', function (e) {
            var shadowHeight = (jQuery(window).width() < 1200)? (jQuery(this).parents('.second-level').height() * 0.5) : (jQuery(this).parents('.second-level').height() * 0.25);
            if (jQuery(this).parent().hasClass('jspPane')) {
                jQuery(this).parents('.wrapper-dropdown-menu').removeClass('third_visible').find('.jspPane > li').removeClass('mouseenter');
                jQuery(this).find('.third-level > li').eq(0).css('margin-top', '').find('.fourth-level > li').eq(0).css('margin-top', '');

            };
            if (jQuery(this).parent().hasClass('third-level')) {
                jQuery(this).parents('.wrapper-dropdown-menu').removeClass('fourth_visible third_scroll').addClass('third_visible second_scroll').find('.jspPane').css('margin-top','');
                jQuery(this).parents('.jspPane').children('li').eq(0).css('margin-top','');
                jQuery(this).parent('.third-level').children('li').removeClass('mouseenter');
                jQuery(this).find('.third-level > li').eq(0).css('margin-top', '');
            };
            if (jQuery(this).parent().hasClass('fourth-level')) {
                jQuery(this).parents('.wrapper-dropdown-menu').removeClass('fourth_scroll').addClass('third_scroll')
                    .find('.jspPane').css('height', heightElemArr[0] + heightElemArr[1] + 'px');
                jQuery(this).parent('.fourth-level').children('li').removeClass('mouseenter');
                jQuery(this).find('.fourth-level > li').eq(0).css('margin-top', '');
            };
        });
    var lengthElem = [];

    // 	Hide Body scroll and in-page-share menu
    jQuery('nav .nav-pills .dropdown-toogle').parent().children()
        .on('mouseenter', function () {
            var shadowHeight = (jQuery(window).width() < 1200)? jQuery(this).find('.second-level').height() * 0.25 : jQuery(this).find('.second-level').height() * 0.15;
            var check_unhover = $('.mainmenu .wrapper-dropdown-menu .jspPane li');
            var heightElemSecondLevArr = [];

			if ( jQuery('header .search').find('.tx-solr-search-form').is(':visible') ) {
				jQuery('header .search').find('.tx-solr-search-form').hide();
				jQuery('header .search').find('.lupe').removeClass('active');
			};
            for (var i = 0; i < check_unhover.length; i++) {
                if (check_unhover.eq(i).children('ul').length == 0) {
                    check_unhover.eq(i).addClass('unhover');
                }
            };
            var heightElemSecondLev = jQuery(this).parent('li.dropdown').find('.jspPane').children('li');
            for (var i = 0; i < heightElemSecondLev.length;  i++ ) heightElemSecondLevArr.push(heightElemSecondLev.eq(i)[0].offsetHeight);
            // console.warn('123', heightElemSecondLevArr)

            jQuery('html > body').addClass('productDropMenu_open').find('.in-page-share').addClass('close');
            jQuery(this).parent('li.dropdown').addClass('dropdown_menu_visible').children('.wrapper-dropdown-menu').addClass('second_visible').find('.jspPane').css('top', '');
            customnScroll();

            setTimeout(function() {
                jspPaneHeight = arraySum(heightElemSecondLevArr);
                jQuery(this).parent('.jspPane').css('height', jspPaneHeight + shadowHeight + 'px');
                customnScroll();
            }, 1050);
        })
        .on('mouseleave',function (e) {
        	var visibleElem = jQuery('.dropdown_menu_visible .wrapper-dropdown-menu');
            setTimeout(function() {
               lengthElem.push(visibleElem.length, lengthElem[lengthElem.length - 2] != 0);
                if (lengthElem[lengthElem.length - 2] != 0 && visibleElem[0].offsetTop <= jQuery('#topnavi').offset().top) {
                    jQuery(this).find('.jspPane').children('li').eq(0).attr('style', '')
                        .find('.third-level').children('li').eq(0).attr('style', '');
                    customnScroll();
                    productDropMenu.parent('li.dropdown').removeClass('dropdown_menu_visible');
					jQuery('header .search').find('.tx-solr-search-form').addClass('closed_for_nav').css('z-index', '-2');
                    html_close();
                };
            }, 800);
			if (lengthElem[lengthElem.length - 2] != 0 && lengthElem[lengthElem.length - 1] ) {
	            html_close();
			};
        });
		function html_close() {
				jQuery('.wrapper-dropdown-menu').removeClass('second_visible second_scroll fourth_scroll');
				jQuery('html > body').removeClass('productDropMenu_open').find('.in-page-share').removeClass('close');
		}



    /*		End New Version Product Nav	*/
    /********************************  start style filter slider  ***********************************/

    function filter_slider_position() {
        setTimeout(function() {
            var elements = $('.slider_filter .slider_mark');

            for (var i = 0; i < elements.length; i++) {
                var count = (i % 2 == 0)? true : false ;
                if (elements.eq(i).find('span').width() > 0){
                    elements.eq(i).find('span').css('margin-left', -( ( elements.eq(i).find('span').width() / 2) - 2 ) + 'px');
                };
            };
            (count)? $('.slider_filter .slider_line').addClass('even_styles') : $('.slider_filter .slider_line').addClass('odd_styles') ;
        }, 50);
    };
    function filter_slider() {
        var attributeLists = $(".catalog-filter-attribute .slider-list");
        if (attributeLists.find("#slider").attr("value") > attributeLists.find("#slider-min").attr("value") ) {
            attributeLists.parents("fieldset").addClass("checked")
                .find("#slider").attr("checked-attr", "checked");
        };


        var defaultVal = $('#slider-max').val();
        if ($(window).width() > 1180) {
            $('.slider_filter').parents('.module').on('mousemove', function(e) {
                $(this).find('.slider_line_active').width($(this).find('.value_mark').position().left);
				if (defaultVal != $('#slider-max').val() ) {
					$('.attr-list.slider-list').addClass('valueChange');
				};
            });
        } else {
        	if ($('.value_mark').length > 0) {
				$('.slider_line_active').width($('.value_mark').position().left);
				$('.slider_filter').parents('.module').on('touchmove', function(e) {
					$(this).find('.slider_line_active').width($(this).find('.value_mark').position().left);
					if (defaultVal != $('#slider-max').val() ) {
						$('.attr-list.slider-list').addClass('valueChange');
					};
				});
			};
        };

        $('.attr-list.slider-list').parent('fieldset').children('legend').on('click', function(e) {
            filter_slider_position();
        });
    };

    if ($('.catalog-filter-attribute div').hasClass('stahl-global-filter')) {
        $(window).resize(function () {
            filter_slider();
            filter_slider_position();
        });
        filter_slider();
        filter_slider_position();

    };

    /********************************  end style filter slider  ***********************************/


    var flyout_children = "";
	var flyout_parent = "";
	var flyout_link = "";

	$(document).on("click", "nav.navbar-collapse.in ul li.dropdown > a.a_center", function (e) {
		e.preventDefault();
		$(flyout_parent).removeClass("menu_left");
		$(".navbar-collapse.in").css("overflow-y", "auto");
		$(flyout_children).removeClass("menu_center");
		$(flyout_link).removeClass("a_center");
		$(".flyout_2_link").hide();
	});

	$(document).on("click", "nav.navbar-collapse.in ul li.dropdown > a:not(.a_center)", function (e) {
		e.preventDefault();
		flyout_link = $(this);
		$(flyout_link).addClass("a_center");
		flyout_parent = $(this).parents("ul.nav-pills");
		flyout_children = $(this).next("ul.dropdown-menu");
		$(flyout_parent).addClass("menu_left");
		$(".navbar-collapse.in").css("overflow-y", "hidden");
		$(flyout_children).addClass("menu_center");
		$("a.flyout_2_link").attr("href", $(this).attr("href")).show();
	});


	$(document).on("click", ".navbar-toggle", function (e) {
		$(".meta_navi_mobile").delay(500).fadeIn();
		$("#move-to-top").fadeOut();
		$("body").addClass("block_scrolling");
		set_distance_top("on");
	});

	var accordion_detail = "";
	$(document).on("click", ".panel-group .panel-heading:not(.active)", function (e) {
		e.preventDefault();
		$(this).addClass("active");
		accordion_detail = "#" + $(this).attr("child");
		$(this).next(accordion_detail).slideDown();
	});

	$(".panel-group").each(function (index, element) {
		$(this).find(".panel-heading").first().click();
	});

	$(document).on("click", ".panel-group .panel-heading.active", function (e) {
		e.preventDefault();
		$(this).removeClass("active");
		accordion_detail = "#" + $(this).attr("child");
		$(this).next(accordion_detail).slideUp();
	});



	$(document).on("click", ".accordion_close_button", function (e) {
		$(this).parent(".detail").slideUp();
	});

	$(document).on("click", ".bilder_accordion .accordion_close_button", function (e) {
		var detail_id = $(this).parent(".detail").attr("id");
		detail_id = detail_id.replace("detail_", "teaser_");
		var parent_id = $(this).parents(".bilder_accordion").attr("id");
		scrollto("#" + parent_id + " #" + detail_id);
	});




	$(".langnavi li.active").insertBefore(".langnavi li:first-child");

	$(document).on("click", ".langnavi li.active", function () {
		if (!($(this).parent(".langnavi").hasClass("drop"))) {
			$(".langnavi li:not(.active)").fadeIn("fast");
			$(this).parent(".langnavi").addClass("drop");
		} else {
			$(".langnavi li:not(.active)").fadeOut("fast");
			$(this).parent(".langnavi").removeClass("drop");
		}

	});


	$('.mainmenu ul.nav-pills > li:has(ul)').doubleTapToGo();



	/******************************************/
	var mobile_client = false;
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		mobile_client = true;
	}

	var teaser_parent = "";
	var parent_id = 0;
	var this_parent_id = "";
	var small_width = 3;
	var big_width = 6;

	function refresh_slider_width() {
		if ($(window).width() < 768) {
			small_width = 6;
			big_width = 12;
		} else {
			small_width = 3;
			big_width = 6;
		}
	}
	refresh_slider_width();


	$(".bilder_accordion.module").each(function (index, element) {
		var parent_id = "teaser_parent_" + index;
		$(this).attr("id", parent_id);

		$(this).find(".teaser").each(function (index, element) {
			var id = $(this).attr("id");
			if (typeof id !== 'undefined') {
				id = $(this).attr("id").replace("teaser_", "detail_");
				if ($("#" + parent_id + " #" + id).length > 0) {
					$(this).addClass("with-details");
				}

			}

		});

	});

	$(document).on("click", ".bilder_accordion .teaser.with-details a", function (e) {
		e.preventDefault();
	});

	$(document).on("click", ".bilder_accordion .teaser.with-details", function () {
		refresh_slider_width();
		teaser_parent = $(this).parents(".bilder_accordion");

		if (!$(teaser_parent).hasClass("active")) {
			$(teaser_parent).addClass("active");
		}
		this_parent_id = "#" + $(teaser_parent).attr("id");


		$(this_parent_id + " .detail").slideUp();
		var id = $(this).attr("id").replace("teaser_", "detail_");
		var detail_div = $(this_parent_id + " #" + id);
		var teaser_index = $(this_parent_id + " .teaser").index(this);
		var detail_pos = 0;
		var big_teaser_counter = 0;
		var small_summe = 0;
		var big_summe = 0;
		$(this_parent_id + " .teaser").each(function (index, element) {
			if (index <= teaser_index) {
				if ($(this).hasClass("big")) {
					big_teaser_counter++;
					big_summe++;
				} else {
					small_summe++;
				}
			}
		});

		if ($(window).width() < 768) {

		}
		small_summe = small_summe * small_width;
		big_summe = big_summe * big_width;
		var new_position = ((big_summe + small_summe) % 12);
		var new_summe = big_summe + small_summe;
		var after_small_summe = 0;
		var after_big_summe = 0;
		if (new_position === 0) {
			detail_pos = teaser_index;
		} else {
			for (i = 1; i < 100; i++) {
				$(this_parent_id + " .teaser").each(function (index, element) {

					if (index === (teaser_index + i)) {
						if ($(this).hasClass("big")) {
							after_big_summe++;
						} else {
							after_small_summe++;
						}
					}
				});
				if ((new_summe + (after_small_summe * small_width) + (after_big_summe * big_width)) % 12 === 0) {
					detail_pos = (teaser_index + i);
					break;
				}
			}

		}

		$(detail_div).insertAfter($(this_parent_id + " .teaser").eq(detail_pos));
		$(detail_div).stop(true, false).slideToggle(function () {
			scrollto(detail_div);
		});

	});

	var navbar_height = 0;
	var save_top_distance = 0;

	define_navbar_height();

	/** content_toggle **/
	$(document).on("click", ".content_toggle_headline", function (e) {
		$(this).parents(".content_toggle").find(".content_toggle_content").fadeToggle();
	});

	$(document).on("click", ".toggle_close", function (e) {
		$(this).parents(".content_toggle_content").fadeOut();
	});



	/*** navi tab module fix  -> MODULE in MODULE FIX***/


	$(".multilevel_step_1, .multilevel_step_2, .multilevel_step_3, .teaserboxen_volle_breite_tabs").each(function (index, element) {

		$(this).find(".module").each(function (index, element) {
			$(this).addClass("container-without-padding");
		});



	});


	$(".setheightheadline").each(function (index, element) {
		if ($(this).height() === 0) {
			$(this).hide();
		}
	});


	$(".bild_video_einbettung_content_all").each(function (index, element) {
		$(this).find(".bild_video_einbettung_image").each(function (index, element) {
			if ($(this).children("figure").length <= 0) {
				$(this).prev(".bild_video_einbettung_content").addClass("last_elem");
				$(this).hide();
			}
		});
	});


	$(".teaserboxen_volle_breite_tabs").each(function (index, element) {
		if ($(this).find(".headline-row").length <= 0) {
			$(this).addClass("tabs-without-headline");
		}
	});



	/** teaserboxen_volle_breite_tab **/
	$(document).on("click", ".teaserboxen_volle_breite_tabs ul li", function (e) {
		e.preventDefault();
		var active_li = $(this);
		var add_margin = parseInt($(active_li).children("a").css("margin-left"), 10);
		var active_li_left = $(active_li).position().left;
		var active_li_top = $(active_li).position().top + 27 + $(active_li).children("a").height();
		active_li_left = active_li_left + add_margin;

		var active_li_width = $(active_li).children("a").innerWidth() + 3;
		$(this).nextAll(".tab_slider").css("width", active_li_width).css("left", active_li_left).css("top", active_li_top);
		/*if ($(this).parents(".teaserboxen_volle_breite_tabs").find(".setheight").length > 0) {
		    setTimeout(function() {
		        sameHeight();
		    }, 700);

		}*/
	});
	$("#move-to-top").click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 1000);
	});

	/*** formular quick fix ***/

	$(".tx-powermail").each(function (index, element) {
		$(this).addClass("gutter-8");
		if ($(this).parent("div").hasClass("frame") && $(this).parent("div").hasClass("default") && !($(this).parent("div").parent("div").hasClass("location_content_form")) && !($(this).parent("div").parent("div").hasClass("layout_sticky_content")) && !($(this).parent("div").parent("div").hasClass("tx_locationsearch_callback_form"))) {
			$(this).parent("div").addClass("module");
		}
	});



	$(".frame.well").addClass("module");




	if ($(".content_marginal").length > 0) {
		$(".content_marginal").addClass("module");
	}
	if ($(".teaser_externe_footer").length > 0) {
		$(".module:not(.container-without-padding)").last().addClass("last");
		$(".subcontent-wrap .module:not(.container-without-padding)").last().addClass("last");
		$(".right-content-wrap .module:not(.container-without-padding)").last().addClass("last");
		$(".right-content-wrap .teaser_item").last().addClass("last");
		$(".location_template.module .location_content_more .module.last").parents(".location_template.module").addClass("last");

	}



	$(".content_marginal").each(function (index, element) {
		if ($(this).next(".content_bottom").length > 0) {
			$(this).addClass("marginal_bottom");
		}
	});




	$(".main-section .module").each(function (index, element) {
		if (!($(this).hasClass("content_marginal"))) {
			if (!($(this).hasClass("background"))) {
				if ($(this).parents(".content_marginal").length > 0) {
					if (!($(".subcontent-wrap > .row > .module").first().hasClass("background"))) {
						$(".subcontent-wrap > .row > .module").first().addClass("first");
					}
					if (!($(".right-content-wrap > .row > .module").first().hasClass("background"))) {
						$(".right-content-wrap > .row > .module").first().addClass("first");
					}
					return (false);
				} else if (!($(this).hasClass("key_facts"))) {
					$(this).addClass("first");
					return (false);
				}
			} else {
				return (false);
			}
		}
	});



	/*if (!($(".container > .module").first().hasClass("background"))) {
		$(".container > .module").first().addClass("first");
	}
	if (!($(".subcontent-wrap > .module").first().hasClass("background"))) {
		$(".subcontent-wrap > .module").first().addClass("first");
	}
	if (!($(".right-content-wrap > .module").first().hasClass("background"))) {
		$(".right-content-wrap > .module").first().addClass("first");
	}*/




	$(".content_bottom").each(function (index, element) {
		if ($(this).find(".module").length <= 0) {
			if ($(".teaser_externe_footer").length > 0) {
				$(".content_marginal .subcontent-wrap .module:not(.container-without-padding)").last().addClass("last");
			}
			$(this).hide();
		}
	});




	/*** check if site has more then one first module ***/
	/*	if($(".module.first").length > 1){
			$(".module.first").each(function(index, element) {
	            if(index !== 0)
				{
					$(this).removeClass("first");
				}
	        });
		}*/


	/** news_feed **/

	$(document).on("click", ".news_feed ul li, .product_download ul li, .product_accessories ul li, .tab_navigation ul li", function (e) {

		e.preventDefault();
		var active_li = $(this);
		var temp;
		$(this).parents("ul").children("li").removeClass("active");
		$(this).addClass("active");
		if ($(window).width() > 991 || $(this).parents("ul").hasClass("no_default")) {
			var add_margin = parseInt($(active_li).css("margin-left"), 10);
			var active_li_left = $(active_li).position().left;
			var active_li_top = $(active_li).position().top + 47;
			var additional_height = 0;
			if ($(active_li).children("a").length > 0) {
				additional_height = $(active_li).children("a").height();
			} else {
				additional_height = $(active_li).outerHeight() - 56;
			}
			active_li_top = active_li_top + additional_height;
			active_li_left = active_li_left + add_margin;
			temp = active_li_left;

			var active_li_width = $(active_li).innerWidth() + 3;
			$(this).nextAll(".tab_slider").css("width", active_li_width).css("left", active_li_left).css("top", active_li_top);
		}
		createCookie("pos_cookie", temp);
	});
	$(document).on("click", ".news-menu-view > h3", function (e) {
		/*	$(".news-menu-view h3").removeClass("active");*/
		$(".news-menu-view ul").slideUp();
		$(this).next("ul").stop().slideDown();
	});

	var active_year = false;
	if ($(".news-menu-view").length > 0) {
		$(".news-menu-view").each(function (index, element) {
			if ($(this).find("li.itemactive").length > 0) {
				var active_li = $(this).find("li.itemactive");
				$(active_li).parent("ul").prev("h3").addClass("active").click();
				active_year = true;
			}

		});
		if (!active_year) {
			$(this).children("h3").first().addClass("active");
		}
	}

	var refresh_needed = false;

	function refresh_tab_size() {
		if ($(window).width() > 991 && refresh_needed) {
			refresh_needed = false;
			$("ul:not(.no_default) li.tab_slider").each(function (index, element) {
				var active_li = $(this).siblings("li.active");
				var add_margin = parseInt($(active_li).css("margin-left"), 10);
				var active_li_left = $(active_li).position().left;
				var active_li_top = $(active_li).position().top + 47;
				var additional_height = 0;
				if ($(active_li).children("a").length > 0) {
					additional_height = $(active_li).children("a").height();
				} else {
					additional_height = $(active_li).outerHeight() - 56;
				}
				active_li_top = active_li_top + additional_height;
				active_li_left = active_li_left + add_margin;
				var active_li_width = $(active_li).innerWidth() + 3;
				$(this).css("width", active_li_width).css("left", active_li_left).css("top", active_li_top);
			});
		} else if ($(window).width() < 992 && !(refresh_needed)) {
			refresh_needed = true;
		}
	}

	$(window).resize(function () {
		refresh_tab_size();
	})







	function description_last_in_form(description, last_elem) {
		if (description.length > 0) {
			if ($(window).width() < 768) {
				$(description).insertAfter($(last_elem));
			}
		}
	}

	// News Category
	var only_one_scroll = true;
	var first_click = false;
	$(document).on("click", ".news_feed.module ul li", function (e) {
		$('.news-list-view .article, .news_list .article').removeClass("even");
		only_one_scroll = true;
		$(".news-list-view .article, .news_list .article").hide();
		var id = $(this).attr("id");
		console.log(readCookie('history_back'));
		if (id === "all") {
			if (!first_click) {
				first_click = true;
				$(".news-list-view .article, .news_list .article").fadeIn(function () {
					$('.news-list-view .article:visible:odd, .news_list .article:visible:odd').addClass("even");
					if ($(window).width() < 991 && only_one_scroll) {
						only_one_scroll = false;
						scrollto(".news_list .article");
					}
				});
			} else {

				eraseCookie('history_back');
				$(".news-list-view .article, .news_list .article").hide();
				$('.news-list-view .article:visible:odd, .news_list .article:visible:odd').addClass("even");
				location.reload();
			}
		}
		else{
			createCookie('history_back', id );
			$(".news-list-view ." + id + " , .news_list ." + id).fadeIn(function () {
			$('.news-list-view .article:visible:odd, .news_list .article:visible:odd').addClass("even");
			if ($(window).width() < 991 && only_one_scroll) {
				only_one_scroll = false;
				scrollto(".news_list ." + id);
			}
		});
		}

	});

	$('.news-list-view .article:visible:odd, .news_list .article:visible:odd').addClass("even");

	$(window).load(function() {
  if(readCookie('history_back')){
		history_back_cookies(readCookie('history_back'));
	}
});


	function history_back_cookies(cok){

		if(cok==="all"){
			$(".news-list-view .article, .news_list .article").show();
		}
		else{
			var ulWidth = 0;
			$(".news_feed ul li").each(function() {
			var inde= $(this, 'ul').index();
			if(inde < $("#"+cok,'ul').index()){
				 ulWidth = ulWidth + $(this).width() + 50;
				 active_li_width = $("#"+cok).width();
			}});

			$(".tab_slider").css("left", ulWidth ).css("width", active_li_width);
			$(".news_feed ul li").removeClass("active");
			$(".news_feed ul #" + cok).addClass("active");
			$(".news-list-view .article, .news_list .article").hide();
			$(".news-list-view ." + cok + " , .news_list ." + cok).fadeIn(function (e) {
			$('.news-list-view .article:visible:odd, .news_list .article:visible:odd').addClass("even");
			});
		}


	}

	/*** Theme Controller ***/
	var variante = $(".slider").attr("theme");
	if (variante == "dark") {
		$("body").addClass("dark-theme");
		$(".slider a.button").addClass("blue");
	}


	var distance_to_top = 0;
	$(document).on("click", ".in-page-share .slider_btn", function () {
		if (!$(this).parent(".in-page-share").hasClass("closed")) {
			$(this).parent(".in-page-share").addClass("closed");
		} else {
			$(this).parent(".in-page-share").removeClass("closed");
			distance_to_top = $(document).scrollTop();
		}
	});

	/*** Slider headline ***/
	function viewport() {
		var e = window,
			a = 'inner';
		if (!('innerWidth' in window)) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		return {
			width: e[a + 'Width'],
			height: e[a + 'Height']
		};
	}

	var vpWidth = viewport().width; // This should match your media query


	var pos_flag = false;

	function pos_headline() {
		if (viewport().width > 991 && (!pos_flag)) {
			pos_flag = true;
			$(".slider .hero-wrapper").each(function (index, element) {
				var width = $(window).width();
				var skal_width = 1450 / width;
				var full_height = $(this).actual("height");
				var skal_height = full_height * skal_width;
				var top = 68 / skal_height * 100;
				$(this).children(".hero-wrapper_content").css("top", top + "%");
			});
		} else if (viewport().width <= 991) {
			if (pos_flag === true) {}

			pos_flag = false;
		}
	}
	pos_headline();

	$(window).resize(function (e) {
		pos_headline();
	});


	/*** Product Details ***/

	var product_carousel = $(".product_carousels").owlCarousel({
		margin: 0,
		loop: false,
		autoWidth: false,
		items: 1,
		nav: true,
		navText: false,
		dots: true,
		autoHeight: true,
	});

	if ($(".product-media .images > a > div > img").length > 0) {
		$(".product-media .images > a > div > img").each(function () {
			var img = $(this).attr("src");
			$(this).parent("div").css("background-image", "url('" + img + "')");
		});
	}

	$(document).on("click", ".product-media .images a", function (e) {
		e.preventDefault();
		var elem = $(this).children("div");
		if (!($(elem).hasClass("active"))) {
			$(".product-media .images a > div").removeClass("active");
			$(elem).addClass("active");
			var carousel = $(elem).attr("carousel");
			$(".product_carousels").hide(),
				$("#" + carousel).fadeIn();
		}

	});

	$(document).on("click", ".product_download.module ul li", function (e) {
		$(".product_download .details_container .detail").hide();
		var id = $(this).attr("id");
		$("#" + id + "_detail").fadeIn();
	});


	$(document).on("click", ".product_accessories.module ul li", function (e) {
		$(".product_accessories .details_container .detail").hide();
		var id = $(this).attr("id");
		$(".detail[category='" + id + "']").fadeIn();
	});


	/*** Product Category ***/
	$(document).on("click", ".fylout_icon", function () {
		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
		} else {
			$(this).addClass("active");
		}
		$(".headline-row > .flyout_container").slideToggle("fast");
	});


	/*** products_compare_table ***/
	var item_carousel = $("#item_carousel");
	$(item_carousel).owlCarousel({
		margin: 0,
		loop: false,
		autoWidth: false,
		items: 3,
		nav: true,
		navText: false,
		dots: true,
		responsive: {
			0: {
				items: 1,
			},
			768: {
				items: 3,
			}
		}

	});

	var change_item = "";

	function set_start_order() {
		$("#item_carousel .owl-item").each(function (index, element) {
			$(this).attr("order_number", index);
		});
	}
	set_start_order();

	function refresh_order(pos) {
		$("#item_carousel .owl-item").each(function (index, element) {
			var old_number = $(this).attr("order_number");
			var new_number = 0;
			if (old_number > pos) {
				new_number = old_number - 1;
			} else {
				new_number = old_number;
			}
			$(this).attr("order_number", new_number);
		});
	}

	//remove
	$(document).on("click", "#item_carousel .head .remove", function () {
		change_item = $(this).parents(".owl-item");
		var position = change_item.attr("order_number");

		item_carousel.trigger('remove.owl.carousel', [position]);
		item_carousel.trigger('refresh.owl.carousel');
		refresh_order(position);

	});

	//change position
	var left = false;
	$(document).on("click", "#item_carousel .head .change_pos div", function () {
		var last = false;
		var first = false;
		change_item = $(this).parents(".owl-item");
		var item_counter = ($("#item_carousel .owl-item").length - 1);
		var position = change_item.index();
		var pos_index = 0;
		if ($(this).hasClass("right")) {
			left = false;
			if (position === item_counter) {
				pos_index = 0;
				left = true;
				last = true;
			} else {
				pos_index = position + 1;
			}
		} else {
			left = true;
			if (position === 0) {
				pos_index = item_counter;
				left = false;
				first = true;
			} else {
				pos_index = position - 1;
			}
		}

		var move_elem = $("#item_carousel .owl-item").eq(pos_index);
		if (!left) {
			$(change_item).insertAfter(move_elem);

		} else {
			$(change_item).insertBefore(move_elem);
		}

		if (first) {
			move_elem = $("#item_carousel .owl-item:first-child");
		} else if (last) {
			move_elem = $("#item_carousel .owl-item:last-child");
		}

		$(change_item).addClass("move_mark");
		$(move_elem).addClass("stop_hover");


		setTimeout(function () {
			$(change_item).removeClass("move_mark");
			$(move_elem).removeClass("stop_hover");
		}, 1000);


		item_carousel.trigger('refresh.owl.carousel');

	});

	$(document).on("click", ".products_compare_table .close_table", function () {
		$(".products_compare_table.module").fadeOut();
	});

	$(document).on("click", ".products_compare_table .left_column .head .options", function () {
		if (!$(this).hasClass("active")) {
			$(this).addClass("active");
			$(".table_wrapper .product_item > .head > .image .remove, .table_wrapper .product_item > .head > .image .change_pos").fadeIn();
		} else {
			$(this).removeClass("active");
			$(".table_wrapper .product_item > .head > .image .remove, .table_wrapper .product_item > .head > .image .change_pos").fadeOut();
		}
	});




	/*** Anprechpartner Mobile ***/
	$(document).on("click", "img.person_mobile_contact", function () {
		$(".person_mobile_content").addClass("mobile_slide");
		$(".person_mobile .layout_sticky_content_close").fadeIn();
		$("body").addClass("block_scrolling");
		set_distance_top("on");
	});

	$(document).on("click", ".person_mobile .layout_sticky_content_close", function () {
		$(".person_mobile .layout_sticky_content_close").fadeOut("fast");
		$(".person_mobile_content").removeClass("mobile_slide");
		$("body").removeClass("block_scrolling");
		set_distance_top("off");
	});

	/*** Contact Flyout Mobile ***/
	$(document).on("click", ".meta_navi_mobile .item.kontakt", function () {
		$(".contact_mobile").addClass("mobile_slide");
	});

	$(document).on("click", ".contact_mobile > .layout_sticky_content_close", function () {
		$(".contact_mobile").removeClass("mobile_slide");
	});

	var was_sticky = false;
	var distance = 0;
	var distance_minus = 0;

	function set_distance_top(stance) {
		if (stance === "on") {
			distance = $(document).scrollTop();
			distance_minus = distance * (-1);
			if ($(".language_url > .main_element").hasClass("sticky")) {
				was_sticky = true;
			} else {
				was_sticky = false;
			}
			$(".navitop_left_side, .navbar-toggle, .search, #move-to-top").fadeOut("fast", function () {
				$(".logo_background, #move-to-top").css("visibility", "hidden");
				$("body").css({
					"margin-top": distance_minus,
					"position": "fixed",
				});
			});
		} else {
			$("body").css({
				"margin-top": 0,
				"position": "relative",
			});
			$(document).scrollTop(distance);
			$(".navitop_left_side").show();
			$(".logo_background, #move-to-top").css("visibility", "visible");
			$(".navitop_left_side, .navbar-toggle, .search").fadeIn("fast");
			if (was_sticky) {
				$("#move-to-top").fadeIn("fast");

			}


		}

	}



	var pos_changed = false;

	function change_position_location_content_more() {
		var location_content = $(".location_template .location_content");
		var location_content_person = $(".location_template .location_content_person");
		var location_content_more = $(".location_template .location_content_more");
		if ($(window).width() < 992 && !(pos_changed)) {
			pos_changed = true;
			$(location_content_more).addClass("col-xs-12").addClass("change_pos").insertAfter($(location_content_person));
		} else if ($(window).width() > 991 && pos_changed) {
			pos_changed = false;
			$(location_content_more).removeClass("col-xs-12").removeClass("change_pos").insertAfter($(location_content));
		}
	}
	change_position_location_content_more();

	$(window).resize(function () {
		change_position_location_content_more();
	});

	$(document).on("click", ".page_items .anker_item", function () {
		$(".step_anker").toggleClass("display");
		$(".step_anker").toggleClass("mobile_slide");
	});


	/************** tabellen mobile ****************/
	var ios = false;

	if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
		ios = true;
		$("body").addClass("ios");
		document.addEventListener('touchmove', function (event) {
			event = event.originalEvent || event;
			if (event.scale > 1) {
				event.preventDefault();
			}
		}, false);
	}

	var last_width = 0;

	function table_mobile() {
		if ($(window).width() < 992 && last_width !== $(window).width()) {
			last_width = $(window).width();
			$("table:not(.table-bordered)").each(function () {
				if (!($(this).parent("div").hasClass("table_overflow"))) {
					$(this).wrap("<div class='table_overflow'></div>");
				}

			})
		}
	}

	table_mobile();

	$(window).resize(function (e) {
		table_mobile();
	});


	function second_scroll() {
		if (!ios) {
			$(".table_overflow").each(function (index, element) {
				if (!($(this).hasClass(second_scroll))) {
					var this_table = $(this);
					$(this_table).wrap("<div class='scroll_wrap'></div>");
					var copy = $(this).clone();
					$(this_table).attr("id", "table_" + index);
					var width = $(this).width()

					$(copy).addClass("second_scroll").css("width", width).attr("id", "table_copy_" + index);
					$(copy).insertAfter(this_table);
				}
			})
		}
	}


	/*** CALENDAR ***/
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1;
	var yyyy = today.getFullYear();

	today = dd + '/' + mm + '/' + yyyy;

	var active_month = mm;
	var all_month = $(".right-content-wrap .calendarize table").length;


	if ($(".calendarize").length > 0) {
		init_calendar();
	}

	function init_calendar() {
		$("<div class='prev'></div><div class='next'></div>").appendTo("div:not(.subcontent-wrap) > div .calendarize");
		$(".right-content-wrap .calendarize table:nth-child(" + active_month + ")").addClass("active");
		$(".content_top .calendarize table:nth-child(" + active_month + ")").addClass("active");
		active_month = ($(".right-content-wrap .calendarize table.active").index() + 1);
		set_day();



	}

	function set_day() {
		$(".right-content-wrap .calendarize table tbody tr > td").removeClass("selectedDay");
		$(".right-content-wrap .calendarize table").each(function (index, element) {
			var active_month = false;
			if ($(this).hasClass("active")) {
				active_month = true;
			}
			var last_month = true;
			var next_month = false;

			$(this).find("tbody").find("td").each(function (index, element) {

				var cal_day = $(this).text().match(/\d+/);
				if ((parseInt(cal_day[0])) === 1 && (!last_month)) {
					next_month = true;
				}
				if ((parseInt(cal_day[0])) === 1) {
					last_month = false;
				}
				if (last_month) {
					$(this).addClass("last_month");
				}
				if (next_month) {
					$(this).addClass("next_month");
				}
				if (active_month) {
					if ((parseInt(cal_day[0])) === dd) {
						$(".right-content-wrap .calendarize table.active tr > td").removeClass("selectedDay");
						$(this).addClass("selectedDay");

					}
				}
				$(this).wrapInner("<span></span>");

			});
		});
		$(".content_top .calendarize table tbody tr > td").removeClass("selectedDay");
		$(".content_top .calendarize table").each(function (index, element) {
			var active_month = false;
			if ($(this).hasClass("active")) {
				active_month = true;
			}
			var last_month = true;
			var next_month = false;

			$(this).find("tbody").find("td").each(function (index, element) {

				var cal_day = $(this).text().match(/\d+/);
				if ((parseInt(cal_day[0])) === 1 && (!last_month)) {
					next_month = true;
				}
				if ((parseInt(cal_day[0])) === 1) {
					last_month = false;
				}
				if (last_month) {
					$(this).addClass("last_month");
				}
				if (next_month) {
					$(this).addClass("next_month");
				}
				if (active_month) {
					if ((parseInt(cal_day[0])) === dd) {
						$(".content_top .calendarize table.active tr > td").removeClass("selectedDay");
						$(this).addClass("selectedDay");

					}
				}
				$(this).wrapInner("<span></span>");

			});
		});

		add_days_name();

	}

	function add_days_name() {
		$("<tr class='days_des'><td>M</td><td>D</td><td>M</td><td>D</td><td>F</td><td>S</td><td>S</td></tr>").insertBefore($(".right-content-wrap .calendarize table tbody tr:first-child"));
		$("<tr class='days_des'><td>M</td><td>D</td><td>M</td><td>D</td><td>F</td><td>S</td><td>S</td></tr>").insertBefore($(".content_top .calendarize table tbody tr:first-child"));
	}

	$(document).on("click", ".calendarize .prev", function () {
		if (active_month === all_month) {
			$(".right-content-wrap .calendarize .next").fadeIn("fast");
			$(".content_top .calendarize .next").fadeIn("fast");
		}
		if (active_month > 1) {
			$(".right-content-wrap .calendarize table").removeClass("active");
			$(".content_top .calendarize table").removeClass("active");
			active_month--;
			$(".right-content-wrap .calendarize table:nth-child(" + active_month + ")").addClass("active");
			$(".content_top .calendarize table:nth-child(" + active_month + ")").addClass("active");
		}
		if (active_month === 1) {
			$(".right-content-wrap .calendarize .prev").hide();
			$(".content_top .calendarize .prev").hide();
		}

	});

	$(document).on("click", ".calendarize .next", function () {
		if (active_month === 1) {
			$(".right-content-wrap .calendarize .prev").fadeIn("fast");
			$(".content_top .calendarize .prev").fadeIn("fast");
		}
		if (active_month <= (all_month - 1)) {
			$(".right-content-wrap .calendarize table").removeClass("active");
			$(".content_top .calendarize table").removeClass("active");
			active_month++;
			$(".right-content-wrap .calendarize table:nth-child(" + active_month + ")").addClass("active");
			$(".content_top .calendarize table:nth-child(" + active_month + ")").addClass("active");
		}
		if (active_month === all_month) {
			$(".right-content-wrap .calendarize .next").hide();
			$(".content_top .calendarize .next").hide();
		}
	});

	var calendar_href = "";

	$(document).on("click", ".calendarize tr td", function () {
		if ($(this).hasClass("hasEvents")) {
			var second_calendar_href = $(this).find("a").attr('href');
			if (isTouch) {
				if (calendar_href === second_calendar_href) {
					window.location.href = second_calendar_href;
				}
			} else {
				window.location.href = second_calendar_href;
			}
			calendar_href = second_calendar_href;
		} else {
			calendar_href = "";
		}
	});

	$('.right-content-wrap .calendarize table tr td ul.events a');
	$('.content_top .calendarize table tr td ul.events a');


	/* IMAGE MAP */
	/* CONFIGRATION VIEW SECTION */
	if (getQueryVariable("ADMCMD_editIcons") === "1") {
		$(".image_map_info").show();
		$(".image_map").each(function (index) {
			$(this).find("img").attr("id", "image_map_img_" + index);
			$(this).find(".image_map_info").attr("id", "image_map_info_" + index);

			$("#image_map_img_" + index).attr("data-id", index);

			$(this).find(".image_map_inner").append("<div class='map_point_demo' id='map_point_demo_" + index + "'></div>");
		});
		$(".image_map_point_detail_wrapper").remove();
		$(".image_map img").click(function (event) {
			var id = $(this).attr("data-id");
			var image_width = $("#image_map_img_" + id).width();
			var image_height = $("#image_map_img_" + id).height();

			var rect = document.getElementById("image_map_img_" + id).getBoundingClientRect();
			var x = event.clientX - rect.left;
			var y = event.clientY - rect.top;

			var perc_x = (100 / image_width) * x;
			var perc_y = (100 / image_height) * y;

			$("#map_point_demo_" + id).css("left", perc_x + "%");
			$("#map_point_demo_" + id).css("top", perc_y + "%");
			$("#map_point_demo_" + id).show();

			$("#image_map_info_" + id).find(".image_map_info_x").val(perc_x.toFixed(2));
			$("#image_map_info_" + id).find(".image_map_info_y").val(perc_y.toFixed(2));
		});
	}


	/*** Login quick add class ***/
	if ($("#pwrk-login-form").length > 0) {
		$("#pwrk-login-form").each(function (index, element) {
			$(this).parent("div").addClass("login_wrapper");
		});
	}

	/* $(window).on("load", function() {
	     $("th.pwrk-jobboard-col-select > select").selecter();
	 });*/


	/*** kapitel menu sticky ***/
	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : sParameterName[1];
			}
		}
	};

	if ($(".multilevel_step_4_anker").length > 1) {
		$(".multilevel_step_4_anker").each(function (index_parent, element) {
			$(this).find(".step_anker_content").children("div").each(function (index, element) {
				$(this).attr("id", "anker_" + index_parent + "_" + index);
			});

			$(this).find(".side_sticky_nav").find("a.nav-link").each(function (index, element) {
				$(this).attr("href", "#anker_" + index_parent + "_" + index);
			});
		});
	}

	function check_multilevel_step_4_anker_visibility() {
		$(".multilevel_step_4_anker").each(function (index_parent, element) {
			if ($(this).is(":visible")) {
				$(this).addClass("step_anker_active");
			} else {
				$(this).removeClass("step_anker_active");
			}
		});
	}
	check_multilevel_step_4_anker_visibility();


	var tab_param = getUrlParameter('tab');

	if (tab_param !== undefined && tab_param !== null) {
		/*scrollto(".nav-tabs li.tab a[href='#"+tab_param+"']");*/
		$(".nav-tabs li.tab a[href='#" + tab_param + "']").click();
		if ($(".multilevel_step_4_anker").length > 1) {
			$(".multilevel_step_4_anker").removeClass("step_anker_active");
			$("#" + tab_param).find(".multilevel_step_4_anker").addClass("step_anker_active");
		}

	}


	$(document).on("click", ".teaserboxen_volle_breite_tabs .nav-tabs li.tab a", function () {
		if ($(".multilevel_step_4_anker").length > 1) {
			var id = $(this).attr("href");
			$(".multilevel_step_4_anker").removeClass("step_anker_active");
			$(id).find(".multilevel_step_4_anker").addClass("step_anker_active");
		}
	});


	var sticky_side, side_left_offset, sticky_distance_to_menu, sticky_nav_height, content_left_height, sticky_side_start, sticky_side_stop, sticky_navi_new = 0;

	if ($(".step_anker").length > 0) {
		$("img").each(function (index, element) {
			if ($(this).hasClass("lazyload")) {
				var src = $(this).attr("data-src");
				$(this).attr("src", src);
				$(this).load();
			}
		});
		set_anker_vars();
	} else {
		$(".page_items .anker_item").css("background-image", "none");
	}

	function set_anker_vars() {
		sticky_side = 0;
		side_left_offset = 0;
		sticky_distance_to_menu = (-17);
		sticky_navi_new = $(".main_element").outerHeight(false) + $(".language_url").height();
		sticky_side_start = $(".step_anker_active .step_anker").offset().top - sticky_navi_new - sticky_distance_to_menu;
		sticky_nav_height = $(".step_anker_active .step_anker").height();
		content_left_height = $(".step_anker_active .step_anker_content").height();
	}

	$(window).on("load", function () {
		if ($(".step_anker").length > 0) {
			sticky_side_start = $(".step_anker_active .step_anker").offset().top - sticky_navi_new - sticky_distance_to_menu;
		}
	});

	function side_nav_refresh_var() {
		sticky_nav_height = $(".step_anker_active .step_anker").height();
		sticky_side_stop = ($(".step_anker_active .step_anker").closest(".module").offset().top) + ($(".step_anker_active .step_anker").closest(".module").height()) - sticky_nav_height - sticky_distance_to_menu - sticky_navi_new - 32;
	}


	function sticky_side_menu() {
		if ($(".step_anker").length > 0) {
			side_nav_refresh_var();
			var top_distance = 0;
			top_distance = $(window).scrollTop();
			if (top_distance > sticky_side_start && sticky_side === 0 && top_distance < sticky_side_stop) {
				side_left_offset = $(".step_anker_active .step_anker").offset().left;
				$(".step_anker_active .step_anker").addClass("sticky_side");
				$(".step_anker_active .step_anker").css("left", side_left_offset);
				$(".step_anker_active .step_anker").removeClass("fixed_bottom");
				sticky_side = 1;
			} else if (top_distance <= sticky_side_start && sticky_side) {
				$(".step_anker_active .step_anker").removeClass("sticky_side");
				$(".step_anker_active .step_anker").css("left", "auto");
				sticky_side = 0;
			} else if (top_distance > sticky_side_stop && sticky_side) {
				$(".step_anker_active .step_anker").removeClass("sticky_side");
				$(".step_anker_active .step_anker").addClass("fixed_bottom");
				sticky_side = 0;
			}
		}

	}
	if ($(".step_anker").length > 0) {
		var scrollspy_offset = sticky_navi_new - sticky_distance_to_menu;

		$('body').scrollspy({
			target: '.step_anker_active .side_sticky_nav',
			offset: scrollspy_offset
		});
	}

	$(".step_anker a.nav-link").click(function (e) {
		e.stopPropagation();
		var elem = $(this).attr("href");
		scrollto(elem);
	});


	function refresh_anker_right() {
		if ($(".step_anker_active .step_anker").hasClass("sticky_side")) {
			$(".step_anker_active .step_anker").removeClass("sticky_side");
			$(".step_anker_active .step_anker").css("left", "auto");
			side_left_offset = $(".step_anker_active .step_anker").offset().left;
			$(".step_anker_active .step_anker").css("left", side_left_offset);
			$(".step_anker_active .step_anker").addClass("sticky_side");
		} else if ($(".step_anker_active .step_anker").hasClass("fixed_bottom")) {
			$(".step_anker_active .step_anker").removeClass("fixed_bottom");
			$(".step_anker_active .step_anker").css("left", "auto");
			side_left_offset = $(".step_anker_active .step_anker").offset().left;
			$(".step_anker_active .step_anker").css("left", side_left_offset);
			$(".step_anker_active .step_anker").addClass("fixed_bottom");
		}
	}

	$(window).resize(function (e) {
		refresh_anker_right();
	});


	/*** product_headline_flyout_extended ***/
	var last_detail = "";
	var img_active = "";
	var last_img = "";

	if ($(".product_headline_flyout_extended .info_bar ul li img").length > 0) {
		$(".product_headline_flyout_extended .info_bar ul li img").each(function (index, element) {
			var cloned = $(this).clone();
			var src = cloned.attr("src")
			src = src.replace(".png", "_active.png");
			cloned.attr("src", src);
			cloned.addClass("hover").insertAfter($(this));
		});
	}

	$(document).on("click", ".info_bar ul li", function () {
		var detail = $(this).attr("detail");
		if (!(detail === last_detail)) {
			$(".product_headline_flyout_extended .info_bar ul li").removeClass("active");
			$(".product_headline_flyout_extended .add_content").hide();
			$(".product_headline_flyout_extended .add_content." + detail).slideDown(function () {
				if (detail === "detailinfos") {
					scrollto(".add_content.detailinfos");
				}
			});
			$(this).addClass("active");
		} else {
			$(".product_headline_flyout_extended .add_content." + detail).slideToggle(
				function () {
					if (detail === "detailinfos") {
						scrollto(".add_content.detailinfos");
					}
				});
			$(this).toggleClass("active");
		}
		last_detail = $(this).attr("detail");
		if (detail === "downloads") {
			$(".product_download.module ul li:first-child").click();
			/*			updateTables();*/
		}

	});



	if ($(".slider.anker a > .anker").length > 0) {
		$(".slider.anker").addClass("blur");
	}

	/*** teaserzeile_te_07 background mod ***/

	/*if($(".headline-row.with-text-and-link").length > 0)
		{
			$(".headline-row.with-text-and-link").each(function(index, element) {
				var flag = true;
	            $(this).find("p").each(function(index, element) {
					if($(this).text().trim() === ""){
						$(this).remove();
					}
					else{
						console.log("else");
						flag = false;
					}
				});
				$(this).find("a").each(function(index, element) {
					if($(this).attr("src") === ""){
						$(this).remove();
					}
					else{
						flag = false;
					}
				});



				if(flag)
				{
					$(this).addClass("without-text");
				}

	        });
		}
	*/

	function set_headlines() {
		$(".headline-row").each(function (index, element) {
			if ($(this).find("a").length === 0 && $(this).find("span.more_news").length === 0 && $(this).find("p").length === 0 && $(this).find(":header").length === 0) {
				$(this).remove();
			} else if ($(this).find("a").length === 0 && $(this).find("span.more_news").length === 0 && $(this).find("p").length === 0 && $(this).find(":header").length > 0) {
				$(this).addClass("only-headline");
			} else if ($(this).find("a").length === 0 && $(this).find("span.more_news").length === 0 && $(this).find("p").length > 0 && $(this).find(":header").length === 0) {
				$(this).addClass("only-p");
			} else if (($(this).find("a").length > 0 || $(this).find("span.more_news").length > 0) && $(this).find("p").length === 0 && $(this).find(":header").length === 0) {
				$(this).addClass("only-cta");
			} else if ($(this).find("a").length === 0 && $(this).find("span.more_news").length === 0 && $(this).find("p").length > 0 && $(this).find(":header").length > 0) {
				$(this).addClass("with-p");
			} else if (($(this).find("a").length > 0 || $(this).find("span.more_news").length > 0) && $(this).find("p").length === 0 && $(this).find(":header").length > 0) {
				$(this).addClass("with-cta");
			}
		});
	}

	set_headlines();

	$(".bilder_accordion .headline-row").each(function (index, element) {
		if ($(this).find(".catalog-filter").length > 0) {
			$(this).addClass("headline-search-filter");
		}
	});

	$(".bilder_accordion .headline-row").each(function (index, element) {
		if ($(this).find(".catalog-filter").length > 0) {
			$(this).addClass("headline-search-filter");
		}
	});






	/*** slide button ***/
	$(document).on("click", "a.slide_button", function (e) {
		var detail_id = $(this).attr("detail_id");
		e.preventDefault();
		if (!($(this).hasClass("active"))) {
			$(this).addClass("active");
			$(this).children("span").text("weniger zeigen");
			$("#" + detail_id).slideDown();
		} else {
			$(this).removeClass("active");
			$(this).children("span").text("mehr zeigen");
			$("#" + detail_id).slideUp();
		}
	});


	/*** search list ***/
	$(window).on("load", function () {
		$("#results-per-page select").selecter();
	});
	$(document).on("click", ".search > .lupe", function () {
		var searchWrapper = $("#tx-solr-search-form-pi-results");

		$('header #tx-solr-search-form-pi-results_2 input[type="text"]').removeClass('ui-autocomplete-input')
		if (!($(this).hasClass("active"))) {
			$(this).addClass("active");
			// $(".search .tx-solr-search-form").fadeIn(); // $("#tx-solr-search-form-pi-results > input.tx-solr-q.js-solr-q.ui-autocomplete-input").focus();

			searchWrapper.parent(".tx-solr-search-form").fadeIn();
			searchWrapper.children('.tx-solr-q.js-solr-q.ui-autocomplete-input').focus();

			if ($("body > script + .ui-autocomplete").length && $(window).width() < 990) {
				var paddingSize = ($(window).width() > 550)? parseInt($('header .search .tx-solr-search-form').css('padding').split('px ')[1]) : parseInt($('header .search .tx-solr-search-form').css('padding'));
				$("body > script + .ui-autocomplete").addClass('input_tx-solr-search-form-pi-results');
				$("body > script + .ui-autocomplete").css("max-width", $(window).width() - (paddingSize * 2) + 'px');
			};
		} else {
			$(this).removeClass("active");
			$(".search .tx-solr-search-form, body > script + .ui-autocomplete").fadeOut();
		}

	});

    $("#tx-solr-search-form-pi-results > input.tx-solr-q.js-solr-q.ui-autocomplete-input").bind("blur", function () {
        $("body > script + .ui-autocomplete").removeClass("input_tx-solr-search-form-pi-results");
    });

    $(window).resize(function (e) {
        if ($("body > script + .ui-autocomplete").length && $(window).width() < 990) {
            var paddingSize = ($(window).width() > 550)? parseInt($('header .search .tx-solr-search-form').css('padding').split('px ')[1]) : parseInt($('header .search .tx-solr-search-form').css('padding'));
			$("body > script + .ui-autocomplete").addClass('input_tx-solr-search-form-pi-results');
            $("body > script + .ui-autocomplete").css("max-width", $(window).width() - (paddingSize * 2) + 'px');
        };
    });

	function add_span_search_filter() {
		$(".tx_solr .right-content-wrap ul li a").each(function (index, element) {
			$(this).wrapInner("<span></span>");
		});

		$(".tx_solr .right-content-wrap ul li").each(function (index, element) {
			$(this).contents().filter(function () {
				return this.nodeType === 3;
			}).remove();
		});
	}
	add_span_search_filter();


	$(document).on('keyup focus click', '.search .ui-autocomplete-input', function (e) {
		var css_offset = $(this).offset(),
			css_height = $(this).outerHeight(false),
			new_top = css_offset.top + css_height + "px",
			width = $(this).outerWidth(false),
			checkForUserAgent = (checkForUserAgentGetVersion() > 0)? css_height : new_top;

		width = width + "px";
         // alert("This is IE ");
			$("ul.ui-autocomplete").style('top', checkForUserAgent, '!important');
		/*
		$("ul.ui-autocomplete").style('top', new_top, '!important');
		css("top", (css_offset.top + css_height)).css("left", css_offset.left);
		*/
	});

	function mobile_navi_wrap_last_word() {
		if ($(document).width() < 992) {
			$('.mainmenu ul > li > a').each(function (index, element) {
				var heading = $(element),
					word_array, last_word, first_part;
				word_array = heading.html().split(/\s+/); // split on spaces
				last_word = word_array.pop(); // pop the last word
				first_part = word_array.join(' '); // rejoin the first words together

				heading.html([first_part, ' <span>', last_word, '</span>'].join(''));
			});
		}
	}
	mobile_navi_wrap_last_word();

	$(window).on("orientationchange", function (event) {
		mobile_navi_wrap_last_word();
	});



});
// find sum from array values
function arraySum(array){
    var sum = 0;
    for(var i = 0; i < array.length; i++){
        sum += array[i];
    }
    return sum;
}

// user agent string checking on IE Get Version
function checkForUserAgentGetVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    if (Idx > 0) return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));
    else if (!!navigator.userAgent.match(/Trident\/7\./)) return 11;
    else return 0;
};

function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if (pair[0] == variable) {
			return pair[1];
		}
	}
	return (false);
};

// ie9 placeholder fix
jQuery(function () {
	jQuery.support.placeholder = false;
	webkit_type = document.createElement('input');
	if ('placeholder' in webkit_type) jQuery.support.placeholder = true;
});

$(function () {

	if (!$.support.placeholder) {

		var active = document.activeElement;

		$(':text, textarea, :password').focus(function () {

			if (($(this).attr('placeholder')) && ($(this).attr('placeholder').length > 0) && ($(this).attr('placeholder') != '') && $(this).val() == $(this).attr('placeholder')) {
				$(this).val('').removeClass('hasPlaceholder');
			}
		}).blur(function () {
			if (($(this).attr('placeholder')) && ($(this).attr('placeholder').length > 0) && ($(this).attr('placeholder') != '') && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
				$(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
			}
		});

		$(':text, textarea, :password').blur();
		$(active).focus();
		$('form').submit(function () {
			$(this).find('.hasPlaceholder').each(function () {
				$(this).val('');
			});
		});
	}
});

(function ($) {
	if ($.fn.style) {
		return;
	}

	// Escape regex chars with \
	var escape = function (text) {
		return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
	};

	// For those who need them (< IE 9), add support for CSS functions
	var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;
	if (!isStyleFuncSupported) {
		CSSStyleDeclaration.prototype.getPropertyValue = function (a) {
			return this.getAttribute(a);
		};
		CSSStyleDeclaration.prototype.setProperty = function (styleName, value, priority) {
			this.setAttribute(styleName, value);
			var priority = typeof priority != 'undefined' ? priority : '';
			if (priority != '') {
				// Add priority manually
				var rule = new RegExp(escape(styleName) + '\\s*:\\s*' + escape(value) +
					'(\\s*;)?', 'gmi');
				this.cssText =
					this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
			}
		};
		CSSStyleDeclaration.prototype.removeProperty = function (a) {
			return this.removeAttribute(a);
		};
		CSSStyleDeclaration.prototype.getPropertyPriority = function (styleName) {
			var rule = new RegExp(escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?',
				'gmi');
			return rule.test(this.cssText) ? 'important' : '';
		}
	}

	// The style function
	$.fn.style = function (styleName, value, priority) {
		// DOM node
		var node = this.get(0);
		// Ensure we have a DOM node
		if (typeof node == 'undefined') {
			return this;
		}
		// CSSStyleDeclaration
		var style = this.get(0).style;
		// Getter/Setter
		if (typeof styleName != 'undefined') {
			if (typeof value != 'undefined') {
				// Set style property
				priority = typeof priority != 'undefined' ? priority : '';
				style.setProperty(styleName, value, priority);
				return this;
			} else {
				// Get style property
				return style.getPropertyValue(styleName);
			}
		} else {
			// Get CSSStyleDeclaration
			return style;
		}
	};
})(jQuery);

function define_navbar_height() {
	navbar_height = $(".main_element").outerHeight(false) + $(".language_url").height();
}

function scrollto(elem) {
	define_navbar_height();
	var save_top_distance = $(elem).offset().top - navbar_height;
	$('html, body').animate({
		scrollTop: save_top_distance
	}, 600, function () {

		//check or scroll is in right place (lazy load change distance after image is load)
		/*if($(window).width() < 768)
		{
			if ($(elem).offset().top - navbar_height !== save_top_distance) {
				scrollto(elem);
			}
		}*/
	});
};
