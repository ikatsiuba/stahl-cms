<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Catalog
 */


/**
 * Catalog manager with methods for managing categories products, text, media.
 *
 * @package MShop
 * @subpackage Catalog
 */
class MShop_Catalog_Manager_DefaultExt
    extends MShop_Catalog_Manager_Default
{

    protected function _createItem( array $values = array(), array $listItems = array(), array $refItems = array(),
        array $children = array(), MW_Tree_Node_Interface $node = null )
    {
        if( $node === null )
        {
            if( !isset( $values['siteid'] ) ) {
                throw new MShop_Catalog_Exception( 'No site ID available for creating a catalog item' );
            }

            $node = $this->_createTreeManager( $values['siteid'] )->createNode();
            $node->siteid = $values['siteid'];
        }

        if( isset( $node->config ) && ( $result = json_decode( $node->config, true ) ) !== null ) {
            $node->config = $result;
        }

        return new MShop_Catalog_Item_DefaultExt( $node, $children, $listItems, $refItems );
    }

}
