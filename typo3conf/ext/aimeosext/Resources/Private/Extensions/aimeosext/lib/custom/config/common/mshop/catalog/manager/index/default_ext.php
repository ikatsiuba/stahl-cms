<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */

return array(
	'aggregate' => '
		SELECT "key", COUNT("id") AS "count"
		FROM (
			SELECT DISTINCT :key AS "key", mpro."id" AS "id"
			FROM "mshop_product" AS mpro
            left JOIN "mshop_catalog_index_product" AS pr_ind ON pr_ind."prodid" = mpro."id"
			:joins
			WHERE :cond
	        ORDER BY mpro.code ASC 
			LIMIT :size OFFSET :start
		) AS list
		GROUP BY "key"
	',
	'item' => array(
		'search' => '
			SELECT DISTINCT mpro."id" as id
			FROM "mshop_product" AS mpro
	        left JOIN "mshop_catalog_index_product" AS pr_ind ON pr_ind."prodid" = mpro."id"
            left JOIN "mshop_product_stock" AS pr_stock ON pr_stock."prodid" = mpro."id"
	        :joins
			WHERE :cond
	        ORDER BY pr_ind.pos ASC, pr_stock.stocklevel DESC 
			LIMIT :size OFFSET :start
		',
		'count' => '
			SELECT COUNT(*) AS "count"
			FROM (
				SELECT DISTINCT mpro."id" as id
				FROM "mshop_product" AS mpro
	            left JOIN "mshop_catalog_index_product" AS pr_ind ON pr_ind."prodid" = mpro."id"
				:joins
				WHERE :cond
				LIMIT 1000 OFFSET 0
			) AS list
		',
	),
	'aggregate_ps' => '
		SELECT "key", COUNT("id") AS "count"
		FROM (
			SELECT DISTINCT :key AS "key", pr_ind."pssid" AS "id"
			FROM "mshop_product" AS mpro
            JOIN "mshop_catalog_index_product" AS pr_ind ON pr_ind."prodid" = mpro."id"
			:joins
			WHERE :cond
	        ORDER BY  mcatinca.pos,mpro.code ASC 
			LIMIT :size OFFSET :start
		) AS list
		GROUP BY "key"
	',
	'item_ps' => array(
		'search' => '
			SELECT DISTINCT pr_ind."pssid" as id
			FROM "mshop_product" AS mpro
            JOIN "mshop_catalog_index_product" AS pr_ind ON pr_ind."prodid" = mpro."id"
            JOIN "mshop_product" AS pr_ind_pss ON pr_ind."pssid" = pr_ind_pss."id"
	        :joins
			WHERE :cond
	        ORDER BY  mcatinca.pos,mpro.code ASC 
			LIMIT :size OFFSET :start
		',
		'count' => '
			SELECT COUNT(*) AS "count"
			FROM (
				SELECT DISTINCT pr_ind."pssid" as id
				FROM "mshop_product" AS mpro
                JOIN "mshop_catalog_index_product" AS pr_ind ON pr_ind."prodid" = mpro."id"
                JOIN "mshop_product" AS pr_ind_pss ON pr_ind."pssid" = pr_ind_pss."id"
				:joins
				WHERE :cond
				LIMIT 1000 OFFSET 0
			) AS list
		',
	),
	'item_ps_wc' => array(
		'search' => '
			SELECT DISTINCT pr_ind."pssid" as id
			FROM "mshop_product" AS mpro
            JOIN "mshop_catalog_index_product" AS pr_ind ON pr_ind."prodid" = mpro."id"
            JOIN "mshop_product" AS pr_ind_pss ON pr_ind."pssid" = pr_ind_pss."id"
	        :joins
	        JOIN mshop_catalog_index_text AS mcatinte_wc ON (mcatinte_wc.prodid = mcatinte.prodid AND mcatinte_wc.type = \'AT_WebCode\')
			WHERE :cond
	        ORDER BY :order
			LIMIT :size OFFSET :start
		',
		'count' => '
			SELECT COUNT(*) AS "count"
			FROM (
				SELECT DISTINCT pr_ind."pssid" as id
				FROM "mshop_product" AS mpro
                JOIN "mshop_catalog_index_product" AS pr_ind ON pr_ind."prodid" = mpro."id"
                JOIN "mshop_product" AS pr_ind_pss ON pr_ind."pssid" = pr_ind_pss."id"
				:joins
				WHERE :cond
				LIMIT 1000 OFFSET 0
			) AS list
		',
	),
);
