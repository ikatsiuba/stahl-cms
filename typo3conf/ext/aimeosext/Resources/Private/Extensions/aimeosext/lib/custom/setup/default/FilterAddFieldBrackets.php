<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */


/**
 * Adds code column to media table.
 */
class MW_Setup_Task_FilterAddFieldBrackets extends MW_Setup_Task_Abstract
{
    private $_mysql = array(
        'ALTER TABLE "mshop_filter" ADD "brackets" INT(11) DEFAULT 0 AFTER "condition" '
    );


    /**
     * Returns the list of task names which this task depends on.
     *
     * @return array List of task names
     */
    public function getPreDependencies()
    {
        return array('FilterAddFields');
    }


    /**
     * Returns the list of task names which depends on this task.
     *
     * @return string[] List of task names
     */
    public function getPostDependencies()
    {
        return array();
    }


    /**
     * Executes the task for MySQL databases.
     */
    protected function _mysql()
    {
        $this->_msg( 'Adding code column to filter table', 0 ); $this->_status( '' );

        $this->_process( $this->_mysql );
    }


    /**
     * Add column to table if the column doesn't exist.
     *
     * @param array $stmts List of SQL statements to execute for adding columns
     */
    protected function _process( array $stmts )
    {
        $this->_msg( sprintf( 'Checking columns "%1$s": ', 'brackets' ), 1 );

        if( $this->_schema->tableExists( 'mshop_filter' ) === true
            && $this->_schema->columnExists( 'mshop_filter', 'brackets' ) === false )
        {
            $this->_executeList( $stmts );
            $this->_status( 'added' );
        }
        else
        {
            $this->_status( 'OK' );
        }
    }
}