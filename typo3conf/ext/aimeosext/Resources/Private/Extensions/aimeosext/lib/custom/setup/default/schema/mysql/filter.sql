--
-- Filter database definitions
--
-- Copyright (c) Metaways Infosystems GmbH, 2011
-- License LGPLv3, http://opensource.org/licenses/LGPL-3.0
--


SET SESSION sql_mode='ANSI';


--
-- Filter types
--

CREATE TABLE "mshop_filter_type" (
	-- Unique id of the filter type
	"id" INTEGER NOT NULL AUTO_INCREMENT,
	-- site id
	"siteid" INTEGER NOT NULL,
	-- domain
	"domain" VARCHAR(32) NOT NULL,
	-- Code of the filter type
	"code" VARCHAR(32) NOT NULL COLLATE utf8_bin,
	-- Name of the type
	"label" VARCHAR(255) NOT NULL,
	-- Status (0=disabled, 1=enabled, >1 for special)
	"status" SMALLINT NOT NULL,
	-- Date of last modification of this database entry
	"mtime" DATETIME NOT NULL,
	-- Date of creation of this database entry
	"ctime" DATETIME NOT NULL,
	-- Editor who modified this entry at last
	"editor" VARCHAR(255) NOT NULL,
CONSTRAINT "pk_msfiltty_id"
	PRIMARY KEY ("id"),
CONSTRAINT "unq_msfiltty_sid_dom_code"
	UNIQUE ("siteid", "domain", "code")
) ENGINE=InnoDB CHARACTER SET = utf8;

CREATE INDEX "idx_msfiltty_sid_status" ON "mshop_filter_type" ("siteid", "status");

CREATE INDEX "idx_msfiltty_sid_label" ON "mshop_filter_type" ("siteid", "label");

CREATE INDEX "idx_msfiltty_sid_code" ON "mshop_filter_type" ("siteid", "code");


--
-- Table structure for table `mshop_filter`
--

CREATE TABLE "mshop_filter" (
	-- Unique id of the tree node
	"id" INTEGER NOT NULL AUTO_INCREMENT,
	-- site id, references mshop_locale_site.id
	"siteid" INTEGER NOT NULL,
	-- filter type
	"typeid" INTEGER NOT NULL,
	-- domain the filters belongs to
	"domain" VARCHAR(32) NOT NULL,
	-- code
	"code" VARCHAR(32) NOT NULL COLLATE utf8_bin,
	-- label
	"label" VARCHAR(255) NOT NULL,
    -- Language id
    "langid" VARCHAR(5) DEFAULT NULL,
    -- attribute
    "attribute" VARCHAR(255) NOT NULL,
    -- subattribute
    "subattribute" VARCHAR(255) DEFAULT NULL,
    -- webtype
    "webtype" VARCHAR(255) NOT NULL,
	-- position
	"pos" INTEGER NOT NULL,
	-- status code (0=hidden, 1=display, >1 for anything special)
	"status" SMALLINT NOT NULL DEFAULT 0,
	-- Date of last modification of this database entry
	"mtime" DATETIME NOT NULL,
	-- Date of creation of this database entry
	"ctime" DATETIME NOT NULL,
	-- Editor who modified this entry at last
	"editor" VARCHAR(255) NOT NULL,
CONSTRAINT "pk_msfilt_id"
	PRIMARY KEY ("id"),
CONSTRAINT "unq_msfilt_sid_dom_cod_tid"
	UNIQUE ("siteid", "domain", "code", "typeid"),
CONSTRAINT "fk_msfilt_typeid"
	FOREIGN KEY ("typeid")
	REFERENCES "mshop_filter_type" ("id")
	ON UPDATE CASCADE
	ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX "idx_msfilt_sid_status_langid" ON "mshop_text" ("siteid", "status", "langid");

CREATE INDEX "idx_msfilt_sid_dom_lid" ON "mshop_text" ("siteid", "domain", "langid");

CREATE INDEX "idx_msfilt_sid_dom_label" ON "mshop_filter" ("siteid", "domain", "label");

CREATE INDEX "idx_msfilt_sid_dom_pos" ON "mshop_filter" ("siteid", "domain", "pos");

CREATE INDEX "idx_msfilt_sid_dom_ctime" ON "mshop_filter" ("siteid", "domain", "ctime");

CREATE INDEX "idx_msfilt_sid_dom_mtime" ON "mshop_filter" ("siteid", "domain", "mtime");

CREATE INDEX "idx_msfilt_sid_dom_editor" ON "mshop_filter" ("siteid", "domain", "editor");


--
-- Table structure for table `mshop_filter_list_type`
--

CREATE TABLE "mshop_filter_list_type" (
    -- Unique id
    "id" INTEGER NOT NULL AUTO_INCREMENT,
    -- site id, references mshop_locale_site.id
    "siteid" INTEGER NOT NULL,
    -- domain
    "domain" VARCHAR(32) NOT NULL,
    -- code
    "code" VARCHAR(32) NOT NULL COLLATE utf8_bin,
    -- Name of the list type
    "label" VARCHAR(255) NOT NULL,
    -- Status (0=disabled, 1=enabled, >1 for special)
    "status" SMALLINT NOT NULL,
    -- Date of last modification of this database entry
    "mtime" DATETIME NOT NULL,
    -- Date of creation of this database entry
    "ctime" DATETIME NOT NULL,
    -- Editor who modified this entry at last
    "editor" VARCHAR(255) NOT NULL,
CONSTRAINT "pk_msfiltlity_id"
    PRIMARY KEY ("id"),
CONSTRAINT "unq_msfiltlity_sid_dom_code"
    UNIQUE ("siteid", "domain", "code")
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX "idx_msfiltlity_sid_status" ON "mshop_filter_list_type" ("siteid", "status");

CREATE INDEX "idx_msfiltlity_sid_label" ON "mshop_filter_list_type" ("siteid", "label");

CREATE INDEX "idx_msfiltlity_sid_code" ON "mshop_filter_list_type" ("siteid", "code");


--
-- filter list
--

CREATE TABLE "mshop_filter_list" (
    -- Unique list id
    "id" INTEGER NOT NULL AUTO_INCREMENT,
    -- filter id
    "parentid" INTEGER NOT NULL,
    -- site id, references mshop_locale_site.id
    "siteid" INTEGER NOT NULL,
    -- typeid
    "typeid" INTEGER NOT NULL,
    -- Referenced domain
    "domain" VARCHAR(32) NOT NULL,
    -- Featured reference
    "refid" VARCHAR(32) NOT NULL,
    -- Valid from
    "start" DATETIME DEFAULT NULL,
    -- Valid until
    "end" DATETIME DEFAULT NULL,
    -- Configuration
    "config" TEXT NOT NULL,
    -- Position of the list entry
    "pos" INTEGER NOT NULL,
    -- status code (0=hidden, 1=display, >1 for anything special)
    "status" SMALLINT NOT NULL DEFAULT 0,
    -- Date of last modification of this database entry
    "mtime" DATETIME NOT NULL,
    -- Date of creation of this database entry
    "ctime" DATETIME NOT NULL,
    -- Editor who modified this entry at last
    "editor" VARCHAR(255) NOT NULL,
CONSTRAINT "pk_msfiltli_id"
    PRIMARY KEY ("id"),
CONSTRAINT "unq_msfiltli_sid_dm_rid_tid_pid"
    UNIQUE ("siteid", "domain", "refid", "typeid", "parentid"),
CONSTRAINT "fk_msfiltli_parentid"
    FOREIGN KEY ("parentid")
    REFERENCES "mshop_filter" ("id")
    ON UPDATE CASCADE
    ON DELETE CASCADE,
CONSTRAINT "fk_msfiltli_typeid"
    FOREIGN KEY ( "typeid" )
    REFERENCES "mshop_filter_list_type" ("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB CHARACTER SET = utf8;

CREATE INDEX "idx_msfiltli_sid_stat_start_end" ON "mshop_filter_list" ("siteid", "status", "start", "end");

CREATE INDEX "idx_msfiltli_pid_sid_rid_dm_tid" ON "mshop_filter_list" ("parentid", "siteid", "refid", "domain", "typeid");

CREATE INDEX "idx_msfiltli_pid_sid_start" ON "mshop_filter_list" ("parentid", "siteid", "start");

CREATE INDEX "idx_msfiltli_pid_sid_end" ON "mshop_filter_list" ("parentid", "siteid", "end");

CREATE INDEX "idx_msfiltli_pid_sid_pos" ON "mshop_filter_list" ("parentid", "siteid", "pos");

