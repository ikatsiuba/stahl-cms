<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */

return array(
	'item' => array(
		'delete' => '
			DELETE FROM "mshop_filter"
			WHERE :cond AND siteid = ?
		',
		'insert' => '
			INSERT INTO "mshop_filter" (
				"siteid", "typeid", "domain", "code", "label","langid",
	            "attribute","subattribute","webtype","parentid","keys","values","condition","brackets","direction",
				"pos", "status", "mtime", "editor", "ctime"
			) VALUES (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		',
		'update' => '
			UPDATE "mshop_filter"
			SET "siteid" = ?, "typeid" = ?, "domain" = ?, "code" = ?, "label" = ?,
				"langid" = ?,"attribute" = ?,"subattribute" = ?,
	            "webtype" = ?,"parentid" =?,"keys"=?,"values"=?, "condition"=?, "brackets"=?, "direction"=?, "pos" = ?,
	             "status" = ? ,"mtime" = ?, "editor" = ?
			WHERE "id" = ?
		',
		'search' => '
			SELECT DISTINCT mfilt."id", mfilt."siteid", mfilt."typeid",
				mfilt."domain", mfilt."code", mfilt."status", mfilt."pos",
	            mfilt."attribute",mfilt."subattribute",mfilt."webtype",
	            mfilt."parentid",mfilt."keys",mfilt."values",mfilt."condition",mfilt."brackets",mfilt."direction",
	            mfilt."label", mfilt."mtime", mfilt."ctime", mfilt."editor"
			FROM "mshop_filter" AS mfilt
			:joins
			WHERE :cond
			/*-orderby*/ ORDER BY :order /*orderby-*/
			LIMIT :size OFFSET :start
		',
		'count' => '
			SELECT COUNT(*) AS "count"
			FROM (
				SELECT DISTINCT mfilt."id"
				FROM "mshop_filter" AS mfilt
				:joins
				WHERE :cond
				LIMIT 10000 OFFSET 0
			) AS list
		',
	),
);
