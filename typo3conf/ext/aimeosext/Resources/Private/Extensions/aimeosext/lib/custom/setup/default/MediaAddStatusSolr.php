<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */


/**
 * Adds code column to media table.
 */
class MW_Setup_Task_MediaAddStatusSolr extends MW_Setup_Task_Abstract
{
    private $_mysql = array(
        'ALTER TABLE "mshop_media" ADD "status_solr" SMALLINT(6)  NOT NULL DEFAULT \'0\' AFTER "editor"',
        'ALTER TABLE "mshop_media" ADD INDEX "idx_status_status_solr" ("status","status_solr")'
    );


    /**
     * Returns the list of task names which this task depends on.
     *
     * @return array List of task names
     */
    public function getPreDependencies()
    {
        return array('TablesCreateMShop');
    }


    /**
     * Returns the list of task names which depends on this task.
     *
     * @return string[] List of task names
     */
    public function getPostDependencies()
    {
        return array(  );
    }


    /**
     * Executes the task for MySQL databases.
     */
    protected function _mysql()
    {
        $this->_process( $this->_mysql );
    }


    /**
     * Add column to table if the column doesn't exist.
     *
     * @param array $stmts List of SQL statements to execute for adding columns
     */
    protected function _process( array $stmts )
    {
        $this->_msg( 'Add column "status_solr"', 1 );

        if( $this->_schema->tableExists( 'mshop_media' ) === true
            && $this->_schema->columnExists( 'mshop_media', 'status_solr' ) === false
            && $this->_schema->indexExists( 'mshop_media', 'idx_status_status_solr' ) === false
        ){
            $this->_executeList( $stmts );
            $this->_status( 'added' );
        }
        else
        {
            $this->_status( 'OK' );
        }
    }
}