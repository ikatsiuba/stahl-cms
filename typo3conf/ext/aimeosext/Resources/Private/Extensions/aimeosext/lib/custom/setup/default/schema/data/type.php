<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */

return array(
	'attributegroup/list/type' => array(
		array( 'domain' => 'attribute', 'code' => 'default', 'label' => 'Default', 'status' => 1 ),
		array( 'domain' => 'catalog', 'code' => 'default', 'label' => 'Default', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'default', 'label' => 'Default', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'icon', 'label' => 'Icon', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'stage', 'label' => 'Stage', 'status' => 1 ),
		array( 'domain' => 'product', 'code' => 'default', 'label' => 'Default', 'status' => 1 ),
		array( 'domain' => 'text', 'code' => 'default', 'label' => 'Default', 'status' => 1 ),
		array( 'domain' => 'attribute/type', 'code' => 'default', 'label' => 'Default', 'status' => 1 ),
		array( 'domain' => 'text/type', 'code' => 'default', 'label' => 'Default', 'status' => 1 ),
	),
   'text/type' => array(
        array( 'domain' => 'attributegroup', 'code' => 'DisplayName', 'label' => 'DisplayName', 'status' => 1 ),
        array( 'domain' => 'attributegroup', 'code' => 'Purpose', 'label' => 'Purpose', 'status' => 1 ),
        array( 'domain' => 'attribute', 'code' => 'DisplayName', 'label' => 'DisplayName', 'status' => 1 ),
        array( 'domain' => 'text', 'code' => 'DisplayName', 'label' => 'DisplayName', 'status' => 1 )
   ),
   'attribute/type' => array(
        array( 'domain' => 'attributegroup', 'code' => 'DisplaySequence', 'label' => 'DisplaySequence', 'status' => 1 )
   )
);