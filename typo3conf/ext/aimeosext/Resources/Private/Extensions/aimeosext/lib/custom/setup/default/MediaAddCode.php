<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */


/**
 * Adds code column to media table.
 */
class MW_Setup_Task_MediaAddCode extends MW_Setup_Task_Abstract
{
    private $_mysql = array(
        'ALTER TABLE "mshop_media" ADD "code" VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL AFTER "domain"',
        'ALTER TABLE "mshop_media" ADD INDEX "idx_msmed_sid_dom_code" ("siteid","domain","code")'
    );


    /**
     * Returns the list of task names which this task depends on.
     *
     * @return array List of task names
     */
    public function getPreDependencies()
    {
        return array();
    }


    /**
     * Returns the list of task names which depends on this task.
     *
     * @return string[] List of task names
     */
    public function getPostDependencies()
    {
        return array( 'TablesCreateMShop' );
    }


    /**
     * Executes the task for MySQL databases.
     */
    protected function _mysql()
    {
        $this->_msg( 'Adding code column to media table', 0 ); $this->_status( '' );

        $this->_process( $this->_mysql );
    }


    /**
     * Add column to table if the column doesn't exist.
     *
     * @param array $stmts List of SQL statements to execute for adding columns
     */
    protected function _process( array $stmts )
    {
        $this->_msg( sprintf( 'Checking column "%1$s": ', 'code' ), 1 );

        if( $this->_schema->tableExists( 'mshop_media' ) === true
            && $this->_schema->columnExists( 'mshop_media', 'code' ) === false )
        {
            $this->_executeList( $stmts );
            $this->_status( 'added' );
        }
        else
        {
            $this->_status( 'OK' );
        }
    }
}