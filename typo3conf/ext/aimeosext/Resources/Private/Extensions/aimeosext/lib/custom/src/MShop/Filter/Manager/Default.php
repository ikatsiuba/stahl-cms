<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Filter
 */


/**
 * Default attribute manager for creating and handling attributes.
 * @package MShop
 * @subpackage Filter
 */
class MShop_Filter_Manager_Default
    extends MShop_Common_Manager_ListRef_Abstract
    implements MShop_Filter_Manager_Interface
{
    private $_searchConfig = array(
        'filter.id'=> array(
            'code'=>'filter.id',
            'internalcode'=>'mfilt."id"',
            'label'=>'filter ID',
            'type'=> 'integer',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
        ),
        'filter.siteid'=> array(
            'code'=>'filter.siteid',
            'internalcode'=>'mfilt."siteid"',
            'label'=>'filter site',
            'type'=> 'integer',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
            'public' => false,
        ),
        'filter.typeid'=> array(
            'code'=>'filter.typeid',
            'internalcode'=>'mfilt."typeid"',
            'label'=>'filter type',
            'type'=> 'integer',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
            'public' => false,
        ),
        'filter.domain'=> array(
            'code'=>'filter.domain',
            'internalcode'=>'mfilt."domain"',
            'label'=>'filter domain',
            'type'=> 'string',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
        ),
        'filter.code'=> array(
            'code'=>'filter.code',
            'internalcode'=>'mfilt."code"',
            'label'=>'filter code',
            'type'=> 'string',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
        ),
        'filter.position'=> array(
            'code'=>'filter.position',
            'internalcode'=>'mfilt."pos"',
            'label'=>'filter position',
            'type'=> 'integer',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
        ),
        'filter.label'=> array(
            'code'=>'filter.label',
            'internalcode'=>'mfilt."label"',
            'label'=>'filter label',
            'type'=> 'string',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.languageid' => array(
             'code'=>'filter.languageid',
             'internalcode'=>'mfilt."langid"',
             'label'=>'Filter language code',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.attribute' => array(
             'code'=>'filter.attribute',
             'internalcode'=>'mfilt."attribute"',
             'label'=>'Attribute code',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.subattribute' => array(
             'code'=>'filter.subattribute',
             'internalcode'=>'mfilt."subattribute"',
             'label'=>'SubAttribute code',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.webtype' => array(
             'code'=>'filter.webtype',
             'internalcode'=>'mfilt."webtype"',
             'label'=>'web type',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.parentid' => array(
             'code'=>'filter.parentid',
             'internalcode'=>'mfilt."parentid"',
             'label'=>'parent id',
             'type'=> 'integer',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.keys' => array(
             'code'=>'filter.keys',
             'internalcode'=>'mfilt."keys"',
             'label'=>'filter semicolon keys',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.values' => array(
             'code'=>'filter.values',
             'internalcode'=>'mfilt."values"',
             'label'=>'filter semicolon values',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.condition' => array(
             'code'=>'filter.condition',
             'internalcode'=>'mfilt."condition"',
             'label'=>'condition type',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
       'filter.brackets' => array(
             'code'=>'filter.brackets',
             'internalcode'=>'mfilt."brackets"',
             'label'=>'condition type',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
        'filter.direction' => array(
             'code'=>'filter.direction',
             'internalcode'=>'mfilt."direction"',
             'label'=>'sort direction desc/asc',
             'type'=> 'string',
             'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
        ),
        'filter.status'=> array(
            'code'=>'filter.status',
            'internalcode'=>'mfilt."status"',
            'label'=>'filter status',
            'type'=> 'integer',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
        ),
        'filter.ctime'=> array(
            'code'=>'filter.ctime',
            'internalcode'=>'mfilt."ctime"',
            'label'=>'filter create date/time',
            'type'=> 'datetime',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
        ),
        'filter.mtime'=> array(
            'code'=>'filter.mtime',
            'internalcode'=>'mfilt."mtime"',
            'label'=>'filter modification date/time',
            'type'=> 'datetime',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
        ),
        'filter.editor'=> array(
            'code'=>'filter.editor',
            'internalcode'=>'mfilt."editor"',
            'label'=>'filter editor',
            'type'=> 'string',
            'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
        ),
    );


    /**
     * Initializes the object.
     *
     * @param MShop_Context_Item_Interface $context Context object
     */
    public function __construct( MShop_Context_Item_Interface $context )
    {
        parent::__construct( $context );
        $this->_setResourceName( 'db-filter' );
    }


    /**
     * Removes old entries from the storage.
     *
     * @param integer[] $siteids List of IDs for sites whose entries should be deleted
     */
    public function cleanup( array $siteids )
    {
        $path = 'classes/filter/manager/submanagers';
        foreach( $this->_getContext()->getConfig()->get( $path, array( 'type') ) as $domain ) {
            $this->getSubManager( $domain )->cleanup( $siteids );
        }

        $this->_cleanup( $siteids, 'mshop/filter/manager/default/item/delete' );
    }


    /**
     * Returns the filters that can be used for searching.
     *
     * @param boolean $withsub Return also filters of sub-managers if true
     * @return array List of filter items implementing MW_Common_Criteria_Filter_Interface
     */
    public function getSearchAttributes( $withsub = true )
    {
        /** classes/filter/manager/submanagers
         * List of manager names that can be instantiated by the filter manager
         *
         * Managers provide a generic interface to the underlying storage.
         * Each manager has or can have sub-managers caring about particular
         * aspects. Each of these sub-managers can be instantiated by its
         * parent manager using the getSubManager() method.
         *
         * The search keys from sub-managers can be normally used in the
         * manager as well. It allows you to search for items of the manager
         * using the search keys of the sub-managers to further limit the
         * retrieved list of items.
         *
         * @param array List of sub-manager names
         * @since 2014.03
         * @category Developer
         */
        $path = 'classes/filter/manager/submanagers';

        return $this->_getSearchAttributes( $this->_searchConfig, $path, array( 'type'), $withsub );
    }


    /**
     * Creates a new empty filter item instance.
     *
     * @return MShop_Filter_Item_Interface Creates a blank filter item
     */
    public function createItem()
    {
        $values = array( 'siteid' => $this->_getContext()->getLocale()->getSiteId() );
        return $this->_createItem( $values );
    }


    /**
     * Returns the filters item specified by its ID.
     *
     * @param integer $id Unique ID of the filter item in the storage
     * @param string[] $ref List of domains to fetch list items and referenced items for
     * @return MShop_Filter_Item_Interface Returns the filter item of the given id
     * @throws MShop_Exception If item couldn't be found
     */
    public function getItem( $id, array $ref = array() )
    {
        return $this->_getItem( 'filter.id', $id, $ref );
    }


    /**
     * Saves an filter item to the storage.
     *
     * @param MShop_Common_Item_Interface $item filter item
     * @param boolean $fetch True if the new ID should be returned in the item
     * @throws MShop_Flter_Exception If Afiltercouldn't be saved
     */
    public function saveItem( MShop_Common_Item_Interface $item, $fetch = true )
    {
        $iface = 'MShop_Filter_Item_Interface';
        if( !( $item instanceof $iface ) ) {
            throw new MShop_filterception( sprintf( 'Object is not of required type "%1$s"', $iface ) );
        }

        if( $item->isModified() === false ) { return; }

        $context = $this->_getContext();

        $dbm = $context->getDatabaseManager();
        $dbname = $this->_getResourceName();
        $conn = $dbm->acquire( $dbname );

        try
        {
            $id = $item->getId();
            $date = date( 'Y-m-d H:i:s' );

            if( $id === null )
            {
                $path = 'mshop/filter/manager/default/item/insert';
            }
            else
            {
                $path = 'mshop/filter/manager/default/item/update';
            }

            $stmt = $this->_getCachedStatement( $conn, $path );
            $stmt->bind( 1, $context->getLocale()->getSiteId() );
            $stmt->bind( 2, $item->getTypeId() );
            $stmt->bind( 3, $item->getDomain() );
            $stmt->bind( 4, $item->getCode() );
            $stmt->bind( 5, $item->getLabel() );
            $stmt->bind( 6, $item->getLanguageId() );
            $stmt->bind( 7, $item->getAttribute() );
            $stmt->bind( 8, $item->getSubAttribute() );
            $stmt->bind( 9, $item->getWebType() );
            $stmt->bind( 10, $item->getParentId() );
            $stmt->bind( 11, $item->getKeys() );
            $stmt->bind( 12, $item->getValues() );
            $stmt->bind( 13, $item->getCondition() );
            $stmt->bind( 14, $item->getBrackets() );
            $stmt->bind( 15, $item->getDirection() );
            $stmt->bind( 16, $item->getPosition(), MW_DB_Statement_Abstract::PARAM_INT );
            $stmt->bind( 17, $item->getStatus(), MW_DB_Statement_Abstract::PARAM_INT );
            $stmt->bind( 18, $date );
            $stmt->bind( 19, $context->getEditor() );

            if( $id !== null ) {
                $stmt->bind( 20, $id, MW_DB_Statement_Abstract::PARAM_INT );
                $item->setId( $id );
            } else {
                $stmt->bind( 20, $date );
            }

            $stmt->execute()->finish();

            if( $id === null && $fetch === true )
            {
                $path = 'mshop/filter/manager/default/item/newid';
                $item->setId( $this->_newId( $conn, $context->getConfig()->get( $path, $path ) ) );
            }

            $dbm->release( $conn, $dbname );
        }
        catch( Exception $e )
        {
            $dbm->release( $conn, $dbname );
            throw $e;
        }
    }


    /**
     * Removes multiple items specified by ids in the array.
     *
     * @param array $ids List of IDs
     */
    public function deleteItems( array $ids )
    {
        $path = 'mshop/filter/manager/default/item/delete';
        $this->_deleteItems( $ids, $this->_getContext()->getConfig()->get( $path, $path ) );
    }


    /**
     * Searches for filter items based on the given criteria.
     *
     * @param MW_Common_Criteria_Interface $search Search object containing the conditions
     * @param array $ref List of domains to fetch list items and referenced items for
     * @param integer &$total Number of items that are available in total
     * @return array List of filter items implementing MShop_filter_Item_Interface
     *
     * @throws MW_DB_Exception On failures with the db object
     * @throws MShop_Common_Exception On failures with the MW_Common_Criteria_ object
     * @throws MShop_Filter_Exception On failures with the filter items
     */
    public function searchItems( MW_Common_Criteria_Interface $search, array $ref = array(), &$total = null )
    {
        $map = $typeIds = array();
        $context = $this->_getContext();

        $dbm = $context->getDatabaseManager();
        $dbname = $this->_getResourceName();
        $conn = $dbm->acquire( $dbname );

        try
        {
            $required = array( 'filter' );
            $level = MShop_Locale_Manager_Abstract::SITE_ALL;
            $cfgPathSearch = 'mshop/filter/manager/default/item/search';
            $cfgPathCount = 'mshop/filter/manager/default/item/count';

            $results = $this->_searchItems( $conn, $search, $cfgPathSearch, $cfgPathCount, $required, $total, $level );

            while( ( $row = $results->fetch() ) !== false )
            {
                $map[$row['id']] = $row;
                $typeIds[$row['typeid']] = null;
            }

            $dbm->release( $conn, $dbname );
        }
        catch( Exception $e )
        {
            $dbm->release( $conn, $dbname );
            throw $e;
        }

        if( !empty( $typeIds ) )
        {
            $typeManager = $this->getSubManager( 'type' );
            $typeSearch = $typeManager->createSearch();
            $typeSearch->setConditions( $typeSearch->compare( '==', 'filter.type.id', array_keys( $typeIds ) ) );
            $typeSearch->setSlice( 0, $search->getSliceSize() );
            $typeItems = $typeManager->searchItems( $typeSearch );

            foreach( $map as $id => $row )
            {
                if( isset( $typeItems[$row['typeid']] ) ) {
                    $map[$id]['type'] = $typeItems[$row['typeid']]->getCode();
                }
            }
        }

        return $this->_buildItems( $map, $ref, 'filter' );
    }


    /**
     * creates a search object and sets base criteria
     *
     * @param boolean $default
     * @return MW_Common_Criteria_Interface
     */
    public function createSearch( $default = false )
    {
        if( $default === true ) {
            return $this->_createSearch( 'filter' );
        }

        return parent::createSearch();
    }


    /**
     * Returns a new manager for filter extensions
     *
     * @param string $manager Name of the sub manager type in lower case
     * @param string|null $name Name of the implementation, will be from configuration (or Default) if null
     * @return MShop_Common_Manager_Interface Manager for different extensions, e.g Type, List's etc.
     */
    public function getSubManager( $manager, $name = null )
    {
        return $this->_getSubManager( 'filter', $manager, $name );
    }


    /**
     * Creates a new filter item instance.
     *
     * @param array $values Associative list of key/value pairs
     * @param array $listItems List of items implementing MShop_Common_Item_List_Interface
     * @param array $refItems List of items implementing MShop_Text_Item_Interface
     * @return MShop_Filter_Item_Interface New product item
     */
    protected function _createItem( array $values = array(), array $listItems = array(), array $refItems = array() )
    {
        return new MShop_Filter_Item_Default( $values, $listItems, $refItems );
    }
}
