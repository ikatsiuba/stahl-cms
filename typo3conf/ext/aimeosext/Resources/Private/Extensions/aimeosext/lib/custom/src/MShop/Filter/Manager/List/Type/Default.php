<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2013
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Filter
 */


/**
 * Default Filter list type manager for creating and handling attribute list type items.
 * @package MShop
 * @subpackage Filter
 */
class MShop_Filter_Manager_List_Type_Default
	extends MShop_Common_Manager_Type_Abstract
	implements MShop_Filter_Manager_List_Type_Interface
{
	private $_searchConfig = array(
		'filter.list.type.id' => array(
			'code'=>'filter.list.type.id',
			'internalcode'=>'mfiltlity."id"',
			'internaldeps' => array( 'LEFT JOIN "mshop_filter_list_type" AS mfiltlity ON ( mattli."typeid" = mfiltlity."id" )' ),
			'label'=>'filter list type Id',
			'type'=> 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'filter.list.type.siteid' => array(
			'code'=>'filter.list.type.siteid',
			'internalcode'=>'mfiltlity."siteid"',
			'label'=>'filter list type site Id',
			'type'=> 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'filter.list.type.code' => array(
			'code'=>'filter.list.type.code',
			'internalcode'=>'mfiltlity."code"',
			'label'=>'filter list type code',
			'type'=> 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.type.domain' => array(
			'code'=>'filter.list.type.domain',
			'internalcode'=>'mfiltlity."domain"',
			'label'=>'filter list type domain',
			'type'=> 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.type.label' => array(
			'code' => 'filter.list.type.label',
			'internalcode' => 'mfiltlity."label"',
			'label' => 'filter list type label',
			'type' => 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.type.status' => array(
			'code' => 'filter.list.type.status',
			'internalcode' => 'mfiltlity."status"',
			'label' => 'filter list type status',
			'type' => 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
		),
		'filter.list.type.ctime'=> array(
			'code'=>'filter.list.type.ctime',
			'internalcode'=>'mfiltlity."ctime"',
			'label'=>'filter list type create date/time',
			'type'=> 'datetime',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.type.mtime'=> array(
			'code'=>'filter.list.type.mtime',
			'internalcode'=>'mfiltlity."mtime"',
			'label'=>'filter list type modification date/time',
			'type'=> 'datetime',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.type.editor'=> array(
			'code'=>'filter.list.type.editor',
			'internalcode'=>'mfiltlity."editor"',
			'label'=>'filter list type editor',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
	);


	/**
	 * Initializes the object.
	 *
	 * @param MShop_Context_Item_Interface $context Context object
	 */
	public function __construct( MShop_Context_Item_Interface $context )
	{
		parent::__construct( $context );
		$this->_setResourceName( 'db-filter' );
	}


	/**
	 * Removes old entries from the storage.
	 *
	 * @param array $siteids List of IDs for sites whose entries should be deleted
	 */
	public function cleanup( array $siteids )
	{
		$path = 'classes/filter/manager/list/type/submanagers';
		foreach( $this->_getContext()->getConfig()->get( $path, array() ) as $domain ) {
			$this->getSubManager( $domain )->cleanup( $siteids );
		}

		$this->_cleanup( $siteids, 'mshop/filter/manager/list/type/default/item/delete' );
	}


	/**
	 * Returns the filters that can be used for searching.
	 *
	 * @param boolean $withsub Return also filters of sub-managers if true
	 * @return array List of filter items implementing MW_Common_Criteria_Attribute_Interface
	 */
	public function getSearchAttributes( $withsub = true )
	{

		$path = 'classes/filter/manager/list/type/submanagers';

		return $this->_getSearchAttributes( $this->_searchConfig, $path, array(), $withsub );
	}


	/**
	 * Returns a new manager for filter list type extensions.
	 *
	 * @param string $manager Name of the sub manager type in lower case
	 * @param string|null $name Name of the implementation, will be from configuration (or Default) if null
	 * @return MShop_Common_Manager_Interface Manager for different extensions, e.g types, lists etc.
	 */
	public function getSubManager( $manager, $name = null )
	{

		return $this->_getSubManager( 'filter', 'list/type/' . $manager, $name );
	}


	/**
	 * Returns the config path for retrieving the configuration values.
	 *
	 * @return string Configuration path
	 */
	protected function _getConfigPath()
	{

		return 'mshop/filter/manager/list/type/default/item/';
	}


	/**
	 * Returns the search configuration for searching items.
	 *
	 * @return array Associative list of search keys and search definitions
	 */
	protected function _getSearchConfig()
	{
		return $this->_searchConfig;
	}
}