<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */


/**
 * Update product/category status.
 */
class MW_Setup_Task_UpdateAtWebDisplay extends MW_Setup_Task_Abstract
{
	private $_mysql = array(
		"update mshop_product as e
          join mshop_product_list as pr_ on pr_.parentid=e.id
          join mshop_text te_ on te_.id=pr_.refid
          set e.status=0
          where te_.label='AT_WebDisplay-2' and te_.domain='product' and pr_.domain='text'",
	    "update mshop_catalog as e
          join mshop_catalog_list as pr_ on pr_.parentid=e.id
          join mshop_text te_ on te_.id=pr_.refid
          set e.status=0
          where te_.label='AT_WebDisplay-2' and te_.domain='catalog' and pr_.domain='text'"
	);

	/**
	 * Returns the list of task names which this task depends on.
	 *
	 * @return array List of task names
	 */
	public function getPreDependencies()
	{
		return array('TablesCreateMShop');
	}


	/**
	 * Returns the list of task names which depends on this task.
	 *
	 * @return string[] List of task names
	 */
	public function getPostDependencies()
	{
		return array();
	}


	/**
	 * Executes the task for MySQL databases.
	 */
	protected function _mysql()
	{
		$this->_process( $this->_mysql );
	}


	/**
	 * Migrates service text data to list table.
	 *
	 * @param array $stmts Associative array of tables names and lists of SQL statements to execute.
	 */
	protected function _process( array $stmts )
	{
		$this->_msg( 'Updating AT_WebDisplay', 0 );

		if( $this->_schema->tableExists( 'mshop_product' ) === true
			&& $this->_schema->tableExists( 'mshop_catalog' ) === true)
		{
			$this->_executeList( $stmts );
			$this->_status( 'updated' );
		}
		else
		{
			$this->_status( 'OK' );
		}
	}

}
