--
-- Category database definitions
--
-- Copyright (c) Metaways Infosystems GmbH, 2014
-- License LGPLv3, http://opensource.org/licenses/LGPL-3.0
--


SET SESSION sql_mode='ANSI';


CREATE TABLE "mshop_catalog_index_product" (
	-- Product id
	"prodid" INTEGER NOT NULL,
	-- site id, references mshop_locale_site.id
	"siteid" INTEGER NOT NULL,
	-- catalog node
	"pssid" INTEGER NOT NULL,
	-- catalog list type
	"listtype" VARCHAR(32) NOT NULL,
	-- product position
	"pos" INTEGER NOT NULL,
	-- Date of last modification of this database entry
	"mtime" DATETIME NOT NULL,
	-- Date of creation of this database entry
	"ctime" DATETIME NOT NULL,
	-- Editor who modified this entry at last
	"editor" VARCHAR(255) NOT NULL,
CONSTRAINT "unq_mscatinpr_p_s_cid_lt_po"
	UNIQUE ("prodid", "siteid", "pssid", "listtype", "pos")
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX "idx_mscatinpr_s_ca_lt_po" ON "mshop_catalog_index_product" ("siteid", "pssid", "listtype", "pos");