<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */


/**
 * Adds unit column to attribute_type table.
 */
class MW_Setup_Task_AttributeTextAddUnit extends MW_Setup_Task_Abstract
{
    private $_mysql = array(
        'ALTER TABLE "mshop_attribute" ADD "unitvalue" VARCHAR(255) AFTER "status"',
        'ALTER TABLE "mshop_text" ADD "unitvalue" VARCHAR(255) AFTER "status"',
    );


    /**
     * Returns the list of task names which this task depends on.
     *
     * @return array List of task names
     */
    public function getPreDependencies()
    {
        return array( 'TablesCreateMShop' );
    }


    /**
     * Returns the list of task names which depends on this task.
     *
     * @return string[] List of task names
     */
    public function getPostDependencies()
    {
        return array();
    }


    /**
     * Executes the task for MySQL databases.
     */
    protected function _mysql()
    {
        $this->_msg( 'Adding unit column to attribute/text table', 0 ); $this->_status( '' );

        $this->_process( $this->_mysql );
    }


    /**
     * Add column to table if the column doesn't exist.
     *
     * @param array $stmts List of SQL statements to execute for adding columns
     */
    protected function _process( array $stmts )
    {
        $this->_msg( sprintf( 'Checking column "%1$s": ', 'unit' ), 1 );

        if( $this->_schema->tableExists( 'mshop_attribute' ) === true
            && $this->_schema->columnExists( 'mshop_attribute', 'unitvalue' ) === false
            && $this->_schema->tableExists( 'mshop_text' ) === true
            && $this->_schema->columnExists( 'mshop_text', 'unitvalue' ) === false
        ){
            $this->_executeList( $stmts );
            $this->_status( 'added' );
        }
        else
        {
            $this->_status( 'OK' );
        }
    }
}