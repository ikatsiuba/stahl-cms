<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Attributegroup
 */


/**
 * Interface for catalog list manager.
 *
 * @package MShop
 * @subpackage Attributegroup
 */
interface MShop_Attributegroup_Manager_List_Interface
	extends MShop_Common_Manager_Factory_Interface
{
}
