<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */


/**
 * Creates all required tables.
 */
class MW_Setup_Task_TablesCreateAttributeGroup extends MW_Setup_Task_Abstract
{
	/**
	 * Returns the list of task names which this task depends on.
	 *
	 * @return array List of task names
	 */
	public function getPreDependencies()
	{
		return array('TablesCreateMShop');
	}


	/**
	 * Returns the list of task names which depends on this task.
	 *
	 * @return array List of task names
	 */
	public function getPostDependencies()
	{
		return array();
	}


	/**
	 * Executes the task for MySQL databases.
	 */
	protected function _mysql()
	{
		$this->_msg( 'Creating attributegroup tables', 0 );
		$this->_status( '' );

		$ds = DIRECTORY_SEPARATOR;

		$files = array(
			'db-attributegroup' => realpath( __DIR__ ) . $ds .  'schema' . $ds . 'mysql' . $ds . 'attributegroup.sql',
		);

		$this->_setup( $files );
	}


	/**
	 * Creates all required tables if they don't exist
	 */
	protected function _setup( array $files )
	{
		foreach( $files as $rname => $filepath )
		{
			$this->_msg( 'Using tables from ' . basename( $filepath ), 1 ); $this->_status( '' );

			if( ( $content = file_get_contents( $filepath ) ) === false ) {
				throw new MW_Setup_Exception( sprintf( 'Unable to get content from file "%1$s"', $filepath ) );
			}

			$schema = $this->_getSchema( $rname );

			foreach( $this->_getTableDefinitions( $content ) as $name => $sql )
			{
				$this->_msg( sprintf( 'Checking table "%1$s": ', $name ), 2 );

				if( $schema->tableExists( $name ) !== true ) {
					$this->_execute( $sql, $rname );
					$this->_status( 'created' );
				} else {
					$this->_status( 'OK' );
				}
			}

			foreach( $this->_getIndexDefinitions( $content ) as $name => $sql )
			{
				$parts = explode( '.', $name );
				$this->_msg( sprintf( 'Checking index "%1$s": ', $name ), 2 );

				if( $schema->indexExists( $parts[0], $parts[1] ) !== true ) {
					$this->_execute( $sql, $rname );
					$this->_status( 'created' );
				} else {
					$this->_status( 'OK' );
				}
			}
		}
	}
}
