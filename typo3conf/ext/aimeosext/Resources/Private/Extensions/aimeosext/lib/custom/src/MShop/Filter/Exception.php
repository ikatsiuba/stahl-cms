<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @package MShop
 * @subpackage Filter
 */


/**
 * Exception thrown by product classes.
 *
 * @package MShop
 * @subpackage Filter
 */
class MShop_Filter_Exception extends MShop_Exception
{
}
