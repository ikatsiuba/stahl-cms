<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */

return array(
	'product/list/type' => array(
		array( 'domain' => 'media', 'code' => 'RF_LightDistributionCurveImage', 'label' => 'RF_LightDistributionCurveImage', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_BA_SAP', 'label' => 'RF_BA_SAP', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_TechnicalDrawing', 'label' => 'RF_TechnicalDrawing', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_CertificateSAP', 'label' => 'RF_CertificateSAP', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_DimensionDrawing', 'label' => 'RF_DimensionDrawing', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_PrimaryImage', 'label' => 'RF_PrimaryImage', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_AdditionalImage', 'label' => 'RF_AdditionalImage', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_ProductLogo', 'label' => 'RF_ProductLogo', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_IconsCalc', 'label' => 'RF_IconsCalc', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_Icons', 'label' => 'RF_Icons', 'status' => 1 ),
		array( 'domain' => 'media', 'code' => 'RF_Galerie', 'label' => 'RF_Galerie', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_AccessoiresImage', 'label' => 'RF_AccessoiresImage', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_EGK', 'label' => 'RF_EGK', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_DataSheet', 'label' => 'RF_DataSheet', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_DataSheetGenerated', 'label' => 'RF_DataSheetGenerated', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_Handbook', 'label' => 'RF_Handbook', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_ShortInstruction', 'label' => 'RF_ShortInstruction', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_InstallationManual', 'label' => 'RF_InstallationManual', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_TenderSpecification', 'label' => 'RF_TenderSpecification', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'AT_3DLink', 'label' => 'AT_3DLink', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_3DPreview', 'label' => 'RF_3DPreview', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'AT_Youtube', 'label' => 'AT_Youtube', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_LightDistributionCurve', 'label' => 'RF_LightDistributionCurve', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_3D-Data', 'label' => 'RF_3D-Data', 'status' => 1 ),
	    array( 'domain' => 'media', 'code' => 'RF_TableImages', 'label' => 'RF_TableImages', 'status' => 1 ),
	    array( 'domain' => 'product', 'code' => 'RF_MandatoryAccessories', 'label' => 'RF_MandatoryAccessories', 'status' => 1 ),
	    array( 'domain' => 'product', 'code' => 'RF_OptionalAccessories', 'label' => 'RF_OptionalAccessories', 'status' => 1 ),
	    array( 'domain' => 'product', 'code' => 'RF_SimilarProducts', 'label' => 'RF_SimilarProducts', 'status' => 1 ),
	    array( 'domain' => 'product', 'code' => 'RF_SuccessorMaterialSAP', 'label' => 'RF_SuccessorMaterialSAP', 'status' => 1 ),
	),
    'catalog/list/type' => array(
        array( 'domain' => 'media', 'code' => 'RF_Broschures', 'label' => 'RF_Broschures', 'status' => 1 ),
        array( 'domain' => 'media', 'code' => 'RF_ClassificationImage', 'label' => 'RF_ClassificationImage', 'status' => 1 ),
        array( 'domain' => 'media', 'code' => 'RF_Galerie', 'label' => 'RF_Galerie', 'status' => 1 )
    )
);