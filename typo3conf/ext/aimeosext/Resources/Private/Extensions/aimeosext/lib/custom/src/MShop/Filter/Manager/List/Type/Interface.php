<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2013
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Filter
 */


/**
 * Interface for Filter list type manager.
 *
 * @package MShop
 * @subpackage Filter
 */
interface MShop_Filter_Manager_List_Type_Interface
	extends MShop_Common_Manager_Factory_Interface
{
}
