<?php

class MShop_Product_Item_DefaultExt
    extends MShop_Product_Item_Default
{
    use MShop_Common_Item_ListRef_AbstractTrait;

    private $_catalog_list,$_context;

    public function __construct( array $values = array(), array $listItems = array(), array $refItems = array(), $context = null)
    {
        $this->_context = $context;
        parent::__construct( $values, $listItems, $refItems );
    }

    public function getCatalogListFirst(){
        return reset($this->getCatalogList());
    }
    public function getCatalogList(){
        if(is_null($this->_catalog_list)) {
            $_categories = $this->getParentItems('catalog');
            $this->_catalog_list = $_categories ? $_categories : [];
        }
        return $this->_catalog_list;
    }
    public function getParentItems($type)
    {
        $typeItem = $this->_getTypeItem( $type.'/list/type', 'product', 'default' );
        $manager = $this->_getManager( $type.'/list' );
        $search = $manager->createSearch( true );
        $expr = array(
            $search->compare( '==', $type.'.list.refid', $this->getId() ),
            $search->compare( '==', $type.'.list.typeid', $typeItem->getId() ),
            $search->compare( '==', $type.'.list.domain', 'product' ),
            $search->getConditions(),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSortations( array( $search->sort( '+', $type.'.list.position' ) ) );
        return $manager->searchItems( $search );
    }

    protected function _getTypeItem( $prefix, $domain, $code )
    {
        $manager = $this->_getManager( $prefix );
        $prefix = str_replace( '/', '.', $prefix );
        $search = $manager->createSearch();
        $expr = array(
            $search->compare( '==', $prefix . '.domain', $domain ),
            $search->compare( '==', $prefix . '.code', $code ),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $result = $manager->searchItems( $search );
        if( ( $item = reset( $result ) ) === false )
        {
            $msg = sprintf( 'No type item for "%1$s/%2$s" in "%3$s" found', $domain, $code, $prefix );
            throw new \Controller_Jobs_Exception( $msg );
        }
        return $item;
    }

    protected function _getManager($type)
    {
        return \MShop_Factory::createManager( $this->_context, $type );
    }
    
    public function getStockInfo()
    {
        $stockManager = $this->_getManager('product/stock' );
        $siteConfig = $this->_context->getLocale()->getSite()->getConfig();
        
        $search = $stockManager->createSearch( true );
        $expr = array( $search->compare( '==', 'product.stock.productid', $this->getId() ) );
        
        if( isset( $siteConfig['warehouse'] ) ) {
            $expr[] = $search->compare( '==', 'product.stock.warehouse.code', $siteConfig['warehouse'] );
        }
        
        $expr[] = $search->getConditions();
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSlice( 0, 0x7fffffff );
        
        return reset($stockManager->searchItems( $search ));
    }
    
    public function getSubSeriesName($langCode,$subProduct=null)
    {
        $dName = reset($this->getRefItems('text', 'TitleProductSubSeries',null,$langCode));
        $result = $this->getName();
        if(!is_null($subProduct)){
            $result = $subProduct->getName();
        }
        if($dName && !empty($dName->getContent())){
            $result = $dName->getContent();
        }
        return $result;
    }
    
    public function getSubSeriesNameCalc($langCode, $subProduct=null)
    {
        $dName = reset($this->getRefItems('text', 'TitleProductSubSeries',null,$langCode));
        $dNameCalc = reset($subProduct->getRefItems('text', 'TitleProductSubSeriesCalc',null,$langCode));
        $result = $this->getName();

        if($dName && !empty($dName->getContent())){
            $result = $dName->getContent();
        }
        if($dNameCalc && !empty($dNameCalc->getContent())){
            $result = $dNameCalc->getContent();
        }
        return $result;
    }
    
    public function getMaterialName($langCode)
    {
        $dName = reset($this->getRefItems('text', 'TitleProductSubSeries',null,$langCode));
        $aMaterialId = reset($this->getRefItems('attribute', 'AT_SAP_MainMaterialID'));
        
        $result = $this->getName();
        if($dName && !empty($dName->getContent())){
            $result = $dName->getContent();
        }
        if($aMaterialId && !empty($aMaterialId->getName())){
            $result = $aMaterialId->getName();
        }
        return $result;
    }

}

