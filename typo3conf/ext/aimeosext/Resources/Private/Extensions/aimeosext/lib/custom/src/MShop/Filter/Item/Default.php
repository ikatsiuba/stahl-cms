<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Filter
 */


/**
 * Default filter item implementation.
 *
 * @package MShop
 * @subpackage Filter
 */
class MShop_Filter_Item_Default
	extends MShop_Common_Item_ListRef_Abstract
	implements MShop_Filter_Item_Interface
{
	private $_values;


	/**
	 * Initializes the attribute item.
	 *
	 * @param array $values Associative array with id, domain, code, and status to initialize the item properties; Optional
	 * @param MShop_Common_List_Item_Interface[] $listItems List of list items
	 * @param MShop_Common_Item_Interface[] $refItems List of referenced items
	 */
	public function __construct( array $values = array(), array $listItems = array(), array $refItems = array() )
	{
		parent::__construct( 'filter.', $values, $listItems, $refItems );

		$this->_values = $values;
	}


	/**
	 * Returns the domain of the attribute item.
	 *
	 * @return string Returns the domain for this item e.g. text, media, price...
	 */
	public function getDomain()
	{
		return ( isset( $this->_values['domain'] ) ? (string) $this->_values['domain'] : '' );
	}


	/**
	 * Set the name of the domain for this attribute item.
	 *
	 * @param string $domain Name of the domain e.g. text, media, price...
	 */
	public function setDomain( $domain )
	{
		if( $domain == $this->getDomain() ) { return; }

		$this->_values['domain'] = (string) $domain;
		$this->setModified();
	}


	/**
	 * Returns the type id of the attribute.
	 *
	 * @return integer|null Type of the attribute
	 */
	public function getTypeId()
	{
		return ( isset( $this->_values['typeid'] ) ? (int) $this->_values['typeid'] : null );
	}


	/**
	 * Sets the new type of the attribute.
	 *
	 * @param integer|null $typeid Type of the attribute
	 */
	public function setTypeId( $typeid )
	{
		if( $typeid == $this->getTypeId() ) { return; }

		$this->_values['typeid'] = (int) $typeid;
		$this->setModified();
	}


	/**
	 * Returns the type code of the attribute item.
	 *
	 * @return string|null Type code of the attribute item
	 */
	public function getType()
	{
		return ( isset( $this->_values['type'] ) ? (string) $this->_values['type'] : null );
	}


	/**
	 * Returns a unique code of the attribute item.
	 *
	 * @return string Returns the code of the attribute item
	 */
	public function getCode()
	{
		return ( isset( $this->_values['code'] ) ? (string) $this->_values['code'] : '' );
	}


	/**
	 * Sets a unique code for the attribute item.
	 *
	 * @param string $code Code of the attribute item
	 */
	public function setCode( $code )
	{
		$this->_checkCode( $code );

		if( $code == $this->getCode() ) { return; }

		$this->_values['code'] = (string) $code;
		$this->setModified();
	}


	/**
	 * Returns the status (enabled/disabled) of the attribute item.
	 *
	 * @return integer Returns the status of the item
	 */
	public function getStatus()
	{
		return ( isset( $this->_values['status'] ) ? (int) $this->_values['status'] : 0 );
	}


	/**
	 * Sets the new status of the attribute item.
	 *
	 * @param integer $status Status of the item
	 */
	public function setStatus( $status )
	{
		if( $status == $this->getStatus() ) { return; }

		$this->_values['status'] = (int) $status;
		$this->setModified();
	}


	/**
	 * Gets the position of the attribute item.
	 *
	 * @return integer Position of the attribute item
	 */
	public function getPosition()
	{
		return ( isset( $this->_values['pos'] ) ? (int) $this->_values['pos'] : 0 );
	}


	/**
	 * Sets the position of the attribute item
	 *
	 * @param integer $pos Position of the attribute item
	 */
	public function setPosition( $pos )
	{
		if( $pos == $this->getPosition() ) { return; }

		$this->_values['pos'] = (int) $pos;
		$this->setModified();
	}


	/**
	 * Returns the name of the attribute item.
	 *
	 * @return string Label of the attribute item
	 */
	public function getLabel()
	{
		return ( isset( $this->_values['label'] ) ? (string) $this->_values['label'] : '' );
	}


	/**
	 * Sets the new label of the attribute item.
	 *
	 * @param string $label Type label of the attribute item
	 */
	public function setLabel( $label )
	{
		if( $label == $this->getLabel() ) { return; }

		$this->_values['label'] = (string) $label;
		$this->setModified();
	}


	/**
	 * Sets the item values from the given array.
	 *
	 * @param array $list Associative list of item keys and their values
	 * @return array Associative list of keys and their values that are unknown
	 */
	public function fromArray( array $list )
	{
		$unknown = array();
		$list = parent::fromArray( $list );

		foreach( $list as $key => $value )
		{
			switch( $key )
			{
				case 'filter.domain': $this->setDomain( $value ); break;
				case 'filter.code': $this->setCode( $value ); break;
				case 'filter.status': $this->setStatus( $value ); break;
				case 'filter.typeid': $this->setTypeId( $value ); break;
				case 'filter.position': $this->setPosition( $value ); break;
				case 'filter.label': $this->setLabel( $value ); break;
				case 'filter.webtype': $this->setWebType( $value ); break;
				case 'filter.languageid': $this->setLanguageId( $value ); break;
				case 'filter.attribute': $this->setAttribute( $value ); break;
				case 'filter.subattribute': $this->setSubAttribute( $value ); break;
				case 'filter.parentid': $this->setParentId( $value ); break;
				case 'filter.keys': $this->setKeys( $value ); break;
				case 'filter.values': $this->setValues( $value ); break;
				case 'filter.condition': $this->setCondition( $value ); break;
				case 'filter.brackets': $this->setBrackets( $value ); break;
				case 'filter.direction': $this->setDirection( $value ); break;
				default: $unknown[$key] = $value;
			}
		}

		return $unknown;
	}


	/**
	 * Returns the item values as array.
	 *
	 * @return Associative list of item properties and their values
	 */
	public function toArray()
	{
		$list = parent::toArray();

		$list['filter.domain'] = $this->getDomain();
		$list['filter.code'] = $this->getCode();
		$list['filter.status'] = $this->getStatus();
		$list['filter.typeid'] = $this->getTypeId();
		$list['filter.type'] = $this->getType();
		$list['filter.position'] = $this->getPosition();
		$list['filter.label'] = $this->getLabel();
		$list['filter.webtype'] = $this->getWebType();
		$list['filter.languageid'] = $this->getLanguageId();
		$list['filter.attribute'] = $this->getAttribute();
		$list['filter.subattribute'] = $this->getSubAttribute();
		$list['filter.parentid'] = $this->getParentId();
		$list['filter.keys'] = $this->getKeys();
		$list['filter.values'] = $this->getValues();
		$list['filter.condition'] = $this->getCondition();
		$list['filter.brackets'] = $this->getBrackets();
		$list['filter.direction'] = $this->getDirection();
		return $list;
	}

	/**
	 * Tests if the code is valid.
	 *
	 * @param string $code New code for an item
	 * @throws MShop_Exception If the code is invalid
	 */
	protected function _checkCode( $code )
	{
	    if( strlen( $code ) > 100 ) {
	        throw new MShop_Exception( sprintf( 'Code must not be longer than 100 characters' ) );
	    }
	}
	
	/**
	 * Returns the web_filter of the common list type item
	 *
	 * @return string Web_filter of the common list type item
	 */
	public function getWebType()
	{
	    return ( isset( $this->_values['webtype'] ) ? (string) $this->_values['webtype'] : '' );
	}
	
	/**
	 * Sets the new label of the attribute item.
	 *
	 * @param string $label Type label of the attribute item
	 */
	public function setWebType( $webtype )
	{
	    if( $webtype == $this->getWebType() ) { return; }
	
	    $this->_values['webtype'] = (string) $webtype;
	    $this->setModified();
	}
	
	/**
	 * Returns the language ID of the product property item.
	 *
	 * @return string|null Language ID of the product property item
	 */
	public function getLanguageId()
	{
	    return ( isset( $this->_values['langid'] ) ? (string) $this->_values['langid'] : null );
	}
	
	
	/**
	 *  Sets the language ID of the product property item.
	 *
	 * @param string|null $id Language ID of the product property item
	 */
	public function setLanguageId( $id )
	{
	    if ( $id === $this->getLanguageId() ) { return; }
	
	    $this->_checkLanguageId( $id );
	    $this->_values['langid'] = $id;
	    $this->setModified();
	}
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getAttribute()
	{
	    return ( isset( $this->_values['attribute'] ) ? (string) $this->_values['attribute'] : null );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setAttribute( $attribute )
	{
	    if ( $attribute == $this->getAttribute() ) { return; }
	
	    $this->_values['attribute'] = (int) $attribute;
	    $this->setModified();
	}

	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getSubAttribute()
	{
	    return ( isset( $this->_values['subattribute'] ) ? (string) $this->_values['subattribute'] : null );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setSubAttribute( $subattribute )
	{
	    if ( $subattribute == $this->getSubAttribute() ) { return; }
	
	    $this->_values['subattribute'] = (int) $subattribute;
	    $this->setModified();
	}
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getParentId()
	{
	    return ( isset( $this->_values['parentid'] ) ? (string) $this->_values['parentid'] : null );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setParentId( $parentId )
	{
	    if ( $parentId == $this->getParentId() ) { return; }
	
	    $this->_values['parentid'] = (int) $parentId;
	    $this->setModified();
	}
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getKeys()
	{
	    return ( isset( $this->_values['keys'] ) ? (string) $this->_values['keys'] : '' );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setKeys( $keys )
	{
	    if ( $keys == $this->getKeys() ) { return; }
	
	    $this->_values['keys'] = (string) $keys;
	    $this->setModified();
	}
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getValues()
	{
	    return ( isset( $this->_values['values'] ) ? (string) $this->_values['values'] : '' );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setValues( $values )
	{
	    if ( $values == $this->getValues() ) { return; }
	
	    $this->_values['values'] = (string) $values;
	    $this->setModified();
	}
	
	public function getChildren()
	{
	    return ( isset( $this->_values['filters'] ) ? (array)$this->_values['filters'] : array() );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setChildren( $filters )
	{
//	    if ( $filters == $this->getValues() ) { return; }
	
	    $this->_values['filters'] = $filters;
	}
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function hasChildren()
	{
		if( count( $this->_values['filters'] ) > 0 ) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getCondition()
	{
	    return ( isset( $this->_values['condition'] ) ? (string) $this->_values['condition'] : '' );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setCondition( $condition )
	{
	    if ( $condition == $this->getCondition() ) { return; }
	
	    $this->_values['condition'] = (string) $condition;
	    $this->setModified();
	}

	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getDirection()
	{
	    return ( isset( $this->_values['direction'] ) ? (string) $this->_values['direction'] : '' );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setDirection( $direction )
	{
	    if ( $direction == $this->getDirection() ) { return; }
	
	    $this->_values['direction'] = (string) $direction;
	    $this->setModified();
	}
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getBrackets()
	{
	    return ( isset( $this->_values['brackets'] ) ? (string) $this->_values['brackets'] : '' );
	}
	
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setBrackets( $brackets )
	{
	    if ( $brackets == $this->getBrackets() ) { return; }
	
	    $this->_values['brackets'] = (string) $brackets;
	    $this->setModified();
	}
	
}
