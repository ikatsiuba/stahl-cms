<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Attributegroup
 */


/**
 * Default catalog list manager for creating and handling catalog list items.
 * @package MShop
 * @subpackage Attributegroup
 */
class MShop_Attributegroup_Manager_List_Default
	extends MShop_Common_Manager_List_Abstract
	implements MShop_Attributegroup_Manager_List_Interface
{
	private $_searchConfig = array(
		'attributegroup.list.id'=> array(
			'code'=>'attributegroup.list.id',
			'internalcode'=>'mcatli."id"',
			'internaldeps'=> array( 'LEFT JOIN "mshop_attributegroup_list" AS mcatli ON ( mcat."id" = mcatli."parentid" )' ),
			'label'=>'attributegroup list ID',
			'type'=> 'integer',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'attributegroup.list.siteid'=> array(
			'code'=>'attributegroup.list.siteid',
			'internalcode'=>'mcatli."siteid"',
			'label'=>'attributegroup list site ID',
			'type'=> 'integer',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'attributegroup.list.parentid'=> array(
			'code'=>'attributegroup.list.parentid',
			'internalcode'=>'mcatli."parentid"',
			'label'=>'attributegroup list parent ID',
			'type'=> 'integer',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'attributegroup.list.domain'=> array(
			'code'=>'attributegroup.list.domain',
			'internalcode'=>'mcatli."domain"',
			'label'=>'attributegroup list Domain',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.typeid'=> array(
			'code'=>'attributegroup.list.typeid',
			'internalcode'=>'mcatli."typeid"',
			'label'=>'attributegroup list type ID',
			'type'=> 'integer',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'attributegroup.list.refid'=> array(
			'code'=>'attributegroup.list.refid',
			'internalcode'=>'mcatli."refid"',
			'label'=>'attributegroup list reference ID',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.datestart' => array(
			'code'=>'attributegroup.list.datestart',
			'internalcode'=>'mcatli."start"',
			'label'=>'attributegroup list start date',
			'type'=> 'datetime',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.dateend' => array(
			'code'=>'attributegroup.list.dateend',
			'internalcode'=>'mcatli."end"',
			'label'=>'attributegroup list end date',
			'type'=> 'datetime',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.config' => array(
			'code'=>'attributegroup.list.config',
			'internalcode'=>'mcatli."config"',
			'label'=>'attributegroup list config',
			'type'=> 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.position' => array(
			'code'=>'attributegroup.list.position',
			'internalcode'=>'mcatli."pos"',
			'label'=>'attributegroup list position',
			'type'=> 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
		),
		'attributegroup.list.status' => array(
			'code'=>'attributegroup.list.status',
			'internalcode'=>'mcatli."status"',
			'label'=>'attributegroup list status',
			'type'=> 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
		),
		'attributegroup.list.ctime'=> array(
			'label' => 'attributegroup list creation time',
			'code' => 'attributegroup.list.ctime',
			'internalcode' => 'mcatli."ctime"',
			'type' => 'datetime',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.mtime'=> array(
			'label' => 'attributegroup list modification time',
			'code' => 'attributegroup.list.mtime',
			'internalcode' => 'mcatli."mtime"',
			'type' => 'datetime',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.editor'=> array(
			'code'=>'attributegroup.list.editor',
			'internalcode'=>'mcatli."editor"',
			'label'=>'attributegroup list editor',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
	);


	/**
	 * Initializes the object.
	 *
	 * @param MShop_Context_Item_Interface $context Context object
	 */
	public function __construct( MShop_Context_Item_Interface $context )
	{
		parent::__construct( $context );
		$this->_setResourceName( 'db-attributegroup' );
	}


	/**
	 * Removes old entries from the storage.
	 *
	 * @param array $siteids List of IDs for sites whose entries should be deleted
	 */
	public function cleanup( array $siteids )
	{
		$path = 'classes/attributegroup/manager/list/submanagers';
		foreach( $this->_getContext()->getConfig()->get( $path, array( 'type' ) ) as $domain ) {
			$this->getSubManager( $domain )->cleanup( $siteids );
		}

		$this->_cleanup( $siteids, 'mshop/attributegroup/manager/list/default/item/delete' );
	}


	/**
	 * Returns the list attributes that can be used for searching.
	 *
	 * @param boolean $withsub Return also attributes of sub-managers if true
	 * @return array List of attribute items implementing MW_Common_Criteria_Attribute_Interface
	 */
	public function getSearchAttributes( $withsub = true )
	{
		$path = 'classes/attributegroup/manager/list/submanagers';

		return $this->_getSearchAttributes( $this->_searchConfig, $path, array( 'type' ), $withsub );
	}


	/**
	 * Returns a new manager for attributegroup list extensions.
	 *
	 * @param string $manager Name of the sub manager type in lower case
	 * @param string|null $name Name of the implementation, will be from configuration (or Default) if null
	 * @return MShop_Common_Manager_Interface Manager for different extensions, e.g types, lists etc.
	 */
	public function getSubManager( $manager, $name = null )
	{


		return $this->_getSubManager( 'attributegroup', 'list/' . $manager, $name );
	}


	/**
	 * Returns the config path for retrieving the configuration values.
	 *
	 * @return string Configuration path
	 */
	protected function _getConfigPath()
	{
		return 'mshop/attributegroup/manager/list/default/item/';
	}


	/**
	 * Returns the search configuration for searching items.
	 *
	 * @return array Associative list of search keys and search definitions
	 */
	protected function _getSearchConfig()
	{
		return $this->_searchConfig;
	}
}