<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */

return array(
	'filter/type' => array(
		array( 'domain' => 'catalog', 'code' => 'default', 'label' => 'Category Based Filter', 'status' => 1 ),
		array( 'domain' => 'catalog', 'code' => 'global', 'label' => 'Global Filter', 'status' => 1 )
	),
    'catalog/list/type' => array(
        array( 'domain' => 'filter', 'code' => 'default', 'label' => 'Default', 'status' => 1 ),
        array( 'domain' => 'filter', 'code' => 'global', 'label' => 'Global', 'status' => 1 )
    ),
    'text/type' => array(
        array( 'domain' => 'filter', 'code' => 'name', 'label' => 'Name', 'status' => 1 ),
        array( 'domain' => 'filter', 'code' => 'childname', 'label' => 'ChildName', 'status' => 1 )
    ),
    'filter/list/type' => array(
        array( 'domain' => 'text', 'code' => 'default', 'label' => 'Default', 'status' => 1 )
    )
);