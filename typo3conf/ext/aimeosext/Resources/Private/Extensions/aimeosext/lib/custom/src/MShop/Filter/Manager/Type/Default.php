<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2013
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Filter
 */


/**
 * Default filter type manager for creating and handling attribute type items.
 * @package MShop
 * @subpackage Filter
 */
class MShop_Filter_Manager_Type_Default
	extends MShop_Common_Manager_Type_Abstract
	implements MShop_Filter_Manager_Type_Interface
{
	private $_searchConfig = array(
		'filter.type.id' => array(
			'label' => 'filter type ID',
			'code' => 'filter.type.id',
			'internalcode' => 'mfiltty."id"',
			'internaldeps' => array( 'LEFT JOIN "mshop_filter_type" AS mfiltty ON ( mfilt."typeid" = mfiltty."id" )' ),
			'type' => 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'filter.type.siteid' => array(
			'code' => 'filter.type.siteid',
			'internalcode' => 'mfiltty."siteid"',
			'label' => 'filter type site ID',
			'type' => 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'filter.type.code' => array(
			'label' => 'filter type code',
			'code' => 'filter.type.code',
			'internalcode' => 'mfiltty."code"',
			'type' => 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.type.domain' => array(
			'label' => 'filter type domain',
			'code' => 'filter.type.domain',
			'internalcode' => 'mfiltty."domain"',
			'type' => 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.type.label' => array(
			'code' => 'filter.type.label',
			'internalcode' => 'mfiltty."label"',
			'label' => 'filter type label',
			'type' => 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.type.status' => array(
			'code' => 'filter.type.status',
			'internalcode' => 'mfiltty."status"',
			'label' => 'filter type status',
			'type' => 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
		),
		'filter.type.ctime'=> array(
			'code'=>'filter.type.ctime',
			'internalcode'=>'mfiltty."ctime"',
			'label'=>'filter type create date/time',
			'type'=> 'datetime',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.type.mtime'=> array(
			'code'=>'filter.type.mtime',
			'internalcode'=>'mfiltty."mtime"',
			'label'=>'filter type modification date/time',
			'type'=> 'datetime',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.type.editor'=> array(
			'code'=>'filter.type.editor',
			'internalcode'=>'mfiltty."editor"',
			'label'=>'filter type editor',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
	);


	/**
	 * Initializes the object.
	 *
	 * @param MShop_Context_Item_Interface $context Context object
	 */
	public function __construct( MShop_Context_Item_Interface $context )
	{
		parent::__construct( $context );
		$this->_setResourceName( 'db-filter' );
	}


	/**
	 * Removes old entries from the storage.
	 *
	 * @param array $siteids List of IDs for sites whose entries should be deleted
	 */
	public function cleanup( array $siteids )
	{
		$path = 'classes/filter/manager/type/submanagers';
		foreach( $this->_getContext()->getConfig()->get( $path, array() ) as $domain ) {
			$this->getSubManager( $domain )->cleanup( $siteids );
		}

		$this->_cleanup( $siteids, 'mshop/filter/manager/type/default/item/delete' );
	}


	/**
	 * Returns the filters that can be used for searching.
	 *
	 * @param boolean $withsub Return also filters of sub-managers if true
	 * @return array List of filter items implementing MW_Common_Criteria_filter_Interface
	 */
	public function getSearchAttributes( $withsub = true )
	{
		$path = 'classes/filter/manager/type/submanagers';

		return $this->_getSearchAttributes( $this->_searchConfig, $path, array(), $withsub );
	}


	/**
	 * Returns a new manager for filter type extensions.
	 *
	 * @param string $manager Name of the sub manager type in lower case
	 * @param string|null $name Name of the implementation, will be from configuration (or Default) if null
	 * @return MShop_Common_Manager_Interface Manager for different extensions, e.g types, lists etc.
	 */
	public function getSubManager( $manager, $name = null )
	{
		return $this->_getSubManager( 'filter', 'type/' . $manager, $name );
	}


	/**
	 * Returns the config path for retrieving the configuration values.
	 *
	 * @return string Configuration path
	 */
	protected function _getConfigPath()
	{
		return 'mshop/filter/manager/type/default/item/';
	}


	/**
	 * Returns the search configuration for searching items.
	 *
	 * @return array Associative list of search keys and search definitions
	 */
	protected function _getSearchConfig()
	{
		return $this->_searchConfig;
	}
}