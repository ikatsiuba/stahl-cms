<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Filter
 */


/**
 * Interface for filter manager.
 *
 * @package MShop
 * @subpackage Filter
 */
interface MShop_Filter_Manager_Interface
	extends MShop_Common_Manager_Factory_Interface
{
}
