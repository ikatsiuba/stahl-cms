<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Common
 */


/**
 * Default implementation of the list item.
 *
 * @package MShop
 * @subpackage Common
 */
class MShop_Common_Item_Type_DefaultExt
	extends MShop_Common_Item_Abstract
	implements MShop_Common_Item_Type_Interface
{
	private $_prefix;
	private $_values;


	/**
	 * Initializes the type item object.
	 *
	 * @param string $prefix Property prefix when converting to array
	 * @param array $values Initial values of the list type item
	 */
	public function __construct( $prefix, array $values = array() )
	{
		parent::__construct( $prefix, $values );

		$this->_prefix = $prefix;
		$this->_values = $values;
	}


	/**
	 * Returns the code of the common list type item
	 *
	 * @return string Code of the common list type item
	 */
	public function getCode()
	{
		return ( isset( $this->_values['code'] ) ? (string) $this->_values['code'] : '' );
	}


	/**
	 * Sets the code of the common list type item
	 *
	 * @param string $code New code of the common list type item
	 */
	public function setCode( $code )
	{
		$this->_checkCode( $code );

		if( $code == $this->getCode() ) { return; }

		$this->_values['code'] = (string) $code;
		$this->setModified();
	}


	/**
	 * Returns the domain of the common list type item
	 *
	 * @return string Domain of the common list type item
	 */
	public function getDomain()
	{
		return ( isset( $this->_values['domain'] ) ? (string) $this->_values['domain'] : '' );
	}


	/**
	 * Sets the domain of the common list type item
	 *
	 * @param string $domain New domain of the common list type item
	 */
	public function setDomain( $domain )
	{
		if( $domain == $this->getDomain() ) { return; }

		$this->_values['domain'] = (string) $domain;
		$this->setModified();
	}


	/**
	 * Returns the label of the common list type item
	 *
	 * @return string Label of the common list type item
	 */
	public function getLabel()
	{
		return ( isset( $this->_values['label'] ) ? (string) $this->_values['label'] : '' );
	}


	/**
	 * Sets the label of the common list type item
	 *
	 * @param string $label New label of the common list type item
	 */
	public function setLabel( $label )
	{
		if( $label == $this->getLabel() ) { return; }

		$this->_values['label'] = (string) $label;
		$this->setModified();
	}

	/**
	 * Returns the web_filter of the common list type item
	 *
	 * @return string Web_filter of the common list type item
	 */
	public function getWebfilter()
	{
		return ( isset( $this->_values['web_filter'] ) ? (string) $this->_values['web_filter'] : '' );
	}

	/**
	 * Sets the web_filter of the common list type item
	 *
	 * @param string $web_filter New web_filter of the common list type item
	 */
	public function setWebfilter( $web_filter )
	{
		if( $web_filter == $this->getWebfilter() ) { return; }

		$this->_values['web_filter'] = (string) $web_filter;
		$this->setModified();
	}



	/**
	 * Returns the web_style of the common list type item
	 *
	 * @return string Web_style of the common list type item
	 */
	public function getWebstyle()
	{
		return ( isset( $this->_values['web_style'] ) ? (string) $this->_values['web_style'] : '' );
	}

	/**
	 * Sets the web_style of the common list type item
	 *
	 * @param string $web_style New web_style of the common list type item
	 */
	public function setWebstyle( $web_style )
	{
		if( $web_style == $this->getWebstyle() ) { return; }

		$this->_values['web_style'] = (string) $web_style;
		$this->setModified();
	}


	
	
	/**
	 * Returns the unit of the common list type item
	 *
	 * @return string unit of the common list type item
	 */
	public function getUnit()
	{
	    return ( isset( $this->_values['unit'] ) ? (string) $this->_values['unit'] : '' );
	}
	
	/**
	 * Sets the unit of the common list type item
	 *
	 * @param string $unit New unit of the common list type item
	 */
	public function setUnit( $unit )
	{
	    if( $unit == $this->getUnit() ) { return; }
	
	    $this->_values['unit'] = (string) $unit;
	    $this->setModified();
	}
	
	


	/**
	 * Returns the status of the common list type item
	 *
	 * @return integer Status of the common list type item
	 */
	public function getStatus()
	{
		return ( isset( $this->_values['status'] ) ? (int) $this->_values['status'] : 0 );
	}


	/**
	 * Sets the status of the common list type item
	 *
	 * @param integer $status New status of the common list type item
	 */
	public function setStatus( $status )
	{
		if( $status == $this->getStatus() ) { return; }

		$this->_values['status'] = (int) $status;
		$this->setModified();
	}


	/**
	 * Sets the item values from the given array.
	 *
	 * @param array $list Associative list of item keys and their values
	 * @return array Associative list of keys and their values that are unknown
	 */
	public function fromArray( array $list )
	{
		$unknown = array();
		$list = parent::fromArray( $list );

		foreach( $list as $key => $value )
		{
			switch( $key )
			{
				case $this->_prefix . 'code': $this->setCode( $value ); break;
				case $this->_prefix . 'domain': $this->setDomain( $value ); break;
				case $this->_prefix . 'label': $this->setLabel( $value ); break;
				case $this->_prefix . 'status': $this->setStatus( $value ); break;
				case $this->_prefix . 'web_filter': $this->setWebfilter( $value ); break;
				case $this->_prefix . 'web_style': $this->setWebstyle( $value ); break;
				case $this->_prefix . 'unit': $this->setUnit( $value ); break;
				default: $unknown[$key] = $value;
			}
		}

		return $unknown;
	}


	/**
	 * Returns an associative list of item properties.
	 *
	 * @return array List of item properties.
	 */
	public function toArray()
	{
		$list = parent::toArray();

		$list[$this->_prefix . 'code'] = $this->getCode();
		$list[$this->_prefix . 'domain'] = $this->getDomain();
		$list[$this->_prefix . 'label'] = $this->getLabel();
		$list[$this->_prefix . 'status'] = $this->getStatus();
		$list[$this->_prefix . 'web_filter'] = $this->getWebfilter();
		$list[$this->_prefix . 'web_style'] = $this->getWebstyle();
		$list[$this->_prefix . 'unit'] = $this->getUnit();
		
		return $list;
	}
	
	/**
	 * Tests if the code is valid.
	 *
	 * @param string $code New code for an item
	 * @throws MShop_Exception If the code is invalid
	 */
	protected function _checkCode( $code )
	{
	    if( strlen( $code ) > 100 ) {
	        throw new MShop_Exception( sprintf( 'Code must not be longer than 100 characters' ) );
	    }
	}
	
}
