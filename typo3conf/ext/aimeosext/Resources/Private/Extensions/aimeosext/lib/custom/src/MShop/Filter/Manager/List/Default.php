<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2013
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Filter
 */


/**
 * Default Filter list manager for creating and handling attribute list items.
 * @package MShop
 * @subpackage Filter
 */
class MShop_Filter_Manager_List_Default
	extends MShop_Common_Manager_List_Abstract
	implements MShop_Filter_Manager_List_Interface
{
	private $_searchConfig = array(
		'filter.list.id'=> array(
			'code'=>'filter.list.id',
			'internalcode'=>'mfiltli."id"',
			'internaldeps' => array( 'LEFT JOIN "mshop_filter_list" AS mfiltli ON ( matt."id" = mfiltli."parentid" )' ),
			'label'=>'List ID',
			'type'=> 'integer',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'filter.list.parentid'=> array(
			'code'=>'filter.list.parentid',
			'internalcode'=>'mfiltli."parentid"',
			'label'=>'filter ID',
			'type'=> 'integer',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'filter.list.siteid' => array(
			'code' => 'filter.list.siteid',
			'internalcode' => 'mfiltli."siteid"',
			'label' => 'filter list site ID',
			'type' => 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'filter.list.domain'=> array(
			'code'=>'filter.list.domain',
			'internalcode'=>'mfiltli."domain"',
			'label'=>'filter list domain',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.typeid'=> array(
			'code'=>'filter.list.typeid',
			'internalcode'=>'mfiltli."typeid"',
			'label'=>'filter list typeID',
			'type'=> 'integer',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'filter.list.refid'=> array(
			'code'=>'filter.list.refid',
			'internalcode'=>'mfiltli."refid"',
			'label'=>'filter list reference ID',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.datestart' => array(
			'code'=>'filter.list.datestart',
			'internalcode'=>'mfiltli."start"',
			'label'=>'filter list start date',
			'type'=> 'datetime',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.dateend' => array(
			'code'=>'filter.list.dateend',
			'internalcode'=>'mfiltli."end"',
			'label'=>'filter list end date',
			'type'=> 'datetime',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.config' => array(
			'code'=>'filter.list.config',
			'internalcode'=>'mfiltli."config"',
			'label'=>'filter list config',
			'type'=> 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.position' => array(
			'code'=>'filter.list.position',
			'internalcode'=>'mfiltli."pos"',
			'label'=>'filter list position',
			'type'=> 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
		),
		'filter.list.status' => array(
			'code'=>'filter.list.status',
			'internalcode'=>'mfiltli."status"',
			'label'=>'filter list status',
			'type'=> 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
		),
		'filter.list.ctime'=> array(
			'code'=>'filter.list.ctime',
			'internalcode'=>'mfiltli."ctime"',
			'label'=>'filter list create date/time',
			'type'=> 'datetime',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.mtime'=> array(
			'code'=>'filter.list.mtime',
			'internalcode'=>'mfiltli."mtime"',
			'label'=>'filter list modification date/time',
			'type'=> 'datetime',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
		'filter.list.editor'=> array(
			'code'=>'filter.list.editor',
			'internalcode'=>'mfiltli."editor"',
			'label'=>'filter list editor',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
	);


	/**
	 * Initializes the object.
	 *
	 * @param MShop_Context_Item_Interface $context Context object
	 */
	public function __construct( MShop_Context_Item_Interface $context )
	{
		parent::__construct( $context );
		$this->_setResourceName( 'db-filter' );
	}


	/**
	 * Removes old entries from the storage.
	 *
	 * @param array $siteids List of IDs for sites whose entries should be deleted
	 */
	public function cleanup( array $siteids )
	{
		$path = 'classes/filter/manager/list/submanagers';
		foreach( $this->_getContext()->getConfig()->get( $path, array( 'type' ) ) as $domain ) {
			$this->getSubManager( $domain )->cleanup( $siteids );
		}

		$this->_cleanup( $siteids, 'mshop/filter/manager/list/default/item/delete' );
	}


	/**
	 * Returns the list filters that can be used for searching.
	 *
	 * @param boolean $withsub Return also filters of sub-managers if true
	 * @return array List of filter items implementing MW_Common_Criteria_Attribute_Interface
	 */
	public function getSearchAttributes( $withsub = true )
	{

		$path = 'classes/filter/manager/list/submanagers';

		return $this->_getSearchAttributes( $this->_searchConfig, $path, array( 'type' ), $withsub );
	}


	/**
	 * Returns a new manager for filter list extensions.
	 *
	 * @param string $manager Name of the sub manager type in lower case
	 * @param string|null $name Name of the implementation, will be from configuration (or Default) if null
	 * @return MShop_Common_Manager_Interface Manager for different extensions, e.g types, lists etc.
	 */
	public function getSubManager( $manager, $name = null )
	{
		return $this->_getSubManager( 'filter', 'list/' . $manager, $name );
	}


	/**
	 * Returns the config path for retrieving the configuration values.
	 *
	 * @return string Configuration path
	 */
	protected function _getConfigPath()
	{
		return 'mshop/filter/manager/list/default/item/';
	}


	/**
	 * Returns the search configuration for searching items.
	 *
	 * @return array Associative list of search keys and search definitions
	 */
	protected function _getSearchConfig()
	{
		return $this->_searchConfig;
	}
}