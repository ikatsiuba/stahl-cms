<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Attributegroup
 */


/**
 * Factory for Attributegroup Manager.
 *
 * @package MShop
 * @subpackage Attributegroup
 */
class MShop_Attributegroup_Manager_Factory
	extends MShop_Common_Factory_Abstract
	implements MShop_Common_Factory_Interface
{
	/**
	 * Creates a attributegroup DAO object.
	 *
	 * @param MShop_Context_Item_Interface $context Shop context instance with necessary objects
	 * @param string $name Manager name
	 * @return MShop_Common_Manager_Interface Manager object
	 * @throws MShop_Attributegroup_Exception If requested manager implementation couldn't be found
	 */
	public static function createManager( MShop_Context_Item_Interface $context, $name = null )
	{
		/** classes/attributegroup/manager/name
		 */
		if( $name === null ) {
			$name = $context->getConfig()->get( 'classes/attributegroup/manager/name', 'Default' );
		}

		if( ctype_alnum( $name ) === false )
		{
			$classname = is_string( $name ) ? 'MShop_Attributegroup_Manager_' . $name : '<not a string>';
			throw new MShop_Attributegroup_Exception( sprintf( 'Invalid characters in class name "%1$s"', $classname ) );
		}

		$iface = 'MShop_Attributegroup_Manager_Interface';
		$classname = 'MShop_Attributegroup_Manager_' . $name;

		$manager = self::_createManager( $context, $classname, $iface );

		/** mshop/attributegroup/manager/decorators/excludes
		 */

		/** mshop/attributegroup/manager/decorators/global
		 */

		/** mshop/attributegroup/manager/decorators/local
		 */
		return self::_addManagerDecorators( $context, $manager, 'attributegroup' );
	}
}