<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */

return array(
	'item' => array(
		'delete' => '
			DELETE FROM "mshop_attributegroup"
			WHERE "siteid" = :siteid AND "nleft" >= ? AND "nright" <= ?
		',
		'get' => '
			SELECT DISTINCT mcat."id", mcat."label", mcat."config",
				mcat."code", mcat."status", mcat."level", mcat."parentid",
				mcat."siteid", mcat."nleft" AS "left",
				mcat."nright" AS "right", mcat."mtime", mcat."editor",
				mcat."ctime"
			FROM "mshop_attributegroup" AS mcat, "mshop_attributegroup" AS parent
			WHERE mcat."siteid" = :siteid AND mcat."nleft" >= parent."nleft"
				AND mcat."nleft" <= parent."nright"
				AND parent."siteid" = :siteid AND parent."id" = ?
				AND mcat."level" <= parent."level" + ? AND :cond
			ORDER BY mcat."nleft"
		',
		'insert' => '
			INSERT INTO "mshop_attributegroup" (
				"siteid", "label", "code", "status", "parentid", "level",
				"nleft", "nright"
			) VALUES (
				:siteid, ?, ?, ?, ?, ?, ?, ?
			)
		',
		'update' => '
			UPDATE "mshop_attributegroup"
			SET "label" = ?, "code" = ?, "status" = ?
			WHERE "siteid" = :siteid AND "id" = ?
		',
		'update-parentid' => '
			UPDATE "mshop_attributegroup"
			SET "parentid" = ?
			WHERE "siteid" = :siteid AND "id" = ?
		',
		'move-left' => '
			UPDATE "mshop_attributegroup"
			SET "nleft" = "nleft" + ?, "level" = "level" + ?
			WHERE "siteid" = :siteid AND "nleft" >= ? AND "nleft" <= ?
		',
		'move-right' => '
			UPDATE "mshop_attributegroup"
			SET "nright" = "nright" + ?
			WHERE "siteid" = :siteid AND "nright" >= ? AND "nright" <= ?
		',
		'search' => '
			SELECT DISTINCT mcat."id", mcat."label", mcat."config",
				mcat."code", mcat."status", mcat."level", mcat."siteid",
				mcat."nleft" AS "left", mcat."nright" AS "right",
				mcat."mtime", mcat."editor", mcat."ctime"
			FROM "mshop_attributegroup" AS mcat
			WHERE mcat."siteid" = :siteid AND mcat."nleft" >= ?
				AND mcat."nright" <= ? AND :cond
			ORDER BY :order
		',
		'search-item' => '
			SELECT DISTINCT mcat."id", mcat."label", mcat."config",
				mcat."code", mcat."status", mcat."level", mcat."parentid",
				mcat."siteid", mcat."nleft" AS "left", mcat."nright" AS "right",
				mcat."mtime", mcat."editor", mcat."ctime",atlst."label"
			FROM "mshop_attributegroup" AS mcat
	        LEFT JOIN "mshop_attributegroup_list" as aglst ON aglst."parentid"=mcat."id" and aglst."domain"=\'attribute\'
	        LEFT JOIN "mshop_attribute" as atlst ON atlst."id"=aglst."refid" and atlst."domain"=\'attributegroup\'
	        :joins
			WHERE :cond
			ORDER BY atlst."label" ASC
			LIMIT :size OFFSET :start
		',
		'count' => '
			SELECT COUNT(*) AS "count"
			FROM (
				SELECT DISTINCT mcat."id"
				FROM "mshop_attributegroup" AS mcat
				:joins
				WHERE :cond
				LIMIT 10000 OFFSET 0
			) AS list
		',
		'usage' => array(
			'update' => '
				UPDATE "mshop_attributegroup"
				SET "config" = ?, "mtime" = ?, "editor" = ?
				WHERE "siteid" = ? AND "id" = ?
			',
			'add' => '
				UPDATE "mshop_attributegroup"
				SET "config" = ?, "mtime" = ?, "editor" = ?, "ctime" = ?
				WHERE "siteid" = ? AND "id" = ?
			',
		),
	),
);
