<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Filter
 */


/**
 * Generic interface for all filter items.
 *
 * @package MShop
 * @subpackage Filter
 */
interface MShop_Filter_Item_Interface
	extends MShop_Common_Item_ListRef_Interface, MShop_Common_Item_Position_Interface, MShop_Common_Item_Typeid_Interface
{

    /**
     * Returns the domain of the attribute item.
     *
     * @return string Returns the domain for this item e.g. text, media, price...
     */
    public function getDomain();
    
    /**
     * Set the name of the domain for this attribute item.
     *
     * @param string $domain Name of the domain e.g. text, media, price...
     * @return void
     */
    public function setDomain( $domain );
    
    /**
     * Returns a unique code of the attribute item.
     *
     * @return string Returns the code of the attribute item
     */
    public function getCode();
    
    /**
     * Sets a unique code for the attribute item.
     *
     * @param string $code Code of the attribute item
     * @return void
     */
    public function setCode( $code );
    
    /**
     * Returns the status (enabled/disabled) of the attribute item.
     *
     * @return integer Returns the status
     */
    public function getStatus();
    
    /**
     * Sets the new status of the attribute item.
     *
     * @param integer $status Status of attribute item
     * @return void
     */
    public function setStatus( $status );
    
    /**
     * Returns the name of the attribute item.
     *
     * @return string Label of the attribute item
     */
    public function getLabel();
    
    /**
     * Sets the new label of the attribute item.
     *
     * @param string $label Type label of the attribute item
     * @return void
     */
    public function setLabel( $label );
	/**
	 * Returns the language id of the property item
	 *
	 * @return string Language ID of the property item
	 */
	public function getLanguageId();

	/**
	 * Sets the Language Id of the property item
	 *
	 * @param string $id New Language ID of the property item
	 * @return void
	 */
	public function setLanguageId( $id );
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getAttribute();

	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setAttribute( $attribute );

	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getSubAttribute();
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setSubAttribute( $attribute );
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getWebType();
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setWebType( $webtype );
	
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getParentId();
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setParentId( $id );
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getKeys();
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setKeys( $keys );
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getValues();
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setValues( $values );
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getCondition();
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setCondition( $condition );
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getDirection();
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setDirection( $direction );
	/**
	 * Returns the value of the property item.
	 *
	 * @return string Value of the property item
	 */
	public function getBrackets();
	
	/**
	 * Sets the new value of the property item.
	 *
	 * @param string $value Value of the property item
	 * @return void
	 */
	public function setBrackets( $brackets );
}
