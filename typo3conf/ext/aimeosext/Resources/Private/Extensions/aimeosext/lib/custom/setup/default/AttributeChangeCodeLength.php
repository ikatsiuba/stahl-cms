<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 */


/**
 * Renames column "text" to "content".
 */
class MW_Setup_Task_AttributeChangeCodeLength extends MW_Setup_Task_Abstract
{
	private $_mysql = array(
		'ALTER TABLE "mshop_attribute" MODIFY "code" VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL',
		'ALTER TABLE "mshop_attribute_type" MODIFY "code" VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL',
	);

	/**
	 * Returns the list of task names which this task depends on.
	 *
	 * @return array List of task names
	 */
	public function getPreDependencies()
	{
		return array('TablesCreateMShop');
	}


	/**
	 * Returns the list of task names which depends on this task.
	 *
	 * @return string[] List of task names
	 */
	public function getPostDependencies()
	{
		return array();
	}


	/**
	 * Executes the task for MySQL databases.
	 */
	protected function _mysql()
	{
		$this->_process( $this->_mysql );
	}


	/**
	 * Migrates service text data to list table.
	 *
	 * @param array $stmts Associative array of tables names and lists of SQL statements to execute.
	 */
	protected function _process( array $stmts )
	{
		$this->_msg( 'Changing mshop_attribute/type.code', 0 );

		if( $this->_schema->tableExists( 'mshop_attribute' ) === true
			&& $this->_schema->columnExists( 'mshop_attribute', 'code' ) === true )
		{
			$this->_executeList( $stmts );
			$this->_status( 'migrated' );
		}
		else
		{
			$this->_status( 'OK' );
		}
	}

}
