<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Media
 */


/**
 * Default media manager implementation.
 *
 * @package MShop
 * @subpackage Media
 */
class MShop_Media_Manager_DefaultExt
    extends MShop_Media_Manager_Default
{
    private $_searchConfig = array(
            'media.id' => array(
                    'label' => 'Media ID',
                    'code' => 'media.id',
                    'internalcode' => 'mmed."id"',
                    'type' => 'integer',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
            ),
            'media.siteid' => array(
                    'label' => 'Media site ID',
                    'code' => 'media.siteid',
                    'internalcode' => 'mmed."siteid"',
                    'type' => 'integer',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
                    'public' => false,
            ),
            'media.typeid' => array(
                    'label' => 'Media type ID',
                    'code' => 'media.typeid',
                    'internalcode' => 'mmed."typeid"',
                    'type' => 'integer',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
                    'public' => false,
            ),
            'media.languageid' => array(
                    'label' => 'Media language code',
                    'code' => 'media.languageid',
                    'internalcode' => 'mmed."langid"',
                    'type' => 'string',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.domain' => array(
                    'label' => 'Media domain',
                    'code' => 'media.domain',
                    'internalcode' => 'mmed."domain"',
                    'type' => 'string',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.label' => array(
                    'label' => 'Media label',
                    'code' => 'media.label',
                    'internalcode' => 'mmed."label"',
                    'type' => 'string',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.mimetype' => array(
                    'label' => 'Media mimetype',
                    'code' => 'media.mimetype',
                    'internalcode' => 'mmed."mimetype"',
                    'type' => 'string',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.url' => array(
                    'label' => 'Media URL',
                    'code' => 'media.url',
                    'internalcode' => 'mmed."link"',
                    'type' => 'string',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.preview' => array(
                    'label' => 'Media preview URL',
                    'code' => 'media.preview',
                    'internalcode' => 'mmed."preview"',
                    'type' => 'string',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.status' => array(
                    'label' => 'Media status',
                    'code' => 'media.status',
                    'internalcode' => 'mmed."status"',
                    'type' => 'integer',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
            ),
            'media.ctime'=> array(
                    'code'=>'media.ctime',
                    'internalcode'=>'mmed."ctime"',
                    'label'=>'Media create date/time',
                    'type'=> 'datetime',
                    'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.mtime'=> array(
                    'code'=>'media.mtime',
                    'internalcode'=>'mmed."mtime"',
                    'label'=>'Media modification date/time',
                    'type'=> 'datetime',
                    'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.editor'=> array(
                    'code'=>'media.editor',
                    'internalcode'=>'mmed."editor"',
                    'label'=>'Media editor',
                    'type'=> 'string',
                    'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.code' => array(
                    'label' => 'Media code',
                    'code' => 'media.code',
                    'internalcode' => 'mmed."code"',
                    'type' => 'string',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
            ),
            'media.status_solr' => array(
                    'label' => 'Status solr',
                    'code' => 'media.status_solr',
                    'internalcode' => 'mmed."status_solr"',
                    'type' => 'integer',
                    'internaltype' => MW_DB_Statement_Abstract::PARAM_INT
            )

    );

    /**
     * Adds a new item to the storage or updates an existing one.
     *
     * @param MShop_Media_Item_Interface $item New item that should be saved to the storage
     * @param boolean $fetch True if the new ID should be returned in the item
     */
    public function saveItem( MShop_Common_Item_Interface $item, $fetch = true )
    {
        $iface = 'MShop_Media_Item_Interface';
        if( !( $item instanceof $iface ) ) {
            throw new MShop_Media_Exception( sprintf( 'Object is not of required type "%1$s"', $iface ) );
        }

        if( !$item->isModified() ) { return; }

        $context = $this->_getContext();

        $dbm = $context->getDatabaseManager();
        $dbname = $this->_getResourceName();
        $conn = $dbm->acquire( $dbname );

        try
        {
            $id = $item->getId();
            $date = date( 'Y-m-d H:i:s' );

            if( $id === null )
            {
                $path = 'mshop/media/manager/default_ext/item/insert';
            }
            else
            {
                $path = 'mshop/media/manager/default_ext/item/update';
            }

            $stmt = $this->_getCachedStatement( $conn, $path );
            $stmt->bind( 1, $context->getLocale()->getSiteId(), MW_DB_Statement_Abstract::PARAM_INT );
            $stmt->bind( 2, $item->getLanguageId() );
            $stmt->bind( 3, $item->getTypeId(), MW_DB_Statement_Abstract::PARAM_INT );
            $stmt->bind( 4, $item->getLabel() );
            $stmt->bind( 5, $item->getMimeType() );
            $stmt->bind( 6, $item->getUrl() );
            $stmt->bind( 7, $item->getStatus(), MW_DB_Statement_Abstract::PARAM_INT );
            $stmt->bind( 8, $item->getDomain() );
            $stmt->bind( 9, $item->getPreview() );
            $stmt->bind( 10, $date ); // mtime
            $stmt->bind( 11, $context->getEditor() );
            $stmt->bind( 12, $item->getCode());
            $stmt->bind( 13, $item->getStatusSolr(), MW_DB_Statement_Abstract::PARAM_INT );

            if( $id !== null ) {
                $stmt->bind( 14, $id, MW_DB_Statement_Abstract::PARAM_INT );
                $item->setId( $id ); //is not modified anymore
            } else {
                $stmt->bind( 14, $date ); // ctime
            }

            $stmt->execute()->finish();

            if( $id === null && $fetch === true )
            {
                $path = 'mshop/media/manager/default/item/newid';
                $item->setId( $this->_newId( $conn, $context->getConfig()->get( $path, $path ) ) );
            }

            $dbm->release( $conn, $dbname );
        }
        catch( Exception $e )
        {
            $dbm->release( $conn, $dbname );
            throw $e;
        }
    }


    /**
     * Returns the item objects matched by the given search criteria.
     *
     * @param MW_Common_Criteria_Interface $search Search criteria object
     * @param array $ref List of domains to fetch list items and referenced items for
     * @param integer &$total Number of items that are available in total
     * @return array List of items implementing MShop_Media_Item_Interface
     * @throws MShop_Media_Exception If creating items failed
     */
    public function searchItems( MW_Common_Criteria_Interface $search, array $ref = array(), &$total = null )
    {
        $map = $typeIds = array();
        $context = $this->_getContext();

        $dbm = $context->getDatabaseManager();
        $dbname = $this->_getResourceName();
        $conn = $dbm->acquire( $dbname );

        try
        {
            $required = array( 'media' );
            $level = MShop_Locale_Manager_Abstract::SITE_ALL;
            $cfgPathSearch = 'mshop/media/manager/default_ext/item/search';
            $cfgPathCount = 'mshop/media/manager/default_ext/item/count';

            $results = $this->_searchItems( $conn, $search, $cfgPathSearch, $cfgPathCount, $required, $total, $level );

            while( ( $row = $results->fetch() ) !== false )
            {
                $map[$row['id']] = $row;
                $typeIds[$row['typeid']] = null;
            }

            $dbm->release( $conn, $dbname );
        }
        catch( Exception $e )
        {
            $dbm->release( $conn, $dbname );
            throw $e;
        }

        if( !empty( $typeIds ) )
        {
            $typeManager = $this->getSubManager( 'type' );
            $typeSearch = $typeManager->createSearch();
            $typeSearch->setConditions( $typeSearch->compare( '==', 'media.type.id', array_keys( $typeIds ) ) );
            $typeSearch->setSlice( 0, $search->getSliceSize() );
            $typeItems = $typeManager->searchItems( $typeSearch );

            foreach( $map as $id => $row )
            {
                if( isset( $typeItems[$row['typeid']] ) ) {
                    $map[$id]['type'] = $typeItems[$row['typeid']]->getCode();
                }
            }
        }

        return $this->_buildItems( $map, $ref, 'media' );
    }


    /**
     * Returns the attributes that can be used for searching.
     *
     * @param boolean $withsub Return also attributes of sub-managers if true
     * @return array List of attribute items implementing MW_Common_Criteria_Attribute_Interface
     */
    public function getSearchAttributes( $withsub = true )
    {
        $path = 'classes/media/manager/submanagers';
    
        return $this->_getSearchAttributes( $this->_searchConfig, $path, array( 'type', 'list' ), $withsub );
    }

    /**
     * Creates a new media item instance.
     *
     * @param array $values Associative list of key/value pairs
     * @param array $listItems List of items implementing MShop_Common_Item_List_Interface
     * @param array $refItems List of items reference to this item
     * @return MShop_Media_Item_Interface New product item
     */
    protected function _createItem( array $values = array(), array $listItems = array(), array $refItems = array() )
    {
        return new MShop_Media_Item_DefaultExt( $values, $listItems, $refItems );
    }
}
