<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package MShop
 * @subpackage Attributegroup
 */


/**
 * Default attributegroup list type manager for creating and handling attributegroup list type items.
 * @package MShop
 * @subpackage Attributegroup
 */
class MShop_Attributegroup_Manager_List_Type_Default
	extends MShop_Common_Manager_Type_Abstract
	implements MShop_Attributegroup_Manager_List_Type_Interface
{
	private $_searchConfig = array(
		'attributegroup.list.type.id' => array(
			'code'=>'attributegroup.list.type.id',
			'internalcode'=>'mcatlity."id"',
			'internaldeps'=>array( 'LEFT JOIN "mshop_attributegroup_list_type" as mcatlity ON ( mcatli."typeid" = mcatlity."id" )' ),
			'label'=>'attributegroup list type ID',
			'type'=> 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'attributegroup.list.type.siteid' => array(
			'code'=>'attributegroup.list.type.siteid',
			'internalcode'=>'mcatlity."siteid"',
			'label'=>'attributegroup list type site ID',
			'type'=> 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
			'public' => false,
		),
		'attributegroup.list.type.code' => array(
			'code'=>'attributegroup.list.type.code',
			'internalcode'=>'mcatlity."code"',
			'label'=>'attributegroup list type code',
			'type'=> 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.type.domain' => array(
			'code'=>'attributegroup.list.type.domain',
			'internalcode'=>'mcatlity."domain"',
			'label'=>'attributegroup list type domain',
			'type'=> 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.type.label' => array(
			'code' => 'attributegroup.list.type.label',
			'internalcode' => 'mcatlity."label"',
			'label' => 'attributegroup list type label',
			'type' => 'string',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.type.status' => array(
			'code' => 'attributegroup.list.type.status',
			'internalcode' => 'mcatlity."status"',
			'label' => 'attributegroup list type status',
			'type' => 'integer',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_INT,
		),
		'attributegroup.list.type.ctime'=> array(
			'label' => 'attributegroup list type creation time',
			'code' => 'attributegroup.list.type.ctime',
			'internalcode' => 'mcatlity."ctime"',
			'type' => 'datetime',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.type.mtime'=> array(
			'label' => 'attributegroup list type modification time',
			'code' => 'attributegroup.list.type.mtime',
			'internalcode' => 'mcatlity."mtime"',
			'type' => 'datetime',
			'internaltype' => MW_DB_Statement_Abstract::PARAM_STR,
		),
		'attributegroup.list.type.editor'=> array(
			'code'=>'attributegroup.list.type.editor',
			'internalcode'=>'mcatlity."editor"',
			'label'=>'attributegroup list type editor',
			'type'=> 'string',
			'internaltype'=> MW_DB_Statement_Abstract::PARAM_STR,
		),
	);


	/**
	 * Initializes the object.
	 *
	 * @param MShop_Context_Item_Interface $context Context object
	 */
	public function __construct( MShop_Context_Item_Interface $context )
	{
		parent::__construct( $context );
		$this->_setResourceName( 'db-attributegroup' );
	}


	/**
	 * Removes old entries from the storage.
	 *
	 * @param array $siteids List of IDs for sites whose entries should be deleted
	 */
	public function cleanup( array $siteids )
	{
		$path = 'classes/attributegroup/manager/list/type/submanagers';
		foreach( $this->_getContext()->getConfig()->get( $path, array() ) as $domain ) {
			$this->getSubManager( $domain )->cleanup( $siteids );
		}

		$this->_cleanup( $siteids, 'mshop/attributegroup/manager/list/type/default/item/delete' );
	}


	/**
	 * Returns the attributes that can be used for searching.
	 *
	 * @param boolean $withsub Return also attributegroups of sub-managers if true
	 * @return array List of attributegroup items implementing MW_Common_Criteria_Attribute_Interface
	 */
	public function getSearchAttributes( $withsub = true )
	{
		$path = 'classes/attributegroup/manager/list/type/submanagers';

		return $this->_getSearchAttributes( $this->_searchConfig, $path, array(), $withsub );
	}


	/**
	 * Returns a new manager for attributegroup list type extensions.
	 *
	 * @param string $manager Name of the sub manager type in lower case
	 * @param string|null $name Name of the implementation, will be from configuration (or Default) if null
	 * @return MShop_Common_Manager_Interface Manager for different extensions, e.g types, lists etc.
	 */
	public function getSubManager( $manager, $name = null )
	{

		return $this->_getSubManager( 'attributegroup', 'list/type/' . $manager, $name );
	}


	/**
	 * Returns the config path for retrieving the configuration values.
	 *
	 * @return string Configuration path
	 */
	protected function _getConfigPath()
	{

		return 'mshop/attributegroup/manager/list/type/default/item/';
	}


	/**
	 * Returns the search configuration for searching items.
	 *
	 * @return array Associative list of search keys and search definitions
	 */
	protected function _getSearchConfig()
	{
		return $this->_searchConfig;
	}
}