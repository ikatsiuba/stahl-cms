<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2011
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @package Controller
 * @subpackage ExtJS
 */


/**
 * attributegroup list type controller factory.
 *
 * @package Controller
 * @subpackage ExtJS
 */
class Controller_ExtJS_AttributeGroup_List_Type_Factory
    extends Controller_ExtJS_Common_Factory_Abstract
    implements Controller_ExtJS_Common_Factory_Interface
{
    public static function createController( MShop_Context_Item_Interface $context, $name = null )
    {
        if( $name === null ) {
            $name = $context->getConfig()->get( 'classes/controller/extjs/attributegroup/list/type/name', 'Default' );
        }

        if( ctype_alnum( $name ) === false )
        {
            $classname = is_string( $name ) ? 'Controller_ExtJS_AttributeGroup_List_Type_' . $name : '<not a string>';
            throw new Controller_ExtJS_Exception( sprintf( 'Invalid class name "%1$s"', $classname ) );
        }

        $iface = 'Controller_ExtJS_Common_Interface';
        $classname = 'Controller_ExtJS_AttributeGroup_List_Type_' . $name;

        $controller = self::_createController( $context, $classname, $iface );

        return self::_addControllerDecorators( $context, $controller, 'attributegroup/list/type' );
    }
}
