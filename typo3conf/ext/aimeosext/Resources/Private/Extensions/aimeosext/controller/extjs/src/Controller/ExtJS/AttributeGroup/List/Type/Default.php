<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @package Controller
 * @subpackage ExtJS
 */


/**
 * ExtJS attributegroup list type controller for admin interfaces.
 *
 * @package Controller
 * @subpackage ExtJS
 */
class Controller_ExtJS_AttributeGroup_List_Type_Default
    extends Controller_ExtJS_Abstract
    implements Controller_ExtJS_Common_Interface
{
    private $_manager = null;


    /**
     * Initializes the attributegroup list type controller.
     *
     * @param MShop_Context_Item_Interface $context MShop context object
     */
    public function __construct( MShop_Context_Item_Interface $context )
    {
        parent::__construct( $context, 'AttributeGroup_List_Type' );
    }


    /**
     * Returns the manager the controller is using.
     *
     * @return MShop_Common_Manager_Interface Manager object
     */
    protected function _getManager()
    {
        if( $this->_manager === null ) {
            $this->_manager = MShop_Factory::createManager( $this->_getContext(), 'attributegroup/list/type' );
        }

        return $this->_manager;
    }


    /**
     * Returns the prefix for searching items
     *
     * @return string MShop search key prefix
     */
    protected function _getPrefix()
    {
        return 'attributegroup.list.type';
    }
}
