<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package Controller
 * @subpackage ExtJS
 */


/**
 * ExtJS attributegroup text import controller factory.
 *
 * @package Controller
 * @subpackage ExtJS
 */
class Controller_ExtJS_AttributeGroup_Import_Text_Factory
    extends Controller_ExtJS_Common_Factory_Abstract
    implements Controller_ExtJS_Common_Factory_Interface
{
    public static function createController( MShop_Context_Item_Interface $context, $name = null )
    {
        if( $name === null ) {
            $name = $context->getConfig()->get( 'classes/controller/extjs/attributegroup/import/text/name', 'Default' );
        }

        if( ctype_alnum( $name ) === false )
        {
            $classname = is_string( $name ) ? 'Controller_ExtJS_AttributeGroup_Import_Text_' . $name : '<not a string>';
            throw new Controller_ExtJS_Exception( sprintf( 'Invalid class name "%1$s"', $classname ) );
        }

        $iface = 'Controller_ExtJS_Common_Load_Text_Interface';
        $classname = 'Controller_ExtJS_AttributeGroup_Import_Text_' . $name;

        return self::_createController( $context, $classname, $iface );
    }
}
