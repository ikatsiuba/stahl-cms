<?php

/**
 * @copyright AgiliWay Group, Inc. (info@agiliway.com), 2015
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package Controller
 * @subpackage ExtJS
 */


/**
 * AttributeGroup controller factory.
 *
 * @package Controller
 * @subpackage ExtJS
 */
class Controller_ExtJS_AttributeGroup_Factory
    extends Controller_ExtJS_Common_Factory_Abstract
    implements Controller_ExtJS_Common_Factory_Interface
{
    public static function createController( MShop_Context_Item_Interface $context, $name = null )
    {
        if( $name === null ) {
            $name = $context->getConfig()->get( 'classes/controller/extjs/attributegroup/name', 'Default' );
        }

        if( ctype_alnum( $name ) === false )
        {
            $classname = is_string( $name ) ? 'Controller_ExtJS_AttributeGroup_' . $name : '<not a string>';
            throw new Controller_ExtJS_Exception( sprintf( 'Invalid class name "%1$s"', $classname ) );
        }

        $iface = 'Controller_ExtJS_Common_Interface';
        $classname = 'Controller_ExtJS_AttributeGroup_' . $name;

        $controller = self::_createController( $context, $classname, $iface );

        return self::_addControllerDecorators( $context, $controller, 'attributegroup' );
    }
}
