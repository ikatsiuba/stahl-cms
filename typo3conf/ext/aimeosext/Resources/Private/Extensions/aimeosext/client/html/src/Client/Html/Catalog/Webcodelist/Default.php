<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2012
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package Client
 * @subpackage Html
 */


/**
 * Default implementation of catalog list section HTML clients.
 *
 * @package Client
 * @subpackage Html
 */
class Client_Html_Catalog_Webcodelist_Default
	extends Client_Html_Catalog_List_DefaultExt
	implements Client_Html_Common_Client_Factory_Interface
{

	private $_subPartPath = 'client/html/catalog/webcodelist/default/subparts';
	private $_subPartNames = array();

	private $_tags = array();
	private $_expire;
	private $_cache;


	/**
	 * Returns the HTML code for insertion into the body.
	 *
	 * @param string $uid Unique identifier for the output if the content is placed more than once on the same page
	 * @param array &$tags Result array for the list of tags that are associated to the output
	 * @param string|null &$expire Result variable for the expiration date of the output (null for no expiry)
	 * @return string HTML code
	 */
	public function getBody( $uid = '', array &$tags = array(), &$expire = null )
	{
		$prefixes = array( 'f','d','l');

		/** client/html/catalog/list
		 * All parameters defined for the catalog list component and its subparts
		 *
		 * This returns all settings related to the filter component.
		 * Please refer to the single settings for details.
		 *
		 * @param array Associative list of name/value settings
		 * @category Developer
		 * @see client/html/catalog#list
		 */
		$confkey = 'client/html/catalog/webcodelist';

		if( ( $html = $this->_getCached( 'body', $uid, $prefixes, $confkey ) ) === null )
		{
			$context = $this->_getContext();
			$view = $this->getView();

			try
			{
				$view = $this->_setViewParams( $view, $tags, $expire );

				$html = '';
				foreach( $this->_getSubClients() as $subclient ) {
					$html .= $subclient->setView( $view )->getBody( $uid, $tags, $expire );
				}
				$view->listBody = $html;
			}
			catch( Client_Html_Exception $e )
			{
				$error = array( $context->getI18n()->dt( 'client/html', $e->getMessage() ) );
				$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
			}
			catch( Controller_Frontend_Exception $e )
			{
				$error = array( $context->getI18n()->dt( 'controller/frontend', $e->getMessage() ) );
				$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
			}
			catch( MShop_Exception $e )
			{
				$error = array( $context->getI18n()->dt( 'mshop', $e->getMessage() ) );
				$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
			}
			catch( Exception $e )
			{
				$context->getLogger()->log( $e->getMessage() . PHP_EOL . $e->getTraceAsString() );

				$error = array( $context->getI18n()->dt( 'client/html', 'A non-recoverable error occured' ) );
				$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
			}

			$tplconf = 'client/html/catalog/webcodelist/default/template-body';
			$default = 'catalog/webcodelist/body-default.html';

			$html = $view->render( $this->_getTemplate( $tplconf, $default ) );

			$this->_setCached( 'body', $uid, $prefixes, $confkey, $html, $tags, $expire );
		}
		else
		{
			$html = $this->modifyBody( $html, $uid );
		}

		return $html;
	}


	/**
	 * Returns the HTML string for insertion into the header.
	 *
	 * @param string $uid Unique identifier for the output if the content is placed more than once on the same page
	 * @param array &$tags Result array for the list of tags that are associated to the output
	 * @param string|null &$expire Result variable for the expiration date of the output (null for no expiry)
	 * @return string|null String including HTML tags for the header on error
	 */
	public function getHeader( $uid = '', array &$tags = array(), &$expire = null )
	{
		$prefixes = array( 'f', 'd', 'l');
		$confkey = 'client/html/catalog/webcodelist';

		if( ( $html = $this->_getCached( 'header', $uid, $prefixes, $confkey ) ) === null )
		{
			$view = $this->getView();

			try
			{
				$view = $this->_setViewParams( $view, $tags, $expire );

				$html = '';
				foreach( $this->_getSubClients() as $subclient ) {
					$html .= $subclient->setView( $view )->getHeader( $uid, $tags, $expire );
				}
				$view->listHeader = $html;

				$tplconf = 'client/html/catalog/webcodelist/default/template-header';
				$default = 'catalog/webcodelist/header-default.html';

				$html = $view->render( $this->_getTemplate( $tplconf, $default ) );

				$this->_setCached( 'header', $uid, $prefixes, $confkey, $html, $tags, $expire );
			}
			catch( Exception $e )
			{
				$this->_getContext()->getLogger()->log( $e->getMessage() . PHP_EOL . $e->getTraceAsString() );
			}
		}
		else
		{
			$html = $this->modifyHeader( $html, $uid );
		}

		return $html;
	}


	/**
	 * Returns the sub-client given by its name.
	 *
	 * @param string $type Name of the client type
	 * @param string|null $name Name of the sub-client (Default if null)
	 * @return Client_Html_Interface Sub-client object
	 */
	public function getSubClient( $type, $name = null )
	{
		return $this->_createSubClient( 'catalog/webcodelist/' . $type, $name );
	}


	/**
	 * Processes the input, e.g. store given values.
	 * A view must be available and this method doesn't generate any output
	 * besides setting view variables.
	 */
	public function process()
	{
		$context = $this->_getContext();
		$view = $this->getView();

		try
		{
			$site = $context->getLocale()->getSite()->getCode();
			$params = $this->_getClientParams( $view->param() );
			$context->getSession()->set( 'arcavias/catalog/webcodelist/params/last/' . $site, $params );

			parent::process();
		}
		catch( Client_Html_Exception $e )
		{
			$error = array( $context->getI18n()->dt( 'client/html', $e->getMessage() ) );
			$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
		}
		catch( Controller_Frontend_Exception $e )
		{
			$error = array( $context->getI18n()->dt( 'controller/frontend', $e->getMessage() ) );
			$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
		}
		catch( MShop_Exception $e )
		{
			$error = array( $context->getI18n()->dt( 'mshop', $e->getMessage() ) );
			$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
		}
		catch( Exception $e )
		{
			$context->getLogger()->log( $e->getMessage() . PHP_EOL . $e->getTraceAsString() );

			$error = array( $context->getI18n()->dt( 'client/html', 'A non-recoverable error occured' ) );
			$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
		}
	}


	/**
	 * Returns the list of sub-client names configured for the client.
	 *
	 * @return array List of HTML client names
	 */
	protected function _getSubClientNames()
	{
		return $this->_getContext()->getConfig()->get( $this->_subPartPath, $this->_subPartNames );
	}


	/**
	 * Sets the necessary parameter values in the view.
	 *
	 * @param MW_View_Interface $view The view object which generates the HTML output
	 * @param array &$tags Result array for the list of tags that are associated to the output
	 * @param string|null &$expire Result variable for the expiration date of the output (null for no expiry)
	 * @return MW_View_Interface Modified view object
	 */
	protected function _setViewParams( MW_View_Interface $view, array &$tags = array(), &$expire = null )
	{
		if( !isset( $this->_cache ) )
		{
			$context = $this->_getContext();
			$config = $context->getConfig();
			$view->langCode = $context->getLocale()->getLanguageId();
			$controller = Controller_Frontend_Factory::createController( $context, 'catalog' );

 			$config->set('mshop/catalog/manager/index/default/item/search',$config->get('mshop/catalog/manager/index/default_ext/item_ps_wc/search'));
 			$config->set('mshop/catalog/manager/index/default/item/count',$config->get('mshop/catalog/manager/index/default_ext/item_ps_wc/count'));
	
			$manager = $controller->createManager('catalog/index' );
			
			$locale = $this->_getContext()->getLocale();
			$langid = $locale->getLanguageId();

			$itemData = array();
			$categoryIds = $view->param('f_catid');

			if(empty($categoryIds)){
                $categoryIds = [];
			    $categoryIds[]=0; 
			}
            $tree = $controller->getCatalogTree( $categoryIds, array());
            $ids = array($categoryIds);
            $categoryIds = $this->_getCatalogIds($tree, $ids);
            $view->direction = '-';
            $view->sort = 'name';

			if(!empty($categoryIds) || $view->param('f_search')){
    			$view->listPageCurr = $this->_getProductListPage( $view );
    			$view->listPageSize = $this->_getProductListSize( $view );
    			if($view->param('f_catid')){
                    $view->listPageCurr = 1;
    			    $view->listPageSize = 10000;
                }
    				
    			$search = $manager->createSearch( true );
    			$listtype = 'default';
    			$direction = '-';
                $sort =  $this->_getProductListSortByParam([
                    'f_sort_wc'=>$view->param('f_sort_wc'),
                    'f_search'=>$view->param('f_search')
                ],$direction);
    			$expr = $sortations = array();

    			//if($view->param('f_catid')){
    			    $expr[] = $search->compare( '==', 'catalog.index.catalog.id', $categoryIds );
    			//}

    			if($view->param('f_search')){
    			    $expr[] = $search->compare( '>', $search->createFunction( 'catalog.index.text.relevance', array( $listtype, $langid, $view->param('f_search') ) ), 0 );
    			    $expr[] = $search->compare( '>', $search->createFunction( 'catalog.index.text.value', array( $listtype, $langid, array('AT_WebCode','TitleProductSubSeries'), 'product' ) ), '' );
    			}else{
                    $expr[] = $search->compare( '>', $search->createFunction( 'catalog.index.text.value', array( $listtype, $langid, array('TitleProductSubSeries'), 'product' ) ), '' );
    			}

    			switch ($sort){
                    case 'webcode':
                        $sortfunc = $search->createFunction( 'sort:catalog.index.text.value.webcode', array( $listtype, $langid) );
                        break;
                    default:
                        $sortfunc = $search->createFunction( 'sort:catalog.index.text.value', array( $listtype, $langid) );
                }
    			$sortations[] = $search->sort( $direction, $sortfunc );


    			$search->setSortations( $sortations );
    		    $search->setConditions( $search->combine( '&&', $expr ) );
    			$search->setSlice(( $view->listPageCurr - 1 ) * $view->listPageSize, $view->listPageSize );
    			$total = 0;
    			$itemData = $manager->searchItems($search,array('text'), $total);

                if(!empty($itemData)){
        			$manager = $controller->createManager('catalog/list' );
        			$search = $manager->createSearch( true );
        			$expr = array(
        			             $search->compare( '==', 'catalog.list.refid', array_keys($itemData)),
        			             $search->compare( '==', 'catalog.list.domain', 'product'),
        			             $search->compare( '==', 'catalog.list.type.code', 'default'),
        			);
        			$search->setConditions( $search->combine( '&&', $expr ) );
        			$catalogData = $manager->searchItems($search);

        			
        			$catalogList = array();
        			
        			$catalogManager= $controller->createManager('catalog');
        			$rootCatalogList = $config->get( 'client/html/catalog/webcodelist/category', array() );
        			foreach ($catalogData as $catalog){
        			    $path = array_keys($catalogManager->getPath($catalog->getParentId()));
        			    $catalogList[$catalog->getRefId()]['catalog']=end($path);
        			    $el= array_intersect($rootCatalogList, $path);
        			    end($el);
        			    $catalogList[$catalog->getRefId()]['target']=key($el);
        			}
        			
        			$view->catalogList = $catalogList;
                }
                if($catId=$view->param('f_catid')){

                    $catalogManager= $controller->createManager('catalog');
                    $rootCatalogList = $config->get( 'client/html/catalog/webcodelist/category', array() );
                    $path = $catalogManager->getPath($catId, array('text'));
                    $el= array_intersect($rootCatalogList, array_keys($path));
                    $start= end($el);
                    foreach ($path as $key => $cat){
                        if($key==$start){
                            break;
                        }else{
                            unset($path[$key]);
                        }
                    }
                    $view->categoryTarget=key($el);
                    $view->categoryWebcodePath=$path;
                }

    			$view->itemData = $itemData;
    			$view->itemTotal = $total;
    			
    			
    			
    			
    			$current = $view->listPageCurr;
    			$last = ( $total != 0 ? ceil( $total / $view->listPageSize ) : 1 );
    			
    			$view->pagiPageFirst = 1;
    			$view->pagiPagePrev = ( $current > 1 ? $current - 1 : 1 );
    			$view->pagiPageNext = ( $current < $last ? $current + 1 : $last );
    			$view->pagiPageLast = $last;

                $view->direction = $direction;
                $view->sort = $sort;

    			$this->_tags[] = 'webcodelist_'.$listType;

			}
			
			$this->_cache = $view;
		}

		$expire = $this->_expires( $this->_expire, $expire );
		$tags = array_merge( $tags, $this->_tags );

		return $this->_cache;
	}

    protected function _getProductListSortByParam( array $params, &$sortdir )
    {
        $sortation = ( isset( $params['f_sort_wc'] ) ? (string) $params['f_sort_wc'] : (isset($params['f_search']) && !empty($params['f_search']) ? 'name': 'webcode' ));
        $sortdir = ( $sortation[0] === '-' ? '-' : '+' );
        $sort = ltrim( $sortation, '-' );
        $sort = ltrim( $sort, '+' );
        return ( strlen( $sort ) > 0 ? $sort : 'name' );
    }


    /**
	 * Returns the category IDs of the given catalog tree.
	 *
	 * Only the IDs of the children of the current category are returned.
	 *
	 * @param MShop_Catalog_Item_Interface $tree Catalog node as entry point of the tree
	 * @param array $path Associative list of category IDs as keys and the catalog
	 * 	nodes from the currently selected category up to the root node
	 * @return array List of category IDs
	 */
 	protected function _getCatalogIds( MShop_Catalog_Item_Interface $tree, array &$ids )
	{
	    foreach( $tree->getChildren() as $item )
	    {
	        if($item->getStatus()==1){
	            $ids[] = $item->getId();
	        }
	        
	        if(	$item->getStatus() > 0 && count( $item->getChildren() ) ){
	            $this->_getCatalogIds( $item, $ids);
	        }
	    }
	    
	    return $ids;
	    
	}
}
