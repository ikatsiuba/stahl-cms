Ext.ns('MShop.panel.attributegroup');

MShop.panel.attributegroup.TreeUi = Ext.extend(MShop.panel.AbstractTreeUi, {

    rootVisible : true,
    useArrows : true,
    autoScroll : true,
    animate : true,
    enableDD : true,
    containerScroll : true,
    border : false,
    ddGroup : 'MShop.panel.attributegroup',
    maskDisabled : true,
    domain : 'attributegroup',

    recordName : 'AttributeGroup',
    idProperty : 'attributegroup.id',
    exportMethod : 'AttributeGroup_Export_Text.createJob',
    importMethod : 'AttributeGroup_Import_Text.uploadFile',


    initComponent : function() {
        this.title = MShop.I18n.dt('client/extjs', 'AttributeGroup');
        this.domain = 'attributegroup';
        MShop.panel.AbstractListUi.prototype.initActions.call(this);
        MShop.panel.AbstractListUi.prototype.initToolbar.call(this);

        this.recordClass = MShop.Schema.getRecord(this.recordName);

        this.initLoader(true);

        // fake a root -> needed by extjs
        this.root = new Ext.tree.AsyncTreeNode({
            id : 'root'
        });

        MShop.panel.attributegroup.TreeUi.superclass.initComponent.call(this);
    },

    inspectCreateNode : function(attr) {

        attr.id = attr['attributegroup.id'];
        attr.label = attr['attributegroup.label'];
        attr.text = attr['attributegroup.id'] + " - " + attr['attributegroup.label'];
        attr.code = attr['attributegroup.code'];
        attr.status = attr['attributegroup.status'];
        attr.cls = 'statustext-' + attr.status;

        // create record and insert into own store
        this.store.suspendEvents(false);

        this.store.remove(this.store.getById(attr.id));
        this.store.add([new this.recordClass(attr, attr.id)]);

        this.store.resumeEvents();
    }
});


// hook this into the main tab panel
Ext.ux.ItemRegistry.registerItem('MShop.panel.type.tabUi', 'MShop.panel.attributegroup.treeui', MShop.panel.attributegroup.TreeUi, 15);

Ext.ns('MShop.panel.attributegroup');

MShop.panel.attributegroup.ItemUi = Ext.extend(MShop.panel.AbstractTreeItemUi, {
    idProperty : 'id',
    siteidProperty : 'attributegroup.siteid',

    initComponent : function() {

        MShop.panel.AbstractItemUi.prototype.setSiteCheck(this);

        this.items = [{
            xtype : 'tabpanel',
            activeTab : 0,
            border : false,
            itemId : 'MShop.panel.attributegroup.ItemUi',
            plugins : ['ux.itemregistry'],
            items : [{
                xtype : 'panel',
                title : MShop.I18n.dt('client/extjs', 'Basic'),
                border : false,
                layout : 'hbox',
                layoutConfig : {
                    align : 'stretch'
                },
                itemId : 'MShop.panel.attributegroup.ItemUi.BasicPanel',
                plugins : ['ux.itemregistry'],
                defaults : {
                    bodyCssClass : this.readOnlyClass
                },
                items : [{
                    title : MShop.I18n.dt('client/extjs', 'Details'),
                    xtype : 'form',
                    flex : 1,
                    ref : '../../mainForm',
                    autoScroll : true,
                    items : [{
                        xtype : 'fieldset',
                        style : 'padding-right: 25px;',
                        border : false,
                        labelAlign : 'top',
                        defaults : {
                            readOnly : this.fieldsReadOnly,
                            anchor : '100%'
                        },
                        items : [{
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'ID'),
                            name : 'id'
                        }, {
                            xtype : 'MShop.elements.status.combo',
                            name : 'status'
                        }, {
                            xtype : 'textfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Code'),
                            name : 'code',
                            allowBlank : false,
                            maxLength : 32,
                            regex : /^[^ \v\t\r\n\f]+$/,
                            emptyText : MShop.I18n.dt('client/extjs', 'Unique code (required)')
                        }, {
                            xtype : 'textfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Label'),
                            name : 'label',
                            allowBlank : false,
                            emptyText : MShop.I18n.dt('client/extjs', 'Internal name (required)')
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Created'),
                            name : 'attributegroup.ctime'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Last modified'),
                            name : 'attributegroup.mtime'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Editor'),
                            name : 'attributegroup.editor'
                        }]
                    }]
                }, {
                    xtype : 'MShop.panel.configui',
                    layout : 'fit',
                    flex : 1,
                    data : (this.record ? this.record.get('attributegroup.config') : {})
                }]
            }]
        }];

        this.store.on('beforesave', this.onBeforeSave, this);

        MShop.panel.attributegroup.ItemUi.superclass.initComponent.call(this);
    },


    afterRender : function() {
        var label = this.record ? this.record.data['label'] : MShop.I18n.dt('client/extjs', 'new');
        //#: attributegroup item panel title with attributegroup label ({0}) and site code ({1)}
        var string = MShop.I18n.dt('client/extjs', 'Attributegroup: {0} ({1})');
        this.setTitle(String.format(string, label, MShop.config.site["locale.site.label"]));

        MShop.panel.attributegroup.ItemUi.superclass.afterRender.apply(this, arguments);
    },


    onBeforeSave : function(store, data) {
        MShop.panel.attributegroup.ItemUi.superclass.onBeforeSave.call(this, store, data, {
            configname : 'attributegroup.config'
        });
    },


    onSaveItem : function() {
        if(!this.mainForm.getForm().isValid() && this.fireEvent('validate', this) !== false) {
            Ext.Msg.alert(MShop.I18n.dt('client/extjs', 'Invalid data'), MShop.I18n.dt('client/extjs',
                'Please recheck your data'));
            return;
        }

        this.saveMask.show();
        this.isSaveing = true;

        this.record.dirty = true;

        if(this.fireEvent('beforesave', this, this.record) === false) {
            this.isSaveing = false;
            this.saveMask.hide();
        }

        this.record.beginEdit();
        this.record.set('attributegroup.label', this.mainForm.getForm().findField('label').getValue());
        this.record.set('attributegroup.status', this.mainForm.getForm().findField('status').getValue());
        this.record.set('attributegroup.code', this.mainForm.getForm().findField('code').getValue());
        this.record.endEdit();

        if(this.action == 'add' || this.action == 'copy') {
            this.store.add(this.record);
        }

        if(!this.store.autoSave) {
            this.onAfterSave();
        }
    }
});

Ext.reg('MShop.panel.attributegroup.itemui', MShop.panel.attributegroup.ItemUi);


Ext.ns('MShop.panel.attributegroup');

//hook product picker into the attributegroup ItemUi
Ext.ux.ItemRegistry.registerItem('MShop.panel.attributegroup.ItemUi', 'MShop.panel.attributegroup.ProductItemPickerUi', {
 xtype : 'MShop.panel.product.itempickerui',
 itemConfig : {
     recordName : 'AttributeGroup_List',
     idProperty : 'attributegroup.list.id',
     siteidProperty : 'attributegroup.list.siteid',
     listNamePrefix : 'attributegroup.list.',
     listTypeIdProperty : 'attributegroup.list.type.id',
     listTypeLabelProperty : 'attributegroup.list.type.label',
     listTypeControllerName : 'AttributeGroup_List_Type',
     listTypeCondition : {
         '&&' : [{
             '==' : {
                 'attributegroup.list.type.domain' : 'product'
             }
         }]
     },
     listTypeKey : 'attributegroup/list/type/product'
 },
 listConfig : {
     prefix : 'product.'
 }
}, 30);

Ext.ns('MShop.panel.text.type');

MShop.panel.text.type.ItemPickerUi = Ext.extend(MShop.panel.AbstractListItemPickerUi, {

    title : MShop.I18n.dt('client/extjs', 'Text'),

    initComponent : function() {

        Ext.apply(this.itemConfig, {
            title : MShop.I18n.dt('client/extjs', 'Associated texts'),
            xtype : 'MShop.panel.listitemlistui',
            domain : 'text/type',
            getAdditionalColumns : this.getAdditionalColumns.createDelegate(this)
        });

        Ext.apply(this.listConfig, {
            title : MShop.I18n.dt('client/extjs', 'Available texts'),
            xtype : 'MShop.panel.text.type.listui'
        });

        MShop.panel.text.type.ItemPickerUi.superclass.initComponent.call(this);
    },

    getAdditionalColumns : function() {

        var conf = this.itemConfig;
        this.typeStore = MShop.GlobalStoreMgr.get('Text_Type', conf.domain);
        this.listTypeStore = MShop.GlobalStoreMgr.get(conf.listTypeControllerName, conf.domain);

        return [
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'typeid',
                header : MShop.I18n.dt('client/extjs', 'List type'),
                id : 'listtype',
                width : 70,
                renderer : this.typeColumnRenderer.createDelegate(this,
                    [this.listTypeStore, conf.listTypeLabelProperty], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Status'),
                id : 'refstatus',
                width : 50,
                align : 'center',
                renderer : this.refStatusColumnRenderer.createDelegate(this, ['text.type.status'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Domain'),
                id : 'refdomain',
                renderer : this.refColumnRenderer.createDelegate(this, ['text.type.domain'], true)
            },            
/*            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Type'),
                id : 'reftype',
                width : 70,
                renderer : this.refTypeColumnRenderer.createDelegate(this, [
                    this.typeStore,
                    'text.typeid',
                    'text.type.label'], true)
            },
*/            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Code'),
                id : 'refcode',
                width : 80,
                renderer : this.refColumnRenderer.createDelegate(this, ['text.type.code'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Label'),
                id : 'refcontent',
                renderer : this.refColumnRenderer.createDelegate(this, ['text.type.label'], true)
            }];
    }
});

Ext.reg('MShop.panel.text.type.itempickerui', MShop.panel.text.type.ItemPickerUi);


Ext.ns('MShop.panel.attributegroup');

// hook text picker into the attributegroup ItemUi
Ext.ux.ItemRegistry.registerItem('MShop.panel.attributegroup.ItemUi', 'MShop.panel.attributegroup.TextItemPickerUi', {
    xtype : 'MShop.panel.text.type.itempickerui',
    itemConfig : {
        recordName : 'AttributeGroup_List',
        idProperty : 'attributegroup.list.id',
        siteidProperty : 'attributegroup.list.siteid',
        listNamePrefix : 'attributegroup.list.',
        listTypeIdProperty : 'attributegroup.list.type.id',
        listTypeLabelProperty : 'attributegroup.list.type.label',
        listTypeControllerName : 'AttributeGroup_List_Type',
        listTypeCondition : {
            '&&' : [{
                '==' : {
                    'attributegroup.list.type.domain' : 'text/type'
                }
            }]
        },
        listTypeKey : 'attributegroup/list/type/text'
    },
    listConfig : {
//        domain : 'attributegroup',
        prefix : 'text.type.'
    }
}, 10);


Ext.ns('MShop.panel.attribute.type');

MShop.panel.attribute.type.ItemPickerUi = Ext.extend(MShop.panel.AbstractListItemPickerUi, {

    title : MShop.I18n.dt('client/extjs', 'Attribute'),

    initComponent : function() {

        Ext.apply(this.itemConfig, {
            title : MShop.I18n.dt('client/extjs', 'Associated attributes'),
            xtype : 'MShop.panel.listitemlistui',
            domain : 'attribute/type',
            getAdditionalColumns : this.getAdditionalColumns.createDelegate(this)
        });

        Ext.apply(this.listConfig, {
            title : MShop.I18n.dt('client/extjs', 'Available attributes'),
            xtype : 'MShop.panel.attribute.type.listui'
        });

        MShop.panel.attribute.type.ItemPickerUi.superclass.initComponent.call(this);
    },

    getAdditionalColumns : function() {

        var conf = this.itemConfig;
        this.typeStore = MShop.GlobalStoreMgr.get('Attribute_Type', conf.domain);
        this.listTypeStore = MShop.GlobalStoreMgr.get(conf.listTypeControllerName, conf.domain);

        return [
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'typeid',
                header : MShop.I18n.dt('client/extjs', 'List type'),
                id : 'listtype',
                width : 70,
                renderer : this.typeColumnRenderer.createDelegate(this,
                    [this.listTypeStore, conf.listTypeLabelProperty], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Status'),
                id : 'refstatus',
                width : 50,
                align : 'center',
                renderer : this.refStatusColumnRenderer.createDelegate(this, ['attribute.type.status'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Domain'),
                id : 'refdomain',
                renderer : this.refColumnRenderer.createDelegate(this, ['attribute.type.domain'], true)
            },            
/*            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Type'),
                id : 'reftype',
                width : 70,
                renderer : this.refTypeColumnRenderer.createDelegate(this, [
                    this.typeStore,
                    'attribute.typeid',
                    'attribute.type.label'], true)
            },
*/            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Code'),
                id : 'refcode',
                width : 80,
                renderer : this.refColumnRenderer.createDelegate(this, ['attribute.type.code'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Label'),
                id : 'refcontent',
                renderer : this.refColumnRenderer.createDelegate(this, ['attribute.type.label'], true)
            }];
    }
});

Ext.reg('MShop.panel.attribute.type.itempickerui', MShop.panel.attribute.type.ItemPickerUi);



Ext.ns('MShop.panel.attributegroup');

// hook attribute picker into the attributegroup ItemUi
Ext.ux.ItemRegistry.registerItem('MShop.panel.attributegroup.ItemUi', 'MShop.panel.attributegroup.AttributeItemPickerUi', {
    xtype : 'MShop.panel.attribute.type.itempickerui',
    itemConfig : {
        recordName : 'AttributeGroup_List',
        idProperty : 'attributegroup.list.id',
        siteidProperty : 'attributegroup.list.siteid',
        listNamePrefix : 'attributegroup.list.',
        listTypeIdProperty : 'attributegroup.list.type.id',
        listTypeLabelProperty : 'attributegroup.list.type.label',
        listTypeControllerName : 'AttributeGroup_List_Type',
        listTypeCondition : {
            '&&' : [{
                '==' : {
                    'attributegroup.list.type.domain' : 'attribute/type'
                }
            }]
        },
        listTypeKey : 'attributegroup/list/type/attribute'
    },
    listConfig : {
//        domain : 'attributegroup',
        prefix : 'attribute.type.'
    }
}, 50);

Ext.ns('MShop.panel');

MShop.panel.AbstractTypeItemUi = Ext.extend(MShop.panel.AbstractItemUi, {
    typeDomain : 'text.type',

    initComponent : function() {
        MShop.panel.AbstractItemUi.prototype.setSiteCheck(this);
        this.items = [{
            xtype : 'tabpanel',
            activeTab : 0,
            border : false,
            itemId : 'MShop.panel.' + this.typeDomain + '.ItemUi',
            plugins : ['ux.itemregistry'],
            items : [{
                xtype : 'panel',
                title : MShop.I18n.dt('client/extjs', 'Basic'),
                border : false,
                layout : 'hbox',
                layoutConfig : {
                    align : 'stretch'
                },
                itemId : 'MShop.panel.' + this.typeDomain + '.ItemUi.BasicPanel',
                plugins : ['ux.itemregistry'],
                defaults : {
                    bodyCssClass : this.readOnlyClass
                },
                items : [{
                    xtype : 'form',
                    flex : 1,
                    ref : '../../mainForm',
                    autoScroll : true,
                    items : [{
                        xtype : 'fieldset',
                        style : 'padding-right: 25px;',
                        border : false,
                        labelAlign : 'top',
                        defaults : {
                            anchor : '100%',
                            readOnly : this.fieldsReadOnly
                        },
                        items : [{
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'ID'),
                            name : this.typeDomain + '.id'
                        }, {
                            xtype : 'MShop.elements.status.combo',
                            name : this.typeDomain + '.status',
                            allowBlank : false
                        }, {
                            xtype : 'MShop.elements.domain.combo',
                            name : this.typeDomain + '.domain',
                            allowBlank : false
                        }, {
                            xtype : 'textfield',
                            name : this.typeDomain + '.code',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Code'),
                            allowBlank : false,
                            maxLength : 32,
                            regex : /^[^ \v\t\r\n\f]+$/,
                            emptyText : MShop.I18n.dt('client/extjs', 'Unique code (required)')
                        }, {
                            xtype : 'textfield',
                            name : this.typeDomain + '.label',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Label'),
                            allowBlank : false,
                            maxLength : 255,
                            emptyText : MShop.I18n.dt('client/extjs', 'Internal name (required)')
                        }, {
                            xtype : 'textfield',
                            name : this.typeDomain + '.web_style',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Web style'),
                            maxLength : 255
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Created'),
                            name : this.typeDomain + '.ctime'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Last modified'),
                            name : this.typeDomain + '.mtime'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Editor'),
                            name : this.typeDomain + '.editor'
                        }]
                    }]
                }]
            }]
        }];

        MShop.panel.AbstractTypeItemUi.superclass.initComponent.call(this);
    }
});
Ext.reg('MShop.panel.abstracttypeitemui', MShop.panel.AbstractTypeItemUi);

Ext.ns('MShop.panel.text.type');
MShop.panel.text.type.ListUi = Ext.extend(MShop.panel.AbstractListUi, {

    recordName : 'Text_Type',
    idProperty : 'text.type.id',
    siteidProperty : 'text.type.siteid',
    itemUiXType : 'MShop.panel.text.type.itemui',

    // Sort by id ASC
    sortInfo : {
        field : 'text.type.id',
        direction : 'ASC'
    },

    // Create filter
    filterConfig : {
        filters : [{
            dataIndex : 'text.type.label',
            operator : '=~',
            value : ''
        }]
    },

    // Override initComponent to set Label of tab.
    initComponent : function() {
        this.title = MShop.I18n.dt('client/extjs', 'Text type');

        MShop.panel.AbstractListUi.prototype.initActions.call(this);
        MShop.panel.AbstractListUi.prototype.initToolbar.call(this);

        MShop.panel.text.type.ListUi.superclass.initComponent.call(this);
    },

    autoExpandColumn : 'text-type-label',

    getColumns : function() {
        return [{
            xtype : 'gridcolumn',
            dataIndex : 'text.type.id',
            header : MShop.I18n.dt('client/extjs', 'ID'),
            sortable : true,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'text.type.status',
            header : MShop.I18n.dt('client/extjs', 'Status'),
            sortable : true,
            width : 50,
            align : 'center',
            renderer : this.statusColumnRenderer.createDelegate(this)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'text.type.domain',
            header : MShop.I18n.dt('client/extjs', 'Domain'),
            sortable : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'text.type.code',
            header : MShop.I18n.dt('client/extjs', 'Code'),
            sortable : true,
            width : 150,
            align : 'center',
            editable : false
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'text.type.label',
            id : 'text-type-label',
            header : MShop.I18n.dt('client/extjs', 'Label'),
            sortable : true,
            editable : false
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'text.type.web_style',
            header : MShop.I18n.dt('client/extjs', 'Web style'),
            sortable: true
        }, {
            xtype : 'datecolumn',
            dataIndex : 'text.type.ctime',
            header : MShop.I18n.dt('client/extjs', 'Created'),
            sortable : true,
            width : 130,
            format : 'Y-m-d H:i:s',
            editable : false,
            hidden : true
        }, {
            xtype : 'datecolumn',
            dataIndex : 'text.type.mtime',
            header : MShop.I18n.dt('client/extjs', 'Last modified'),
            sortable : true,
            width : 130,
            format : 'Y-m-d H:i:s',
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'text.type.editor',
            header : MShop.I18n.dt('client/extjs', 'Editor'),
            sortable : true,
            width : 130,
            editable : false,
            hidden : true
        }];
    }
});

Ext.reg('MShop.panel.text.type.listui', MShop.panel.text.type.ListUi);
Ext.ux.ItemRegistry.registerItem('MShop.panel.type.tabUi', 'MShop.panel.text.type.listui',
    MShop.panel.text.type.ListUi, 70);

Ext.ns('MShop.panel.text.type');

MShop.panel.text.type.ItemUi = Ext.extend(MShop.panel.AbstractTypeItemUi, {
    siteidProperty : 'text.type.siteid',
    typeDomain : 'text.type',

    initComponent : function() {
        MShop.panel.AbstractTypeItemUi.prototype.setSiteCheck(this);
        MShop.panel.text.type.ItemUi.superclass.initComponent.call(this);
    },

    afterRender : function() {
        var label = this.record ? this.record.data['text.type.label'] : MShop.I18n.dt('client/extjs', 'new');
        //#: Text type item panel title with type label ({0}) and site code ({1)}
        var string = MShop.I18n.dt('client/extjs', 'Text type: {0} ({1})');
        this.setTitle(String.format(string, label, MShop.config.site["locale.site.label"]));

        MShop.panel.text.type.ItemUi.superclass.afterRender.apply(this, arguments);
    }
});

Ext.reg('MShop.panel.text.type.itemui', MShop.panel.text.type.ItemUi);
Ext.reg('MShop.panel.type.tabui', MShop.panel.type.TabUi);
Ext.ux.ItemRegistry.registerItem('MShop.MainTabPanel', 'MShop.panel.type.tabui', MShop.panel.type.TabUi, 120);



Ext.ns('MShop.panel');

MShop.panel.AbstractTypeItemUi = Ext.extend(MShop.panel.AbstractItemUi, {
    /**
     * Domain to configure fields E.g. attribute.type
     */
    typeDomain : 'attribute.type',

    initComponent : function() {
        MShop.panel.AbstractItemUi.prototype.setSiteCheck(this);
        this.items = [{
            xtype : 'tabpanel',
            activeTab : 0,
            border : false,
            itemId : 'MShop.panel.' + this.typeDomain + '.ItemUi',
            plugins : ['ux.itemregistry'],
            items : [{
                xtype : 'panel',
                title : MShop.I18n.dt('client/extjs', 'Basic'),
                border : false,
                layout : 'hbox',
                layoutConfig : {
                    align : 'stretch'
                },
                itemId : 'MShop.panel.' + this.typeDomain + '.ItemUi.BasicPanel',
                plugins : ['ux.itemregistry'],
                defaults : {
                    bodyCssClass : this.readOnlyClass
                },
                items : [{
                    xtype : 'form',
                    flex : 1,
                    ref : '../../mainForm',
                    autoScroll : true,
                    items : [{
                        xtype : 'fieldset',
                        style : 'padding-right: 25px;',
                        border : false,
                        labelAlign : 'top',
                        defaults : {
                            anchor : '100%',
                            readOnly : this.fieldsReadOnly
                        },
                        items : [{
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'ID'),
                            name : this.typeDomain + '.id'
                        }, {
                            xtype : 'MShop.elements.status.combo',
                            name : this.typeDomain + '.status',
                            allowBlank : false
                        }, {
                            xtype : 'MShop.elements.domain.combo',
                            name : this.typeDomain + '.domain',
                            allowBlank : false
                        }, {
                            xtype : 'textfield',
                            name : this.typeDomain + '.code',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Code'),
                            allowBlank : false,
                            maxLength : 32,
                            regex : /^[^ \v\t\r\n\f]+$/,
                            emptyText : MShop.I18n.dt('client/extjs', 'Unique code (required)')
                        }, {
                            xtype : 'textfield',
                            name : this.typeDomain + '.label',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Label'),
                            allowBlank : false,
                            maxLength : 255,
                            emptyText : MShop.I18n.dt('client/extjs', 'Internal name (required)')
                        }, {
                            xtype : 'textfield',
                            name : this.typeDomain + '.web_filter',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Web filter'),
                            maxLength : 255
                        }, {
                            xtype : 'textfield',
                            name : this.typeDomain + '.web_style',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Web style'),
                            maxLength : 255
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Created'),
                            name : this.typeDomain + '.ctime'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Last modified'),
                            name : this.typeDomain + '.mtime'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Editor'),
                            name : this.typeDomain + '.editor'
                        }]
                    }]
                }]
            }]
        }];

        MShop.panel.AbstractTypeItemUi.superclass.initComponent.call(this);
    }
});
Ext.reg('MShop.panel.abstracttypeitemui', MShop.panel.AbstractTypeItemUi);


Ext.ns('MShop.panel.attribute.type');

MShop.panel.attribute.type.ListUi = Ext.extend(MShop.panel.AbstractListUi, {

    recordName : 'Attribute_Type',
    idProperty : 'attribute.type.id',
    siteidProperty : 'attribute.type.siteid',

    itemUiXType : 'MShop.panel.attribute.type.itemui',

    // Sort by id ASC
    sortInfo : {
        field : 'attribute.type.id',
        direction : 'ASC'
    },

    // Create filter
    filterConfig : {
        filters : [{
            dataIndex : 'attribute.type.label',
            operator : '=~',
            value : ''
        }]
    },

    // Override initComponent to set Label of tab.
    initComponent : function() {
        this.title = MShop.I18n.dt('client/extjs', 'Attribute type');

        MShop.panel.AbstractListUi.prototype.initActions.call(this);
        MShop.panel.AbstractListUi.prototype.initToolbar.call(this);

        MShop.panel.attribute.type.ListUi.superclass.initComponent.call(this);
    },


    autoExpandColumn : 'attribute-type-label',

    getColumns : function() {
        return [{
            xtype : 'gridcolumn',
            dataIndex : 'attribute.type.id',
            header : MShop.I18n.dt('client/extjs', 'ID'),
            sortable : true,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'attribute.type.status',
            header : MShop.I18n.dt('client/extjs', 'Status'),
            sortable : true,
            width : 50,
            align : 'center',
            renderer : this.statusColumnRenderer.createDelegate(this)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'attribute.type.domain',
            header : MShop.I18n.dt('client/extjs', 'Domain'),
            sortable : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'attribute.type.code',
            header : MShop.I18n.dt('client/extjs', 'Code'),
            sortable : true,
            width : 150,
            align : 'center',
            editable : false
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'attribute.type.label',
            id : 'attribute-type-label',
            header : MShop.I18n.dt('client/extjs', 'Label'),
            sortable : true,
            editable : false
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'attribute.type.web_filter',
            header : MShop.I18n.dt('client/extjs', 'Web filter'),
            sortable : true,
            hidden : false
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'attribute.type.web_style',
            header : MShop.I18n.dt('client/extjs', 'Web style'),
            sortable : true,
            hidden : false
        }, {
            xtype : 'datecolumn',
            dataIndex : 'attribute.type.ctime',
            header : MShop.I18n.dt('client/extjs', 'Created'),
            sortable : true,
            width : 130,
            format : 'Y-m-d H:i:s',
            editable : false,
            hidden : true
        }, {
            xtype : 'datecolumn',
            dataIndex : 'attribute.type.mtime',
            header : MShop.I18n.dt('client/extjs', 'Last modified'),
            sortable : true,
            width : 130,
            format : 'Y-m-d H:i:s',
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'attribute.type.editor',
            header : MShop.I18n.dt('client/extjs', 'Editor'),
            sortable : true,
            width : 130,
            editable : false,
            hidden : true
        }];
    }
});

Ext.reg('MShop.panel.attribute.type.listui', MShop.panel.attribute.type.ListUi);

Ext.ux.ItemRegistry.registerItem('MShop.panel.type.tabUi', 'MShop.panel.attribute.type.listui',
    MShop.panel.attribute.type.ListUi, 10);

Ext.ns('MShop.panel.attribute.type');

MShop.panel.attribute.type.ItemUi = Ext.extend(MShop.panel.AbstractTypeItemUi, {

    siteidProperty : 'attribute.type.siteid',

    typeDomain : 'attribute.type',

    initComponent : function() {
        MShop.panel.AbstractTypeItemUi.prototype.setSiteCheck(this);
        MShop.panel.attribute.type.ItemUi.superclass.initComponent.call(this);
    },

    afterRender : function() {
        var label = this.record ? this.record.data['attribute.type.label'] : MShop.I18n.dt('client/extjs', 'new');
        //#: Attribute type item panel title with type label ({0}) and site code ({1)}
        var string = MShop.I18n.dt('client/extjs', 'Attribute type: {0} ({1})');
        this.setTitle(String.format(string, label, MShop.config.site["locale.site.label"]));

        MShop.panel.attribute.type.ItemUi.superclass.afterRender.apply(this, arguments);
    }
});

Ext.reg('MShop.panel.attribute.type.itemui', MShop.panel.attribute.type.ItemUi);

delete Ext.ux.ItemRegistry.itemMap['MShop.MainTabPanel']['MShop.panel.customer.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.MainTabPanel']['MShop.panel.order.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.MainTabPanel']['MShop.panel.plugin.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.MainTabPanel']['MShop.panel.service.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.MainTabPanel']['MShop.panel.stock.warehouse.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.MainTabPanel']['MShop.panel.supplier.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.MainTabPanel']['MShop.panel.coupon.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.panel.product.ItemUi']['MShop.panel.product.PriceItemPickerUi'];
/*delete Ext.ux.ItemRegistry.itemMap['MShop.panel.product.ItemUi']['MShop.panel.product.stock.listui'];*/
delete Ext.ux.ItemRegistry.itemMap['MShop.panel.type.tabUi']['MShop.panel.plugin.type.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.panel.type.tabUi']['MShop.panel.price.type.listui'];
delete Ext.ux.ItemRegistry.itemMap['MShop.panel.type.tabUi']['MShop.panel.service.type.listui'];


Ext.ns('MShop.panel.filter.type');

MShop.panel.filter.type.ListUi = Ext.extend(MShop.panel.AbstractListUi, {

    recordName : 'Filter_Type',
    idProperty : 'filter.type.id',
    siteidProperty : 'filter.type.siteid',

    itemUiXType : 'MShop.panel.filter.type.itemui',

    // Sort by id ASC
    sortInfo : {
        field : 'filter.type.id',
        direction : 'ASC'
    },

    // Create filter
    filterConfig : {
        filters : [{
            dataIndex : 'filter.type.label',
            operator : '=~',
            value : ''
        }]
    },

    // Override initComponent to set Label of tab.
    initComponent : function() {
        this.title = MShop.I18n.dt('client/extjs', 'Filter type');

        MShop.panel.AbstractListUi.prototype.initActions.call(this);
        MShop.panel.AbstractListUi.prototype.initToolbar.call(this);

        MShop.panel.filter.type.ListUi.superclass.initComponent.call(this);
    },


    autoExpandColumn : 'filter-type-label',

    getColumns : function() {
        return [{
            xtype : 'gridcolumn',
            dataIndex : 'filter.type.id',
            header : MShop.I18n.dt('client/extjs', 'ID'),
            sortable : true,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.type.status',
            header : MShop.I18n.dt('client/extjs', 'Status'),
            sortable : true,
            width : 50,
            align : 'center',
            renderer : this.statusColumnRenderer.createDelegate(this)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.type.domain',
            header : MShop.I18n.dt('client/extjs', 'Domain'),
            sortable : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.type.code',
            header : MShop.I18n.dt('client/extjs', 'Code'),
            sortable : true,
            width : 150,
            align : 'center',
            editable : false
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.type.label',
            id : 'filter-type-label',
            header : MShop.I18n.dt('client/extjs', 'Label'),
            sortable : true,
            editable : false
        }, {
            xtype : 'datecolumn',
            dataIndex : 'filter.type.ctime',
            header : MShop.I18n.dt('client/extjs', 'Created'),
            sortable : true,
            width : 130,
            format : 'Y-m-d H:i:s',
            editable : false,
            hidden : true
        }, {
            xtype : 'datecolumn',
            dataIndex : 'filter.type.mtime',
            header : MShop.I18n.dt('client/extjs', 'Last modified'),
            sortable : true,
            width : 130,
            format : 'Y-m-d H:i:s',
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.type.editor',
            header : MShop.I18n.dt('client/extjs', 'Editor'),
            sortable : true,
            width : 130,
            editable : false,
            hidden : true
        }];
    }
});

Ext.reg('MShop.panel.filter.type.listui', MShop.panel.filter.type.ListUi);

Ext.ux.ItemRegistry.registerItem('MShop.panel.type.tabUi', 'MShop.panel.filter.type.listui',
    MShop.panel.filter.type.ListUi, 25);
/*!
 * Copyright (c) Metaways Infosystems GmbH, 2014
 * LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @author Michael Spahn <m.spahn@metaways.de>
 */


Ext.ns('MShop.panel.filter.type');

MShop.panel.filter.type.ItemUi = Ext.extend(MShop.panel.AbstractTypeItemUi, {
    siteidProperty : 'filter.type.siteid',
    typeDomain : 'filter.type',

    initComponent : function() {
        MShop.panel.AbstractTypeItemUi.prototype.setSiteCheck(this);
        MShop.panel.filter.type.ItemUi.superclass.initComponent.call(this);
    },

    afterRender : function() {
        var label = this.record ? this.record.data['filter.type.label'] : MShop.I18n.dt('client/extjs', 'new');
        //#: Media type item panel title with type label ({0}) and site code ({1)}
        var string = MShop.I18n.dt('client/extjs', 'filter type: {0} ({1})');
        this.setTitle(String.format(string, label, MShop.config.site["locale.site.label"]));

        MShop.panel.filter.type.ItemUi.superclass.afterRender.apply(this, arguments);
    }
});

Ext.reg('MShop.panel.filter.type.itemui', MShop.panel.filter.type.ItemUi);

Ext.ns('MShop.panel.filter');

MShop.panel.filter.ListUi = Ext.extend(MShop.panel.AbstractListUi, {

    recordName : 'Filter',
    idProperty : 'filter.id',
    siteidProperty : 'filter.siteid',
    itemUiXType : 'MShop.panel.filter.itemui',
//    exportMethod : 'Filter_Export_Text.createJob',
//    importMethod : 'Filter_Import_Text.uploadFile',

    autoExpandColumn : 'filter-list-label',

    filterConfig : {
        filters : [{
        	dataIndex : 'filter.label',
            operator : '=~',
            value : ''
        }]
    },

    initComponent : function() {
        this.title = MShop.I18n.dt('client/extjs', 'Filter');

        MShop.panel.AbstractListUi.prototype.initActions.call(this);
        MShop.panel.AbstractListUi.prototype.initToolbar.call(this);

        MShop.panel.filter.ListUi.superclass.initComponent.call(this);
        var storeConfig = {
                baseParams : {
                    site : MShop.config.site["locale.site.code"],
                    condition : {
                        '&&' : [{
                            '==' : {
                                'attribute.type.domain' : 'product'
                            }
                        },{
                            '==' : {
                                'attribute.type.web_filter' : '1'
                            }
                        }]
                    }
                }
            };
        var attributeStore = MShop.GlobalStoreMgr.get('Attribute_Type','product/filter',storeConfig);
    },
    
    attributeTypeColumnRenderer : function(attributeId, metaData, record, rowIndex, colIndex, store, typeStore, displayField) {
        var type = typeStore.getById(attributeId);
        return type ? type.get(displayField) : attributeId;
    },

    getColumns : function() {
        // make sure filter type store gets loaded in same batch as this grid data
        this.typeStore = MShop.GlobalStoreMgr.get('Filter_Type');
        this.attributeTypeStore = MShop.GlobalStoreMgr.get('Attribute_Type');

        return [{
            xtype : 'gridcolumn',
            dataIndex : 'filter.id',
            header : MShop.I18n.dt('client/extjs', 'ID'),
            sortable : true,
            width : 50,
            editable : false,
            hidden : false
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.status',
            header : MShop.I18n.dt('client/extjs', 'Status'),
            sortable : true,
            width : 50,
            align : 'center',
            renderer : this.statusColumnRenderer.createDelegate(this)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.typeid',
            header : MShop.I18n.dt('client/extjs', 'Type'),
            width : 100,
            renderer : this.typeColumnRenderer.createDelegate(this, [this.typeStore, "filter.type.label"], true)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.code',
            header : MShop.I18n.dt('client/extjs', 'Code'),
            sortable : true,
            width : 255,
            editable : false
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.webtype',
            header : MShop.I18n.dt('client/extjs', 'Web Type1'),
            sortable : true,
            width : 100,
            editable : false,
            renderer : MShop.elements.webtype.renderer
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.attribute',
            header : MShop.I18n.dt('client/extjs', 'Attribute Code'),
            sortable : true,
            width : 100,
            renderer : this.attributeTypeColumnRenderer.createDelegate(this, [this.attributeTypeStore, "attribute.type.code"], true)
            //editable : false
        }, /*{
            xtype : 'gridcolumn',
            dataIndex : 'filter.subattribute',
            header : MShop.I18n.dt('client/extjs', 'SubAttribute Code'),
            sortable : true,
            width : 100,
            renderer : this.attributeTypeColumnRenderer.createDelegate(this, [this.attributeTypeStore, "attribute.type.code"], true)
        },*/ {
            xtype : 'gridcolumn',
            dataIndex : 'filter.parentid',
            header : MShop.I18n.dt('client/extjs', 'Parent Id'),
            sortable : true,
            width : 100,
            editable : false,
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.position',
            header : MShop.I18n.dt('client/extjs', 'Position'),
            sortable : true,
            width : 100,
            editable : false,
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.label',
            header : MShop.I18n.dt('client/extjs', 'Label'),
            sortable : true,
            width : 10,
            editable : false,
            id : 'filter-list-label'
        }, {
            xtype : 'datecolumn',
            dataIndex : 'filter.ctime',
            header : MShop.I18n.dt('client/extjs', 'Created'),
            sortable : true,
            width : 130,
            format : 'Y-m-d H:i:s',
            editable : false,
            hidden : true
        }, {
            xtype : 'datecolumn',
            dataIndex : 'filter.mtime',
            header : MShop.I18n.dt('client/extjs', 'Last modified'),
            sortable : true,
            width : 130,
            format : 'Y-m-d H:i:s',
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.editor',
            header : MShop.I18n.dt('client/extjs', 'Editor'),
            sortable : true,
            width : 130,
            editable : false,
            hidden : true
        }];
    }
});

Ext.reg('MShop.panel.filter.listui', MShop.panel.filter.ListUi);

// hook this into the main tab panel
Ext.ux.ItemRegistry.registerItem('MShop.MainTabPanel', 'MShop.panel.filter.listui', MShop.panel.filter.ListUi, 25);

Ext.ns('MShop.panel.filter');

MShop.panel.filter.ItemUi = Ext.extend(MShop.panel.AbstractListItemUi, {

    siteidProperty : 'filter.siteid',

    initComponent : function() {

        var storeConfig = {
                baseParams : {
                    site : MShop.config.site["locale.site.code"],
                    condition : {
                        '&&' : [{
                            '==' : {
                                'attribute.type.domain' : 'product'
                            }
                        },{
                            '==' : {
                                'attribute.type.web_filter' : '1'
                            }
                        }]
                    }
                }
            };
    	var attributeStore = MShop.GlobalStoreMgr.get('Attribute_Type','product/filter',storeConfig);
        MShop.panel.AbstractListItemUi.prototype.setSiteCheck(this);

        this.items = [{
            xtype : 'tabpanel',
            activeTab : 0,
            border : false,
            itemId : 'MShop.panel.filter.ItemUi',
            plugins : ['ux.itemregistry'],
            items : [{
                xtype : 'panel',
                title : MShop.I18n.dt('client/extjs', 'Basic'),
                border : false,
                layout : 'hbox',
                layoutConfig : {
                    align : 'stretch'
                },
                itemId : 'MShop.panel.filter.ItemUi.BasicPanel',
                plugins : ['ux.itemregistry'],
                defaults : {
                    bodyCssClass : this.readOnlyClass
                },
                items : [{
                    xtype : 'form',
                    title : MShop.I18n.dt('client/extjs', 'Details'),
                    flex : 1,
                    ref : '../../mainForm',
                    autoScroll : true,
                    items : [{
                        xtype : 'fieldset',
                        style : 'padding-right: 25px;',
                        border : false,
                        labelAlign : 'top',
                        defaults : {
                            readOnly : this.fieldsReadOnly,
                            anchor : '100%'
                        },
                        items : [{
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'ID'),
                            name : 'filter.id'
                        }, {
                            xtype : 'MShop.elements.status.combo',
                            name : 'filter.status'
                        }, {
                            xtype : 'combo',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Type'),
                            name : 'filter.typeid',
                            mode : 'local',
                            store : MShop.GlobalStoreMgr.get('Filter_Type'),
                            displayField : 'filter.type.label',
                            valueField : 'filter.type.id',
                            forceSelection : true,
                            triggerAction : 'all',
                            allowBlank : false,
                            typeAhead : true,
                            listeners : {
                                'render' : {
                                    fn : function() {
                                        var record, index = this.store.find('filter.type.code', 'default');
                                        if(record = this.store.getAt(index)) {
                                            this.setValue(record.id);
                                        }
                                    }
                                },
                                'select': {
                                    fn:function(combo, record, index){
                                        var subel =Ext.getCmp('hierarchical_filter');
                                        var val = this.store.getAt(index);
                                        if(val && val.data['filter.type.code']=='global'){
                                            subel.show();
                                        }else{
                                            subel.hide();
                                        }
                                    }
                                }
                            }
                        }, {
                            xtype : 'textfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Code'),
                            name : 'filter.code',
                            allowBlank : false,
                            maxLength : 255,
                            regex : /^[^ \v\t\r\n\f]+$/,
                            emptyText : MShop.I18n.dt('client/extjs', 'code (required)')
                        }, {
                            xtype : 'textarea',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Label'),
                            name : 'filter.label',
                            allowBlank : false,
                            maxLength : 255,
                            emptyText : MShop.I18n.dt('client/extjs', 'Internal name (required)')
                        }, {
                            xtype : 'MShop.elements.webtype.combo',
                            name : 'filter.webtype'
                        },/* {
                            xtype : 'textfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Attribute Code'),
                            name : 'filter.attribute',
                            allowBlank : false,
                            maxLength : 100,
                            regex : /^[^ \v\t\r\n\f]+$/,
                            emptyText : MShop.I18n.dt('client/extjs', 'Attribute code (required)')
                        },*/ {
                            xtype : 'combo',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Attribute'),
                            name : 'filter.attribute',
                            mode : 'local',
                            store : attributeStore,
                            displayField : 'attribute.type.code',
                            valueField : 'attribute.type.id',
                            forceSelection : true,
                            triggerAction : 'all',
                            allowBlank : false,
                            typeAhead : true,
                            listeners : {
                                'render' : {
                                    fn : function() {
                                        var record, index = this.store.find('attribute.type.code', 'default');
                                        if((record = this.store.getAt(index))) {
                                            this.setValue(record.id);
                                        }
                                    }
                                }
                            }
                        },{
                            xtype : 'combo',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'SubAttribute'),
                            name : 'filter.subattribute',
                            id: 'filter.subattribute',
                            mode : 'local',
                            store : attributeStore,
                            displayField : 'attribute.type.code',
                            valueField : 'attribute.type.id',
                            forceSelection : true,
                            triggerAction : 'all',
                            allowBlank : true,
                            typeAhead : true,
                            hidden : (this.record && this.record.get('filter.webtype')=='slider2') ? false:true,
                            listeners : {
                                'render' : {
                                    fn : function() {
                                        var record, index = this.store.find('attribute.type.code', 'default');
                                        if((record = this.store.getAt(index))) {
                                            this.setValue(record.id);
                                        }
                                    }
                                }
                            }
                        },{
                            xtype:'fieldset',
                            checkboxToggle:true,
                            title: 'Hierarchical Filter',
                            id: 'hierarchical_filter',
                            style : 'padding-right: 25px;',
                            border : true,
                            labelAlign : 'top',
                            defaults : {
                                readOnly : this.fieldsReadOnly,
                                anchor : '100%'
                            },
                            autoHeight:true,
                            hidden : (this.record && this.record.get('filter.typeid')=='2') ? false : true,
                            defaultType: 'textfield',
                            collapsed: (this.record && (this.record.get('filter.parentid')||this.record.get('filter.values'))) ? false : true,
                            items :[ {
		                            	xtype : 'combo',
		                                fieldLabel : MShop.I18n.dt('client/extjs', 'Parent Filter'),
		                                name : 'filter.parentid',
		                                mode : 'local',
		                                store : MShop.GlobalStoreMgr.get('Filter'),
		                                displayField : 'filter.label',
		                                valueField : 'filter.id',
		                                forceSelection : false,
		                                triggerAction : 'all',
		                                allowBlank : true,
		                                typeAhead : true,
		                                listeners : {
		                                    'render' : {
		                                        fn : function() {
		                                            var record, index = this.store.find('filter.type.code', 'global');
		                                            if((record = this.store.getAt(index))) {
		                                                this.setValue(record.id);
		                                            }
		                                        }
		                                    }
		                                }
			                        }, {
			                            fieldLabel : MShop.I18n.dt('client/extjs', 'Key list'),
			                            name : 'filter.keys',
			                            allowBlank : true,
			                            //maxLength : 400,
			                            emptyText : MShop.I18n.dt('client/extjs', 'Semicolon list')
			                        }, {
			                            fieldLabel : MShop.I18n.dt('client/extjs', 'Values list'),
			                            name : 'filter.values',
			                            allowBlank : true,
			                            //maxLength : 400,
			                            emptyText : MShop.I18n.dt('client/extjs', 'Semicolon list')
			                        } ]
                        }, {
                            xtype : 'MShop.elements.cond.combo',
                            name : 'filter.condition'
                        }, {
                            xtype : 'MShop.elements.bool.combo',
                            name : 'filter.brackets'
                        }, {
                            xtype : 'MShop.elements.dir.combo',
                            name : 'filter.direction'
                        }, {
                            xtype : 'textfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Position'),
                            allowBlank : true,
                            name : 'filter.position'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Created'),
                            name : 'filter.ctime'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Last modified'),
                            name : 'filter.mtime'
                        }, {
                            xtype : 'displayfield',
                            fieldLabel : MShop.I18n.dt('client/extjs', 'Editor'),
                            name : 'filter.editor'
                        }]
                    }]
                }, {
                    xtype : 'MShop.panel.configui',
                    layout : 'fit',
                    flex : 1,
                    data : (this.record ? this.record.get('filter.config') : {})
                }]
            }]
        }];

        this.store.on('beforesave', this.onBeforeSave, this);

        MShop.panel.filter.ItemUi.superclass.initComponent.call(this);
    },

    afterRender : function() {
        var label = this.record ? this.record.data['filter.label'] : MShop.I18n.dt('client/extjs', 'new');
        //#: filter item panel title with filter label ({0}) and site code ({1)}
        var string = MShop.I18n.dt('client/extjs', 'Filter: {0} ({1})');
        this.setTitle(String.format(string, label, MShop.config.site["locale.site.label"]));

        MShop.panel.filter.ItemUi.superclass.afterRender.apply(this, arguments);
    },

    onBeforeSave : function(store, data) {
        MShop.panel.filter.ItemUi.superclass.onBeforeSave.call(this, store, data, {
            configname : 'filter.config'
        });
    },

    onStoreWrite : function(store, action, result, transaction, rs) {

        var records = Ext.isArray(rs) ? rs : [rs];
        var ids = [];

        MShop.panel.filter.ItemUi.superclass.onStoreWrite.apply(this, arguments);

        for( var i = 0; i < records.length; i++) {
            ids.push(records[i].id);
        }

        MShop.API.Filter.finish(MShop.config.site["locale.site.code"], ids);
    }
});

Ext.reg('MShop.panel.filter.itemui', MShop.panel.filter.ItemUi);

Ext.ns('MShop.elements.webtype');

MShop.elements.webtype.ComboBox = function(config) {
    Ext.applyIf(config, {
        fieldLabel : MShop.I18n.dt('client/extjs', 'Web Type'),
        anchor : '100%',
        store : MShop.elements.webtype.getStore(),
        mode : 'local',
        displayField : 'label',
        valueField : 'value',
        triggerAction : 'all',
        typeAhead : true,
        value : 'checkbox',
        listeners: {
            select: {
                fn:function(combo, record, index){
                    var subel =Ext.getCmp('filter.subattribute');
                    if(record.get('value')=='slider2'){
                        subel.show();
                    }else{
                        subel.hide();
                    }
                }
            }
        }
    
    });

    MShop.elements.webtype.ComboBox.superclass.constructor.call(this, config);
};

Ext.extend(MShop.elements.webtype.ComboBox, Ext.form.ComboBox);

Ext.reg('MShop.elements.webtype.combo', MShop.elements.webtype.ComboBox);

/**
 * @static
 * @return {Ext.data.DirectStore}
 */
MShop.elements.webtype.getStore = function() {

    if(!MShop.elements.webtype._store) {

        MShop.elements.webtype._store = new Ext.data.ArrayStore({
            idIndex : 'checkbox',
            fields : [{
                name : 'value',
                type : 'string'
            }, {
                name : 'label',
                type : 'string'
            }],
            data : [
                ['slider2', MShop.I18n.dt('client/extjs', 'slider 2 attribute')],
                ['checkbox', MShop.I18n.dt('client/extjs', 'Multi-select')],
                ['radio', MShop.I18n.dt('client/extjs', 'Select')],
                //['dropdown', MShop.I18n.dt('client/extjs', 'dropdown')],
                ['slider', MShop.I18n.dt('client/extjs', 'Slider')]]
        });
    }

    return MShop.elements.webtype._store;
};

MShop.elements.webtype.renderer = function(value) {

	var store = MShop.elements.webtype.getStore();
    var data = store.getAt(store.find('value', value));

    if(data) {
        return data.get('label');
    }

    return value;
};


Ext.ns('MShop.elements.dir');

MShop.elements.dir.ComboBox = function(config) {
    Ext.applyIf(config, {
        fieldLabel : MShop.I18n.dt('client/extjs', 'Sort Direction'),
        anchor : '100%',
        store : MShop.elements.dir.getStore(),
        mode : 'local',
        allowBlank : true,
        displayField : 'label',
        valueField : 'value',
        triggerAction : 'all',
        typeAhead : true,
        value : ''
    });

    MShop.elements.dir.ComboBox.superclass.constructor.call(this, config);
};

Ext.extend(MShop.elements.dir.ComboBox, Ext.form.ComboBox);

Ext.reg('MShop.elements.dir.combo', MShop.elements.dir.ComboBox);

/**
 * @static
 * @return {Ext.data.DirectStore}
 */
MShop.elements.dir.getStore = function() {

    if(!MShop.elements.dir._store) {

        MShop.elements.dir._store = new Ext.data.ArrayStore({
            idIndex : 'radio',
            fields : [{
                name : 'value',
                type : 'string'
            }, {
                name : 'label',
                type : 'string'
            }],
            data : [
                ['-', MShop.I18n.dt('client/extjs', 'desc')],
                ['+', MShop.I18n.dt('client/extjs', 'asc')]]
        });
    }

    return MShop.elements.dir._store;
};

Ext.ns('MShop.elements.cond');

/**
 * @static
 * @return {Ext.data.DirectStore}
 */
MShop.elements.cond.getStore = function() {

    if(!MShop.elements.cond._store) {

        MShop.elements.cond._store = new Ext.data.ArrayStore({
            idIndex : 'radio',
            fields : [{
                name : 'value',
                type : 'string'
            }, {
                name : 'label',
                type : 'string'
            }],
            data : [
                ['<=', MShop.I18n.dt('client/extjs', '<=')],
                ['>=', MShop.I18n.dt('client/extjs', '>=')],
                ['', MShop.I18n.dt('client/extjs', '-empty-')]],
        });
    }

    return MShop.elements.cond._store;
};

MShop.elements.cond.ComboBox = function(config) {
    Ext.applyIf(config, {
        fieldLabel : MShop.I18n.dt('client/extjs', 'Condition'),
        anchor : '100%',
        store : MShop.elements.cond.getStore(),
        mode : 'local',
        allowBlank : true,
        displayField : 'label',
        valueField : 'value',
        triggerAction : 'all',
        typeAhead : true,
        value : ''
    });

    MShop.elements.cond.ComboBox.superclass.constructor.call(this, config);
};

Ext.extend(MShop.elements.cond.ComboBox, Ext.form.ComboBox);

Ext.reg('MShop.elements.cond.combo', MShop.elements.cond.ComboBox);


Ext.ns('MShop.elements.bool');

/**
 * @static
 * @return {Ext.data.DirectStore}
 */
MShop.elements.bool.getStore = function() {

    if(!MShop.elements.bool._store) {

        MShop.elements.bool._store = new Ext.data.ArrayStore({
            idIndex : 'radio',
            fields : [{
                name : 'value',
                type : 'int'
            }, {
                name : 'label',
                type : 'string'
            }],
            data : [
                [1, MShop.I18n.dt('client/extjs', 'Yes')],
                [0, MShop.I18n.dt('client/extjs', 'No')]]
        });
    }

    return MShop.elements.bool._store;
};

MShop.elements.bool.ComboBox = function(config) {
    Ext.applyIf(config, {
        fieldLabel : MShop.I18n.dt('client/extjs', 'Skip values with brackets'),
        anchor : '100%',
        store : MShop.elements.bool.getStore(),
        mode : 'local',
        allowBlank : true,
        displayField : 'label',
        valueField : 'value',
        triggerAction : 'all',
        typeAhead : true,
        value : 0
    });

    MShop.elements.bool.ComboBox.superclass.constructor.call(this, config);
};

Ext.extend(MShop.elements.bool.ComboBox, Ext.form.ComboBox);

Ext.reg('MShop.elements.bool.combo', MShop.elements.bool.ComboBox);




Ext.ns('MShop.panel.filter');

MShop.panel.filter.ListUiSmall = Ext.extend(MShop.panel.AbstractListUi, {

    recordName : 'Filter',
    idProperty : 'filter.id',
    siteidProperty : 'filter.siteid',
    itemUiXType : 'MShop.panel.filter.itemui',

    autoExpandColumn : 'filter-list-label',

    filterConfig : {
        filters : [{
            dataIndex : 'filter.label',
            operator : '=~',
            value : ''
        }]
    },
    
    onBeforeLoad : function(store, options) {
        this.setSiteParam(store);

        if(this.domain) {
            this.setDomainFilter(store, options);
        }

        options.params = options.params || {};
        options.params.condition = {
            '&&' : [{
                '==' : {
                    'filter.type.code' : 'default'
                }
            }]
        };

    },
    getColumns : function() {
        // make sure type store gets loaded in same batch as this grid data
        this.typeStore = MShop.GlobalStoreMgr.get('Filter_Type', this.domain);
        this.itemTypeStore = MShop.GlobalStoreMgr.get('Filter_Type', this.domain);

        return [{
            xtype : 'gridcolumn',
            dataIndex : 'filter.id',
            header : MShop.I18n.dt('client/extjs', 'ID'),
            sortable : true,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.status',
            header : MShop.I18n.dt('client/extjs', 'Status'),
            sortable : true,
            width : 40,
            align : 'center',
            renderer : this.statusColumnRenderer.createDelegate(this)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.typeid',
            header : MShop.I18n.dt('client/extjs', 'Type'),
            width : 120,
            renderer : this.typeColumnRenderer.createDelegate(this, [this.typeStore, "filter.type.label"], true)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.webtype',
            header : MShop.I18n.dt('client/extjs', 'Web Type'),
            sortable : true,
            width : 70
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.label',
            header : MShop.I18n.dt('client/extjs', 'Label'),
            width : 50,
            sortable : true,
            id : 'filter-list-label'
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.ctime',
            header : MShop.I18n.dt('client/extjs', 'Created'),
            sortable : true,
            width : 120,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.mtime',
            header : MShop.I18n.dt('client/extjs', 'Last modified'),
            sortable : true,
            width : 120,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.editor',
            header : MShop.I18n.dt('client/extjs', 'Editor'),
            sortable : true,
            width : 120,
            editable : false,
            hidden : true
        }];
    },
});

Ext.reg('MShop.panel.filter.listuismall', MShop.panel.filter.ListUiSmall);


Ext.ns('MShop.panel.filter');

MShop.panel.filter.ItemPickerUi = Ext.extend(MShop.panel.AbstractListItemPickerUi, {

    title : MShop.I18n.dt('client/extjs', 'Filter'),

    initComponent : function() {

        Ext.apply(this.itemConfig, {
            title : MShop.I18n.dt('client/extjs', 'Associated filters'),
            xtype : 'MShop.panel.listitemlistuifilter',
            domain : 'filter',
            getAdditionalColumns : this.getAdditionalColumns.createDelegate(this)
        });

        Ext.apply(this.listConfig, {
            title : MShop.I18n.dt('client/extjs', 'Available filters'),
            xtype : 'MShop.panel.filter.listuismall'
        });

        MShop.panel.filter.ItemPickerUi.superclass.initComponent.call(this);
    },

    getAdditionalColumns : function() {

        var conf = this.itemConfig;
        this.typeStore = MShop.GlobalStoreMgr.get('Filter_Type', conf.domain);
        this.listTypeStore = MShop.GlobalStoreMgr.get(conf.listTypeControllerName, conf.domain);

        return [
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'typeid',
                header : MShop.I18n.dt('client/extjs', 'List type'),
                id : 'listtype',
                width : 60,
                renderer : this.typeColumnRenderer.createDelegate(this,
                    [this.listTypeStore, conf.listTypeLabelProperty], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Status'),
                id : 'refstatus',
                width : 40,
                align : 'center',
                renderer : this.refStatusColumnRenderer.createDelegate(this, ['filter.status'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Type'),
                id : 'reftype',
                width : 100,
                renderer : this.refTypeColumnRenderer.createDelegate(this, [
                    this.typeStore,
                    'filter.typeid',
                    'filter.type.label'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Web Type'),
                id : 'reflabel',
                width : 70,
                renderer : this.refColumnRenderer.createDelegate(this, ['filter.webtype'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Label'),
                id : 'refcontent',
                width : 60,
                renderer : this.refColumnRenderer.createDelegate(this, ['filter.label'], true)
            }];
    }
});

Ext.reg('MShop.panel.filter.itempickerui', MShop.panel.filter.ItemPickerUi);


Ext.ns('MShop.panel.catalog');

//hook filter picker into the catalog ItemUi
Ext.ux.ItemRegistry.registerItem('MShop.panel.catalog.ItemUi', 'MShop.panel.catalog.FilterItemPickerUi', {
 xtype : 'MShop.panel.filter.itempickerui',
 itemConfig : {
     recordName : 'Catalog_List',
     idProperty : 'catalog.list.id',
     siteidProperty : 'catalog.list.siteid',
     listNamePrefix : 'catalog.list.',
     listTypeFilterCode : 'default',
     listTypeIdProperty : 'catalog.list.type.id',
     listTypeLabelProperty : 'catalog.list.type.label',
     listTypeControllerName : 'Catalog_List_Type',
     listTypeCondition : {
         '&&' : [{
             '==' : {
                 'catalog.list.type.domain' : 'filter'
             },
             
         }]
     },
     listTypeKey : 'catalog/list/type/filter'
 },
 listConfig : {
     prefix : 'filter.'
 }
}, 22);


Ext.ns('MShop.panel.filter');

//hook text picker into the catalog ItemUi
Ext.ux.ItemRegistry.registerItem('MShop.panel.filter.ItemUi', 'MShop.panel.filter.TextItemPickerUi', {
 xtype : 'MShop.panel.text.itempickerui',
 itemConfig : {
     recordName : 'Filter_List',
     idProperty : 'filter.list.id',
     siteidProperty : 'filter.list.siteid',
     listNamePrefix : 'filter.list.',
     listTypeIdProperty : 'filter.list.type.id',
     listTypeLabelProperty : 'filter.list.type.label',
     listTypeControllerName : 'Filter_List_Type',
     listTypeCondition : {
         '&&' : [{
             '==' : {
                 'filter.list.type.domain' : 'text'
             }
         }]
     },
     listTypeKey : 'filter/list/type/text'
 },
 listConfig : {
     domain : 'filter',
     prefix : 'text.'
 }
}, 10);


///////////////////////////////////////////////////////////////////////////////////////////////////

Ext.ns('MShop.panel.filter');

MShop.panel.filter.ListUiSmall1 = Ext.extend(MShop.panel.AbstractListUi, {

    recordName : 'Filter',
    idProperty : 'filter.id',
    siteidProperty : 'filter.siteid',
    itemUiXType : 'MShop.panel.filter.itemui',

    autoExpandColumn : 'filter-list-label',

    filterConfig : {
        filters : [{
            dataIndex : 'filter.label',
            operator : '=~',
            value : ''
        }]
    },
    
    onBeforeLoad : function(store, options) {
        this.setSiteParam(store);

        if(this.domain) {
            this.setDomainFilter(store, options);
        }

        options.params = options.params || {};
        options.params.condition = {
            '&&' : [{
                '==' : {
                    'filter.type.code' : 'global'
                }
            }]
        };

    },
    getColumns : function() {
        // make sure type store gets loaded in same batch as this grid data
        this.typeStore = MShop.GlobalStoreMgr.get('Filter_Type', this.domain);
        this.itemTypeStore = MShop.GlobalStoreMgr.get('Filter_Type', this.domain);

        return [{
            xtype : 'gridcolumn',
            dataIndex : 'filter.id',
            header : MShop.I18n.dt('client/extjs', 'ID'),
            sortable : true,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.status',
            header : MShop.I18n.dt('client/extjs', 'Status'),
            sortable : true,
            width : 40,
            align : 'center',
            renderer : this.statusColumnRenderer.createDelegate(this)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.typeid',
            header : MShop.I18n.dt('client/extjs', 'Type'),
            width : 120,
            renderer : this.typeColumnRenderer.createDelegate(this, [this.typeStore, "filter.type.label"], true)
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.webtype',
            header : MShop.I18n.dt('client/extjs', 'Web Type'),
            sortable : true,
            width : 70
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.label',
            header : MShop.I18n.dt('client/extjs', 'Label'),
            width : 50,
            sortable : true,
            id : 'filter-list-label'
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.ctime',
            header : MShop.I18n.dt('client/extjs', 'Created'),
            sortable : true,
            width : 120,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.mtime',
            header : MShop.I18n.dt('client/extjs', 'Last modified'),
            sortable : true,
            width : 120,
            editable : false,
            hidden : true
        }, {
            xtype : 'gridcolumn',
            dataIndex : 'filter.editor',
            header : MShop.I18n.dt('client/extjs', 'Editor'),
            sortable : true,
            width : 120,
            editable : false,
            hidden : true
        }];
    },
});

Ext.reg('MShop.panel.filter.listuismall1', MShop.panel.filter.ListUiSmall1);


Ext.ns('MShop.panel');

MShop.panel.listItemListUiFilter = Ext.extend(MShop.panel.ListItemListUi,{
    setFilters : function(store, options) {
    	
        MShop.panel.listItemListUiFilter.superclass.setFilters.call(this,store, options);
        
        if(this.listItemPickerUi.itemConfig.listTypeFilterCode){
            var typeidCriteria = {};
            typeidCriteria[this.listItemPickerUi.itemConfig.listNamePrefix + 'type.code'] = this.listItemPickerUi.itemConfig.listTypeFilterCode;
            
            options.params.condition['&&'].push({
                '==' : typeidCriteria
            });
         }
        
        return true;
    },
});
Ext.reg('MShop.panel.listitemlistuifilter', MShop.panel.listItemListUiFilter);

Ext.ns('MShop.panel.filter');

MShop.panel.filter.ItemPickerUi1 = Ext.extend(MShop.panel.AbstractListItemPickerUi, {

    title : MShop.I18n.dt('client/extjs', 'Filter Global'),

    initComponent : function() {

        Ext.apply(this.itemConfig, {
            title : MShop.I18n.dt('client/extjs', 'Associated filters'),
            xtype : 'MShop.panel.listitemlistuifilter',
            domain : 'filter',
            getAdditionalColumns : this.getAdditionalColumns.createDelegate(this),
        });

        Ext.apply(this.listConfig, {
            title : MShop.I18n.dt('client/extjs', 'Available filters'),
            xtype : 'MShop.panel.filter.listuismall1'
        });

        MShop.panel.filter.ItemPickerUi1.superclass.initComponent.call(this);
    },
    
    insertListItems : function(records, rowIndex) {
        var refStore = this.getRefStore();
        var clones = [];

        this.listTypeStore.filter([{
            property : this.itemConfig.listNamePrefix + 'type.domain',
            value : this.itemListUi.domain
        }, {
            property : this.itemConfig.listNamePrefix + 'type.code',
            value : 'global'
        }]);

        var typeId = this.listTypeStore.getCount() ? this.listTypeStore.getAt(0).id : null;

        this.listTypeStore.clearFilter();

        Ext.each([].concat(records), function(record, id) {
            var refIdProperty = this.itemConfig.listNamePrefix + "refid";
            var typeIdPropery = this.itemConfig.listNamePrefix + "typeid";
            var recordTypeIdProperty = this.itemConfig.domain + ".typeid";

            if(refStore.getById(record.id)) {
                records.remove(record);

                // Get index of duplicated entry.
                var index = this.itemListUi.store.findBy(function(item) {
                    return (record.id == item.get(refIdProperty) && typeId == item.get(typeIdPropery));
                }, this);

                if(index != -1) {
                    // If entry is duplicated highlight it.
                    Ext.fly(this.itemListUi.grid.getView().getRow(index)).highlight();
                } else {
                    clones.push(record.copy());
                }
            } else {
                clones.push(record.copy());
            }
        }, this);

        records = clones;
        if(Ext.isEmpty(records)) {
            return;
        }

        // insert self in refStore
        refStore.add(records);

        // insert new list item records at the right position
        var rs = [], recordType = MShop.Schema.getRecord(this.itemListUi.recordName);

        Ext.each(records, function(record) {
            var data = {};
            data[this.itemConfig.listNamePrefix + 'refid'] = record.id;
            data[this.itemConfig.listNamePrefix + 'domain'] = this.itemListUi.domain;
            data[this.itemConfig.listNamePrefix + 'typeid'] = typeId;
            data[this.itemConfig.listNamePrefix + 'status'] = 1;
            rs.push(new recordType(data));
        }, this);

        this.itemListUi.store.insert(rowIndex !== false ? rowIndex : this.itemListUi.store.getCount(), rs);

        this.itemListUi.store.each(function(record, idx) {
            record.set(this.itemConfig.listNamePrefix + 'position', idx);
        }, this);
    },
    
    getAdditionalColumns : function() {

        var conf = this.itemConfig;
        this.typeStore = MShop.GlobalStoreMgr.get('Filter_Type', conf.domain);
        this.listTypeStore = MShop.GlobalStoreMgr.get(conf.listTypeControllerName, conf.domain);

        return [
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'typeid',
                header : MShop.I18n.dt('client/extjs', 'List type'),
                id : 'listtype',
                width : 60,
                renderer : this.typeColumnRenderer.createDelegate(this,
                    [this.listTypeStore, conf.listTypeLabelProperty], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Status'),
                id : 'refstatus',
                width : 40,
                align : 'center',
                renderer : this.refStatusColumnRenderer.createDelegate(this, ['filter.status'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Type'),
                id : 'reftype',
                width : 80,
                renderer : this.refTypeColumnRenderer.createDelegate(this, [
                    this.typeStore,
                    'filter.typeid',
                    'filter.type.label'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Web Type'),
                id : 'reflabel',
                width : 70,
                renderer : this.refColumnRenderer.createDelegate(this, ['filter.webtype'], true)
            },
            {
                xtype : 'gridcolumn',
                dataIndex : conf.listNamePrefix + 'refid',
                header : MShop.I18n.dt('client/extjs', 'Label'),
                id : 'refcontent',
                width : 50,
                renderer : this.refColumnRenderer.createDelegate(this, ['filter.label'], true)
            }];
    }
});

Ext.reg('MShop.panel.filter.itempickerui1', MShop.panel.filter.ItemPickerUi1);


Ext.ns('MShop.panel.catalog');

//hook filter picker into the catalog ItemUi
Ext.ux.ItemRegistry.registerItem('MShop.panel.catalog.ItemUi', 'MShop.panel.catalog.FilterItemPickerUi1', {
 xtype : 'MShop.panel.filter.itempickerui1',
 itemConfig : {
     recordName : 'Catalog_List',
     idProperty : 'catalog.list.id',
     siteidProperty : 'catalog.list.siteid',
     listNamePrefix : 'catalog.list.',
     listDomain : 'filter',
     listTypeFilterCode : 'global',
     listTypeIdProperty : 'catalog.list.type.id',
     listTypeLabelProperty : 'catalog.list.type.label',
     listTypeControllerName : 'Catalog_List_Type',
     listTypeCondition : {
         '&&' : [{
             '==' : {
                 'catalog.list.type.domain' : 'filter'
             }
         }]
     },
     listTypeKey : 'catalog/list/type/filter'
 },
 listConfig : {
     prefix : 'filter.'
 }
}, 23);
