<?php

/**
 * @copyright Copyright (c) Metaways Infosystems GmbH, 2012
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @package Client
 * @subpackage Html
 */


/**
 * Default implementation of catalog list section HTML clients.
 *
 * @package Client
 * @subpackage Html
 */
class Client_Html_Catalog_Medialist_Default
	extends Client_Html_Catalog_List_DefaultExt
	implements Client_Html_Common_Client_Factory_Interface
{

	private $_subPartPath = 'client/html/catalog/medialist/default/subparts';
	private $_subPartNames = array();

	private $_tags = array();
	private $_expire;
	private $_cache;


	/**
	 * Returns the HTML code for insertion into the body.
	 *
	 * @param string $uid Unique identifier for the output if the content is placed more than once on the same page
	 * @param array &$tags Result array for the list of tags that are associated to the output
	 * @param string|null &$expire Result variable for the expiration date of the output (null for no expiry)
	 * @return string HTML code
	 */
	public function getBody( $uid = '', array &$tags = array(), &$expire = null )
	{
		$prefixes = array( 'f','d');

		/** client/html/catalog/list
		 * All parameters defined for the catalog list component and its subparts
		 *
		 * This returns all settings related to the filter component.
		 * Please refer to the single settings for details.
		 *
		 * @param array Associative list of name/value settings
		 * @category Developer
		 * @see client/html/catalog#list
		 */
		$confkey = 'client/html/catalog/list';

		if( ( $html = $this->_getCached( 'body', $uid, $prefixes, $confkey ) ) === null )
		{
			$context = $this->_getContext();
			$view = $this->getView();

			try
			{
				$view = $this->_setViewParams( $view, $tags, $expire );

				$html = '';
				foreach( $this->_getSubClients() as $subclient ) {
					$html .= $subclient->setView( $view )->getBody( $uid, $tags, $expire );
				}
				$view->listBody = $html;
			}
			catch( Client_Html_Exception $e )
			{
				$error = array( $context->getI18n()->dt( 'client/html', $e->getMessage() ) );
				$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
			}
			catch( Controller_Frontend_Exception $e )
			{
				$error = array( $context->getI18n()->dt( 'controller/frontend', $e->getMessage() ) );
				$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
			}
			catch( MShop_Exception $e )
			{
				$error = array( $context->getI18n()->dt( 'mshop', $e->getMessage() ) );
				$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
			}
			catch( Exception $e )
			{
				$context->getLogger()->log( $e->getMessage() . PHP_EOL . $e->getTraceAsString() );

				$error = array( $context->getI18n()->dt( 'client/html', 'A non-recoverable error occured' ) );
				$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
			}

			$tplconf = 'client/html/catalog/medialist/default/template-body';
			$default = 'catalog/medialist/body-default.html';

			$html = $view->render( $this->_getTemplate( $tplconf, $default ) );

			$this->_setCached( 'body', $uid, $prefixes, $confkey, $html, $tags, $expire );
		}
		else
		{
			$html = $this->modifyBody( $html, $uid );
		}

		return $html;
	}


	/**
	 * Returns the HTML string for insertion into the header.
	 *
	 * @param string $uid Unique identifier for the output if the content is placed more than once on the same page
	 * @param array &$tags Result array for the list of tags that are associated to the output
	 * @param string|null &$expire Result variable for the expiration date of the output (null for no expiry)
	 * @return string|null String including HTML tags for the header on error
	 */
	public function getHeader( $uid = '', array &$tags = array(), &$expire = null )
	{
		$prefixes = array( 'f', 'd');
		$confkey = 'client/html/catalog/medialist';

		if( ( $html = $this->_getCached( 'header', $uid, $prefixes, $confkey ) ) === null )
		{
			$view = $this->getView();

			try
			{
				$view = $this->_setViewParams( $view, $tags, $expire );

				$html = '';
				foreach( $this->_getSubClients() as $subclient ) {
					$html .= $subclient->setView( $view )->getHeader( $uid, $tags, $expire );
				}
				$view->listHeader = $html;

				$tplconf = 'client/html/catalog/medialist/default/template-header';
				$default = 'catalog/medialist/header-default.html';

				$html = $view->render( $this->_getTemplate( $tplconf, $default ) );

				$this->_setCached( 'header', $uid, $prefixes, $confkey, $html, $tags, $expire );
			}
			catch( Exception $e )
			{
				$this->_getContext()->getLogger()->log( $e->getMessage() . PHP_EOL . $e->getTraceAsString() );
			}
		}
		else
		{
			$html = $this->modifyHeader( $html, $uid );
		}

		return $html;
	}


	/**
	 * Returns the sub-client given by its name.
	 *
	 * @param string $type Name of the client type
	 * @param string|null $name Name of the sub-client (Default if null)
	 * @return Client_Html_Interface Sub-client object
	 */
	public function getSubClient( $type, $name = null )
	{
		return $this->_createSubClient( 'catalog/medialist/' . $type, $name );
	}


	/**
	 * Processes the input, e.g. store given values.
	 * A view must be available and this method doesn't generate any output
	 * besides setting view variables.
	 */
	public function process()
	{
		$context = $this->_getContext();
		$view = $this->getView();

		try
		{
			$site = $context->getLocale()->getSite()->getCode();
			$params = $this->_getClientParams( $view->param() );
			$context->getSession()->set( 'arcavias/catalog/medialist/params/last/' . $site, $params );

			parent::process();
		}
		catch( Client_Html_Exception $e )
		{
			$error = array( $context->getI18n()->dt( 'client/html', $e->getMessage() ) );
			$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
		}
		catch( Controller_Frontend_Exception $e )
		{
			$error = array( $context->getI18n()->dt( 'controller/frontend', $e->getMessage() ) );
			$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
		}
		catch( MShop_Exception $e )
		{
			$error = array( $context->getI18n()->dt( 'mshop', $e->getMessage() ) );
			$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
		}
		catch( Exception $e )
		{
			$context->getLogger()->log( $e->getMessage() . PHP_EOL . $e->getTraceAsString() );

			$error = array( $context->getI18n()->dt( 'client/html', 'A non-recoverable error occured' ) );
			$view->listErrorList = $view->get( 'listErrorList', array() ) + $error;
		}
	}


	/**
	 * Returns the list of sub-client names configured for the client.
	 *
	 * @return array List of HTML client names
	 */
	protected function _getSubClientNames()
	{
		return $this->_getContext()->getConfig()->get( $this->_subPartPath, $this->_subPartNames );
	}


	/**
	 * Sets the necessary parameter values in the view.
	 *
	 * @param MW_View_Interface $view The view object which generates the HTML output
	 * @param array &$tags Result array for the list of tags that are associated to the output
	 * @param string|null &$expire Result variable for the expiration date of the output (null for no expiry)
	 * @return MW_View_Interface Modified view object
	 */
	protected function _setViewParams( MW_View_Interface $view, array &$tags = array(), &$expire = null )
	{
		if( !isset( $this->_cache ) )
		{
			$context = $this->_getContext();
			$config = $context->getConfig();
			$controller = Controller_Frontend_Factory::createController( $context, 'catalog' );
			$filters = $downloadMap = $media = array();
			$view->langCode = $context->getLocale()->getLanguageId();
			
			$catId = $view->param('f_catids');
			$configData = $config->get('client/html/catalog/detail/additional/download/types',array());
			$listType = $config->get( 'client/html/catalog/medialist/type', 'data_sheets' );
			if($view->param('f_listtype')!=''){
			    $listType = $view->param('f_listtype');
			}
				
			$manager = $controller->createManager('catalog');
			$search = $manager->createSearch( true );
			$temp = array($search->compare( '==', 'catalog.level', 3));
			if(strtolower($listType)=='light_distribution_curve'){
			    $temp = array($search->compare( '==', 'catalog.parentid', 4));
			}
			if($catId && is_array($catId)){
			    foreach ($catId as $key => $item){
			        if(!empty($item)){
			            $temp[]=$search->compare( '==', 'catalog.parentid', $item);
			        }
			    }
			}
			
			$expr = array(
			        $search->combine( '||', $temp ),
			        $search->getConditions(),
			);
			$search->setConditions( $search->combine( '&&', $expr ) );
			$search->setSlice( 0, 0x7FFFFFFF );
			$attributes = $manager->searchItems( $search,array('text','product','attribute'));
			$productManager = $controller->createManager('product');
			foreach( $attributes as $id => $item ) {
			    $i = reset($item->getRefItems( 'attribute', array('DisplaySequence')));
			    $filters[$item->getNode()->__get('level')][$i ? $i->getName().$id : $id] = $item;
			    $categoryProducts = $item->getRefItems( 'product', null, 'default' );
			    if($catId && is_array($catId) && $catId[max(array_keys($catId))]==$id && count($categoryProducts) > 0){
			        
			        $search = $productManager->createSearch( true );
			        $expr = array(
			                $search->compare( '==', 'product.id', array_keys($categoryProducts)),
			                $search->getConditions(),
			        );
			        $search->setConditions( $search->combine( '&&', $expr )  );
			        $resultProducts = $productManager->searchItems( $search, array('text','attribute'));
			        //sorting
			        $sorting = array();
			        foreach ($resultProducts as $node){
			                $i = reset($node->getRefItems( 'attribute', array('DisplaySequence')));
			            $filters[$item->getNode()->__get('level')+1][$i?$i->getName().$node->getId():$node->getId()] = $node;
			        }
			    }
			}
			
			foreach ($filters as $type => $filt){
			    ksort($filters[$type]);
			}
				
			
			$mediaIds = $productIds = $selectedFilters = array();
			if($view->param('d_prodid') || $view->param('f_search') /*|| $view->param('f_attrid')*/){
			    $config->set('client/html/catalog/domains', array() );
			    $config->set('client/html/catalog/list/domains', array() );
			    $config->set('client/html/catalog/list/size', 100000);
			    if($view->param('f_search')){
    			    $config->set('mshop/catalog/manager/index/default/item/search',$config->get('mshop/catalog/manager/index/default_ext/item/search'));
    			    $config->set('mshop/catalog/manager/index/default/item/count',$config->get('mshop/catalog/manager/index/default_ext/item/count'));
			    }
    			$products = $this->_getProductList( $view );
    			$productIds = array_keys($products);
    			
    			$productManager = $controller->createManager('product/list');
    			$search = $productManager->createSearch( true );
    			$expr = array(
    			        $search->compare( '==', 'product.list.refid', $productIds ),
    			        $search->compare( '==', 'product.list.domain', 'product' ),
    			        $search->compare( '==', 'product.list.type.code', array('default','variant') ),
    			        $search->getConditions(),
    			);
    			$search->setConditions( $search->combine( '&&', $expr ) );
    			$search->setSlice( 0, 0x7FFFFFFF );    			
    			$pssProducts = $productManager->searchItems( $search);
    			foreach ($pssProducts as $item){
    			    $productIds[]=$item->getParentId();
    			}
    			
    			$indexManager = MShop_Factory::createManager( $this->_getContext(), 'catalog/index/product');
    			$search = $indexManager->createSearch( true );
    			$expr = array(
    			        $search->compare( '==', 'catalog.index.product.id', array_keys( $products ) ),
    			        $search->getConditions(),
    			);
    			$search->setConditions( $search->combine( '&&', $expr ) );
    			$search->setSlice( 0, 0x7FFFFFFF );
    			$variantProducts = $indexManager->searchItems( $search, array('products'));
    			foreach ($variantProducts as $item){
    			    $productIds[]=$item->getId();
    			}
    			
    			$productManager = $controller->createManager('product/list');
    			$search = $productManager->createSearch( true );
    			$expr = array(
    			        $search->compare( '==', 'product.list.parentid', array_unique($productIds) ),
    			        $search->compare( '==', 'product.list.domain', 'media' ),
    			        $search->compare( '==', 'product.list.type.code', $configData[$listType]),
    			        $search->getConditions(),
    			);
    			$search->setConditions( $search->combine( '&&', $expr ) );
    			$search->setSlice( 0, 0x7FFFFFFF );
    			$mediaList = $productManager->searchItems( $search,array('text'));
    			foreach ($mediaList as $item){
    			    $mediaIds[$item->getRefId()]=$item->getRefId();
    			}
			}
			
			
			if($view->param('f_attrib_id')){
			    
			    $mediaManager = $controller->createManager( 'media/list' );
			    
			    foreach ($view->param('f_attrib_id') as $key=> $val){
			        $tempArray = array();
    			    $mediaSearch = $mediaManager->createSearch( true );
    			    $expr = array(
    			            $mediaSearch->getConditions()
    			    );
  			        if($val){
  			            $selectedFilters[]=$key;
  			            $tempArray = array();
  			            $mediaSearch = $mediaManager->createSearch( true );
  			            $expr = array(
  			                    $mediaSearch->getConditions(),
  			                    $mediaSearch->compare( '==', 'media.list.refid', $val),
  			                    $mediaSearch->compare( '==', 'media.list.domain', $key=='ZPB_TYP'?'attribute':'text')
  			            );
  			            $mediaSearch->setConditions( $mediaSearch->combine( '&&', $expr ) );
  			            $mediaSearch->setSlice( 0, 0x7FFFFFFF );
  			            $media = $mediaManager->searchItems( $mediaSearch);
  			            foreach ($media as $item){
  			                $tempArray[$item->getParentId()]=$item->getParentId();
  			            }
  			            if(empty($mediaIds)){
  			                $mediaIds=$tempArray;
  			            }else{
  			                $mediaIds = array_intersect($mediaIds,$tempArray);
  			            }
  			        }
			    }
			    
			}
			
			// software tab for dowloads from category
			if(in_array(strtolower($listType), array('software','broschures','operating_instructions','internal_VIS','internal_ProductCommunication','internal_ProductPresentation','internal_TestReport','internal_InternalGeneral','3D_Data'))){
			    if($catId && is_array($catId)){
			        ksort($catId);
			        $id= end($catId);
			        if(!empty($id)){
    			        $manager = $controller->createManager('catalog');
    			        $catalog = $manager->getItem($id,array('media'));
    			        if($refs = $catalog->getRefItems('media',null, $configData[$listType])){
    			            foreach ($refs as $ref){
    			                $mediaIds[$ref->getId()]=$ref->getId();
    			            }
    			        }
			        }
			    }
			}
			
			if(!empty($mediaIds)){
    			$mediaManager = $controller->createManager( 'media' );
    			$mediaSearch = $mediaManager->createSearch( true );
    			$expr = array(
    			        $mediaSearch->compare( '==', 'media.id', array_keys($mediaIds)),
    			        $mediaSearch->getConditions(),
    			);

    			$mediaSearch->setConditions( $mediaSearch->combine( '&&', $expr ) );
    			$mediaSearch->setSlice( 0, 0x7FFFFFFF );
    			$media = $mediaManager->searchItems( $mediaSearch, array('text','attribute') );
    			
    			$mediaAttribute = array();
    			$mediaText = array();
    			foreach ($media as $key=>$el){
    			    $mediaAttribute = array_merge($mediaAttribute, array_keys($el->getRefItems('attribute')));
    			    $mediaText = array_merge($mediaText, array_keys($el->getRefItems('text')));
                    if($listType=='certificates' && empty($el->getRefItems( 'text', array('ZPB_EX-ZERT','ZKO_ART')))){
                        unset($media[$key]);
                    }
    			}
    			 
    			/*if($view->param('is_german')){
        			foreach ($media as $key => $item){
        			    $langItems = $item->getRefItems( 'text', array('DVS_LANGUAGE','AT_AssetLanguage'));
        			    if(empty($langItems)){
        			        unset($media[$key]);
        			        continue;
        			    }
        			    foreach($langItems as $lang){
        			        if($langs=(array)explode('</br>',$lang->getContent())){
        			            if(!in_array('German',$langs) && !in_array('Deutsch',$langs)){
        			                unset($media[$key]);
        			                continue;
        			            }
        			        }else{
        			            unset($media[$key]);
        			            continue;
        			        }
        			    }
        			}
    			}*/

    			if($view->param('is_certificate') || $view->param('is_declaration')){
      			    foreach ($media as $key => $item){
      			        if( ($view->param('is_certificate') && !empty($item->getRefItems( 'text',array('ZPB_EX-ZERT')))) || 
      			            ($view->param('is_declaration') && !empty($item->getRefItems( 'text',array('ZKO_ART')))) ){
      			        }else{
      			            unset($media[$key]);
      			            continue;
      			        }
       			        
        			}
			    }
			}
			
			if(!empty($media) && $listType=='certificates'){
    			//sorting by filename
    			$sorting = $sortedMedia =  array();
    			foreach ($media as $item){
    			    if($el = reset($item->getRefItems( 'text', $listType=='certificates'? 'ZPB_GELTUNG':'asset.filename'))){
    			        $sorting[strtolower($el->getContent()).$item->getId()]=$item;
    			    }else{
    			        $sorting[strtolower($item->getUrl()).$item->getId()]=$item;
    			    }
    			}
    			ksort($sorting);
    			foreach($sorting as $item){
    			    $sortedMedia[$item->getId()]=$item;
    			}
    			$media = $sortedMedia;
			}else if(!empty($media) && $listType != 'certificates'){
                $sortDescription = $sortLanguage =  array();
                foreach ($media as $key=>$item){
                    $el = reset($item->getRefItems( 'text', 'AT_AssetDescription'));
                    $sortDescription[$key] = $el ? $el->getContent() : '';
                    $el = reset($item->getRefItems( 'text', 'AT_AssetLanguage'));
                    $sortLanguage[$key] = $el ? $el->getContent() : '';
                }
                array_multisort($sortDescription, SORT_ASC, $sortLanguage, SORT_ASC, $media);
            }


			
			
			$view->itemData = $media;
			$view->mediaItems = $media;
			
			
			//filters for certificates
			$groups = $config->get( 'client/html/catalog/medialist/filters',
			        array('ZPB_IAUTH', 'DVS_DOKNR','ZPB_TYP','ZPB_GELTUNG') );
			
			foreach ($groups as $group){ 
    			$manager = $controller->createManager('attribute');
    			$search = $manager->createSearch( true );
    			$expr = array(
    			        $search->compare( '==', 'attribute.type.code', $group ),
    			        $search->compare( '==', 'attribute.domain', array('media','product') ),
    			        $search->getConditions(),
    			);
    			$search->setConditions( $search->combine( '&&', $expr ) );
    			$search->setSortations( array($search->sort('+', 'attribute.label' )) );
    			$search->setSlice( 0, 0x7FFFFFFF );
    			$attributes = $manager->searchItems( $search,array('text'));

    		    foreach( $attributes as $id => $item ) {
    		        if((isset($mediaAttribute) && in_array($id,$mediaAttribute)) || !isset($mediaAttribute) /*|| in_array($item->getType(),$selectedFilters)*/){
    				    $filters[$item->getType()][$id] = $item;
    		        }
    			}
    
    			$manager = $controller->createManager('text');
    			$search = $manager->createSearch( true );
    			$expr = array(
    			        $search->compare( '==', 'text.type.code', $group ),
    			        $search->compare( '==', 'text.domain', array('media','product') ),
    			        $search->getConditions(),
    			);
    			$search->setConditions( $search->combine( '&&', $expr ) );
    			$search->setSortations( array($search->sort('+', 'text.content' )) );
    			$search->setSlice( 0, 0x7FFFFFFF );
    			$attributes = $manager->searchItems( $search,array('text'));
    			
    		    foreach( $attributes as $id => $item ) {
    		        if((isset($mediaText) && in_array($id,$mediaText)) || !isset($mediaText) /*|| in_array($item->getType(),$selectedFilters)*/){
    		            $filters[$item->getType()][$id] = $item;
    		        }
    			}
			}
			
			$view->attributeTypes = $controller->getAttributeTypesByCodes($groups);//$this->_getAttributeTypes($groups, $controller);
			$view->listMediaFilters=$filters;
			
			$this->_tags[] = 'medialist_'.$listType;

			$this->_cache = $view;
		}

		$expire = $this->_expires( $this->_expire, $expire );
		$tags = array_merge( $tags, $this->_tags );

		return $this->_cache;
	}
}
