<?php

return array(
	'customer' => array(
		'manager' => array(
			'name' => 'Typo3',
		)
	),
	'catalog' => array(
		'manager' => array(
			'index' => array(
				'name' => 'MySQL',
				'attribute' => array(
					'name' => 'MySQL',
				),
				'catalog' => array(
					'name' => 'MySQL',
				),
				'price' => array(
					'name' => 'MySQL',
				),
				'text' => array(
					'name' => 'MySQLExt',
				),
			),
		),
	),
);
