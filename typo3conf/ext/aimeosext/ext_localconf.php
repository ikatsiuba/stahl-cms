<?php

if ( ! defined( 'TYPO3_MODE' ) ) {
	die ( 'Access denied.' );
}

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['aimeos']['extDirs']['1_'.$_EXTKEY] =
  'EXT:' . $_EXTKEY . '/Resources/Private/Extensions/';

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['aimeos']['confDirs']['1_'.$_EXTKEY] =
  'EXT:' . $_EXTKEY . '/Resources/Private/Config/';

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('external_import')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['tx_externalimport_importer'] = array(
     'className' => 'Aimeos\\Aimeosext\\Import\\ExternalImport'
    );
}
/*
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Frontend\\Controller\\TypoScriptFrontendController'] = array(
        'className' => 'Aimeos\\Aimeosext\\Controller\\LocalTypoScriptFrontendController'
);
*/
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('solrfal')) {
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['ApacheSolrForTypo3\\Solrfal\\Detection\\PageContextDetector'] = array(
        'className' => 'Aimeos\\Aimeosext\\Solr\\Detection\\PageContextDetector'
);

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['ApacheSolrForTypo3\\Solrfal\\Indexing\\DocumentFactory'] = array(
        'className' => 'Aimeos\\Aimeosext\\Solr\\Indexing\\DocumentFactory'
);
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['ApacheSolrForTypo3\\Solrfal\\Queue\\Queue'] = array(
        'className' => 'Aimeos\\Aimeosext\\Solr\\IndexQueue\\Queue'
);
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['ApacheSolrForTypo3\\Solrfal\\Indexing\\Indexer'] = [
        'className' => 'Aimeos\\Aimeosext\\Solrfal\\Indexing\\Indexer'
    ];
}

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['encodeSpURL_postProc'][] = 'Aimeos\\Aimeosext\\Hooks\\RealUrl\\PathConfig->userEncodeSpURLPostProc';
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['decodeSpURL_preProc'][] = 'Aimeos\\Aimeosext\\Hooks\\RealUrl\\PathConfig->userDecodeSpURLPreProc';
}

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['Aimeos\\Aimeos\\Controller\\CatalogController'] = array(
        'className' => 'Aimeos\\Aimeosext\\Controller\\CatalogController'
);

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('dd_googlesitemap')) { 
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['dd_googlesitemap']['generateSitemapForPagesClass'][] = \Aimeos\Aimeosext\Hooks\GoogleSitemap\Sitemap::class;
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Aimeos.' . $_EXTKEY,
	'catalog-medialist',
	array( 'Catalog' => 'medialist' ),
	array( 'Catalog' => 'medialist' )
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Aimeos.' . $_EXTKEY,
        'catalog-webcodelist',
        array( 'Catalog' => 'webcodelist' ),
        array( 'Catalog' => 'webcodelist' )
        );
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Aimeos.' . $_EXTKEY,
        'catalog-searchlist',
        array( 'Catalog' => 'searchlist,list,filter' ),
        array( 'Catalog' => 'searchlist,list,filter' )
        );
?>