<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'aimeosext');

if (TYPO3_MODE == 'BE' && isset($GLOBALS['TBE_MODULES']['_configuration']['web_AimeosTxAimeosAdmin'])){
    $GLOBALS['TBE_MODULES']['_configuration']['web_AimeosTxAimeosAdmin']['labels']='LLL:EXT:aimeosext/Resources/Private/Language/admin.xlf';
    $GLOBALS['TBE_MODULES']['_configuration']['web_AimeosTxAimeosAdmin']['icon']='EXT:aimeosext/ext_icon.png';
}

 
$pluginName = str_replace( '_', '', $_EXTKEY );
 
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginName . '_catalog-medialist'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue( $pluginName . '_catalog-medialist', 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/CatalogMediaList.xml' );
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin( 'Aimeos.' . $_EXTKEY, 'catalog-medialist', 'Aimeos Shop - Catalog Media list' );

$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginName . '_catalog-webcodelist'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue( $pluginName . '_catalog-webcodelist', 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/CatalogWebcodeList.xml' );
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin( 'Aimeos.' . $_EXTKEY, 'catalog-webcodelist', 'Aimeos Shop - Catalog Webcode list' );

#$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginName . '_catalog-searchlist'] = 'pi_flexform';
#\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue( $pluginName . '_catalog-searchlist', 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/CatalogSearchList.xml' );
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin( 'Aimeos.' . $_EXTKEY, 'catalog-searchlist', 'Aimeos Shop - Catalog Search' );