<?php
namespace Aimeos\Aimeosext\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Frontend\Controller;
use TYPO3\CMS\Backend\FrontendBackendUserAuthentication;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Charset\CharsetConverter;
use TYPO3\CMS\Core\Error\Http\PageNotFoundException;
use TYPO3\CMS\Core\Error\Http\ServiceUnavailableException;
use TYPO3\CMS\Core\Localization\Locales;
use TYPO3\CMS\Core\Localization\LocalizationFactory;
use TYPO3\CMS\Core\Locking\Exception\LockAcquireWouldBlockException;
use TYPO3\CMS\Core\Locking\Locker;
use TYPO3\CMS\Core\Messaging\ErrorpageMessage;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Service\DependencyOrderingService;
use TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser;
use TYPO3\CMS\Core\TypoScript\TemplateService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Locking\LockingStrategyInterface;
use TYPO3\CMS\Core\Locking\LockFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Http\UrlHandlerInterface;
use TYPO3\CMS\Frontend\Page\CacheHashCalculator;
use TYPO3\CMS\Frontend\Page\PageGenerator;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Frontend\View\AdminPanelView;

/**
 * Class for the built TypoScript based frontend. Instantiated in
 * index_ts.php script as the global object TSFE.
 *
 * Main frontend class, instantiated in the index_ts.php script as the global
 * object TSFE.
 *
 * This class has a lot of functions and internal variable which are used from
 * index_ts.php.
 *
 * The class is instantiated as $GLOBALS['TSFE'] in index_ts.php.
 *
 * The use of this class should be inspired by the order of function calls as
 * found in index_ts.php.
 */
class LocalTypoScriptFrontendController extends \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
{
    /**
     * Calculates and sets the internal linkVars based upon the current
     * $_GET parameters and the setting "config.linkVars".
     *
     * @return void
     */
    public function calculateLinkVars()
    { 
        parent::calculateLinkVars();
        $linkVars = GeneralUtility::trimExplode(',', (string)$this->config['config']['linkVars']);
        if (empty($linkVars)) {
            return;
        }
        $getData = GeneralUtility::_GET();
        foreach ($linkVars as $linkVar) {
            if(strpos($linkVar,'|')){
                $specialLinkVar = GeneralUtility::trimExplode('|', (string)$linkVar);
                if(count($specialLinkVar)!=2){
                    continue;
                }
                if ($specialLinkVar[0] === '' || $specialLinkVar[1] === '' || !isset($getData[$specialLinkVar[0]][$specialLinkVar[1]])) {
                    continue;
                } 
                 if (!is_array($getData[$specialLinkVar[0]][$specialLinkVar[1]])) {
                     $temp = rawurlencode($getData[$specialLinkVar[0]][$specialLinkVar[1]]);
                     $value = '&' . $specialLinkVar[0].'['.$specialLinkVar[1].']' . '=' . $temp;
                 }
                 $this->linkVars .= $value;
            }
        }
        
    }
}
