<?php

namespace Aimeos\Aimeosext\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Aimeos\Aimeos\Base;
/**
 * Aimeos catalog controller.
 *
 * @package TYPO3_Aimeos
 */
class CatalogController extends \Aimeos\Aimeos\Controller\CatalogController
{
	/**
	 * Renders the catalog list section.
	 */
	public function listAction()
	{
	    $ai=GeneralUtility::_GP('ai');
	    if(is_null($ai) || (empty($ai['f_search']) && empty($ai['d_prodid']) && empty($ai['f_catid']))){
	        return;
	    }
	    return parent::listAction(); 
	}


	/**
	 * Renders the catalog detail section.
	 */
	public function detailAction()
	{ 
	    if((int)GeneralUtility::_GP('ai')['d_prodid']==0){
	        return;
	    }
	    
		return parent::detailAction();
	}
	
	/**
	 * Renders the catalog list section.
	 */
	public function medialistAction()
	{
	    if( is_object( $GLOBALS['TSFE'] ) && isset( $GLOBALS['TSFE']->config['config'] ) ) {
	        $GLOBALS['TSFE']->config['config']['noPageTitle'] = 2;
	    }

	    $templatePaths = Base::getAimeos()->getCustomPaths( 'client/html' ); 
	    $client = \Client_Html_Catalog_Medialist_Factory::createClient( $this->getContext(), $templatePaths );
	
	    return $this->getClientOutput( $client );
	}
	
	/**
	 * Renders the catalog list section.
	 */
	public function webcodelistAction()
	{
	    if( is_object( $GLOBALS['TSFE'] ) && isset( $GLOBALS['TSFE']->config['config'] ) ) {
	        $GLOBALS['TSFE']->config['config']['noPageTitle'] = 2;
	    }
	
	    $templatePaths = Base::getAimeos()->getCustomPaths( 'client/html' );
	    $client = \Client_Html_Catalog_Webcodelist_Factory::createClient( $this->getContext(), $templatePaths );
	
	    return $this->getClientOutput( $client );
	}
	
	/**
	 * Renders the catalog search section.
	 */
	public function searchlistAction()
	{ //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(GeneralUtility::_GP('ai'));
	    
	    if ($this->request->hasArgument('s_matirial') || $this->request->hasArgument('s_code')){
	        $sCode = $this->request->getArguments();
	        $controller = \Controller_Frontend_Factory::createController( $this->getContext(), 'catalog' );
	        $params = []; $pageId=null;
            $controller->searchBySType($sCode,$params,$pageId);
            if(!empty($params) && $pageId){
                $this->redirect('detail','Catalog',null,$params,$pageId);
	        }
	        $uriBuilder = $this->controllerContext->getUriBuilder();
	        $uriBuilder->reset();
	        
	        $redirectUid = 6;
	        $uriBuilder->setTargetPageUid($redirectUid);
	        $uri = $uriBuilder->build();
	        $this->redirectToUri($uri, 0, 301);
	    }
	}
	
}
