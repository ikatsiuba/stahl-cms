<?php
namespace Aimeos\Aimeosext\Solrfal\Indexing;

use ApacheSolrForTypo3\Solrfal\Queue\ItemGroup;

class Indexer extends \ApacheSolrForTypo3\Solrfal\Indexing\Indexer
{

    /**
     * Adds a group of queue items to the solr index and applies merging or not
     * @param ItemGroup $group
     * @return bool
     */
    protected function addGroupToIndex(ItemGroup $group)
    {
        $success = false;

        if (!$group->getRootItem()->getFile()->exists()) {
            $this->getItemRepository()->markFailed($group->getRootItem(), 'File missing');
            return $success;
        }

        $merge = $this->getIsMergingEnabledFormItem($group->getRootItem());
        $documents = $this->getDocumentFactory()->createDocumentsForQueueItemGroup($group, $merge);
        $solr = $this->getSolrServiceByContext($group->getRootItem()->getContext());

        foreach ($documents as $document) {
            unset($document->teaser);

            $pid = $document->getField('pid');
            $ptype = $document->getField('fileReferenceType');

             if('tx_solr_file' == $document->getType() && $pid) {
                    $rawQuery = 'pid:'.$pid['value'].' && type:tx_solr_file';
                    $solr->deleteByQuery( $rawQuery,true,true,0);
             }

             $row = null;
             if($pid && $ptype && ('mshop_media' == $ptype['value'] )){
                 $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                     'id, status, status_solr',
                     'mshop_media',
                     'id = ' . $pid['value']. ' '
                 );

                 if(!empty($row) && !$row['status']){
                     continue;
                 }
             }
             
            // todo document why teaser is unset
            $response = $solr->addDocument($document);
            $this->emitIndexedFileToSolr($group->getRootItem()->getFile());


            if ($response->getHttpStatus() == 200) {
                $success = true;
                foreach ($group->getItems() as $item) {
                    $this->getItemRepository()->markIndexedSuccessfully($item);
                }
            } else {
                foreach ($group->getItems() as $item) {
                    $this->getItemRepository()->markFailed($item,
                        $response->getHttpStatusMessage() . ': ' . $response->getRawResponse());
                }
            }

            if(!empty($row)){
                $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                    'mshop_media',
                    'id = ' . $pid['value']. ' ',
                    ['status_solr' => $success ? 0 : 1]
                );
            }

            if(!$success){
                file_put_contents(
                    PATH_site."typo3temp/logs/solfar_indexer_error.log",
                    date("Y-m-d H:i:s")
                        .' SORL NO INDEXED ('.$document->getRecordUid().') '
                        .print_r($response->getHttpStatus().': '.$response->getHttpStatusMessage(), true),
                    FILE_APPEND
                );
            }

        }

        return $success;
    }

}
