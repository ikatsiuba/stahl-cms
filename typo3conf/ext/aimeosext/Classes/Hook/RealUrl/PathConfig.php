<?php
namespace Aimeos\Aimeosext\Hooks\RealUrl;

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use DmitryDulepov\Realurl\Configuration\ConfigurationReader;

class PathConfig
{

    protected $_cfg=array('useUniqueCache_conf' => array(
                                            'strtolower' => true,
                                            'spaceCharacter' => '-',
                                    ));
    protected $_config = array();
    protected $_rootPages = null;
    private $_cache = array();
    private static $config;
    private static $context;
    private static $extConfig;
    
    private static $locale = array();
    private static $_managers = array();
    
    public function userEncodeSpURLPostProc(&$params, &$ref) {
        //TODO: fix Exception AIMEOS
        try { 
            $parts = $this->getParameters($params['params']['args']);
            $path = array();
            $this->sys_language_uid = $GLOBALS['TSFE']->sys_language_uid;
            if (isset($parts['L'])) {
                $this->sys_language_uid = $parts['L'];
            }
            if(( $result = $this->_getCached($this->sys_language_uid, $parts) ) === null){
               
                $config = $this->_getContext()->getConfig();
                if (isset($parts['ai[f_catid]']) && intval($parts['ai[f_catid]']) && empty($parts['ai[d_prodid]'])) {
                    $id = intval($parts['ai[f_catid]']);
                    if ($id) {
                        $path = $this->_getCategoryPath($id, $ref);
                        if (count($path)) {
                            $params['URL'] = str_replace(array( (strpos($params['URL'],reset($path))? reset($path).'/':'') .end($path), '//'),
                                array(implode('/', $path), '/'), $params['URL']);
                            $params['URL'] = str_replace('//', '/', $params['URL']);
//                            file_put_contents(PATH_site . "typo3temp/records.encode",__LINE__ . '  ' . print_r(array($params ['URL'], $path), true) . "\n", FILE_APPEND);
                            $this->_setCached($this->sys_language_uid, $parts, $params['URL']);
                            
                        }
                    }
                } elseif (isset($parts['ai[d_prodid]']) && intval($parts['ai[d_prodid]'])) {
                    $id = intval($parts['ai[d_prodid]']);
                    if ($id) { 
                        $parentId = isset($parts['ai[d_parent]']) && intval($parts['ai[d_parent]']) ? intval($parts['ai[d_parent]']) : null;
                        $path = $this->_getProductPath($id, $parentId, $ref, $parts);
                        if (count($path)) {
                            if (isset($parentId) && $parentId) {
                                $params ['URL'] = str_replace(array(reset($path) . '//' . implode('/', array_slice($path, -2)),'//'), array(implode('/', $path), '/'), $params['URL']);
                            } else {
                                $params ['URL'] = str_replace(array(reset($path) . '/' . implode('//', array_slice($path, -2)),'//'),array(implode('/', $path), '/'), $params['URL']);
                            }
//                            file_put_contents(PATH_site . "typo3temp/records.encodeP",'aaa1: ' . print_r(array($params ['URL'], $path), true) . "\n", FILE_APPEND);
                            $this->_setCached($this->sys_language_uid, $parts, $params['URL']);
                        }
                    }
                }
            }else{ 
                $params ['URL']=$result;
            }
        }catch (\Exception $exception){
            $GLOBALS['TSFE']->pageNotFoundAndExit();
        }
    }
    
    public function userDecodeSpURLPreProc(&$params, &$ref)
    { 
        $pathParts = explode('/', $params['URL']);
        $pathParts0 = $pathParts;
        $cnt=count($pathParts);
        $found =false;
        $langId=0;
        if(isset($pathParts[0])){
            $langId = $this->getLangIdByCode($pathParts[0]);
        }
        for ($j=$cnt;$j>0;$j--){
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('tablename', 'tx_realurl_uniqalias',
                    'value_alias=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($pathParts[$j], 'tx_realurl_uniqalias').
                    ' AND field_id="id" and lang='.$langId);
            if(!$found && isset($row['tablename'])){
                $z=$j;
                if($row['tablename']=='mshop_product'){
                    if(isset($pathParts[$j-1])){
                        $parentRow = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('tablename', 'tx_realurl_uniqalias',
                                'value_alias=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($pathParts[$j-1], 'tx_realurl_uniqalias').
                                ' AND field_id="parent_id" and lang='.$langId);
                    }
                    if(isset($parentRow['tablename'])){
                        //$pathParts[$j-1] = '/'.$pathParts[$j-1];
                        $z=$j-2;
                    }else{
                        $pathParts[$j] = '/'.$pathParts[$j];
                        $z=$j-1;
                    }
                }
                $found=true;
                continue;
            }
            if($found && isset($row['tablename']) && $j<$z){
                unset($pathParts[$j]);
            }
        }
        if($found){
            $params['URL'] = implode('/',$pathParts);
//            file_put_contents(PATH_site."typo3temp/records.decode", print_r(array($pathParts,$params['URL'],$pathParts0), true)."\n", FILE_APPEND);
        }
    }

    protected function _getCategoryPath($categoryId, $ref)
    {
        $addPath = array();
        $manager = $this->_getManager( $this->_getContext(), 'catalog');
        $config = $this->_getContext()->getConfig();
        $startid = 3;//$config->get( 'client/html/catalog/filter/tree/startid', '');
        if(!isset($this->sys_language_uid)){
            $this->sys_language_uid = $GLOBALS['TSFE']->sys_language_uid;
        }
        
        $path = $manager->getPath($categoryId,array('text'));
        foreach($path as $key => $el){
            unset($path[$key]);
            if( $key == $startid ) {
                break;
            }
        }
        $first = true;
        foreach ($path as $el){
            if($el->getStatus()==1){
                $dName = reset($el->getRefItems('text', 'DisplayName'));
                
                $path = $ref->cleanUpAlias($this->_cfg, $first && $dName && !empty($dName->getContent()) ? $dName->getContent() : $el->getName());
                
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('tablename', 'tx_realurl_uniqalias',
                        'value_alias=' .$GLOBALS['TYPO3_DB']->fullQuoteStr($path.'-'.$el->getId(), 'tx_realurl_uniqalias').
                        ' AND tablename="mshop_catalog"'.
                        ' AND lang='. $this->sys_language_uid );
                $row1 = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('tablename', 'tx_realurl_uniqalias',
                        'value_alias=' .$GLOBALS['TYPO3_DB']->fullQuoteStr($path, 'tx_realurl_uniqalias').
                        ' AND value_id=' .$el->getId().
                        ' AND tablename="mshop_catalog"'.
                        ' AND lang='. $this->sys_language_uid );
                
                if(isset($row['tablename']) && !isset($row1['tablename'])){
                    $path .= '-'.$el->getId();
                }
                $addPath[] = $path;
                //$first = false;
            }
        }
        return $addPath;
    }
    
    protected function _getProductPath($id, $parentId=null, $ref, $parts)
    {
        $path = array();
        $manager = $this->_getManager( $this->_getContext(), 'product');
        $product = $manager->getItem($id,array('text','attribute'));
        $catId = isset($parts['ai[f_catid]']) && intval($parts['ai[f_catid]'])?$parts['ai[f_catid]']:null;
        if(!isset($this->sys_language_uid)){
            $this->sys_language_uid = $GLOBALS['TSFE']->sys_language_uid;
        }
        if($product){
            //$parent = $this->_getParentItem($id);
            if(!is_null($parentId)){
                $parentProduct = $manager->getItem($parentId,array('text'));
                $catalog = $this->_getParentItem($parentProduct->getId(), 'catalog');
                if($catalog){
                    $path = $this->_getCategoryPath(!empty($catId)?$catId:$catalog->getParentId(), $ref);
                }
                $dName = reset($parentProduct->getRefItems('text', 'TitleProductSubSeries'));
                $ps = $ref->cleanUpAlias($this->_cfg, $dName && !empty($dName->getContent()) ? $dName->getContent() : $parentProduct->getName());
                /*
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('tablename', 'tx_realurl_uniqalias',
                        'value_alias=' .$GLOBALS['TYPO3_DB']->fullQuoteStr($ps.'-'.$parentProduct->getId(), 'tx_realurl_uniqalias').
                        ' AND tablename="mshop_product"'.
                        ' AND lang='. $this->sys_language_uid );
                if(isset($row['tablename'])){
                    $ps .= '-'.$parentProduct->getId();
                }
                */
                $path[]=$ps.'-'.$parentProduct->getId();
            }else{
                $catalog = $this->_getParentItem($product->getId(), 'catalog');
                if($catalog){
                    $path = $this->_getCategoryPath(!empty($catId)?$catId:$catalog->getParentId(), $ref);
//                    file_put_contents(PATH_site."typo3temp/records.encodeP", __LINE__.'  '.print_r($path, true)."\n", FILE_APPEND);
                }
            }
            $dName = reset($product->getRefItems('text', 'TitleProductSubSeries'));
            $aMaterialId = reset($product->getRefItems('attribute', 'AT_SAP_MainMaterialID'));
            $ps = $ref->cleanUpAlias($this->_cfg, $dName && !empty($dName->getContent()) ? $dName->getContent() : ($aMaterialId && !empty($aMaterialId->getName()) ? $aMaterialId->getName(): $product->getName()));
            /*
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('tablename', 'tx_realurl_uniqalias',
                    'value_alias=' .$GLOBALS['TYPO3_DB']->fullQuoteStr($ps.'-'.$product->getId(), 'tx_realurl_uniqalias').
                    ' AND tablename="mshop_product"'.
                    ' AND lang='. $this->sys_language_uid );
            if(isset($row['tablename'])){
                $ps .= '-'.$product->getId();
            }
            */
            $path[] = $ps.'-'.$product->getId();
        }
        return $path;
    }
    
    protected function _getParentItem($prodId,$type='product')
    {
        $typeItem = $this->_getTypeItem( $type.'/list/type', 'product', 'default' );
        $manager = $this->_getManager( $this->_getContext(), $type.'/list' );
    
        $search = $manager->createSearch( true );
        $expr = array(
                $search->compare( '==', $type.'.list.refid', $prodId ),
                $search->compare( '==', $type.'.list.typeid', $typeItem->getId() ),
                $search->compare( '==', $type.'.list.domain', 'product' ),
                $search->getConditions(),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSortations( array( $search->sort( '+', $type.'.list.position' ) ) );
    
        return reset($manager->searchItems( $search ));
    }
    
    protected function _getTypeItem( $prefix, $domain, $code )
    {
        $manager = $this->_getManager( $this->_getContext(), $prefix );
        $prefix = str_replace( '/', '.', $prefix );
    
        $search = $manager->createSearch();
        $expr = array(
                $search->compare( '==', $prefix . '.domain', $domain ),
                $search->compare( '==', $prefix . '.code', $code ),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $result = $manager->searchItems( $search );
    
        if( ( $item = reset( $result ) ) === false )
        {
            $msg = sprintf( 'No type item for "%1$s/%2$s" in "%3$s" found', $domain, $code, $prefix );
            throw new \Controller_Jobs_Exception( $msg );
        }
    
        return $item;
    }    
    
    protected function _getContext()
    { 
        
        $settings = self::convertTypoScriptArrayToPlainArray($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_aimeos.']['settings.']);
        $config = self::getConfig( $settings);
        $context = $this->getContext( $config );
        
        $localI18n = ( isset( $settings['i18n'] ) ? $settings['i18n'] : array() );
        $locale = $this->getLocale( $context );
        $langid = $locale->getLanguageId();
        
        if(is_null($this->_config[$langid])){
            $context->setLocale( $locale );
            $context->setI18n( \Aimeos\Aimeos\Base::getI18n( array( $langid ), $localI18n ) );
            $this->_config[$langid] = $context;
        }
        
        return $this->_config[$langid];
    }
    
    /**
     * Creates a new configuration object.
     *
     * @param array $local Multi-dimensional associative list with local configuration
     * @return MW_Config_Interface Configuration object
     */
    public static function getConfig( array $local = array() )
    {
        if( self::$config === null )
        {
            $configPaths = \Aimeos\Aimeos\Base::getAimeos()->getConfigPaths( 'mysql' );
            
            // Hook for processing extension config directories
            if( is_array( $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['aimeos']['confDirs'] ) )
            {
                ksort( $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['aimeos']['confDirs'] );
                
                foreach( $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['aimeos']['confDirs'] as $dir )
                {
                    $absPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName( $dir );
                    if( !empty( $absPath ) ) {
                        $configPaths[] = $absPath;
                    }
                }
            }
            
            $conf = new \MW_Config_Array( array(), $configPaths );
            
            if( function_exists( 'apc_store' ) === true && self::getExtConfig( 'useAPC', false ) == true ) {
                $conf = new \MW_Config_Decorator_APC( $conf, self::getExtConfig( 'apcPrefix', 't3:' ) );
            }
            
            self::$config = $conf;
        }
        
        return new \MW_Config_Decorator_Memory( self::$config, $local );
    }
 
    /**
     * Returns the current context.
     *
     * @param \MW_Config_Interface Configuration object
     * @return MShop_Context_Item_Interface Context object
     */
    public static function getContext( \MW_Config_Interface $config )
    {
        if( self::$context === null )
        {
            $context = new \MShop_Context_Item_Typo3();
            $context->setConfig( $config );
            
            $dbm = new \MW_DB_Manager_PDO( $config );
            $context->setDatabaseManager( $dbm );
            
            $logger = \MAdmin_Log_Manager_Factory::createManager( $context );
            $context->setLogger( $logger );
            
            $cache = self::getCache( $context );
            $context->setCache( $cache );
            
            $mailer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance( 'TYPO3\CMS\Core\Mail\MailMessage' );
            $context->setMail( new \MW_Mail_Typo3( $mailer ) );
            
            if( \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded( 'saltedpasswords' )
                && \TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled( 'FE' )
                ) {
                    $object = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance();
                    $context->setHasherTypo3( $object );
                }
                
                if( isset( $GLOBALS['TSFE']->fe_user ) ) {
                    $session = new \MW_Session_Typo3( $GLOBALS['TSFE']->fe_user );
                } else {
                    $session = new \MW_Session_None();
                }
                $context->setSession( $session );
                
                self::$context = $context;
        }
        
        self::$context->setConfig( $config );
        
        return self::$context;
    }
    
    /**
     * Returns the extension configuration.
     *
     * @param string Name of the configuration setting
     * @param mixed Value returned if no value in extension configuration was found
     * @return mixed Value associated with the configuration setting
     */
    public static function getExtConfig( $name, $default = null )
    {
        if( self::$extConfig === null )
        {
            if( ( $conf = unserialize( $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['aimeos'] ) ) === false ) {
                $conf = array();
            }
            
            self::$extConfig = $conf;
        }
        
        if( isset( self::$extConfig[$name] ) ) {
            return self::$extConfig[$name];
        }
        
        return $default;
    }
    
    /**
     * Returns the cache object for the context
     *
     * @param \MShop_Context_Item_Interface $context Context object including config
     * @param string $siteid Unique site ID
     * @return \MW_Cache_Interface Cache object
     */
    protected static function getCache( \MShop_Context_Item_Interface $context )
    {
        $config = $context->getConfig();
        
        switch( \Aimeos\Aimeos\Base::getExtConfig( 'cacheName', 'Typo3' ) )
        {
            case 'None':
                $config->set( 'client/html/basket/cache/enable', false );
                return \MW_Cache_Factory::createManager( 'None', array(), null );
                
            case 'Typo3':
                if( class_exists( '\TYPO3\CMS\Core\Cache\Cache' ) ) {
                    \TYPO3\CMS\Core\Cache\Cache::initializeCachingFramework();
                }
                $manager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance( 'TYPO3\\CMS\\Core\\Cache\\CacheManager' );
                
                return new \MAdmin_Cache_Proxy_Typo3( $context, $manager->getCache( 'aimeos' ) );
                
            default:
                return new \MAdmin_Cache_Proxy_Default( $context );
        }
    }
    
    protected function getLocale( \MShop_Context_Item_Interface $context )
    {

        $session = $context->getSession();
        $config = $context->getConfig();
        
        
        $sitecode = $config->get( 'mshop/locale/site', 'default' );
        $name = $config->get( 'typo3/param/name/site', 'loc-site' );
        
        $langid = $config->get( 'mshop/locale/language', '' );
        $name = $config->get( 'typo3/param/name/language', 'loc-language' );
        
        if( empty($langid) && !is_null(GeneralUtility::_GP('L')) ) {
            $langid = $this->getLangCodeById(GeneralUtility::_GP('L'));
        }
        
        if(isset( $this->sys_language_uid ) ) {
            $langid = $this->getLangCodeById($this->sys_language_uid);
        }
        
        if( empty($langid) && isset( $GLOBALS['TSFE']->config['config']['language'] ) ) {
            $langid = $GLOBALS['TSFE']->config['config']['language'];
        }
        
        $currency = $config->get( 'mshop/locale/currency', '' );
        $name = $config->get( 'typo3/param/name/currency', 'loc-currency' );
        
        if( !isset( self::$locale[$langid] ) )
        {
            $localeManager = \MShop_Locale_Manager_Factory::createManager( $context );
            self::$locale[$langid] = $localeManager->bootstrap( $sitecode, $langid, $currency );
        }
    
        return self::$locale[$langid];
    }
   
    protected function getLangCodeById($language)
    {
        $result='de';
    
        if(isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'])){
            $preVars = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'];
            foreach ($preVars as $pre){
                if($pre['GETvar']==$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['pagePath']['languageGetVar']){
                    $code = array_search($language, $pre['valueMap']);
                    if($code!==false){
                        $result=$code;
                    }
                }
            }
        }
        return $result;
    }
    
    protected function getLangIdByCode($language)
    {
        $result=0;
        if(isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'])){
            $preVars = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'];
            foreach ($preVars as $pre){
                if($pre['GETvar']==$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['pagePath']['languageGetVar']){
                    if(isset($pre['valueMap'][$language])){
                        $result=$pre['valueMap'][$language];
                    }
                }
            }
        }
        return $result;
    }

    protected function validateLang($language)
    {
        $result=0;
        if(isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'])){
            $preVars = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'];
            foreach ($preVars as $pre){
                if($pre['GETvar']==$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['pagePath']['languageGetVar']){
                    $result=in_array($language, $pre['valueMap']);
                }
            }
        }
        return $result;
    }
    
    protected function _getManager($context, $type){
    
        if( !isset( self::$_managers[$type] ) ){
            self::$_managers[$type] = \MShop_Factory::createManager( $context, $type );
        }
        return self::$_managers[$type];
    }
    
    protected function getParameters($args){
       $urlParameters = array();
         if (isset($args['addParams'])) {
                $parts = GeneralUtility::trimExplode('&', $args['addParams']);
                foreach ($parts as $part) {
                    list($parameter, $value) = explode('=', $part);
                    $urlParameters[urldecode($parameter)] = urldecode($value);
                }
         }
       return $urlParameters;  
    }
    
    protected static function convertTypoScriptArrayToPlainArray(array $typoScriptArray)
    {
        foreach ($typoScriptArray as $key => &$value) {
            if (substr($key, -1) === '.') {
                $keyWithoutDot = substr($key, 0, -1);
                $hasNodeWithoutDot = array_key_exists($keyWithoutDot, $typoScriptArray);
                $typoScriptNodeValue = $hasNodeWithoutDot ? $typoScriptArray[$keyWithoutDot] : NULL;
                if (is_array($value)) {
                    $typoScriptArray[$keyWithoutDot] = self::convertTypoScriptArrayToPlainArray($value);
                    if (!is_null($typoScriptNodeValue)) {
                        $typoScriptArray[$keyWithoutDot]['_typoScriptNodeValue'] = $typoScriptNodeValue;
                    }
                    unset($typoScriptArray[$key]);
                } else {
                    $typoScriptArray[$keyWithoutDot] = NULL;
                }
            }
        }
        return $typoScriptArray;
    }
    
    public function getCategory(&$params, &$ref){
        //TODO: fix Exception AIMEOS
        try{
               $this->params = $params;
               if($ref instanceof \DmitryDulepov\Realurl\Encoder\UrlEncoder ){
                   $orig = $ref->getOriginalUrlParameters();
               }
               $this->sys_language_uid = $GLOBALS['TSFE']->sys_language_uid;
               if(isset($orig['L'])){
                   $this->sys_language_uid = $this->validateLang($orig['L'])?$orig['L']:0;
               }
               $result = $this->params['value'];
               if ($this->params['decodeAlias']) {
                   $result = $this->convertAliasToId($this->params['setup'], $this->params['value']);
                   return $result;
               } else {
                   if($this->params['value'] && MathUtility::canBeInterpretedAsInteger($this->params['value']) && intval($this->params['value'])>0){
                       $manager = $this->_getManager( $this->_getContext(), 'catalog');
                       $catalog = $manager->getItem($this->params['value'],array('text'));
                       $dName = reset($catalog->getRefItems('text', 'DisplayName'));
                       $path = $ref->cleanUpAlias($this->_cfg,$dName && !empty($dName->getContent()) ? $dName->getContent() : $catalog->getName());

                       $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('tablename', 'tx_realurl_uniqalias',
                               'value_alias=' .$GLOBALS['TYPO3_DB']->fullQuoteStr($path, 'tx_realurl_uniqalias').
                               ' AND tablename="mshop_catalog" AND value_id!='.intval($this->params['value']).
                               ' AND lang='. $this->sys_language_uid );

                       $pages = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('pagepath', 'tx_realurl_pathdata',
                               '(pagepath like "%/' .$path.'/%" OR pagepath like "%/' .$path.'") AND language_id='. $this->sys_language_uid );
                       if(isset($row['tablename']) || isset($pages['pagepath'])){
                           $path .='-'.$this->params['value'];
                       }

                       if($path && $catalog->getStatus()==1 && $catalog->getNode()->__get('level')>3){
                           $result=$path;
                           $this->storeInAliasCache($this->params['setup'], $result, $this->params['value'], $this->sys_language_uid, $ref);
                       }

                   }
                   return $result;
               }
        }catch (\Exception $exception){
            $GLOBALS['TSFE']->pageNotFoundAndExit();
        }
    }

    public function getProduct(&$params, &$ref){
        //TODO: fix Exception AIMEOS
        try{
            $this->params = $params;
            if($ref instanceof \DmitryDulepov\Realurl\Encoder\UrlEncoder ){
               $orig = $ref->getOriginalUrlParameters();
            }

            $this->sys_language_uid = $GLOBALS['TSFE']->sys_language_uid;
            if(isset($orig['L'])){
                $this->sys_language_uid = $this->validateLang($orig['L'])?$orig['L']:0;
            }

            $result = $this->params['value'];
            if ($this->params['decodeAlias']) {
                $result = $this->convertAliasToId($this->params['setup'], $this->params['value']);
                return $result;
            } else {
                if($this->params['value'] && MathUtility::canBeInterpretedAsInteger($this->params['value'])){
                    $manager = $this->_getManager( $this->_getContext(), 'product');
                    $product = $manager->getItem($this->params['value'],array('text','attribute'));
                    $dName = reset($product->getRefItems('text', 'TitleProductSubSeries'));
                    $aMaterialId = reset($product->getRefItems('attribute', 'AT_SAP_MainMaterialID'));
                    $path = $ref->cleanUpAlias($this->_cfg,$dName && !empty($dName->getContent()) ? $dName->getContent() : ($aMaterialId && !empty($aMaterialId->getName()) ? $aMaterialId->getName(): $product->getName()));
                    $path .='-'.$this->params['value'];
                    /*$row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('tablename', 'tx_realurl_uniqalias',
                            'value_alias=' .$GLOBALS['TYPO3_DB']->fullQuoteStr($path, 'tx_realurl_uniqalias').
                            ' AND value_id!='.$this->params['value'].
                            ' AND lang='. $this->sys_language_uid );

                    if(isset($row['tablename'])){
                        $path .='-'.$this->params['value'];
                    }*/

                    if($path && $product->getStatus()==1){
                        $result=$path;
                        $this->storeInAliasCache($this->params['setup'], $result, $this->params['value'], $this->sys_language_uid, $ref);
                    }

                }
                return $result;
            }
        }catch (\Exception $exception){
            $GLOBALS['TSFE']->pageNotFoundAndExit();
        }
    }
    
    
    
    protected function storeInAliasCache(array $configuration, $newAliasValue, $idValue, $languageUid , &$ref)
    {
        $newAliasValue = $ref->cleanUpAlias($configuration, $newAliasValue);
        
        if ($configuration['autoUpdate'] && $this->getFromAliasCache($configuration, $idValue, $languageUid, $newAliasValue)) {
            return $newAliasValue;
        }
        
        $uniqueAlias = $this->createUniqueAlias($configuration, $newAliasValue, $idValue);

        // if no unique alias was found in the process above, just suffix a hash string and assume that is unique...
        if (!$uniqueAlias) {
            $newAliasValue .= '-' . GeneralUtility::shortMD5(microtime());
            $uniqueAlias = $newAliasValue;
        }
        
        // Checking that this alias hasn't been stored since we looked last time
        $returnAlias = $this->getFromAliasCache($configuration, $idValue, $languageUid, $uniqueAlias);
        if ($returnAlias) {
            // If we are here it is because another process managed to create this alias in the time between we looked the first time and now when we want to put it in database.
            $uniqueAlias = $returnAlias;
        } else {
            // Store new alias
            $insertArray = array(
                    'tablename' => $configuration['table'],
                    'field_alias' => $configuration['alias_field'],
                    'field_id' => $configuration['id_field'],
                    'value_alias' => $uniqueAlias,
                    'value_id' => $idValue,
                    'lang' => $languageUid
            );
            
            $GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_realurl_uniqalias', $insertArray);
            $aliasRecordId = $GLOBALS['TYPO3_DB']->sql_insert_id();
        }
        
        return $uniqueAlias;
    }
    
    
    protected function getFromAliasCache(array $configuration, $getVarValue, $languageUid, $onlyThisAlias = '') {
        $result = NULL;
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('*', 'tx_realurl_uniqalias',
                'value_id=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($getVarValue, 'tx_realurl_uniqalias') .
                ' AND field_alias=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configuration['alias_field'], 'tx_realurl_uniqalias') .
                ' AND field_id=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configuration['id_field'], 'tx_realurl_uniqalias') .
                ' AND tablename=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configuration['table'], 'tx_realurl_uniqalias') .
                ' AND lang=' . intval($languageUid) .
                ($onlyThisAlias ? ' AND value_alias=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($onlyThisAlias, 'tx_realurl_uniqalias') : '') .
                ' AND expire=0'
                );
        if (is_array($row)) {
            $result = $row['value_alias'];
        }
    
        return $result;
    }
    
    protected function createUniqueAlias(array $configuration, $newAliasValue, $idValue) {
        $uniqueAlias = '';
        $counter = 0;
        $maxTry = 100;
        $testNewAliasValue = $newAliasValue;
        while ($counter < $maxTry) {
            // If the test-alias did NOT exist, it must be unique and we break out
            $foundId = $this->getFromAliasCacheByAliasValue($configuration, $testNewAliasValue, TRUE);
            if (!$foundId || $foundId == $idValue) {
                $uniqueAlias = $testNewAliasValue;
                break;
            }
            $counter++;
            $testNewAliasValue = $newAliasValue . '-' . $counter;
        }
    
        return $uniqueAlias;
    }
    
    protected function getFromAliasCacheByAliasValue(array $configuration, $aliasValue, $onlyNonExpired) {
        /** @noinspection PhpUndefinedMethodInspection */
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('value_id', 'tx_realurl_uniqalias',
                'value_alias=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($aliasValue, 'tx_realurl_uniqalias') .
                ' AND field_alias=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configuration['alias_field'], 'tx_realurl_uniqalias') .
                ' AND field_id=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configuration['id_field'], 'tx_realurl_uniqalias') .
                ' AND tablename=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configuration['table'], 'tx_realurl_uniqalias') .
                ' AND ' . ($onlyNonExpired ? 'expire=0' : '(expire=0 OR expire>' . time() . ')'));
        return (is_array($row) ? (int)$row['value_id'] : false);
    }
    
    protected function convertAliasToId(array $configuration, $value) {
        $result = (string)$value;
    
        // Assemble list of fields to look up. This includes localization related fields
        $translationEnabled = FALSE;
        $fieldList = array();
        if ($configuration['languageGetVar'] && $configuration['transOrigPointerField'] && $configuration['languageField']) {
            $fieldList[] = 'uid';
            $fieldList[] = $configuration['transOrigPointerField'];
            $fieldList[] = $configuration['languageField'];
            $translationEnabled = TRUE;
        }
    
        $cachedId = $this->getFromAliasCacheByAliasValue($configuration, $value, FALSE);
        if (MathUtility::canBeInterpretedAsInteger($cachedId)) {
           $result = (int)$cachedId;
        }
    
/*        if (!is_int($result)) {
            $fieldList[] = $configuration['id_field'];
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(implode(',', $fieldList),
                    $configuration['table'],
                    $configuration['alias_field'] . '=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($value, $configuration['table']) .
                    ' ' . $configuration['addWhereClause']);
                    if (is_array($row)) {
                        $result = (int)$row[$configuration['id_field']];
    
                        // If localization is enabled, check if this record is a localized version and if so, find uid of the original version.
                        if ($translationEnabled && $row[$configuration['languageField']] > 0) {
                            $result = (int)$row[$configuration['transOrigPointerField']];
                        }
                    }
        }
*/    
        return $result;
    }
    protected function _setCached($language, $key, $value)
    {
        
        $context = $this->_getContext();
        try
        {
            $context->getCache()->set(md5( 'realurl-'.json_encode( $key).$language), $value, array(), null );
            
        }
        catch( \Exception $e )
        {die(print_r($e->getMessage(),1));
            $msg = sprintf( 'Unable to set cache entry: %1$s', $e->getMessage() );
            $context->getLogger()->log( $msg, \MW_Logger_Abstract::NOTICE );
        }
    }
    
    protected function _getCached($language, $key)
    {
        if( !isset( $this->_cache[md5(json_encode($key).$language)] ) ){
            $context = $this->_getContext();
            try
            {
                $this->_cache[md5(json_encode($key).$language)] = $context->getCache()->get( md5('realurl-'.json_encode( $key).$language));
            }
            catch( \Exception $e )
            {
                $msg = sprintf( 'Unable to set cache entry: %1$s', $e->getMessage() );
                $context->getLogger()->log( $msg, \MW_Logger_Abstract::NOTICE );
            }
        }
        return $this->_cache[md5(json_encode($key).$language)];
    }
    
}
