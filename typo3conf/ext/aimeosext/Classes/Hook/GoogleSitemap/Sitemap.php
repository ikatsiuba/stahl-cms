<?php
namespace Aimeos\Aimeosext\Hooks\GoogleSitemap;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

class Sitemap
{

    private static $_managers = array();
    private static $_locale = array();
    private $_cache = array();
    protected $_config = array();
    
    /**
     * A sitemap renderer
     *
     * @var	AbstractSitemapRenderer
     */
    protected $renderer;
    
    protected $_catalogList = array();
    protected $_catalogCategoryList = array();
    
    /**
     * cObject to generate links
     *
     * @var	\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    public $cObj = null;
    
    
    public function postProcessPageInfo($params)
    {
        @set_time_limit(3600);
        $this->renderer = $params['renderer'];
        if(is_null($this->cObj)){
            $this->cObj = GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
            $this->cObj->start(array());
        }
        $this->_getAimeosDataForPage($params['pageInfo']);
        
    }
    
    protected function _getAimeosDataForPage($pageInfo)
    {
        if($pageInfo['uid']>0){
            $language = GeneralUtility::_GET('L');
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                'uid, pi_flexform',
                'tt_content',
                'pid = ' . $pageInfo['uid']. ' and list_type = \'aimeos_catalog-filter\' and hidden=0  and deleted=0 and sys_language_uid in(-1,'.$language.') and pid not in (5,6)'
                );
            
            if(!empty($row)){
                $flex = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($row['pi_flexform']);
                if(!empty($flex['data']['sDEF']['lDEF']['settings.client.html.catalog.filter.tree.startid']['vDEF'])){
                    //file_put_contents(PATH_site."typo3temp/sitemap.xxx", __METHOD__.'::pid'.$pageInfo['uid']."\n", FILE_APPEND);
                    //$time1 = microtime(true);
                    
                     $catalogId = $flex['data']['sDEF']['lDEF']['settings.client.html.catalog.filter.tree.startid']['vDEF'];
                     $context = $this->_getContext($language);
                     $controller = \Controller_Frontend_Factory::createController( $context, 'catalog' );
                     $tree = $controller->getCatalogTree( $catalogId, array('product'), \MW_Tree_Manager_Abstract::LEVEL_TREE);
                     
                     //$time13 = microtime(true);
                     $this->_getCatalog($pageInfo, $tree, $language);
                    // $time23 = microtime(true);
                     //file_put_contents(PATH_site."typo3temp/sitemap.xxx", __METHOD__.'_getCatalog::'.($time23-$time13)."\n", FILE_APPEND);
                     
                    //$time2 = microtime(true);
                    //file_put_contents(PATH_site."typo3temp/sitemap.xxx", __METHOD__.'::'.($time2-$time1)."\n", FILE_APPEND);
                }
                
            }
        }
    }
    
    protected function _getCatalog($pageInfo, $tree, $language  )
    {
        
        foreach( $tree->getChildren() as $child ) {
            if($child->getStatus()==1){
                $this->_renderObject($child, $pageInfo, $language);
                
                $this->_getCatalog( $pageInfo, $child, $language);
            }
        }
        $catId=$tree->getId();
        $products = $tree->getRefItems( 'product', null, 'default' );
        if(count($products)){
            $context = $this->_getContext($language);
            $manager = $this->getManager($context, 'product');
            foreach ($products as $child){
                if($child && $child->getStatus()==1){
                    //$product = $manager->getItem($child->getId());
                    $this->_renderObject($child, $pageInfo, $language, null, $catId);
                    $this->_renderObject($child, $pageInfo, $language, null, $catId,true);
                    
                    /*$ids = $this->_getRefProductsArray($child->getId());
                    if(count($ids)){
                        $search = $manager->createSearch(true);
                        $expr = array(
                            $search->compare( '==', 'product.id', $ids ),
                            $search->getConditions()
                        );
                        $search->setConditions( $search->combine( '&&', $expr ) );
                        $search->setSlice( 0, 0x7FFFFFFF );
                        $list = $manager->searchItems( $search);
                        foreach($list as $productVariant){
                            $this->_renderObject($productVariant, $pageInfo, $language, $child, $catId);
                        }
                        //foreach($ids as $productVariant){
                        //    $this->_renderObject($productVariant, $pageInfo, $language, $child, $catId);
                        //}
                    }*/
                    //unset($list,$search, $ids);
                }
                
            }
        }
    }
    
    protected function _renderObject($aimeosObject, $pageInfo, $language, $aimeosParent=null, $catId=6, $download=false)
    {   
        $additionalParams ='&L='.$language.'&ai[controller]=catalog';
        $priority = 1;
        $siteId= $this->_getContext($language)->getLocale()->getSiteId();
        $dateModification = $aimeosObject->getTimeModified();
        if($aimeosObject instanceof \MShop_Catalog_Item_Interface){
            $additionalParams .='&ai[action]=list&ai[f_catid]='.$aimeosObject->getId();
            $priority = 10;
            
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'max(mtime) as mtime',
                    'mshop_catalog_list',
                    'siteid='.$siteId.' and parentid = ' .$aimeosObject->getId());
            if(isset($row['mtime'])){
                $dateModification = max($dateModification,$row['mtime']);
            }
            
        }elseif($aimeosObject instanceof \MShop_Product_Item_Interface){
            $additionalParams .='&ai[action]=detail&ai[f_catid]='.$catId.'&ai[d_prodid]='.$aimeosObject->getId();
            
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'max(mtime) as mtime',
                    'mshop_product_list',
                    'siteid='.$siteId.' and parentid = ' .$aimeosObject->getId());
            if(isset($row['mtime'])){
                $dateModification = max($dateModification,$row['mtime']);
            }
            
            if($download){
                $additionalParams .='&ai[listType]=downloads';
                
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'max(mtime) as mtime',
                        'mshop_product_list',
                        'siteid='.$siteId.' and parentid in (select prodid from mshop_catalog_index_product where siteid='.$siteId.' and pssid =' .$aimeosObject->getId().')');
                if(isset($row['mtime'])){
                    $dateModification = max($dateModification,$row['mtime']);
                }
            }
            $priority = 8;
            if(!is_null($aimeosParent)){
                $additionalParams .='&ai[d_parent]='.$aimeosParent->getId();
                $priority = 5;
            }
        }/*elseif($aimeosObject instanceof \MShop_Common_Item_List_Interface){
            $additionalParams .='&ai[action]=detail&ai[f_catid]='.$catId.'&ai[d_prodid]='.$aimeosObject->getRefId();
            $priority = 8;
            if(!is_null($aimeosParent)){
                $additionalParams .='&ai[d_parent]='.$aimeosParent->getId();
                $priority = 5;
            }
        }*/

        if ($url = $this->getPageLink($pageInfo['uid'],$additionalParams)) {
            echo $this->renderer->renderEntry($url, '',
                    strtotime($dateModification),
                'daily','',$priority);
        }
    }
    
    /**
     * Creates a link to a single page
     *
     * @param	array	$pageId	Page ID
     * @return	string	Full URL of the page including host name (escaped)
     */
    protected function getPageLink($pageId, $addititonalParams, $absolute=true) {
        $conf = array(
            'parameter' => $pageId,
            'returnLast' => 'url',
            'additionalParams' => $addititonalParams
        );
        $link = htmlspecialchars($this->cObj->typoLink('', $conf));
        if($absolute){
            return GeneralUtility::locationHeaderUrl($link);
        }else{
            return $link;
        }
    }
    
    protected function _getContext($language= null)
    {
        if(is_null($this->_config[$language])){
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
            $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
            $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,'aimeos');
            $config = \Aimeos\Aimeos\Base::getConfig( $settings);
            $context = \Aimeos\Aimeos\Base::getContext( $config );
            
            $localI18n = ( isset( $settings['i18n'] ) ? $settings['i18n'] : array() );
            $locale = $this->getLocale( $context, $language );
            $langid = $locale->getLanguageId();
            $context->setLocale( $locale );
            $context->setI18n( \Aimeos\Aimeos\Base::getI18n( array( $langid ), $localI18n ) );
            $this->_config[$language] = $context;
        }
        
        return $this->_config[$language];
    }
    
    protected function getLocale( \MShop_Context_Item_Interface $context, $language = null )
    {
        if( !isset( self::$_locale[$language] ) )
        {
            $session = $context->getSession();
            $config = $context->getConfig();
            
            $sitecode = $config->get( 'mshop/locale/site', 'default' );
            $name = $config->get( 'typo3/param/name/site', 'loc-site' );
            
            $langid = $config->get( 'mshop/locale/language', '' );
            $name = $config->get( 'typo3/param/name/language', 'loc-language' );
            
            if( !is_null( $language ) ) {
                $langid = $this->getLangCodeById($language);
            }
            $currency = $config->get( 'mshop/locale/currency', '' );
            $name = $config->get( 'typo3/param/name/currency', 'loc-currency' );
            
            $localeManager = \MShop_Locale_Manager_Factory::createManager( $context );
            self::$_locale[$language] = $localeManager->bootstrap( $sitecode, $langid, $currency );
        }
        
        return self::$_locale[$language];
    }
    
    protected function getLangCodeById($language)
    {
        $result='de';
        
        if(isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'])){
            $preVars = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'];
            foreach ($preVars as $pre){
                if($pre['GETvar']==$GLOBALS['TSFE']->config['config']['linkVars']){
                    $code = array_search($language, $pre['valueMap']);
                    if($code!==false){
                        $result=$code;
                    }
                }
            }
        }
        return $result;
    }
    
    protected function getManager($context, $type){
        
        if( !isset( self::$_managers[$type] ) ){
            self::$_managers[$type] = \MShop_Factory::createManager( $context, $type );
        }
        return self::$_managers[$type];
    }
    
    protected function _getRefProductsArray( $productId)
    {
        $result= array();
        $manager = $this->getManager( $this->_getContext(), 'product/list' );
        $search = $manager->createSearch();
        
        $expr = array(
            $search->compare( '==', 'product.list.domain', 'product' ),
            //$search->compare( '==', 'product.list.typeid', 1 ),
            $search->compare( '==', 'product.list.parentid', $productId ),
        );
        
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSlice( 0, 0x7FFFFFFF );
        $list = $manager->searchItems( $search );
        //return $list;
        foreach($list as $item){
            if($item->getTypeId()==1){
                $result[]=$item->getRefId();
            }
        }
        unset($list,$search,$manager);
        return $result;
    }
    
    public function printProductMenu($content, $conf)
    { 
        $sorting = $productIds = array();
        $language = GeneralUtility::_GET('L');
        if(( $result = $this->_getCached($language, 'product_menu') ) === null){
            $context = $this->_getContext($language);
            $domains = array('product','attribute','text');
            $controller = \Controller_Frontend_Factory::createController( $context, 'catalog' );
            $tree = $controller->getCatalogTree( 3, $domains, \MW_Tree_Manager_Abstract::LEVEL_TREE);
            $rootCatalogList = $context->getConfig()->get( 'client/html/catalog/webcodelist/category', array() );
            $nodes = $tree->getChildren();
            $controller->getProductIds($tree, $productIds); 
            $this->_catalogList = $controller->getCatalogList($productIds);
            $this->_catalogCategoryList = $this->_catalogCategoryList($productIds,$language);
            
            $pManager = $controller->createManager('product');
            $pSearch = $pManager->createSearch(true);
            $expr = array(
                $pSearch->compare('==', 'product.id', $productIds),
                $pSearch->getConditions(),
            );
            $pSearch->setConditions($pSearch->combine('&&', $expr));
            $pSearch->setSlice(0, 0x7FFFFFFF);
            $this->_treeProducts = $pManager->searchItems($pSearch, $domains);
            //sorting nodes
            $sorting = array();
            foreach ($nodes as $node){
                if($node->getStatus()>0){
                    $i=false;
                    if($node instanceof \MShop_Catalog_Item_Interface){
                        $i = reset($node->getRefItems( 'attribute', array('DisplaySequence')));
                    }
                    $sorting[$i?$i->getName().$node->getId():$node->getId()] = $node;
                }
            }
            ksort($sorting);
            $url0 = $GLOBALS['TSFE']->cObj->typoLink_URL(array('parameter' => 6,'forceAbsoluteUrl' => false));
            $label = $context->getI18n()->dt( 'client/html', 'Products');
            $result='<li class="dropdown"><a href="'.$url0.'" title="'.$label.'" class="dropdown-toogle" data-toogle="dropdown" aria-haspopup="true">'.$label.'</a><span class="wrapper-dropdown-menu"><ul class="dropdown-menu second-level scroll-pane">';
            
            foreach( $sorting as $item ){
                if($item->getStatus() > 0 ){
                    $id = $item->getId();
                    $par = array_search($id,$rootCatalogList) ? array_search($id,$rootCatalogList):6;
                    $url = $GLOBALS['TSFE']->cObj->typoLink_URL(array('parameter' => $par,'forceAbsoluteUrl' => false));
                    $dName = reset($item->getRefItems('text', 'DisplayName'));
                    $name = (($dName && !empty($dName->getContent())) ? $dName->getContent() :  $item->getName());
                    $result .='<li>';
                    $result .='<a href="'.$url.'" title="'.$name.'">'.$name.'</a>';
                    if($item->getChildren()>0){
                        $result .='<ul class="third-level">';
                        $result .=$this->_renderTreeElement($item,$language);
                        $result .='</ul>';
                    }
                    $result .='</li>';
                }
            }
            
            $result .='</span></ul></li>';
            $this->_setCached($language, 'product_menu', $result);
        }
        
        return $result;
    }
    
    protected function _renderTreeElement($tree, $language)
    {
        $sorting = array();
        $result = '';
        $detailTarget = 6;
        $context = $this->_getContext($language);
        $rootCatalogList = $context->getConfig()->get( 'client/html/catalog/webcodelist/category', array() );
        $catId=$tree->getId();
        if($tree->getChildren()){
            $items = $tree->getChildren();
        }elseif($tree->getRefItems( 'product', null, 'default' )){
            $items = $tree->getRefItems( 'product', null, 'default' );
        }
        
        foreach ($items as $node) {
            $i = false;
            if($node instanceof \MShop_Catalog_Item_Interface){
                $i = reset($node->getRefItems( 'attribute', array('DisplaySequence')));
            }else{
                if(isset($this->_treeProducts[$node->getId()])){
                    $i = reset($this->_treeProducts[$node->getId()]->getRefItems( 'attribute', array('DisplaySequence')));
                }
            }
            $sorting[$i?$i->getName().$node->getId():$node->getId()] = $node;
        }
        ksort($sorting);
        foreach ($sorting as $item) {
            if($item->getStatus() > 0 ){
                $result .= '<li>';
                $additionalParams ='&L='.$language.'&ai[controller]=catalog';
                if($item instanceof \MShop_Catalog_Item_Interface){
                    $additionalParams .='&ai[action]=list&ai[f_catid]='.$item->getId();
                    $detailTarget = isset($this->_catalogCategoryList[$item->getId()]) ? $this->_catalogCategoryList[$item->getId()]:6;
                }elseif($item instanceof \MShop_Product_Item_Interface){
                    $additionalParams .='&ai[action]=detail&ai[f_catid]='.$catId.'&ai[d_prodid]='.$item->getId();
                    if(isset($this->_catalogList[$item->getId()])){
                        $detailTarget = $this->_catalogList[$item->getId()];
                    }
                }
                if($item instanceof \MShop_Catalog_Item_Interface ){
                    $dName = reset($item->getRefItems('text', 'DisplayName'));
                }else{
                    $productItem = isset($this->_treeProducts[$item->getId()]) ? $this->_treeProducts[$item->getId()] : null;
                    if($productItem && count($productItem->getRefItems('text', 'TitleProductSubSeries'))>1){
                        foreach ($productItem->getRefItems('text', 'TitleProductSubSeries') as $lProduct){
                            if($lProduct->getLanguageId()== $context->getLocale()->getLanguageId()){
                                $dName = $lProduct;
                            }
                        }
                    }else{
                        $dName = $productItem ? reset($productItem->getRefItems('text', 'TitleProductSubSeries')): '';
                    }
                }
                $name = (($dName && !empty($dName->getContent())) ? $dName->getContent() :  $item->getName());
                
                if ($url = $this->getPageLink($detailTarget,$additionalParams, false)) {
                    $result .= '<a href="'.$url.'">'.$name.'</a>';
                    if($item instanceof \MShop_Catalog_Item_Interface && count( $item->getChildren() ) > 0){
                        $result .='<ul  class="fourth-level">';
                        $result .= $this->_renderTreeElement($item,$language);
                        $result .= '</ul>';
                    }
                    if($item instanceof \MShop_Catalog_Item_Interface && $item->getNode()->__get('level')<=4 && count( $item->getRefItems( 'product', null, 'default' )) > 0 ){
                        $result .='<ul  class="fourth-level">';
                        $result .= $this->_renderTreeElement($item,$language);
                        $result .= '</ul>';
                    }
                }
                $result .= '</li>';
            }
        }

        return $result;
    }
    
    protected function _catalogCategoryList($itemData, $language)
    {
        $context = $this->_getContext($language);
        $manager = $this->getManager($context, 'catalog/list' );
        $search = $manager->createSearch( true );
        $expr = array(
//                $search->compare( '==', 'catalog.list.refid', $itemData),
                $search->compare( '==', 'catalog.list.domain', 'text'),
//                $search->compare( '==', 'catalog.list.type.code', 'default'),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSlice( 0, 0x7FFFFFFF );
        $catalogData = $manager->searchItems($search);
        $catalogList = array();
        
        $catalogManager= $this->getManager($context,'catalog');
        $rootCatalogList = $context->getConfig()->get( 'client/html/catalog/webcodelist/category', array() );
        foreach ($catalogData as $catalog){
            if(!isset($catalogList[$catalog->getParentId()])){
                $path = array_keys($catalogManager->getPath($catalog->getParentId()));
                $el= array_intersect($rootCatalogList, $path);
                end($el);
                if(key($el)){
                    $catalogList[$catalog->getParentId()]=key($el);
                }
            }
        }
        
        return $catalogList;
    }
    
    protected function _setCached($language, $key, $value)
    {
        
        $context = $this->_getContext($language);
        $config = $context->getConfig();
        
        try
        {
            $context->getCache()->set( $key.$language, $value, array(), null );
        }
        catch( \Exception $e )
        {
            $msg = sprintf( 'Unable to set cache entry: %1$s', $e->getMessage() );
            $context->getLogger()->log( $msg, \MW_Logger_Abstract::NOTICE );
        }
    }
    
    protected function _getCached($language, $key)
    {
        if( !isset( $this->_cache[$key.$language] ) ){
            $context = $this->_getContext($language);
            $config = $context->getConfig();
            
            try
            {
                $this->_cache[$key.$language] = $context->getCache()->get( $key.$language);
            }
            catch( \Exception $e )
            {
                $msg = sprintf( 'Unable to set cache entry: %1$s', $e->getMessage() );
                $context->getLogger()->log( $msg, \MW_Logger_Abstract::NOTICE );
            }
        }
        return $this->_cache[$key.$language];
    }
}
