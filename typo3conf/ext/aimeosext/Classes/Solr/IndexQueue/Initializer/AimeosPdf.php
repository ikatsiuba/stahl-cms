<?php
namespace Aimeos\Aimeosext\Solr\IndexQueue\Initializer;

use ApacheSolrForTypo3\Solr\IndexQueue\Initializer\AbstractInitializer;


class AimeosPdf extends AbstractInitializer
{

    /**
     * Constructor, sets type and indexingConfigurationName to "pages".
     *
     */
    public function __construct()
    {
        parent::__construct();
    
        $this->type = 'mshop_media';
        $this->indexingConfigurationName = 'aimeosPdf';
    }
    
    /**
     * Overrides the general setType() implementation, forcing type to "pages".
     *
     * @param string $type Type to initialize (ignored).
     */
    public function setType($type)
    {
        $this->type = 'mshop_media';
    }
    
    protected function buildSelectStatement()
    {
        $changedField = 'UNIX_TIMESTAMP(ctime)';
        if (!empty($GLOBALS['TCA'][$this->type]['ctrl']['enablecolumns']['starttime'])) {
            $changedField = 'GREATEST(' . $GLOBALS['TCA'][$this->type]['ctrl']['enablecolumns']['starttime'] . ',' . $GLOBALS['TCA'][$this->type]['ctrl']['tstamp'] . ')';
        }
        $select = 'SELECT '
            . '\'' . $this->site->getRootPageId() . '\' as root, '
            . '\'' . $this->type . '\' AS item_type, '
            . 'id AS item_uid, '
            . '\'' . $this->indexingConfigurationName . '\' as indexing_configuration, '
            . $this->getIndexingPriority() . ' AS indexing_priority, '
            . $changedField . ' AS changed';

        return $select;
    }
    
    
    protected function buildPagesClause()
    {
        $configuration = $this->indexingConfiguration;
        
        $productTypeIds = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('id','mshop_product_list_type','domain="media" and code in ("RF_VIS","RF_ProuctCommunication","RF_ProductPresentation","RF_TestReport","RF_InternalGeneral")');
        $catalogTypeIds = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('id','mshop_catalog_list_type','domain="media" and code in ("RF_VIS","RF_ProuctCommunication","RF_ProductPresentation","RF_TestReport","RF_InternalGeneral")');
        
        $exclude = ' and link like "%/Files/%.pdf" and id not in (select refid from mshop_product_list where domain="media" and 
typeid in ('.implode(',',array_column($productTypeIds, 'id')).'))
and id not in (select refid from mshop_catalog_list where domain="media" and 
typeid in ('.implode(',',array_column($catalogTypeIds, 'id')).'))
';
        if($configuration['mediaexclude']=='0'){
            $exclude = ' and link like "%/Files%.pdf"';
        }
        //die(print_r([$configuration,$exclude],1));
        return ' (status=0 OR status_solr = 1) '.$exclude;
    }
}
