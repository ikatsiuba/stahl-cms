<?php
namespace Aimeos\Aimeosext\Solr\IndexQueue\Initializer;

use ApacheSolrForTypo3\Solr\IndexQueue\Initializer\Page;

class Aimeos extends Page
{

    /**
     * Constructor, sets type and indexingConfigurationName to "pages".
     *
     */
    public function __construct()
    {
        parent::__construct();
    
        $this->type = 'pages';
        $this->indexingConfigurationName = 'aimeos';
    }
    
    /**
     * Overrides the general setType() implementation, forcing type to "pages".
     *
     * @param string $type Type to initialize (ignored).
     */
    public function setType($type)
    {
        $this->type = 'pages';
    }
    
}
