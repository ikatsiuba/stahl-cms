<?php
namespace Aimeos\Aimeosext\Solr\IndexQueue;
use ApacheSolrForTypo3\Solr\IndexQueue\Indexer;
use ApacheSolrForTypo3\Solr\IndexQueue\Item;
use Apache_Solr_Document;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solr\HtmlContentExtractor;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\Access\Rootline;


/**
 * A special purpose indexer to index pages.
 *
 * In the case of pages we can't directly index the page records, we need to
 * retrieve the content that belongs to a page from tt_content, too. Also
 * plugins may be included on a page and thus may need to be executed.
 *
 * @author Ingo Renner <ingo@typo3.org>
 */
class AimeosIndexer extends Indexer
{
    private static $_managers = array();
    private static $_locale = array();
    protected $_config = array();
    
    
    /**
     * Creates a single Solr Document for an item in a specific language.
     *
     * @param Item $item An index queue item to index.
     * @param int $language The language to use.
     * @return bool TRUE if item was indexed successfully, FALSE on failure
     */
    protected function indexItem(Item $item, $language = 0)
    {
        $itemIndexed = false;
        $documents = array();

        $documents = $this->getAimeosInstanceForPage($item, $language);
        $documents = $this->processDocuments($item, $documents);
        $documents = $this->preAddModifyDocuments($item, $language, $documents);
        if('pages' == $item->getType()) {
            if($uid = $item->getRecordUid()) {
                $rawQuery = 'uid:'.$uid.' && type:products';
                $this->solr->deleteByQuery( $rawQuery,true,true,0);
            }
        }
        $response = $this->solr->addDocuments($documents);
        if ($response->getHttpStatus() == 200) {
            $itemIndexed = true;
            //GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Indexing\\Indexer')->processIndexQueue(50, false);
        }
        $this->log($item, $documents, $response);
    
        return $itemIndexed;
    }

    
    protected function getAimeosInstanceForPage(Item $item, $language = 0 )
    {
        $itemRecord = $item->getRecord();
        $documents = array();
        
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                'uid, pi_flexform',
                'tt_content',
                'pid = ' . $itemRecord['uid']. ' and list_type = \'aimeos_catalog-filter\' and hidden=0  and deleted=0 and sys_language_uid in(-1,'.$language.')' 
                );
        if(!empty($row)){
            $flex = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($row['pi_flexform']);
            if(!empty($flex['data']['sDEF']['lDEF']['settings.client.html.catalog.filter.tree.startid']['vDEF'])){
               $catalogId = $flex['data']['sDEF']['lDEF']['settings.client.html.catalog.filter.tree.startid']['vDEF'];
               $context = $this->_getContext($language);
               $controller = \Controller_Frontend_Factory::createController( $context, 'catalog' );
               $tree = $controller->getCatalogTree( $catalogId, array( 'text','attribute','product' ), \MW_Tree_Manager_Abstract::LEVEL_TREE/*, $search*/ );
               
               $documents = $this->_getCatalog($item, $tree, $catalogId, $documents, $language);
            }
            
        }
        return $documents;
    }

    
    protected function postProcessPageDocument(Item $item, \Apache_Solr_Document $pageDocument, $mediaData, $params,  $language, $aimeosId)
    {
        $record = $item->getRecord();
        $site = Site::getSiteByPageId($record['uid']);
        $accessField = $pageDocument->getField('access');
        $pageAccessRootline = new Rootline($accessField['value']);
        
        /** @var PageContextDetector $pageContextDetector */
        $pageContextDetector = GeneralUtility::makeInstance('ApacheSolrForTypo3\Solrfal\Detection\PageContextDetector', $site);
        $pageContextDetector->addDetectedFilesToPageAimeos($item, $pageAccessRootline, $mediaData, $params, $language, $aimeosId);
    }    
    
    /**
     * Converts an item array (record) to a Solr document by mapping the
     * record's fields onto Solr document fields as configured in TypoScript.
     *
     * @param Item $item An index queue item
     * @param int $language Language Id
     * @return Apache_Solr_Document The Solr document converted from the record
     */
    protected function itemToDocumentAimeos(Item $item, $language = 0, $aimeosObject, $aimeosParent = null, $aimeosCat = null)
    {
        $document = null;

        $itemRecord = $this->getFullItemRecordAimeos($item, $language, $aimeosObject, $aimeosParent,$aimeosCat);
        if (!is_null($itemRecord)) {
            $itemIndexingConfiguration = $this->getItemTypeConfiguration($item,
                    $language);
            $itemIndexingConfiguration['url.']['typolink.']['parameter']=$itemRecord['uid'];
            $itemIndexingConfiguration['url.']['typolink.']['additionalParams']='&L='.$language.'&ai[controller]=catalog';
            if($aimeosObject instanceof \MShop_Catalog_Item_Interface){
                $itemIndexingConfiguration['url.']['typolink.']['additionalParams'] .='&ai[action]=list&ai[f_catid]='.$itemRecord['aimeos.id'];
            }elseif($aimeosObject instanceof \MShop_Product_Item_Interface){
                $itemIndexingConfiguration['url.']['typolink.']['additionalParams'] .= '&ai[action]=detail';
                if($aimeosCat && is_null($aimeosParent) && $aimeosCat->getId()) {
                    $itemIndexingConfiguration['url.']['typolink.']['additionalParams'] .= '&ai[f_catid]=' . $aimeosCat->getId();
                }
                $itemIndexingConfiguration['url.']['typolink.']['additionalParams'] .= '&ai[d_prodid]='.$itemRecord['aimeos.id'];
                if(!is_null($aimeosParent)){
                    $itemIndexingConfiguration['url.']['typolink.']['additionalParams'] .='&ai[d_parent]='.$aimeosParent->getId();
                }
            }
            $document = $this->getBaseDocument($item, $itemRecord);
            $document = $this->addDocumentFieldsFromTyposcript($document,
                    $itemIndexingConfiguration, $itemRecord);
            if($url = $document->getField('url')){
                $url['value'] = str_replace(ltrim(PATH_typo3,'/'),'',$url['value']);
                $document->setField($url['name'], $url['value'], $url['boost']);
            }
         /*   $mediaData= array();
            if($aimeosObject instanceof \MShop_Catalog_Item_Interface || !is_null($aimeosParent)){
                $this->_fillMediaArray($mediaData,$aimeosObject,$language);
            }
            if($aimeosObject instanceof \MShop_Product_Item_Interface && !is_null($aimeosParent)){
                $this->_fillMediaArray($mediaData,$aimeosParent,$language);
            }
            
            $this->postProcessPageDocument($item, $document, $mediaData, ['typolink_additional_params'=>$itemIndexingConfiguration['url.']['typolink.']['additionalParams']], $language, $aimeosObject->getId());
        */
        }
        return $document;
    }

    protected function _fillMediaArray(&$mediaData, $aimeosObject, $language)
    {
        $context = $this->_getContext($language);
        $config = $context->getConfig();
        $configData = $config->get('client/html/catalog/detail/additional/download/types',array());
        foreach ($configData as $conf => $type){
            if($media = $aimeosObject->getRefItems('media',null,$type)){
                
                if(strtolower($conf)=='operating_instructions'){
                    $manager = $this->getManager($context, 'media');
                    foreach( $media as $mediaItem ){
                        $element = $manager->getItem($mediaItem->getId(),array('text'));
                        if($mediaItem->getStatus() && $element){
                            $el = reset($element->getRefItems( 'text', 'DVS_DOKST'));
                            if($el && !in_array($el->getContent(),array('FR - freigegeben','RE - Released'))){
                                $mediaData[$mediaItem->getId()]=$mediaItem->getUrl();
                            }
                        }
                    }
                }else{
                    foreach ($media as $mediaItem){
                        if($mediaItem->getStatus()){
                            $mediaData[$mediaItem->getId()]=$mediaItem->getUrl();
                        }
                    }
                }
                
            }
        }
    }
    
    /**
     * Gets the full item record.
     *
     * This general record indexer simply gets the record from the item. Other
     * more specialized indexers may provide more data for their specific item
     * types.
     *
     * @param Item $item The item to be indexed
     * @param int $language Language Id (sys_language.uid)
     * @return array|NULL The full record with fields of data to be used for indexing or NULL to prevent an item from being indexed
     */
    protected function getFullItemRecordAimeos(Item $item, $language = 0, $aimeosObject, $aimeosParent = null)
    {
        $itemRecord = parent::getFullItemRecord($item, $language);
        $langCode = $this->getLangCodeById($language);
        if($aimeosObject){
            $data = $aimeosObject->toArray();
            foreach($data as $key => $val){
                $itemRecord[preg_replace('/catalog|product/', 'aimeos', $key)]=$val;
            }
            $itemRecord['aimeos.text']=$this->getAimeosListItems($aimeosObject);
            if(!is_null($aimeosParent)){
                $itemRecord['aimeos.text'] .=$this->getAimeosListItems($aimeosParent);
                $aMaterialId = reset($aimeosObject->getRefItems('attribute', 'AT_SAP_MainMaterialID',null,$langCode));
                $aimeosObject = $aimeosParent;
            }
            $dName = reset($aimeosObject->getRefItems('text', ($aimeosObject instanceof \MShop_Product_Item_Interface ? 'TitleProductSubSeries' : 'DisplayName'),null,$langCode));
            $itemRecord['aimeos.name'] = ($dName && !empty($dName->getContent())) ?$dName->getContent():$aimeosObject->getName();
            
            if(!empty($aMaterialId)){
                $itemRecord['aimeos.name'] .= ($language?" | Art. No.: ":" | Art. Nr.: ").$aMaterialId->getName();
            }
            
            if($dName = reset($aimeosObject->getRefItems('text', 'AT_WebCode',null,$langCode ))){
                $itemRecord['aimeos.webcode'] = $dName->getContent();
            }
//            $aimeosPath = array('Produkte');
            $aimeosPath = array('');
            
            if($aimeosObject instanceof \MShop_Product_Item_Interface){
                if($catalog = $this->_getParentItem($aimeosObject->getId(), 'catalog',$language)){
                    $path= $this->_getCategoryPath($catalog->getParentId(), $language);
                }
                foreach($path as $ps){
                    $lastEl= end($aimeosPath);
                    $aimeosPath[] = $lastEl.'/'.str_replace('/','|',$ps);
                }
            }else{
                    $lastEl= end($aimeosPath);
                    $aimeosPath[] = $lastEl.'/'.str_replace('/','|',$itemRecord['aimeos.name']);
            }
            $itemRecord['aimeos.path'] = $aimeosPath;
        }
        return $itemRecord;
    }
    
    
    protected function getAimeosListItems($aimeosObject)
    {
        $text='';
        if($aimeosObject){
            $list = $aimeosObject->getListItems('attribute');
            if(!empty($list)){
                foreach ($list as $el){
                    if($refItem = $el->getRefItem()){
                        $text .= ' '.HtmlContentExtractor::cleanContent($refItem->getLabel());
                    }
                }
            }
            $list = $aimeosObject->getListItems('text');
            if(!empty($list)){
                foreach ($list as $el){
                    if($refItem = $el->getRefItem()){
                        $text .= ' '.HtmlContentExtractor::cleanContent($refItem->getContent());
                    }
                }
            }
        }
        return $text;
    }
    
    
    /**
     * Creates a Solr document with the basic / core fields set already.
     *
     * @param Item $item The item to index
     * @param array $itemRecord The record to use to build the base document
     * @return Apache_Solr_Document A basic Solr document
     */
    protected function getBaseDocument(Item $item, array $itemRecord)
    {
        $site = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\Site',
                $item->getRootPageUid());
        $document = GeneralUtility::makeInstance('Apache_Solr_Document');
        /* @var $document Apache_Solr_Document */
    
        // required fields
        $document->setField('id', Util::getDocumentId(
                'products'/*$item->getType()*/,
                $itemRecord['pid'],
                $itemRecord['uid'],
                $itemRecord['aimeos.code']. '/' . $itemRecord['aimeos.id']
                ));
        $document->setField('type', 'products'/*$item->getType()*/);
        $document->setField('appKey', 'EXT:solr');
    
        // site, siteHash
        $document->setField('site', $site->getDomain());
        $document->setField('siteHash', $site->getSiteHash());
    
        // uid, pid
        $document->setField('uid', $itemRecord['uid']);
        $document->setField('pid', $itemRecord['pid']);
    
        // variantId
        $document->setField('variantId', 'products'/*$item->getType()*/ .'/'. $itemRecord['pid']. '/' . $itemRecord['aimeos.code']. '/' . $itemRecord['aimeos.id']);
    
        // created, changed
        if (!empty($GLOBALS['TCA'][$item->getType()]['ctrl']['crdate'])) {
            $document->setField('created',
                    $itemRecord['aimeos.ctime']);
        }
        if (!empty($GLOBALS['TCA'][$item->getType()]['ctrl']['tstamp'])) {
            $document->setField('changed',
                    $itemRecord['aimeos.mtime']);
        }
    
        // access, endtime
        $document->setField('access', $this->getAccessRootline($item));
        if (!empty($GLOBALS['TCA'][$item->getType()]['ctrl']['enablecolumns']['endtime'])
                && $itemRecord[$GLOBALS['TCA'][$item->getType()]['ctrl']['enablecolumns']['endtime']] != 0
                ) {
                    $document->setField('endtime',
                            $itemRecord[$GLOBALS['TCA'][$item->getType()]['ctrl']['enablecolumns']['endtime']]);
                }
    
                return $document;
    }
    
    
    protected function _getCatalog(Item $item, $tree, $currentId, &$documents, $language  )
    {
        if( $tree->getId() == $currentId )
        {
            $documents[]=$this->itemToDocumentAimeos($item, $language, $tree);
        }            
            foreach( $tree->getChildren() as $child ) {
                if($child->getStatus()==1){
                    $documents[] = $this->itemToDocumentAimeos($item, $language, $child);
                    $this->_getCatalog( $item, $child, $currentId, $documents, $language);
                }
            }
            $products = $tree->getRefItems( 'product', null, 'default' );
            if(count($products)){
                $context = $this->_getContext($language);
                $manager = $this->getManager($context, 'product');
                $site = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\Site',$item->getRootPageUid());
                $configuration = $site->getSolrConfiguration()->getObjectByPathOrDefault('plugin.tx_solr.index.queue.aimeos.',[]);
                $productStatus = [1];
                if(!empty($configuration['productstatus'])){
                    $productStatus = (array)explode(',',$configuration['productstatus']);
                }
                foreach ($products as $child){
                    if($child && in_array($child->getStatus(),$productStatus)){
                        $product = $manager->getItem($child->getId(),array('attribute','text'/*,'media'*/));
                        $documents[] = $this->itemToDocumentAimeos($item, $language, $product,null,$tree);
                        
                        $ids = $this->_getRefProductsArray($child->getId(),$language);
                        if(count($ids)){
                            $search = $manager->createSearch(true);
                            $expr = array(
                                    $search->compare( '==', 'product.id', $ids ),
                                    $search->compare( '==', 'product.status', $productStatus ),
                                    $search->getConditions()
                            );
                            $search->setConditions( $search->combine( '&&', $expr ) );
                            $search->setSlice( 0, 0x7FFFFFFF );
                            $list = $manager->searchItems( $search, array('attribute','text'/*,'media'*/) );
                            foreach($list as $productVariant){
                                $documents[] = $this->itemToDocumentAimeos($item, $language, $productVariant, $product,$tree);
                            }
                        }
                        unset($product,$list,$search, $ids);
                    }
                    
                }
            }
        return $documents;
    }
    
    protected function _getRefProductsArray( $productId,$language)
    {
        $result= array();
        $manager = $this->getManager( $this->_getContext($language), 'product/list' );
        $search = $manager->createSearch();
    
        $expr = array(
                $search->compare( '==', 'product.list.domain', 'product' ),
                $search->compare( '==', 'product.list.typeid', 1 ),
                $search->compare( '==', 'product.list.parentid', $productId ),
        );
    
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSlice( 0, 0x7FFFFFFF );
        $list = $manager->searchItems( $search );
    
        foreach($list as $item){
            $result[]=$item->getRefId();
        }
        unset($list,$search,$manager);
        return $result;
    }
    
    protected function getManager($context, $type){
    
        if( !isset( self::$_managers[$type] ) ){
            self::$_managers[$type] = \MShop_Factory::createManager( $context, $type );
        }
        return self::$_managers[$type];
    }
    
    
    protected function _getContext($language= null)
    {
        if(is_null($this->_config[$language])){
            //$settings = self::convertTypoScriptArrayToPlainArray($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_aimeos.']['settings.']);
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
            $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
            $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,'aimeos');
            $config = \Aimeos\Aimeos\Base::getConfig( $settings);
            $context = \Aimeos\Aimeos\Base::getContext( $config );
    
            $localI18n = ( isset( $settings['i18n'] ) ? $settings['i18n'] : array() );
            $locale = $this->getLocale( $context, $language );
            $langid = $locale->getLanguageId();
            $context->setLocale( $locale );
            $context->setI18n( \Aimeos\Aimeos\Base::getI18n( array( $langid ), $localI18n ) );
            $this->_config[$language] = $context;
        }
    
        return $this->_config[$language];
    }
    
    protected function getLocale( \MShop_Context_Item_Interface $context, $language = null )
    {
        if( !isset( self::$_locale[$language] ) )
        {
            $session = $context->getSession();
            $config = $context->getConfig();
    
            $sitecode = $config->get( 'mshop/locale/site', 'default' );
            $name = $config->get( 'typo3/param/name/site', 'loc-site' );
    
            $langid = $config->get( 'mshop/locale/language', '' );
            $name = $config->get( 'typo3/param/name/language', 'loc-language' );

            if( !is_null( $language ) ) {
                $langid = $this->getLangCodeById($language);
            }
            $currency = $config->get( 'mshop/locale/currency', '' );
            $name = $config->get( 'typo3/param/name/currency', 'loc-currency' );
    
            $localeManager = \MShop_Locale_Manager_Factory::createManager( $context );
            self::$_locale[$language] = $localeManager->bootstrap( $sitecode, $langid, $currency );
        }
    
        return self::$_locale[$language];
    }
    
    protected function getLangCodeById($language)
    {  
        $result='de';
        
        if(isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'])){
            $preVars = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['preVars'];
            foreach ($preVars as $pre){
                if($pre['GETvar']==$GLOBALS['TSFE']->config['config']['linkVars']){
                    $code = array_search($language, $pre['valueMap']);
                    if($code!==false){
                        $result=$code; 
                    }
                }
            }
        }
        return $result; 
    }
    
    protected function _getParentItem($prodId,$type='product',$language,$domain='product')
    {
        $typeItem = $this->_getTypeItem( $type.'/list/type', $domain, 'default', $language );
        $manager = $this->getManager( $this->_getContext($language), $type.'/list' );
    
        $search = $manager->createSearch( true );
        $expr = array(
                $search->compare( '==', $type.'.list.refid', $prodId ),
                $search->compare( '==', $type.'.list.typeid', $typeItem->getId() ),
                $search->compare( '==', $type.'.list.domain', $domain ),
                $search->getConditions(),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSortations( array( $search->sort( '+', $type.'.list.position' ) ) );
    
        return reset($manager->searchItems( $search ));
    }
    
    protected function _getTypeItem( $prefix, $domain, $code, $language )
    {
        $manager = $this->getManager( $this->_getContext($language), $prefix );
        $prefix = str_replace( '/', '.', $prefix );
    
        $search = $manager->createSearch();
        $expr = array(
                $search->compare( '==', $prefix . '.domain', $domain ),
                $search->compare( '==', $prefix . '.code', $code ),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $result = $manager->searchItems( $search );
    
        if( ( $item = reset( $result ) ) === false )
        {
            $msg = sprintf( 'No type item for "%1$s/%2$s" in "%3$s" found', $domain, $code, $prefix );
            throw new \Controller_Jobs_Exception( $msg );
        }
    
        return $item;
    }
    
    protected function _getCategoryPath($categoryId, $language)
    {
        $addPath = array();
        
        $context = $this->_getContext($language);
        $manager = $this->getManager( $context, 'catalog');
        $config = $context->getConfig();
        $startid = $config->get( 'client/html/catalog/filter/tree/startid', '');
    
        $path = $manager->getPath($categoryId,array('text'));
        foreach($path as $key => $el){
            unset($path[$key]);
            if( $key == $startid ) {
                break;
            }
        }
        foreach ($path as $el){
            if($el->getStatus()==1){
                $dName = reset($el->getRefItems('text', 'DisplayName'));
                $addPath[] = $dName && !empty($dName->getContent()) ? $dName->getContent() : $el->getName();
            }
        }
        return $addPath;
    }
}
