<?php
namespace Aimeos\Aimeosext\Solr\IndexQueue;
use ApacheSolrForTypo3\Solr\IndexQueue\Indexer;
use ApacheSolrForTypo3\Solr\IndexQueue\Item;
use Apache_Solr_Document;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solr\HtmlContentExtractor;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\Access\Rootline;


/**
 * A special purpose indexer to index pages.
 *
 * In the case of pages we can't directly index the page records, we need to
 * retrieve the content that belongs to a page from tt_content, too. Also
 * plugins may be included on a page and thus may need to be executed.
 *
 * @author Ingo Renner <ingo@typo3.org>
 */
class AimeosPdfIndexer extends AimeosIndexer
{
    
    /**
     * Creates a single Solr Document for an item in a specific language.
     *
     * @param Item $item An index queue item to index.
     * @param int $language The language to use.
     * @return bool TRUE if item was indexed successfully, FALSE on failure
     */
    protected function indexItem(Item $item, $language = 0)
    {
        $itemIndexed = false;
        $documents = array();
       
        $record = $item->getRecord();
        $site = Site::getSiteByPageId($record['uid']);
       // $accessField = $pageDocument->getField('access');
        $pageAccessRootline = new Rootline('r:0');
        $context = $this->_getContext($language);
        $config = $context->getConfig();
        $view = $view = new \MW_View_Default();
        $helper = new \MW_View_Helper_Translate_Default( $view, $context->getI18n( $this->getLangCodeById($language)) );
        
        $manager = $this->getManager( $context, 'product/list' );
        $search = $manager->createSearch( true );
        $expr = array(
                $search->compare( '==', 'product.list.refid', $record['id'] ),
                $search->compare( '==', 'product.list.domain', 'media' ),
                $search->getConditions(),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $paramType = '';
        $productListArray = $manager->searchItems( $search );
        $productList = reset($productListArray);
        $params['typolink_additional_title'] = '';
        $catalogListId = null;

        if($productList){
            
            
            $configData = $config->get('client/html/catalog/detail/additional/download/types',array());
            $isInstruction = in_array($productList->getType(),$configData['Operating_Instructions']);
            $isCertificate = in_array($productList->getType(),$configData['certificates']);
            $certificateScope = '';
           
            if($isInstruction || $isCertificate){
                $manager = $this->getManager($context, 'media');
                $element = $manager->getItem($record['id'],array('text'));
                if($element){
                    $el = reset($element->getRefItems( 'text', 'DVS_DOKST'));
                    $elZp = reset($element->getRefItems( 'text', 'ZPB_VEROE'));
                    if($elScope = reset($element->getRefItems( 'text', 'ZPB_GELTUNG'))){
                        $certificateScope = $elScope->getContent();
                    }
                    
                    if($el){
                        if($isInstruction && !in_array($el->getContent(),array('FR - freigegeben','RE - Released'))){
                            return true;
                        }
                    }
                    if($el && $elZp){
                        if($isCertificate && (!in_array($el->getContent(),array('FR - freigegeben','RE - Released'))
                                || !in_array($elZp->getContent(),array('extern'))) ){
                                   return true;
                        }
                    }
                }
                
            }
            foreach ($configData as $key => $conf){
                if(in_array($productList->getType(),$conf)){
                    $paramType = ucfirst($helper->transform( 'client/html',str_replace('_',' ',$key)));
                    break;
                }
            }
            if(!empty($paramType) && $isCertificate && !empty($certificateScope)){
                $paramType = $paramType .' | '. $certificateScope;
            }
            
            $manager = $this->getManager( $context, 'product');
            $ids=[];
            foreach ($productListArray as $lst){
                $ids[] = $lst->getParentId();
            }
            $product = $this->_getProductById($ids, $manager, ['product','text','attribute']);
            if($product && $product->getId() && !empty($paramType)){
                $catFilterId = $product->getId();
                $params['typolink_additional_params'] = '&L='.$language.'&ai[controller]=catalog&ai[action]=detail&ai[d_prodid]='.$product->getId();
                
                if($product->getTypeId()==1){
                    $aimeosParent = $this->_getParentItem($product->getId(), 'product',$language);
                    if($aimeosParent){
                        $prParent = $manager->getItem($aimeosParent->getParentId(),['text']);
                        $params['typolink_additional_params'] .='&ai[d_parent]='.$aimeosParent->getParentId();
                         $catFilterId = $aimeosParent->getParentId();

                         $dName = reset($prParent->getRefItems('text', 'TitleProductSubSeries')); 
                         $params['title'] = (($dName && !empty($dName->getContent())) ?$dName->getContent():$prParent->getName()) .' | '. $paramType;
                         
                         if($ctlog = $this->_getParentItem($prParent->getId(), 'catalog',$language)){
                             $pth = $this->_getCategoryPath($ctlog->getParentId(), $language);
                             $aimeosPath = array_merge( array($helper->transform( 'client/html','Produkte')),$pth);
                             $aimeosPath[] = (($dName && !empty($dName->getContent())) ?$dName->getContent():$prParent->getName());
                             $aMaterialId = reset($product->getRefItems('attribute', 'AT_SAP_MainMaterialID'));
                             $aimeosPath[] =  ($aMaterialId && !empty($aMaterialId->getName())) ? $aMaterialId->getName(): $product->getName();
                             $params['typolink_additional_title'] = implode(' > ',$aimeosPath);
                         }
                    }    
                }else{
                    $aimeosVariant = $this->_getProductById(array_keys($product->getRefItems( 'product', null, 'default')), $manager, ['attribute']);
                    if($aimeosVariant){
                        
                        $dName = reset($product->getRefItems('text', 'TitleProductSubSeries'));
                        $params['title'] = (($dName && !empty($dName->getContent())) ?$dName->getContent():$product->getName()).' | '. $paramType;
                        
                        $params['typolink_additional_params'] = '&L='.$language.'&ai[controller]=catalog&ai[action]=detail&ai[d_prodid]='.$aimeosVariant->getId(). '&ai[d_parent]='.$product->getId();
                        
                        if($ctlog = $this->_getParentItem($product->getId(), 'catalog',$language)){
                            $pth = $this->_getCategoryPath($ctlog->getParentId(), $language);
                            $aimeosPath = array_merge( array($helper->transform( 'client/html','Produkte')),$pth);
                            
                            $dName = reset($product->getRefItems('text', 'TitleProductSubSeries'));
                            $aimeosPath[] = ($dName && !empty($dName->getContent())) ? $dName->getContent():$product->getName();
                            
                            $aMaterialId = reset($aimeosVariant->getRefItems('attribute', 'AT_SAP_MainMaterialID'));
                            $aimeosPath[] =  ($aMaterialId && !empty($aMaterialId->getName())) ? $aMaterialId->getName(): $aimeosVariant->getName();
                            $params['typolink_additional_title'] = implode(' > ',$aimeosPath);
                        }
                    }
                }
                
                
                $manager = $this->getManager( $context,'catalog/list' );
                $search = $manager->createSearch( true );
                $expr = array(
                        $search->compare( '==', 'catalog.list.refid', $catFilterId),
                        $search->compare( '==', 'catalog.list.domain', 'product'),
                        $search->compare( '==', 'catalog.list.type.code', 'default'),
                );
                $search->setConditions( $search->combine( '&&', $expr ) );
                $catalogData = $manager->searchItems($search);
                 
                 
                $catalogListId = null;
                 
                $catalogManager= $this->getManager( $context,'catalog');
                $config = $this->_getContext($language)->getConfig();
                $rootCatalogList = $config->get( 'client/html/catalog/webcodelist/category', array() );
                foreach ($catalogData as $catalog){
                    $path = array_keys($catalogManager->getPath($catalog->getParentId()));
                    $catalogList[$catalog->getRefId()]['catalog']=end($path);
                    $el= array_intersect($rootCatalogList, $path);
                    end($el);
                    $catalogListId = key($el);
                }
                if(is_null($catalogListId)){
                    return true;
                }
            }else{
                return true;// no products found
            }
        }
        if($productList===false){
            $manager = $this->getManager( $context, 'catalog/list' );
            
            $search = $manager->createSearch( true );
            $expr = array(
                    $search->compare( '==', 'catalog.list.refid', $record['id'] ),
                    $search->compare( '==', 'catalog.list.domain', 'media' ),
                    $search->getConditions(),
            );
            $search->setConditions( $search->combine( '&&', $expr ) );
            
            $catalogList = reset($manager->searchItems( $search ));
            if($catalogList){
                $manager = $this->getManager( $this->_getContext($language), 'catalog' );
                $catalog = $manager->getItem($catalogList->getParentId(),['text']);
    
                $configData = $config->get('client/html/catalog/extended/download/types',array());
                foreach ($configData as $key => $conf){
                    if(in_array($catalogList->getType(),$conf)){
                        $paramType = $helper->transform( 'client/html',str_replace('_',' ',$key));
                        break;
                    }
                }
            }
            
            if($catalogList && $catalog->getId() && !empty($paramType)){
                $params['typolink_additional_params'] = '&L='.$language.'&ai[controller]=catalog&ai[action]=list&ai[f_catid]='.$catalog->getId();
                
                
                $pth = $this->_getCategoryPath($catalog->getId(), $language);
                $aimeosPath = array_merge( array($helper->transform( 'client/html','Produkte')),$pth);
                $params['typolink_additional_title'] = implode(' > ',$aimeosPath);
                
                
                
                $dName = reset($catalog->getRefItems('text', 'DisplayName'));
                $params['title'] = (($dName && !empty($dName->getContent())) ?$dName->getContent():$catalog->getName()). ' | '. $paramType;
                
                $catalogListId = null;
                 
                $catalogManager= $this->getManager( $context,'catalog');
                $rootCatalogList = $config->get( 'client/html/catalog/webcodelist/category', array() );
                $path = $catalogManager->getPath($catalog->getId(), array('text'));
                $el= array_intersect($rootCatalogList, array_keys($path));
                $start= end($el);
                foreach ($path as $key => $cat){
                   if($key==$start){
                       break;
                    }else{
                       unset($path[$key]);
                   }
                }
                $catalogListId=key($el);
            }
            if(is_null($catalogListId)){
                return true;
            }
        }
       // die(print_r(array($catalogListId, $pageAccessRootline, $record, $params, $language, $record['id']),1));
        
        /** @var PageContextDetector $pageContextDetector */
        $pageContextDetector = GeneralUtility::makeInstance('ApacheSolrForTypo3\Solrfal\Detection\PageContextDetector', $site);
        $pageContextDetector->addDetectedFilesToPageAimeos($catalogListId, $pageAccessRootline, $record, $params, $language, $record['id']);

        return true;
    }
    
    protected function _getProductById($id, $manager, $domain)
    {
        $search = $manager->createSearch( true );
        $expr = array(
            $search->compare( '==', 'product.id', $id),
            $search->compare( '==', 'product.status', 1),
            $search->getConditions(),
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        return reset($manager->searchItems( $search, $domain ));
    }
}
