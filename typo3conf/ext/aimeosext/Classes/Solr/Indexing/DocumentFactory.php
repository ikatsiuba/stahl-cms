<?php
namespace Aimeos\Aimeosext\Solr\Indexing;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solrfal\Context\RecordContext;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use ApacheSolrForTypo3\Solrfal\Queue\Item;
use ApacheSolrForTypo3\Solrfal\Service\FieldProcessingService;

/**
 * Class TransformationService
 */
class DocumentFactory extends \ApacheSolrForTypo3\Solrfal\Indexing\DocumentFactory
{
    const SOLR_TYPE = 'downloads';

    /**
     * @param \Apache_Solr_Document $document
     * @param Item $item
     * @return void
     */
    protected function addFieldsFromTypoScriptConfiguration(\Apache_Solr_Document $document, Item $item)
    {
        $translatedMetaData = $this->getTranslatedFileMetaData($item->getFile(), $item->getContext()->getLanguage());
        $this->emitFileMetaDataRetrieved($item, $translatedMetaData);

        $fileConfiguration = Util::getSolrConfigurationFromPageId(
            $item->getContext()->getSite()->getRootPageId(),
            false,
            $item->getContext()->getLanguage()
        );

        $fieldConfiguration = $fileConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.queue._FILES.default.', []);
        $contextConfiguration = $item->getContext()->getSpecificFieldConfigurationTypoScript();

        if (count($contextConfiguration) > 0) {
            ArrayUtility::mergeRecursiveWithOverrule(
                $fieldConfiguration,
                $contextConfiguration
            );
        }

        $fieldConfigurationInRecordContext = null;
        if (is_array($fieldConfiguration['__RecordContext.'])) {
            $fieldConfigurationInRecordContext = $fieldConfiguration['__RecordContext.'];
            unset($fieldConfiguration['__RecordContext']);
            unset($fieldConfiguration['__RecordContext.']);
        }
        
        FieldProcessingService::addTypoScriptFieldsToDocument($item->getContext(), $document, $fieldConfiguration, $translatedMetaData);

        if ($item->getContext() instanceof RecordContext && is_array($fieldConfigurationInRecordContext)) {
            /** @var RecordContext $recordContext */
            $recordContext = $item->getContext();
            
            $params = $recordContext->getAdditionalStaticDocumentFields();
            if(isset($params['title'])){
                $document->setField('title',$params['title']);
            }
            
            if(isset($params['typolink_additional_title'])){
                $document->setField('fileReferenceTitle',$params['typolink_additional_title']);
            }
            
            if(is_array($params) && isset($params['typolink_additional_params'])){
                unset($fieldConfigurationInRecordContext['fileReferenceUrl.']['typolink.']['parameter.'],
                      $fieldConfigurationInRecordContext['fileReferenceUrl.']['typolink.']['section.']);
                $fieldConfigurationInRecordContext['fileReferenceUrl.']['typolink.']['parameter']=$recordContext->getUid();
                $fieldConfigurationInRecordContext['fileReferenceUrl.']['typolink.']['additionalParams']=$params['typolink_additional_params'];
            }else{
                $relatedRecord = $this->getDatabaseConnection()->exec_SELECTgetSingleRow(
                        '*',
                        $recordContext->getTable(),
                        'uid = ' . $recordContext->getUid()
                        );
                $relatedRecord = $this->getTranslatedRecord($recordContext->getTable(), $relatedRecord, $recordContext->getLanguage());
            }

            FieldProcessingService::addTypoScriptFieldsToDocument(
                $item->getContext(),
                $document,
                $fieldConfigurationInRecordContext,
                array()
            );

            if($fileReferenceUrl = $document->getField('fileReferenceUrl')){
                $fileReferenceUrl['value'] = str_replace(ltrim(PATH_typo3,'/'),'',$fileReferenceUrl['value']);
                $document->setField($fileReferenceUrl['name'], $fileReferenceUrl['value'], $fileReferenceUrl['boost']);
            }
        }
    }
    
    /**
     * @param Item $item
     *
     * @return string
     */
    protected function calculateDocumentId(Item $item)
    {
        return Util::getDocumentId(
                self::SOLR_TYPE,
                $item->getContext()->getTable()=='mshop_media' ? $item->getContext()->getUid():$item->getContext()->getPageId(),
                $item->getFile()->getUid(),
                md5(implode(',', $item->getContext()->toArray()))
                );
    }
}
