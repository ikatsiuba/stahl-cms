<?php
namespace Aimeos\Aimeosext\Solr\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2014 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solrfal\Context\PageContext;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Class PageContextDetector
 */
class PageContextDetector extends \ApacheSolrForTypo3\Solrfal\Detection\PageContextDetector
{


    
     protected function getStorageByName($name)
     {
         $storage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
         $collection= $storage->findAll();
         foreach ($collection as $_item){
             if($_item->getName()==$name){
                 return $_item; 
             }
         }
         return false;
     }

    /**
     * @param TypoScriptFrontendController $page
     * @param Rootline $pageAccessRootline
     * @param array $allowedFileUids
     * @param array $contentElements
     */
    public function addDetectedFilesToPageAimeos($pageId, Rootline $pageAccessRootline, $mediaData, $additionalParams, $language, $aimeosId)
    {
        
        if (!$this->isIndexingEnabledForContext('page')) {
//            return;
        }
        
        if (!$this->isIndexingEnabledForContext('aimeos')) {
            return;
        }

        $storagePdf = $this->getStorageByName('aimeos_pdf/');
        if(!$storagePdf){
            return;
        }
        
        $storageFileadmin = $this->getStorageByName('fileadmin/ (auto-created)');
        if(!$storageFileadmin){
            return;
        }
      
        $linkedFiles = $successfulUids = array();
            try{
                
                if($storageFileadmin->hasFile(str_replace('fileadmin/','',$mediaData['link']))){
                    $file= $storageFileadmin->getFile(str_replace('fileadmin/','',$mediaData['link']));
                }else{
                    $file = $storagePdf->getFile(str_replace('uploads/tx_aimeos/','', $mediaData['link']));
                }
                $linkedFiles[]=$file->getUid();
            }catch(\Exception $e){
                $this->logger->error('File not found: ' . $fileUid);
            }
        
        $context = new PageContext(
                $this->site, $pageAccessRootline,$aimeosId/*$page->getRecordUid()*/,
                'mshop_media', 'label',$pageId, $language
                );
        
        $context->setAdditionalDocumentFields($additionalParams);
        
        foreach ($linkedFiles as $fileUid) {
            if ($this->createIndexQueueEntryForFile($fileUid, $context)) {
                $successfulUids[] = $fileUid;
            }
        }
        if(count($successfulUids)){
            $this->getItemRepository()->removeOldEntriesInPageContext($this->site, $aimeosId/*$page->getRecordUid()*/, $language, $successfulUids);
        }
    }
    
}
