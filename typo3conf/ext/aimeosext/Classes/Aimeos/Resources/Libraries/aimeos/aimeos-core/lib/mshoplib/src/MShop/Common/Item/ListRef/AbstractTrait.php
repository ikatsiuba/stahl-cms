<?php

trait MShop_Common_Item_ListRef_AbstractTrait
{
    
    public function getRefItems( $domain, $type = null, $listtype = null, $lang = null )
    {
        $list = parent::getRefItems($domain, $type, $listtype);
        if($lang){
            $list = $this->checkRefLang($list,$lang);
        }
        return $list;
    }

    public function checkRefLang($list,$lang)
    {
        if(count($list) && !method_exists(reset($list),'getLanguageId')){
            return $list;
        }
        foreach ($list as $k=>$v){
            if($v->getLanguageId() != $lang){
                unset($list[$k]);
            }
        }
        return $list;
    }


}