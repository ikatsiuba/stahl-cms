<?php
namespace Aimeos\Aimeosext\Import\UserFunction;

use Aimeos\Aimeosext\Import\ExternalImport;

class Transformation {

    protected $_langAttr= array('QualifierID'=>array('de-DE','en-GB'),
                                'LOVQualifierID'=>array('de-DE','en-GB'),
                                'DerivedContextID'=>array('Context1','Context2'),
                                'AttributeID'=>array('DisplaySequence'));
    protected $_specialAttr = array('UnitID'=>
            array('unece.unit.ACR'=>'acre',
'unece.unit.CMK'=>'cm²',
'unece.unit.FTK'=>'ft²',
'unece.unit.INK'=>'in²',
'unece.unit.KMK'=>'km²',
'unece.unit.MIK'=>'mile²',
'unece.unit.MMK'=>'mm²',
'unece.unit.MTK'=>'m²',
'unece.unit.YDK'=>'yd²',
'unece.unit.D15'=>'sone(s)',
'unece.unit.2N'=>'dB',
'unece.unit.D61'=>'\'',
'unece.unit.D62'=>'"',
'unece.unit.DD'=>'°',
'unece.unit.B97'=>'µrad',
'unece.unit.C25'=>'mrad',
'unece.unit.C81'=>'rad',
'unece.unit.C74'=>'kbit/s',
'iso4217.unit.CAD'=>'$',
'iso4217.unit.USD'=>'$',
'iso4217.unit.EUR'=>'€',
'iso4217.unit.GBP'=>'£',
'iso4217.unit.JPY'=>'¥',
'unece.unit.P1'=>'%',
'unece.unit.AMP'=>'A',
'unece.unit.AMH'=>'Ah',
'unece.unit.JOU'=>'J',
'unece.unit.B22'=>'kA',
'unece.unit.KVT'=>'kV',
'unece.unit.4K'=>'mA',
'unece.unit.C14'=>'mH',
'unece.unit.VLT'=>'V',
'unece.unit.D46'=>'VA',
'unece.unit.OHM'=>'Ω',
'unece.unit.4O'=>'μF',
'unece.unit.A86'=>'GHz',
'unece.unit.HTZ'=>'Hz',
'unece.unit.KHZ'=>'kHz',
'unece.unit.MHZ'=>'MHz',
'unece.unit.D29'=>'THz',
'unece.unit.BTU'=>'Btu',
'unece.unit.4H'=>'µm',
'unece.unit.A11'=>'Å',
'unece.unit.CMT'=>'cm',
'unece.unit.DM'=>'dm',
'unece.unit.FOT'=>'ft',
'unece.unit.INH'=>'in',
'unece.unit.KTM'=>'km',
'unece.unit.MTR'=>'m',
'unece.unit.SMI'=>'mile',
'unece.unit.MMT'=>'mm',
'unece.unit.C45'=>'nm',
'unece.unit.YRD'=>'yd',
'unece.unit.CDL'=>'cd',
'unece.unit.A24'=>'cd/m²',
'unece.unit.B61'=>'Im/W',
'unece.unit.LUM'=>'lm',
'unece.unit.B62'=>'lm*s',
'unece.unit.LUX'=>'lx',
'unece.unit.MC'=>'µg',
'unece.unit.GRM'=>'g',
'unece.unit.KGM'=>'kg',
'unece.unit.LBR'=>'lb',
'unece.unit.MGM'=>'mg',
'unece.unit.NU'=>'N/m',
'unece.unit.NPM'=>'N/mm²',
'unece.unit.ONZ'=>'oz',
'unece.unit.TNE'=>'t',
'unece.unit.D80'=>'µW',
'unece.unit.A90'=>'GW',
'unece.unit.KWT'=>'kW',
'unece.unit.HJ'=>'metric hp',
'unece.unit.C31'=>'mW',
'unece.unit.MAW'=>'MW',
'unece.unit.WTT'=>'W',
'unece.unit.BAR'=>'bar',
'unece.unit.25'=>'g/cm²',
'unece.unit.GM'=>'g/m²',
'unece.unit.28'=>'kg/m²',
'unece.unit.FP'=>'lb/ft²',
'unece.unit.80'=>'lb/in²',
'unece.unit.GO'=>'mg/m²',
'unece.unit.37'=>'oz/ft²',
'unece.unit.ON'=>'oz/yd²',
'unece.unit.RPM'=>'r/min',
'unece.unit.RPS'=>'r/s',
'unece.unit.CEL'=>'°C',
'unece.unit.FAH'=>'°F',
'unece.unit.A48'=>'°R',
'unece.unit.KEL'=>'K',
'bits/sample'=>'bits/sample',
'dpi'=>'dpi',
'mm'=>'mm',
'pixels'=>'pixels',
'samples/pixel'=>'samples/pixel',
'unece.unit.DAY'=>'dy',
'unece.unit.HUR'=>'hr',
'unece.unit.B98'=>'Microsecond',
'unece.unit.C26'=>'Millisecond',
'unece.unit.MIN'=>'min',
'unece.unit.MON'=>'mon',
'unece.unit.C47'=>'Nanosecond',
'unece.unit.SEC'=>'sec',
'unece.unit.WEE'=>'wk',
'unece.unit.ANN'=>'yr',
'unece.unit.R1'=>'Picas',
'unece.unit.N3'=>'Points',
'std.proportional'=>'Proportional',
'unece.unit.2M'=>'cm/s',
'unece.unit.FR'=>'ft/min',
'unece.unit.FS'=>'ft/s',
'unece.unit.IU'=>'in/s',
'unece.unit.KMH'=>'km/h',
'unece.unit.KNT'=>'kn',
'unece.unit.2X'=>'m/min',
'unece.unit.MTS'=>'m/s',
'unece.unit.HM'=>'mile/h',
'unece.unit.C16'=>'mm/s',
'unece.unit.4G'=>'µl',
'unece.unit.CLT'=>'cl',
'unece.unit.CMQ'=>'cm³',
'unece.unit.DLT'=>'dl',
'unece.unit.OZI'=>'fl oz (UK)',
'unece.unit.OZA'=>'fl oz (US)',
'unece.unit.FTQ'=>'ft³',
'unece.unit.GLI'=>'gal (UK)',
'unece.unit.GLL'=>'gal (US)',
'unece.unit.INQ'=>'in³',
'unece.unit.LTR'=>'l',
'unece.unit.MLT'=>'ml',
'unece.unit.MMQ'=>'mm³',
'unece.unit.MTQ'=>'m³',
'unece.unit.PTI'=>'pt (UK)',
'unece.unit.PT'=>'pt (US)',
'unece.unit.QTI'=>'qt (UK)',
'unece.unit.QT'=>'qt (US)',
'unece.unit.YDQ'=>'yd³',
'unece.unit.5A'=>'barrel (US)/min',
'unece.unit.2J'=>'cm³/s',
'unece.unit.2K'=>'ft³/h',
'unece.unit.2L'=>'ft³/min',
'unece.unit.G3'=>'gal (UK)/min',
'unece.unit.G2'=>'gal (US)/min',
'unece.unit.4X'=>'kl/h',
'unece.unit.LD'=>'l/d',
'unece.unit.L2'=>'l/min',
'unece.unit.41'=>'ml/min',
'unece.unit.40'=>'ml/s',
'unece.unit.MQH'=>'m³/h',
'unece.unit.MQS'=>'m³/s'));
    protected $_importer= null;
    
    public function processAttributes($record, $index, $params) {
        
        $xml = $record[$index];
        if (empty($xml)) {
            return '';
        }
        
        $dom = new \DOMDocument();
        $dom->loadXML('<DataInfo>'.$xml.'</DataInfo>', LIBXML_PARSEHUGE);
        $xPathObject = new \DOMXPath($dom);
        $data=array();
        
        if(is_null($this->_importer)){
            $this->_importer = new ExternalImport();
        }
        
        $resultNodes = $xPathObject->evaluate($params['xpath']);
        foreach ($resultNodes as $node){
            $attributeId = $this->_getAttributeId($node, $params);
            if(isset($params['value_attribute'])){
                $data[$attributeId]['relation_type'][] = $this->_getAttributeId($node,array('attribute'=>$params['value_attribute']));
            }
            if(isset($data[$attributeId]) && is_array($data[$attributeId])){
                if(in_array($node->nodeName,array('ValueGroup','MultiValue')) && !$node->hasChildNodes() && empty($node->nodeValue) && empty($node->textContent)){
                    ;
                }else{
                    $data[$attributeId] = array_merge_recursive($data[$attributeId],(array)$this->getXmlValue($node));
                }
            }else{
                $data[$attributeId] = $this->getXmlValue($node);
                if(!is_array($data[$attributeId])){
                    $unitValue='';
                    // Disable unit value reworked
                    if($unit = $this->_getAttributeId($node,array('attribute'=>'UnitID'))){
                        $unitValue='UnItVaLuE:::'.$unit;
                    }
                    $data[$attributeId] .=$unitValue;
                }
            }
        }
        $record[$index]=$data;
        
        return $record[$index];
    }
    
    
    protected function _getAttributeId($node,$params) {

        $attributeId = $this->_importer->extractValueFromNode($node,$params);
        if(!$attributeId && $node->parentNode){
            $attributeId = $this->_importer->extractValueFromNode($node->parentNode,$params);
        }
        return $attributeId;
    }
    
    public function getXmlValue($node) {
        $innerHTML = array();
        $children = $node->childNodes;
            foreach ($children as $child) {
                $unitValue='';
                if(trim($child->textContent)!=='') {
                    if($child->attributes && $unit = $this->_getAttributeId($child,array('attribute'=>'UnitID'))){
                        $unitValue='UnItVaLuE:::'.$unit;
                    }
                    $key=$this->_getArrayKey($child);
                    if($key!==false){
                        if(isset($innerHTML[$key])){
                            $dt = $this->_chrTrans($child->textContent).$unitValue;
                            $innerHTML[$key]= array_merge_recursive((array)$innerHTML[$key],(array)$dt);
                        } else{    
                            $innerHTML[$key]= $this->_chrTrans($child->textContent).$unitValue;
                        }
                    }else{
                        $innerHTML[]= $this->_chrTrans($child->textContent).$unitValue;
                    }
                }
            }
        if(empty($innerHTML)){
            $unitValue='';
            if($node->attributes && $unit = $this->_getAttributeId($node,array('attribute'=>'UnitID'))){
                $unitValue='UnItVaLuE:::'.$unit;
            }
            $innerHTML=$node->nodeValue.$unitValue;
        }
        return (count($innerHTML)==1 && isset($innerHTML[0]))? $innerHTML[0] : $innerHTML;
    }
    
    protected function _getArrayKey($node)
    {
        if($node->attributes){
            foreach($node->attributes as $atribute){
                if(in_array($atribute->nodeName,array_keys($this->_langAttr))){
                    if(in_array($atribute->nodeValue,$this->_langAttr[$atribute->nodeName])){
                        return $atribute->nodeValue;
                    }
                }
            }
        }
        return false;
    }
    
    protected function _chrTrans($str)
    {
        return preg_replace(array("/<lt\/>/","/<gt\/>/","/0xA0/","/ /","/<Hyperlink target=/","/<\/Hyperlink>/","/<bold>/","/<\/bold>/"),array("<",">"," "," ","<a href=","</a>","<b>","</b>"),html_entity_decode(trim($str)));
    }
    
    public function getUnit()
    {
        return $this->_specialAttr;
    }
}
