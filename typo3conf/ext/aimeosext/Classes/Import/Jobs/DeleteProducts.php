<?php
namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeosext\Import\Jobs;


class DeleteProducts extends AbstractJob
{
    
    public function process(array $records)
    {
        $inserts=$updates=$deletes=$errors=0;
        $context = $this->_getContext();
        foreach ($records as $record) {
                
                $this->_prefix='product.';
                $manager = $this->getManager($context, 'product');
                $product = $this->_getItem($record['product.code'],null, array());
                if($product!==false){
                    $manager->begin();
                    try{
                       
                        $manager->deleteItems((array)$product->getId());
                        
                        $listItems = $this->_getListItems( $product->getId(), null ,'product','product.list');
                        if($listItems){
                            $managerList = $this->getManager($context, 'product/list');
                            $managerList->deleteItems(array_keys($listItems));
                        }
                        $listItems = $this->_getListItems( $product->getId(), null ,'product','catalog.list');
                        if($listItems){
                            $managerList = $this->getManager($context, 'catalog/list');
                            $managerList->deleteItems(array_keys($listItems));
                        }
                        $deletes++;
                    }catch(Exception $e){
                            $manager->rollback();
                            $msg = sprintf( 'Unable to delete object with code "%1$s": %2$s', $code, $e->getMessage() );
                            $context->getLogger()->log( $msg );
                            $errors++;
                        }
                        
                }
                $manager->commit();
                
        }
        return array($inserts,$updates,$deletes,$errors); 
    }
    
    
    
    
    protected function _getListItems( $productId, $types, $domain, $prefix )
    {
        $manager = $this->getManager( $this->_getContext(), str_replace('.', '/', $prefix) );
        $search = $manager->createSearch();
        
        $expr = array(
                $search->compare( '==', $prefix.'.domain', $domain ),
                $search->compare( '==', $prefix.'.refid', $productId ),
        );
        
        if( $types !== null ) {
            $expr[] = $search->compare( '==', $prefix.'.type.code', $types );
        }
        
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSlice( 0, 0x7FFFFFFF );
        
        return $manager->searchItems( $search );
    }
    
}
