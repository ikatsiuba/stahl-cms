<?php
namespace Aimeos\Aimeosext\Import\Jobs;

class Attribute extends AbstractJob
{
    protected $_userType=array(
                            'catalog'=>array('OT_WebClassficationFolder'),
                            'product'=>array('OT_ProductVariant','OT_ProductSubSeries','OT_ProductSeries','OT_ProductRoot','OT_ProductSubGroup','OT_ProductGroup'),
                            'media'=>array('OT_CertificateSAP','OT_Document','OT_BA_SAP','OT_EGK','OT_Driver','OT_Software') 
    );
    
    protected $_baseType=array(
                            'number'=>'attribute/type',
                            'numberrange'=>'attribute/type',
                            'integer'=>'attribute/type',
                            'text'=>'text/type');

    protected $_map = array('attribute'=>array('code','label'),
                            'text'=>array('label','content'));
    
    protected $_systemAttributes = array('DisplayName','Purpose','DisplaySequence');
    
    protected $_textAttributes = array(
        'AT_SAP_MainMaterialID','AT_CertificatesSAP','AT_GlobalWebfilter','ZPB_TYP'
    );
    
    public function process(array $records)
    {
        $this->_prefix='text/type.';
        $inserts=$updates=$deletes=$errors=0;
        $context = $this->_getContext();
        $listManager = $this->getManager($context, 'attributegroup/list');
        $attributes = $this->_getAllAttributes();
        foreach ($records as $record) {

            if (!isset($record['user_type']) || count($record['user_type']) == 0) {
                continue;
            }
            $domains = array();
            
            foreach ($this->_userType as $key => $val) {
                if (count(array_intersect($val, array_keys($record['user_type'])))) {
                    $domains[] = $key;
                }
            }
            if (empty($domains)) {
                file_put_contents(PATH_site."typo3temp/records.skipped", print_r($record, true) . "\n", FILE_APPEND);
                continue;
            }

            if (!empty($record['base_type']) && isset($this->_baseType[strtolower($record['base_type'])])) {
                $this->_prefix = $this->_baseType[strtolower($record['base_type'])] . '.';
            } else {
                $this->_prefix = 'text/type.';
            }
            
            if(isset($record['web_filter']) || in_array($record['code'],$this->_textAttributes)) {
                $this->_prefix = 'attribute/type.';
            }
            
            if (!isset($record['web_filter'])) {
                $record['web_filter'] = '';
            }
            if (!isset($record['web_style'])) {
                $record['web_style'] = '';
            }
            if (!isset($record['unit'])) {
                $record['unit'] = '';
            }
            
            $manager = $this->getManager($context, rtrim($this->_prefix, '.'));
            $this->_prefixStore = $this->_prefix;
            foreach ($domains as $domain){
                $this->_prefix = $this->_prefixStore; 
                $this->_prefix = str_replace('/', '.', $this->_prefix);
                $record[$this->_prefix . 'status'] = 1;
                $record[$this->_prefix . 'domain'] = $domain;
                $record[$this->_prefix . 'code'] = $record['code'];
                $record[$this->_prefix . 'label'] = $record['label'];
                $record[$this->_prefix . 'siteid'] = $record['siteid'];
                $record[$this->_prefix . 'web_filter'] = $record['web_filter'];
                $record[$this->_prefix . 'web_style'] = $record['web_style'];
                $record[$this->_prefix . 'unit'] = $record['unit'];
                
                $manager->begin();
                 try{
                     if($this->_prefix == 'attribute.type.') {
                         $index = array_search($record[$this->_prefix.'code'], $attributes['text']);
                     }
                     else {
                         $index = array_search($record[$this->_prefix.'code'], $attributes['attribute']);
                     }
                     if($index) {
                         $this->_clearAttribute($index);
                         $deletes++;
                     }
                      
                     $attribute = $this->_getItem($record[$this->_prefix.'code'],$domain);
                     
                     if($attribute===false){
                         $attribute = $manager->createItem();
                     }
                     
                     if($attribute->getId()){
                         $updates++;
                     }else{
                         $inserts++;
                     }
                     $attribute->fromArray( $record );
                     
                     $manager->saveItem( $attribute );
                      
                     $listItems = $this->_getListItems( $attribute->getId(), array('default') ,str_replace('.', '/', rtrim($this->_prefix,'.')));
                     
                     foreach ($record['attributegroup'] as $group => $val){
                     
                         $attributeGroup = $this->_getAttributeGroupItem($group);
                         if($attributeGroup) {
                             $listItem = $this->_getListItemByGroupId($attributeGroup->getId(), $listItems);
                             //die(print_r(array($attributeGroup->getId(), $listItem),1));
                             if( $listItem === false) {
                                 $listItem = $listManager->createItem();
                             }

                             $list['attributegroup.list.typeid'] = $this->_getTypeId( 'attributegroup/list/type', str_replace('.', '/', rtrim($this->_prefix,'.')), 'default');
                             $list['attributegroup.list.parentid'] = $attributeGroup->getId();
                             $list['attributegroup.list.refid'] = $attribute->getId();
                             $list['attributegroup.list.domain'] = str_replace('.', '/', rtrim($this->_prefix,'.'));
                             $list['attributegroup.list.status'] = 1;
                             $list['attributegroup.list.position'] = isset($record['attribute']['DisplaySequence'])?$record['attribute']['DisplaySequence']:0;
                             
                             $listItem->fromArray($list);
                             $listManager->saveItem( $listItem );
                         }
                             
                     }
                         
                     foreach( $listItems as $listItem ){
                        $listManager->deleteItem( $listItem->getId() );
                     }
                     
                     if(isset($record['attribute']['DisplayName'])){
                         $this->_prefix = str_replace('type.', '', $this->_prefix);
                         $rec['attribute']['DisplayName']= $record['attribute']['DisplayName'];
                         $processors = $this->_getProcessors($rec);
                         if(count($processors)){
                             $langAttribute = $this->_generateLangElement($attribute, $domain);
                         }
                         foreach ($processors as $processor){
                             $processor->run($langAttribute);
                         }
                     }
                     
                     $manager->commit();
                     
                 
                      
                 }catch(Exception $e){
                     $manager->rollback();
                     
                     $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $record['code'], $e->getMessage() );
                     $context->getLogger()->log( $msg );
                     
                     $errors++;
                 }
             
            }
        }
        return array($inserts,$updates,$deletes,$errors); 
    }
 
    
    /**
     * Returns the attributegroup list items for the given attrubutegroup and attribute/type ID
     *
     * @param string $attrid Unique attribute ID
     * @param array|null $types List of catalog list types
     * @param string $domain
     * @return array List of catalog list items
     */
    protected function _getListItems( $attrId, $types, $domain )
    {
        $manager = $this->getManager( $this->_getContext(), 'attributegroup/list' );
        $search = $manager->createSearch();

        $expr = array(
                $search->compare( '==', 'attributegroup.list.domain', $domain ),
                $search->compare( '==', 'attributegroup.list.refid', $attrId ),
        );
    
        if( $types !== null ) {
            $expr[] = $search->compare( '==', 'attributegroup.list.type.code', $types );
        }
    
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSortations( array( $search->sort( '+', 'attributegroup.list.position' ) ) );
        $search->setSlice( 0, 0x7FFFFFFF );
        
        return $manager->searchItems( $search);
    }
    
    protected function _getListItemByAttributeId($attrId, &$listItems)
    {
        foreach ($listItems as $listId => $listItem){
            if($listItem->getRefId()==$attrId) {
                unset($listItems[$listId]);
                return $listItem;
            }
        }
        return false;
    }    
    
    protected function _getListItemByGroupId($id, &$listItems)
    {
        foreach ($listItems as $listId => $listItem){
            if($listItem->getParentId()==$id) {
                unset($listItems[$listId]);
                return $listItem;
            }
        }
        return false;
    }
    
    protected function _getAttributeGroupItem($code)
    {
        $manager = $this->getManager( $this->_getContext(), 'attributegroup' );
        $search = $manager->createSearch();
    
        $search->setConditions( $search->compare( '==', 'attributegroup.code', $code ) );
        $search->setSlice( 0, 1);
        
        return reset($manager->searchItems( $search ));
    }

    protected function _getAllAttributes() {
        $result = array();
        $types = array_unique( $this->_baseType );
        foreach($types as $ar_key => $type) {
            $manager = $this->getManager( $this->_getContext(), $type );
            $search = $manager->createSearch();
            $key = str_replace('/type', '', $type);
            $search->setConditions( $search->compare( '!=', str_replace('/', '.', $type).'.code', $this->_systemAttributes) );
            $search->setSlice( 0, 0x7FFFFFFF );
            $items = $manager->searchItems( $search, array() );
            foreach($items as $item) {
                if(!empty($item)) {
                    $result[$key][$item->getId()] =  $item->getCode();
                }
            }
        }
        return $result;
    }

    protected function _clearAttribute($typeId) {
        if($this->_prefix == 'text.type.') {
            $old_prefix = 'attribute/type';
        }
        else {
            $old_prefix = 'text/type';
        }
        $typeManager = $this->getManager( $this->_getContext(), $old_prefix );
        $typeManager->deleteItem( $typeId ); // Delete from table attribute_type or text_type
        
        $listItems = $this->_getListItems( $typeId, array('default') ,$old_prefix);
        $listManager = $this->getManager($context, 'attributegroup/list');
        foreach( $listItems as $listItem ){
            $listManager->deleteItem( $listItem->getId() ); // Delete from table attributegroup_list or text_type
        }
        
    }

    protected function _generatePos($parentId, $typeId) {
        $manager = $this->getManager( $this->_getContext(), 'attributegroup/list' );
        $search = $manager->createSearch();
        $expr = array(
            $search->compare( '==', 'attributegroup.list.parentid', $parentId ),
            $search->compare( '==', 'attributegroup.list.typeid', $typeId )
        );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSortations( array( $search->sort( '-', 'attributegroup.list.position' ) ) );
        $search->setSlice( 0, 1 );
        $item = reset($manager->searchItems( $search ));
        return empty($item) ? 0 : $item->getPosition() + 1;
    }
    
    protected function _generateLangElement($attribute,$domain) {
        
        $storePrefix = $this->_prefix;
        $this->_prefix = str_replace('type.', '', $this->_prefix);
        $manager = $this->getManager( $this->_getContext(), rtrim($this->_prefix,'.'));
        
        $langElement = $this->_getLangItem($attribute->getCode(), rtrim($this->_prefix,'.'), rtrim($this->_prefix,'.'), rtrim($this->_prefix,'.').'/type');
        if($langElement===false){
            $langElement = $manager->createItem();
        }
        $rec[$this->_prefix.'status']=1;
        $rec[$this->_prefix.'siteid']=$attribute->getSiteId();
        $rec[$this->_prefix.'typeid'] = $attribute->getId();
        $rec[$this->_prefix.'domain'] = rtrim($this->_prefix,'.').'/type';
        $langElement->fromArray($this->_setValue($rec, rtrim($this->_prefix,'.'), $attribute->getCode(), ''));
        $manager->saveItem( $langElement );
        return $langElement;
    }
    
    protected function _setValue($list, $type, $code, $value)
    {
        $list[$type.'.'.$this->_map[$type][0]]=$code;
        $list[$type.'.'.$this->_map[$type][1]]=$value;
        return $list;
    }
    
    protected function _getLangItem($code, $path, $type, $domain=null)
    {
        $manager = $this->getManager( $this->_getContext(), $path );
        $search = $manager->createSearch();
    
        $expr =array( $search->compare( '==', $type.($type==='text' ? '.label':'.code'), $code ) );
        if(!is_null($domain)){
            $expr[] = $search->compare( '==', $type.'.domain', $domain );
        }
        $search->setConditions($search->combine( '&&', $expr ));
        $search->setSlice( 0, 1 );
    
        return reset($manager->searchItems( $search, array('text')));
    }
}
