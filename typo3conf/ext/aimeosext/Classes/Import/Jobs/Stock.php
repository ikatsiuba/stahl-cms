<?php
namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeosext\Import\Jobs;
use Aimeos\Aimeos\Base;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use Aimeos\Aimeosext\Import\Jobs\Processor\Attribute as Attr;

class Stock extends AbstractJob
{
    protected $_table = 'mshop_product_stock_tmp';
    
    public function process(array $records)
    {
        try{
            $res = $this->_importCsv($records);
            $this->_mergeData();
        }catch(\Exception $e){
            $res[]=$e->getMessage();
        }
        if(!isset($res[3])){
            $this->_archiveStockFile();
        }
        return $res; 
    }
    
    
    protected function _importCsv(array $records)
    { 
        $GLOBALS['TYPO3_DB']->exec_UPDATEquery($this->_table,'',array('status'=>-1));
        $ar = ['menge','gwzt','pzt'];
        foreach ($records as $record){
            
            if($prodid = $this->_getProductIdByMaterialId(intval($record['material_id']))){;
                $record['prodid']=$prodid;
                foreach ($ar as $_el){
                    $record[$_el]=strtr($record[$_el], array(','=>'.'));
                }
                $query = $GLOBALS['TYPO3_DB']->INSERTquery($this->_table, $record);
                $GLOBALS['TYPO3_DB']->sql_query($query.' ON DUPLICATE KEY UPDATE status=2');
            }else{
                file_put_contents(PATH_site."typo3temp/logs/import_stock.log", date("Y-m-d H:i:s").' NO Material ID: '.print_r($record['material_id'], true)."\n", FILE_APPEND);
            }
        }
        $inserts=$GLOBALS['TYPO3_DB']->exec_SELECTcountRows('*',$this->_table,'status=0');
        $updates=$GLOBALS['TYPO3_DB']->exec_SELECTcountRows('*',$this->_table,'status=2');
        $deletes=$GLOBALS['TYPO3_DB']->exec_SELECTcountRows('*',$this->_table,'status=-1');
        
        return array($inserts,$updates,$deletes);
    }
    
    protected function _mergeData()
    {
        $GLOBALS['TYPO3_DB']->exec_TRUNCATEquery('mshop_product_stock');
        
        $query = "insert into mshop_product_stock (prodid,siteid,warehouseid,stocklevel,backdate,ctime) 
select prodid,1,1, sum(menge) as stocklevel, 0 as backdate,CURRENT_TIMESTAMP 
from mshop_product_stock_tmp 
where menge>0 and status in (0,2) 
group by prodid
union
select prodid,1,1, 0 as stocklevel, DATE_ADD(CURDATE(), INTERVAL max(if(besch_art='E',gwzt,pzt)) DAY) as backdate,CURRENT_TIMESTAMP 
from mshop_product_stock_tmp 
where status in (0,2) and prodid not in (select prodid from mshop_product_stock_tmp where menge>0 and status in (0,2))
group by prodid";
        $GLOBALS['TYPO3_DB']->sql_query($query.' ON DUPLICATE KEY UPDATE mtime=CURRENT_TIMESTAMP');
        
     
        
    }
    
    
    
    protected function _getProductIdByMaterialId($materialId)
    {
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('prodid',
        'mshop_catalog_index_attribute as a_
        join mshop_product as p_ on p_.id=a_.prodid',
        'a_.code="AT_SAP_MainMaterialID-'.$materialId.'" and p_.typeid=1 and p_.status!=0'
        );
        
        return isset($res['prodid'])? $res['prodid']: false;
    }
    
    protected function _archiveStockFile()
    {
        $repository = \TYPO3\CMS\Core\Utility\GeneralUtility::getUserObj('Tx_ExternalImport_Domain_Repository_ConfigurationRepository');
        $storage = $this->getStorageByName('import/');
        $fl= $storage->getFile('/stock/HAVAS_DOWNLOAD.CSV');
        $repository->archiveFile($storage, $fl);
    }
    
    protected function getStorageByName($name)
    {
        $storage = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
        $collection= $storage->findAll();
        foreach ($collection as $_item){
            if($_item->getName()==$name){
                return $_item;
            }
        }
        return false;
    }
}