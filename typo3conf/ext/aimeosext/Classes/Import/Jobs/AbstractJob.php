<?php

namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeos\Base;

abstract class AbstractJob
{
    protected $_conf;
    protected $_prefix;
    private static $_types = array();
    private static $_managers = array();
    
    public function __construct(array $conf)
    {
        $this->_conf = $conf;
    }
    
    
    public function _getContext()
    {
        $config = Base::getConfig($this->_conf);
        $context = Base::getContext( $config );
        $manager = $this->getManager($context, 'locale');
        $localeItem = $manager->bootstrap( 'default', '', '', false );
        $localeItem->setLanguageId( null );
        $localeItem->setCurrencyId( null );
        $context->setLocale( $localeItem );
    
        return $context;
    }
    
    protected function getManager($context, $type){
        
        if( !isset( self::$_managers[$type] ) ){
            self::$_managers[$type] = \MShop_Factory::createManager( $context, $type );
        }
        return self::$_managers[$type];
    }
    
    protected function _getItem($code, $domain =null, $ref = null)
    {
        $type= rtrim($this->_prefix,'.');
        if(strpos($type, '.')!==false){
            $type=str_replace('.', '/', $type);
        }
        
        $manager = $this->getManager($this->_getContext(), $type);
        $search = $manager->createSearch();
        $expr =array( $search->compare( '==', $this->_prefix.'code', $code ) );
        if(!is_null($domain)){
            $expr[] = $search->compare( '==', $this->_prefix.'domain', $domain );
        }
        $search->setConditions($search->combine( '&&', $expr ));
        $search->setSlice( 0, 1 );
    
        return reset($manager->searchItems( $search, is_null($ref) ? array('text','media','attribute'): $ref));
    }
    
    protected function _getProcessors( array $record)
    {
        $object= array();
        foreach( $record as $code => $data )
        {
            if(is_array($data)){
                $type=str_replace($this->_prefix, '', $code);
                $classname = 'Aimeos\\Aimeosext\\Import\\Jobs\\Processor\\'.ucfirst($type);
                if( class_exists( $classname )) {
                    $object[$type] = new $classname($this->_getContext(),$data, $this->_prefix);
//                    throw new \Exception( sprintf( 'Class "%1$s" not found', $classname ) );
                }
            }
        }
    
        return $object;
    }
    
    protected function _getTypeId( $path, $domain, $code )
    {
        if( !isset( self::$_types[$path][$domain] ) )
        {
            $manager = $this->getManager($this->_getContext(), $path);
            $key = str_replace( '/', '.', $path );
    
            $search = $manager->createSearch();
            $search->setConditions( $search->compare( '==', $key . '.domain', $domain ) );
            $search->setSlice( 0, 0x7FFFFFFF );
            foreach( $manager->searchItems( $search ) as $id => $item ) {
                self::$_types[$path][$domain][ $item->getCode() ] = $id;
            }
        }
    
        if( !isset( self::$_types[$path][$domain][$code] ) ) {
            
            $manager = $this->getManager($this->_getContext(), $path);
            $key = str_replace( '/', '.', $path );
            
            $record[$key . 'status'] = 1;
            $record[$key . 'domain'] = $domain;
            $record[$key . 'code'] = $code;
            $record[$key . 'label'] = $code;
            $typeRecord = $manager->createItem();
            $typeRecord->fromArray( $record );
            $manager->saveItem( $typeRecord );
            
            self::$_types[$path][$domain][ $code ] = $typeRecord->getId();
            
        }
        if( !isset( self::$_types[$path][$domain][$code] ) ) {
            throw new \Exception( sprintf( 'No type item for "%1$s/%2$s" in "%3$s" found', $domain, $code, $path ) );
        }
        
        return self::$_types[$path][$domain][$code];
    } 
    
    public abstract function process(array $records);
    
}