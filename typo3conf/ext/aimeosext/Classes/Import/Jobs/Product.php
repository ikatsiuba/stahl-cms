<?php
namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeosext\Import\Jobs;


class Product extends AbstractJob
{
    protected $_productType=array('OT_ProductSubSeries'=>'select',
                                  'OT_ProductVariant'=>'default');
    
    
    public function process(array $records)
    {
        $this->_prefix='product.';
        $inserts=$updates=$deletes=$errors=0;
        $context = $this->_getContext();
        $manager = $this->getManager( $context, rtrim($this->_prefix,'.'));
        $listManager = $this->getManager( $context, 'catalog/list' );
        foreach ($records as $record) {
            
            if( !isset( $record[$this->_prefix.'code'] ) || $record[$this->_prefix.'code'] === '') {
                continue;
            }
            
            $manager->begin();
            try{
                $product = $this->_getItem($record[$this->_prefix.'code']);
                 
                if($product===false){
                    $product = $manager->createItem();
                }
                
                $productType = isset($this->_productType[$record[$this->_prefix.'type']]) ? $this->_productType[$record[$this->_prefix.'type']] : 'default';
                $record[$this->_prefix.'status'] = 0;
                if(isset($record[$this->_prefix.'attribute']['AT_WebDisplay'])){
                    if($record[$this->_prefix.'attribute']['AT_WebDisplay']==1){
                        $record[$this->_prefix.'status'] = 1;
                    }elseif($record[$this->_prefix.'attribute']['AT_WebDisplay']==3){
                        $record[$this->_prefix.'status'] = -1;
                    }
                }
                
                $record[$this->_prefix.'typeid'] = $this->_getTypeId( 'product/type', 'product', $productType );
                
                $product->fromArray( $record );
                if($product->getId()){
                    $updates++;
                }else{
                    $inserts++;
                }
                $manager->saveItem( $product );

                $listItems = $this->_getListItems( $product->getId(), array('default') ,str_replace('.', '/', rtrim($this->_prefix,'.')));
                //die(print_r($listItems,1));
                
                if(isset($record[$this->_prefix.'category_id']) && !empty($record[$this->_prefix.'category_id'])) {
                    $listElements = $refElements = array();
                    /*while ($listItem = $listItem = $this->_getListItemByProductId($product->getId(), $listItems)){
                        $listElements[]=$listItem;
                    }*/
                    $refElements = $this->_getCatalogItem(is_array($record[$this->_prefix.'category_id'])? array_keys($record[$this->_prefix.'category_id']): $record[$this->_prefix.'category_id']);

                    foreach ($listItems as $listId=>$listItem){
                        if(isset($refElements[$listItem->getParentId()])){
                            $listElements[] = $listItems[$listId];
                            unset($listItems[$listId]);
                        }
                    }

                    foreach ($listElements as $listItem){
                        $list = [];
                        if(isset($refElements[$listItem->getParentId()])){
                            unset($refElements[$listItem->getParentId()]);
                        }
                        
                        $list['catalog.list.typeid'] = $this->_getTypeId( 'catalog/list/type', str_replace('.', '/', rtrim($this->_prefix,'.')), 'default');
                        $list['catalog.list.parentid'] = $listItem->getParentId();
                        $list['catalog.list.refid'] = $product->getId();
                        $list['catalog.list.domain'] = str_replace('.', '/', rtrim($this->_prefix,'.'));
                        $list['catalog.list.status'] = 1;
                        $list['catalog.list.position'] = isset($record[$this->_prefix.'attribute']['DisplaySequence'])?$record[$this->_prefix.'attribute']['DisplaySequence']:11100100111;
                        
                        $listItem->fromArray($list);
                        $resultList[] = $listItem;
                    }
                    foreach ($refElements as $refItem){
                        $listItem = $listManager->createItem();
                        $list = [];
                        $list['catalog.list.typeid'] = $this->_getTypeId( 'catalog/list/type', str_replace('.', '/', rtrim($this->_prefix,'.')), 'default');
                        $list['catalog.list.parentid'] = $refItem->getId();
                        $list['catalog.list.refid'] = $product->getId();
                        $list['catalog.list.domain'] = str_replace('.', '/', rtrim($this->_prefix,'.'));
                        $list['catalog.list.status'] = 1;
                        $list['catalog.list.position'] = isset($record[$this->_prefix.'attribute']['DisplaySequence'])?$record[$this->_prefix.'attribute']['DisplaySequence']:11100100111;
                        
                        $listItem->fromArray($list);
                        $resultList[] = $listItem;
                    }
                    
                    foreach( $resultList as $listItem )
                    {
                        $listManager->saveItem( $listItem );
                    }
                    unset($resultList,$listElements,$refElements,$list);
                    
                }

                foreach( $listItems as $listItem )
                {
                    $listManager->deleteItem( $listItem->getId() );
                }
                unset($listItems);
                
                if(isset($record[$this->_prefix.'parent_pss']) && !empty($record[$this->_prefix.'parent_pss'])) {
                    $pssProduct = $this->_getItem($record[$this->_prefix.'parent_pss'], null, array('product'));
                    if($pssProduct){
                        //$pvProducts = $pssProduct->getRefItems('product');
                        $pvProducts = $this->_getRefProductsArray($pssProduct->getId());
                        if(!in_array($product->getId(),$pvProducts)){
                            $productListManager = $this->getManager( $context, 'product/list');
                            $listItem = $productListManager->createItem();
                            
                            $list['product.list.typeid'] = $this->_getTypeId( 'product/list/type', str_replace('.', '/', rtrim($this->_prefix,'.')), 'default');
                            $list['product.list.parentid'] = $pssProduct->getId();
                            $list['product.list.refid'] = $product->getId();
                            $list['product.list.domain'] = str_replace('.', '/', rtrim($this->_prefix,'.'));
                            $list['product.list.status'] = 1;
                            $list['product.list.position'] = isset($record[$this->_prefix.'pos'])?$record[$this->_prefix.'pos']:1001001;
                             
                            $listItem->fromArray($list);
                            $productListManager->saveItem( $listItem );
                            unset($listItem,$list);
                        }
                        unset($pvProducts);
                    }
                    $pssProduct= null;
                }
                
                
                 $processors = $this->_getProcessors($record);
                 foreach ($processors as $processor){
                     $processor->run($product);
                 }
                
                 $manager->commit();
                 $product = null;
                 unset($listItems,$productType);
                
            }catch(Exception $e){
                $manager->rollback();
                 
                $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $code, $e->getMessage() );
                $context->getLogger()->log( $msg );
                 
                $errors++;
            }
            
        }
        return array($inserts,$updates,$deletes,$errors); 
    }
    
    
    protected function _getListItems( $productId, $types, $domain )
    {
        $manager = $this->getManager( $this->_getContext(), 'catalog/list' );
        $search = $manager->createSearch();
    
        $expr = array(
                $search->compare( '==', 'catalog.list.domain', $domain ),
                $search->compare( '==', 'catalog.list.refid', $productId ),
        );
    
        if( $types !== null ) {
            $expr[] = $search->compare( '==', 'catalog.list.type.code', $types );
        }
    
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSortations( array( $search->sort( '+', 'catalog.list.position' ) ) );
        $search->setSlice( 0, 0x7FFFFFFF );
    
        return $manager->searchItems( $search );
    }
    
    protected function _getListItemByProductId($productId, &$listItems)
    {
        foreach ($listItems as $listId => $listItem){
            if($listItem->getRefId()===$productId) {
                unset($listItems[$listId]);
                return $listItem;
            }
        }
        return false;
    }
    
    protected function _getCatalogItem($code)
    {
        $manager = $this->getManager( $this->_getContext(), 'catalog' );
        $search = $manager->createSearch();
    
        $search->setConditions( $search->compare( '==', 'catalog.code', $code ) );
        $search->setSlice( 0, 0x7FFFFFFF );
    
        return $manager->searchItems( $search );
    }
    
    
    protected function _getRefProductsArray( $productId)
    {
        $result= array();
        $manager = $this->getManager( $this->_getContext(), 'product/list' );
        $search = $manager->createSearch();
    
        $expr = array(
                $search->compare( '==', 'product.list.domain', 'product' ),
                $search->compare( '==', 'product.list.typeid', 1 ),
                $search->compare( '==', 'product.list.parentid', $productId ),
        );
    
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSlice( 0, 0x7FFFFFFF );
        $list = $manager->searchItems( $search );
        
        foreach($list as $item){
            $result[]=$item->getRefId();
        }
        
        return $result;
    }
}
