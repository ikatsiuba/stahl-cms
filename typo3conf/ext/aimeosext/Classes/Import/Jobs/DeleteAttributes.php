<?php
namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeosext\Import\Jobs;


class DeleteAttributes extends AbstractJob
{
    
    public function process(array $records)
    {
        $inserts=$updates=$deletes=$errors=0;
        $context = $this->_getContext();
        foreach ($records as $record) {
                
                $this->_prefix='attributegroup.';
                $manager = $this->getManager($context, 'attributegroup');
                $attributegroup = $this->_getItem($record['attribute_group.code'],null, array());
                if($attributegroup!==false){
                    $manager->begin();
                    try{
                        $manager->deleteItems((array)$attributegroup->getId());
                        $deletes++;
                    }catch(Exception $e){
                            $manager->rollback();
                            $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $code, $e->getMessage() );
                            $context->getLogger()->log( $msg );
                            $errors++;
                        }
                        
                }
                $manager->commit();

                $manager = $this->getManager($context, 'attribute/type');
                $this->_prefix='attribute.type.';
                $attribute = $this->_getItem($record['attribute_group.code']);
                if($attribute!==false){
                    $manager->begin();
                    try{
                        
                        $managerAttribute = $this->getManager($context, 'attribute');
                        $search = $manager->createSearch();
                        $expr =array( $search->compare( '==', 'attribute.typeid', $attribute->getId() ) );
                        $search->setConditions($search->combine( '&&', $expr ));
                        $search->setSlice( 0, 0x7FFFFFFF );
                        $ids= $managerAttribute->searchItems( $search);
                        
                        $managerList = $this->getManager($context, 'product/list');
                        $search = $manager->createSearch();
                        $expr =array( $search->compare( '==', 'product.list.refid', array_keys($ids) ) );
                        $expr[] = $search->compare( '==', 'product.list.domain', 'attribute' );
                        $search->setConditions($search->combine( '&&', $expr ));
                        $search->setSlice( 0, 0x7FFFFFFF );
                        $refs= $manager->searchItems( $search);
                        $managerList->deleteItems(array_keys($refs));

                        $manager->deleteItems((array)$attribute->getId());
                        $deletes++;
                        
                    }catch(Exception $e){
                        $manager->rollback();
                        $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $code, $e->getMessage() );
                        $context->getLogger()->log( $msg );
                        $errors++;
                    }
                
                }
                $manager->commit();
                

                $manager = $this->getManager($context, 'text/type');
                $this->_prefix='text.type.';
                $attribute = $this->_getItem($record['attribute_group.code']);
                if($attribute!==false){
                    $manager->begin();
                    try{
                        $managerAttribute = $this->getManager($context, 'text');
                        $search = $manager->createSearch();
                        $expr =array( $search->compare( '==', 'text.typeid', $attribute->getId() ) );
                        $search->setConditions($search->combine( '&&', $expr ));
                        $search->setSlice( 0, 0x7FFFFFFF );
                        $ids= $managerAttribute->searchItems( $search);
                        
                        $managerList = $this->getManager($context, 'product/list');
                        $search = $manager->createSearch();
                        $expr =array( $search->compare( '==', 'product.list.refid', array_keys($ids) ) );
                        $expr[] = $search->compare( '==', 'product.list.domain', 'text' );
                        $search->setConditions($search->combine( '&&', $expr ));
                        $search->setSlice( 0, 0x7FFFFFFF );
                        $refs= $manager->searchItems( $search);
                        $managerList->deleteItems(array_keys($refs));

                        $manager->deleteItems((array)$attribute->getId());
                        $deletes++;
                        
                    }catch(Exception $e){
                        $manager->rollback();
                        $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $code, $e->getMessage() );
                        $context->getLogger()->log( $msg );
                        $errors++;
                    }
                
                }
                $manager->commit();
                
                
        }
        return array($inserts,$updates,$deletes,$errors); 
    }
    
}
