<?php
namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeosext\Import\Jobs;


class ProductRelation extends AbstractJob
{
    protected $_productType=array('OT_ProductSubSeries'=>'select',
                                  'OT_ProductVariant'=>'default');
    
    
    public function process(array $records)
    {
        $inserts=$updates=$deletes=$errors=0;
        $context = $this->_getContext();
        $manager = $this->getManager( $context, 'product/list');
        foreach ($records as $record) {
            $this->_prefix='product.';
            $relates = array();
            if(isset($record[$this->_prefix.'parent_id']) && !empty($record[$this->_prefix.'parent_id'])){
                $relates['variant'] = $record[$this->_prefix.'parent_id'];
            }
            if(isset($record[$this->_prefix.'suggested']) && !empty($record[$this->_prefix.'suggested'])){
                $i = 0;
                foreach($record[$this->_prefix.'suggested'] as $relate_code => $val) {
                    $relates['suggested_'.$i] = $relate_code;
                    $i++;
                }
            }
            if(empty($relates) || !isset( $record[$this->_prefix.'code'] ) || empty($record[$this->_prefix.'code'])) {
                continue;
            }
            
            $product = $this->_getItem($record[$this->_prefix.'code']);
            
            foreach($relates as $relate_type => $relate_code) {

                $parentProduct = $this->_getItem($relate_code);
                if($relate_type !== 'variant' && $parentProduct===false){
                    $parentProduct = $this->_createEmptyProduct($relate_code, $record[$this->_prefix.'siteid']);
                }
                if($product === false || $parentProduct === false) {
                    continue;
                }

                $manager->begin();
                try{
                    $specialType= (isset($record[$this->_prefix.'suggested'][$relate_code]['relation_type'][0]) && !empty($record[$this->_prefix.'suggested'][$relate_code]['relation_type'][0])) ? $record[$this->_prefix.'suggested'][$relate_code]['relation_type'][0] :'suggestion';
                    $productType = $relate_type == 'variant' ? isset($this->_productType[$record[$this->_prefix.'type']]) ? $this->_productType[$record[$this->_prefix.'type']] : 'default' : $specialType;
                    $type = $this->_getTypeId( 'product/list/type', 'product', $productType );
                    $domain = str_replace('.', '/', rtrim($this->_prefix,'.'));
                    
                    $pos=(isset($record[$this->_prefix.'suggested'][$relate_code][0]) && $record[$this->_prefix.'suggested'][$relate_code][0]>0)?$record[$this->_prefix.'suggested'][$relate_code][0]:($relate_type=='variant' && isset($record[$this->_prefix.'pos'])?$record[$this->_prefix.'pos']:1001001);
                    
                    $list['product.list.parentid'] = $relate_type == 'variant' ? $parentProduct->getId() : $product->getId();
                    $list['product.list.siteid'] = $record[$this->_prefix.'siteid'];
                    $list['product.list.typeid'] = $type;
                    $list['product.list.domain'] = $domain;
                    $list['product.list.refid'] = $relate_type == 'variant' ? $product->getId() : $parentProduct->getId();
                    $list['product.list.status'] = 1;
                    $list['product.list.position'] = $pos;

                    $listItems = $this->_getListItems($list['product.list.refid'], $list['product.list.parentid'], $record[$this->_prefix.'siteid'], $type, $domain);
                    $listItem = $this->_getListItemByProductId($list['product.list.refid'], $listItems);
                    if( empty($listItem)) {
                        $listItem = $manager->createItem();
                    }
                    $listItem->fromArray($list);
                    if($listItem->getId()){
                        $updates++;
                    }else{
                        $inserts++;
                    }

                    $manager->saveItem( $listItem );
                    $manager->commit();

                }catch(Exception $e){
                    $manager->rollback();

                    $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $e->getMessage() );
                    $context->getLogger()->log( $msg );

                    $errors++;
                }
                unset($parentProduct,$specialType,$productType,$type,$listItems,$listItem,$list);
            }
            
            unset($relates,$product);
        }
        return array($inserts,$updates,$deletes,$errors); 
    }
    
    
    protected function _getListItems( $productId, $parentId, $siteId = null, $typeId = null, $domain = null)
    {
        $manager = $this->getManager( $this->_getContext(), 'product/list' );
        $search = $manager->createSearch();
    
        $expr = array(
                $search->compare( '==', 'product.list.refid', $productId ),
                $search->compare( '==', 'product.list.parentid', $parentId )
        );
    
        if( $siteId !== null ) {
            $expr[] = $search->compare( '==', 'product.list.siteid', $siteId );
        }

        if( $typeId !== null ) {
            $expr[] = $search->compare( '==', 'product.list.typeid', $typeId );
        }

        if( $domain !== null ) {
            $expr[] = $search->compare( '==', 'product.list.domain', $domain );
        }
    
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSortations( array( $search->sort( '+', 'product.list.position' ) ) );
        $search->setSlice( 0, 0x7FFFFFFF );
        $result = $manager->searchItems( $search );
        
        unset($search,$expr);
        
        return $result;
    }

    protected function _getListItemByProductId($productId, &$listItems)
    {
        foreach ($listItems as $listId => $listItem){
            if($listItem->getRefId()===$productId) {
                unset($listItems[$listId]);
                return $listItem;
            }
        }
        return false;
    }
    
    protected function _createEmptyProduct($code, $siteId)
    {
        $context = $this->_getContext();
        $manager = $this->getManager( $context, 'product');
        $product = $manager->createItem();
        $record['product.code'] = $code;
        $record['product.status'] = 0;
        $record['product.siteid'] = $siteId;
        $record['product.typeid'] = $this->_getTypeId( 'product/type', 'product', 'default' );
        
        $product->fromArray( $record );
        $manager->saveItem( $product );
        
        unset($record);
        
        return $product;
    }
    
}
