<?php
namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeosext\Import\Jobs;
use Aimeos\Aimeos\Base;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use Aimeos\Aimeosext\Import\Jobs\Processor\Attribute as Attr;

class Media extends AbstractJob
{
    protected $_spacialAttributes = array('asset.filename','AT_AssetLanguage','asset.extension',
                                          'DVS_DOKNR','DVS_LANGUAGE','ZPB_IAUTH','ZPB_GELTUNG',
                                          'DVS_DOKNR','asset.size','ZPB_EX-ZERT','ZPB_TYP','ZKO_ART','asset.size','AT_AssetDescription','AT_DocumentType','AT_SoftwareVersion','asset.uploaded');
    
    const FILES_INTERNAL = 'FilesInternal';
    const MEDIA_PATH = 'tx_aimeos';
    
    public function process(array $records)
    {
        $this->_prefix='media.';
        $inserts=$updates=$deletes=$errors=0;
        $context = $this->_getContext();
        $config = $this->_getContext()->getConfig();
        $manager = $this->getManager( $context, rtrim($this->_prefix,'.'));

        foreach ($records as $record) {

            if( (!isset( $record[$this->_prefix.'url'] ) || $record[$this->_prefix.'url'] === '') &&
                (!isset( $record[$this->_prefix.'file'] ) || $record[$this->_prefix.'file'] === '')) {
                //continue;
            }
            $multiLangFiles=array();
            
            $url=$record[$this->_prefix.'url'];
            if(empty($url)){
                $url=$record[$this->_prefix.'file'];
            }
            
            $record[$this->_prefix.'status']=1;
            if(empty($url)){
                $url='aimeos.png';
                $record[$this->_prefix.'status']=0;
                file_put_contents(PATH_site."typo3temp/logs/import_error.log", date("Y-m-d H:i:s").' NO ASSETS LOCATION: '.print_r($record[$this->_prefix.'code'], true)."\n", FILE_APPEND);
            }
            
            $filePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('uploads/tx_aimeos').'/'.$url;
            
            if(!file_exists($filePath)) {
                $url1 = isset($record[$this->_prefix.'file']) && !empty($record[$this->_prefix.'file']) ? 'pdf.jpg':'aimeos.png';
                $filePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('uploads/tx_aimeos').'/'.$url1;
                $record[$this->_prefix.'status']=0;
            }
            if(!isset($record[$this->_prefix.'label'])){
                $record[$this->_prefix.'label']=$record[$this->_prefix.'code'];
            }
            
            $storageRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
            $storage = $storageRepository->findByUid(1); //fileadmin
            
                $fileParts=pathinfo($url);
                if(isset($record[$this->_prefix.'attribute']['AT_FilenameSEO'])){
                    $fileArray=(array)$record[$this->_prefix.'attribute']['AT_FilenameSEO'];
                    foreach($fileArray as $lang => $_file){
                        $langId = isset(Attr::$_lang[$lang])?Attr::$_lang[$lang]:null;
                        $media = $this->_getItemLang($record[$this->_prefix.'code'], $langId);
                        $dirName = $fileParts['dirname'].'/'.$fileParts['filename'];
                        $searchFile = self::MEDIA_PATH."/".$dirName.'/'.$_file.'.'.$fileParts['extension'];
                        
                        $this->checkIsInternal($media, $searchFile, $dirName);
                        if($this->checkFileCondition($storage, $searchFile, $media, $record)){
                             $newFile = $this->getFileNew($storage, $dirName, $filePath, $_file.'.'.$fileParts['extension']);
                        }else{
                            $newFile = $storage->getFile($searchFile);
                        }
                        $multiLangFiles[]=array('link' =>'fileadmin'.$newFile->getIdentifier(),
                                                'label' =>is_array($record[$this->_prefix.'attribute']['AT_AssetAltTag']) && isset($record[$this->_prefix.'attribute']['AT_AssetAltTag'][$lang])?
                                                               $record[$this->_prefix.'attribute']['AT_AssetAltTag'][$lang]:
                                                               ($record[$this->_prefix.'attribute']['AT_AssetAltTag'] ? (string)$record[$this->_prefix.'attribute']['AT_AssetAltTag'] 
                                                                       :$record[$this->_prefix.'label']),
                                                'langid'=> $langId);
                        
                    }
                }else{
                    
                    $media = $this->_getItemLang($record[$this->_prefix.'code'], null);
                    $searchFile = self::MEDIA_PATH."/".$url;
                    $dirName = $fileParts['dirname'];
                    
                    $this->checkIsInternal($media, $searchFile, $dirName);
                    
                    if($this->checkFileCondition($storage, $searchFile, $media, $record)){
                        $newFile = $this->getFileNew($storage, $dirName, $filePath, $fileParts['basename']);
                    }else{
                        $newFile = $storage->getFile($searchFile);
                    }
                    $multiLangFiles[]=array('link' =>'fileadmin'.$newFile->getIdentifier(),
                                            'label' =>$record[$this->_prefix.'label'],
                                            'langid'=> null);
                    
                }

            //begin for
            foreach ($multiLangFiles as $_fl){
                $manager->begin();
                try{

                    $media = $this->_getItemLang($record[$this->_prefix.'code'],$_fl['langid']);
                    if($media===false){
                        $media = $manager->createItem();
                    }
                    
                    $lnk = $_fl['link'];
                    if($media->getId()){
                        $mediaExclude = $config->get( 'client/html/common/images/excludeimages', array() );
                        if(in_array($media->getCode(),$mediaExclude)){
                            $lnk = $media->getUrl();
                        }
                    }

                    if((isset($record[$this->_prefix.'file']) && preg_match('/Files\/.*?\.pdf$/is', $record[$this->_prefix.'file']))
                        &&
                        ((isset($record['media.attribute']['asset.uploaded']) && !$media->getStatusSolr()
                            && ($el = reset($media->getRefItems( 'text', 'asset.uploaded')))
                            && ($record['media.attribute']['asset.uploaded'] > $el->getContent())
                         ) || !$media->getId())
                    ){
                        $record[$this->_prefix.'status_solr'] = 1;
                    }
                    //die(print_r($lnk,1));
                    $record[$this->_prefix.'url'] = $lnk;//'/'.$_fl['link']; //'/'.$url;
                    $record[$this->_prefix.'typeid'] = $this->_getTypeId( 'media/type', 'product', 'default' );
                    $record[$this->_prefix.'domain'] = 'product';
                    $record[$this->_prefix.'label'] = $_fl['label'];//'product';
                    $record[$this->_prefix.'languageid'] = $_fl['langid'];
                    $record[$this->_prefix.'preview'] = '/'.isset($record[$this->_prefix.'file']) && !empty($record[$this->_prefix.'file']) ? 'pdf.jpg' : $url;
                    $record[$this->_prefix.'mimetype'] = $this->_getMimeType($filePath);// \MW_Media_Factory::get( $filePath)->getMimetype();

                    $media->fromArray( $record );
                    if($media->getId()){
                        $updates++;
                    }else{
                        $inserts++;
                    }

                    $manager->saveItem( $media );

                    if(isset($record['media.attribute']) && (!isset($record['media.type']) || !in_array($record['media.type'], array('OT_CertificateSAP','OT_EGK','OT_BA_SAP','OT_CertificateExtern')))){
                        foreach ($record['media.attribute'] as $code =>$val){
                            if(!in_array($code,$this->_spacialAttributes)){
                                unset($record['media.attribute'][$code]);
                            }
                        }
                    }

                    $processors = $this->_getProcessors($record);
                     foreach ($processors as $processor){
                         $processor->run($media);
                     }
                     
                     $manager->commit();
                    
                }catch(Exception $e){
                    $manager->rollback();
                     
                    $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $code, $e->getMessage() );
                    $context->getLogger()->log( $msg );
                     
                    $errors++;
                }
            }
            //end for
            
             
        }
        return array($inserts,$updates,$deletes,$errors); 
    }
    
    
    protected function _getItemLang($code, $lang)
    {
        $type= rtrim($this->_prefix,'.');
        if(strpos($type, '.')!==false){
            $type=str_replace('.', '/', $type);
        }
    
        $manager = $this->getManager($this->_getContext(), $type);
        $search = $manager->createSearch();
        $expr =array( $search->compare( '==', $this->_prefix.'code', $code ) );
        //if(!is_null($lang)){
            $expr[] = $search->compare( '==', $this->_prefix.'languageid', $lang );
        //}
        $search->setConditions($search->combine( '&&', $expr ));
        $search->setSlice( 0, 1 );

        return reset($manager->searchItems( $search, array('text','media','attribute')));
    }
    
    protected function _getMimeType($filename)
    {
        if( class_exists( 'finfo' ) )
        {
            try
            {
                $finfo = new \finfo( FILEINFO_MIME_TYPE );
                $mimetype = $finfo->file( $filename );
            }
            catch( Exception $e )
            {
                throw new \MW_Media_Exception( $e->getMessage() );
            }
        }
        else if( function_exists( 'mime_content_type' ) )
        {
            $mimetype = mime_content_type( $filename );
        }
        
        return $mimetype;
    }
    
    public function updateImages()
    {
        $context = $this->_getContext();
        $config = $this->_getContext()->getConfig();
        $mediaPath = $config->get( 'client/html/common/images/baseurl', '' );
        $manager = $this->getManager( $context, 'media');
        
        $search = $manager->createSearch(true);
        $expr =array( $search->compare( '==', 'media.status', 0 ) );
        $search->setConditions($search->combine( '&&', $expr ));
        $search->setSlice( 0, 0x7FFFFFFF );
        
        $mediaList = $manager->searchItems( $search);
        $storageRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
        $storage = $storageRepository->findByUid(1); //fileadmin
        
        foreach ($mediaList as $media){
            $filePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('uploads/tx_aimeos').$media->getUrl();
            if(file_exists($filePath)) {
                if(strpos($media->getUrl(), 'WEB_HighRes') !== false){
                    $fileParts=pathinfo($media->getUrl());
                    $dirName = "{$mediaPath}/".$fileParts['dirname'];
                    if(!$storage->hasFolder($dirName)){
                        $dir = $storage->createFolder($dirName);
                    }else{
                        $dir = $storage->getFolder($dirName);
                    }
                    $newFile = $storage->addFile($filePath, $dir, $fileParts['basename'],DuplicationBehavior::REPLACE, false);
                    $media->setUrl(str_replace('tx_aimeos/','',$newFile->getIdentifier()));
                }
                $media->setStatus(1);
                $manager->saveItem($media);
                
            }
        }
        unset($mediaList,$storageRepository,$storage,$dirName,$search,$manager,$filePath,$context);
    }
    
    
    public function hideInternalImages()
    {
        $context = $this->_getContext();
        $config = $this->_getContext()->getConfig();
        $manager = $this->getManager( $context, 'media');
        
        $productTypeIds = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('id','mshop_product_list_type','domain="media" and code in ("RF_VIS","RF_ProuctCommunication","RF_ProductPresentation","RF_TestReport","RF_InternalGeneral")');
        $catalogTypeIds = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('id','mshop_catalog_list_type','domain="media" and code in ("RF_VIS","RF_ProuctCommunication","RF_ProductPresentation","RF_TestReport","RF_InternalGeneral")');
        
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('id', 
                'mshop_media','status=1 and (id in (select refid from mshop_product_list where domain="media" and
                typeid in ('.implode(',',array_column($productTypeIds, 'id')).'))
                or id in (select refid from mshop_catalog_list where domain="media" and
                        typeid in ('.implode(',',array_column($catalogTypeIds, 'id')).')))');
        if($res){
            $ids = array_column($res, 'id');
            $storageRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
            $storageFileadmin = $storageRepository->findByUid(1); //fileadmin
    
            $search = $manager->createSearch(true);
            $expr =array( $search->compare( '==', 'media.id', $ids ) );
            $search->setConditions($search->combine( '&&', $expr ));
            $search->setSlice( 0, 0x7FFFFFFF );
            
            $mediaList = $manager->searchItems( $search);
            foreach ($mediaList as $media){
                $this->checkStorage($storageFileadmin, $storageFileadmin->getConfiguration()['basePath'], $media);
                $manager->saveItem($media);
            }
        }
      

        $res = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('id',
                'mshop_media','status=1 and link like "%FilesInternal/%" and (id not in (select refid from mshop_product_list where domain="media" and
                typeid in ('.implode(',',array_column($productTypeIds, 'id')).'))
                and id not in (select refid from mshop_catalog_list where domain="media" and
                        typeid in ('.implode(',',array_column($catalogTypeIds, 'id')).')))');
       
        if($res){
            $ids = array_column($res, 'id');
            $storageRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
            $storageFileadmin = $storageRepository->findByUid(1); //fileadmin
            
            $search = $manager->createSearch(true);
            $expr =array( $search->compare( '==', 'media.id', $ids ) );
            $search->setConditions($search->combine( '&&', $expr ));
            $search->setSlice( 0, 0x7FFFFFFF );
            
            $mediaList = $manager->searchItems( $search);
            foreach ($mediaList as $media){
                $url = $media->getUrl();
                $basePath = $storageFileadmin->getConfiguration()['basePath'];
                $searchFile = str_replace($basePath,'',$url);
                if($storageFileadmin->hasFile($searchFile)){ 
                    $fileNew = $storageFileadmin->getFile($searchFile);
                    $fileParts=pathinfo($url);
                    $dirName = str_replace(self::FILES_INTERNAL.'/', 'Files/', str_replace($basePath,'',$fileParts['dirname']));
                    if(!$storageFileadmin->hasFolder($dirName)){
                        $dir = $storageFileadmin->createFolder($dirName);
                    }else{
                        $dir = $storageFileadmin->getFolder($dirName);
                    }
                    $movedFile = $storageFileadmin->moveFile($fileNew,$dir,null,DuplicationBehavior::REPLACE);
                    $media->setUrl($basePath.ltrim($movedFile->getIdentifier(),'/'));
                    $manager->saveItem($media);
                }
            }
        }
        
    }
    
    protected function getFileNew($storage, $dirName, $filePath, $fileName)
    {
        if(strpos($dirName, self::MEDIA_PATH) === false){
           $dirName = self::MEDIA_PATH."/".$dirName;
        }
        if(!$storage->hasFolder($dirName)){
           $dir = $storage->createFolder($dirName);
        }else{
           $dir = $storage->getFolder($dirName);
        }
        $newFile = $storage->addFile($filePath, $dir, $fileName, DuplicationBehavior::REPLACE, false);
        
        return $newFile;
    }
    
    
    protected function checkStorage($storage, $basePath, &$media)
    {
        $url = $media->getUrl();
        $searchFile = str_replace($basePath,'',$url);
        if($storage->hasFile($searchFile)){
            if(strpos($searchFile, self::FILES_INTERNAL)!==false){
                $searchFileNew = str_replace(self::FILES_INTERNAL.'/','Files/',$searchFile);
                if($storage->hasFile($searchFileNew)){
                    $fileNew = $storage->getFile($searchFileNew);
                    $dir= $this->getInternalDir($fileNew->getIdentifier(), $storage);
                    $movedFile = $storage->moveFile($fileNew,$dir,null,DuplicationBehavior::REPLACE);
                }
            }else{
                $file= $storage->getFile($searchFile);
                $dir= $this->getInternalDir($file->getIdentifier(), $storage);
                $movedFile = $storage->moveFile($file,$dir,null,DuplicationBehavior::REPLACE);
                $media->setUrl($basePath.ltrim($movedFile->getIdentifier(),'/'));
            }
        }
        
    }
    
    protected function checkFileCondition($storage, $searchFile, $media, $record )
    {   
        return (!$storage->hasFile($searchFile) || !$media || empty($record['media.attribute']['asset.uploaded']) ||
            (isset($record['media.attribute']['asset.uploaded']) && ($el = reset($media->getRefItems( 'text', 'asset.uploaded')))
                    && ($record['media.attribute']['asset.uploaded'] != $el->getContent())));
    }
    
    protected function checkIsInternal($media, &$searchFile, &$dirName)
    {
        if($media && $media->getId() && strpos($media->getUrl(), self::FILES_INTERNAL)!==false){
            $searchFile = str_replace('Files/',self::FILES_INTERNAL.'/',$searchFile);
            $dirName = str_replace('Files/',self::FILES_INTERNAL.'/',$dirName);
        }
    }
    
    
    protected function getInternalDir($url, $storage)
    {
        $fileParts=pathinfo($url);
        $dirName = str_replace('/Files','/'.self::FILES_INTERNAL, $fileParts['dirname']);
        if(!$storage->hasFolder($dirName)){
            $dir = $storage->createFolder($dirName);
        }else{
            $dir = $storage->getFolder($dirName);
        }
        return $dir;
    }
    
    
    protected function getStorageByName($name)
    {
        $storage = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
        $collection= $storage->findAll();
        foreach ($collection as $_item){
            if($_item->getName()==$name){
                return $_item;
            }
        }
        return false;
    }
}
