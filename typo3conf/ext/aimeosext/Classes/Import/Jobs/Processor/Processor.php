<?php

namespace Aimeos\Aimeosext\Import\Jobs\Processor;

abstract class Processor
{
    protected $_data;
    protected $_contect;
    protected $_prefix;
    private static $_types = array();
    private static $_managers = array();
    
    public function __construct($context, array $data, $prefix)
    {
        $this->_context = $context;
        $this->_data = $data;
        $this->_prefix = $prefix;
    }
    
    public function _getContext()
    {
        return $this->_context;
    }

    protected function getManager($context, $type){
    
        if( !isset( self::$_managers[$type] ) ){
            self::$_managers[$type] = \MShop_Factory::createManager( $context, $type );
        }
        return self::$_managers[$type];
    }
    
    protected function _getTypeId( $path, $domain, $code )
    {
        if( !isset( self::$_types[$path][$domain] ) )
        {
            $manager = $this->getManager( $this->_getContext(), $path );
            $key = str_replace( '/', '.', $path );
    
            $search = $manager->createSearch();
            $search->setConditions( $search->compare( '==', $key . '.domain', $domain ) );
            $search->setSlice( 0, 0x7FFFFFFF );
            foreach( $manager->searchItems( $search ) as $id => $item ) {
                self::$_types[$path][$domain][ $item->getCode() ] = $id;
            }
            unset($manager,$search);
        }
    
        if( !isset( self::$_types[$path][$domain][$code] ) ) {
        
            $manager = $this->getManager($this->_getContext(), $path);
            $key = str_replace( '/', '.', $path );
        
            $record[$key . '.status'] = 1;
            $record[$key . '.domain'] = $domain;
            $record[$key . '.code'] = $code;
            $record[$key . '.label'] = $code;
            $typeRecord = $manager->createItem();
            $typeRecord->fromArray( $record );
            $manager->saveItem( $typeRecord );
        
            self::$_types[$path][$domain][ $code ] = $typeRecord->getId();
            unset($manager,$record,$typeRecord);
        
        }
        
        if( !isset( self::$_types[$path][$domain][$code] ) ) {
            throw new \Exception( sprintf( 'No type item for "%1$s/%2$s" in "%3$s" found', $domain, $code, $path ) );
        }
    
        return self::$_types[$path][$domain][$code];
    }
    
    public abstract function run($object);
    
    protected function _getListItemByRefCode($code, &$listItems)
    {
        foreach ($listItems as $listId => $listItem){
            if($refItem=$listItem->getRefItem()){
                if($this->_compareCode($refItem, $code)) {
                    unset($listItems[$listId]);
                    return $listItem;
                }
            }
        }
        return false;
    }
    
    protected function _compareCode($refItem, $code)
    {
        if($refItem instanceof \MShop_Text_Item_Interface) {
            $str1= str_replace(array("ä","ü","ö","Ä","Ü","Ö","ß"),array('a','u','o','A','U','O','s'),$refItem->getLabel());
            $str2= str_replace(array("ä","ü","ö","Ä","Ü","Ö","ß"),array('a','u','o','A','U','O','s'),$code);
            if(trim($str1)===trim($str2)){
                return trim($str1)===trim($str2);
            }else{
                return strtolower($str1)===strtolower($str2);
            }
            //return strtolower($refItem->getLabel())===strtolower($code);
        }else{
            if(trim($refItem->getCode())===trim($code)){
                return trim($refItem->getCode())===trim($code);
            }else{
                return strtolower($refItem->getCode())===strtolower($code);
            }
        }
    }
}