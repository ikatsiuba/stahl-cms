<?php

namespace Aimeos\Aimeosext\Import\Jobs\Processor;


class Mediadelete extends Processor
{
    public function run($object)
    {

        $listManager = $this->getManager( $this->_getContext(), rtrim($this->_prefix,'.').'/list' );
        $manager = $this->getManager( $this->_getContext(), 'media' );
        
        
        $manager->begin();
        try
        {
            
            
            $listItems = $object->getListItems('media');
            
            foreach( $this->_data as $code => $value )
            {
                $listItem = $this->_getListItemByRefCode($code, $listItems);
                if($listItem) {
                    $listManager->deleteItem( $listItem->getId() );
                }
            }
        
            $manager->commit();
        }
        catch( Exception $e )
        {
            $manager->rollback();
            throw $e;
        }
        
    }
    
    protected function _getItem($code)
    {
        $manager = $this->getManager( $this->_getContext(),'media');
        $search = $manager->createSearch();
    
        $search->setConditions( $search->compare( '==', 'media.code', $code ) );
        $search->setSlice(0);
    
        return reset($manager->searchItems( $search));
    }

    protected function _getRefItem($id)
    {
        $manager = $this->getManager( $this->_getContext(),'media');
        $search = $manager->createSearch();
    
        $search->setConditions( $search->compare( '==', 'media.id', $id ) );
        $search->setSlice(0);
    
        return reset($manager->searchItems( $search));
    }
    
    protected function _createEmptyMedia($code, $object)
    {
        $context = $this->_getContext();
        $manager = $this->getManager( $context, 'media');
        
        $filePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('uploads/tx_aimeos').'/aimeos.png';
        $manager->begin();
        $media = $manager->createItem();
        $record['media.code'] = $code;
        $record['media.label'] = $code;
        $record['media.url'] = '/aimeos.png';
        $record['media.status'] = 0;
        $record['media.typeid'] = $this->_getTypeId( 'media/type', 'product', 'default' );
        $record['media.domain'] = 'product';
        $record['media.preview'] = 'aimeos.png';
        $record['media.mimetype'] = \MW_Media_Factory::get( $filePath)->getMimetype();
        
        $media->fromArray( $record );
        $manager->saveItem( $media );
        $manager->commit();
        
        return $media;
    }
    
}