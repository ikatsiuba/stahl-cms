<?php

namespace Aimeos\Aimeosext\Import\Jobs\Processor;


class Media extends Processor
{
    public function run($object)
    {

        $listManager = $this->getManager( $this->_getContext(), rtrim($this->_prefix,'.').'/list' );
        $manager = $this->getManager( $this->_getContext(), 'media' );
        
        
        $manager->begin();
        try
        {
            
            $listItems = $object->getListItems('media');
            foreach ($listItems as $item){
                if(!$item->getRefItem()){
                    $ref = $this->_getRefItem($item->getRefId());
                    if($ref){
                        $item->setRefItem($this->_getRefItem($item->getRefId()));
                    }
                }
            }
            
            foreach( $this->_data as $code => $value )
            {
                $mediaType= (isset($value['relation_type']) && !empty($value['relation_type'])) ? array_unique($value['relation_type']) :array('default');
                $mediaPos= isset($value['DisplaySequence']) ? $value['DisplaySequence'][0] : 0;
                $list[$this->_prefix.'list.parentid'] = $object->getId();
                $list[$this->_prefix.'list.domain'] = 'media';
                $list[$this->_prefix.'list.position'] = $mediaPos;
                $list[$this->_prefix.'list.status'] = 1;
                $resultList = array();
                foreach ($mediaType as $mtp){
                    $typeId = $this->_getTypeId( rtrim($this->_prefix,'.').'/list/type', 'media', $mtp);
                    
                    $listElements = $refElements = array();
                    while ($listItem = $this->_getListItemByRefCodeTypeId($code, $typeId, $listItems)){
                        $listElements[]=$listItem;
                    }
                    //die(print_r($listItems,1));
                    $refElements = $this->_getItem($code);
                    if(empty($refElements)){
                        $refElements = $this->_createEmptyMedia($code,$object);
                    }
                    
                    foreach ($listElements as $listItem){
                        $refItem = $listItem->getRefItem();
                        if(isset($refElements[$refItem->getId()])){
                            unset($refElements[$refItem->getId()]);
                        }
                        $list[$this->_prefix.'list.refid'] = $refItem->getId();
                        $list[$this->_prefix.'list.typeid'] = $typeId;
                        
                        $listItem->fromArray($list);
                        $resultList[] = $listItem;
                    }
                    foreach ($refElements as $refItem){
                        $listItem = $listManager->createItem();
                        $list[$this->_prefix.'list.refid'] = $refItem->getId();
                        $list[$this->_prefix.'list.typeid'] = $typeId;
                        $listItem->fromArray($list);
                        $resultList[] = $listItem;
                    }
                
                }
                foreach( $resultList as $listItem )
                {
                    $listManager->saveItem( $listItem );
                }
                unset($resultList,$listElements,$refElements,$list, $mediaType, $mediaPos);
                
            }
            
            foreach( $listItems as $listItem )
            {
                $listManager->deleteItem( $listItem->getId() );
            }
        
            $manager->commit();
            unset($listItems, $listManager, $manager);
        }
        catch( Exception $e )
        {
            $manager->rollback();
            throw $e;
        }
        
    }
    
    protected function _getItem($code)
    {
        $manager = $this->getManager( $this->_getContext(),'media');
        $search = $manager->createSearch();
    
        $search->setConditions( $search->compare( '==', 'media.code', $code ) );
        $search->setSlice(0);

        return $manager->searchItems( $search);
    }

    protected function _getRefItem($id)
    {
        $manager = $this->getManager( $this->_getContext(),'media');
        $search = $manager->createSearch();
    
        $search->setConditions( $search->compare( '==', 'media.id', $id ) );
        $search->setSlice(0);
    
        return reset($manager->searchItems( $search));
    }
    
    protected function _createEmptyMedia($code, $object)
    {
        $context = $this->_getContext();
        $manager = $this->getManager( $context, 'media');
        
        $filePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('uploads/tx_aimeos').'/aimeos.png';
        $manager->begin();
        $media = $manager->createItem();
        $record['media.code'] = $code;
        $record['media.label'] = $code;
        $record['media.url'] = '/aimeos.png';
        $record['media.status'] = 0;
        $record['media.typeid'] = $this->_getTypeId( 'media/type', 'product', 'default' );
        $record['media.domain'] = 'product';
        $record['media.preview'] = 'aimeos.png';
        $record['media.mimetype'] = \MW_Media_Factory::get( $filePath)->getMimetype();
        
        $media->fromArray( $record );
        $manager->saveItem( $media );
        $manager->commit();
        
        return array($media->getId()=>$media);
    }
    
    protected function _getListItemByRefCodeTypeId($code, $typeId,  &$listItems)
    {
        foreach ($listItems as $listId => $listItem){
            if($refItem=$listItem->getRefItem()){
                if($this->_compareCode($refItem, $code) && $listItem->getTypeId()==$typeId) {
                    unset($listItems[$listId]);
                    return $listItem;
                }
            }
        }
        return false;
    }
    
}