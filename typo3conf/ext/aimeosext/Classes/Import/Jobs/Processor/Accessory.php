<?php

namespace Aimeos\Aimeosext\Import\Jobs\Processor;


class Accessory extends Processor
{
    public function run($object)
    {

        $listManager = $this->getManager( $this->_getContext(), rtrim($this->_prefix,'.').'/list' );
        $manager = $this->getManager( $this->_getContext(), 'product' );
       
        
        $manager->begin();
        try
        {
            
            $listItems = $object->getListItems('product');
            foreach ($listItems as $item){
                if(!$item->getRefItem()){
                    $ref = $this->_getRefItem($item->getRefId());
                    if($ref){
                        $item->setRefItem($this->_getRefItem($item->getRefId()));
                    }
                }
            }
            
            foreach( $this->_data as $code => $value )
            { 
                $listItem = $this->_getListItemByRefCode($code, $listItems);
                
                if( $listItem) {
                    $refItem = $listItem->getRefItem();
                } else {
                    $refItem = $this->_getItem($code);
                    if($refItem === false){
                        $refItem = $this->_createEmptyProduct($code, $object->getSiteId());
                    }
                    if($refItem && $refItem->getId()){
                        $listItem = $listManager->createItem();
                    }else{
                        continue;
                    }
                }

                $mediaType= (isset($value['relation_type']) && !empty($value['relation_type'])) ? $value['relation_type'][0] :'default';
                $mediaPos= isset($value['DisplaySequence']) ? $value['DisplaySequence'][0] : 0;
                $list[$this->_prefix.'list.typeid'] = $this->_getTypeId( rtrim($this->_prefix,'.').'/list/type', 'product', $mediaType);
                $list[$this->_prefix.'list.parentid'] = $object->getId();
                $list[$this->_prefix.'list.refid'] = $refItem->getId();
                $list[$this->_prefix.'list.domain'] = 'product';
                $list[$this->_prefix.'list.position'] = $mediaPos;
                $list[$this->_prefix.'list.status'] = 1;
     
                $listItem->fromArray( $list );
                $listManager->saveItem( $listItem );
            }
             
            foreach( $listItems as $listItem )
            {
                /*if($listItem->getRefItem()){
                    $manager->deleteItem( $listItem->getRefItem()->getId() );
                }*/
         
               if($listItem->getType()!=='default'){ 
                   $listManager->deleteItem( $listItem->getId() );
               }
            }
        
            $manager->commit();
        }
        catch( Exception $e )
        {
            $manager->rollback();
            throw $e;
        }
        
    }
    
    protected function _getItem($code)
    {
        $manager = $this->getManager( $this->_getContext(),'product');
        $search = $manager->createSearch();
    
        $search->setConditions( $search->compare( '==', 'product.code', $code ) );
        $search->setSlice(0);
    
        return reset($manager->searchItems( $search));
    }

    protected function _getRefItem($id)
    {
        $manager = $this->getManager( $this->_getContext(),'product');
        $search = $manager->createSearch();
    
        $search->setConditions( $search->compare( '==', 'product.id', $id ) );
        $search->setSlice(0);
    
        return reset($manager->searchItems( $search));
    }
    
    protected function _createEmptyProduct($code, $siteId)
    {
        $context = $this->_getContext();
        $manager = $this->getManager( $context, 'product');
        $product = $manager->createItem();
        $record['product.code'] = $code;
        $record['product.status'] = 0;
        $record['product.siteid'] = $siteId;
        $record['product.typeid'] = $this->_getTypeId( 'product/type', 'product', 'default' );
        
        $product->fromArray( $record );
        $manager->saveItem( $product );
        return $product;
    }
}