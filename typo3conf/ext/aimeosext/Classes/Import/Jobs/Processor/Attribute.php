<?php

namespace Aimeos\Aimeosext\Import\Jobs\Processor;


class Attribute extends Processor
{
    const VARIANT_ATTRIBUTE = 'AT_SAP_MainMaterialID';
    protected $_items = array();
    
    protected $_map = array('attribute'=>array('code','label'),
                            'text'=>array('label','content'));

    protected $_explodeAttr = array('AT_CertificatesSAP'=>',','AT_ZoneFilter'=>'<multisep/>', 'ZMN00006_Calc'=>'<multisep/>');
    
    public static $_lang = array('de-DE'=>'de','Context2'=>'de','en-GB'=>'en','Context1'=>'en');
   
    
    public function run($object)
    {
        
        foreach( $this->_data as $code => $value ) {
            $item = $this->_getItem($code, 'attribute/type', 'attribute.type',rtrim($this->_prefix,'.'));
            if($item && $item->getId()) {
                if(is_array($value)) {
                    if(array_intersect(array_keys($value),array_keys(self::$_lang))){
                        if(in_array($code,array_keys($this->_explodeAttr))){
                            foreach($value as $key => $val) {
                                $contextVal = array_unique(explode($this->_explodeAttr[$code],$val));
                                foreach ($contextVal as $k => $v){
                                    $this->_items['attribute'][$code.'|||'.$k][$key]=trim($v);
                                }
                            }
                        }else {
                            if(is_array(reset($value))){
                                foreach($value as $key => $val){
                                    foreach ($val as $k => $v){
                                        $this->_items['attribute'][$code.'|||'.$k][$key] = $v;
                                    }
                                }
                            }else{
                                $this->_items['attribute'][$code] = $value;
                            }
                        }
                    } else {
                        $value = array_intersect_key($value, array_unique(array_map('strtolower', $value)));
                        foreach($value as $key => $val) {
                            $this->_items['attribute'][$code.'|||'.$key]=$val;
                        }
                    }
                } else {
                    $this->_items['attribute'][$code]=$value;
                }
                unset($this->_data[$code]);
            }
        }
        unset($item, $contextVal);
        
        foreach( $this->_data as $code => $value ) {
            $item = $this->_getItem($code, 'text/type', 'text.type',rtrim($this->_prefix,'.'));
            if($item && $item->getId()) {
                if(is_array($value)) {
                    if(array_intersect(array_keys($value),array_keys(self::$_lang)) ){
                        foreach($value as $key => $val) {
                            $this->_items['text'][$code.'|||'.$key] = is_array($val) ? implode("</br>", $val) : $val;
                        }
                    }else {
                        $this->_items['text'][$code] = implode("</br>", $value);
                    }
                } else {
                    $this->_items['text'][$code] = $value;
                }
                unset($this->_data[$code]);
            }
        }
        unset($item);
        if(count($this->_data)) {
            throw new \Exception (sprintf( 'Attributes "%1$s" not available. Please import attributes for beginning.', implode(',',array_keys($this->_data))));
        }
        //die(print_r($this->_items,1));
        foreach ($this->_items as $key => $value){
            $this->_processData($object, $key, $value);
        } 
        unset($this->_items);
        
        
    }
    
    protected function _processData($object, $type, $items)
    {
        
        $listManager = $this->getManager( $this->_getContext(), rtrim($this->_prefix,'.').'/list' );
        $manager = $this->getManager( $this->_getContext(), $type );
        $manager->begin();
        try
        {
            $listItems = $object->getListItems($type);
            foreach( $items as $code => $value ){
                $list = array();
                $codeList = explode("|||",$code);
                $lang='';
                if(isset($codeList[1]) && in_array($codeList[1],array_keys(self::$_lang))){
                  $lang=self::$_lang[$codeList[1]];  
                }
                $code=$codeList[0];
                $refVal=$value;
                if(is_array($value)){
                    if(isset($value['en-GB'])){
                        $refVal=$value['en-GB'];
                    }else{
                        $refVal=reset($value);
                    }
                }
                preg_match('/(.*)UnItVaLuE:::(.*)$/is', $refVal, $arrUnit);
                if(isset($arrUnit[2])){
                    $list[$type.'.unitvalue']=$arrUnit[2];
                    $refVal = $arrUnit[1];
                }
                
                $refCode = $lang.$code.'-'.$refVal;
                
                $listItem = $this->_getListItemByRefCode($refCode, $listItems);
                if( $listItem) {
                    $refItem = $listItem->getRefItem();
                } else {
                    $listItem = $listManager->createItem();
                    $refItem = $this->_getItem($refCode, $type, $type, rtrim($this->_prefix,'.'));
                    if($refItem === false){
                        $refItem = $manager->createItem();
                    }
                }
                
                $list[$type.'.typeid'] = $this->_getTypeId( $type.'/type', rtrim($this->_prefix,'.'), $code);
                $list[$type.'.domain'] = rtrim($this->_prefix,'.');
                $list[$type.'.status'] = 1;
                if($lang){
                    $list[$type.'.languageid'] = $lang;
                }
                $_lst = $this->_setValue($list, $type, $lang.$code, $refVal);
                if($this->_validate($refItem, $_lst )){
                    $refItem->fromArray( $_lst);
                    $manager->saveItem( $refItem );
                }
                
                if(is_array($value)){
                    $this->_storeAttributeLangData($code,$value,$refItem);
                }
                
                $attr_type = 'default';
                if($code === self::VARIANT_ATTRIBUTE) {
                    $attr_type = 'variant';
                }
                $list[$this->_prefix.'list.typeid'] = $this->_getTypeId( rtrim($this->_prefix,'.').'/list/type', $type, $attr_type );
                $list[$this->_prefix.'list.parentid'] = $object->getId();
                $list[$this->_prefix.'list.refid'] = $refItem->getId();
                $list[$this->_prefix.'list.domain'] = $type;
                $list[$this->_prefix.'list.status'] = 1;
                
                if($this->_validate($listItem, $list )){
                    $listItem->fromArray( $list );
                    $listManager->saveItem( $listItem );
                }
                
                $listItem=$refItem=null;
                unset($listItem,$refItem,$list,$attr_type);
                
            }
            foreach( $listItems as $listItem){ 
                $listManager->deleteItem( $listItem->getId() );
                if($listItem->getRefItem()){
                    $cnt = $this->_getRefItemsCount($listManager,$type, $listItem->getRefItem()->getId());
                    if($cnt==0){
                         $manager->deleteItem( $listItem->getRefItem()->getId());
                    }
                }
            }
            unset($listItems);
        
            $manager->commit();
        }
        catch( Exception $e )
        {
            $manager->rollback();
            throw $e;
        }
    }
    
    
    protected function _getItem($code, $path, $type, $domain=null)
    {
        $manager = $this->getManager( $this->_getContext(), $path );
        $search = $manager->createSearch();
    
        $expr =array( $search->compare( '==', $type.($type==='text' ? '.label':'.code'), $code ) );
        if(!is_null($domain)){
            $expr[] = $search->compare( '==', $type.'.domain', $domain );
        }
        $search->setConditions($search->combine( '&&', $expr ));
        $search->setSlice( 0, 1 );
    
        $result = reset($manager->searchItems( $search, array('text')));
        unset($search, $manager);
        
        return $result;
    }
    
    protected function _setValue($list, $type, $code, $value)
    {
        $list[$type.'.'.$this->_map[$type][0]]=$code.'-'.$value;
        $list[$type.'.'.$this->_map[$type][1]]=$value;
        return $list;
    }

    protected function _getTypeById($id, $path)
    {
        $manager = $this->getManager( $this->_getContext(), $path );
        $key = str_replace( '/', '.', $path );
        $search = $manager->createSearch();
        $search->setConditions( $search->compare( '==', $key . '.id', $id ) );
        $type = $manager->searchItems( $search);
        unset($search, $manager);
        if(isset($type[$id]) && !empty($type[$id])) {
            return $type[$id]->getCode();
        }
        return false;
    }
    
    protected function _storeAttributeLangData($code,$value,$attributeItem)
    {
        $attributeListManager = $this->getManager( $this->_getContext(), 'attribute/list' );
        $textManager = $this->getManager( $this->_getContext(), 'text' );
        $attributeListItems = $attributeItem->getListItems('text');
        if(empty($attributeListItems)){
            $attributeItem = $this->_getItem($attributeItem->getCode(), 'attribute', 'attribute');
            $attributeListItems = $attributeItem->getListItems('text');
        }
        foreach($value as $key => $val){
                
            $refCode = self::$_lang[$key].$code.'-'.$val;
            $listItem = $this->_getListItemByRefCode($refCode, $attributeListItems);
            if( $listItem) {
                $refItem = $listItem->getRefItem();
            } else {
                $listItem = $attributeListManager->createItem();
                $refItem = $this->_getItem($refCode, 'text', 'text', 'attribute');
                if($refItem === false){
                    $refItem = $textManager->createItem();
                }
            }
            
            $list['text.typeid'] = $this->_getTypeId('text/type', 'attribute', 'name');
            $list['text.domain'] = 'attribute';
            $list['text.status'] = 1;
            $list['text.languageid'] = self::$_lang[$key];
            $list['text.label'] = $refCode;
            $list['text.content'] = $val;
            if($this->_validate($refItem, $list )){
                $refItem->fromArray($list);
                $textManager->saveItem( $refItem );
            }
            
            unset($list,$val,$refCode);
            
            $list['attribute.list.typeid'] = $this->_getTypeId( 'attribute/list/type', 'text', 'default' );
            $list['attribute.list.parentid'] = $attributeItem->getId();
            $list['attribute.list.refid'] = $refItem->getId();
            $list['attribute.list.domain'] = 'text';
            $list['attribute.list.status'] = 1;
            if($this->_validate($listItem, $list )){
                $listItem->fromArray( $list );
                $attributeListManager->saveItem( $listItem );
            }
            
            unset($listItem,$list,$refItem);
       }
       
       foreach( $attributeListItems as $listItem){
           $attributeListManager->deleteItem( $listItem->getId() );
       }
       unset($attributeListItems);
        
    }
    
    protected function _getRefItemsCount($listManager,$type, $refId)
    {
        $search = $listManager->createSearch();
        $expr = array();
        $expr[] = $search->compare( '==', rtrim($this->_prefix,'.') . '.list.refid', $refId );
        $expr[] = $search->compare( '==', rtrim($this->_prefix,'.') . '.list.domain', $type );
        $search->setConditions( $search->combine( '&&', $expr ) );
        $list = $listManager->searchItems( $search);
        unset($search);
        return count($list);
    }
    
    protected function _validate ($refItem, $to)
    {  $result=false;
       if(empty($refItem->getId())){
           return true;
       }
       $from = $refItem->toArray();
        foreach ($to as $key => $value){
            if(isset($from[$key]) && strtolower($from[$key])!==strtolower($to[$key])){
                $result = true;
            }
        }
        return $result;
    }
}