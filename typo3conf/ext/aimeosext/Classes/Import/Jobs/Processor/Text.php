<?php

namespace Aimeos\Aimeosext\Import\Jobs\Processor;


class Text extends Processor
{
    public function run($object)
    {

        $listManager = $this->getManager( $this->_getContext(), rtrim($this->_prefix,'.').'/list' );
        $manager = $this->getManager( $this->_getContext(), 'text' );
        $manager->begin();
        
        try
        {
            $listItems = $object->getListItems( 'text' );
        
            foreach( $this->_data as $code => $value )
            {
        
                if( ( $listItem = array_shift( $listItems ) ) !== null ) {
                    $refItem = $listItem->getRefItem();
                } else {
                    $listItem = $listManager->createItem();
                    $refItem = $manager->createItem();
                }
     
                $typecode = 'default';
                $list['text.typeid'] = $this->_getTypeId( 'text/type', rtrim($this->_prefix,'.'), $code);
                $list['text.domain'] = rtrim($this->_prefix,'.');
                $list['text.status'] = 1;
                $list['text.label'] = $code;
                $list['text.content'] = $value;
                
                $refItem->fromArray( $list );
                $manager->saveItem( $refItem );
                
                $list[$this->_prefix.'list.typeid'] = $this->_getTypeId( rtrim($this->_prefix,'.').'/list/type', 'text', $typecode );
                $list[$this->_prefix.'list.parentid'] = $object->getId();
                $list[$this->_prefix.'list.refid'] = $refItem->getId();
                $list[$this->_prefix.'list.domain'] = 'text';
                $list[$this->_prefix.'list.status'] = 1;
     
                $listItem->fromArray( $list );
                $listManager->saveItem( $listItem );
            }
             
            foreach( $listItems as $listItem )
            {
                $manager->deleteItem( $listItem->getRefItem()->getId() );
                $listManager->deleteItem( $listItem->getId() );
            }
        
            $manager->commit();
        }
        catch( Exception $e )
        {
            $manager->rollback();
            throw $e;
        }
        
    }
    
}