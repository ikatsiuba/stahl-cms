<?php
namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeosext\Import\Jobs;


class AttributeGroup extends AbstractJob
{
    
    public function process(array $records)
    {
        $this->_prefix='attributegroup.';
        $inserts=$updates=$deletes=$errors=0;
        $context = $this->_getContext();
        $manager = $this->getManager($context, rtrim($this->_prefix,'.'));
        foreach ($records as $record) {
            $manager->begin();
            try{
                $catalog = $this->_getItem($record[$this->_prefix.'code']);
                if($catalog===false){
                    $catalog = $manager->createItem();
                }
                if( isset($record[$this->_prefix.'parent_id']) && $item = $this->_getItem($record[$this->_prefix.'parent_id'])) {
                    $parentId =$item->getId();
                }else{
                    $parentId =$this->_getItem('AttributeGroupList')->getId();
                }
                 
                $record[$this->_prefix.'status'] = 1;
                $catalog->fromArray($record);
                
                if($catalog->getId()>0){
                    $manager->saveItem($catalog);
                    if($catalog->getNode()->__get('parentid')!=$parentId){
                        $manager->moveItem($catalog->getId(),$catalog->getNode()->__get('parentid'), $parentId);
                    }
                    $updates++;
                } else {
                    $manager->insertItem($catalog, $parentId);
                    $inserts++;
                }
                
                $processors = $this->_getProcessors($record);
                foreach ($processors as $processor){
                    $processor->run($catalog);
                }
                $manager->commit();
                
            }catch(Exception $e){
                $manager->rollback();
                 
                $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $code, $e->getMessage() );
                $context->getLogger()->log( $msg );
                 
                $errors++;
            }
        }
        return array($inserts,$updates,$deletes,$errors); 
    }
    
}
