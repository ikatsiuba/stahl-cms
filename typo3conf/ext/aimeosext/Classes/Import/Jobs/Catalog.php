<?php
namespace Aimeos\Aimeosext\Import\Jobs;

use Aimeos\Aimeosext\Import\Jobs;


class Catalog extends AbstractJob
{
    protected $_internalCategories = array('WC_249654','WC_249748','WC_249749','WC_314536');
    
    public function process(array $records)
    {
        $this->_prefix='catalog.';
        $inserts=$updates=$deletes=$errors=0;
        $context = $this->_getContext();
        $manager = $this->getManager( $context, rtrim($this->_prefix,'.'));
        foreach ($records as $record) {
            
            $manager->begin();
            try{
                $catalog = $this->_getItem($record[$this->_prefix.'code'],null,array('text','media','attribute','product'));
                 
                if($catalog===false){
                    $catalog = $manager->createItem();
                }
        
                if( isset($record[$this->_prefix.'parent_id']) && $this->_getItem($record[$this->_prefix.'parent_id'])) {
                    $parentId =$this->_getItem($record[$this->_prefix.'parent_id'])->getId();
                }else{
                    $parentId =$this->_getItem('home')->getId();
                }
                 
                $record[$this->_prefix.'status']=1;
                if (in_array($record[$this->_prefix.'code'],$this->_internalCategories) 
                        || (isset($record[$this->_prefix.'attribute']['AT_WebDisplay']) && $record[$this->_prefix.'attribute']['AT_WebDisplay']==2) ){
                    $record[$this->_prefix.'status']=0;
                }
                if(empty($record[$this->_prefix.'label'])){
                    $record[$this->_prefix.'label']=$record[$this->_prefix.'code'];
                }
                $catalog->fromArray($record);
                
                if($catalog->getId()>0){
                    if($catalog->getNode()->__get('parentid')!=$parentId){
                        $manager->moveItem($catalog->getId(),$catalog->getNode()->__get('parentid'), $parentId);
                    }else{
                        $manager->saveItem($catalog);
                    }
                    $updates++;
                } else {
                    $manager->insertItem($catalog,$parentId);
                    $inserts++;
                }
                
                /*if($filters = isset($record['catalog.attribute']['AT_WebFilter']) ? (array)explode(',',$record['catalog.attribute']['AT_WebFilter']):array() ){
                    $this->_applyFilters($catalog, $filters);
                    unset($record['catalog.attribute']['AT_WebFilter']);
                }*/
                if(isset($record['catalog.attribute']['AT_WebFilter'])){
                    unset($record['catalog.attribute']['AT_WebFilter']);
                }
                $processors = $this->_getProcessors($record);
                foreach ($processors as $processor){
                    $processor->run($catalog);
                }
                $manager->commit();
                $catalog=null;
                unset($catalog,$parentId);
                
            }catch(Exception $e){
                $manager->rollback();
                 
                $msg = sprintf( 'Unable to import object with code "%1$s": %2$s', $code, $e->getMessage() );
                $context->getLogger()->log( $msg );
                 
                $errors++;
            }
             
        }
        return array($inserts,$updates,$deletes,$errors); 
    }
    
    protected function _applyFilters($catalog, $filters)
    {
        $context = $this->_getContext();
        $manager = $this->getManager( $context, 'filter');
        $listManager = $this->getManager($context, 'catalog/list');
        
        $search = $manager->createSearch( true );
        $expr = array(
                $search->compare( '==', 'filter.code', $filters ),
                $search->compare( '==', 'filter.type.code', 'default')
        );
        
        $search->setConditions( $search->combine( '&&', $expr ) );
        $filterList = $manager->searchItems( $search);
        
        $listItems = $this->_getListItems( $catalog->getId(), array('default') ,array('filter'));
        
        foreach ($filterList as $filter){

            $listItem = $this->_getListItemByFilterId($filter->getId(), $listItems);
            if( $listItem === false) {
                $listItem = $listManager->createItem();
            }
            $list['catalog.list.typeid'] = $this->_getTypeId( 'catalog/list/type', 'filter', 'default');
            $list['catalog.list.parentid'] = $catalog->getId();
            $list['catalog.list.refid'] = $filter->getId();
            $list['catalog.list.domain'] = 'filter';
            $list['catalog.list.status'] = 1;
             
            $listItem->fromArray($list);
            $listManager->saveItem( $listItem );
            
            if(($key = array_search($filter->getCode(), $filters)) !== false) {
                unset($filters[$key]);
            }
            
        }
        
        foreach( $listItems as $listItem )
        {
            $listManager->deleteItem( $listItem->getId() );
        }
        if(count($filters)){
            file_put_contents(PATH_site."typo3temp/logs/import_error.log", date("Y-m-d H:i:s").' Not existent Filters: '.$catalog->getCode().' '.print_r(implode(',',$filters), true)."\n", FILE_APPEND);
        }
        
    }
    
    protected function _getListItems( $catalogId, $types, $domain )
    {
        $manager = $this->getManager( $this->_getContext(), 'catalog/list' );
        $search = $manager->createSearch();
    
        $expr = array(
                $search->compare( '==', 'catalog.list.domain', $domain ),
                $search->compare( '==', 'catalog.list.parentid', $catalogId ),
        );
    
        if( $types !== null ) {
            $expr[] = $search->compare( '==', 'catalog.list.type.code', $types );
        }
    
        $search->setConditions( $search->combine( '&&', $expr ) );
        $search->setSlice( 0, 0x7FFFFFFF );
    
        return $manager->searchItems( $search );
    }
    
    protected function _getListItemByFilterId($catalogId, &$listItems)
    {
        foreach ($listItems as $listId => $listItem){
            if($listItem->getRefId()===$catalogId) {
                unset($listItems[$listId]);
                return $listItem;
            }
        }
        return false;
    }
    
    
}
