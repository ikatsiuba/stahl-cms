<?php
namespace Aimeos\Aimeosext\Import;

use Aimeos\Aimeos\Base;
use Aimeos\Aimeosext\Import\Jobs;


class ExternalImport extends \tx_externalimport_importer
{

    protected $_file = null;
    
    protected $_data = null;
    
    protected $_dom = null;
    
    protected $_config = null;
    
    private static $locale;
    
    /**
     * This method stores the imported data in the database
     * New data is inserted, existing data is updated and absent data is deleted
     *
     * @param    array        $records: records containing the data
     * @return    void
     */
    protected function storeData($records) 
    {
        if ($this->extConf['debug'] || TYPO3_DLOG) {
            \TYPO3\CMS\Core\Utility\GeneralUtility::devLog('Data received for storage', $this->extKey, 0, $records);
        }
        file_put_contents(PATH_site."typo3temp/records.xxx", print_r($records, true)."\n", FILE_APPEND);
        $classname = 'Aimeos\\Aimeosext\\Import\\Jobs\\'.ucfirst($this->externalConfig['manager']);
        if( class_exists( $classname ) === false ) {
            throw new \Exception( sprintf( 'Manager job "%1$s" not available', $classname ) );
        }
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
        $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,'aimeos');
        $time1 = microtime(true);
        $job = new $classname($settings);
        $result = $job->process($records);
        $time2 = microtime(true);
        file_put_contents(PATH_site."typo3temp/profiler.xxx", __METHOD__.'::'.($time2-$time1)."\n", FILE_APPEND);
        file_put_contents(PATH_site."typo3temp/profiler.xxx", __METHOD__.'::memory::'.number_format(memory_get_usage())."\n", FILE_APPEND);
        $logger = \MAdmin_Log_Manager_Factory::createManager( $job->_getContext() );
        
          //Set informational messages
        $this->messages[\TYPO3\CMS\Core\Messaging\FlashMessage::OK][] = sprintf($this->externalConfig['manager'].'-'.$GLOBALS['LANG']->getLL('records_inserted'), $result[0]);
        $this->messages[\TYPO3\CMS\Core\Messaging\FlashMessage::OK][] = sprintf($this->externalConfig['manager'].'-'.$GLOBALS['LANG']->getLL('records_updated'), $result[1]);
        $this->messages[\TYPO3\CMS\Core\Messaging\FlashMessage::OK][] = sprintf($this->externalConfig['manager'].'-'.$GLOBALS['LANG']->getLL('records_deleted'), $result[2]);
        
        if($result[3]){
            $this->messages[\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR][] = sprintf($GLOBALS['LANG']->getLL('records_errors'), $result[3]);
        }
        $logger->log( sprintf( $this->getFile().'  %s', implode( ',', $this->messages[\TYPO3\CMS\Core\Messaging\FlashMessage::OK]) ), \MW_Logger_Abstract::ERR );
        
    }
    
    public function extractValueFromNode($node, $columnData)
    {
        // Get the named attribute, if defined
        if (!empty($columnData['attribute'])) {
            // Use namespace or not
            if (empty($columnData['attributeNS'])) {
                $value = $node->attributes->getNamedItem($columnData['attribute'])->nodeValue;
            } else {
                $value = $node->attributes->getNamedItemNS($columnData['attributeNS'], $columnData['attribute'])->nodeValue;
            }
        
            // Otherwise directly take the node's value
        } else {
            // If "xmlValue" is set, we want the node's inner XML structure as is.
            // Otherwise, we take the straight node value, which is similar but with tags stripped.
            if (empty($columnData['xmlValue'])) {
                $value = preg_replace(array("/<lt\/>/","/<gt\/>/","/0xA0/","/ /"),array("<",">"," "," "),html_entity_decode($node->nodeValue));
            } else {
                $value = $this->getXmlValue($node);
            }
        }
        return $value;
    }
    
    public function getXmlValue($node)
    {
        return parent::getXmlValue($node);
    }
    
    public function setFile($filename) {
        $this->_file = $filename;
    }
    
    public function getFile() {
        return $this->_file;
    }
    
    public function setData($data) {
        $this->_data = $data;
    }
    
    public function getData() {
        return $this->_data;
    }
    public function clearData(){
        $this->_file = null;
        $this->_data = null;
        $this->_dom = null;
    }
    
    /**
     * This method calls on the distant data source and synchronises the data in the TYPO3 database
     * It returns information about the results of the operation
     *
     * @param	string		$table: name of the table to synchronise
     * @param	integer		$index: index of the synchronisation configuration to use
     * @return	array		List of error or success messages
     */
    public function synchronizeDataAllJobs($table, $index) {
        // If the user has enough rights on the table, proceed with synchronization
        if ($GLOBALS['BE_USER']->check('tables_modify', $table)) {
            $this->initTCAData($table, $index);
            $abortImportProcess = FALSE;
            // Instantiate specific connector service
            if (empty($this->externalConfig['connector'])) {
                $this->addMessage(
                        $GLOBALS['LANG']->getLL('no_connector')
                        );
            } elseif(is_null($this->_data)) {
                $services = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::findService('connector', $this->externalConfig['connector']);
    
                // The service is not available
                if ($services === FALSE) {
                    $this->addMessage(
                            $GLOBALS['LANG']->getLL('no_service')
                            );
                } else {
                    /** @var $connector tx_svconnector_base */
                    $connector = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstanceService('connector', $this->externalConfig['connector']);
    
                    // The service was instantiated, but an error occurred while initiating the connection
                    // If the returned value is an array, an error has occurred
                    if (is_array($connector)) {
                        $this->addMessage(
                                $GLOBALS['LANG']->getLL('data_not_fetched')
                                );
    
                        // The connection is established, get the data
                    } else {
                        $data = array();
    
                        if($this->_file){
                            $this->externalConfig['parameters']['uri']=$this->_file;
                        }
                        // Pre-process connector parameters
                        $this->externalConfig['parameters'] = $this->processParameters($this->externalConfig['parameters']);
    
                        // A problem may happen while fetching the data
                        // If so, the import process has to be aborted
                        $abortImportProcess = FALSE;
                        switch ($this->externalConfig['data']) {
                            case 'xml':
                                try {
                                    $this->_data = $connector->fetchXML($this->externalConfig['parameters']);
                                }
                                catch (Exception $e) {
                                    $abortImportProcess = TRUE;
                                    $this->addMessage(
                                            sprintf($GLOBALS['LANG']->getLL('data_not_fetched_connector_error'), $e->getMessage())
                                            );
                                }
                                break;
    
                            case 'array':
                                try {
                                    $this->_data = $connector->fetchArray($this->externalConfig['parameters']);
                                }
                                catch (Exception $e) {
                                    $abortImportProcess = TRUE;
                                    $this->addMessage(
                                            sprintf($GLOBALS['LANG']->getLL('data_not_fetched_connector_error'), $e->getMessage())
                                            );
                                }
                                break;
    
                                // If the data type is not defined, issue error and abort process
                            default:
                                $abortImportProcess = TRUE;
                                $this->addMessage(
                                        $GLOBALS['LANG']->getLL('data_type_not_defined')
                                        );
                                break;
                        }
                    }
                }
            }
            
            if (!$abortImportProcess && !is_null($this->_data)) {  
                $this->handleData($this->_data);
            }
            
            // Call connector's post-processing with a rough error status
            $errorStatus = FALSE;
            if (count($this->messages[\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR]) > 0) {
                $errorStatus = TRUE;
            }
            // The user doesn't have enough rights on the table
            // Log error
        } else {
            $userName = $GLOBALS['BE_USER']->user['username'];
            $this->addMessage(
                    sprintf($GLOBALS['LANG']->getLL('no_rights_for_sync'), $userName, $table)
                    );
        }
    
        // Log results to devlog
        if ($this->extConf['debug'] || TYPO3_DLOG) {
            $this->logMessages();
        }
        
        return $this->messages;
    }
    
    protected function handleXML($rawData) {
        $data = array();
    
        // Load the XML into a DOM object
        if(is_null($this->_dom)){
            $this->_dom = new \DOMDocument();
            $this->_dom->loadXML($rawData, LIBXML_PARSEHUGE);
        }
        // Instantiate a XPath object and load with any defined namespaces
        $xPathObject = new \DOMXPath($this->_dom);
        if (isset($this->externalConfig['namespaces']) && is_array($this->externalConfig['namespaces'])) {
            foreach ($this->externalConfig['namespaces'] as $prefix => $uri) {
                $xPathObject->registerNamespace($prefix, $uri);
            }
        }
    
        // Get the nodes that represent the root of each data record
        $records = $this->_dom->getElementsByTagName($this->externalConfig['nodetype']);
        //die(print_r($this->_dom,1));
        for ($i = 0; $i < $records->length; $i++) {
            /** @var DOMElement $theRecord */
            $theRecord = $records->item($i);
            $theData = array();
            // Loop on the database columns and get the corresponding value from the import data
            foreach ($this->tableTCA['columns'] as $columnName => $columnData) {
                // Act only if there's an external import definition
                if (isset($columnData['external'][$this->columnIndex])) {
                    // If a "field" is defined, refine the selection to get the correct node
                    if (isset($columnData['external'][$this->columnIndex]['field'])) {
                        // Use namespace or not
                        if (empty($columnData['external'][$this->columnIndex]['fieldNS'])) {
                            $nodeList = $theRecord->getElementsByTagName($columnData['external'][$this->columnIndex]['field']);
                        } else {
                            $nodeList = $theRecord->getElementsByTagNameNS($columnData['external'][$this->columnIndex]['fieldNS'], $columnData['external'][$this->columnIndex]['field']);
                        }
    
                        if ($nodeList->length > 0) {
                            /** @var $selectedNode DOMNode */
                            $selectedNode = $nodeList->item(0);
                            // If an XPath expression is defined, apply it (relative to currently selected node)
                            if (!empty($columnData['external'][$this->columnIndex]['xpath'])) {
                                try {
                                    $selectedNode = $this->selectNodeWithXpath(
                                            $xPathObject,
                                            $columnData['external'][$this->columnIndex]['xpath'],
                                            $selectedNode
                                            );
                                }
                                catch (Exception $e) {
                                    // Nothing to do, data is ignored
                                }
                            }
                            $theData[$columnName] = $this->extractValueFromNode(
                                    $selectedNode,
                                    $columnData['external'][$this->columnIndex]
                                    );
                        }
    
                        unset($nodeList);
                        // Without "field" property, use the current node itself
                    } else {
                        // If an XPath expression is defined, apply it (relative to current node)
                        if (!empty($columnData['external'][$this->columnIndex]['xpath'])) {
                            try {  
                                $selectedNode = $this->selectNodeWithXpath(
                                        $xPathObject,
                                        $columnData['external'][$this->columnIndex]['xpath'],
                                        $theRecord
                                        );
                                $theData[$columnName] = $this->extractValueFromNode(
                                        $selectedNode,
                                        $columnData['external'][$this->columnIndex]
                                        );
                                
                                unset($selectedNode);
                            }
                            catch (\Exception $e) {
                                // Nothing to do, data is ignored
                            }
                        } else {
                            $theData[$columnName] = $this->extractValueFromNode(
                                    $theRecord,
                                    $columnData['external'][$this->columnIndex]
                                    );
                        }
                    }
                }
            }
    
            // Get additional fields data, if any
            if ($this->numAdditionalFields > 0) {
                foreach ($this->additionalFields as $fieldName) {
                    $node = $theRecord->getElementsByTagName($fieldName);
                    if ($node->length > 0) {
                        $theData[$fieldName] = $node->item(0)->nodeValue;
                    }
                }
            }
    
            unset($theRecord);
            
            if (count($theData) > 0) {
                $data[] = $theData;unset($theData);
            }
        }
        unset($records, $xPathObject);
        return $data;
    }
    
    /**
     * This method synchronises all the external tables, respecting the order of priority
     *
     * @return array List of all messages
     */
    public function synchronizeAllTables() {
    
        $externalTables = array();
        foreach ($GLOBALS['TCA'] as $tableName => $sections) {
            if (isset($sections['ctrl']['external'])) {
                foreach ($sections['ctrl']['external'] as $index => $externalConfig) {
                    if (!empty($externalConfig['connector']) && $externalConfig['connector']=='feed') {
                        // Default priority if not defined, set to very low
                        $priority = 1000;
                        if (isset($externalConfig['priority'])) {
                            $priority = $externalConfig['priority'];
                        }
                        if (!isset($externalTables[$priority])) $externalTables[$priority] = array();
                        $externalTables[$priority][] = array('table' => $tableName, 'index' => $index, 'manager'=>$externalConfig['manager']);
                    }
                }
            }
        }
        
        // Sort tables by priority (lower number is highest priority)
        ksort($externalTables);
        if ($this->extConf['debug'] || TYPO3_DLOG) {
            \TYPO3\CMS\Core\Utility\GeneralUtility::devLog($GLOBALS['LANG']->getLL('sync_all'), $this->extKey, 0, $externalTables);
        }
        $repository = \TYPO3\CMS\Core\Utility\GeneralUtility::getUserObj('Tx_ExternalImport_Domain_Repository_ConfigurationRepository');
        $storage = $this->getStorageByName('import/');
        
        $files = $this->prepareFilesCollection($externalTables, $storage, $repository);
        $allMessages = array();
        foreach ($files as $key => $fl){
            $uri = PATH_site.$storage->getPublicUrl($fl['file']);
            $this->setFile(PATH_site.'/'.$storage->getPublicUrl($fl['file']));
            foreach ($externalTables as $tables) {
                foreach ($tables as $tableData) {
                    $this->messages = array(\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR => array(), 
                                            \TYPO3\CMS\Core\Messaging\FlashMessage::WARNING => array(), 
                                            \TYPO3\CMS\Core\Messaging\FlashMessage::OK => array());
                    if(in_array($tableData['manager'],$fl['manager']) && $fl['store']==$tableData['index']){
                        $messages = $this->synchronizeDataAllJobs($tableData['table'], $tableData['index']);
                        $key = $fl['file']->getName().'/'.$tableData['table'] .'/'.$tableData['index'];
                        $allMessages[\TYPO3\CMS\Core\Messaging\FlashMessage::OK][] = $messages[\TYPO3\CMS\Core\Messaging\FlashMessage::OK];
                    }
                }
            }
            $this->clearData();
            $repository->archiveFile($storage, $fl['file']);
        }
        
        //refresh indexes
        $aimeos = \Aimeos\Aimeos\Base::getAimeos();
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
        $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,'aimeos');
//         $config = \Aimeos\Aimeos\Base::getConfig($settings);
//         $context = \Aimeos\Aimeos\Base::getContext( $config );
        
        if(count($files)>0){ 
           $media = new \Aimeos\Aimeosext\Import\Jobs\Media($settings);
           $media->updateImages();
           $media->hideInternalImages();
           \Controller_Jobs_Factory::createController( $this->_getContext(), $aimeos, 'catalog/index/rebuild' )->run();
        }        
        // Return compiled array of messages for all imports
        
        unset($files,$externalTables,$repository,$storageRepository,$storage);
            
        return $allMessages;
    }

    protected function getStorageByName($name)
    {
        $storage = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
        $collection= $storage->findAll();
        foreach ($collection as $_item){
            if($_item->getName()==$name){
                return $_item;
            }
        }
        return false;
    }
    
    
    public function prepareFilesCollection($externalTables, $storage, $repository)
    {
        $filesCollection=array();
        foreach ($externalTables as $priority =>$tables) {
            foreach ($tables as $key => $tableData) {
                $files = $storage->getFilesInFolder($storage->getFolder($tableData['index']),0,0,true,false,'name');
                foreach ($files as $fl){
                    $uri = PATH_site.$storage->getPublicUrl($fl);
                    $jobs= $repository->checkFile($uri,$tableData['index']);
                    if(empty($jobs['manager'])){
                        $repository->trashFile($storage, $fl);
                        continue;
                    }
                    if(in_array($tableData['manager'],$jobs['manager'])){
                        $filesCollection[$fl->getIdentifier()]=array('file'=>$fl,'manager'=>$jobs['manager'],'store'=>$tableData['index']);
                    }
                    unset($uri,$jobs);
                }
                unset($files);
            }
        }
        return $filesCollection;
    }
    
    protected function _getContext()
    {
        if(is_null($this->_config)){
            //$settings = self::convertTypoScriptArrayToPlainArray($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_aimeos.']['settings.']);
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
            $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
            $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,'aimeos');
            $config = \Aimeos\Aimeos\Base::getConfig( $settings);
            $context = \Aimeos\Aimeos\Base::getContext( $config );
    
            $localI18n = ( isset( $settings['i18n'] ) ? $settings['i18n'] : array() );
            $locale = $this->getLocale( $context );
            $langid = $locale->getLanguageId();
            $context->setLocale( $locale );
            $context->setI18n( \Aimeos\Aimeos\Base::getI18n( array( $langid ), $localI18n ) );
            $this->_config = $context;
        }
    
        return $this->_config;
    }
    
    protected function getLocale( \MShop_Context_Item_Interface $context )
    {
        if( !isset( self::$locale ) )
        {
            $session = $context->getSession();
            $config = $context->getConfig();
    
            $sitecode = $config->get( 'mshop/locale/site', 'default' );
            $name = $config->get( 'typo3/param/name/site', 'loc-site' );
    
            $langid = $config->get( 'mshop/locale/language', '' );
            $name = $config->get( 'typo3/param/name/language', 'loc-language' );
    
            if( isset( $GLOBALS['TSFE']->config['config']['language'] ) ) {
                $langid = $GLOBALS['TSFE']->config['config']['language'];
            }
            $currency = $config->get( 'mshop/locale/currency', '' );
            $name = $config->get( 'typo3/param/name/currency', 'loc-currency' );
    
            $localeManager = \MShop_Locale_Manager_Factory::createManager( $context );
            self::$locale = $localeManager->bootstrap( $sitecode, $langid, $currency );
        }
    
        return self::$locale;
    }
    protected static function convertTypoScriptArrayToPlainArray(array $typoScriptArray)
    {
        foreach ($typoScriptArray as $key => &$value) {
            if (substr($key, -1) === '.') {
                $keyWithoutDot = substr($key, 0, -1);
                $hasNodeWithoutDot = array_key_exists($keyWithoutDot, $typoScriptArray);
                $typoScriptNodeValue = $hasNodeWithoutDot ? $typoScriptArray[$keyWithoutDot] : NULL;
                if (is_array($value)) {
                    $typoScriptArray[$keyWithoutDot] = self::convertTypoScriptArrayToPlainArray($value);
                    if (!is_null($typoScriptNodeValue)) {
                        $typoScriptArray[$keyWithoutDot]['_typoScriptNodeValue'] = $typoScriptNodeValue;
                    }
                    unset($typoScriptArray[$key]);
                } else {
                    $typoScriptArray[$keyWithoutDot] = NULL;
                }
            }
        }
        return $typoScriptArray;
    }
}
