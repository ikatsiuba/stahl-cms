<?php
return array(
	'ctrl' => array(
		'title'	=> 'Product stock tmp',
		'label' => 'prodid',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
	    'rootLevel' => 1,
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
	    'external' => array (
	        'stock' => array(
	            'connector' => 'csv',
	            'parameters' => array(
	                'filename' => PATH_site. 'import/stock/HAVAS_DOWNLOAD.CSV',
	                'delimiter' => ";",
	                'text_qualifier' => '',
	                'encoding' => 'utf8',
	                'skip_rows' => 1
	            ),
	            'data' => 'array',
	            'priority' => 6000,
	            'reference_uid' => 'prodid',
	            'disabledOperations' => 'delete',
	            'description' => 'List of products stock',
                'manager' => 'Stock',
	        )
	    ),
		'searchFields' => 'prodid,material_id,material_name,besch_art,menge,gwzt,pzt,',
	    'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('aimeosext') . 'Resources/Public/Images/product.png'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, prodid, material_id, material_name, besch_art, menge, gwzt, pzt',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, prodid, material_id, material_name, besch_art, menge, gwzt, pzt, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'material_id' => array(
			'exclude' => 1,
			'label' => 'material_id',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		    'external' =>array(
		        'stock' => array(
		            'field' => 'MATNR'
		        )
		    )
		),
/*		'material_name' => array(
			'exclude' => 1,
			'label' => 'material_name',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
		    'external' =>array(
		        'stock' => array(
		            'field' => 'Materialkurztext'
		        )
		    )
		),*/
		'besch_art' => array(
			'exclude' => 1,
			'label' => 'besch_art',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		    'external' =>array(
		        'stock' => array(
		            'field' => 'B'
		        )
		    )
		),
		'menge' => array(
			'exclude' => 1,
			'label' => 'menge',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'double2'
			),
		    'external' =>array(
		        'stock' => array(
		            'field' => 'ATP'
		        )
		    )
		),
		'gwzt' => array(
			'exclude' => 1,
			'label' => 'gwzt',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		    'external' =>array(
		        'stock' => array(
		            'field' => 'WBZ'
		        )
		    )
		),
		'pzt' => array(
			'exclude' => 1,
			'label' => 'pzt',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			),
		    'external' =>array(
		        'stock' => array(
		            'field' => 'PLZ'
		        )
		    )
		),
        'status' => array(
                'exclude' => 1,
                'label' => 'pzt',
                'config' => array(
                        'type' => 'input',
                        'size' => 4,
                        'eval' => 'int'
                ),
                'external' =>array(
                        'stock' => array(
                                'field' => 'status',
                                'value' => 0
                        )
                )
        ),
	),
);