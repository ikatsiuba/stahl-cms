<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

return array(
	'ctrl' => array(
		'title' => 'Classifications',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY name',
        'hideTable' =>true,
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('aimeosext') . 'Resources/Public/Images/product.png',
		'external' => array(
			'base' => array(
				'connector' => 'feed',
				'data' => 'xml',
				'nodetype' => 'Classification',
				'reference_uid' => 'code',
				'priority' => 4500,
				// NOTE: this would not make sense in a real-life configuration. A separate pid would be used.
				'disabledOperations' => 'delete',
				'description' => 'Classifications',
			    'manager' => 'Catalog', 
			),
		),
      'searchFields' => 'title'
	),
	'interface' => array(
		'showRecordFieldList' => 'code,label'
	),
	'columns' => array(
		'catalog.siteid' => array(
			'exclude' => 0,
			'label' => 'SiteId',
			'config' => array(
				'type' => 'input',
				'size' => '1',
				'eval' => 'required',
			),
			'external' => array(
				'base' => array(
					'field' => 'siteid',
					'value' => 1
				)
			)
		),
		'catalog.code' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '255',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'attribute' => 'ID'
				)
			)
		),
		'catalog.label' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './Name',
				)
			)
		),
	    'catalog.parent_id' => array(
	                'exclude' => 0,
	                'label' => 'Label',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => '(parent::Classification)[1]',
	                                'attribute' => 'ID'
	                        )
	                )
	    ),
	    'catalog.attribute' => array(
	                'exclude' => 0,
	                'label' => 'Text',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => './MetaData',
	                                'xmlValue' => true,
	                                'userFunc' => array(
	                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                        'method' => 'processAttributes',
	                                        'params' => array(
	                                                'xpath' => 'Value[@AttributeID]|MultiValue[@AttributeID]/ValueGroup|ValueGroup[@AttributeID]|MultiValue[not(ValueGroup)]',
	                                                'attribute' => 'AttributeID',
	                                        )
	                                )
	                        )
	                )
	    ),
	    'catalog.media' => array(
	                'exclude' => 0,
	                'label' => 'Media',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => '.',
	                                'xmlValue' => true,
	                                'userFunc' => array(
	                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                        'method' => 'processAttributes',
	                                        'params' => array(
	                                                'xpath' => 'AssetCrossReference',
	                                                'attribute' => 'AssetID',
	                                                'value_attribute' => 'Type',
	                                        )
	                                )
	                        )
	                )
	    ),
	    'catalog.accessory' => array(
	                'exclude' => 0,
	                'label' => 'Product',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => '.',
	                                'xmlValue' => true,
	                                'userFunc' => array(
	                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                        'method' => 'processAttributes',
	                                        'params' => array(
	                                                'xpath' => 'ProductCrossReference',
	                                                'attribute' => 'ProductID',
	                                                'value_attribute' => 'Type',
	                                        )
	                                )
	                        )
	                )
	    ),
	    /*'catalog.childrens' => array(
	                'exclude' => 0,
	                'label' => 'childrens',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => '.',
	                                'xmlValue' => true,
	                                'userFunc' => array(
	                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                        'method' => 'processAttributes',
	                                        'params' => array(
	                                                'xpath' => 'Classification[@UserTypeID="OT_WebClassficationFolder"]',
	                                                'attribute' => 'ID'
	                                        )
	                                )
	                        )
	                )
	        ),*/
	         
	),
	'types' => array(
		'0' => array('showitem' => 'code,label')
	),
);
