<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

return array(
	'ctrl' => array(
		'title' => 'Product relations',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY name',
        'hideTable' =>true,
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('aimeosext') . 'Resources/Public/Images/product.png',
		'external' => array(
			'base' => array(
				'connector' => 'feed',
				'data' => 'xml',
				'nodetype' => 'Product',
				'reference_uid' => 'product.code',
				'priority' => 5500,
				// NOTE: this would not make sense in a real-life configuration. A separate pid would be used.
				'disabledOperations' => 'delete',
				'description' => 'Product relations',
			    'manager' => 'ProductRelation',
			),
		),
	    'searchFields' => 'title'	        
	),
	'interface' => array(
		'showRecordFieldList' => 'product.code'
	),
	'columns' => array(
		'product.siteid' => array(
			'exclude' => 0,
			'label' => 'SiteId',
			'config' => array(
				'type' => 'input',
				'size' => '1',
				'eval' => 'required',
			),
			'external' => array(
				'base' => array(
					'field' => 'siteid',
					'value' => 1
				)
			)
		),
		'product.code' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '255',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'attribute' => 'ID'
				)
			)
		),
		'product.type' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '255',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'attribute' => 'UserTypeID'
				)
			)
		),
		'product.parent_id' => array(
			'exclude' => 0,
			'label' => 'parentId',
			'config' => array(
				'type' => 'input',
				'size' => '255',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => './parent::Product',
					'attribute' => 'ID'
				)
			)
		),
		'product.attribute' => array(
			'exclude' => 0,
			'label' => 'Attribute',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './Values/Value[@AttributeID="AT_SAP_MainMaterialID"]'
				)
			)
		),
	    'product.pos' => array(
	        'exclude' => 0,
	        'label' => 'Position',
	        'config' => array(
	            'type' => 'input',
	            'size' => '255',
	        ),
	        'external' => array(
	            'base' => array(
	                'xpath' => './Values/Value[@AttributeID="DisplaySequence"]'
	            )
	        )
	    ),
		'product.suggested' => array(
			'exclude' => 0,
			'label' => 'Suggested products',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'xmlValue' => true,
					'userFunc' => array(
						'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
						'method' => 'processAttributes',
						'params' => array(
							'xpath' => 'ProductCrossReference',
							'attribute' => 'ProductID',
				            'value_attribute' => 'Type'
						)
					)
				)
			)
		),
	),
	'types' => array(
		'0' => array('showitem' => 'product.code')
	),
);
