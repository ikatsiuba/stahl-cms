<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

return array(
	'ctrl' => array(
		'title' => 'Attributes',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY name',
        'hideTable' =>true,
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('aimeosext') . 'Resources/Public/Images/product.png',
		'external' => array(
			'base' => array(
				'connector' => 'feed',
				'data' => 'xml',
				'nodetype' => 'Attribute',
				'reference_uid' => 'code',
				'priority' => 4100,
				// NOTE: this would not make sense in a real-life configuration. A separate pid would be used.
				'disabledOperations' => 'delete',
				'description' => 'Attributes',
			    'manager' => 'attribute'
			),
		),
	        'searchFields' => 'title'
    ),
	'interface' => array(
		'showRecordFieldList' => 'code,label'
	),
	'columns' => array(
		'siteid' => array(
			'exclude' => 0,
			'label' => 'SiteId',
			'config' => array(
				'type' => 'input',
				'size' => '1',
				'eval' => 'required',
			),
			'external' => array(
				'base' => array(
					'field' => 'siteid',
					'value' => 1
				)
			)
		),
		'code' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '32',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'attribute' => 'ID'
				)
			)
		),
		'label' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './Name | ./MetaData/ValueGroup[@AttributeID="DisplayName"]/Value[@QualifierID="en-GB"] | ./MetaData/Value[@AttributeID="DisplayName"]',
				)
			)
		),
		'attributegroup' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
				    'xmlValue' => true,
				    'userFunc' => array(
				                'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
				                'method' => 'processAttributes',
				                'params' => array(
				                        'xpath' => 'AttributeGroupLink',
				                        'attribute' => 'AttributeGroupID',
				                )
				        )
				)
			)
		),
		'user_type' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
			        'xpath' => '.',
			        'xmlValue' => true,
			        'userFunc' => array(
			                'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
			                'method' => 'processAttributes',
			                'params' => array(
			                        'xpath' => 'UserTypeLink',
			                        'attribute' => 'UserTypeID',
			                )
			        )
				)
			)
		),
		'base_type' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './Validation',
				    'attribute' => 'BaseType'
				)
			)
		),
		'unit' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './Validation/UnitLink',
				    'attribute' => 'UnitID'
				)
			)
		),
	    'web_filter' => array(
			'exclude' => 0,
			'label' => 'Web filter',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './MetaData/ValueGroup[@AttributeID="AT_WebFilter"]//Value|./MetaData/MultiValue[@AttributeID="AT_WebFilter"]//Value',
					'xmlValue' => true,
				)
			)
		),
	    'attribute' => array(
	                'exclude' => 0,
	                'label' => 'Text',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => './MetaData',
	                                'xmlValue' => true,
	                                'userFunc' => array(
	                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                        'method' => 'processAttributes',
	                                        'params' => array(
	                                                'xpath' => 'Value[@AttributeID]|MultiValue[@AttributeID]/ValueGroup|ValueGroup[@AttributeID]|MultiValue[not(ValueGroup)]',
	                                                'attribute' => 'AttributeID',
	                                        )
	                                )
	                        )
	                )
	    ),
	    'web_style' => array(
			'exclude' => 0,
			'label' => 'Web style',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './MetaData/ValueGroup[@AttributeID="AT_WebStyle"]/Value[@LOVQualifierID="en-GB"]',
					'attribute' => 'ID'
				)
			)
		),
	),
	'types' => array(
		'0' => array('showitem' => 'code,label')
	),
);
