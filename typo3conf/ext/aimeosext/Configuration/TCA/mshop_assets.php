<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

return array(
	'ctrl' => array(
		'title' => 'Assets',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY name',
	    'hideTable' =>true,
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('aimeosext') . 'Resources/Public/Images/product.png',
		'external' => array(
			'base' => array(
				'connector' => 'feed',
				'data' => 'xml',
				'nodetype' => 'Asset',
				'reference_uid' => 'code',
				'priority' => 4200,
				// NOTE: this would not make sense in a real-life configuration. A separate pid would be used.
				'disabledOperations' => 'delete',
				'description' => 'Assets',
			    'manager' => 'Media'
			),
		),
        'searchFields' => 'title'
	),
	'interface' => array(
		'showRecordFieldList' => 'code,label'
	),
	'columns' => array(
		'media.siteid' => array(
			'exclude' => 0,
			'label' => 'SiteId',
			'config' => array(
				'type' => 'input',
				'size' => '1',
				'eval' => 'required',
			),
			'external' => array(
				'base' => array(
					'field' => 'siteid',
					'value' => 1
				)
			)
		),
		'media.code' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '32',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'attribute' => 'ID'
				)
			)
		),
		'media.label' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './Name',
				)
			)
		),
        'media.type' => array(
                'exclude' => 0,
                'label' => 'Label',
                'config' => array(
                        'type' => 'input',
                        'size' => '255',
                ),
                'external' => array(
                        'base' => array(
                                'xpath' => '.',
                                'attribute' => 'UserTypeID'
                        )
                )
        ),
		'media.url' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './AssetPushLocation[@ConfigurationID="WEB_HighRes"]',
				)
			)
		),
		'media.file' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './AssetPushLocation[@ConfigurationID="Files"]',
				)
			)
		),
         'media.attribute' => array(
                'exclude' => 0,
                'label' => 'Attribute',
                'config' => array(
                        'type' => 'input',
                        'size' => '255',
                ),
                'external' => array(
                        'base' => array(
                                'xpath' => './Values',
                                'xmlValue' => true,
                                'userFunc' => array(
                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
                                        'method' => 'processAttributes',
                                        'params' => array(
                                                'xpath' => 'Value[@AttributeID]|MultiValue[@AttributeID]/ValueGroup|ValueGroup[@AttributeID]|MultiValue[not(ValueGroup)]',
                                                'attribute' => 'AttributeID',
                                        )
                                )
                        )
                )
        ),
	),
	'types' => array(
		'0' => array('showitem' => 'code,label')
	),
);
