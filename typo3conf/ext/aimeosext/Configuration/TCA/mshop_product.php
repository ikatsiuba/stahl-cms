<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

return array(
	'ctrl' => array(
		'title' => 'Products',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY name',
        'hideTable' =>true,
         'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('aimeosext') . 'Resources/Public/Images/product.png',
		'external' => array(
			'base' => array(
				'connector' => 'feed',
				'data' => 'xml',
				'nodetype' => 'Product',
				'reference_uid' => 'product.code',
				'priority' => 5000,
				// NOTE: this would not make sense in a real-life configuration. A separate pid would be used.
				'disabledOperations' => 'delete',
				'description' => 'Products',
			    'manager' => 'Product', 
			),
		),
       'searchFields' => 'title'
	),
	'interface' => array(
		'showRecordFieldList' => 'product.code'
	),
	'columns' => array(
		'product.siteid' => array(
			'exclude' => 0,
			'label' => 'SiteId',
			'config' => array(
				'type' => 'input',
				'size' => '1',
				'eval' => 'required',
			),
			'external' => array(
				'base' => array(
					'field' => 'siteid',
					'value' => 1
				)
			)
		),
		'product.code' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '255',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'attribute' => 'ID'
				)
			)
		),
		'product.type' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '255',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'attribute' => 'UserTypeID'
				)
			)
		),
        'product.label' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './Name',
				)
			)
		),
	    'product.category_id' => array(
	                'exclude' => 0,
	                'label' => 'Label',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                            'xpath' => '.',
	                            'xmlValue' => true,
	                            'userFunc' => array(
	                                'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                'method' => 'processAttributes',
	                                'params' => array(
	                                    'xpath' => 'ClassificationReference[@Type="RF_WebClassification"]',
	                                    'attribute' => 'ClassificationID',
	                                //    'value_attribute' => 'Type',
	                                )
	                            )
	                                //'xpath' => './ClassificationReference[@Type="RF_WebClassification"]',
	                                //'attribute' => 'ClassificationID'
	                        )
	                )
	    ),
        'product.parent_pss' => array(
                'exclude' => 0,
                'label' => 'Label',
                'config' => array(
                        'type' => 'input',
                        'size' => '255',
                ),
                'external' => array(
                        'base' => array(
                                'xpath' => '.',
                                'attribute' => 'ParentID'
                        )
                )
        ),	        
	    'product.attribute' => array(
	                'exclude' => 0,
	                'label' => 'Text',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => './Values',
	                                'xmlValue' => true,
	                                'userFunc' => array(
	                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                        'method' => 'processAttributes',
	                                        'params' => array(
	                                                'xpath' => 'Value[@AttributeID]|MultiValue[@AttributeID]/ValueGroup|ValueGroup[@AttributeID]|MultiValue[not(ValueGroup)]',
	                                                'attribute' => 'AttributeID',
	                                        )
	                                )
	                        )
	                )
	    ),
	    'product.pos' => array(
	        'exclude' => 0,
	        'label' => 'Position',
	        'config' => array(
	            'type' => 'input',
	            'size' => '255',
	        ),
	        'external' => array(
	            'base' => array(
	                'xpath' => './Values/Value[@AttributeID="DisplaySequence"]'
	            )
	        )
	    ),
	    'product.media' => array(
	                'exclude' => 0,
	                'label' => 'Media',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => '.',
	                                'xmlValue' => true,
	                                'userFunc' => array(
	                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                        'method' => 'processAttributes',
	                                        'params' => array(
	                                                'xpath' => 'AssetCrossReference[not(MetaData)]|AssetCrossReference/MetaData',
	                                                'attribute' => 'AssetID',
                                                    'value_attribute' => 'Type'
	                                        )
	                                )
	                        )
	                )
	    ),
	),
	'types' => array(
		'0' => array('showitem' => 'product.code')
	),
);
