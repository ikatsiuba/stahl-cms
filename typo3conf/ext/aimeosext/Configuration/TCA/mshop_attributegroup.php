<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

return array(
	'ctrl' => array(
		'title' => 'AttributeGroupList',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY name',
        'hideTable' =>true,
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('aimeosext') . 'Resources/Public/Images/product.png',
		'external' => array(
			'base' => array(
				'connector' => 'feed',
				'data' => 'xml',
				'nodetype' => 'AttributeGroup',
				'reference_uid' => 'code',
				'priority' => 4000,
				// NOTE: this would not make sense in a real-life configuration. A separate pid would be used.
				'disabledOperations' => 'delete',
				'description' => 'AttributeGroup',
			    'manager' => 'AttributeGroup', 
			),
		),
       'searchFields' => 'title'
	),
	'interface' => array(
		'showRecordFieldList' => 'code,label'
	),
	'columns' => array(
		'attributegroup.siteid' => array(
			'exclude' => 0,
			'label' => 'SiteId',
			'config' => array(
				'type' => 'input',
				'size' => '1',
				'eval' => 'required',
			),
			'external' => array(
				'base' => array(
					'field' => 'siteid',
					'value' => 1
				)
			)
		),
		'attributegroup.code' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '255',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => '.',
					'attribute' => 'ID'
				)
			)
		),
		'attributegroup.label' => array(
			'exclude' => 0,
			'label' => 'Label',
			'config' => array(
				'type' => 'input',
				'size' => '255',
			),
			'external' => array(
				'base' => array(
					'xpath' => './Name',
				)
			)
		),
	    'attributegroup.parent_id' => array(
	                'exclude' => 0,
	                'label' => 'Label',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => '(parent::AttributeGroup)[1]',
	                                'attribute' => 'ID'
	                        )
	                )
	    ),
	    'attributegroup.attribute' => array(
	                'exclude' => 0,
	                'label' => 'Attribute',
	                'config' => array(
	                        'type' => 'input',
	                        'size' => '255',
	                ),
	                'external' => array(
	                        'base' => array(
	                                'xpath' => './MetaData',
	                                'xmlValue' => true,
	                                'userFunc' => array(
	                                        'class' => 'Aimeos\Aimeosext\Import\UserFunction\Transformation',
	                                        'method' => 'processAttributes',
	                                        'params' => array(
	                                                'xpath' => 'Value[@AttributeID]|MultiValue[@AttributeID]/ValueGroup|ValueGroup[@AttributeID]|MultiValue[not(ValueGroup)]',
	                                                'attribute' => 'AttributeID',
	                                        )
	                                )
	                        )
	                )
	        ),
	),
	'types' => array(
		'0' => array('showitem' => 'code,label')
	),
);
