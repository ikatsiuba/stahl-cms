<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

return array(
	'ctrl' => array(
		'title' => 'DeleteAttributes',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY name',
        'hideTable' =>true,
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('aimeosext') . 'Resources/Public/Images/product.png',
		'external' => array(
			'base' => array(
				'connector' => 'feed',
				'data' => 'xml',
				'nodetype' => 'DeleteAttributes',
				'reference_uid' => 'code',
				'priority' => 4150,
				// NOTE: this would not make sense in a real-life configuration. A separate pid would be used.
				'disabledOperations' => 'delete',
				'description' => 'DeleteAttributes',
			    'manager' => 'DeleteAttributes', 
			),
		),
       'searchFields' => 'title'
	),
	'interface' => array(
		'showRecordFieldList' => 'code,label'
	),
	'columns' => array(
		'attribute_group.siteid' => array(
			'exclude' => 0,
			'label' => 'SiteId',
			'config' => array(
				'type' => 'input',
				'size' => '1',
				'eval' => 'required',
			),
			'external' => array(
				'base' => array(
					'field' => 'siteid',
					'value' => 1
				)
			)
		),
		'attribute_group.code' => array(
			'exclude' => 0,
			'label' => 'Code',
			'config' => array(
				'type' => 'input',
				'size' => '255',
				'eval' => 'required,trim',
			),
			'external' => array(
				'base' => array(
					'xpath' => './DeleteAttributeOrGroup',
					'attribute' => 'ID'
				)
			)
		),
	),
	'types' => array(
		'0' => array('showitem' => 'code,label')
	),
);
