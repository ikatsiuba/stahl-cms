/**
 * Created by janschroeder on 07.04.16.
 */


var CaptchaCallback = function () {

    $('.g-recaptcha').each(function (index, el) {
        grecaptcha.render(el.attr("id"), {
            'sitekey': $(this).attr("data-sitekey"),
            'callback' : verifyCallback
        });
    });
    
};

var verifyCallback = function(response) {

    $('.g-recaptcha').each(function (index, el) {
        //copy captcha result to powermail field
        $(el).next().val($(el).find(".g-recaptcha-response").val())

    });
};