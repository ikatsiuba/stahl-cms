<?php
namespace Havasww\LocationSearch\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ZipcodeRange
 */
class ZipcodeRange extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * from
     *
     * @var int
     */
    protected $from = 0;
    
    /**
     * to
     *
     * @var int
     */
    protected $to = 0;
    
    /**
     * zipcodeFrom
     *
     * @var int
     */
    protected $zipcodeFrom = 0;
    
    /**
     * zipcodeTo
     *
     * @var int
     */
    protected $zipcodeTo = 0;
    
    /**
     * Returns the from
     *
     * @return int $from
     */
    public function getFrom()
    {
        return $this->from;
    }
    
    /**
     * Sets the from
     *
     * @param int $from
     * @return void
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }
    
    /**
     * Returns the to
     *
     * @return int $to
     */
    public function getTo()
    {
        return $this->to;
    }
    
    /**
     * Sets the to
     *
     * @param int $to
     * @return void
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

}