<?php
namespace Havasww\LocationSearch\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ContactPerson
 */
class ContactPerson extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * fax
     *
     * @var string
     */
    protected $fax = '';
    
    /**
     * salutation
     *
     * @var string
     */
    protected $salutation = '';
    
    /**
     * firstName
     *
     * @var string
     */
    protected $firstName = '';
    
    /**
     * lastName
     *
     * @var string
     */
    protected $lastName = '';
    
    /**
     * position
     *
     * @var string
     */
    protected $position = '';
    
    /**
     * description
     *
     * @var string
     */
    protected $description = '';
    
    /**
     * mail
     *
     * @var string
     */
    protected $mail = '';
    
    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';
    
    /**
     * mobile
     *
     * @var string
     */
    protected $mobile = '';
    
    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image = '';
    
    /**
     * foreignfolder
     *
     * @var string
     */
    protected $foreignfolder = '';
    
    /**
     * foreignimage
     *
     * @var string
     */
    protected $foreignimage = '';
    
    /**
     * Nr out of tabplzndl
     *
     * @var int
     */
    protected $foreignid = 0;
    
    /**
     * User for Ordering, equivalent to tabplzndl nr
     *
     * @var int
     */
    protected $number = 0;
    
    /**
     * location
     *
     * @var \Havasww\LocationSearch\Domain\Model\Location
     */
    protected $location = null;
    
    /**
     * zipcodeRange
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Havasww\LocationSearch\Domain\Model\ZipcodeRange>
     */
    protected $zipcodeRange = null;
    
    /**
     * Returns the firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Sets the firstName
     *
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }
    
    /**
     * Returns the lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }
    
    /**
     * Sets the lastName
     *
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
	
	 /**
     * Returns the position
     *
     * @return string $position
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    /**
     * Sets the position
     *
     * @param string $position
     * @return void
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }
    
    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * Returns the mail
     *
     * @return string $mail
     */
    public function getMail()
    {
        return $this->mail;
    }
    
    /**
     * Sets the mail
     *
     * @param string $mail
     * @return void
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }
    
    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
    /**
     * Returns the fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }
    
    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }
    
    /**
     * Returns the location
     *
     * @return \Havasww\LocationSearch\Domain\Model\Location location
     */
    public function getLocation()
    {
        return $this->location;
    }
    
    /**
     * Sets the location
     *
     * @param \Havasww\LocationSearch\Domain\Model\Location $location
     * @return void
     */
    public function setLocation(\Havasww\LocationSearch\Domain\Model\Location $location)
    {
        $this->location = $location;
    }
    
    /**
     * Returns the zipcodeRange
     *
     * @return \Havasww\LocationSearch\Domain\Model\ZipcodeRange zipcodeRange
     */
    public function getZipcodeRange()
    {
        return $this->zipcodeRange;
    }
    
    /**
     * Sets the zipcodeRange
     *
     * @param \Havasww\LocationSearch\Domain\Model\ZipcodeRange $zipcodeRange
     * @return void
     */
    public function setZipcodeRange(\Havasww\LocationSearch\Domain\Model\ZipcodeRange $zipcodeRange)
    {
        $this->zipcodeRange = $zipcodeRange;
    }
    
    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference image
     */
    public function getImage()
    {
        return $this->image;
    }
    
    /**
     * Sets the image
     *
     * @param string $image
     * @return void
     */
    public function setImage($image)
    {
        $this->image = $image;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->zipcodeRange = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

}