<?php
namespace Havasww\LocationSearch\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Location
 */
class Location extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';
    
    /**
     * address1
     *
     * @var string
     */
    protected $address1 = '';
    
    /**
     * address2
     *
     * @var string
     */
    protected $address2 = '';
    
    /**
     * zipcode
     *
     * @var string
     */
    protected $zipcode = '';
    
    /**
     * city
     *
     * @var string
     */
    protected $city = '';
    
    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';
    
    /**
     * mobile
     *
     * @var string
     */
    protected $mobile = '';
    
    /**
     * fax
     *
     * @var string
     */
    protected $fax = '';
    
    /**
     * mail
     *
     * @var string
     */
    protected $mail = '';
    
    /**
     * website
     *
     * @var string
     */
    protected $website = '';
    
    /**
     * Untergruppe
     *
     * @var string
     */
    protected $subgroup = '';
    
    /**
     * Postfach
     *
     * @var string
     */
    protected $pobox = '';
    
    /**
     * Postfach PLZ
     *
     * @var string
     */
    protected $poboxzip = '';
    
    /**
     * worldwide
     *
     * @var bool
     */
    protected $worldwide = false;
    
    /**
     * europe
     *
     * @var bool
     */
    protected $europe = false;
    
    /**
     * germany
     *
     * @var bool
     */
    protected $germany = false;
    
    /**
     * Wegbeschreibung
     *
     * @var string
     */
    protected $directions = '';
    
    /**
     * language
     *
     * @var string
     */
    protected $language = '';
    
    /**
     * additional
     *
     * @var string
     */
    protected $additional = '';
    
    /**
     * Foreign key from old Table
     *
     * @var int
     */
    protected $tabAdrKurzbez = 0;
    
    /**
     * country
     *
     * @var \Havasww\LocationSearch\Domain\Model\Country
     */
    protected $country = null;
    
    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Returns the address1
     *
     * @return string $address1
     */
    public function getAddress1()
    {
        return $this->address1;
    }
    
    /**
     * Sets the address1
     *
     * @param string $address1
     * @return void
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }
    
    /**
     * Returns the address2
     *
     * @return string $address2
     */
    public function getAddress2()
    {
        return $this->address2;
    }
    
    /**
     * Sets the address2
     *
     * @param string $address2
     * @return void
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }
    
    /**
     * Returns the zipcode
     *
     * @return string $zipcode
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }
    
    /**
     * Sets the zipcode
     *
     * @param string $zipcode
     * @return void
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }
    
    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }
    
    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
    
    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
    /**
     * Returns the fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }
    
    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }
    
    /**
     * Returns the mail
     *
     * @return string $mail
     */
    public function getMail()
    {
        return $this->mail;
    }
    
    /**
     * Sets the mail
     *
     * @param string $mail
     * @return void
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }
    
    /**
     * Returns the website
     *
     * @return string $website
     */
    public function getWebsite()
    {
        return $this->website;
    }
    
    /**
     * Sets the website
     *
     * @param string $website
     * @return void
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        
    }
    
    /**
     * Returns the country
     *
     * @return \Havasww\LocationSearch\Domain\Model\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Sets the country
     *
     * @param \Havasww\LocationSearch\Domain\Model\Country $country
     * @return void
     */
    public function setCountry(\Havasww\LocationSearch\Domain\Model\Country $country)
    {
        $this->country = $country;
    }

}