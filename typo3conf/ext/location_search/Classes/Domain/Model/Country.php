<?php
namespace Havasww\LocationSearch\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Country
 */
class Country extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';
    
    /**
     * description
     *
     * @var string
     */
    protected $description = '';
    
    /**
     * languageCode
     *
     * @var string
     */
    protected $languageCode = '';
    
    /**
     * region
     *
     * @var \Havasww\LocationSearch\Domain\Model\Region
     * @lazy
     */
    protected $region = null;
    
    /**
     * defaultContact
     *
     * @var \Havasww\LocationSearch\Domain\Model\ContactPerson
     */
    protected $defaultContact = null;
    
	/**
     * locationNumber
     *
     * @var int
     */
    protected $locationNumber = 0;
	
    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * Returns the languageCode
     *
     * @return string $languageCode
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }
    
    /**
     * Sets the languageCode
     *
     * @param string $languageCode
     * @return void
     */
    public function setLanguageCode($languageCode)
    {
        $this->languageCode = $languageCode;
    }
    
    /**
     * Returns the defaultContact
     *
     * @return \Havasww\LocationSearch\Domain\Model\ContactPerson defaultContact
     */
    public function getDefaultContact()
    {
        return $this->defaultContact;
    }
    
    /**
     * Sets the defaultContact
     *
     * @param \Havasww\LocationSearch\Domain\Model\ContactPerson $defaultContact
     * @return void
     */
    public function setDefaultContact(\Havasww\LocationSearch\Domain\Model\ContactPerson $defaultContact)
    {
        $this->defaultContact = $defaultContact;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        
    }
    
    /**
     * Returns the region
     *
     * @return \Havasww\LocationSearch\Domain\Model\Region $region
     */
    public function getRegion()
    {
        return $this->region;
    }
    
    /**
     * Sets the region
     *
     * @param \Havasww\LocationSearch\Domain\Model\Region $region
     * @return void
     */
    public function setRegion(\Havasww\LocationSearch\Domain\Model\Region $region)
    {
        $this->region = $region;
    }
	
	/**
     * Returns the locationNumber
     *
     * @return int $locationNumber
     */
    public function getLocationNumber()
    {
        return $this->locationNumber;
    }
    
    /**
     * Sets the LocationNumber
     *
     * @param int $locationNumber
     * @return void
     */
    public function setLocationNumber($locationNumber)
    {
        $this->locationNumber = $locationNumber;
    }


}