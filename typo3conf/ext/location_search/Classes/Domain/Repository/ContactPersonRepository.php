<?php
namespace Havasww\LocationSearch\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for ContactPersons
 */
class ContactPersonRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
		// Order by BE sorting
	protected $defaultOrderings = array(
		'number' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
	);

	/**
     * @param $search_namezipcodecity
     */
    public function search($search_namezipcodecity)
    {
        $query = $this->createQuery();
		
        if (is_numeric($search_namezipcodecity)) {
			while(strlen($search_namezipcodecity)<5){
				$search_namezipcodecity .= "0";
			}
            $query->statement('SELECT contactperson.* FROM tx_locationsearch_domain_model_contactperson as contactperson '
					. 'JOIN tx_locationsearch_contactperson_zipcoderange_mm as contact_zipcode_mm ON contactperson.uid =  contact_zipcode_mm.uid_local '
					. 'JOIN tx_locationsearch_domain_model_zipcoderange as zipcoderange ON contact_zipcode_mm.uid_foreign = zipcoderange.uid '
					. 'WHERE contactperson.hidden=0 AND contactperson.deleted = 0 AND zipcoderange.zipcode_from <= ' . $search_namezipcodecity . ' AND zipcoderange.zipcode_to >= ' . $search_namezipcodecity
					. ' GROUP BY contactperson.uid' 
					. ' ORDER BY `contactperson`.`number` ASC');
            $locations = $query->execute();
        } else {
			
            $query->statement('SELECT * FROM tx_locationsearch_domain_model_contactperson');
			$locations = $query->execute();
        }
        return $locations;
    }
	
	 public function searchdesc($search_namezipcodecity)
    {
        $query = $this->createQuery();
		
        if (is_numeric($search_namezipcodecity)) {
			while(strlen($search_namezipcodecity)<5){
				$search_namezipcodecity .= "0";
			}
            $query->statement('SELECT contactperson.* FROM tx_locationsearch_domain_model_contactperson as contactperson '
					. 'JOIN tx_locationsearch_contactperson_zipcoderange_mm as contact_zipcode_mm ON contactperson.uid =  contact_zipcode_mm.uid_local '
					. 'JOIN tx_locationsearch_domain_model_zipcoderange as zipcoderange ON contact_zipcode_mm.uid_foreign = zipcoderange.uid '
					. 'WHERE contactperson.position = \'Innendienst\' AND contactperson.hidden=0 AND contactperson.deleted = 0 AND zipcoderange.zipcode_from <= ' . $search_namezipcodecity . ' AND zipcoderange.zipcode_to >= ' . $search_namezipcodecity
					. ' GROUP BY contactperson.uid' 
					. ' ORDER BY `contactperson`.`number` DESC');
            $locations = $query->execute();
        } else {
			
            $query->statement('SELECT * FROM tx_locationsearch_domain_model_contactperson');
			$locations = $query->execute();
        }
        return $locations;
    }
	
}