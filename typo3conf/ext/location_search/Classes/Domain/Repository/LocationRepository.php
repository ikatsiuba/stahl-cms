<?php
namespace Havasww\LocationSearch\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Locations
 */
class LocationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

	/**
     * @param $search
     */
	public function search($search,$country_id){
		$query = $this->createQuery();
		if(is_numeric($search)){
			$query->matching(
				$query->logicalAnd(
					$query->like('zipcode', $search."%"),
					$query->equals('country', $country_id)
				)
			);
			$temp = $search;
			$locations = $query->execute();
			
			if(strlen($temp) > 1){
				for($i = strlen($temp); $i > 0 && $query->count()===0; $i--){
					$temp = substr($temp, 0, $i);
					$query = $this->createQuery();
					$query->matching(
						$query->logicalAnd(
							$query->like('zipcode', $temp."%"),
							$query->equals('country', $country_id)
						)
					);
					$locations = $query->execute();
				}
			}
			if($locations->count() === 0){
				$temp = substr($search, 0, 1);
				$search_bottom = ($temp-1).substr($search, 1, strlen($search)-1);
				$search_top = ($temp+1).substr($search, 1, strlen($search)-1);
				$locations = $this->searchNearest($search_bottom, $search_top,$country_id);
			}
		}else{
			$query->matching($query->like('city', "%".$search."%"));
			$locations = $query->execute();
		}
		
		return $locations;
	}
	
	/**
     * @param $search
     */
	private function searchNearest($search_bottom, $search_top,$country_id){
		if($search_bottom<10000 && $search_top>99999){
			return $this->findByCountry($country_id);
		}
		
		$query = $this->createQuery();
		$query->matching(
			$query->logicalAnd(
				$query->logicalOr(
					$query->like('zipcode', $search_bottom."%"),
					$query->like('zipcode', $search_top."%")
				),
				$query->equals('country', $country_id)
			)
		);
		
		$locations = $query->execute();

		if(strlen($search_bottom) > 1){
			for($i = strlen($search_bottom); $i > 0 && $query->count()===0; $i--){
				$temp_bottom = substr($search_bottom, 0, $i);
				$temp_top = substr($search_bottom, 0, $i);
				$query->matching(
					$query->logicalAnd(
						$query->logicalOr(
							$query->like('zipcode', $temp_bottom."%"),
							$query->like('zipcode', $temp_top."%")
						),
						$query->equals('country', $country_id)
					)
				);
				$locations = $query->execute();
			}
		}
		if($locations->count() === 0){
			
			$temp_bottom = substr($search_bottom, 0, 1);
			$temp_top = substr($search_top, 0, 1);
			$search_bottom = ($temp_bottom-1).substr($search_bottom, 1, strlen($search_bottom)-1);
			$search_top = ($temp_top+1).substr($search_top, 1, strlen($search_top)-1);
			return $this->searchNearest($search_bottom, $search_top,$country_id);
		}else{
			return $locations;
		}
	}
}