<?php
namespace Havasww\LocationSearch\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Countries
 */
class CountryRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
	// Order by BE sorting
	protected $defaultOrderings = array(
		'name' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
	);
	
	public function getZipcodesForCity($city){
		$result = $GLOBALS["TYPO3_DB"]->exec_SELECT("SELECT opengeodb_zipcode.* FROM opengeodb_city JOIN opengeodb_zipcode ON opengeodb_city.id=opengeodb_zipcode.city_id WHERE opengeodb_city.name LIKE '%".$city."%'");
		/*$query = $this->createQuery();
		$query->statement("SELECT opengeodb_zipcode.* FROM opengeodb_city JOIN opengeodb_zipcode ON opengeodb_city.id=opengeodb_zipcode.city_id WHERE opengeodb_city.name LIKE '%".$city."%'");
		$zipcodes = $query->execute();*/
		return $result;	
	}
}