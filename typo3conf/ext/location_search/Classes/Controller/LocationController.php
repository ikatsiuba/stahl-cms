<?php
namespace Havasww\LocationSearch\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * LocationController
 */
class LocationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * locationRepository
     *
     * @var \Havasww\LocationSearch\Domain\Repository\LocationRepository
     * @inject
     */
    protected $locationRepository = NULL;
	
	/**
     * countryRepository
     *
     * @var \Havasww\LocationSearch\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $locations = $this->locationRepository->findAll();
        $this->view->assign('locations', $locations);
    }

	/**
     * action ajaxGetLocationsForCountry
	 * 
	 * @return void
     */
    public function ajaxGetLocationsForCountryAction()
    {

		$filter = "";
        if($this->request->hasArgument("country_uid")){
			$filter = $this->request->getArgument("country_uid");
			$locations = $this->locationRepository->findByCountry($this->request->getArgument("country_uid"));
		}else if($this->request->hasArgument("search")){
			$language_code = $GLOBALS['TSFE']->config['config']['language'];
			$countries = $this->countryRepository->findByLanguageCode($language_code);
			$search = $this->request->getArgument("search");
			$country_uid = $countries->getFirst()->getUid();

			$locations = $this->locationRepository->search($search,$country_uid);
			
			/*if($locations->count() === 0){
				$language_code = $GLOBALS['TSFE']->config['config']['language'];
				$countries = $this->countryRepository->findByLanguageCode($language_code);
				$country_uid = $countries->getFirst()->getUid();
				$locations = $this->locationRepository->findByCountry($country_uid);
			}*/
		}else{
			$locations = $this->locationRepository->findAll();
		}

		$this->view->assign("locations",$locations);
    }
}