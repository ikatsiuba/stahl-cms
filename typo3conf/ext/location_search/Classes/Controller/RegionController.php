<?php
namespace Havasww\LocationSearch\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * RegionController
 */
class RegionController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * regionRepository
     *
     * @var \Havasww\LocationSearch\Domain\Repository\RegionRepository
     * @inject
     */
    protected $regionRepository = NULL;
    
    /**
     * countryRepository
     *
     * @var \Havasww\LocationSearch\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository = NULL;
    
    /**
     * action locations
     *
     * @return void
     */
    public function locationsAction()
    {		
	
		$geolocation = $this->getLocationInfoByIp();
		$search_preset = "";
		if(isset($geolocation["city"])){
			$search_preset = $geolocation["city"];
		}
		$this->view->assign('search_preset', $search_preset);
        $regions = $this->regionRepository->findAll();
		
		
        $this->view->assign('regions', $regions);
    }
	
	private function getLocationInfoByIp(){
		try{
			$geoip_response = file_get_contents("http://geoip.nekudo.com/api/".$_SERVER['REMOTE_ADDR']."/de/city");
		}catch(Exception $e){
			$geoip_response = "";
		}
		return json_decode($geoip_response,true);
	}
	/**
     * Gets Geocode Coordinates for given Adress
	 * 
	 * @param string $address
     *
     * @return object $coordinates
     */
	private function geocode($address){
 
    // url encode the address
    $address = urlencode($address);
     
    // google map geocode api url
    $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";
 
    // get the json response
    $resp_json = file_get_contents($url);
     
    // decode the json
    $resp = json_decode($resp_json, true);
 
    // response status will be 'OK', if able to geocode given address 
    if($resp['status']=='OK'){
		print_r($resp);
		exit;
        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];
         
        // verify if data is complete
        if($lati && $longi && $formatted_address){
         
            // put the data in the array
            $data_arr = array();            
             
            array_push(
                $data_arr, 
                    $lati, 
                    $longi, 
                    $formatted_address
                );
             
            return $data_arr;
             
        }else{
            return false;
        }
         
    }else{
        return false;
    }
}
}