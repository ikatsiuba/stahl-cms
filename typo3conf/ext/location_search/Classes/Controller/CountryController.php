<?php
namespace Havasww\LocationSearch\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * CountryController
 */
class CountryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * countryRepository
     *
     * @var \Havasww\LocationSearch\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository = NULL;
	
	/**
     * contactPersonRepository
     *
     * @var \Havasww\LocationSearch\Domain\Repository\ContactPersonRepository
     * @inject
     */
    protected $contactPersonRepository = NULL;
	
	/**
     * locationRepository
     *
     * @var \Havasww\LocationSearch\Domain\Repository\LocationRepository
     * @inject
     */
    protected $locationRepository = NULL;
    
    /**
     * action showContactPerson
     *
     * @return void
     */
    public function showContactPersonAction()
    {
		if($this->settings["contact"]){
			$contact_person = $this->contactPersonRepository->findByUid($this->settings["contact"]);
			$this->view->assign("contactPerson",$contact_person);
		}else{
			$geolocation = $this->getLocationInfoByIp();
			if(isset($geolocation["city"])){
				//$zipcodes = $this->countryRepository->getZipcodesForCity($geolocation["city"]);
				$result = $GLOBALS["TYPO3_DB"]->exec_SELECTgetSingleRow("opengeodb_zipcode.*","opengeodb_city JOIN opengeodb_zipcode ON opengeodb_city.id=opengeodb_zipcode.city_id","opengeodb_city.name LIKE '%".$geolocation["city"]."%'");
				$contact_persons = $this->contactPersonRepository->searchdesc($result["opengeodb_zipcode"]);

				if(isset($contact_persons[0])){
					$this->view->assign("contactPerson",$contact_persons[0]);
				}else{
					$country = $this->countryRepository->findByUid(1);
					$this->view->assign("contactPerson",$country->getDefaultContact());
				}
			}else{
				$country = $this->countryRepository->findByUid(1);
				$this->view->assign("contactPerson",$country->getDefaultContact());
			}
		}
    }
	
	private function getLocationInfoByIp(){
		try{
			$geoip_response = file_get_contents("http://geoip.nekudo.com/api/".$_SERVER['REMOTE_ADDR']."/de/city");
		}catch(Exception $e){
			$geoip_response = "";
		}
		return json_decode($geoip_response,true);
	}

	/**
     * action ajaxGetCountriesForRegion
	 * 
	 * @return void
     */
    public function ajaxGetCountriesForRegionAction()
    {

		$filter = "";
        if($this->request->hasArgument("region_uid")){
			$filter = $this->request->getArgument("region_uid");
			$countries = $this->countryRepository->findByRegion($this->request->getArgument("region_uid"));
			foreach($countries as $country){
				$country->setLocationNumber($this->locationRepository->findByCountry($country->getUid())->count());
			}
		}else{
			$countries = $this->countryRepository->findAll();
		}

		$this->view->assign("countries",$countries);
    }
}