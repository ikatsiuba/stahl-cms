<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,address1,address2,zipcode,city,phone,mobile,fax,mail,website,subgroup,pobox,poboxzip,worldwide,europe,germany,directions,language,additional,tab_adr_kurzbez,country,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('location_search') . 'Resources/Public/Icons/tx_locationsearch_domain_model_location.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, address1, address2, zipcode, city, phone, mobile, fax, mail, website, subgroup, pobox, poboxzip, worldwide, europe, germany, directions, language, additional, tab_adr_kurzbez, country',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, name, address1, address2, zipcode, city, phone, mobile, fax, mail, website, subgroup, pobox, poboxzip, worldwide, europe, germany, directions, language, additional, tab_adr_kurzbez, country, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_locationsearch_domain_model_location',
				'foreign_table_where' => 'AND tx_locationsearch_domain_model_location.pid=###CURRENT_PID### AND tx_locationsearch_domain_model_location.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'name' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'address1' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.address1',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'address2' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.address2',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'zipcode' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.zipcode',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'city' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'phone' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.phone',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'mobile' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.mobile',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'fax' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.fax',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'mail' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.mail',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'website' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.website',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'subgroup' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.subgroup',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'pobox' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.pobox',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'poboxzip' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.poboxzip',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'worldwide' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.worldwide',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'europe' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.europe',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'germany' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.germany',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'directions' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.directions',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'language' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.language',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'additional' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.additional',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'tab_adr_kurzbez' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.tab_adr_kurzbez',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:location_search/Resources/Private/Language/locallang_db.xlf:tx_locationsearch_domain_model_location.country',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'tx_locationsearch_domain_model_country',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		
	),
);