<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Havasww.' . $_EXTKEY,
	'Locations',
	array(
		'Region' => 'locations',
		'Country' => 'ajaxGetCountriesForRegion',
		'Location' => 'ajaxGetLocationsForCountry',
		'ContactPerson' => 'ajaxGetContactPersonForZipcode'
	),
	// non-cacheable actions
	array(
		'Region' => 'locations',
		'Country' => 'ajaxGetCountriesForRegion',
		'Location' => 'ajaxGetLocationsForCountry',
		'ContactPerson' => 'ajaxGetContactPersonForZipcode'
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Havasww.' . $_EXTKEY,
	'Contactperson',
	array(
		'Country' => 'showContactPerson',
		
	),
	// non-cacheable actions
	array(
		'Country' => 'showContactPerson',
		
	)
);
