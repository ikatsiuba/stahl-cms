<?php

namespace Havasww\LocationSearch\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Havasww\LocationSearch\Domain\Model\Location.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Alexander Boos <alexander.boos@havasww.com>
 */
class LocationTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Havasww\LocationSearch\Domain\Model\Location
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Havasww\LocationSearch\Domain\Model\Location();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getName()
		);
	}

	/**
	 * @test
	 */
	public function setNameForStringSetsName()
	{
		$this->subject->setName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'name',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAddress1ReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAddress1()
		);
	}

	/**
	 * @test
	 */
	public function setAddress1ForStringSetsAddress1()
	{
		$this->subject->setAddress1('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'address1',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAddress2ReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAddress2()
		);
	}

	/**
	 * @test
	 */
	public function setAddress2ForStringSetsAddress2()
	{
		$this->subject->setAddress2('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'address2',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getZipcodeReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getZipcode()
		);
	}

	/**
	 * @test
	 */
	public function setZipcodeForStringSetsZipcode()
	{
		$this->subject->setZipcode('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'zipcode',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCityReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getCity()
		);
	}

	/**
	 * @test
	 */
	public function setCityForStringSetsCity()
	{
		$this->subject->setCity('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'city',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPhoneReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getPhone()
		);
	}

	/**
	 * @test
	 */
	public function setPhoneForStringSetsPhone()
	{
		$this->subject->setPhone('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'phone',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getMobileReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getMobile()
		);
	}

	/**
	 * @test
	 */
	public function setMobileForStringSetsMobile()
	{
		$this->subject->setMobile('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'mobile',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFaxReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getFax()
		);
	}

	/**
	 * @test
	 */
	public function setFaxForStringSetsFax()
	{
		$this->subject->setFax('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'fax',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getMailReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getMail()
		);
	}

	/**
	 * @test
	 */
	public function setMailForStringSetsMail()
	{
		$this->subject->setMail('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'mail',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getWebsiteReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getWebsite()
		);
	}

	/**
	 * @test
	 */
	public function setWebsiteForStringSetsWebsite()
	{
		$this->subject->setWebsite('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'website',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSubgroupReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getSubgroup()
		);
	}

	/**
	 * @test
	 */
	public function setSubgroupForStringSetsSubgroup()
	{
		$this->subject->setSubgroup('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'subgroup',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPoboxReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getPobox()
		);
	}

	/**
	 * @test
	 */
	public function setPoboxForStringSetsPobox()
	{
		$this->subject->setPobox('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'pobox',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPoboxzipReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getPoboxzip()
		);
	}

	/**
	 * @test
	 */
	public function setPoboxzipForStringSetsPoboxzip()
	{
		$this->subject->setPoboxzip('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'poboxzip',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getWorldwideReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getWorldwide()
		);
	}

	/**
	 * @test
	 */
	public function setWorldwideForBoolSetsWorldwide()
	{
		$this->subject->setWorldwide(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'worldwide',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEuropeReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getEurope()
		);
	}

	/**
	 * @test
	 */
	public function setEuropeForBoolSetsEurope()
	{
		$this->subject->setEurope(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'europe',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getGermanyReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getGermany()
		);
	}

	/**
	 * @test
	 */
	public function setGermanyForBoolSetsGermany()
	{
		$this->subject->setGermany(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'germany',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDirectionsReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDirections()
		);
	}

	/**
	 * @test
	 */
	public function setDirectionsForStringSetsDirections()
	{
		$this->subject->setDirections('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'directions',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLanguageReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getLanguage()
		);
	}

	/**
	 * @test
	 */
	public function setLanguageForStringSetsLanguage()
	{
		$this->subject->setLanguage('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'language',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAdditionalReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAdditional()
		);
	}

	/**
	 * @test
	 */
	public function setAdditionalForStringSetsAdditional()
	{
		$this->subject->setAdditional('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'additional',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTabAdrKurzbezReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setTabAdrKurzbezForIntSetsTabAdrKurzbez()
	{	}

	/**
	 * @test
	 */
	public function getCountryReturnsInitialValueForCountry()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getCountry()
		);
	}

	/**
	 * @test
	 */
	public function setCountryForCountrySetsCountry()
	{
		$countryFixture = new \Havasww\LocationSearch\Domain\Model\Country();
		$this->subject->setCountry($countryFixture);

		$this->assertAttributeEquals(
			$countryFixture,
			'country',
			$this->subject
		);
	}
}
