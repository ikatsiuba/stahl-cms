<?php

namespace Havasww\LocationSearch\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Havasww\LocationSearch\Domain\Model\ContactPerson.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Alexander Boos <alexander.boos@havasww.com>
 */
class ContactPersonTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Havasww\LocationSearch\Domain\Model\ContactPerson
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Havasww\LocationSearch\Domain\Model\ContactPerson();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getLastNameReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getLastName()
		);
	}

	/**
	 * @test
	 */
	public function setLastNameForStringSetsLastName()
	{
		$this->subject->setLastName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'lastName',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFirstNameReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getFirstName()
		);
	}

	/**
	 * @test
	 */
	public function setFirstNameForStringSetsFirstName()
	{
		$this->subject->setFirstName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'firstName',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSalutationReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getSalutation()
		);
	}

	/**
	 * @test
	 */
	public function setSalutationForStringSetsSalutation()
	{
		$this->subject->setSalutation('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'salutation',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPositionReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getPosition()
		);
	}

	/**
	 * @test
	 */
	public function setPositionForStringSetsPosition()
	{
		$this->subject->setPosition('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'position',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDescription()
		);
	}

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription()
	{
		$this->subject->setDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'description',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getMailReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getMail()
		);
	}

	/**
	 * @test
	 */
	public function setMailForStringSetsMail()
	{
		$this->subject->setMail('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'mail',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPhoneReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getPhone()
		);
	}

	/**
	 * @test
	 */
	public function setPhoneForStringSetsPhone()
	{
		$this->subject->setPhone('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'phone',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getMobileReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getMobile()
		);
	}

	/**
	 * @test
	 */
	public function setMobileForStringSetsMobile()
	{
		$this->subject->setMobile('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'mobile',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getImage()
		);
	}

	/**
	 * @test
	 */
	public function setImageForFileReferenceSetsImage()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImage($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'image',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getForeignfolderReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getForeignfolder()
		);
	}

	/**
	 * @test
	 */
	public function setForeignfolderForStringSetsForeignfolder()
	{
		$this->subject->setForeignfolder('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'foreignfolder',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getForeignimageReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getForeignimage()
		);
	}

	/**
	 * @test
	 */
	public function setForeignimageForStringSetsForeignimage()
	{
		$this->subject->setForeignimage('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'foreignimage',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getForeignidReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setForeignidForIntSetsForeignid()
	{	}

	/**
	 * @test
	 */
	public function getNumberReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setNumberForIntSetsNumber()
	{	}

	/**
	 * @test
	 */
	public function getLocationReturnsInitialValueForLocation()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getLocation()
		);
	}

	/**
	 * @test
	 */
	public function setLocationForLocationSetsLocation()
	{
		$locationFixture = new \Havasww\LocationSearch\Domain\Model\Location();
		$this->subject->setLocation($locationFixture);

		$this->assertAttributeEquals(
			$locationFixture,
			'location',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getZipcodeRangeReturnsInitialValueForZipcodeRange()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getZipcodeRange()
		);
	}

	/**
	 * @test
	 */
	public function setZipcodeRangeForObjectStorageContainingZipcodeRangeSetsZipcodeRange()
	{
		$zipcodeRange = new \Havasww\LocationSearch\Domain\Model\ZipcodeRange();
		$objectStorageHoldingExactlyOneZipcodeRange = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneZipcodeRange->attach($zipcodeRange);
		$this->subject->setZipcodeRange($objectStorageHoldingExactlyOneZipcodeRange);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneZipcodeRange,
			'zipcodeRange',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addZipcodeRangeToObjectStorageHoldingZipcodeRange()
	{
		$zipcodeRange = new \Havasww\LocationSearch\Domain\Model\ZipcodeRange();
		$zipcodeRangeObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$zipcodeRangeObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($zipcodeRange));
		$this->inject($this->subject, 'zipcodeRange', $zipcodeRangeObjectStorageMock);

		$this->subject->addZipcodeRange($zipcodeRange);
	}

	/**
	 * @test
	 */
	public function removeZipcodeRangeFromObjectStorageHoldingZipcodeRange()
	{
		$zipcodeRange = new \Havasww\LocationSearch\Domain\Model\ZipcodeRange();
		$zipcodeRangeObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$zipcodeRangeObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($zipcodeRange));
		$this->inject($this->subject, 'zipcodeRange', $zipcodeRangeObjectStorageMock);

		$this->subject->removeZipcodeRange($zipcodeRange);

	}
}
