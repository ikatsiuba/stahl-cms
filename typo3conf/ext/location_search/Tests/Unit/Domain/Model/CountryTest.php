<?php

namespace Havasww\LocationSearch\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Alexander Boos <alexander.boos@havasww.com>, HavasWW Düsseldorf
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Havasww\LocationSearch\Domain\Model\Country.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Alexander Boos <alexander.boos@havasww.com>
 */
class CountryTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Havasww\LocationSearch\Domain\Model\Country
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Havasww\LocationSearch\Domain\Model\Country();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getName()
		);
	}

	/**
	 * @test
	 */
	public function setNameForStringSetsName()
	{
		$this->subject->setName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'name',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDescription()
		);
	}

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription()
	{
		$this->subject->setDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'description',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLanguageCodeReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getLanguageCode()
		);
	}

	/**
	 * @test
	 */
	public function setLanguageCodeForStringSetsLanguageCode()
	{
		$this->subject->setLanguageCode('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'languageCode',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRegionReturnsInitialValueForRegion()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getRegion()
		);
	}

	/**
	 * @test
	 */
	public function setRegionForRegionSetsRegion()
	{
		$regionFixture = new \Havasww\LocationSearch\Domain\Model\Region();
		$this->subject->setRegion($regionFixture);

		$this->assertAttributeEquals(
			$regionFixture,
			'region',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDefaultContactReturnsInitialValueForContactPerson()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getDefaultContact()
		);
	}

	/**
	 * @test
	 */
	public function setDefaultContactForContactPersonSetsDefaultContact()
	{
		$defaultContactFixture = new \Havasww\LocationSearch\Domain\Model\ContactPerson();
		$this->subject->setDefaultContact($defaultContactFixture);

		$this->assertAttributeEquals(
			$defaultContactFixture,
			'defaultContact',
			$this->subject
		);
	}
}
