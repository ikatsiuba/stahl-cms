﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
Location Search
=============================================================

.. only:: html

	:Classification:
		location_search

	:Version:
		|release|

	:Language:
		en

	:Description:
		Database of Contact Persons, ordered in structure of regions, countries and locations.
Including Zipcode and City Search and two different FE Layouts.

	:Keywords:
		comma,separated,list,of,keywords

	:Copyright:
		2016

	:Author:
		Alexander Boos

	:Email:
		alexander.boos@havasww.com

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Links
