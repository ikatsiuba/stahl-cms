var listener;
var top_search = false;

$(document).ready(function() {
	//Wait for HTTPS
	//getLocation();
	$("#location_search_close").click(function(){
		if($(window).width() > 990)
		{
			$(".tx_locationsearch_locations").slideUp();
		}
		else{
			$(".tx_locationsearch_locations").toggleClass("mobile_slide");
		}
		//clear_locations();
	});
	
	$(".tx-location-search .tab_navigation li").click(function(){
		clear_locations("showCountry");
		var action = $(this).find(".get_countries").attr("href"); 
		$.ajax({
			url: action,
			type: "post",
			beforeSend: function(){
				$("#locations_loading").show();	
			}
		}).done(function (data){
			$(".tx_locationsearch_locations .tab_slider").show();
			initCountryContainer(data);
		});
	});
	
	//FIRST INIT
	$(".tx_locationsearch_open").one("click",function(){
		$(".tx-location-search .tab_navigation li").eq(0).trigger("click");
		window.setTimeout(function(){
			$(".tab_slider").width($(".tx-location-search .tab_navigation li").eq(0).width());
		},500);
	});
	
	$("#get_locations_top").click(getLocationsTop);
	$("#location_top").pressEnter(function(){$("#get_locations_top").trigger("click");});
	
	$("#send_plz_top").click(function(){
		var action = $(this).attr("href");
		action += "&tx_locationsearch_locations[search]="+$("#zipcode_top").val();
		$.ajax({
			url: action,
			type: "post",
			beforeSend: function(){
				$("#locations_loading").show();
			}
		}).done(function (data){
			//Hide To init Carousel
			$("#tx_locationsearch_locations_contactpersons").css("position","absolute");
			$("#tx_locationsearch_locations_contactpersons").css("left","-10000px");
			$("#tx_locationsearch_locations_contactpersons").css("width","100%");
			$("#tx_locationsearch_locations_contactpersons").show();
			$("#tx_locationsearch_locations_contactpersons").html(data);
			$("#tx_locationsearch_locations_contactpersons .location_carousel").owlCarousel({
				margin: 0,
				loop: false,
				autoWidth: false,
				nav: true,
				navText: false,
				dots: true,
				responsive: {
				0: {
					items: 1,
				},
				991: {
					items: 2,
				}
		}
			});
			$("#tx_locationsearch_locations_contactpersons").css("position","static");
			$("#tx_locationsearch_locations_contactpersons").css("left","auto");
			$("#tx_locationsearch_locations_contactpersons").hide();
			$("#locations_loading").hide();
			$("#tx_locationsearch_locations_contactpersons").slideDown("fast");
			load_location_plz($("#zipcode_top").val());
		});
	});
	$("#zipcode_top").pressEnter(function(){$("#send_plz_top").trigger("click");});
	
	$(".tx_locationsearch_locations .tx_locationsearch_back_button").click(locationsearch_back);
	
	/* SHOW CONTACT PARTNER */
	$(".layout_sticky_content .tx_locationsearch_direct_contact .show_callback_form").addClass("sticky_form");
	$(".tx_locationsearch_direct_contact .show_callback_form").click(function(){
		if($(this).hasClass("sticky_form")){
			$(this).parents(".tx_locationsearch_direct_contact").find(".kontakt_wrapper").slideUp();
			$(this).parents(".layout_sticky_content").find(".layout_sticky_content_close").hide();
			$(this).parents(".tx_locationsearch_direct_contact").find(".tx_locationsearch_back_button").show();
			$(this).parents(".tx_locationsearch_direct_contact").find(".tx_locationsearch_callback_form").slideDown();
			console.log($(this).parents(".tx-location-search").find(".email").val());
			$("#powermail_field_receiver_mail").val($(this).parents(".tx-location-search").find(".email").html());
		}else{
			$(this).parents(".row").find(".location_content_more").slideToggle(function(){
				scrollto(".location_content_more");
			}); 
			console.log($(this).parents(".tx-location-search").find(".email").html());
			$("#powermail_field_receiver_mail").val($(this).parents(".tx-location-search").find(".email").html()); 
		}
	});
	
	$(".location_content_more .tx_locationsearch_back_button").click(function(){
		$(this).parents(".location_content_more").slideUp(function(){
			scrollto(".location_content_person");
		}); 
	});
	
	$(".tx_locationsearch_direct_contact .tx_locationsearch_back_button").click(function(){
		$(this).parents(".tx_locationsearch_direct_contact").find(".tx_locationsearch_callback_form").slideUp();
		$(this).parents(".tx_locationsearch_direct_contact").find(".tx_locationsearch_back_button").hide();
		$(this).parents(".layout_sticky_content").find(".layout_sticky_content_close").show();
		$(this).parents(".tx_locationsearch_direct_contact").find(".kontakt_wrapper").slideDown();
	});
});

function getLocationsTop(){
	if(!$(this).hasClass("active")){
		$(this).addClass("active");
		$("#location_top").toggleClass("open");
	}else if($("#location_top").val() === ""){
		$(this).removeClass("active");
		$("#location_top").toggleClass("open");
	}else{
		if($("#tx_locationsearch_locations_sub_container .active_container").length === 0){
		top_search = true;
	}
		$("#tx_locationsearch_locations_sub_container .active_container").slideUp();
		
		$("#tx_locationsearch_locations_sub_container").slideUp(function(){
			load_location_plz($("#location_top").val(),function(){
				$("#tx_locationsearch_locations_sub_container").slideDown("fast",function(){
					switchToContainer("#tx_locationsearch_locations_locationlist");
				});
				$("#country_top").val("TEST");
			});
		});
	}
}

function load_location_plz(val, callback){
	
	$(".tx_locationsearch_locations .tx_locationsearch_back_button").hide();

	var action = $("#get_locations_top").attr("href");
	action += "&tx_locationsearch_locations[search]="+val;
	$.ajax({
		url: action,
		type: "post",
		beforeSend: function(){
			$("#locations_loading").show();
		}
	}).done(function (data){
		$(".tx_locationsearch_locations .tx_locationsearch_back_button").show();
		$("#tx_locationsearch_locations_locationlist").html(data);

		//Hide To init Carousel
		$("#tx_locationsearch_locations_locationlist").css("position","absolute");
		$("#tx_locationsearch_locations_locationlist").css("left","-10000px");
		$("#tx_locationsearch_locations_locationlist").show();

		$("#tx_locationsearch_locations_locationlist .location_carousel").owlCarousel({
			margin: 0,
			loop: false,
			autoWidth: false,
			nav: true,
			navText: false,
			dots: true,
			responsive: {
				0: {
					items: 1,
				},
				768: {
					items: 2,
				},
				991: {
					items: 3,
				}
			}
		});
		$("#tx_locationsearch_locations_locationlist").css("position","static");
		$("#tx_locationsearch_locations_locationlist").css("left","auto");
		$("#tx_locationsearch_locations_locationlist").hide();
		if(typeof listener === "undefined"){
			listener = $("#show_country_locations").click(function(){
				switchToContainer("#tx_locationsearch_locations_locationlist");
			});
		}
		$("#locations_loading").hide();
		if(typeof callback == "function"){
			callback();
		}
	});
}

function initCountryContainer(data){
	$("#tx_locationsearch_locations_country_container").html(data);
	$("#locations_loading").hide();
	$("#tx_locationsearch_locations_country_container").show();
	$("#tx_locationsearch_locations_sub_container").slideDown("fast",function(){
		if($(window).width()>=768){
			var height = $("#tx_locationsearch_locations_country_container").outerHeight();
			if(height>0){
				$("#tx_locationsearch_locations_locationsearch").height(height);
			}else{
				setTimeout(function(){
					var height = $("#tx_locationsearch_locations_country_container").outerHeight();
					$("#tx_locationsearch_locations_locationsearch").height(height);
				},500);
			}
		}
		$("#tx_locationsearch_locations_country_container").addClass("active_container");
	});
	
	$("#tx_locationsearch_locations_country_container .location_carousel").owlCarousel({
		margin: 0,
		loop: false,
		autoWidth: false,
		nav: true,
		navText: false,
		dots: true,
		responsive: {
				0: {
					items: 1,
				},
				768: {
					items: 3,
				},
				991: {
					items: 4,
				}
		}
	});
	$("#tx_locationsearch_locations_country_container a").click(function(){
		var action = $(this).attr("href"); 
		var location = $(this).find("h4").html();
		$.ajax({
			url: action,
			type: "post",
			beforeSend: function(){
				$("#locations_loading").show();
			}
		}).done(function (data){
			$(".tx_locationsearch_locations .tx_locationsearch_back_button").show();
			$("#tx_locationsearch_locations_locationlist").html(data);
			
			//Hide To init Carousel
			$("#tx_locationsearch_locations_locationlist").css("position","absolute");
			$("#tx_locationsearch_locations_locationlist").css("left","-10000px");
			$("#tx_locationsearch_locations_locationlist").show();
			
			$("#tx_locationsearch_locations_locationlist .location_carousel").owlCarousel({
				margin: 0,
				loop: false,
				autoWidth: false,
				nav: true,
				navText: false,
				dots: true,
				responsive: {
				0: {
					items: 1,
				},
				768: {
					items: 2,
				},
				991: {
					items: 3,
				}
			}
			});
			$("#tx_locationsearch_locations_locationlist").css("position","static");
			$("#tx_locationsearch_locations_locationlist").css("left","auto");
			var dot_count = $("#tx_locationsearch_locations_locationlist .owl-dot").length;
			var dot_nav_width = dot_count*5;
			var right_next = $("#tx_locationsearch_locations_locationlist .owl-next").css("right").replace("px","");
			var offset = (right_next-dot_nav_width/2)+"px";
			
			$("#tx_locationsearch_locations_locationlist .owl-prev").css("left",offset);
			$("#tx_locationsearch_locations_locationlist .owl-next").css("right",offset);
			$("#tx_locationsearch_locations_locationlist").hide();
			if(typeof listener === "undefined"){
				listener = $("#show_country_locations").click(function(){
					switchToContainer("#tx_locationsearch_locations_locationlist");
				});
			}
			
			
			if(location==="Deutschland"){
				$("#locations_loading").hide();
				switchToContainer("#tx_locationsearch_locations_locationsearch");
			}else{
				window.setTimeout(function(){
					$("#locations_loading").hide();
					switchToContainer("#tx_locationsearch_locations_locationlist");
				},500);
				
			}
			$("#country_top").val(location);
		});
	});
}

function switchToContainer(selector, callback){
	$("#tx_locationsearch_locations_sub_container .active_container").slideUp("fast");
	$("#tx_locationsearch_locations_sub_container .active_container").removeClass("active_container");
	$(selector).slideDown("fast");
	$(selector).addClass("active_container");
	if(typeof callback === "function"){
		callback();
	}
}

function locationsearch_back(){
	if(!top_search){
		if($("#tx_locationsearch_locations_locationlist").hasClass("active_container") && $("#country_top").val() == "Deutschland" && false){
			switchToContainer("#tx_locationsearch_locations_locationsearch");
		}else{
			switchToContainer("#tx_locationsearch_locations_country_container");
			$(".tx_locationsearch_locations .tx_locationsearch_back_button").hide();
		}
	}else{
		if($("#tx_locationsearch_locations_country_container").html() !== ""){
			$("#tx_locationsearch_locations_locationlist").slideUp();
			clear_locations();
			$(".tx_locationsearch_locations .tx_locationsearch_back_button").hide();
			top_search = false;
		}else{
			$("#tx_locationsearch_locations_sub_container").slideUp();
			$(".tx_locationsearch_locations .tx_locationsearch_back_button").hide();
		}
	}
	clear_location_forms();
}

function clear_locations(option){
	
	$("#tx_locationsearch_locations_locationlist").slideUp("fast",function(){
		if(typeof option === "undefined" || option !== "showCountry"){
			$("#tx_locationsearch_locations_country_container").hide();
		}
		
		$("#tx_locationsearch_locations_contactpersons").hide();
		$("#tx_locationsearch_locations_locationsearch").hide();
		$("#tx_locationsearch_locations_sub_container .active_container").removeClass(".active_container");
		$(".tx_locationsearch_locations .tab_navigation li").removeClass("active");
		$(".tx_locationsearch_locations .tab_slider").hide();
		$("#tx_locationsearch_locations_locationlist").html("");
		$("#tx_locationsearch_locations_contactpersons").html("");
		
		$(".tx_locationsearch_locations .tx_locationsearch_back_button").hide();
		$("#get_locations_top").removeClass("active");
		$("#location_top").removeClass("open");
		clear_location_forms();
		if(typeof option === "undefined" || option !== "showCountry"){
			$("#tx_locationsearch_locations_sub_container").hide();
		}
	});
}

function clear_location_forms(){
	$("#location_top").val("");
	$("#zipcode_top").val("");
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    console.log(position.coords.latitude+" "+position.coords.longitude);
}
$.fn.pressEnter = function(fn) {

    return this.each(function() {  
        $(this).bind('enterPress', fn);
        $(this).keyup(function(e){
            if(e.keyCode == 13)
            {
              $(this).trigger("enterPress");
            }
        });
    });  
 }; 