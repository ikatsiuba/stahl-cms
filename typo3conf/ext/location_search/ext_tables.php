<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Havasww.' . $_EXTKEY,
	'Locations',
	'Vertriebsstandorte'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Havasww.' . $_EXTKEY,
	'Contactperson',
	'Kontaktperson'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Location Search');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_locationsearch_domain_model_region', 'EXT:location_search/Resources/Private/Language/locallang_csh_tx_locationsearch_domain_model_region.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_locationsearch_domain_model_region');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_locationsearch_domain_model_country', 'EXT:location_search/Resources/Private/Language/locallang_csh_tx_locationsearch_domain_model_country.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_locationsearch_domain_model_country');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_locationsearch_domain_model_location', 'EXT:location_search/Resources/Private/Language/locallang_csh_tx_locationsearch_domain_model_location.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_locationsearch_domain_model_location');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_locationsearch_domain_model_contactperson', 'EXT:location_search/Resources/Private/Language/locallang_csh_tx_locationsearch_domain_model_contactperson.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_locationsearch_domain_model_contactperson');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_locationsearch_domain_model_zipcoderange', 'EXT:location_search/Resources/Private/Language/locallang_csh_tx_locationsearch_domain_model_zipcoderange.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_locationsearch_domain_model_zipcoderange');

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginName = strtolower('Contactperson');
$pluginSignature = $extensionName.'_'.$pluginName; 

$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages'; 
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$_EXTKEY . '/Configuration/FlexForms/Contactperson.xml');