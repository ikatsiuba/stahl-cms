<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "caretaker_instance".
 *
 * Auto generated 14-11-2016 14:27
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Caretaker Instance',
  'description' => 'Client for caretaker observation system',
  'category' => 'misc',
  'author' => 'Martin Ficzel,Thomas Hempel,Christopher Hlubek,Tobias Liebig',
  'author_email' => 'ficzel@work.de,hempel@work.de,hlubek@networkteam.com,typo3@etobi.de',
  'state' => 'stable',
  'uploadfolder' => true,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'author_company' => '',
  'version' => '0.7.7',
  'constraints' => 
  array (
    'depends' => 
    array (
      'cms' => '',
      'typo3' => '6.2.0-7.99.99',
      'php' => '5.3.0-',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'autoload' => 
  array (
    'classmap' => 
    array (
      0 => 'services',
      1 => 'classes',
      2 => 'eid',
    ),
  ),
  '_md5_values_when_last_written' => 'a:5:{s:9:"ChangeLog";s:4:"9c48";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"1bdc";s:19:"doc/wizard_form.dat";s:4:"9f12";s:20:"doc/wizard_form.html";s:4:"9378";}',
  'clearcacheonload' => true,
);

