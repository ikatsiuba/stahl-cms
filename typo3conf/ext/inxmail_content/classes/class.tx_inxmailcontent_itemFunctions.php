<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Inxmail GmbH - http://www.inxmail.de
*  (c) 2010 [di] digitale informationssysteme gmbh - http://www.digi-info.de
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * class.tx_inxmailcontent_itemFunctions.php
 *
 * Provides item functions for the backend.
 *
 * $Id:$
 *
 * @author Jens Jacobsen <jacobsen@digi-info.de>
 */

require_once(t3lib_extMgm::extPath('inxmail_content').'classes/class.tx_inxmailcontent.php');

/**
 * This class provides item functions for the backend.
 *
 * @author  Jens Jacobsen <jacobsen@digi-info.de>
 * @package TYPO3
 * @subpackage tx_inxmailcontent
 */
class tx_inxmailcontent_itemFunctions
{
    /**
     * tx_inxmailcontent object
     *
     * @var     tx_inxmailcontent
     */
    protected $_inxmail;

    /**
     * Constructor
     *
     * @return  void
     */
    public function __construct()
    {
        $this->_inxmail = t3lib_div::makeInstance('tx_inxmailcontent');

        $GLOBALS['LANG']->includeLLFile('EXT:inxmail_content/locallang_db.xml');
    }

    /**
     * gets the available lists from the inxmail system
     *
     * @param   array $config
     * @return  array
     */
    public function getListOptions($config)
    {
        $lists = $this->_inxmail->getLists();

        if (is_array($lists) && count($lists)) {
            foreach ($lists as $list) {
                $config['items'][] = array($list['name'], $list['id']);
            }
        }
        return $config;
    }

    /**
     * gets the text templates
     *
     * @return  string
     */
    public function getSalutations()
    {
        $editor       = t3lib_div::_GP('edit');
        $editorNumber = array_flip($editor['pages']);
        $editorNumber = $editorNumber['edit'];

        $newsletter = array();
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            'tx_inxmailcontent_type',
            'pages',
            'uid = '. (int)$editorNumber
        );

        if ($GLOBALS['TYPO3_DB']->sql_num_rows($res) == 1) {
            $newsletter                           = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
            $newsletter['tx_inxmailcontent_data'] = unserialize($newsletter['tx_inxmailcontent_data']);

            if (!is_array($newsletter['tx_inxmailcontent_data'])) {
                $newsletter['tx_inxmailcontent_data'] = array();
            }

            switch($newsletter['tx_inxmailcontent_type']) {
                case 'html/text':
                    $mimeType = Inx_Api_TextModule_TextModule::MIME_TYPE_MULTIPART;
                    break;
                case 'html';
                    $mimeType = Inx_Api_TextModule_TextModule::MIME_TYPE_HTML_TEXT;
                    break;
                case 'text';
                    $mimeType = Inx_Api_TextModule_TextModule::MIME_TYPE_PLAIN_TEXT;
                    break;
            }

            $texts = $this->_inxmail->getTextModules(0, $mimeType);
        }

        if (is_array($texts) && count($texts)) {
            foreach($texts as $key => $value) {
                $texts[$key] = '<span onclick="text=document.createTextNode(\'' . str_replace("'", "\'", $value) . '\');rte=RTEarea[\'data[pages][' . $editorNumber . '][tx_inxmailcontent_editorial]\'];if(HTMLArea.is_ie){range = rte._createRange(sel);range.pasteHTML(text.outerHTML);}else{rte.editor.insertNodeAtSelection(text);}" style="cursor:pointer;background-color:#FFF;border:1px #ddd solid;padding:4px;" title="' . $GLOBALS['LANG']->getLL('insertSalutation') . '">' . $value . '</span>';
            }
        }
        return '<div style="margin:10px 0;">' . implode(',', $texts) . '</div>';
    }
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/classes/class.tx_inxmailcontent_itemFunctions.php']) {
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/classes/class.tx_inxmailcontent_itemFunctions.php']);
}
?>