<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Inxmail GmbH - http://www.inxmail.de
*  (c) 2010 [di] digitale informationssysteme gmbh - http://www.digi-info.de
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * class.tx_inxmailcore.php
 *
 * Provides extends the api wrapper for the inxmail typo3 extensions.
 *
 * $Id:$
 *
 * @author Jens Jacobsen <jacobsen@digi-info.de>
 */

require_once(t3lib_extMgm::extPath('inxmail_core').'classes/class.tx_inxmailcore.php');

/**
 * This class extends the api wrapper for the inxmail typo3 extensions.
 *
 * @author  Jens Jacobsen <jacobsen@digi-info.de>
 * @package TYPO3
 * @subpackage tx_inxmailcontent
 */
class tx_inxmailcontent extends tx_inxmailcore {

    /**
     * link replacement storage
     *
     * @var array
     */
    private $_linkReplacement = array();

    /**
     * newsletter data
     *
     * @var array
     */
    private $_newsletter = array();

    /**
     * listcontext
     *
     * @var Inx_Api_List_ListContext
     */
    private $_listContext = null;

    /**
     * Inxmail Professional link types
     *
     * @var array
     */
    public static $linkTypes = array('simple', 'unsubscribe', 'count', 'unique-count');

    /**
     * callback method for replacing "a"-tags to inxmail url codes
     *
     * @param   array   $matches matches from the preg_match_callback method
     * @return  string
     */
    private function _aHrefParser($matches, $returnHtml = true) {

        if (!is_array($matches)) {
            return '';
        }
        $matches[2] = trim($matches[2]);

        // do not rewrite mail links
        if (preg_match('~^mailto:~Uis', $matches[2])) {
            return $returnHtml ? $matches[0] : str_replace('mailto:', '', $matches[2]);
        }

        if (!isset($this->_newsletter['tx_inxmailcontent_data']['linktype'][md5($matches[2])]) ||
            empty($this->_newsletter['tx_inxmailcontent_data']['linktype'][md5($matches[2])])) {

            return $returnHtml ? $matches[0] : str_replace('mailto:', '', $matches[2]);
        }

        $type = $this->_newsletter['tx_inxmailcontent_data']['linktype'][md5($matches[2])];

        if (!in_array($type, self::$linkTypes)) {

            return $returnHtml ? $matches[0] : str_replace('mailto:', '', $matches[2]);
        }

        $inxmailTrackingCode = $this->getUrl($type, $matches[2], $matches[4], $matches[4]);
        $matches[2]          = 'href="' . $inxmailTrackingCode . '"';

        // remove the complete match before gluing the rest together
        unset($matches[0]);

        if ($returnHtml) {
            $aTag = implode('', $matches);
            // do some cleanup
            $aTag = preg_replace('~(target|class|rtekeep)\s*=\s*"[^"]*"~Uis', '', $aTag);
            $aTag = preg_replace('~(\s)\s+~', '\1', $aTag);
            return $aTag;
        }
        else {
            return $inxmailTrackingCode;
        }
    }

    /**
     * callback method for replacing "a"-tags to inxmail url codes (text version)
     *
     * @param   array   $matches matches from the preg_match_callback method
     * @return  string
     */
    private function _aHrefParserText($matches) {

        return $this->_aHrefParser($matches, false);
    }

    /**
     * replaces links in the html
     *
     * @param   string  $htmltext
     * @param   boolean $returnHtml
     * @return  string
     */
    private function _trackLinks(&$htmltext, $returnHtml = true) {

        $aTagRegex = '~(<a[^>]*)href\s*=\s*"([^"]*)"([^>]*>)(.*)(<\/a>)~Uis';

        if ($returnHtml) {
            $htmltext = preg_replace_callback($aTagRegex, array($this, "_aHrefParser"), $htmltext);
            $htmltext = $this->_handleResources($htmltext);
        }
        else {
            $htmltext = preg_replace_callback($aTagRegex, array($this, "_aHrefParserText"), $htmltext);
        }
        return $htmltext;
    }


    private function _embedResources(&$htmltext, &$resources) {

        $mediaBackPath   = preg_replace('~[^\./]+~is', '..', t3lib_extMgm::extRelPath('inxmail_content'));
        $embeddedImages  = array();
        $resourceManager = $this->getSession()->getResourceManager();

        foreach($resources as $media) {

            $resource = null;

            if (is_array($this->_newsletter['tx_inxmailcontent_data']['embeddedImages']) &&
                array_key_exists(md5($media), $this->_newsletter['tx_inxmailcontent_data']['embeddedImages'])) {

                $embeddedImages[md5($media)] = $this->_newsletter['tx_inxmailcontent_data']['embeddedImages'][md5($media)];

                try {
                    $resource = $resourceManager->get($this->_newsletter['tx_inxmailcontent_data']['embeddedImages'][md5($media)]['id']);
                }
                catch (Exception $e) {

                }
            }

            if (is_null($resource) && file_exists($mediaBackPath . $media)) {

                $mediaFileName = substr($media, (strrpos($media, '/') + 1));
                $mediaFile = fopen($mediaBackPath . $media, 'rb');
                $resource = $resourceManager->upload(null, $mediaFileName, $mediaFile);
                fclose($mediaFile);

                $embeddedImages[md5($media)] = array(
                    'id'    => $resource->getId(),
                    'name'  => $resource->getName(),
                    'media' => $media,
                    'tag'   => '[%embedded-image(' . $resource->getId() . ');' . $resource->getName() . ']',
                );
            }
            $htmltext = str_replace($media, $embeddedImages[md5($media)]['tag'], $htmltext);
        }

        if (is_array($this->_newsletter['tx_inxmailcontent_data']) && is_array($embeddedImages) && count($embeddedImages)) {
            $this->_newsletter['tx_inxmailcontent_data']['embeddedImages'] = $embeddedImages;
            $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid = ' . $this->_newsletter['uid'], array('tx_inxmailcontent_data' => serialize($this->_newsletter['tx_inxmailcontent_data'])));
        }
        return $htmltext;
    }

    private function _linkResources(&$htmltext, &$resources) {

        $url = (preg_match('~https~Uis', $_SERVER['SERVER_PROTOCOL']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

        foreach($resources as $media) {

            $htmltext = str_replace($media, '[%url; "' . $url . $media . '"]', $htmltext);
        }

        return $htmltext;
    }

    /**
     * replaces resources in the html
     *
     * @param   string  $htmltext
     * @return  string
     */
    private function _handleResources(&$htmltext) {

        // Resource handling
        //<img src="[%embedded-image(33);headline-teaser-01.gif]" alt="" width="287" height="140" border="0" />

        preg_match_all('~<img[^>]*src\s*=\s*"([^"]*)"[^>]*>~Uis', $htmltext, $imgResources);
        preg_match_all('~url\(("|\')?([^\)]*)("|\')?\)~Uis', $htmltext, $cssResources);
        if (is_array($imgResources[1]) && is_array($cssResources[2])) {
            $resources = array_unique(array_merge($imgResources[1], $cssResources[2]));
        }
        elseif(is_array($imgResources[1])) {
            $resources = $imgResources[1];
        }
        elseif(is_array($cssResources[2])) {
            $resources = $cssResources[2];
        }
        $resourcesToEmbed = array();
        $recourcesToLink  = array();
        foreach($resources as $media) {
            if (preg_match('~^uploads~', $media)) {
                $recourcesToLink[] = $media;
            }
            else {
                $resourcesToEmbed[] = $media;
            }
        }
        unset($resources);

        if (is_array($resourcesToEmbed) && count($resourcesToEmbed)) {

            $htmltext = $this->_embedResources($htmltext, $resourcesToEmbed);
        }

        if (is_array($recourcesToLink) && count($recourcesToLink)) {

            $htmltext = $this->_linkResources($htmltext, $recourcesToLink);
        }

        return $htmltext;
    }

    /**
     * updates the name, subject, content handler and content of a given mailing
     *
     * @param   string  $html
     * @param   string  $text
     * @param   Inx_Apiimpl_Mailing_MailingImpl $mailing
     */
    private function _mailingData(&$html, &$text, Inx_Apiimpl_Mailing_MailingImpl $mailing) {

        $mailing->updateName($this->_newsletter['title']);
        $mailing->updateSubject($this->_newsletter['subtitle']);
        $mailing->commitUpdate();

        switch($this->_newsletter['tx_inxmailcontent_type']) {
            case 'html/text':
                $mailing->setContentHandler('Inx_Api_Mailing_MultiPartContentHandler');
                $mailing->commitUpdate();
                $contentHandler = $mailing->getContentHandler();
                /* @var $contentHandler Inx_Api_Mailing_MultiPartContentHandler */

                $contentHandler->updateHtmlTextContent($this->_trackLinks($html));
                $contentHandler->updatePlainTextContent($this->_trackLinks($text, false));
                break;
            case 'html';
                $mailing->setContentHandler('Inx_Api_Mailing_HtmlTextContentHandler');
                $mailing->commitUpdate();
                $contentHandler = $mailing->getContentHandler();
                /* @var $contentHandler Inx_Api_Mailing_HtmlTextContentHandler */

                $contentHandler->updateContent($this->_trackLinks($html));
                break;
            case 'text';
                $mailing->setContentHandler('Inx_Api_Mailing_PlainTextContentHandler');
                $mailing->commitUpdate();
                $contentHandler = $mailing->getContentHandler();
                /* @var $contentHandler Inx_Api_Mailing_PlainTextContentHandler */

                $contentHandler->updateContent($this->_trackLinks($text, false));
                break;
        }
        $mailing->commitUpdate();

        return $mailing->getId();
    }

    /**
     * transfer a newsletter rowset to the inxmail professional server
     *
     * @param   array   $newsletter
     * @param   string  $html
     * @param   string  $text
     */
    public function transferData(array &$newsletter, &$html, &$text) {

        $this->_newsletter = &$newsletter;

        try {
            $newsletterLists    = $this->getLists();
            $listId             = (int)$this->_newsletter['tx_inxmailcontent_list'];
            $listName           = $newsletterLists[$listId]['name'];
            $session            = $this->getSession();
            $listContextManager = $session->getListContextManager();
            $this->_listContext = $listContextManager->findByName($listName);
            $mailingManager     = $session->getMailingManager();

            // Check if the newsletter was already transferred to inxmail professional before
            // We try to update the already existing mailing via the API
            if (!empty($this->_newsletter['tx_inxmailcontent_mailing_id'])) {
                try {
                    $mailing = $mailingManager->get((int)$this->_newsletter['tx_inxmailcontent_mailing_id']);
                    /* @var $mailing Inx_Apiimpl_Mailing_MailingImpl */

                    // check if the selected list is still the same
                    if ($listId != $mailing->getListContextId()) {

                        // remove the mailing from the old list and create a new one later on
                        $mailingManager->remove((int)$this->_newsletter['tx_inxmailcontent_mailing_id']);
                        $this->_newsletter['tx_inxmailcontent_mailing_id'] = null;
                    }
                    else {

                        $mailingId = $this->_mailingData($html, $text, $mailing);
                    }
                }
                catch (Exception $e) {
                    // Mailing does not exist or could not be loaded, so we reset the
                    // saved mailing id to create the mailing again in the next step
                    $this->_newsletter['tx_inxmailcontent_mailing_id'] = null;
                }
            }

            // Create a new mailing
            if (empty($this->_newsletter['tx_inxmailcontent_mailing_id'])) {

                $mailing   = $mailingManager->createMailing($this->_listContext);
                $mailingId = $this->_mailingData($html, $text, $mailing);
            }
            return $mailingId;
        }
        catch (Exception $e) {}
    }

    /**
     * retreives a mailing
     *
     * @param   int     $id
     * @return  Inx_Apiimpl_Mailing_MailingImpl | boolean
     */
    public function getMailing($id) {

        try {
            $newsletterLists    = $this->getLists();
            $listId             = (int)$this->_newsletter['tx_inxmailcontent_list'];
            $listName           = $newsletterLists[$listId]['name'];
            $session            = $this->getSession();
            $mailingManager     = $session->getMailingManager();

            if ($id > 0) {
                try {
                    $mailing = $mailingManager->get((int)$id);
                }
                catch (Exception $e) {

                    return false;
                }
            }
            return $mailing;
        }
        catch (Exception $e) {}
        return false;
    }

    /**
     *
     */
    public function getTextModules($listId = 0, $mimeType = Inx_Api_TextModule_TextModule::MIME_TYPE_MULTIPART) {

        $retVal   = array();
        $cacheKey = 'tx_inxmailcontent_textmodules_' . $mimeType;
        $cache    = $this->getCache($cacheKey);

        if (is_null($cache)) {

            try {
                $newsletterLists        = $this->getLists();
                $session                = $this->getSession();
                $textModuleManager = $session->getTextmoduleManager();

                if ($listId > 0) {
                    $listName          = $newsletterLists[(int)$listId]['name'];
                    $textFromList = $textModuleManager->select($session->getListContextManager()->findByName($listName));
                    if ($textFromList->size() > 0) {
                        for ($i = 0; $i < $textFromList->size(); $i++) {

                            $textModule = $textFromList->get($i); /* @var $textModule Inx_Api_TextModule_TextModule */

                            if ($mimeType != $textModule->getMimeType()) {
                                continue;
                            }

                            $retVal[] = '[@' . $textModule->getName() . ']';
                        }
                    }
                }

                $textFromSystem = $textModuleManager->select($session->getListContextManager()->findByName(Inx_Api_List_SystemListContext::NAME));
                if ($textFromSystem->size() > 0) {
                    for ($i = 0; $i < $textFromSystem->size(); $i++) {

                        $textModule = $textFromSystem->get($i); /* @var $textModule Inx_Api_TextModule_TextModule */

                        if ($mimeType != $textModule->getMimeType()) {
                            continue;
                        }

                        $retVal[] = '[@' . $textModule->getName() . ']';
                    }
                }
            }
            catch (Exception $e) {}
            if (count($retVal)) {
                $this->addCache($cacheKey, $retVal);
            }
        }
        else {
            $retVal = $cache;
        }
        return $retVal;
    }
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/classes/class.tx_inxmailcontent.php']) {
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/classes/class.tx_inxmailcontent.php']);
}
?>