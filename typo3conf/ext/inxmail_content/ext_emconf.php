<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "inxmail_content".
 *
 * Auto generated 30-09-2016 16:02
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Inxmail Professional Newsletter',
  'description' => 'Extension for creating, scheduling and sending of newsletters based on a predefined template within TYPO3. Requires Inxmail API (not included in this extension). Enables the use of tt_news by Rupert Germann.',
  'category' => 'module',
  'version' => '1.0.6',
  'state' => 'stable',
  'uploadfolder' => 0,
  'createDirs' => '',
  'clearcacheonload' => 0,
  'author' => 'Jens Jacobsen',
  'author_email' => 'extensions@digi-info.de',
  'author_company' => '',
  'constraints' => 
  array (
    'depends' => 
    array (
      'php' => '5.2.6-0.0.0',
      'typo3' => '4.3.3-4.5.99',
      'cms' => '',
      'inxmail_core' => '1.0.1',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

