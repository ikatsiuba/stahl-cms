<?php
// DO NOT REMOVE OR CHANGE THESE 4 LINES:
define('TYPO3_MOD_PATH', '../typo3conf/ext/inxmail_content/mod_content/');
$BACK_PATH='../../../../typo3/';
$MCONF['name'] = 'txinxmailcoreMainModule_txinxmailcoreContentModule';
$MCONF['script'] = '_DISPATCH';
$MCONF['script'] = 'index.php';

$MCONF['access'] = 'user,group';
$MCONF['workspaces'] = 'online';

$MLANG['default']['tabs_images']['tab'] = 'moduleicon.gif';
$MLANG['default']['ll_ref'] = 'LLL:EXT:inxmail_content/mod_content/locallang_mod.xml';

$MCONF['navFrameScript'] = $BACK_PATH . 'alt_db_navframe.php';
?>