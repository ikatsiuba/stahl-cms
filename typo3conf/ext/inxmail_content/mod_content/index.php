<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Inxmail GmbH
*  (c) 2010 [di] digitale informationssysteme gmbh
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * index.php
 *
 * Provides the content module for the 'inxmail_content' extension.
 *
 * $Id:$
 *
 * @author Jens Jacobsen <jacobsen@digi-info.de>
 */
unset($MCONF);
require_once "conf.php";
require_once $BACK_PATH . "init.php";
require_once $BACK_PATH . "template.php";
require_once PATH_t3lib . 'class.t3lib_tceforms.php';
require_once PATH_t3lib . 'class.t3lib_scbase.php';
require_once t3lib_extMgm::extPath('inxmail_content') . 'classes/class.tx_inxmailcontent.php';

$LANG->includeLLFile('EXT:inxmail_content/mod_content/locallang.xml');
$BE_USER->modAccess($MCONF, 1);  // This checks permissions and exits if the users has no permission for entry.

/**
 * Module 'content' for the 'inxmail_content' extension.
 *
 * @author  Jens Jacobsen <jacobsen@digi-info.de>
 * @package TYPO3
 * @subpackage tx_inxmail_content
 */
class tx_inxmailcore_module_content extends t3lib_SCbase {

    /**
     * pageinfo
     *
     * @var mixed
     */
    private $pageinfo;

    /**
     * extension configuration
     *
     * @var array
     */
    private $extConf;

    /**
     * tt_news container id
     *
     * @var int
     */
    private $startingPoint = 0;

    /**
     * ts template
     *
     * @var t3lib_TStemplate
     */
    private $tmpl;

    /**
     * tx_inxmailcontent object
     *
     * @var tx_inxmailcontent
     */
    private $_inxmail = null;

    /**
     * tx_inxmailcontent object
     *
     * @var t3lib_TCEforms
     */
    private $_tceforms = null;

    /**
     * Initializes the Module
     * @return  void
     */
    public function init() {
        global $BE_USER,$LANG,$BACK_PATH,$TCA_DESCR,$TCA,$CLIENT,$TYPO3_CONF_VARS;

        $this->_inxmail      = t3lib_div::makeInstance('tx_inxmailcontent');

        parent::init();

        $this->startingPoint = (int)$this->_fetchTS('tx_inxmailcontent', 'newsContainerId', (int)t3lib_div::_GP('id'));
        $this->sys_language  = intval(t3lib_div::_GP('sys_language_uid'));

        // initialize the TS template
        $GLOBALS['TT'] = new t3lib_timeTrack;
        $this->tmpl    = t3lib_div::makeInstance('t3lib_TStemplate');
        $this->tmpl->init();

        // initialize the page selector
        $this->sys_page = t3lib_div::makeInstance('t3lib_pageSelect');
        $this->sys_page->init(true);

        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['inxmail_content']);
    }

    /**
     * Adds items to the ->MOD_MENU array. Used for the function menu selector.
     *
     * @return  void
     */
    public function menuConfig()   {
        global $LANG;
        $this->MOD_MENU = Array (
            "function" => Array (
                 0 => $LANG->getLL("newsletterNew"),
                 1 => $LANG->getLL("newsletterShowAllLists"),
            )
        );
        $newsletterLists = $this->_inxmail->getLists();

        if (is_array($newsletterLists) && count($newsletterLists)) {
            foreach($newsletterLists as $newsletterList) {
                $this->MOD_MENU['function'][($newsletterList['id'] + 10)] = sprintf($LANG->getLL("newsletterShowSpecList"), $newsletterList['name']);
            }
        }
        parent::menuConfig();
    }

    /**
     * Main function of the module. Write the content to $this->content
     * If you chose "web" as main module, you will need to consider the $this->id parameter which will contain the uid-number of the page clicked in the page tree
     *
     * @return  [type]      ...
     */
    public function main() {
        global $BE_USER,$LANG,$BACK_PATH,$TCA_DESCR,$TCA,$CLIENT,$TYPO3_CONF_VARS;

        // Access check!
        // The page will show only if there is a valid page and if this page may be viewed by the user
        $this->pageinfo = t3lib_BEfunc::readPageAccess($this->id,$this->perms_clause);
        $access = is_array($this->pageinfo) ? 1 : 0;

            // initialize doc
        $this->doc = t3lib_div::makeInstance('template');
        $this->doc->setModuleTemplate(t3lib_extMgm::extPath('inxmail_content') . 'mod_content/mod_template.html');
        $this->doc->backPath = $BACK_PATH;
        $docHeaderButtons = $this->getButtons();

        if (($this->id && $access) || ($BE_USER->user['admin'] && !$this->id))  {

                // Draw the form
            $this->doc->form = '<form action="" method="post" enctype="multipart/form-data">';

                // JavaScript
            $this->doc->JScode = '
                <script language="javascript" type="text/javascript">
                    script_ended = 0;
                    function jumpToUrl(URL) {
                        document.location = URL;
                    }
                </script>
            ';
            $this->doc->postCode='
                <script language="javascript" type="text/javascript">
                    script_ended = 1;
                    if (top.fsMod) top.fsMod.recentIds["web"] = 0;
                </script>
            ';
            $this->moduleContent();

        } else {
            // If no access or if ID == zero
            $docHeaderButtons['save'] = '';
            $this->content.=$this->doc->spacer(10);
        }

        // compile document
        $markers['FUNC_MENU'] = t3lib_BEfunc::getFuncMenu($this->id, 'SET[function]', $this->MOD_SETTINGS['function'], $this->MOD_MENU['function']);
        $markers['CONTENT'] = $this->content;

        // Build the <body> for the module
        $this->content = $this->doc->startPage($LANG->getLL('title'));
        $this->content.= $this->doc->moduleBody($this->pageinfo, $docHeaderButtons, $markers);
        $this->content.= $this->doc->endPage();
        $this->content = $this->doc->insertStylesAndJS($this->content);
    }

    /**
     * Prints out the module HTML
     *
     * @return  void
     */
    public function printContent() {

        echo $this->content;
    }

    /**
     * helper method to fetch the ts config
     *
     * @param   string $extension
     * @param   mixed $value
     * @param   int $pageId
     */
    private function _fetchTS($extension, $value, $pageId = 1) {
        $sysPageObj = t3lib_div::makeInstance('t3lib_pageSelect');
        $rootLine   = $sysPageObj->getRootLine($pageId);
        $TSObj      = t3lib_div::makeInstance('t3lib_tsparser_ext');

        $TSObj->tt_track = 0;
        $TSObj->init();
        $TSObj->runThroughTemplates($rootLine);
        $TSObj->generateConfig();
        return $TSObj->setup['plugin.'][$extension . '.'][$value];
    }

    /**
     *
     * @param   int     $uid
     * @return  array
     */
    private function _getNewsletter($uid) {

        $retVal = array();
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            'uid,pid,doktype,title,subtitle,abstract,hidden,crdate,tstamp,tx_inxmailcontent_type,tx_inxmailcontent_list,tx_inxmailcontent_salutation,tx_inxmailcontent_template,tx_inxmailcontent_mailing_id,tx_inxmailcontent_data',
            'pages',
            'uid = '. (int)$uid . ' AND doktype = 47'. ' AND ' . $this->perms_clause . t3lib_BEfunc::deleteClause('pages'),
            null,
            'crdate DESC, title ASC',
            '0,1'
        );

        if ($GLOBALS['TYPO3_DB']->sql_num_rows($res) == 1) {
            $retVal                           = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
            $retVal['tx_inxmailcontent_data'] = unserialize($retVal['tx_inxmailcontent_data']);
            if (!is_array($retVal['tx_inxmailcontent_data'])) {
                $retVal['tx_inxmailcontent_data'] = array();
            }
        }
        return $retVal;
    }

    /**
     * handle the transfer form and action
     *
     * @param   int     $uid
     * @return  string
     */
    private function _handleTransfer($uid) {
        global $LANG,$BACK_PATH;

        $newsletter = $this->_getNewsletter($uid);
        $content    = '';

        if (count($newsletter)) {

            if (t3lib_div::_POST('submitAbort') != '') {
                header('Location: ' . 'index.php?id=' . (int)$newsletter['pid']);
                exit;
            }

            $this->doc->form = '<form action="index.php" name="editform" method="post">';
            $content        .= '<input type="hidden" name="id" value="' . (int)$newsletter['pid'] . '" />';
            $content        .= '<input type="hidden" name="sendrecord" value="' . (int)$newsletter['uid'] . '" />';

            $url     = t3lib_div::getIndpEnv('TYPO3_SITE_URL') . 'index.php?id=' . $newsletter['uid'];
            $ch      = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $dataHtml = trim(curl_exec($ch));
            curl_close($ch);

            $ch      = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url . '&type=500');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $dataText = trim(curl_exec($ch));
            curl_close($ch);

            if (t3lib_div::_POST('submitTransform') != '') {

                $newsletter['tx_inxmailcontent_data']['linktype'] = t3lib_div::_POST('linktype');
                $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid = ' . (int)$newsletter['uid'], array('tx_inxmailcontent_data' => serialize($newsletter['tx_inxmailcontent_data'])));

                $mailingId = $this->_inxmail->transferData($newsletter, $dataHtml, $dataText);

                if (is_numeric($mailingId) && $mailingId > 0) {
                    $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid = ' . (int)$newsletter['uid'], array('tx_inxmailcontent_mailing_id' => (int)$mailingId));
                    $newsletter['tx_inxmailcontent_data']['lastTransfer'] = time();
                    $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid = ' . (int)$newsletter['uid'], array('tx_inxmailcontent_data' => serialize($newsletter['tx_inxmailcontent_data'])));
                }
                header('Location: ' . 'index.php?id=' . (int)$newsletter['pid'] . '&schedulerecord=' . (int)$newsletter['uid']);
                exit;
            }
            else {
                $content.= $this->doc->section($LANG->getLL('newsletterInfo'), $this->_renderNewsletter($newsletter), 0, 1);
                $content.= $this->doc->section($LANG->getLL('newsletterTransfer'), $this->_getLinks($newsletter, $dataHtml), 0, 1);
            }
        }
        return $content;
    }

    /**
     * handle the schedule form and action
     *
     * @param   int     $uid
     * @return  string
     */
    private function _handleSchedule($uid) {
        global $LANG,$BACK_PATH;

        $newsletter = $this->_getNewsletter($uid);
        $content    = '';

        if (count($newsletter)) {

            if (t3lib_div::_POST('submitAbort') != '') {
                header('Location: ' . 'index.php?id=' . (int)$newsletter['pid']);
                exit;
            }
            if (t3lib_div::_POST('submitTransfer') != '') {
                header('Location: ' . 'index.php?id=' . (int)$newsletter['pid'] . '&sendrecord=' . (int)$newsletter['uid']);
                exit;
            }

            $mailing = $this->_inxmail->getMailing($newsletter['tx_inxmailcontent_mailing_id']);

            if ($mailing) {

                // setup offset for sending time
                $offset = (isset($this->extConf['sendingTimeOffset']) ? (int)$this->extConf['sendingTimeOffset'] : 0) * 60;

                // actions
                $scheduleDate = is_null($mailing->getScheduleDatetime()) ? time() + $offset : strtotime($mailing->getScheduleDatetime());
                $scheduleTime = t3lib_div::_POST('scheduletime') ? t3lib_div::_POST('scheduletime') : array('hour' => date('H', $scheduleDate), 'minute' => date('i', $scheduleDate));
                $scheduleDate = t3lib_div::_POST('scheduledate_hr') ? strtotime(t3lib_div::_POST('scheduledate_hr') . ' ' . sprintf('%02d', (int)$scheduleTime['hour']) . ':' . sprintf('%02d', (int)$scheduleTime['minute']) . ':00') : $scheduleDate;

                if (t3lib_div::_POST('submitSchedule') != '') {

                    try {
                            $mailing->scheduleMailing(date('c', $scheduleDate));
                            $mailing->commitUpdate();
                    }
                    catch (Exception $e) {
                        try {
                            $mailing->updateScheduleDatetime(date('c', $scheduleDate));
                            $mailing->commitUpdate();
                        }
                        catch (Exception $e) {}
                    }
                    try {
                        $mailing->approve();
                    }
                    catch (Exception $e) {}

                    $content.= $this->doc->section($LANG->getLL('information'), '<p>' . $LANG->getLL('scheduleAdded') . '<br />&nbsp;</p>');
                }
                elseif (t3lib_div::_POST('submitCancel') != '') {
                    try {
                        $mailing->unscheduleMailing();
                    }
                    catch (Exception $e) {}
                    try {
                        $mailing->revokeApproval();
                    }
                    catch (Exception $e) {}

                    $content.= $this->doc->section($LANG->getLL('information'), '<p>' . $LANG->getLL('scheduleDeleted') . '<br />&nbsp;</p>');
                }
                elseif (t3lib_div::_POST('submitNow') != '') {
                    try {
                        $mailing->unscheduleMailing();
                    }
                    catch (Exception $e) {}
                    try {
                        $mailing->approve();
                    }
                    catch (Exception $e) {}
                    try {
                        $mailing->startSending();
                    }
                    catch (Exception $e) {}

                    $content.= $this->doc->section($LANG->getLL('information'), '<p>' . $LANG->getLL('sendingStarted') . '<br />&nbsp;</p>');
                }

                $this->_updateMailingInfos($newsletter, $mailing);

                $content.= $this->doc->section($LANG->getLL('newsletterScheduleInfo'), $this->_renderNewsletter($newsletter), 0, 1);

                if (is_null($mailing->getScheduleDatetime()) && is_null($mailing->getSentDatetime())) {
                    $content .= $this->doc->section($LANG->getLL('newsletterScheduleStatus'), '<p>' . $LANG->getLL('notScheduledYet') . '<br />&nbsp;</p>');
                }
                elseif (!is_null($mailing->getSentDatetime())) {
                    $sendDate = strtotime($mailing->getSentDatetime());
                    $content .= $this->doc->section($LANG->getLL('newsletterScheduleStatus'), '<p>' . sprintf($LANG->getLL('sentText'), date($GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'], $sendDate), date($GLOBALS['TYPO3_CONF_VARS']['SYS']['hhmm'], $sendDate)) . '</p>');
                }
                elseif (!is_null($mailing->getScheduleDatetime())) {
                    $planDate = strtotime($mailing->getScheduleDatetime());
                    $content .= $this->doc->section($LANG->getLL('newsletterScheduleStatus'), '<p>' . sprintf($LANG->getLL('plannedText'), date($GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'], $planDate), date($GLOBALS['TYPO3_CONF_VARS']['SYS']['hhmm'], $planDate)) . '</p>');
                }

                if (is_null($mailing->getSentDatetime())) {

                    $content .= $this->doc->section($LANG->getLL('newsletterSchedule'), '');

                    $this->_tceforms = t3lib_div::makeInstance("t3lib_TCEforms");
                    $this->_tceforms->initDefaultBEmode();
                    $this->_tceforms->backPath = $BACK_PATH;

                    $content                  .= $this->_tceforms->printNeededJSFunctions_top();
                    $this->doc->form           = '<form action="index.php" name="editform" method="post">';
                    $content                  .= '<input type="hidden" name="id" value="' . (int)$newsletter['pid'] . '" />';
                    $content                  .= '<input type="hidden" name="schedulerecord" value="' . (int)$newsletter['uid'] . '" />';

                    $layout = array (
                        'table'  => array('<table border="0" cellpadding="3" cellspacing="1">', '</table>'),
                        'defRow' => array(
                            'tr'     => array('<tr class="tr-normal">', '</tr>'),
                            'defCol' => array('<td style="padding-left:3px;vertical-align:top;">', '</td>'),
                            0        => array('<td style="padding-left:3px;vertical-align:top;width:50px;">', '</td>'),
                        ),
                    );
                    $table = array();

                    $conf = array(
                        'itemFormElName' => 'scheduledate',
                        'itemFormElValue' => $scheduleDate,
                        'fieldConf' => array(
                            'label' => 'test',
                            'config' => array(
                                'type' => 'input',
                                'eval' => 'trim,date',
                                'size' => 9,
                            ),
                        ),
                    );
                    $table[] = array(
                        '<label>' . $LANG->getLL('scheduledate') . ':</label>&nbsp;',
                        $this->_tceforms->getSingleField_SW('', '', array(), $conf),
                    );

                    $confHours = array(
                        'itemFormElName' => 'scheduletime[hour]',
                        'itemFormElValue' => (int)$scheduleTime['hour'],
                        'fieldConf' => array(
                            'config' => array(
                                'type' => 'select',
                                'eval' => 'trim',
                                'size' => '1',
                            ),
                        ),
                    );
                    for ($i = 0; $i <= 23; $i++) {
                        $confHours['fieldConf']['config']['items'][] = array(sprintf('%02d', $i), $i);
                    }
                    $confMinutes = array(
                        'itemFormElName' => 'scheduletime[minute]',
                        'itemFormElValue' => (int)$scheduleTime['minute'],
                        'fieldConf' => array(
                            'config' => array(
                                'type' => 'select',
                                'eval' => 'int',
                                'size' => '1',
                            ),
                        ),
                    );
                    for ($i = 0; $i <= 59; $i++) {
                        $confMinutes['fieldConf']['config']['items'][] = array(sprintf('%02d', $i), $i);
                    }
                    $table[] = array(
                        '<label>' . $LANG->getLL('scheduletime') . ':</label>&nbsp;',
                        $this->_tceforms->getSingleField_SW('', '', array(), $confHours) .
                        '&nbsp;:&nbsp;' .
                        $this->_tceforms->getSingleField_SW('', '', array(), $confMinutes),
                    );

                    $content.= $this->doc->table($table, $layout);
                    $content.= $this->_tceforms->printNeededJSFunctions();
                    if (is_null($mailing->getScheduleDatetime())) {
                        $content.= '<br /><input type="submit" name="submitSchedule" value="' . $LANG->getLL('submitScheduleSet') . '" />';
                    }
                    else {
                        $content.= '<br /><input type="submit" name="submitSchedule" value="' . $LANG->getLL('submitScheduleChange') . '" />';
                        $content.= '&nbsp;<input type="submit" name="submitCancel" value="' . $LANG->getLL('submitCancel') . '" />';
                    }
                    $content.= '&nbsp;<input type="submit" name="submitNow" value="' . $LANG->getLL('submitNow') . '" />&nbsp;';
                }
                $content.= '<input type="submit" name="submitAbort" value="' . $LANG->getLL('backToOverview') . '" />';
            }
            else {
                $content .= $this->doc->section($LANG->getLL('notFoundTitle'), sprintf('<p>%s</p>', $LANG->getLL('notFoundText')));

                $content.= '<br /><input type="submit" name="submitTransfer" value="' . $LANG->getLL('sendtoinxmail') . '" />';
                $content.= '&nbsp;<input type="submit" name="submitAbort" value="' . $LANG->getLL('backToOverview') . '" />';
            }
        }
        return $content;
    }

    /**
     * handles the update action
     *
     * @param   int     $uid
     * @return  void
     */
    private function _handleUpdate($uid) {
        global $LANG,$BACK_PATH;

        $newsletter = $this->_getNewsletter($uid);
        $content    = '';

        if (count($newsletter)) {

            $mailing = $this->_inxmail->getMailing($newsletter['tx_inxmailcontent_mailing_id']);

            if ($mailing) {

                $this->_updateMailingInfos($newsletter, $mailing);
            }

            header('Location: ' . 'index.php?id=' . (int)$newsletter['pid']);
            exit;
        }
    }

    /**
     * updates the inxmail related infos for a given newsletter and its mailing object
     *
     * @param   array       $newsletter
     * @param               $mailing
     */
    private function _updateMailingInfos(array &$newsletter, Inx_Apiimpl_Mailing_MailingImpl $mailing) {

        // update local data for the newsletter
        $newsletter['tx_inxmailcontent_data']['status']               = $mailing->getState();
        $newsletter['tx_inxmailcontent_data']['scheduleDatetime']     = is_null($mailing->getScheduleDatetime()) ? 0 : (int)strtotime($mailing->getScheduleDatetime());;
        $newsletter['tx_inxmailcontent_data']['sentDatetime']         = is_null($mailing->getSentDatetime()) ? 0 : (int)strtotime($mailing->getSentDatetime());;
        $newsletter['tx_inxmailcontent_data']['modificationDatetime'] = is_null($mailing->getModificationDatetime()) ? 0 : (int)strtotime($mailing->getModificationDatetime());;

        if ($newsletter['tx_inxmailcontent_data']['status'] == Inx_Api_Mailing_Mailing::STATE_SENT) {

            $sendingInfo = $mailing->getSendingInfo();
            /* @var $sendingInfo Inx_Apiimpl_Mailing_SendingInfoImpl */
            $newsletter['tx_inxmailcontent_data']['averageMailSize']     = $sendingInfo->getAverageMailSize();
            $newsletter['tx_inxmailcontent_data']['bounceCount']         = $sendingInfo->getBounceCount();
            $newsletter['tx_inxmailcontent_data']['deliveredMailsCount'] = $sendingInfo->getDeliveredMailsCount();
            $newsletter['tx_inxmailcontent_data']['notSentMailsCount']   = $sendingInfo->getNotSentMailsCount();
            $newsletter['tx_inxmailcontent_data']['sentErrorCount']      = $sendingInfo->getSentErrorCount();
        }

        // write updated local data for the newsletter
        $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid = ' . (int)$newsletter['uid'], array('tx_inxmailcontent_data' => serialize($newsletter['tx_inxmailcontent_data'])));
    }

    /**
     * handle the newsletter create form and actions
     *
     * @return  string
     */
    private function _handleNewForm() {
        global $LANG, $BACK_PATH;

        $pid                 = t3lib_div::_GP('id');
        $parentRecord        = t3lib_BEfunc::getRecord('pages', $pid);
        $title               = trim(t3lib_div::_GP('title_hr'));
        $subtitle            = trim(t3lib_div::_GP('subtitle_hr'));
        $list                = trim(t3lib_div::_GP('list'));
        $ttnewsImport        = t3lib_div::_POST('ttnewsImport');
        $ttnewsImportCount   = (int)t3lib_div::_POST('ttnewsImportCount_hr');

        if (is_numeric($pid) && $pid > 0 && !empty($title)) {

            // add the newsletter page
            $data = array();
            $data['pages']['NEW9823be87'] = array(
                "title"                   => $title,
                "subtitle"                => $subtitle,
                "tx_inxmailcontent_list"  => $list,
                "pid"                     => $pid,
                "hidden"                  => 0,
                "doktype"                 => 47,
            );
            require_once (PATH_t3lib."class.t3lib_tcemain.php");
            $t3lib_TCEmain = t3lib_div::makeInstance('t3lib_TCEmain');
            $t3lib_TCEmain->stripslashes_values = 0;
            $t3lib_TCEmain->start($data,array());
            $t3lib_TCEmain->process_datamap();

            // get the new page id
            $newUid = $t3lib_TCEmain->substNEWwithIDs['NEW9823be87'];

            if ($newUid && ($ttnewsImport == 'lastXelements' || $ttnewsImport = 'lastXdays') && 0 < $ttnewsImportCount) {


                if ($ttnewsImport == 'lastXelements') {

                    $news = $this->_getExistingNews(1, (int)$ttnewsImportCount);
                }
                else {

                    $news = $this->_getExistingNews(2, (int)$ttnewsImportCount);
                }

                if (is_array($news) && count($news)) {

                    foreach($news as $newsEntry) {

                        $data = array();
                        $data['tt_content']['NEWbe68s587'] = array(
                            "header"    => $newsEntry['title'],
                            "bodytext"  => strip_tags($newsEntry['bodytext'], '<a><b><i><u><strong><em><link>'),
                            "image"     => $newsEntry['image'],
                            "CType"     => 'inxmail_content_ctype',
                            "pid"       => $newUid,
                            "hidden"    => 0,
                        );
                        $t3lib_TCEmain->stripslashes_values = 0;
                        $t3lib_TCEmain->start($data,array());
                        $t3lib_TCEmain->process_datamap();

                        $newPid = $t3lib_TCEmain->substNEWwithIDs['NEWbe68s587'];
                    }
                }
            }
            header("Location: index.php?id=" . (int)$pid . "&SET[function]=1");
            exit;
        }

        if ($parentRecord['module'] == 'inxnl') {

            $this->_tceforms = t3lib_div::makeInstance("t3lib_TCEforms");
            $this->_tceforms->initDefaultBEmode();
            $this->_tceforms->backPath = $BACK_PATH;

            $content                  .= $this->_tceforms->printNeededJSFunctions_top();
            $this->doc->form           = '<form action="index.php" name="editform" method="post">';
            $content                  .= '<input type="hidden" name="id" value="' . (int)t3lib_div::_GP('id') . '" />';

            $layout = array (
                'table'  => array('<table border="0" cellpadding="3" cellspacing="1">', '</table>'),
                'defRow' => array(
                    'tr'     => array('<tr class="tr-normal">', '</tr>'),
                    'defCol' => array('<td style="padding:3px;vertical-align:top;">', '</td>'),
                    0        => array('<td style="padding:3px;vertical-align:top;width:50px;">', '</td>'),
                ),
            );
            $table = array();

            $conf = array(
                'itemFormElName'  => 'title',
                'itemFormElValue' => $title,
                'fieldConf' => array(
                    'config' => array(
                        'type' => 'input',
                        'eval' => 'required,trim',
                        'size' => 35,
                    ),
                ),
            );
            $table[] = array('<label>' . $LANG->getLL('listTitle.name') . ':</label>&nbsp;', $this->_tceforms->getSingleField_SW('', '', array(), $conf));

            $conf = array(
                'itemFormElName'  => 'subtitle',
                'itemFormElValue' => $subtitle,
                'fieldConf' => array(
                    'config' => array(
                        'type' => 'input',
                        'eval' => 'required,trim',
                        'size' => 35,
                    ),
                ),
            );
            $table[] = array('<label>' . $LANG->getLL('listTitle.subject') . ':</label>&nbsp;', $this->_tceforms->getSingleField_SW('', '', array(), $conf));

            $conf = array(
                'itemFormElName'  => 'list',
                'itemFormElValue' => $list,
                'fieldConf' => array(
                    'config' => array(
                        'type' => 'select',
                        'eval' => 'required,trim',
                        'size' => 1,
                    ),
                ),
            );
            $newsletterLists = $this->_inxmail->getLists();
            if (is_array($newsletterLists) && count($newsletterLists)) {
                foreach($newsletterLists as $newsletterList) {
                    $conf['fieldConf']['config']['items'][] = array($newsletterList['name'], $newsletterList['id']);
                }
            }
            $table[] = array('<label>' . $LANG->getLL('listTitle.list') . ':</label>&nbsp;', $this->_tceforms->getSingleField_SW('', '', array(), $conf));

            if (t3lib_extMgm::isLoaded('tt_news') && $this->startingPoint > 0) {

                $conf = array(
                    'itemFormElName' => 'ttnewsImport',
                    'itemFormElValue' => '',
                    'fieldConf' => array(
                        'config' => array(
                            'type'  => 'radio',
                            'items' => array(
                                0 => array($LANG->getLL('ttnewsImportLastXelements'), 'lastXelements'),
                                1 => array($LANG->getLL('ttnewsImportLastXdays'), 'lastXdays'),
                            )
                        ),
                    ),
                );
                $table[] = array('<label>' . $LANG->getLL('ttnewsImport') . ':</label>&nbsp;', $this->_tceforms->getSingleField_SW('', '', array(), $conf));

                $conf = array(
                    'itemFormElName'  => 'ttnewsImportCount',
                    'itemFormElValue' => 0,
                    'fieldConf' => array(
                        'config' => array(
                            'type' => 'input',
                            'eval' => 'int',
                            'size' => 2,
                        ),
                    ),
                );
                $table[] = array('<label>' . $LANG->getLL('ttnewsImportCount') . ':</label>&nbsp;', $this->_tceforms->getSingleField_SW('', '', array(), $conf));
            }

            $content.= $this->doc->section($LANG->getLL('submit.createnewsletter'), $this->doc->table($table, $layout));
            $content.= $this->_tceforms->printNeededJSFunctions();
            $content.= '<br /><input type="submit" name="submitCreate" value="' . $LANG->getLL('submit.createnewsletter') . '" />&nbsp;';
        }
        else {
            $content = $this->doc->section($LANG->getLL('information'), '<p>' . $LANG->getLL('notAInxmailSysFolder') .'<br />&nbsp;</p>');
        }
        return $this->doc->section($LANG->getLL('function2'),$content,0,1);
    }

    /**
     * Generates the module content
     *
     * @return  void
     */
    public function moduleContent() {
        global $BE_USER,$LANG,$BACK_PATH,$TCA_DESCR,$TCA,$CLIENT,$TYPO3_CONF_VARS;
        $content = null;

        switch((int)$this->MOD_SETTINGS["function"]) {
            case 0:
                $this->content.= $this->_handleNewForm();
            break;
            case 1:
                $showAll = true;
            default:
                $pid            = (int)t3lib_div::_GP('id');
                $sendRecord     = (int)t3lib_div::_GP('sendrecord');
                $scheduleRecord = (int)t3lib_div::_GP('schedulerecord');
                $updateRecord   = (int)t3lib_div::_GP('updaterecord');
                $parentRecord   = t3lib_BEfunc::getRecord('pages', $pid);

                if ((int)$parentRecord['doktype'] === 47) {

                    header('Location: ' . 'db_layout.php?id=' . $pid);
                    exit;
                }
                elseif ((int)$parentRecord['doktype'] === 254) {

                    if ($parentRecord['module'] == 'inxnl') {

                        if ($sendRecord > 0) {

                            $this->content.= $this->_handleTransfer($sendRecord);
                        }
                        elseif ($scheduleRecord > 0) {

                            $this->content.= $this->_handleSchedule($scheduleRecord);
                        }
                        elseif ($updateRecord > 0) {

                            $this->content.= $this->_handleUpdate($updateRecord);
                        }
                        else {

                            $this->content.= $this->doc->section($LANG->getLL('newsletterOverview'), $this->_renderNewsletters((int)$this->MOD_SETTINGS["function"]), 0, 1);
                        }
                    }
                    else {

                        $this->content.= $this->doc->section($LANG->getLL('information'), '<p>' . $LANG->getLL('notAInxmailSysFolder') .'<br />&nbsp;</p>');
                    }
                }
                else {

                    $this->content.= $this->doc->section($LANG->getLL('information'), '<p>' . $LANG->getLL('notAInxmailSysFolder') .'<br />&nbsp;</p>');
                }
                break;
        }
    }

    /**
     * show the list of existing records
     *
     * @param   string $html
     * @return  string
     */
    private function _getLinks(&$newsletter, &$html) {
        global $LANG, $TYPO3_DB, $BACK_PATH;

        $matches = array();
        preg_match_all('~<a[^>]*href\s*=\s*"([^"]*)"[^>]*>(.*)<\/a>~Uis', $html, $matches);

        if (!is_array($matches) || !is_array($matches[0]) || count($matches[0]) < 1) {
            $theOutput.= $this->doc->section($LANG->getLL('error'),$LANG->getLL('noLinksFound'),0,1);
        } else {
            $layout = array (
                'table'  => array('<table class="typo3-dblist" border="0" cellpadding="3" cellspacing="1">', '</table>'),
                'defRow' => array(
                    'tr'     => array('<tr class="tr-normal">', '</tr>'),
                    'defCol' => array('<td style="padding-left:3px;vertical-align:top;">', '</td>'),
                    0        => array('<td style="padding-left:3px;vertical-align:top;width:50px;">', '</td>'),
                    1        => array('<td style="padding-left:3px;vertical-align:top;width:150px;">', '</td>'),
                    2        => array('<td style="padding-left:3px;vertical-align:top;width:450px;font-weight:bold;">', '</td>'),
                ),
            );
            $layout[0]['tr'] = array('<tr class="c-headLineTable bgColor2" style="font-weight:bold;">','</tr>');
            $table           = array();
            $table[0][]      = '<span>' . $LANG->getLL('listTitle.linktype') . '</span>';
            $table[0][]      = '<span>' . $LANG->getLL('listTitle.linktext') . '</span>';
            $table[0][]      = '<span>' . $LANG->getLL('listTitle.url') . '</span>';
            $table[0][]      = '<span>' . $LANG->getLL('listTitle.link') . '</span>';

            $aTags = array();
            foreach($matches[0] as $key => $value) {

                if (!preg_match('~^(\[|mailto:|#)~Uis', $matches[1][$key])) {

                    $aTags[md5($matches[1][$key])] = array(
                        'href'     => $matches[1][$key],
                        'linktext' => $matches[2][$key],
                    );
                }
            }

            $options = array(
                ''             => '<option value="" %%>' . $LANG->getLL('transform.ignore') . '</option>',
                'simple'       => '<option value="simple" %simple%>' . $LANG->getLL('transform.simple') . '</option>',
                'unsubscribe'  => '<option value="unsubscribe" %unsubscribe%>' . $LANG->getLL('transform.unsubscribe') . '</option>',
                'count'        => '<option value="count" %count%>' . $LANG->getLL('transform.count') . '</option>',
                'unique-count' => '<option value="unique-count" %unique-count%>' . $LANG->getLL('transform.unique-count') . '</option>',
            );

            foreach($aTags as $key => $aTag) {

                $currentOptions = implode('', $options);
                foreach ($options as $type => $option) {
                    $currentOptions = str_replace('%'.$type.'%', (!empty($newsletter['tx_inxmailcontent_data']['linktype'][$key]) && $newsletter['tx_inxmailcontent_data']['linktype'][$key] == $type ? 'selected="selected"' : ''), $currentOptions);
                }

                $record   = array();
                $record[] = '<select name="linktype[' . $key . ']">'.$currentOptions.'</select>';
                $record[] = '<span' . (strstr($currentOptions, 'selected="selected"') ? ' style="color:#666;"' : '') . '>' . $aTag['linktext'] . '</span>';
                $record[] = '<span' . (strstr($currentOptions, 'selected="selected"') ? ' style="color:#666;"' : '') . '>' . $aTag['href']     . '</span>';
                $record[] = '<a href="' . $aTag['href'] . '"' . (strstr($currentOptions, 'selected="selected"') ? ' style="color:#666;"' : '') . '>' . $aTag['linktext'] . '</a>';
                $table[]  = $record;
            }
            $theOutput.= $this->doc->table($table, $layout);
            $theOutput.= '<p><br /><input type="submit" name="submitTransform" value="' . $LANG->getLL('transform.submit') . '">';
            $theOutput.= '&nbsp;<input type="submit" name="submitAbort" value="' . $LANG->getLL('backToOverview') . '" /></p>';
        }
        return $theOutput;
    }

    /**
     * returns the status string
     *
     * @param   array     $newsletter
     * @return  string
     */
    private function _getStatus(array $newsletter) {
        global $LANG;

        $retVal = '';

        if ($newsletter['tx_inxmailcontent_data']['status'] > 0) {

            $retVal.= $LANG->getLL('mailing_state_' . (int)$newsletter['tx_inxmailcontent_data']['status']);

            switch((int)$newsletter['tx_inxmailcontent_data']['status']) {

                case Inx_Api_Mailing_Mailing::STATE_SCHEDULED:
                    $datetime = $newsletter['tx_inxmailcontent_data']['scheduleDatetime'];
                    $retVal  .= ':<br />' . date($GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'], $datetime) . ', ' . date($GLOBALS['TYPO3_CONF_VARS']['SYS']['hhmm'], $datetime);
                    break;
                case Inx_Api_Mailing_Mailing::STATE_SENT:
                    $datetime = $newsletter['tx_inxmailcontent_data']['sentDatetime'];
                    $retVal  .= ':<br />' . date($GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'], $datetime) . ', ' . date($GLOBALS['TYPO3_CONF_VARS']['SYS']['hhmm'], $datetime);
                    break;
            }
        }
        else {

            $retVal.= $LANG->getLL('draft');
        }
        return $retVal;
    }

    /**
     * returns the information string
     *
     * @param   array     $newsletter
     * @return  string
     */
    private function _getInformation(array $newsletter, $full = false) {
        global $LANG;

        $retVal = '';

        if ($newsletter['tx_inxmailcontent_data']['status'] == Inx_Api_Mailing_Mailing::STATE_SENT) {

            if ($full) {
                $retVal.= $LANG->getLL('averageMailSize') . ': ' . round($newsletter['tx_inxmailcontent_data']['averageMailSize'] / 1024, 1) . ' KB';
                $retVal.= '<br />' . $LANG->getLL('bounceCount') . ': ' . $newsletter['tx_inxmailcontent_data']['bounceCount'];
                $retVal.= '<br />' . $LANG->getLL('deliveredMailsCount') . ': ' . $newsletter['tx_inxmailcontent_data']['deliveredMailsCount'];
                $retVal.= '<br />' . $LANG->getLL('notSentMailsCount') . ': ' . $newsletter['tx_inxmailcontent_data']['notSentMailsCount'];
                $retVal.= '<br />' . $LANG->getLL('sentErrorCount') . ': ' . $newsletter['tx_inxmailcontent_data']['sentErrorCount'];
            }
            else {
                $retVal.= $newsletter['tx_inxmailcontent_data']['deliveredMailsCount'] . ' ' . $LANG->getLL('sent') . ' / &empty; ' . round($newsletter['tx_inxmailcontent_data']['averageMailSize'] / 1024, 1) . ' KB';
            }
        }
        else {

            $retVal.= '-';
        }
        return $retVal;
    }

    /**
     * show the newsletter record
     *
     * @param   array         $newsletter
     * @return  string
     */
    private function _renderNewsletter($newsletter) {
        global $LANG, $BACK_PATH;

        if (!is_array($newsletter) || !array_key_exists('uid', $newsletter) || 1 > $newsletter['uid']) {
            $theOutput.= $this->doc->section($LANG->getLL('error'),$LANG->getLL('noNewsletterFound'),0,1);
        } else {
            $layout = array (
                'table'  => array('<table border="0" cellpadding="3" cellspacing="1">', '</table>'),
                'defRow' => array(
                    'tr'     => array('<tr class="tr-normal">', '</tr>'),
                    'defCol' => array('<td style="padding:3px;vertical-align:top;">', '</td>'),
                    0        => array('<td style="padding:3px;vertical-align:top;text-align:right;width:50px;">', '</td>'),
                ),
            );
            $newsletterLists = $this->_inxmail->getLists();

            $table[] = array($LANG->getLL('listTitle.name') . ':', '<span style="font-weight:bold;">' . $newsletter['title'] . '</span>');
            $table[] = array($LANG->getLL('listTitle.subject') . ':', $newsletter['subtitle']);
            $table[] = array($LANG->getLL('listTitle.list') . ':', $newsletterLists[(int)$newsletter['tx_inxmailcontent_list']]['name']);
            $table[] = array($LANG->getLL('listTitle.datetime') . ':', ($newsletter['tx_inxmailcontent_data']['lastTransfer'] > 0 ? date($GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'] . ', ' . $GLOBALS['TYPO3_CONF_VARS']['SYS']['hhmm'], $newsletter['tx_inxmailcontent_data']['lastTransfer']) : $LANG->getLL('notTransferred')));
            $table[] = array($LANG->getLL('listTitle.status') . ':', $this->_getStatus($newsletter));
            $table[] = array($LANG->getLL('listTitle.sendinginfo') . ':', $this->_getInformation($newsletter, true));

            $theOutput .= $this->doc->table($table, $layout);
        }
        $theOutput.= $this->doc->spacer(20);
        $theOutput.= $this->doc->section($LANG->getLL('nl_create').t3lib_BEfunc::cshItem($this->cshTable,'create_newsletter',$BACK_PATH),'<a href="#" onClick="'.t3lib_BEfunc::editOnClick('&edit[pages]['.$this->id.']=new&edit[tt_content][prev]=new',$BACK_PATH,'').'"><b>'.$LANG->getLL('nl_create_msg1').'</b></a>', 1, 1, 0, TRUE);
        return $theOutput;
    }

    /**
     * show the list of existing records
     *
     * @return  string
     */
    private function _renderNewsletters($listId = 0) {
        global $LANG, $TYPO3_DB, $BACK_PATH;

        if ($listId > 10) {
            $listId = $listId - 10;
        }
        else {
            $listId = 0;
        }

        $res = $TYPO3_DB->exec_SELECTquery(
            'uid,doktype,title,subtitle,abstract,hidden,crdate,tstamp,tx_inxmailcontent_type,tx_inxmailcontent_list,tx_inxmailcontent_salutation,tx_inxmailcontent_template,tx_inxmailcontent_mailing_id,tx_inxmailcontent_data',
            'pages',
            'pid = '.intval($this->id). ' AND doktype = 47'. ' AND ' . $this->perms_clause . t3lib_BEfunc::deleteClause('pages') . ($listId > 0 ? ' AND tx_inxmailcontent_list = ' . (int)$listId : ''),
            null,
            'crdate DESC, title ASC'
        );

        $layout = array (
            'table'  => array('<table class="typo3-dblist" border="0" cellpadding="3" cellspacing="1">', '</table>'),
            'defRow' => array(
                'tr'     => array('<tr class="tr-normal">', '</tr>'),
                'defCol' => array('<td style="padding-left:3px;vertical-align:top;">', '</td>'),
                0        => array('<td style="vertical-align:top;width:50px;white-space:nowrap;">', '</td>'),
                1        => array('<td style="padding-left:3px;vertical-align:top;width:300px;">', '</td>'),
            ),
        );
        $layout[0]['tr']= array('<tr class="c-headLineTable bgColor2" style="font-weight:bold;">','</tr>');

        $table      = array();
        $table[0][] = '<span>' . $LANG->getLL('listTitle.actions') . '</span>';
        $table[0][] = '<span>' . $LANG->getLL('listTitle.name') . '</span>';
        if ($listId == 0) {
            $table[0][] = '<span>' . $LANG->getLL('listTitle.list') . '</span>';
            $newsletterLists = $this->_inxmail->getLists();
        }
        $table[0][] = '<span>' . $LANG->getLL('listTitle.datetime') . '</span>';
        $table[0][] = '<span>' . $LANG->getLL('listTitle.status') . '</span>';
        $table[0][] = '<span>' . $LANG->getLL('listTitle.sendinginfo') . '</span>';


        if (!$TYPO3_DB->sql_num_rows($res)) {
            $theOutput.= $this->doc->section($LANG->getLL('error'),$LANG->getLL('noNewsletterFound'),0,1);
        } else {
            $outLines = array();
            while($row = $TYPO3_DB->sql_fetch_assoc($res))  {

                $row['tx_inxmailcontent_data'] = unserialize($row['tx_inxmailcontent_data']);
                if (!is_array($row['tx_inxmailcontent_data'])) {
                    $row['tx_inxmailcontent_data'] = array();
                }

                $record   = array();
                $record[] = '<a href="#" onClick="'.t3lib_BEfunc::editOnClick('&edit[pages]['.$row['uid'].']=edit&edit_content=1',$BACK_PATH,"",1).'"><img'.t3lib_iconWorks::skinImg('../../../../', t3lib_extMgm::siteRelPath('inxmail_content') . 'res/img/pencil.png', 'width="16" height="16"').' alt="'.$LANG->getLL("edit").'" style="vertical-align:top;" title="'.$LANG->getLL("edit").'" /></a>'.
                            '<a href="#" onClick="'.t3lib_BEfunc::viewOnClick($row['uid'],$BACK_PATH,'','','','').'"><img'.t3lib_iconWorks::skinImg('../../../../', t3lib_extMgm::siteRelPath('inxmail_content') . 'res/img/html.png', 'width="16" height="16"').' alt="'.$LANG->getLL('htmlpreview').'" style="vertical-align:top;margin-left:2px;" title="'.$LANG->getLL('htmlpreview').'"/></a>'.
                            '<a href="#" onClick="'.t3lib_BEfunc::viewOnClick($row['uid'],$BACK_PATH,'','','','&type=500').'"><img'.t3lib_iconWorks::skinImg('../../../../', t3lib_extMgm::siteRelPath('inxmail_content') . 'res/img/text.png', 'width="16" height="16"').' alt="'.$LANG->getLL('textpreview').'" style="vertical-align:top;margin-left:2px;" title="'.$LANG->getLL('textpreview').'"/></a>'.
                            ($row['tx_inxmailcontent_data']['status'] != Inx_Api_Mailing_Mailing::STATE_SENT && $row['tx_inxmailcontent_data']['status'] != Inx_Api_Mailing_Mailing::STATE_SENDING ? '<a href="index.php?id='.$this->id.'&sendrecord='.$row['uid'].'"><img'.t3lib_iconWorks::skinImg('../../../../', t3lib_extMgm::siteRelPath('inxmail_content') . 'res/img/transmit.png', 'width="16" height="16"').' alt="'.$LANG->getLL('sendtoinxmail').'" style="vertical-align:top;margin-left:2px;" title="'.$LANG->getLL('sendtoinxmail').'"/></a>' : '').
                            ($row['tx_inxmailcontent_mailing_id'] > 0 ? '<a href="index.php?id='.$this->id.'&updaterecord='.$row['uid'].'"><img'.t3lib_iconWorks::skinImg('../../../../', t3lib_extMgm::siteRelPath('inxmail_content') . 'res/img/update.png', 'width="16" height="16"').' alt="'.$LANG->getLL('newsletterUpdate').'" style="vertical-align:top;margin-left:2px;" title="'.$LANG->getLL('newsletterUpdate').'"/></a>' : '').
                            ($row['tx_inxmailcontent_data']['status'] != Inx_Api_Mailing_Mailing::STATE_SENT && $row['tx_inxmailcontent_data']['status'] != Inx_Api_Mailing_Mailing::STATE_SENDING && $row['tx_inxmailcontent_mailing_id'] > 0 ? '<a href="index.php?id='.$this->id.'&schedulerecord='.$row['uid'].'"><img'.t3lib_iconWorks::skinImg('../../../../', t3lib_extMgm::siteRelPath('inxmail_content') . 'res/img/calendar.png', 'width="16" height="16"').' alt="'.$LANG->getLL('newsletterSchedule').'" style="vertical-align:top;margin-left:2px;" title="'.$LANG->getLL('newsletterSchedule').'"/></a>' : '').
                            '';
                $record[] = '<a href="db_layout.php?id='.$row['uid'].'&returnUrl=index.php?id='.$this->id.'">'.t3lib_iconWorks::getIconImage('pages', $row, $BACK_PATH, ' title="'.htmlspecialchars(t3lib_BEfunc::getRecordPath ($row['uid'],$this->perms_clause,20)).'"').'&nbsp;<strong>'.$row['title'].'</strong></a>' . '<br />' . $LANG->getLL('listTitle.subject') . ': ' . $row['subtitle'];
                if ($listId == 0) {
                    $record[] = $newsletterLists[(int)$row['tx_inxmailcontent_list']]['name'];
                }
                $record[] = ($row['tx_inxmailcontent_data']['lastTransfer'] > 0 ? date($GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'] . ', ' . $GLOBALS['TYPO3_CONF_VARS']['SYS']['hhmm'], $row['tx_inxmailcontent_data']['lastTransfer']) : $LANG->getLL('notTransferred'));
                $record[] = $this->_getStatus($row);
                $record[] = $this->_getInformation($row);

                $table[]  = $record;
            }
            $theOutput.=$this->doc->table($table, $layout);
        }
        $theOutput.= $this->doc->spacer(20);
        $theOutput.= $this->doc->section($LANG->getLL('nl_create').t3lib_BEfunc::cshItem($this->cshTable,'create_newsletter',$BACK_PATH),'<a href="#" onClick="'.t3lib_BEfunc::editOnClick('&edit[pages]['.$this->id.']=new&edit[tt_content][prev]=new',$BACK_PATH,'').'"><b>'.$LANG->getLL('nl_create_msg1').'</b></a>', 1, 1, 0, TRUE);
        return $theOutput;
    }

    /**
     * Get subtree from given page id (node) as flat id array
     *
     * @param   int $node   the node (page id) to start with, leave blank to start from ROOT
     * @param   int $depth  maximum number of levels to go downwards, default is 999
     * @param   int $dtMin  lowest doktype to accept as page, default is 0
     * @param   int $dtMax  highest doktype to accept as page, default is 255
     * @param   bool $incC  includes active node in tree result, otherwise its childs only. default is TRUE.
     *
     * @return  mixed           array of page id's (int) or boolean FALSE on error
     * @access  public
     */
    private function _getFlatTreeFromNode( $node = 0, $depth = 999, $dtMin = 0, $dtMax = 255, $incC = true ) {

        require_once ( PATH_t3lib . 'class.t3lib_pagetree.php' );

        // Check for class
        if ( !class_exists ( 't3lib_pageTree' ) ) {
            return false;
        }

        // Get TreeView Instance
        if ( !is_object ( $this->_treeView ) ) {
            if ( !is_object ( $this->_treeView = t3lib_div::makeInstance ( 't3lib_pageTree' ) ) ) {
                return false;
            }
        }

        // Prepare Result, include active node
        $res = ( $incC ) ? array( $node ) : array();

        // Configure Tree
        $this->_treeView->init();
        $this->_treeView->tree = array();
        $this->_treeView->makeHTML = false;

        // Read the Tree
        if ( $this->_treeView->getTree( $node , $depth ) > 0 ) {
              #debug ( $this->_treeView->tree );

            // Feed the Result
            foreach ( $this->_treeView->tree as $aChild ) {
                  if ( ( $aChild['row']['uid'] ) && ( $aChild['row']['doktype'] >= $dtMin ) && ( $aChild['row']['doktype'] <= $dtMax ) ) {
                      $res[] = $aChild['row']['uid'];
                }
            }

            // Uniquen Result
            $res = array_unique ( $res );
        }

        // Return Result
        return $res;
    }

    /**
     * Returns all existing news
     *
     * @param   int     $mode
     * @param   int     $count
     * @return  array
     */
    private function _getExistingNews($mode = 0, $count = 7) {

        $this->pages = $this->_getFlatTreeFromNode((int)$this->startingPoint);

        // create syslangauge expression
        $sLang = array ('-1', (int)t3lib_div::_GP('sys_language_uid'), (int)$this->sys_language);

        $limit = '';
        if ($mode == 1) $limit = '0,' . (int)$count;

        $add_where = '';
        if ($mode == 2) $add_where = ' AND `crdate` > (' . (time() - (60 * 60 * 24 * $count)) .') ';

        // do query
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            '*',
            'tt_news',
            "`pid` IN (" . trim(implode(',', $this->pages), ',') . ") ".
                t3lib_BEfunc::deleteClause('tt_news').
                t3lib_BEfunc::versioningPlaceholderClause('tt_news').
                " AND (`hidden` = '0') ".
                " AND (`starttime` = 0 OR `starttime` >= UNIX_TIMESTAMP(NOW())) ".
                " AND (`endtime` = 0 OR `endtime` <= UNIX_TIMESTAMP(NOW())) ".
                ' AND (`sys_language_uid` IN (' . implode (',', array_unique($sLang)) . ')) '.
                $add_where,
            '',
            'crdate DESC',
            $limit
        );

        $result = array();
        if (is_resource($res)) while ( $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc ( $res ) ) {
            $result[] = $row;
        }
        return $result;
    }


    /**
     * Create the panel of buttons for submitting the form or otherwise perform operations.
     *
     * @return  array   all available buttons as an assoc. array
     */
    protected function getButtons() {

        $buttons = array(
            'csh'      => '',
            'shortcut' => '',
            'save'     => ''
        );

        // CSH
        $buttons['csh']  = t3lib_BEfunc::cshItem('_MOD_web_func', '', $GLOBALS['BACK_PATH']);

        // SAVE button
        $buttons['save'] = '<input type="image" class="c-inputButton" name="submit" value="Update"' . t3lib_iconWorks::skinImg($GLOBALS['BACK_PATH'], 'gfx/savedok.gif', '') . ' title="' . $GLOBALS['LANG']->sL('LLL:EXT:lang/locallang_core.php:rm.saveDoc', 1) . '" />';

        // Shortcut
        if ($GLOBALS['BE_USER']->mayMakeShortcut()) {
            $buttons['shortcut'] = $this->doc->makeShortcutIcon('', 'function', $this->MCONF['name']);
        }
        return $buttons;
    }
}


if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/mod_content/index.php']) {
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/mod_content/index.php']);
}

// Make instance:
$SOBE = t3lib_div::makeInstance('tx_inxmailcore_module_content');
/* @var $SOBE tx_inxmailcore_module_content */
$SOBE->init();

// Include files?
foreach($SOBE->include_once as $INC_FILE) {
    include_once($INC_FILE);
}
$SOBE->main();
$SOBE->printContent();

?>