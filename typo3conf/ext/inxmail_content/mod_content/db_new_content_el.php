<?php
/***************************************************************
*  Copyright notice
*
*  (c) 1999-2009 Kasper Skaarhoj (kasperYYYY@typo3.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * New content elements wizard
 * (Part of the 'cms' extension)
 *
 * $Id: db_new_content_el.php 5947 2009-09-16 17:57:09Z ohader $
 * Revised for TYPO3 3.6 November/2003 by Kasper Skaarhoj
 * XHTML compatible.
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 */
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *
 *
 *  101: class ext_posMap extends t3lib_positionMap
 *  111:     function wrapRecordTitle($str,$row)
 *  125:     function onClickInsertRecord($row,$vv,$moveUid,$pid,$sys_lang=0)
 *
 *
 *  153: class SC_db_new_content_el
 *  176:     function init()
 *  212:     function main()
 *  359:     function printContent()
 *
 *              SECTION: OTHER FUNCTIONS:
 *  388:     function getWizardItems()
 *  398:     function wizardArray()
 *  549:     function removeInvalidElements(&$wizardItems)
 *
 * TOTAL FUNCTIONS: 8
 * (This index is automatically created/updated by the extension "extdeveval")
 *
 */


unset($MCONF);
require('conf.php');
require($BACK_PATH.'init.php');
require($BACK_PATH.'template.php');

    // Unset MCONF/MLANG since all we wanted was back path etc. for this particular script.
unset($MCONF);
unset($MLANG);

    // Merging locallang files/arrays:
$LANG->includeLLFile('EXT:lang/locallang_misc.xml');
$LOCAL_LANG_orig = $LOCAL_LANG;
$LANG->includeLLFile('EXT:cms/layout/locallang_db_new_content_el.xml');
$LANG->includeLLFile("EXT:inxmail_content/mod_content/locallang.xml");
$LOCAL_LANG = t3lib_div::array_merge_recursive_overrule($LOCAL_LANG_orig,$LOCAL_LANG);

    // Exits if 'cms' extension is not loaded:
t3lib_extMgm::isLoaded('cms',1);







/**
 * Local position map class
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 * @package TYPO3
 * @subpackage core
 */
class ext_posMap extends t3lib_positionMap {
    var $dontPrintPageInsertIcons = 1;

    /**
     * Wrapping the title of the record - here we just return it.
     *
     * @param	string		The title value.
     * @param	array		The record row.
     * @return	string		Wrapped title string.
     */
    function wrapRecordTitle($str,$row)	{
        return $str;
    }

    /**
     * Create on-click event value.
     *
     * @param	array		The record.
     * @param	string		Column position value.
     * @param	integer		Move uid
     * @param	integer		PID value.
     * @param	integer		System language
     * @return	string
     */
    function onClickInsertRecord($row,$vv,$moveUid,$pid,$sys_lang=0) {
        $table='tt_content';

        $existing = (int)t3lib_div::_GP('existing');
        if ($existing === 1) {
            $location='db_new_content_el.php?id='.$pid.'&sys_language_uid='.$sys_lang.'&existing=1&takeExisting=1&rowUid='.$row['uid'].'&rowPid='.$row['pid'].'&vv='.$vv.'&moveUid='.$moveUid.'&useUid=\' +document.editForm.selectExistingArticle.value + \'&returnUrl='.rawurlencode($GLOBALS['SOBE']->R_URI);
        }
        else {
            $location=$this->backPath.'alt_doc.php?edit[tt_content]['.(is_array($row)?-$row['uid']:$pid).']=new&defVals[tt_content][colPos]='.$vv.'&defVals[tt_content][sys_language_uid]='.$sys_lang.'&returnUrl='.rawurlencode($GLOBALS['SOBE']->R_URI);
        }


        return 'window.location.href=\''.$location.'\'+document.editForm.defValues.value; return false;';
    }
}







/**
 * Script Class for the New Content element wizard
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 * @package TYPO3
 * @subpackage core
 */
class SC_db_new_content_el {

        // Internal, static (from GPvars):
    var $id;					// Page id
    var $sys_language=0;		// Sys language
    var $R_URI='';				// Return URL.
    var $colPos;				// If set, the content is destined for a specific column.
    var $uid_pid;				//

        // Internal, static:
    var $modTSconfig=array();	// Module TSconfig.

    /**
     * Internal backend template object
     *
     * @var mediumDoc
     */
    var $doc;

        // Internal, dynamic:
    var $include_once = array();	// Includes a list of files to include between init() and main() - see init()
    var $content;					// Used to accumulate the content of the module.
    var $access;					// Access boolean.
    var $config;					// config of the wizard


    /**
     * Constructor, initializing internal variables.
     *
     * @return	void
     */
    function init()	{
        global $BE_USER,$BACK_PATH,$TBE_MODULES_EXT;

            // Setting class files to include:
        if (is_array($TBE_MODULES_EXT['xMOD_db_new_content_el']['addElClasses']))	{
            $this->include_once = array_merge($this->include_once,$TBE_MODULES_EXT['xMOD_db_new_content_el']['addElClasses']);
        }

            // Setting internal vars:
        $this->id = intval(t3lib_div::_GP('id'));
        $this->sys_language = intval(t3lib_div::_GP('sys_language_uid'));
        $this->R_URI = t3lib_div::_GP('returnUrl');
        $this->colPos = t3lib_div::_GP('colPos');
        $this->uid_pid = intval(t3lib_div::_GP('uid_pid'));

        $this->MCONF['name'] = 'xMOD_db_new_content_el';
        $this->modTSconfig = t3lib_BEfunc::getModTSconfig($this->id, 'mod.wizards.newContentElement');

        $config = t3lib_BEfunc::getPagesTSconfig($this->id);
        $this->config = $config['mod.']['wizards.']['newContentElement.'];

            // Starting the document template object:
        $this->doc = t3lib_div::makeInstance('template');
        $this->doc->backPath = $BACK_PATH;
        $this->doc->setModuleTemplate('templates/db_new_content_el.html');
        $this->doc->JScode='';
        $this->doc->JScodeLibArray['dyntabmenu'] = $this->doc->getDynTabMenuJScode();
        $this->doc->form='<form action="" name="editForm"><input type="hidden" name="defValues" value="" />';

            // Setting up the context sensitive menu:
        $this->doc->getContextMenuCode();

            // Getting the current page and receiving access information (used in main())
        $perms_clause = $BE_USER->getPagePermsClause(1);
        $this->pageinfo = t3lib_BEfunc::readPageAccess($this->id,$perms_clause);
        $this->access = is_array($this->pageinfo) ? 1 : 0;
    }

    /**
     * Creating the module output.
     *
     * @return	void
     */
    function main()	{
        global $LANG,$BACK_PATH;


        if ($this->id && $this->access)	{

                // Init position map object:
            $posMap = t3lib_div::makeInstance('ext_posMap');
            $posMap->cur_sys_language = $this->sys_language;
            $posMap->backPath = $BACK_PATH;

            if ((string)$this->colPos!='')	{	// If a column is pre-set:
                if ($this->uid_pid<0)	{
                    $row=array();
                    $row['uid']=abs($this->uid_pid);
                } else {
                    $row='';
                }
                $this->onClickEvent = $posMap->onClickInsertRecord($row, $this->colPos, '', $this->uid_pid, $this->sys_language);
            } else {
                $this->onClickEvent = '';
            }


            // ***************************
            // Creating content
            // ***************************
                // use a wrapper div
            $this->content .= '<div id="user-setup-wrapper">';

            $existing = (int)t3lib_div::_GP('existing');
            if ($existing === 1) {
                $this->content.=$this->doc->header($LANG->getLL('addExistingNewsletterElement'));
            }
            else {
                $this->content.=$this->doc->header($LANG->getLL('newContentElement'));
            }

            $this->content.=$this->doc->spacer(5);

            if ($existing !== 1) {

                    // Wizard
                $code='';
                $wizardItems = $this->getWizardItems();

                    // Wrapper for wizards
                $this->elementWrapper['sectionHeader'] = array('<h3 class="bgColor5">', '</h3>');
                $this->elementWrapper['section'] = array('<table border="0" cellpadding="1" cellspacing="2">', '</table>');
                $this->elementWrapper['wizard'] = array('<tr>', '</tr>');
                $this->elementWrapper['wizardPart'] = array('<td>', '</td>');
                    // copy wrapper for tabs
                $this->elementWrapperForTabs = $this->elementWrapper;

                    // Hook for manipulating wizardItems, wrapper, onClickEvent etc.
                if(is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook'])) {
                    foreach($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook'] as $classData) {
                        $hookObject = t3lib_div::getUserObj($classData);

                        if(!($hookObject instanceof cms_newContentElementWizardsHook)) {
                            throw new UnexpectedValueException('$hookObject must implement interface cms_newContentElementWizardItemsHook', 1227834741);
                        }

                        $hookObject->manipulateWizardItems($wizardItems, $this);
                    }
                }

                if ($this->config['renderMode'] == 'tabs' && $this->elementWrapperForTabs != $this->elementWrapper) {
                        // restore wrapper for tabs if they are overwritten in hook
                    $this->elementWrapper = $this->elementWrapperForTabs;
                }

                    // add document inline javascript
                $this->doc->JScode = $this->doc->wrapScriptTags('
                    function goToalt_doc()  {   //
                        ' . $this->onClickEvent . '
                    }

                    if(top.refreshMenu) {
                        top.refreshMenu();
                    } else {
                        top.TYPO3ModuleMenu.refreshMenu();
                    }

                    if(top.shortcutFrame) {
                        top.shortcutFrame.refreshShortcuts();
                    }
                ');

                    // Traverse items for the wizard.
                    // An item is either a header or an item rendered with a radio button and title/description and icon:
                $cc = $key = 0;
                $menuItems = array();
                foreach ($wizardItems as $k => $wInfo)	{
                    if ($wInfo['header'])	{
                        $menuItems[] = array(
                                'label'   => htmlspecialchars($wInfo['header']),
                                'content' => $this->elementWrapper['section'][0]
                        );
                        $key = count($menuItems) - 1;
                    } else {
                        $content = '';
                            // Radio button:
                        $oC = "document.editForm.defValues.value=unescape('".rawurlencode($wInfo['params'])."');".(!$this->onClickEvent?"window.location.hash='#sel2';":'');
                        $content .= $this->elementWrapper['wizardPart'][0] .
                            '<input type="radio" name="tempB" value="' . htmlspecialchars($k) . '" onclick="' . htmlspecialchars($this->doc->thisBlur().$oC) . '" />' .
                            $this->elementWrapper['wizardPart'][1];

                            // Onclick action for icon/title:
                        $aOnClick = 'document.getElementsByName(\'tempB\')['.$cc.'].checked=1;'.$this->doc->thisBlur().$oC.'return false;';

                            // Icon:
                        $iInfo = @getimagesize($wInfo['icon']);
                        $content .= $this->elementWrapper['wizardPart'][0] .
                            '<a href="#" onclick="' . htmlspecialchars($aOnClick) . '">
                            <img' . t3lib_iconWorks::skinImg($this->doc->backPath, $wInfo['icon'], '') . ' alt="" /></a>' .
                            $this->elementWrapper['wizardPart'][1];

                            // Title + description:
                        $content .= $this->elementWrapper['wizardPart'][0] .
                            '<a href="#" onclick="' . htmlspecialchars($aOnClick) . '"><strong>' . htmlspecialchars($wInfo['title']) . '</strong><br />' .
                            nl2br(htmlspecialchars(trim($wInfo['description']))) . '</a>' .
                            $this->elementWrapper['wizardPart'][1];

                            // Finally, put it together in a container:
                        $menuItems[$key]['content'] .= $this->elementWrapper['wizard'][0] . $content . $this->elementWrapper['wizard'][1];
                        $cc++;
                    }
                }
                    // add closing section-tag
                foreach ($menuItems as $key => $val) {
                    $menuItems[$key]['content'] .=  $this->elementWrapper['section'][1];
                }

                    // Add the wizard table to the content, wrapped in tabs:
                if ($this->config['renderMode'] == 'tabs') {
                    $this->doc->inDocStylesArray[] = '
                        .typo3-dyntabmenu-divs { background-color: #fafafa; border: 1px solid #000; width: 680px; }
                        .typo3-dyntabmenu-divs table { margin: 15px; }
                        .typo3-dyntabmenu-divs table td { padding: 3px; }
                    ';
                    $code = $LANG->getLL('sel1',1) . '<br /><br />' . $this->doc->getDynTabMenu($menuItems, 'new-content-element-wizard', false, false, 100);
                } else {
                    $code = $LANG->getLL('sel1',1) . '<br /><br />';
                    foreach ($menuItems as $section) {
                        $code .= $this->elementWrapper['sectionHeader'][0] . $section['label'] . $this->elementWrapper['sectionHeader'][1] . $section['content'];
                    }
                }

                $this->content.= $this->doc->section(!$this->onClickEvent ? $LANG->getLL('1_selectType') : '', $code, 0, 1);

            }
            else {

                $takeExisting = (int)t3lib_div::_GP('takeExisting');
                if ($takeExisting === 1) {
                    $news = $this->_getExistingNewsSingle(t3lib_div::_GP('useUid'));

                    $this->_createNewsletterArticle($news);
                    header('Location: db_layout.php?id=' . $this->id);
                    exit;
                }

                $start        = (int)$this->fetchTS('tx_inxmailcontent', 'newsContainerId', (int)t3lib_div::_GP('id'));
                $startingNode = (int)t3lib_div::_GP('startingNode');
                $parentNode   = (int)t3lib_div::_GP('parentNode');
                if (!($startingNode > 0)) {
                    $startingNode = $start;
                }
                $browseTree = t3lib_div::makeInstance('t3lib_browseTree');
                $browseTree->init();
                $browseTree->backPath = $BACK_PATH;
                $browseTree->reset();
                $browseTree->getTree($startingNode);
                $articles = $this->_getExistingNews($startingNode);

                $cc = 0;
                foreach($articles as $k => $wInfo)    {
                    $tL=array();

                    $oC = "document.editForm.selectExistingArticle.value=document.getElementsByName('selectExistingArticle')[".$cc."].value;".(!$onClickEvent?"window.location.hash='#sel2';":'');
                    $tL[]='<input type="radio" name="selectExistingArticle" value="'.htmlspecialchars($wInfo['uid']).'" onclick="'.htmlspecialchars($this->doc->thisBlur().$oC).'" /> ';
                    $aOnClick = 'document.getElementsByName(\'selectExistingArticle\')['.$cc.'].checked=1;'.$this->doc->thisBlur().$oC.'return false;';

                    $tL[]='<a href="#" onclick="'.htmlspecialchars($aOnClick).'">'.t3lib_iconWorks::getIconImage('tt_news', $wInfo, $BACK_PATH, ' title="'.$wInfo['uid'].' @ '.htmlspecialchars(t3lib_BEfunc::getRecordPath ($wInfo['pid'],$this->perms_clause,20)).'" style="margin-bottom:-4px;"').' ';
                    $tL[]='<strong>'.htmlspecialchars($wInfo['title']).'</strong><br /><span style="margin-left:35px;">'.date('d.m.Y, H:i:s', $wInfo['datetime']).(!empty($wInfo['author']) ? ', ' . $wInfo['author'] : '').'</span></a>';

                    $lines[]='
                        <tr>
                            <td valign="top">'.implode('',$tL).'</td>
                        </tr>';
                    $cc++;
                }

                $currentLoc = 'db_new_content_el.php?id='.$this->id.'&sys_language_uid='.$this->sys_language.'&existing=1&returnUrl='.rawurlencode($GLOBALS['SOBE']->R_URI);
                $code.='Webseitenstruktur:<ul style="margin:10px 0;padding:0;list-style:none;font-size:1.1em;">';
                if ($startingNode != $start) $code.= '<li style="list-style:none;margin-bottom:3px;"><a href="'.$currentLoc.'&startingNode='.$parentNode.'">../ (' . $LANG->getLL('toTop') . ')</a></li>';

                foreach ($browseTree->tree as $treePart) {
                    $code.= '<li style="list-style:none;margin-bottom:3px;"><img'.t3lib_iconWorks::skinImg('../../../../', t3lib_extMgm::siteRelPath('tt_news') . 'res/gfx/ext_icon_ttnews_folder.gif', 'width="18" height="16"').' alt="" style="margin-bottom:-4px;" title="" />&nbsp;<a href="'.$currentLoc.'&parentNode='.$startingNode.'&startingNode='.$treePart['row']['uid'].'">'.$treePart['row']['title']."</a></li>";
                }
                $code.='</ul>';

                    $code.='
                    <!--
                        Content Element wizard table:
                    -->
                        <table border="0" cellpadding="1" cellspacing="2" id="typo3-ceWizardTable">
                            '.implode('',$lines).'
                        </table>';

                $this->content.= $this->doc->section(!$this->onClickEvent ? $LANG->getLL('1_selectType_ttnews') : '', $code, 0, 1);
            }


                // Add anchor "sel2"
            $this->content.= $this->doc->section('','<a name="sel2"></a>');
            $this->content.= $this->doc->spacer(20);

                // Select position
            $code = $LANG->getLL('sel2',1).'<br /><br />';

                // Load SHARED page-TSconfig settings and retrieve column list from there, if applicable:
            $modTSconfig_SHARED = t3lib_BEfunc::getModTSconfig($this->id,'mod.SHARED');
            $colPosList = strcmp(trim($modTSconfig_SHARED['properties']['colPos_list']),'') ? trim($modTSconfig_SHARED['properties']['colPos_list']) : '1,0,2,3';
            $colPosList = implode(',',array_unique(t3lib_div::intExplode(',',$colPosList)));		// Removing duplicates, if any

                // Finally, add the content of the column selector to the content:
            $code.= $posMap->printContentElementColumns($this->id,0,$colPosList,1,$this->R_URI);
            $this->content.= $this->doc->section($LANG->getLL('2_selectPosition'),$code,0,1);
        } else {		// In case of no access:
            $this->content = '';
            $this->content.= $this->doc->header($LANG->getLL('newContentElement'));
            $this->content.= $this->doc->spacer(5);
        }

            // Setting up the buttons and markers for docheader
        $docHeaderButtons = $this->getButtons();
        $markers['CSH'] = $docHeaderButtons['csh'];
        $markers['CONTENT'] = $this->content;

            // Build the <body> for the module
        $this->content = $this->doc->startPage($LANG->getLL('newContentElement'));
        $this->content.= $this->doc->moduleBody($this->pageinfo, $docHeaderButtons, $markers);
        $this->content .= $this->doc->sectionEnd();
        $this->content.= $this->doc->endPage();
        $this->content = $this->doc->insertStylesAndJS($this->content);
    }

    /**
     * Print out the accumulated content:
     *
     * @return	void
     */
    function printContent()	{
        echo $this->content;
    }

    /**
     * Create the panel of buttons for submitting the form or otherwise perform operations.
     *
     * @return	array	all available buttons as an assoc. array
     */
    protected function getButtons()	{
        global $LANG, $BACK_PATH;

        $buttons = array(
            'csh' => '',
            'back' => ''
        );

        if ($this->id && $this->access)	{
                // CSH
            $buttons['csh'] = t3lib_BEfunc::cshItem('xMOD_csh_corebe', 'new_ce', $GLOBALS['BACK_PATH'], '', TRUE);

                // Back
            if ($this->R_URI)	{
                $buttons['back'] = '<a href="' . htmlspecialchars($this->R_URI) . '" class="typo3-goBack">' .
                    '<img' . t3lib_iconWorks::skinImg($this->doc->backPath, 'gfx/goback.gif') . ' alt="" title="' . $LANG->getLL('goBack', 1) . '" />' .
                    '</a>';
            }
        }
        return $buttons;
    }





    /***************************
     *
     * OTHER FUNCTIONS:
     *
     ***************************/


    /**
     * Returns the content of wizardArray() function...
     *
     * @return	array		Returns the content of wizardArray() function...
     */
    function getWizardItems()	{
        return $this->wizardArray();
    }

    /**
     * Returns the array of elements in the wizard display.
     * For the plugin section there is support for adding elements there from a global variable.
     *
     * @return	array
     */
    function wizardArray()  {
        global $LANG,$TBE_MODULES_EXT;

        $wizardItems = array(
            'common' => array('header'=>$LANG->getLL('title')),
            'common_regularText' => array(
                'icon'=>'gfx/c_wiz/regular_text.gif',
                'title'=>$LANG->getLL('commonRegularText'),
                'description'=>$LANG->getLL('commonRegularTextDescription'),
                'tt_content_defValues' => array(
                    'CType' => 'text'
                )
            ),
            'common_inxmailcontent_ctype' => array(
                'icon'=>'gfx/c_wiz/text_image_right.gif',
                'title'=>$LANG->getLL('addNewNewsletterElement'),
                'description'=>$LANG->getLL('addNewNewsletterElementDescription'),
                'tt_content_defValues' => array(
                    'CType' => 'inxmail_content_ctype',
                )
            ),
        );

        // Remove elements where preset values are not allowed:
        $this->removeInvalidElements($wizardItems);

        return $wizardItems;
    }

    function wizard_appendWizards($wizardElements) {
        if (!is_array($wizardElements)) {
            $wizardElements = array();
        }
        if (is_array($GLOBALS['TBE_MODULES_EXT']['xMOD_db_new_content_el']['addElClasses'])) {
            foreach ($GLOBALS['TBE_MODULES_EXT']['xMOD_db_new_content_el']['addElClasses'] as $class => $path) {
                require_once($path);
                $modObj = t3lib_div::makeInstance($class);
                $wizardElements = $modObj->proc($wizardElements);
            }
        }
        $returnElements = array();
        foreach ($wizardElements as $key => $wizardItem) {
            preg_match('/^[a-zA-Z0-9]+_/', $key, $group);
            $wizardGroup =  $group[0] ? substr($group[0], 0, -1) . '.' : $key;
            $returnElements[$wizardGroup]['elements.'][substr($key, strlen($wizardGroup)) . '.'] = $wizardItem;
        }
        return $returnElements;
    }


    function wizard_getItem($groupKey, $itemKey, $itemConf) {
        $itemConf['title'] = $GLOBALS['LANG']->sL($itemConf['title']);
        $itemConf['description'] = $GLOBALS['LANG']->sL($itemConf['description']);
        $itemConf['tt_content_defValues'] = $itemConf['tt_content_defValues.'];
        unset($itemConf['tt_content_defValues.']);
        return $itemConf;
    }

    function wizard_getGroupHeader($groupKey, $wizardGroup) {
        return array(
            'header' => $GLOBALS['LANG']->sL($wizardGroup['header'])
        );
    }


    /**
     * Checks the array for elements which might contain unallowed default values and will unset them!
     * Looks for the "tt_content_defValues" key in each element and if found it will traverse that array as fieldname / value pairs and check. The values will be added to the "params" key of the array (which should probably be unset or empty by default).
     *
     * @param	array		Wizard items, passed by reference
     * @return	void
     */
    function removeInvalidElements(&$wizardItems)	{
        global $TCA;

            // Load full table definition:
        t3lib_div::loadTCA('tt_content');

            // Get TCEFORM from TSconfig of current page
        $row = array('pid' => $this->id);
        $TCEFORM_TSconfig = t3lib_BEfunc::getTCEFORM_TSconfig('tt_content', $row);
        $removeItems = t3lib_div::trimExplode(',', $TCEFORM_TSconfig['CType']['removeItems'], 1);
        $keepItems = t3lib_div::trimExplode(',', $TCEFORM_TSconfig['CType']['keepItems'], 1);

        $headersUsed = Array();
            // Traverse wizard items:
        foreach($wizardItems as $key => $cfg)	{

                // Exploding parameter string, if any (old style)
            if ($wizardItems[$key]['params'])	{
                    // Explode GET vars recursively
                $tempGetVars = t3lib_div::explodeUrl2Array($wizardItems[$key]['params'],TRUE);
                    // If tt_content values are set, merge them into the tt_content_defValues array, unset them from $tempGetVars and re-implode $tempGetVars into the param string (in case remaining parameters are around).
                if (is_array($tempGetVars['defVals']['tt_content']))	{
                    $wizardItems[$key]['tt_content_defValues'] = array_merge(is_array($wizardItems[$key]['tt_content_defValues']) ? $wizardItems[$key]['tt_content_defValues'] : array(), $tempGetVars['defVals']['tt_content']);
                    unset($tempGetVars['defVals']['tt_content']);
                    $wizardItems[$key]['params'] = t3lib_div::implodeArrayForUrl('',$tempGetVars);
                }
            }

                // If tt_content_defValues are defined...:
            if (is_array($wizardItems[$key]['tt_content_defValues']))	{

                    // Traverse field values:
                foreach($wizardItems[$key]['tt_content_defValues'] as $fN => $fV)	{
                    if (is_array($TCA['tt_content']['columns'][$fN]))	{
                            // Get information about if the field value is OK:
                        $config = &$TCA['tt_content']['columns'][$fN]['config'];
                        $authModeDeny = ($config['type']=='select' && $config['authMode'] && !$GLOBALS['BE_USER']->checkAuthMode('tt_content', $fN, $fV, $config['authMode']));
                        $isNotInKeepItems = (count($keepItems) && !in_array($fV, $keepItems));

                        if ($authModeDeny || ($fN=='CType' && in_array($fV,$removeItems)) || $isNotInKeepItems) {
                                // Remove element all together:
                            unset($wizardItems[$key]);
                            break;
                        } else {
                                // Add the parameter:
                            $wizardItems[$key]['params'].= '&defVals[tt_content]['.$fN.']='.rawurlencode($fV);
                            $tmp = explode('_', $key);
                            $headersUsed[$tmp[0]] = $tmp[0];
                        }
                    }
                }
            }
        }
            // remove headers without elements
        foreach ($wizardItems as $key => $cfg)	{
            $tmp = explode('_',$key);
            if ($tmp[0] && !$tmp[1] && !in_array($tmp[0], $headersUsed))	{
                unset($wizardItems[$key]);
            }
        }
    }


    /***************************
     *
     * TT_NEWS IMPORT FUNCTIONS:
     *
     ***************************/


    /**
     * helper method to fetch the ts config
     *
     * @param   string $extension
     * @param   mixed $value
     * @param   int $pageId
     */
    function fetchTS($extension, $value, $pageId = 1) {
        $sysPageObj = t3lib_div::makeInstance('t3lib_pageSelect');
        $rootLine   = $sysPageObj->getRootLine($pageId);
        $TSObj      = t3lib_div::makeInstance('t3lib_tsparser_ext');

        $TSObj->tt_track = 0;
        $TSObj->init();
        $TSObj->runThroughTemplates($rootLine);
        $TSObj->generateConfig();
        return $TSObj->setup['plugin.'][$extension . '.'][$value];
    }


    /**
     * Get subtree from given page id (node) as flat id array
     *
     * @param   int $node   the node (page id) to start with, leave blank to start from ROOT
     * @param   int $depth  maximum number of levels to go downwards, default is 999
     * @param   int $dtMin  lowest doktype to accept as page, default is 0
     * @param   int $dtMax  highest doktype to accept as page, default is 255
     * @param   bool $incC  includes active node in tree result, otherwise its childs only. default is TRUE.
     *
     * @return  mixed           array of page id's (int) or boolean FALSE on error
     * @access  public
     */
    private function _getFlatTreeFromNode ( $node = 0, $depth = 999, $dtMin = 0, $dtMax = 255, $incC = true ) {

        require_once ( PATH_t3lib . 'class.t3lib_pagetree.php' );

        // Check for class
        if ( !class_exists ( 't3lib_pageTree' ) ) {
            return false;
        }

        // Get TreeView Instance
        if ( !is_object ( $this->_treeView ) ) {
            if ( !is_object ( $this->_treeView = t3lib_div::makeInstance ( 't3lib_pageTree' ) ) ) {
                return false;
            }
        }

        // Prepare Result, include active node
        $res = ( $incC ) ? array( $node ) : array();

        // Configure Tree
        $this->_treeView->init();
        $this->_treeView->tree = array();
        $this->_treeView->makeHTML = false;

        // Read the Tree
        if ( $this->_treeView->getTree( $node , $depth ) > 0 ) {
              #debug ( $this->_treeView->tree );

            // Feed the Result
            foreach ( $this->_treeView->tree as $aChild ) {
                  if ( ( $aChild['row']['uid'] ) && ( $aChild['row']['doktype'] >= $dtMin ) && ( $aChild['row']['doktype'] <= $dtMax ) ) {
                      $res[] = $aChild['row']['uid'];
                }
            }

            // Uniquen Result
            $res = array_unique ( $res );
        }

        // Return Result
        return $res;
    }


    /**
     * Returns all existing articles
     *
     * @param integer $statingPoint The starting point of the article tree
     * @return array    Returns the exitsting articles
     */
    private function _getExistingNews($startingPoint) {

        $this->pages = $this->_getFlatTreeFromNode((int)$startingPoint);

        // create syslangauge expression
        $sLang = array ('-1', t3lib_div::_GP('sys_language_uid'), (int)$this->sys_language);

        // do query
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            '*',
            'tt_news',
            "`pid` IN (" . trim(implode(',', $this->pages), ',') . ") ".
                t3lib_BEfunc::deleteClause('tt_news').
                t3lib_BEfunc::versioningPlaceholderClause('tt_news').
                " AND (`hidden` = '0') ".
                " AND (`starttime` = 0 OR `starttime` >= UNIX_TIMESTAMP(NOW())) ".
                " AND (`endtime` = 0 OR `endtime` <= UNIX_TIMESTAMP(NOW())) ".
                ' AND (`sys_language_uid` IN (' . implode (',', array_unique($sLang)) . ')) '.
                '',
            '',
            'crdate DESC',
            '0,50'
        );

        $result = array();
        if (is_resource($res)) while ( $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc ( $res ) ) {
            $result[] = $row;
        }
        return $result;
    }

    /**
     * gets an existing article
     *
     * @param interger $uid
     * @return array The array containing the existing article information
     */
    private function _getExistingNewsSingle($uid) {
        $WHERE = "(`uid` = $uid) ";

        // create syslangauge expression
        $sLang = array ('-1', t3lib_div::_GP('sys_language_uid'), (int)$this->sys_language);

        // do query
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            '*',
            'tt_news',
            "`uid` = ". (int)$uid . " " .
                t3lib_BEfunc::deleteClause('tt_news').
                t3lib_BEfunc::versioningPlaceholderClause('tt_news').
                " AND (`hidden` = '0') ".
                " AND (`starttime` = 0 OR `starttime` >= UNIX_TIMESTAMP(NOW())) ".
                " AND (`endtime` = 0 OR `endtime` <= UNIX_TIMESTAMP(NOW())) ".
                ' AND (`sys_language_uid` IN (' . implode (',', array_unique($sLang)) . ')) '.
                '',
            '',
            'crdate DESC'
        );

        $result = array();
        if (is_resource($res)) {
            $result = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
        }
        return $result;
    }

    /**
     * creates a new newsletter article by using an existing article
     *
     * @param array $existingArticle The array containing the existing article information
     * @return interger The new record id
     */
    private function _createNewsletterArticle($newsEntry) {

        $sorting = (int)t3lib_div::_GP('id');
        $rowUid  = (int)t3lib_div::_GP('rowUid');

        if (is_numeric($rowUid) && $rowUid > 0) {
            $sorting = -$rowUid;
        }

        $data = array();
        $data['tt_content']['NEWbe68s587'] = array(
            "header"    => $newsEntry['title'],
            "bodytext"  => strip_tags($newsEntry['bodytext'], '<a><b><i><u><strong><em><link>'),
            "image"     => $newsEntry['image'],
            "CType"     => 'inxmail_content_ctype',
            "pid"       => (int)t3lib_div::_GP('id'),
            "hidden"    => 0,
            "sorting"   => $sorting,
        );
        require_once (PATH_t3lib."class.t3lib_tcemain.php");
        $t3lib_TCEmain = t3lib_div::makeInstance('t3lib_TCEmain');
        $t3lib_TCEmain->stripslashes_values = 0;
        $t3lib_TCEmain->start($data,array());
        $t3lib_TCEmain->process_datamap();

        $newPid = $t3lib_TCEmain->substNEWwithIDs['NEWbe68s587'];
    }
}


if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/mod_content/db_new_content_el.php'])	{
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/mod_content/db_new_content_el.php']);
}

// Make instance:
$SOBE = t3lib_div::makeInstance('SC_db_new_content_el');
$SOBE->init();

// Include files?
foreach($SOBE->include_once as $INC_FILE)	include_once($INC_FILE);

$SOBE->main();
$SOBE->printContent();

?>