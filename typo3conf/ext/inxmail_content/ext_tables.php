<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

include_once t3lib_extMgm::extPath($_EXTKEY) . 'classes/class.tx_inxmailcontent_itemFunctions.php';

if (TYPO3_MODE == 'BE') {

    // config module
    t3lib_extMgm::addModulePath ('txinxmailcoreMainModule_txinxmailcoreContentModule', t3lib_extMgm::extPath($_EXTKEY) . 'mod_content/');
    t3lib_extMgm::addModule     ('txinxmailcoreMainModule', 'txinxmailcoreContentModule', '', t3lib_extMgm::extPath($_EXTKEY) . 'mod_content/');
}

// pages modified
t3lib_div::loadTCA('pages');
$TCA['pages']['columns']['module']['config']['items'][] = array('LLL:EXT:' . $_EXTKEY . '/mod_content/locallang.xml:sys_inxmail_newsletter', 'inxnl');

// content modified
t3lib_div::loadTCA('tt_content');
$TCA['tt_content']['types'][$_EXTKEY.'_ctype']['showitem']='
    CType;;4;button;1-1-1,
    header;;3;;2-2-2,
    bodytext;;;richtext[bold|italic|underline]:rte_transform[mode=ts];3-3-3,
    --div--;LLL:EXT:inxmail_content/locallang_db.xml:image,
        image;;;;4-4-4,
        altText;;;;1-1-1,
        titleText
';
t3lib_extMgm::addPlugin(Array('LLL:EXT:' . $_EXTKEY . '/locallang_db.xml:tt_content.CType_ctype', $_EXTKEY.'_ctype'),'CType', 1);

t3lib_extMgm::addStaticFile($_EXTKEY, 'static/', 'Inxmail Professional Content');
//t3lib_extMgm::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/res/pageTSConfig.txt">');
//<INCLUDE_TYPOSCRIPT: source="FILE:EXT:inxmail_content/res/pageTSConfig.txt">

// Add new page type in correct position
$position = 2;
array_splice($TCA['pages']['columns']['doktype']['config']['items'],
             $position,
             0,
             array(
                 $position => array(
                     '0' => "LLL:EXT:".$_EXTKEY."/locallang_db.xml:doktype.I.47",
                     '1' => 47,
                 )
             ));

$TYPO3_CONF_VARS['FE']['content_doktypes'].= ',47';
$PAGES_TYPES[47] = array(
    'type'          => 'web',
    'icon'          => 'pages.gif',
    'allowedTables' => '*'
);

//            doktype;;2;;1-1-1, hidden, nav_hide, title;;3;;2-2-2, subtitle, nav_title,
//            --div--;LLL:EXT:cms/locallang_tca.xml:pages.tabs.metadata,
//                --palette--;LLL:EXT:lang/locallang_general.xml:LGL.author;5;;3-3-3, abstract, keywords, description,
//            --div--;LLL:EXT:cms/locallang_tca.xml:pages.tabs.files,
//                media,
//            --div--;LLL:EXT:cms/locallang_tca.xml:pages.tabs.options,
//                TSconfig;;6;nowrap;6-6-6, storage_pid;;7, l18n_cfg, module, content_from_pid,
//            --div--;LLL:EXT:cms/locallang_tca.xml:pages.tabs.access,
//                starttime, endtime, fe_login_mode, fe_group, extendToSubpages,
//            --div--;LLL:EXT:cms/locallang_tca.xml:pages.tabs.extended,tx_inxmailcontent_list

$TCA['pages']['types'][47]['showitem'] = '
            doktype;;2;;1-1-1,
            title;LLL:EXT:inxmail_content/locallang_db.xml:pages.title;3;;2-2-2,
            hidden;LLL:EXT:inxmail_content/locallang_db.xml:pages.hidden,
            --div--;LLL:EXT:inxmail_content/locallang_db.xml:div,
                tx_inxmailcontent_mailing_id,
                subtitle;LLL:EXT:inxmail_content/locallang_db.xml:pages.subtitle,
                tx_inxmailcontent_type,
                tx_inxmailcontent_list,
                tx_inxmailcontent_editorial_headline,
                tx_inxmailcontent_salutation,
                tx_inxmailcontent_editorial,
                tx_inxmailcontent_template,
';
$TCA['pages']['ctrl']['requestUpdate'] .= 'tx_inxmailcontent_type';

$tempColumns = array (
    'tx_inxmailcontent_type' => array (
        'exclude' => 1,
        'label'  => 'LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_type',
        'config' => Array (
            'type' => 'select',
            'items' => Array (
                 Array('LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_type.htmltext', 'html/text'),
                 Array('LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_type.html', 'html'),
                 Array('LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_type.text', 'text'),
             ),
            'eval' => 'required,trim',
        ),
    ),
    'tx_inxmailcontent_list' => array (
        'exclude' => 1,
        'label'  => 'LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_list',
        'config'  => Array (
            'type'          => 'select',
            'itemsProcFunc' => 'tx_inxmailcontent_itemFunctions->getListOptions',
            'maxitems'      => 1,
            'size'          => 1,
            'eval'          => 'required',
        ),
    ),
    'tx_inxmailcontent_salutation' => array (
        'exclude' => 1,
        'label'  => 'LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_salutation',
        'config' => array (
            'type' => 'user',
            'userFunc' => 'tx_inxmailcontent_itemFunctions->getSalutations',
        ),
    ),
    'tx_inxmailcontent_mailing_id' => array (
        'exclude' => 1,
        'label'  => 'LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_mailing_id',
        'config' => array (
            'type' => 'input',
            'size' => '50',
            'readOnly' => 1,
            'eval' => 'trim',
        ),
    ),
    'tx_inxmailcontent_template' => Array (
        'exclude' => 1,
        'label' => 'LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_template',
        'config' => Array (
            'type'          => 'group',
            'internal_type' => 'file',
            'maxitems'      => 1,
            'size'          => 1,
            'allowed'       => 'tmpl,tpl,htm,html',
            'uploadfolder'  => '',
        ),
    ),
    'tx_inxmailcontent_editorial_headline' => array (
        'exclude' => 1,
        'label'  => 'LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_editorial_headline',
        'config' => array (
            'type' => 'input',
            'size' => '255',
            'max'  => '255',
            'eval' => 'trim',
        ),
    ),
    'tx_inxmailcontent_editorial' => array (
        'exclude' => 1,
        'label'   => 'LLL:EXT:inxmail_content/locallang_db.xml:pages.tx_inxmailcontent_editorial',
        'config'  => array (
            'type' => 'text',
            'cols' => '50',
            'rows' => '5',
            'eval' => 'trim',
        ),
        'defaultExtras' => 'richtext[]:rte_transform[mode=ts_css]',
    ),
);
t3lib_extMgm::addTCAcolumns('pages', $tempColumns, 1);
t3lib_extMgm::addToAllTCAtypes('pages', 'tx_inxmailcontent_list', '47', 'after:nav_hide');

?>