<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Inxmail GmbH - http://www.inxmail.de
*  (c) 2010 [di] digitale informationssysteme gmbh - http://www.digi-info.de
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * class.tx_inxmailcontent_pi_content.php
 *
 * Plugin for (un)subscription forms
 *
 * $Id:$
 *
 * @author Jens Jacobsen <jacobsen@digi-info.de>
 */

/**
 * This class provides the newsletter templates.
 *
 * @author  Jens Jacobsen <jacobsen@digi-info.de>
 * @package TYPO3
 * @subpackage tx_inxmailcontent
 */
class tx_inxmailcontent_pi_content extends tslib_pibase
{
    /**
     * prefix
     *
     * @var string
     */
    public $prefixId = 'tx_inxmailcontent_pi_content';

    /**
     * relative script path
     *
     * @var string
     */
    public $scriptRelPath = 'pi_content/class.tx_inxmailcontent_pi_content.php';

    /**
     * extension key
     *
     * @var string
     */
    public $extKey = 'inxmail_content';

    /**
     * cached obj
     *
     * @var boolean
     */
    public $pi_USER_INT_obj = true;

    /**
     * check for hash
     *
     * @var boolean
     */
    public $pi_checkCHash = false;

    /**
     * create index
     *
     *
     */
    private function _getTableOfContent() {
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            'uid,header',
            'tt_content',
            "pid = " . (int)$this->cObj->data['uid'] . " AND CType IN ('text','inxmail_content_ctype') AND NOT hidden AND NOT deleted",
            null,
            'sorting'
        );
        $retVal = array();
        if ($GLOBALS['TYPO3_DB']->sql_num_rows($res) > 0) {
            while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))  {
                $retVal[] = $row;
            }
        }
        return $retVal;
    }

    /**
     * substitutes all language markers (###LL:.*###) in the given template code
     *
     * @param   string      $template
     * @return  string
     */
    function substituteLangMarkers($template) {
        global $LANG;
        $langMarkers   = array();
        $aLLMarkerList = array();

        preg_match_all('~###LL:([^#]+)###~Uis', $template, $aLLMarkerList);

        foreach($aLLMarkerList[0] as $key => $LLMarker) {

            $template = str_replace($LLMarker, $this->pi_getLL($aLLMarkerList[1][$key]), $template);
        }
        return $template;
    }

    /**
     * prefix resources helper
     *
     * @param   string      $content
     * @return  string
     */
    private function _prefixTemplateResources($content) {

        $prefix = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_inxmailcontent.']['templateImgPathPrefix'];

        if (empty($prefix)) {
            return $content;
        }

        if (preg_match('~^EXT:~i', $prefix)) {
            $t3lib_tsparser_ext = t3lib_div::makeInstance('t3lib_tsparser_ext');
            /* @var $t3lib_tsparser_ext t3lib_tsparser_ext */
            $prefix = $t3lib_tsparser_ext->ext_detectAndFixExtensionPrefix($prefix);
        }

        $content = preg_replace('~(<img[^>]*src\s*=\s*")([^"]*"[^>]*>)~Uis', '\1' . $prefix . '\2', $content);
        $content = preg_replace('~(url\(("|\')?)([^\)]*("|\')?\))~Uis', '\1' . $prefix . '\3', $content);
        return $content;
    }

    /**
     * The main method of the PlugIn
     *
     * @param   string      $content: The PlugIn content
     * @param   array       $conf: The PlugIn configuration
     * @return  string      The content that is displayed on the website
     */
    public function main($content,$conf) {
        $this->pi_loadLL();
        $typeNum = (int)t3lib_div::_GET('type');

        // html version
        if ($typeNum == 0) {

            if (!array_key_exists('tx_inxmailcontent_template', $this->cObj->data) ||
                empty($this->cObj->data['tx_inxmailcontent_template'])) {


                $lConf   = array(
                               'template' => 'FILE',
                               'template.' => array(
                                   'file' => $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_inxmailcontent.']['templateFile'],
                               ),
                           );
                $content = $this->_prefixTemplateResources(trim($this->cObj->TEMPLATE($lConf)));
            }
            else {

                $lConf   = array(
                               'template' => 'FILE',
                               'template.' => array(
                                   'file' => $this->cObj->data['tx_inxmailcontent_template'],
                               ),
                           );
                $content = $this->_prefixTemplateResources(trim($this->cObj->TEMPLATE($lConf)));
            }
        }
        // text version
        else {

            if (!array_key_exists('tx_inxmailcontent_template', $this->cObj->data) ||
                empty($this->cObj->data['tx_inxmailcontent_template'])) {

                $lConf   = array(
                               'template' => 'FILE',
                               'template.' => array(
                                   'file' => $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_inxmailcontent.']['templateFile'],
                               ),
                           );
                $content = trim($this->cObj->TEMPLATE($lConf));
            }
            else {

                $lConf   = array(
                               'template' => 'FILE',
                               'template.' => array(
                                   'file' => $this->cObj->data['tx_inxmailcontent_template'],
                               ),
                           );
                $content = trim($this->cObj->TEMPLATE($lConf));
            }
        }

        // create toc
        $toc = $this->_getTableOfContent();
        if (is_array($toc) && count($toc)) {

            $tocContent         = '';
            $tocContentTemplate = $this->cObj->getSubpart($content, '###TABLE_OF_CONTENT###');

            if (!empty($tocContentTemplate)) {

                $tocElementTemplate = $this->cObj->getSubpart($tocContentTemplate, '###ELEMENT_TABLE_OF_CONTENT###');

                foreach($toc as $tocElement) {

                    $marker = array(
                        '###ID###'    => (int)$tocElement['uid'],
                        '###TITLE###' => $tocElement['header'],
                    );
                    $tocContent.= $this->cObj->substituteMarkerArray($tocElementTemplate, $marker);
                }
            }
            $content = $this->cObj->substituteSubpart($content, '###TABLE_OF_CONTENT###', $tocContent);
        }

        // add editorial
        require_once PATH_t3lib . 'class.t3lib_parsehtml_proc.php';
        $htmlParser = t3lib_div::makeInstance("t3lib_parsehtml_proc");
        $text       = trim($this->cObj->getSubpart($content, '###EDITORIAL_CONTAINER###'));

        if (preg_match('~^(.*)###EDITORIAL###(.*)$~is', $text, $container)) {

            $text = $htmlParser->TS_links_rte($this->pi_RTEcssText($this->cObj->data['tx_inxmailcontent_editorial']));
            $text = preg_replace('~<p[^>]*>(.*)</p>~Uis', $container[1] . '\1' . $container[2], $text);
        }
        else {

            $text = $htmlParser->TS_links_rte($this->pi_RTEcssText($this->cObj->data['tx_inxmailcontent_editorial']));
        }
        $content = $this->cObj->substituteMarker($content, '###EDITORIAL_TITLE###', $this->cObj->data['tx_inxmailcontent_editorial_headline']);
        $content = $this->cObj->substituteSubpart($content, '###EDITORIAL_CONTAINER###', $text);

        return $this->substituteLangMarkers(trim($content));
    }
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/pi_content/class.tx_inxmailcontent_pi_content.php']) {
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/pi_content/class.tx_inxmailcontent_pi_content.php']);
}
?>