config.admPanel = 0
config.no_cache = 1
config.debug = 0

includeLibs.tx_inxmailcontent_pi_content = EXT:inxmail_content/pi_content/class.tx_inxmailcontent_pi_content.php

page >
page = PAGE
page {
    typeNum=0
    bodyTag = <body link="#00628E" vlink="#00628E" alink="#00628E" bgcolor="#00628E">

    10 = TEMPLATE
    10.template < plugin.tx_inxmailcontent_pi_content
    10.workOnSubpart = NEWSLETTER_TEMPLATE_HTML
    10.marks {
        #IMAGEURL = TEXT
        #IMAGEURL.value = {$plugin.tx_inxmailcontent.newsletterimages}
    }
    10.subparts {
        CONTENT = COA_INT
        CONTENT {
            10 < styles.content.get
            10.renderObj = < plugin.tx_inxmailcontent_ctype
        }
    }
}

page.config {
    disableAllHeaderCode = 1
    disablePrefixComment = 1
    additionalHeaders = Content-type: text/html
    debug = 0
}

pageText >
pageText = PAGE
pageText {
    typeNum=500

    10 = TEMPLATE
    10.template < plugin.tx_inxmailcontent_pi_content
    10.workOnSubpart = NEWSLETTER_TEMPLATE_TEXT
    10.marks {
        #IMAGEURL = TEXT
        #IMAGEURL.value = {$plugin.tx_inxmailcontent.newsletterimages}
    }
    10.subparts {
        CONTENT = COA_INT
        CONTENT {
            10 < styles.content.get
            10.renderObj = < plugin.tx_inxmailcontent_ctype
        }
    }
}

pageText.config {
    disableAllHeaderCode = 1
    disablePrefixComment = 1
    additionalHeaders = Content-type: text/plain
    debug = 0
}




###
# WYSIWYG Support für Shift-Enter / Enter
###
lib.parseFunc_RTE {
    nonTypoTagStdWrap.encapsLines {
        addAttributes >
        encapsTagList = p,div,hr,h1,h2,h3,h4,h5,h6,ul,ol,pre,blockquote,table,th,tr,td
        nonWrappedTag >
        wrapNonWrappedLines = <p>|</p>
    }
}
lib.parseFunc_RTE {
    makelinks = 1
    makelinks.http.keep = path
    makelinks.http.extTarget = _blank
    makelinks.mailto.keep = path
    tags {
        link = TEXT
        link {
            current = 1
            typolink.extTarget = _blank
            typolink.target=_self
            typolink.parameter.data = parameters : allParams
        }
    }
}
lib.parseFunc.tags.link.typolink.target =
