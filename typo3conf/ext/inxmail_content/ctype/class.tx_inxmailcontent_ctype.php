<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Inxmail GmbH - http://www.inxmail.de
*  (c) 2010 [di] digitale informationssysteme gmbh - http://www.digi-info.de
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * class.tx_inxmailcontent_ctype.php
 *
 * Plugin for (un)subscription forms
 *
 * $Id:$
 *
 * @author Jens Jacobsen <jacobsen@digi-info.de>
 */
require_once PATH_tslib . 'class.tslib_pibase.php';

/**
 * This class provides ctype rendering.
 *
 * @author  Jens Jacobsen <jacobsen@digi-info.de>
 * @package TYPO3
 * @subpackage tx_inxmailcontent
 */
class tx_inxmailcontent_ctype extends tslib_pibase {

    /**
     * prefix id
     *
     * @var string
     */
    public $prefixId = 'tx_inxmailcontent_ctype';

    /**
     * path to this script relative to the extension dir
     *
     * @var string
     */
    public $scriptRelPath = 'ctype/tx_inxmailcontent_ctype.php';

    /**
     * extension key
     *
     * @var string
     */
    public $extKey = 'inxmail_content';

    /**
     * check for chash
     *
     * @var boolean
     */
    public $pi_checkCHash = TRUE;

    /**
     * template
     *
     * @var string
     */
    private static $_template = null;

    /**
     * loads the template once
     *
     * @return  string
     */
    private function _getTemplate() {

        if (is_null(self::$_template)) {

            $templateImgPathPrefix = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_inxmailcontent.']['templateImgPathPrefix'];
            if (preg_match('~^EXT:~i', $templateImgPathPrefix)) {
                $t3lib_tsparser_ext = t3lib_div::makeInstance('t3lib_tsparser_ext');
                /* @var $t3lib_tsparser_ext t3lib_tsparser_ext */
                $templateImgPathPrefix = $t3lib_tsparser_ext->ext_detectAndFixExtensionPrefix($templateImgPathPrefix);
            }

            if (!array_key_exists('tx_inxmailcontent_template', $this->cObj->data) ||
                empty($this->cObj->data['tx_inxmailcontent_template'])) {


                $lConf   = array(
                               'template' => 'FILE',
                               'template.' => array(
                                   'file' => $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_inxmailcontent.']['templateFile'],
                               ),
                               'workOnSubpart' =>  'NEWSLETTER_TEMPLATE_HTML',
                           );
                $template = trim($this->cObj->TEMPLATE($lConf));
                $template = preg_replace('~(<img[^>]*src\s*=\s*")([^"]*"[^>]*>)~Uis', '\1' . $templateImgPathPrefix . '\2', $template);
            }
            else {

                $lConf   = array(
                               'template' => 'FILE',
                               'template.' => array(
                                   'file' => $this->cObj->data['tx_inxmailcontent_template'],
                               ),
                               'workOnSubpart' =>  'NEWSLETTER_TEMPLATE_HTML',
                           );
                $template = trim($this->cObj->TEMPLATE($lConf));
                $template = preg_replace('~(<img[^>]*src\s*=\s*")([^"]*"[^>]*>)~Uis', '\1' . $templateImgPathPrefix . '\2', $template);
            }
            self::$_template = trim($template);
        }
        return self::$_template;
    }

    /**
     * returns the given subpart
     *
     * @param   string $subpart
     * @return  string
     */
    private function _getTemplateSubpart($subpart) {

        return trim($this->cObj->getSubpart($this->_getTemplate(), $subpart));
    }

    /**
     * returns the output for the content element
     *
     * @return  string
     */
    private function _renderHTML() {

        require_once PATH_t3lib . 'class.t3lib_parsehtml_proc.php';
        $htmlParser = t3lib_div::makeInstance("t3lib_parsehtml_proc");
        $template   = null;

        switch($this->cObj->data['CType']) {

            case 'text':
            case 'inxmail_content_ctype':
                if (is_null($template)) {

                    if (!empty($this->cObj->data['image'])) {

                        $template = $this->_getTemplateSubpart('###ELEMENT_TEXT_WITH_IMAGE###');
                    }
                    else {

                        $template = $this->_getTemplateSubpart('###ELEMENT_TEXT###');
                    }
                }

                $text = trim($this->cObj->getSubpart($template, '###TEXT_CONTAINER###'));

                if (preg_match('~^(.*)###TEXT###(.*)$~is', $text, $container)) {

                    $text = $htmlParser->TS_links_rte($this->pi_RTEcssText($this->cObj->data['bodytext']));
                    $text = preg_replace('~<p[^>]*>(.*)</p>~Uis', $container[1] . '\1' . $container[2], $text);
                }
                else {

                    $text = $htmlParser->TS_links_rte($this->pi_RTEcssText($this->cObj->data['bodytext']));
                }

                $image = '&nbsp;';
                if(!empty($this->cObj->data['image'])){
                    $img                  = array();
                    $img['file']          = 'uploads/pics/' . $this->cObj->data['image'];
                    $img['file.']['maxW'] = 120;

                    $image = $this->cObj->IMAGE($img);
                }

                $marker = array(
                    '###ID###'    => $this->cObj->data['uid'],
                    '###TITLE###' => $this->cObj->data['header'],
                    '###IMAGE###' => $image,
                );
                $template = $this->cObj->substituteSubpart($template, '###TEXT_CONTAINER###', $text);
                $content  = $this->cObj->substituteMarkerArray($template, $marker);

                break;
            default:
                break;
        }
        return $content;
    }

    private function _renderText() {
        switch($this->cObj->data['CType']) {

            case 'text':
            case 'inxmail_content_ctype':

                require_once PATH_t3lib . 'class.t3lib_parsehtml_proc.php';
                $htmlParser = t3lib_div::makeInstance("t3lib_parsehtml_proc");

                $content.= $this->cObj->data['header'] . "\n\n";
                $content.= strip_tags($htmlParser->TS_links_rte($this->pi_RTEcssText($this->cObj->data['bodytext'])), '<a><link>') . "\n\n---\n\n";

                break;
            default:
                break;
        }
        return $content;
    }

    /**
     * The main method of the PlugIn
     *
     * @param	string		$content: The PlugIn content
     * @param	array		$conf: The PlugIn configuration
     * @return	The content that is displayed on the website
     */
    public function main($content, $conf) {
        $typeNum = (int)t3lib_div::_GET('type');
        if ($typeNum == 0) {
            return $this->_renderHTML();
        }
        else {
            return $this->_renderText();
        }
    }
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/ctype/class.tx_inxmailcontent_ctype.php'])	{
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/inxmail_content/ctype/class.tx_inxmailcontent_ctype.php']);
}
?>