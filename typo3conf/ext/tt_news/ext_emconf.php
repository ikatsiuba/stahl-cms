<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "tt_news".
 *
 * Auto generated 02-01-2017 16:13
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'News',
  'description' => 'Website news with front page teasers and article handling inside.',
  'category' => 'plugin',
  'version' => '7.6.3',
  'state' => 'beta',
  'uploadfolder' => true,
  'createDirs' => '',
  'clearcacheonload' => false,
  'author' => 'Rupert Germann [noerdisch]',
  'author_email' => 'rupi@gmx.li',
  'author_company' => 'www.noerdisch.de',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '7.6.0-7.6.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

