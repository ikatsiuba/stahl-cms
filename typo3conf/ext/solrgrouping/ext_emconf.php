<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "solrgrouping".
 *
 * Auto generated 18-01-2017 15:18
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Apache Solr for TYPO3 - Result Grouping',
  'description' => 'Solr Result Grouping allows grouping of documents sharing a common field. For each group the most relevant documents are returned.',
  'version' => '1.3.0',
  'state' => 'stable',
  'category' => 'plugin',
  'author' => 'Ingo Renner',
  'author_email' => 'ingo@typo3.org',
  'author_company' => 'dkd Internet Service GmbH',
  'uploadfolder' => true,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'constraints' => 
  array (
    'depends' => 
    array (
      'solr' => '5.0.0-',
      'typo3' => '7.6.0-8.0.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'autoload' => 
  array (
    'psr-4' => 
    array (
      'ApacheSolrForTypo3\\Solrgrouping\\' => 'Classes/',
    ),
  ),
  '_md5_values_when_last_written' => '',
  'clearcacheonload' => true,
);

