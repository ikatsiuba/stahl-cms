plugin.tx_solrfluid {

	view {
		layoutRootPaths.10 = EXT:solrfluid/Resources/Private/Layouts/
		partialRootPaths.10 = EXT:solrfluid/Resources/Private/Partials/
		templateRootPaths.10 = EXT:solrfluid/Resources/Private/Templates/

		# important! else linkbuilding will fail
		pluginNamespace = tx_solr
	}
}

page.includeCSS {
	search = EXT:solrfluid/Resources/Public/Css/results.css
}
