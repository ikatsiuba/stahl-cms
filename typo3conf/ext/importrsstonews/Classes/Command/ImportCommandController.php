<?php

namespace Havasww\ImportRsstonews\Command;

/**
 * Command controller to import RSS feeds to tx_news extension
 *
 * @author Alexander Boos
 */
class ImportCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController{

	/**
     * importNewsRepository
     *
     * @var Havasww\ImportRsstonews\Domain\Repository\ImportNewsRepository
     * @inject
     */
    protected $importNewsRepository = NULL;
	
	/**
	 * Inserts RSS feed into tx_news_domain_model_news
	 * 
	 * @param string $url URL where RSS Feed is located
	 * @param string $categories
	 * @param int language (0=deutsch,1=englisch) NICHT FINAL
	 * @param bool $active Defines whether the news will be displayed by default or not (0=not active, 1=active)
	 */
	public function importRssCommand($url, $categories, $language, $active = 0){
		//Load and process Feed
		$feed = implode(file($url));
		$xml = simplexml_load_string($feed,null,LIBXML_NOCDATA);
		$json = json_encode($xml);
		$response = json_decode($json,TRUE);
		
		//Instantiate needed Classes and Repository
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$newsRepository = $objectManager->get('GeorgRinger\News\Domain\Repository\NewsRepository');
		$persistenceManager = $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager');
		
		//Initialize needed variables
		$target_categories = explode(",", $categories);
		$now = time();
		
		//Loop news items from feed
		foreach($response["channel"]["item"] as $news){
			$import_id = $news["guid"];
			//Check if news with import id already exists, otherwise insert to db
			if(!$this->importNewsRepository->newsExists($import_id)){
				//Set news properties
				$new = new \GeorgRinger\News\Domain\Model\News;
				$new->setPid(36);
				$new->setStarttime($now);
				$new->setFeGroup(0);
				$new->setTitle(trim($news["title"]));
				$new->setTeaser(trim($news["description"]));
				$new->setBodytext(trim($news["content"]));
				$datetime = $news["pubDate"];
				$new->setDatetime(strtotime($datetime));
				$new->setImportId($import_id);
				$new->setImportSource("RSS");
				$new->setHidden(!$active);
				$new->setSysLanguageUid($language);
				$newsRepository->add($new);
				
				//Persist, to get the UID of inserted news record
				$persistenceManager->persistAll();
				
				//Loop through configured categories to add to news
				//Need to use TYPO3 global array to use native SQL query due to category repository restrictions
				foreach($target_categories as $category){
					$data = [
						"uid_local" => $category,
						"uid_foreign" => $new->getUid(),
						"tablenames" => "tx_news_domain_model_news",
						"fieldname" => "categories",
						"sorting" => 0,
						"sorting_foreign" => 0
					];
					$GLOBALS['TYPO3_DB']->exec_INSERTquery("sys_category_record_mm",$data);
					echo "Category Match ".$new->getUid()." ".$category."<br>";
				}
				
				//Output on successed insert
				echo "Insert ".$new->getTitle()." -> ".$new->getUid()."<hr>";
			}else{
				//Output if news record already exists
				echo "Exists ".$import_id."<hr>";
			}
		}
	}
}
