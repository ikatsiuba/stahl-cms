<?php
namespace Havasww\ImportRsstonews\Domain\Repository;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This Repository extends the original News Repository to set the PID manually
 *
 * @author alexa_000
 */
class ImportNewsRepository extends \GeorgRinger\News\Domain\Repository\NewsRepository{
	
	
	public function newsExists($import_id){
		$sql = "SELECT * FROM tx_news_domain_model_news WHERE import_id = '".$import_id."'";
		$query = $this->createQuery();
		$query->statement($sql);
		$result = $query->execute();
		$result->toArray();
		if($result->count() == 0){
			return false;
		}else{
			return true;
		}
	}
}
