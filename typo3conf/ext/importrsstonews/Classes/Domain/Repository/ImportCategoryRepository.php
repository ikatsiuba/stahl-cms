<?php
namespace Havasww\ImportRsstonews\Domain\Repository;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This Repository extends the original News Repository to set the PID manually
 *
 * @author alexa_000
 */
class ImportCategoryRepository extends \GeorgRinger\News\Domain\Repository\CategoryRepository{
	public function findByUids($uids){
		$uidArray = explode(",", $uids);
		$query = $this->createQuery();
		foreach ($uidArray as $key => $value) {
			$constraints[] =  $query->equals('uid', $value);
		}
		return $query->matching(
			$query->logicalAnd(
				$query->logicalOr(
					$constraints
				),
				$query->equals('hidden', 0),
				$query->equals('deleted', 0)
			)
		)->execute();
	}
}
