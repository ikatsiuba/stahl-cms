<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Havasww.' . $_EXTKEY,
	'Import',
	array(
		'News' => 'import',
		
	),
	// non-cacheable actions
	array(
		'News' => 'import',
		
	)
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][$_EXTKEY] = Havasww\ImportRsstonews\Command\ImportCommandController::class;