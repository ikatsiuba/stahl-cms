<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "calendarize".
 *
 * Auto generated 15-02-2017 12:16
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Calendarize - Event Management',
  'description' => 'Create a structure for timely controlled tables (e.g. events) and one plugin for the different output of calendar views (list, detail, month, year, day, week...). The extension is shipped with one default event table, but you can also "calendarize" your own table/model. It is completely independent and configurable! Use your own models as event items in this calender. Development on https://github.com/lochmueller/calendarize',
  'category' => 'fe',
  'version' => '2.8.1',
  'state' => 'stable',
  'clearcacheonload' => true,
  'author' => 'Tim Lochmüller',
  'author_email' => 'tim@fruit-lab.de',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '6.2.0-8.99.99',
      'php' => '5.5.0-0.0.0',
      'autoloader' => '2.0.0-0.0.0',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'autoload' => 
  array (
    'psr-4' => 
    array (
      'HDNET\\Calendarize\\' => 'Classes/',
      'JMBTechnologyLimited\\ICalDissect\\' => 'Resources/Private/Php/ICalDissect/src/JMBTechnologyLimited/ICalDissect/',
    ),
  ),
  'uploadfolder' => true,
  'createDirs' => NULL,
  'author_company' => NULL,
);

