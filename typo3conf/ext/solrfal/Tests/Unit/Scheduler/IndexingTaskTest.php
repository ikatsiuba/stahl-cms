<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Queue;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class InitializationAspectTest
 *
 */
class IndexingTaskTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{


    /**
     *
     * @return array
     */
    public static function getProgressDataProvider()
    {
        return array(
            'All' => array(100, 100, 0.0),
            'None' => array(0, 100, 100.0),
            '50% done' => array(50, 100, 50.0),
            '66% done' => array(33333, 100000, 66.67),
            '33% done' => array(66666, 100000, 33.33),
        );
    }

    /**
     *
     * @dataProvider getProgressDataProvider
     * @test
     * @param integer $open
     * @param integer $total
     * @param float $result
     */
    public function getProgressCalculatesCorrectPercentages($open, $total, $result)
    {
        /** @var \ApacheSolrForTypo3\Solrfal\Scheduler\IndexingTask $fixture */
        $fixture = $this->getAccessibleMock('ApacheSolrForTypo3\Solrfal\Scheduler\IndexingTask', array('getItemRepository'), array(), '', false);

        $persistenceMock = $this->getAccessibleMock('ApacheSolrForTypo3\\Solrfal\\Queue\\ItemRepository');
        $persistenceMock->expects($this->any())->method('count')->will($this->returnValue($total));
        $persistenceMock->expects($this->any())->method('countIndexingOutstanding')->will($this->returnValue($open));
        $fixture->expects($this->any())->method('getItemRepository')->will($this->returnValue($persistenceMock));

        $this->assertEquals($result, $fixture->getProgress());
    }
}
