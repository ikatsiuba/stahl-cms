<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class InitializationAspectTest
 */
class ContextFactoryTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Context\ContextFactory
     */
    protected $fixture;

    public function setUp()
    {
        $this->fixture = new \ApacheSolrForTypo3\Solrfal\Context\ContextFactory();
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @return void
     */
    public function registerTypeThrowsExceptionIfClassIsNotImplementingContextInterface()
    {
        $this->fixture->registerType('newType', __CLASS__, __CLASS__);
    }

    /**
     * @test
     * @return void
     */
    public function registerTypeWorksIfInterfaceIsImplemented()
    {
        $this->fixture->registerType('newType',
            get_class($this->getMock('ApacheSolrForTypo3\\Solrfal\\Context\\ContextInterface')),
            get_class($this->getMock('ApacheSolrForTypo3\\Solrfal\\Detection\\RecordDetectionInterface'))
            );
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @return void
     */
    public function registerTypeThrowsExceptionForCustomFactoryNotImplementingTheInterface()
    {
        $this->fixture->registerType(
            'newType',
            get_class($this->getMock('ApacheSolrForTypo3\\Solrfal\\Context\\ContextInterface')),
            __CLASS__
        );
    }

    /**
     * @test
     * @return void
     */
    public function registerTypeAcceptsCorrectFactory()
    {
        $this->fixture->registerType(
            'newType',
            get_class($this->getMock('ApacheSolrForTypo3\\Solrfal\\Context\\ContextInterface')),
            get_class($this->getMock('ApacheSolrForTypo3\\Solrfal\\Detection\\RecordDetectionInterface')),
            get_class($this->getMock('ApacheSolrForTypo3\\Solrfal\\Context\\ContextFactoryInterface'))
        );
    }


    /**
     * @test
     * @expectedException \RuntimeException
     * @return void
     */
    public function getByRecordThrowsExceptionForUnsupportedType()
    {
        $this->fixture->getByRecord(array('context_type' => 'foo'));
    }

    /**
     * @test
     * @return void
     */
    public function getByRecordUsesCustomFactoryIfRegistered()
    {
        $record = array('context_type' => 'foo');

        $class = get_class($this->getMock('ApacheSolrForTypo3\\Solrfal\\Context\\ContextInterface'));
        $factory = $this->getMock('ApacheSolrForTypo3\\Solrfal\\Context\\ContextFactoryInterface');
        $factory->expects($this->once())->method('getByRecord')->with($record);
        GeneralUtility::addInstance(get_class($factory), $factory);

        $detectorMock = $this->getMock('ApacheSolrForTypo3\\Solrfal\\Detection\\RecordDetectionInterface');

        $this->fixture->registerType('foo', $class, get_class($detectorMock), get_class($factory));

        $this->fixture->getByRecord($record);

        GeneralUtility::purgeInstances();
    }


    /**
     * @test
     * @dataProvider getContextRecords
     * @param array $record
     * @param string $expectedClass
     */
    public function getByRecordCreatesBuildInContexts(array $record, $expectedClass)
    {
        $site = $this->getMock('ApacheSolrForTypo3\\Solr\\Site', array(), array(), '', false);
        $site->expects($this->any())->method('getRootPageId')->will($this->returnValue('55'));
        GeneralUtility::addInstance('ApacheSolrForTypo3\\Solr\\Site', $site);

        $rootline = $this->getMock('ApacheSolrForTypo3\\Solr\\Access\\Rootline', array(), array(), '', false);
        $rootline->expects($this->any())->method('__toString')->will($this->returnValue('c:0'));

        GeneralUtility::addInstance('ApacheSolrForTypo3\\Solr\\Access\\Rootline', $rootline);

        $this->assertInstanceOf($expectedClass, $this->fixture->getByRecord($record));

        GeneralUtility::purgeInstances();
    }

    /**
     * @return array
     */
    public static function getContextRecords()
    {
        return array(
            'Storage' => array(array('context_type' => 'storage'), 'ApacheSolrForTypo3\Solrfal\Context\StorageContext'),
            'Page' => array(array('context_type' => 'page'), 'ApacheSolrForTypo3\Solrfal\Context\PageContext'),
            'Record' => array(array('context_type' => 'record'), 'ApacheSolrForTypo3\Solrfal\Context\RecordContext')
        );
    }
}
