<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class InitializationAspectTest
 */
class AbstractContextTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @test
     * @return void
     */
    public function gettersReturnConstructorParameters()
    {
        $site = $this->getMock('\ApacheSolrForTypo3\Solr\Site', array(), array(), '', false);
        $rootline = $this->getMock('\ApacheSolrForTypo3\Solr\Access\Rootline', array(), array(), '', false);
        $language = 0;

        /** @var \ApacheSolrForTypo3\Solrfal\Context\AbstractContext $fixture */
        $fixture = $this->getAccessibleMock(
            'ApacheSolrForTypo3\Solrfal\Context\AbstractContext',
            array('getContextIdentifier', 'getIdentifierForItemSpecificFieldConfiguration'),
            array($site, $rootline, $language)
        );

        $this->assertSame($site, $fixture->getSite());
        $this->assertSame($rootline, $fixture->getAccessRestrictions());
        $this->assertSame($language, $fixture->getLanguage());
    }

    /**
     * @test
     * @return void
     */
    public function toArrayReturnsExpectedValues()
    {
        $site = $this->getMock('\ApacheSolrForTypo3\Solr\Site', array(), array(), '', false);
        $site->expects($this->once())->method('getRootPageId')->will($this->returnValue('55'));

        $rootline = $this->getMock('\ApacheSolrForTypo3\Solr\Access\Rootline', array(), array(), '', false);
        $rootline->expects($this->once())->method('__toString')->will($this->returnValue('c:0'));

        $language = 0;

        /** @var \ApacheSolrForTypo3\Solrfal\Context\AbstractContext $fixture */
        $fixture = $this->getAccessibleMock(
            'ApacheSolrForTypo3\Solrfal\Context\AbstractContext',
            array('getContextIdentifier', 'getIdentifierForItemSpecificFieldConfiguration'),
            array($site, $rootline, $language)
        );
        $fixture->expects($this->once())->method('getContextIdentifier')->will($this->returnValue('test'));

        $data = array(
            'context_type' => 'test',
            'context_language' => 0,
            'context_access_restrictions' => 'c:0',
            'context_site' => 55,
            'context_additional_fields' => '[]',
            'context_record_indexing_configuration' => '',
            'error' => 0,
            'error_message' => ''
        );

        $this->assertEquals($data, $fixture->toArray());
    }
}
