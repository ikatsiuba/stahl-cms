<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Context;

use TYPO3\CMS\Core\Resource\File;

/***************************************************************
 * Copyright notice
 *
 * (c) 2015 Timo Schmidt <timo.schmidt@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


class TestContext implements \ApacheSolrForTypo3\Solrfal\Context\ContextInterface
{


    /**
     * @return string
     */
    public function getContextIdentifier()
    {
        // TODO: Implement getContextIdentifier() method.
    }

    /**
     * Returns the Site
     *
     * @return \ApacheSolrForTypo3\Solr\Site
     */
    public function getSite()
    {
        // TODO: Implement getSite() method.
    }

    /**
     * @return integer
     */
    public function getLanguage()
    {
        // TODO: Implement getLanguage() method.
    }

    /**
     * @return \ApacheSolrForTypo3\Solr\Access\Rootline
     */
    public function getAccessRestrictions()
    {
        // TODO: Implement getAccessRestrictions() method.
    }

    /**
     * Returns the pageId of this context
     *
     * @return int
     */
    public function getPageId()
    {
        // TODO: Implement getPageId() method.
    }

    /**
     * Returns an array of context specific field to add to the solr document
     *
     * @return array
     */
    public function getAdditionalStaticDocumentFields()
    {
        // TODO: Implement getAdditionalStaticDocumentFields() method.
    }

    /**
     * Returns an array of context specific field to add to the solr document,
     * dynamically calculated from the FILE
     *
     * @param File $file
     * @return array
     */
    public function getAdditionalDynamicDocumentFields(File $file)
    {
        // TODO: Implement getAdditionalDynamicDocumentFields() method.
    }

    /**
     * @return array
     */
    public function getSpecificFieldConfigurationTypoScript()
    {
        // TODO: Implement getSpecificFieldConfigurationTypoScript() method.
    }

    /**
     * Returns the array representation for database storage
     *
     * @return array
     */
    public function toArray()
    {
        // TODO: Implement toArray() method.
    }
}
