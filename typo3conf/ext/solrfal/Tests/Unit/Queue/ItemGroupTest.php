<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Queue;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Timo Hund <timo.hund@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use ApacheSolrForTypo3\Solr\Tests\Unit\UnitTest;
use ApacheSolrForTypo3\Solrfal\Context\ContextInterface;
use ApacheSolrForTypo3\Solrfal\Queue\Item;
use ApacheSolrForTypo3\Solrfal\Queue\ItemGroup;
use TYPO3\CMS\Core\Resource\File;

/**
 * Class ItemMergeSetTest
 *
 */
class ItemGroupTest extends UnitTest
{

    /**
     * @test
     */
    public function canGetRootItem() {
        $fileMock = $this->getDumbMock(File::class);
        $contextMock = $this->getDumbMock(ContextInterface::class);

        $itemGroup = new ItemGroup();
        $item1 = new Item($fileMock, $contextMock, 14);
        $item2 = new Item($fileMock, $contextMock, 12);
        $item3 = new Item($fileMock, $contextMock, 22);

        $itemGroup->add($item1);
        $itemGroup->add($item2);
        $itemGroup->add($item3);

        $this->assertSame($item2, $itemGroup->getRootItem(), 'ItemSet retrieved unexpected root item');
        $this->assertTrue($itemGroup->getIsRootItem($item2), 'Could not detect root item');

    }
}