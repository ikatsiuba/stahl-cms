<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Queue;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class InitializationAspectTest
 *
 */
class InitializationAspectTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @test
     */
    public function postProcessIndexQueueInitializationCallsInitializeOfFileIndexQueue()
    {
        $dbMock = $this->getMock('\TYPO3\CMS\Core\Database\DatabaseConnection', array(), array(), '', false);
        $dbMock->expects($this->any())->method('fullQuoteStr')->will($this->returnCallback(function ($value) { return $value; }));
        $dbMock->expects($this->any())->method('exec_SELECTgetRows')->will($this->returnCallback(function ($select_fields, $from_table, $where_clause, $groupBy = '', $orderBy = '', $limit = '', $uidIndexField = '') {
            return array(10);
        }));
        $GLOBALS['TYPO3_DB'] = $dbMock;
        $testConfig = array();

        /** @var \ApacheSolrForTypo3\Solrfal\Queue\InitializationAspect $fixture */
        $fixture = $this->getAccessibleMock('ApacheSolrForTypo3\\Solrfal\\Queue\\InitializationAspect', array('getContextDetectorsForSite'));
        $site = $this->getAccessibleMock('\ApacheSolrForTypo3\Solr\Site', array(), array(), '', false);


        $recordContextMock = $this->getAccessibleMock('ApacheSolrForTypo3\\Solrfal\\Detection\\RecordContextDetector', array(), array(), '', false);
        $recordContextMock->expects($this->once())->method('initializeQueue')->with($testConfig);
        $fixture->expects($this->once())->method('getContextDetectorsForSite')->will($this->returnValue(array($recordContextMock)));

        $fixture->postProcessIndexQueueInitialization($site, $testConfig, array());
    }
}
