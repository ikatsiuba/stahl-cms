<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Indexing;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use ApacheSolrForTypo3\Solr\Util;

/**
 * Document factory tests
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class DocumentFactoryTest extends \ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest
{

    /**
     * Item repository
     *
     * @var \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository
     */
    protected $itemRepository;

    /**
     * The document factory
     *
     * @var \ApacheSolrForTypo3\Solrfal\Indexing\DocumentFactory
     */
    protected $documentFactory;

    /**
     * Set up document factory tests
     *
     * @return void
     */
    public function setUp()
    {
        $this->testExtensionsToLoad[] = 'typo3/sysext/filemetadata';
        $this->testExtensionsToLoad[] = 'typo3conf/ext/news';

        parent::setUp();
        $this->documentFactory = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Indexing\\DocumentFactory');
        $this->itemRepository = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Queue\\ItemRepository');
    }

    /**
     * Tests the document creation in record context
     *
     * @test
     * @return void
     */
    public function createDocumentInRecordContext()
    {
        $this->importDataSetFromFixture('index_file_in_record_context.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $queueItem = $this->itemRepository->findByUid(1);
        Util::initializeTsfe(1, 0);
        $document = $this->documentFactory->createDocumentForQueueItem($queueItem);

        $this->assertEquals('Lorem ipsum', $document->getField('fileReferenceTitle')['value'], 'Reference title not set correctly!');
        $this->assertEquals('title sys_file_metadata', $document->getField('metaDataTitle')['value'], 'File meta data title not set correctly');
        $this->assertEquals(true, in_array('fileReferenceUrl', $document->getFieldNames()), 'Reference URL not set!');
        $this->assertNotEmpty($document->getField('fileReferenceUrl')['value'], 'Invalid reference URL!');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalConfiguration_intS", defined via global configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalRecordContextConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalRecordContextConfiguration_intS", defined via global record context configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaRecordContextTableConfiguration_intS')['value'], 'Field "fieldDefinedViaRecordContextTableConfiguration_intS", defined via table specific record context configuration, isn\'t set correctly');
        $this->assertEquals('tx_solr_file/9999', $document->getField('variantId')['value'], 'Field "variantId" did not contain expected content');
    }

    /**
     * Tests the document creation in storage context
     *
     * @test
     * @return void
     */
    public function createDocumentInStorageContext()
    {
        $this->importDataSetFromFixture('index_file_in_storage_context.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $queueItem = $this->itemRepository->findByUid(1);
        Util::initializeTsfe(1, 0);
        $document = $this->documentFactory->createDocumentForQueueItem($queueItem);

        $this->assertEquals('r:1,2', $document->getField('access')['value'], 'File permissions not set correctly');
        $this->assertEquals('title sys_file_metadata', $document->getField('metaDataTitle')['value'], 'File meta data title not set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalConfiguration_intS", defined via global configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalStorageContextConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalStorageContextConfiguration_intS", defined via global storage context configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaSpecificStorageContextConfiguration_intS')['value'], 'Field "fieldDefinedViaSpecificStorageContextConfiguration_intS", defined via table specific storage context configuration, isn\'t set correctly');
        $this->assertEquals('tx_solr_file/9999', $document->getField('variantId')['value'], 'Field "variantId" did not contain expected content');
    }

    /**
     * Tests the document creation in page context
     *
     * @test
     * @return void
     */
    public function createDocumentInPageContext()
    {
        $this->importDataSetFromFixture('index_file_in_page_context.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $queueItem = $this->itemRepository->findByUid(1);
        Util::initializeTsfe(1, 0);
        $document = $this->documentFactory->createDocumentForQueueItem($queueItem);

        $this->assertEquals(1, $document->getField('fileReferenceUrl')['value'], 'Field "fileReferenceUrl" isn\'t filled with page id, as expected');
        $this->assertEquals(1652220000, $document->getField('endtime')['value'], 'Access field "endtime" isn\'t set correctly!');
        $this->assertEquals('Hello Solr', $document->getField('fileReferenceTitle')['value'], 'Field "fileReferenceTitle" isn\'t filled with page title, as expected');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalConfiguration_intS", defined via global configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaPageContextConfiguration_intS')['value'], 'Field "fieldDefinedViaPageContextConfiguration_intS", defined via page context configuration, isn\'t set correctly');
        $this->assertEquals('tx_solr_file/9999', $document->getField('variantId')['value'], 'Field "variantId" did not contain expected content');
    }
}
