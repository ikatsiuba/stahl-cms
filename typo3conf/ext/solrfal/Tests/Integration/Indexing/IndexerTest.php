<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Indexing;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Util;
use TYPO3\CMS\Core\FormProtection\Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use ApacheSolrForTypo3\Solr\Site;

/**
 * Indexer tests
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class IndexerTest extends \ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest
{

    /**
     * The indexer
     *
     * @var \ApacheSolrForTypo3\Solrfal\Indexing\Indexer
     */
    protected $indexer;

    /**
     * @return void
     */
    public function setUp()
    {
        $this->testExtensionsToLoad[] = 'typo3/sysext/filemetadata';
        $this->testExtensionsToLoad[] = 'typo3conf/ext/news';

        parent::setUp();
        $this->indexer = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Indexing\\Indexer');
    }

    /**
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->cleanUpSolrServerAndAssertEmpty();
    }

    /**
     * @test
     */
    public function canRemoveHugeAmountOfDocumentsByQueueUids()
    {
        $this->importDataSetFromFixture('can_remove_documents.xml');
        $site = $this->getSite(1);
        $uidArray = range(1, 10240);

        try {
            $this->indexer->removeByQueueEntriesAndSite($uidArray, $site);
            $succeeded = true;
            $msg = '';
        } catch (\Apache_Solr_HttpTransportException $e) {
            $succeeded = false;
            $msg = 'Failed to remove documents from index: ' . print_r($e->getResponse(), 1);
        }
        $this->assertEquals(true, $succeeded, $msg);
    }

    /**
     * @test
     */
    public function canRunIndexingTask()
    {
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');
        $this->importDataSetFromFixture('run_indexing_task.xml');

        try {
            $this->indexer->processIndexQueue(1, false);
            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }

    /**
     * @test
     */
    public function threeSameFilesInAllContextCreateThreeSolrDocuments()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_notmerged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();

            $this->assertSolrContainsDocumentCount(3);

            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }


    /**
     * @test
     */
    public function threeSameFilesInAllContextCreateOneSolrDocumentWithMerging()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_merged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();

            $this->assertSolrContainsDocumentCount(1);
            $solrContent = file_get_contents('http://localhost:8999/solr/core_en/select?q=*:*');
            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }


    /**
     * @test
     */
    public function whenThreeItemsGetIndexedAndOneIsRemovedWithoutMergingTwoItemsAreLeft()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_notmerged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();

            $this->assertSolrContainsDocumentCount(3);

            $all = $this->itemRepository->findAll();
            $lastItem = array_pop($all);
            $this->indexer->removeFromIndex($lastItem);
            $this->waitToBeVisibleInSolr();

            // because we've removed one item, we expected that two will be left now
            $this->assertSolrContainsDocumentCount(2);

            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }

    /**
     * @test
     */
    public function whenThreeItemsGetIndexedAndOneIsRemovedWithMergingOneItemIsLeft()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_merged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();

            $this->assertSolrContainsDocumentCount(1);

            $all = $this->itemRepository->findAll();
            $lastItem = array_pop($all);

            // remove from index is triggered with the consistency aspect
            $this->itemRepository->remove($lastItem);
            $this->waitToBeVisibleInSolr();

            // because we've removed one item, we expected that two will be left now
            $this->assertSolrContainsDocumentCount(1);

            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }

    /**
     * @test
     */
    public function whenThreeItemsGetIndexedAndAllAreRemovedWithMergingNoItemIsLeft()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_merged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();

            $this->assertSolrContainsDocumentCount(1);

            $all = $this->itemRepository->findAll();
            foreach ($all as $item) {
                // remove from index is triggered with the consistency aspect
                $this->itemRepository->remove($item);
            }
            $this->waitToBeVisibleInSolr();

            //we've removed all items and assume now that no document is left
            $this->assertSolrIsEmpty();

            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }


    /**
     * @test
     */
    public function whenThreeItemsGetIndexedAndAllAreRemovedInReversedOrderWithMergingNoItemIsLeft()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_merged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();

            $this->assertSolrContainsDocumentCount(1);

            $all = $this->itemRepository->findAll();
            $all = array_reverse($all);
            foreach ($all as $item) {
                // remove from index is triggered with the consistency aspect
                $this->itemRepository->remove($item);
            }
            $this->waitToBeVisibleInSolr();

            //we've removed all items and assume now that no document is left
            $this->assertSolrIsEmpty();

            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }

    /**
     * @test
     */
    public function whenTreeItemsGetIndexedAndTheFirstAndLastAreDeletedTheSecondItemIsVisibleInSolr()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_merged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();

            $this->assertSolrContainsDocumentCount(1);

            $first = $this->itemRepository->findByUid(1);
            $this->itemRepository->remove($first);

            $last = $this->itemRepository->findByUid(3);
            $this->itemRepository->remove($last);

            $this->waitToBeVisibleInSolr();

            // one document (with uid 2) should be left
            $this->assertSolrContainsDocumentCount(1);

            // check the content in solr
            $solrContent = file_get_contents('http://localhost:8999/solr/core_en/select?q=*:*');
            $this->assertContains('"fileReferenceType":"tx_news_domain_model_news"', $solrContent, 'No news item in solr');

            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }

    /**
     * @test
     */
    public function canIndexAllDocumentsWhenMergeIdIsMissingAndMergingIsDisabled()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_notmerged_and_nomergeid.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();
            $this->assertSolrContainsDocumentCount(3);

            $succeeded = true;
            $msg = '';
        } catch (Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();

        }
        $this->assertEquals(true, $succeeded, $msg);
    }

    /**
     * @test
     */
    public function deletionOfAllSiteItemsIsWorkingWithMerging()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_merged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();

            $this->assertSolrContainsDocumentCount(1);

            // removing from solr is handled by the signals
            $this->itemRepository->removeBySite(Site::getFirstAvailableSite());
            $this->waitToBeVisibleInSolr();

            // we assume that solr is empty because all documents of a site have been removed.
            $this->assertSolrIsEmpty();

            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }

    /**
     * @test
     */
    public function deletionOfItemsByTypeIsWorkingWithMerging()
    {
        $this->importDataSetFromFixture('index_file_in_all_contexts_merged.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        try {
            $this->indexer->processIndexQueue(3, false);
            $this->waitToBeVisibleInSolr();
            $this->assertSolrContainsDocumentCount(1);

            // removing from solr is handled by the signals
            $site = Site::getFirstAvailableSite();
            $this->itemRepository->removeByTableAndUidInContext('record', $site, 'tx_news_domain_model_news', 1);
            $this->itemRepository->removeByTableAndUidInContext('page', $site, 'tt_content', 1);

            $this->waitToBeVisibleInSolr();
            $this->assertSolrContainsDocumentCount(1);

            // check the content in solr
            $solrContent = file_get_contents('http://localhost:8999/solr/core_en/select?q=*:*');

            // we expect that only one document is left, which is the document from the storage context
            $this->assertContains('"type":"tx_solr_file"', $solrContent, 'No tx_solr_file item in solr');

            $succeeded = true;
            $msg = '';
        } catch (\Exception $e) {
            $succeeded = false;
            $msg = 'Failed to run indexing task: ' . $e->getMessage();
        }

        $this->assertEquals(true, $succeeded, $msg);
    }
}
