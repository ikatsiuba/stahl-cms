<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Record context detector tests
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class RecordContextDetectorTest extends \ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest
{

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect
     */
    protected $consistencyAspect;

    /**
     * @return void
     */
    public function setUp()
    {
        $this->testExtensionsToLoad[] = 'typo3conf/ext/news';

        parent::setUp();
        $this->consistencyAspect = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Queue\\ConsistencyAspect');
    }

    /**
     * @test
     */
    public function detectFileInRecordAfterRecordUpdate()
    {
        $this->importDataSetFromFixture('RecordContext/detects_file_in_record_context.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        // simulate update on news record
        $dataHandler = $this->getDataHandler();
        $this->consistencyAspect->processDatamap_afterDatabaseOperations('update', 'tx_news_domain_model_news', 1, array(), $dataHandler);

        // check index queue
        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Number of file index queue entries is not as expected');
        $this->assertEquals(9999, $indexQueueContent[0]->getFile()->getUid(), 'Wrong file detected');
        $this->assertEquals(
            $indexQueueContent[0]->getContext()->toArray(),
            array(
                'context_type' => 'record',
                'context_language' => 0,
                'context_access_restrictions' => 'r:1',
                'context_site' => 1,
                'context_additional_fields' => '[]',
                'context_record_indexing_configuration' => 'news',
                'error' => 0,
                'error_message' => '',
                'context_record_table' => 'tx_news_domain_model_news',
                'context_record_uid' => 1,
                'context_record_field' => 'bodytext'
            ),
            'Invalid index queue entry found'
        );

        $this->removeTemporaryFiles();
    }

    /**
     * @test
     */
    public function ignoreFileInRecordAfterRecordUpdate()
    {
        $this->importDataSetFromFixture('RecordContext/no_valid_files_in_record_context.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');

        // simulate update on news record
        $dataHandler = $this->getDataHandler();
        $this->consistencyAspect->processDatamap_afterDatabaseOperations('update', 'tx_news_domain_model_news', 1, array(), $dataHandler);

        // check index queue
        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, empty($indexQueueContent), 'Index queue not is not empty as expected');

        $this->removeTemporaryFiles();
    }

    /**
     * @test
     */
    public function ignoreMissingFileInRecordAfterRecordUpdate()
    {
        $this->importDataSetFromFixture('RecordContext/ignore_missing_file_in_record_context.xml');

        // simulate update on news record
        $dataHandler = $this->getDataHandler();
        $this->consistencyAspect->processDatamap_afterDatabaseOperations('update', 'tx_news_domain_model_news', 1, array(), $dataHandler);

        // check index queue
        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(0, count($indexQueueContent), 'Index queue not is not empty as expected, missing file(s) added');
    }

    /**
     * @test
     */
    public function detectRecordDeletion()
    {
        $this->importDataSetFromFixture('RecordContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array(), array('tx_news_domain_model_news' => array(1 => array('delete' => 1))));
        $dataHandler->process_cmdmap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(0, count($indexQueueContent), 'Index queue not is not empty as expected, attached files not removed on record deletion');
    }

    /**
     * @test
     */
    public function detectRecordHiding()
    {
        $this->importDataSetFromFixture('RecordContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array('tx_news_domain_model_news' => array(1 => array('hidden' => 1))), array());
        $dataHandler->process_datamap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(0, count($indexQueueContent), 'Index queue not is not empty as expected, attached files not removed on record hiding');
    }

    /**
     * @test
     */
    public function detectFileRelationDeletion()
    {
        $this->importDataSetFromFixture('RecordContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array(), array('sys_file_reference' => array(8888 => array('delete' => 1))));
        $dataHandler->process_cmdmap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Index queue not updated as expected, deletion of sys_file_reference ignored.');
    }

    /**
     * @test
     */
    public function detectFileRelationHiding()
    {
        $this->importDataSetFromFixture('RecordContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array('sys_file_reference' => array(8888 => array('hidden' => 1))), array());
        $dataHandler->process_datamap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Index queue not updated as expected, hiding of sys_file_reference ignored.');
    }

    /**
     * @dataProvider getGroupSettingVariants
     * @test
     * @param boolean $groupFieldDefined
     * @param string $expectedAccessRestrictions
     */
    public function respectsGroupSettings($groupFieldDefined, $expectedAccessRestrictions)
    {
        $this->importDataSetFromFixture('RecordContext/detects_file_in_record_context.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        // remove group field definition if requested
        if (!$groupFieldDefined) {
            unset($GLOBALS['TCA']['tx_news_domain_model_news']['ctrl']['enablecolumns']['fe_group']);
        }

        // simulate update on news record
        $dataHandler = $this->getDataHandler();
        $this->consistencyAspect->processDatamap_afterDatabaseOperations('update', 'tx_news_domain_model_news', 1, array(), $dataHandler);

        // check index queue
        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Number of file index queue entries is not as expected');
        $accessRestrictions = $indexQueueContent[0]->getContext()->getAccessRestrictions()->__toString();
        $this->assertEquals($expectedAccessRestrictions, $accessRestrictions, 'Access restrictions differ from expected value');

        $this->removeTemporaryFiles();
    }

    /**
     * Returns the group setting variants
     *
     * @return array
     */
    public function getGroupSettingVariants()
    {
        return [
            'group_field_defined' => [true,  'r:1'],
            'no_group_field_defined' => [false, 'r:0']
        ];
    }
}
