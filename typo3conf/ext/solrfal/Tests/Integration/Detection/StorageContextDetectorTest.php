<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solrfal\Detection\StorageContextDetector;

/**
 * Storage context detector tests
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class StorageContextDetectorTest extends \ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest
{

    /**
     * Tests the file detection during index queue initialization, without configured folders
     *
     * Testing the detection whithout folder definitions ensures the backwords compatibility, since
     * older installations may not have defintions for valid or exclude folders
     *
     * @test
     */
    public function detectFilesDuringInitializationWithoutFolderDefinitions()
    {
        $this->importDataSetFromFixture('StorageContext/detects_files_during_initialization_without_folder_definition.xml');
        $this->placeTemporaryFile('file7777.gif', 'fileadmin');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin/exclude');
        $this->placeTemporaryFile('file7777.gif', 'fileadmin/exclude/exclude_sub');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin/exclude/exclude_sub');

        $this->getStorageContextDetector(Site::getFirstAvailableSite())->initializeQueue(array('fileadmin' => true));

        // check index queue
        $indexQueueContent = $this->itemRepository->findAll();

        $this->assertEquals(3, count($indexQueueContent), 'Number of file index queue entries is not as expected');
        $filesInQueue = array();
        foreach ($indexQueueContent as $queueItem) {
            $filesInQueue[] = $queueItem->getFile()->getUid();
        }
        $this->assertEquals(true, in_array(8888, $filesInQueue), 'File 8888 not detected');
        $this->assertEquals(true, in_array(88881, $filesInQueue), 'File 88881 not detected');
        $this->assertEquals(true, in_array(99991, $filesInQueue), 'File 99991 not detected');

        $this->removeTemporaryFiles();
    }

    /**
     * @param array $indexQueueContent
     * @param integer $uid
     */
    protected function assertIndexQueueContainsFileUid($indexQueueContent, $uid) {
        $isInQueue = false;
        foreach($indexQueueContent as $indexQueueItem) {
            if($indexQueueItem->getFile()->getUid() === $uid) {
                $isInQueue = true;
            }
        }

        $this->assertTrue($isInQueue, 'Asserting that the is an item in the queue for uid ' . $uid);
    }

    /**
     * Tests the file detection during index queue initialization
     *
     * @test
     */
    public function detectFilesDuringInitializationWithExcludedFolderDefinitions()
    {
        $this->importDataSetFromFixture('StorageContext/detects_files_during_initialization.xml');
        $this->placeTemporaryFile('file7777.gif', 'fileadmin');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin/exclude');
        $this->placeTemporaryFile('file7777.gif', 'fileadmin/exclude/exclude_sub');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin/exclude/exclude_sub');

        $this->getStorageContextDetector(Site::getFirstAvailableSite())->initializeQueue(array('fileadmin' => true));

        // check index queue
        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Number of file index queue entries is not as expected');
        $this->assertEquals(8888, $indexQueueContent[0]->getFile()->getUid(), 'Wrong file detected');

        $this->removeTemporaryFiles();
    }

    /**
     * Returns the storage context detector
     *
     * @param Site $site
     * @return \ApacheSolrForTypo3\Solrfal\Detection\StorageContextDetector
     */
    protected function getStorageContextDetector(Site $site)
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Detection\\StorageContextDetector', $site);
    }

    /**
     * @test
     */
    public function initializeStorageContext()
    {
        $this->importDataSetFromFixture('detects_files_in_storage_context.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $site = $this->getSite(1);
        $storageContextDetector = $this->getStorageContextDetector($site);
        $storageContextDetector->initializeQueue(array('fileadmin' => true));

        $this->assertEquals(2, $this->itemRepository->countBySiteAndIndexConfigurationName($site, 'fileadmin'), 'Number of file index queue entries is not as expexted (fileadmin)');
        $this->assertEquals(1, $this->itemRepository->countBySiteAndIndexConfigurationName($site, 'fileadmin2'), 'Number of file index queue entries is not as expexted (fileadmin2)');
        $this->assertEquals(3, count($this->itemRepository->findAll()), 'Total number of file index queue entries is not as expected');

        $this->removeTemporaryFiles();
    }
}
