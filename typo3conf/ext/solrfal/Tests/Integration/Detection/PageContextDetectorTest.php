<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solrfal\Queue\Queue;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Page context detector tests
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class PageContextDetectorTest extends \ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest
{

    /**
     * @test
     */
    public function detectContentDeletion()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array(), array('tt_content' => array(10 => array('delete' => 1))));
        $dataHandler->process_cmdmap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Index queue not updated as expected, deletion of sys_file_reference ignored.');
        $this->assertEquals(8888, $indexQueueContent[0]->getFile()->getUid(), 'Remaining file in index queue is not "file8888.pdf" as expected.');
    }

    /**
     * @test
     */
    public function detectRelationDeletion()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array(), array('sys_file_reference' => array(9999 => array('delete' => 1))));
        $dataHandler->process_cmdmap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Relation deletion ignored, file "file9999.txt" is still in queue.');
        $this->assertEquals(8888,  $indexQueueContent[0]->getFile()->getUid(), 'Remaining file in index queue is not "file8888.pdf" as expected.');
    }

    /**
     * @test
     */
    public function detectPageDeletion()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array(), array('pages' => array(2 => array('delete' => 1))));
        $dataHandler->process_cmdmap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(0, count($indexQueueContent), 'Page deletion didn\'t trigger the file deletion.');
    }

    /**
     * @test
     */
    public function detectContentHiding()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array('tt_content' => array(10 => array('hidden' => 1))), array());
        $dataHandler->process_datamap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Index queue not updated as expected, hiding of tt_content record ignored.');
        $this->assertEquals(8888, $indexQueueContent[0]->getFile()->getUid(), 'Remaining file in index queue is not "file8888.pdf" as expected.');
    }

    /**
     * @test
     */
    public function detectRelationHiding()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array('sys_file_reference' => array(9999 => array('hidden' => 1))), array());
        $dataHandler->process_datamap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Relation hiding ignored, file "file9999.txt" is still in queue.');
        $this->assertEquals(8888,  $indexQueueContent[0]->getFile()->getUid(), 'Remaining file in index queue is not "file8888.pdf" as expected.');
    }

    /**
     * @test
     */
    public function detectPageHiding()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array('pages' => array(2 => array('hidden' => 1))), array());
        $dataHandler->process_datamap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(0, count($indexQueueContent), 'Page hidding didn\'t trigger the file deletion.');
    }

    /**
     * This testcase checks if we can create a new testpage on the root level without any errors.
     *
     * @test
     */
    public function canCreateSiteOneRootLevel()
    {
        $this->importDataSetFromFixture('PageContext/observes_create.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $this->assertSolrQueueContainsAmountOfItems(0);
        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array('pages' => array('NEW' => array('hidden' => 0))), array());
        $dataHandler->process_datamap();

        // the item is outside a siteroot so we should not have any queue entry
        $this->assertSolrQueueContainsAmountOfItems(0);
    }

    /**
     * This testcase checks if we can create a new testpage on the root level without any errors.
     *
     * @test
     */
    public function canCreateSubPageBelowSiteRoot()
    {
        $this->importDataSetFromFixture('PageContext/observes_create.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $this->assertSolrQueueContainsAmountOfItems(0);
        $dataHandler = $this->getDataHandler();
        $dataHandler->start(array('pages' => array('NEW' => array('hidden' => 0, 'pid' => 1))), array());
        $dataHandler->process_datamap();

        // we should have one item in the solr queue
        $this->assertSolrQueueContainsAmountOfItems(1);
    }

    /**
     * @param integer $assertedItemCount
     */
    protected function assertSolrQueueContainsAmountOfItems($assertedItemCount)
    {
        /** @var $indexQueue Queue */
        $indexQueue = GeneralUtility::makeInstance(Queue::class);
        $this->assertSame($assertedItemCount, $indexQueue->getAllItemsCount(), 'EXT:solr index queue does not contain expected item amount');
    }
}
