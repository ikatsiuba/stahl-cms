<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\ResourceFactory;

/**
 * Page context tests
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class PageContextTest extends \ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest
{

    /**
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Returns the initialized page context
     *
     * @return \ApacheSolrForTypo3\Solrfal\Context\PageContext
     */
    protected function getPageContext()
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Context\\PageContext',
            GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\Site', 1),
            GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\Access\\Rootline'),
            1, // page uid
            'tt_content',
            'bodytext',
            10, // content element uid
            1 // language
        );
    }

    /**
     * @test
     */
    public function detectsTranslatedFileReferenceTitle()
    {
        $this->importDataSetFromFixture('detects_in_translated_page.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');

        // get dummy file
        try {
            $file = ResourceFactory::getInstance()->getFileObject(8888);
        } catch (\Exception $e) {
        }

        $this->assertEquals(true, ($file instanceof \TYPO3\CMS\Core\Resource\File));

        // check additional fields
        $pageContext = $this->getPageContext();
        $dynamicFields = $pageContext->getAdditionalDynamicDocumentFields($file);
        $this->assertEquals('Hallo Solr', $dynamicFields['fileReferenceTitle'], 'File reference title is not "Hallo Solr" as expected: ' . $dynamicFields['fileReferenceTitle']);
    }
}
