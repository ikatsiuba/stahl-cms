<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Service;

use ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolverAspectInterface;

class TestFileAttachmentResolverProcessor implements FileAttachmentResolverAspectInterface
{

    /**
     * @param array $fileUids
     * @param string $tableName
     * @param string $fieldName
     * @param array $record
     * @param \ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolver $fileAttachmentResolver
     * @return array
     */
    public function postDetectFilesInField($fileUids, $tableName, $fieldName, $record, \ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolver $fileAttachmentResolver)
    {
        $fileUids[] = 9999;
        return $fileUids;
    }
}
