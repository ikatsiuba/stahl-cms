<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Service;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Timo Schmidt <timo.schmidt@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class InitializationAspectTest
 */
class FileAttachmentResolverTest extends \ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest
{

    /**
     * @var array
     */
    protected $globalsBackup;

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolver
     */
    protected $fileAttachmentResolver;

    /**
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->fileAttachmentResolver = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Service\\FileAttachmentResolver');
    }

    /**
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function detectFilesInFieldCanBeModifiedWithAHook()
    {
        $this->globalsBackup = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solrfal'];
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');
        $this->importDataSetFromFixture('detects_file_in_ttcontent.xml');

        $hookClass = 'ApacheSolrForTypo3\\Solrfal\\Tests\\Integration\\Service\\TestFileAttachmentResolverProcessor';
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solrfal']['FileAttachmentResolverAspect'][] = $hookClass;

        $uids = $this->fileAttachmentResolver->detectFilesInField('tt_content', 'bodytext', array('bodytext' => 'this is a test <link file:8888>'));

        $this->assertContains(8888, $uids);
        $this->assertContains(9999, $uids);

        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solrfal'] = $this->globalsBackup;
    }

    /**
     * @test
     */
    public function detectFilesInFieldCanExtractFileFromFileLinkInContentWhenNoHookRegistered()
    {
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');
        $this->importDataSetFromFixture('detects_file_in_ttcontent.xml');

        $this->assertEmpty($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solrfal']['FileAttachmentResolverAspect']);
        $uids = $this->fileAttachmentResolver->detectFilesInField('tt_content', 'bodytext', array('bodytext' => 'this is a test <link file:8888>'));

        $this->assertContains(8888, $uids);
    }
}
