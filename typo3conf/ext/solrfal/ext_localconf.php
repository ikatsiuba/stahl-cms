<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

// Register initializing of the Index Queue for Files
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['postProcessIndexQueueInitialization']['solrfal'] = 'ApacheSolrForTypo3\\Solrfal\\Queue\\InitializationAspect';

// Register garbage collection
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['postProcessGarbageCollector']['fileGarbageCollector'] = 'ApacheSolrForTypo3\\Solrfal\\Queue\\ConsistencyAspect';

// When it is more easy to instanciate a different queue instance this can be replaced
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['ApacheSolrForTypo3\\Solr\\IndexQueue\\Queue'] = array(
	'className' => 'ApacheSolrForTypo3\\Solrfal\\Queue\\Queue'
);

/**********************************************
 *
 *  REGISTER Events in the FAL API
 *
 **********************************************/

/** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher');

$signalsToRegister = array(
	'Purge removed files' => array(
		'TYPO3\\CMS\\Core\\Resource\\ResourceStorage', \TYPO3\CMS\Core\Resource\ResourceStorage::SIGNAL_PostFileDelete,
		'ApacheSolrForTypo3\\Solrfal\\Queue\\ConsistencyAspect', 'removeDeletedFile'
	),
	'File Index Record created' => array(
		'TYPO3\\CMS\\Core\\Resource\\Index\\FileIndexRepository', 'recordCreated',
		'ApacheSolrForTypo3\\Solrfal\\Queue\\ConsistencyAspect', 'fileIndexRecordCreated'
	),
	'File Index Record updated' => array(
		'TYPO3\\CMS\\Core\\Resource\\Index\\FileIndexRepository', 'recordUpdated',
		'ApacheSolrForTypo3\\Solrfal\\Queue\\ConsistencyAspect', 'fileIndexRecordUpdated'
	),
	'File Index Record deleted' => array(
		'TYPO3\\CMS\\Core\\Resource\\Index\\FileIndexRepository', 'recordDeleted',
		'ApacheSolrForTypo3\\Solrfal\\Queue\\ConsistencyAspect', 'fileIndexRecordDeleted'
	),
	'File Index Record marked as missing' => array(
		'TYPO3\\CMS\\Core\\Resource\\Index\\FileIndexRepository', 'recordMarkedAsMissing',
		'TYPO3\\Solr\\Solrfal\\Queue\\ConsistencyAspect', 'removeMissingFile'
	)
);

foreach ($signalsToRegister as $parameters) {
	$signalSlotDispatcher->connect($parameters[0], $parameters[1], $parameters[2], $parameters[3]);
}

/****************************************
 *
 *  REGISTER Events internally
 *
 ****************************************/

$signalSlotDispatcher->connect(
	'ApacheSolrForTypo3\\Solrfal\\Queue\\ItemRepository', 'beforeItemRemoved',
	'ApacheSolrForTypo3\\Solrfal\\Queue\\ConsistencyAspect', 'removeSolrEntryForItem'
);

$signalSlotDispatcher->connect(
	'ApacheSolrForTypo3\\Solrfal\\Queue\\ItemRepository', 'beforeMultipleItemsRemoved',
	'ApacheSolrForTypo3\\Solrfal\\Queue\\ConsistencyAspect', 'removeMultipleQueueItemsFromSolr'
);


// adding scheduler tasks
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['ApacheSolrForTypo3\\Solrfal\\Scheduler\\IndexingTask'] = array(
	'extension'        => $_EXTKEY,
	'title'            => 'LLL:EXT:solrfal/Resources/Private/Language/locallang.xlf:scheduler.title',
	'description'      => 'LLL:EXT:solrfal/Resources/Private/Language/locallang.xlf:scheduler.description',
	'additionalFields' => 'ApacheSolrForTypo3\\Solrfal\\Scheduler\\IndexingTaskAdditionalFieldProvider'
);

if (TYPO3_MODE == 'FE' && isset($_SERVER['HTTP_X_TX_SOLR_IQ'])) {
	// register PageContext stuff
	$signalSlotDispatcher->connect(
		'TYPO3\\CMS\\Core\\Resource\\ResourceStorage',
		'preGeneratePublicUrl',
		'ApacheSolrForTypo3\\Solrfal\\Detection\\PageContextDetectorFrontendIndexingAspect',
		'registerGeneratedPublicUrl'
	);

	$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['Indexer']['indexPagePostProcessPageDocument'][] = 'ApacheSolrForTypo3\\Solrfal\\Detection\\PageContextDetectorFrontendIndexingAspect';
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_content.php']['postInit'][] = 'ApacheSolrForTypo3\\Solrfal\\Detection\\PageContextDetectorFrontendIndexingAspect';
}

$context = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();
if ($context->isProduction()) {
	$logLevel = \TYPO3\CMS\Core\Log\LogLevel::ERROR;
} else {
	$logLevel = \TYPO3\CMS\Core\Log\LogLevel::INFO;
}

$GLOBALS['TYPO3_CONF_VARS']['LOG']['ApacheSolrForTypo3']['Solrfal']['writerConfiguration'] = array(
	$logLevel => array(
		'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
			'logFile' => 'typo3temp/logs/solrfal.log'
		)
	),
);
