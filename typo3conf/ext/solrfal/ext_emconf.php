<?php
$EM_CONF[$_EXTKEY] = array(
	'title' => 'Apache Solr for TYPO3 - File Indexing',
	'description' => 'Add indexing for FileAbstractionLayer based files in TYPO3 CMS.',
	'category' => 'misc',
	'state' => 'stable',
	'uploadfolder' => '0',
	'createDirs' => '',
	'author' => 'Steffen Ritter, Ingo Renner, Timo Hund, Markus Friedrich',
	'author_email' => 'solr@dkd.de',
	'author_company' => 'dkd Internet Service GmbH',
	'clearCacheOnLoad' => 1,
	'version' => '4.0.0',
	'constraints' =>
	array(
		'depends' => array(
			'typo3' => '7.6.0-8.0.99',
			'solr' => '6.0.0-',
			'filemetadata' => '7.6.0-'
		),
		'conflicts' => array(),
		'suggests' => array(
			'tika' => '2.2.0-'
		),
	),
	'autoload' => array(
		'psr-4' => array(
			'ApacheSolrForTypo3\\Solrfal\\' => 'Classes'
		)
	),
	'autoload-dev' => array(
		'psr-4' => array(
			'ApacheSolrForTypo3\\Solrfal\\Tests\\' => 'Tests'
		)
	)
);
