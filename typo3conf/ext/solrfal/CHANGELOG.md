### Version 4.0.0

- Document merging to avoid duplicates of multiple files
- Performance optimization in AbstractRecordDetector
- Fixed TS path in default template plugin.tx_solr.index.queue.__TABLENAME__.fields.url
- Performance fix in backend: When content or pages are updated, now only the detectors for the affected site are triggered instead of all. This gives a huge performance boost on systems with many sites.

### Version 3.3.0

- Fix handling of access restrictions in record context if fe_group is not defined in enable columns

### Version 3.2.0

- Added possibility to configure webRoot or set it to PATH_site to allow having absRefPrefix auto in the TypoScript configuration
- Added improved observation of record, tt_content and sys_file_reference updates
- Add field variantId to the solr fields to allow variants for duplicates in EXT:solr

### Version 3.1.0

- Use configuration object from EXT:solr without array access operators
- Use correct var to load storage configuration in StorageContextDetector
- Missing namespace change in RecordContextDetector
- Add configuration options for valid and exclude folders in storage context
- Respect boolean clause limit and site
- Skip storage permission evaluation while running the indexing task "File Index Queue Worker"
- Use translated page for file reference information

### Version 3.0.0

- Set EXT:solr dependency to 4.0.0 since TypoScript configuration object is not available until version 4
- Removed usage of deprecated method to get the table name ("getTableToIndexByIndexingConfigurationName()")  and switched to TypoScript configuration object
- Fix file detection in record context to ensure file detection in translated records
- Added validity check to the file attachment resolver, to ensure that hidden references and missing files are not added to the index queue
- Registered new signal to remove files marked as missing
- Updates of sys_file records are respected
- Add file extension configuration option to record context
- Added check for non existing index queue items on updates to storage context detector
- Fix incomplete definition of index configuration name in storage context

### Version 2.1.3

- Fixed typo in __RecordContext configuration example

### Version 2.1.2

- Added hook in FileAttachmentResolver (FileAttachmentResolverAspectInterface) that can be used to modify the result of
"detectFilesInField"
- Fix index queue initialization, since the site root wasn't considered entries of all sites were deleted or files of
other site roots got detected
- ContextFactory provided context record field name instead of expected index configuration name. Fixed this by using the right database column
- Fixed Bug that space in "plugin.tx_solr.enableFileIndexing.pageContext.fileExtensions", was not handled properly

### Version 2.1.1

- Set dependency of EXT:solr to 3.1.1 because 3.1.0 contains an invalid composer.json file without a version number
