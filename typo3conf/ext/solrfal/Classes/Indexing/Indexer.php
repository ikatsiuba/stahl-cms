<?php
namespace ApacheSolrForTypo3\Solrfal\Indexing;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solrfal\Context\ContextInterface;
use ApacheSolrForTypo3\Solrfal\Queue\Item;
use ApacheSolrForTypo3\Solrfal\Queue\ItemGroup;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\SingletonInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use ApacheSolrForTypo3\Solr\Site;

/**
 * Class Indexer
 */
class Indexer implements SingletonInterface
{

    /**
     * @param int $limit
     * @param bool $evaluatePermissions
     * @return void
     */
    public function processIndexQueue($limit = 50, $evaluatePermissions = true)
    {
        $itemGroups = $this->getItemGroupRepository()->findAllIndexingOutStanding($limit);

        foreach ($itemGroups as $itemGroup) {
            foreach ($itemGroup->getItems() as $item) {
                $item->getFile()->getStorage()->setEvaluatePermissions($evaluatePermissions);
            }
            $this->addGroupToIndex($itemGroup);
        }
    }

    /**
     * @param Item $item
     *
     * @return bool
     */
    public function addToIndex(Item $item)
    {
        $group = $this->getItemGroupRepository()->findByItem($item);
        return $this->addGroupToIndex($group);
    }

    /**
     * Adds a group of queue items to the solr index and applies merging or not
     * @param ItemGroup $group
     * @return bool
     */
    protected function addGroupToIndex(ItemGroup $group)
    {
        $success = false;

        if (!$group->getRootItem()->getFile()->exists()) {
            $this->getItemRepository()->markFailed($group->getRootItem(), 'File missing');
            return $success;
        }

        $merge = $this->getIsMergingEnabledFormItem($group->getRootItem());
        $documents = $this->getDocumentFactory()->createDocumentsForQueueItemGroup($group, $merge);
        $solr = $this->getSolrServiceByContext($group->getRootItem()->getContext());

        foreach ($documents as $document) {
            unset($document->teaser);

            // todo document why teaser is unset
            $response = $solr->addDocument($document);
            $this->emitIndexedFileToSolr($group->getRootItem()->getFile());

            if ($response->getHttpStatus() == 200) {
                $success = true;
                foreach ($group->getItems() as $item) {
                    $this->getItemRepository()->markIndexedSuccessfully($item);
                }
            } else {
                foreach ($group->getItems() as $item) {
                    $this->getItemRepository()->markFailed($item,
                        $response->getHttpStatusMessage() . ': ' . $response->getRawResponse());
                }
            }
        }

        return $success;
    }

    /**
     * Removes an document from the solr index, which relates to an queueitem
     *
     * @param Item $item
     * @return void
     */
    public function removeFromIndex(Item $item)
    {
        $merge = $this->getIsMergingEnabledFormItem($item);
        if ($merge) {
            // get group for item
            $group = $this->getItemGroupRepository()->findByItem($item);

            // we know that the group is not empty
            $wasRootItem = $group->getIsRootItem($item);
            $group->remove($item);
            $groupIsEmptyAfterRemove = $group->getIsEmpty();

            if ($wasRootItem || $groupIsEmptyAfterRemove) {
                // the item was the root document or the group is empty now => we can remove it from solr
                $this->removeFromIndexByItem($item);
            }

            if (!$groupIsEmptyAfterRemove) {
                $this->addGroupToIndex($group);
            }
        } else {
            $this->removeFromIndexByItem($item);
        }
    }

    /**
     * @param Item $item
     * @return mixed
     */
    protected function getIsMergingEnabledFormItem(Item $item)
    {
        $configuration = $item->getContext()->getSite()->getSolrConfiguration();
        $merge = $configuration->getValueByPathOrDefaultValue('plugin.tx_solr.index.enableFileIndexing.mergeDuplicates', false);

        return filter_var($merge, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @param Item $item
     */
    protected function removeFromIndexByItem(Item $item)
    {
        $solr = $this->getSolrServiceByContext($item->getContext());
        // build query, need to differentiate for the case when deleting whole pages
        $query = array('type:' . DocumentFactory::SOLR_TYPE, 'uid:' . $item->getUid());

        // delete document(s) from index, directly commit
        $solr->deleteByQuery(implode(' AND ', $query));
    }

    /**
     * @param $uidArray
     *
     * @deprecated since 3.1, use removeByQueueEntriesAndSite which considers the site and valid number of boolean clauses, will be removed in v3.3
     */
    public function removeByQueueEntriesByUid($uidArray)
    {
        GeneralUtility::logDeprecatedFunction();

        $uidQueryPart = array();
        foreach ($uidArray as $uid) {
            $uidQueryPart[] = 'uid:' . $uid;
        }
        $query = array(
            'type:' . DocumentFactory::SOLR_TYPE,
            '(' . implode(' OR ', $uidQueryPart) . ')'
        );

        $solrConnections = $this->getAllSolrServices();

        foreach ($solrConnections as $solr) {
            $solr->deleteByQuery(implode(' AND ', $query));
        }
    }

    /**
     * @param $uidArray
     * @param Site
     * @param $chunkSize
     * @return void
     *
     * @throws \Apache_Solr_HttpTransportException If a non 200 response status is returned
     */
    public function removeByQueueEntriesAndSite($uidArray, Site $site, $chunkSize = 1000)
    {
        $chunks = array_chunk($uidArray, $chunkSize);
        $solrConnections = $this->getSolrServicesBySite($site);

        foreach ($chunks as $chunk) {
            $uidQueryPart = array();
            foreach ($chunk as $uid) {
                $uidQueryPart[] = 'uid:' . $uid;
                $this->indexNewRootDocumentWhenRequired($uid);
            }

            $query = array(
                'siteHash:' . $site->getSiteHash(),
                'type:' . DocumentFactory::SOLR_TYPE,
                '(' . implode(' OR ', $uidQueryPart) . ')'
            );
            foreach ($solrConnections as $solr) {
                $solrResponse = $solr->deleteByQuery(implode(' AND ', $query));
                if ($solrResponse->getHttpStatus() != 200) {
                    throw new \Apache_Solr_HttpTransportException($solrResponse);
                }
            }
        }
    }

    /**
     * @return DocumentFactory
     */
    protected function getDocumentFactory()
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Indexing\\DocumentFactory');
    }

    /**
     * @param ContextInterface $context
     * @return \ApacheSolrForTypo3\Solr\SolrService
     */
    protected function getSolrServiceByContext(ContextInterface $context)
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\ConnectionManager')
            ->getConnectionByRootPageId($context->getSite()->getRootPageId(), $context->getLanguage());
    }

    /**
     * @return \ApacheSolrForTypo3\Solr\SolrService[]
     */
    protected function getAllSolrServices()
    {
        $connectionManager = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\ConnectionManager');
        return $connectionManager->getAllConnections();
    }

    /**
     * @param Site
     * @return \ApacheSolrForTypo3\Solr\SolrService[]
     */
    protected function getSolrServicesBySite(Site $site)
    {
        $connectionManager = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\ConnectionManager');
        return $connectionManager->getConnectionsBySite($site);
    }

    /**
     * @return \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository
     */
    protected function getItemRepository()
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Queue\\ItemRepository');
    }


    /**
     * @return \ApacheSolrForTypo3\Solrfal\Queue\ItemGroupRepository
     */
    protected function getItemGroupRepository()
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Queue\\ItemGroupRepository');
    }

    /**
     * @param File $file
     * @return void
     */
    protected function emitIndexedFileToSolr(File $file)
    {
        /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher');
        $signalSlotDispatcher->dispatch(__CLASS__, 'indexedFileToSolr', array($file));
    }

    /**
     * When merging is active and the root document of a mergeGroup is removed,
     * the mergeGroup needs to be re-added to make sure a noew root document is determined.
     *
     * @param integer $uid
     */
    private function indexNewRootDocumentWhenRequired($uid)
    {
        $item = $this->getItemRepository()->findByUid($uid);
        if (is_null($item) || ! $this->getIsMergingEnabledFormItem($item)) {
            return;
        }

        $group = $this->getItemGroupRepository()->findByItem($item);
        if (!$group->getIsRootItem($item)) {
            return;
        }

        $group->remove($item);
        if ($group->getIsEmpty()) {
            return;
        }

        // there are items left after removing, so we need to re-index the rest of the group
        $this->addGroupToIndex($group);
    }
}
