<?php
namespace ApacheSolrForTypo3\Solrfal\Scheduler;

/***************************************************************
 * Copyright notice
 *
 * (c) 2010-2011 Markus Goldbach <markus.goldbach@dkd.de>
 * (c) 2012 Ingo Renner <ingo@typo3.org>
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface;
use \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

/**
 * Additional field provider for the IndexingTask
 */
class IndexingTaskAdditionalFieldProvider implements AdditionalFieldProviderInterface
{

    /**
     * @param array $taskInfo
     * @param IndexingTask $task
     * @param SchedulerModuleController $schedulerModule
     *
     * @return array
     */
    public function getAdditionalFields(array &$taskInfo, $task, SchedulerModuleController $schedulerModule)
    {
        $additionalFields = array();

        // set default value
        if ($schedulerModule->CMD == 'add') {
            $taskInfo['filesToIndexLimit'] = 10;
            $taskInfo['forcedWebRoot'] = '';
        }

        if ($schedulerModule->CMD == 'edit') {
            $taskInfo['filesToIndexLimit'] = $task->getFileCountLimit();
            $taskInfo['forcedWebRoot'] = $task->getForcedWebRoot();
        }

        $additionalFields['filesToIndexLimit'] = array(
            'code'     => '<input type="text" name="tx_scheduler[filesToIndexLimit]" value="' . intval($taskInfo['filesToIndexLimit']) . '" />',
            'label'    => 'LLL:EXT:solrfal/Resources/Private/Language/locallang.xlf:scheduler.fields.filesToIndexLimit',
            'cshKey'   => '',
            'cshLabel' => ''
        );

        $additionalFields['forcedWebRoot'] = array(
            'code' => '<input type="text" name="tx_scheduler[forcedWebRoot]" value="' . htmlspecialchars($taskInfo['forcedWebRoot']) . '" />',
            'label' => 'LLL:EXT:solrfal/Resources/Private/Language/locallang.xlf:scheduler.fields.forcedWebRoot',
            'cshKey' => '',
            'cshLabel' => ''
        );


        return $additionalFields;
    }

    /**
     * Checks any additional data that is relevant to this task. If the task
     * class is not relevant, the method is expected to return true
     *
     * @param array	$submittedData reference to the array containing the data submitted by the user
     * @param SchedulerModuleController	$schedulerModule reference to the calling object (Scheduler's BE module)
     * @return boolean TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
     */
    public function validateAdditionalFields(array &$submittedData, SchedulerModuleController $schedulerModule)
    {
        if (!MathUtility::canBeInterpretedAsInteger($submittedData['filesToIndexLimit'])) {
            return false;
        }
        // check limit
        $submittedData['filesToIndexLimit'] = intval($submittedData['filesToIndexLimit']);
        return true;
    }

    /**
     * Saves any additional input into the current task object if the task
     * class matches.
     *
     * @param array $submittedData array containing the data submitted by the user
     * @param AbstractTask $task reference to the current task object
     * @return void
     */
    public function saveAdditionalFields(array $submittedData, AbstractTask $task)
    {
        $task->setFileCountLimit($submittedData['filesToIndexLimit']);
        $task->setForcedWebRoot($submittedData['forcedWebRoot']);
    }
}
