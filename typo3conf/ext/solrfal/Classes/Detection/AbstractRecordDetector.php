<?php
namespace ApacheSolrForTypo3\Solrfal\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\System\Configuration\TypoScriptConfiguration;
use ApacheSolrForTypo3\Solrfal\Context\ContextInterface;
use ApacheSolrForTypo3\Solrfal\Indexing\DocumentFactory;
use ApacheSolrForTypo3\Solrfal\Queue\Item;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\File;

/**
 * Class AbstractRecordDetector
 */
abstract class AbstractRecordDetector implements RecordDetectionInterface
{

    /**
     * @var Site
     */
    protected $site;

    /**
     * @var array
     */
    protected $siteConfiguration;

    /**
     * @var \TYPO3\CMS\Core\Log\Logger
     */
    protected $logger = null;


    /**
     * @param Site $site
     * @param \ApacheSolrForTypo3\Solr\System\Configuration\TypoScriptConfiguration $siteConfiguration
     */
    public function __construct(Site $site, $siteConfiguration = null)
    {
        $this->site              = $site;
        $this->siteConfiguration = is_null($siteConfiguration) ? $site->getSolrConfiguration() : $siteConfiguration;
        $this->logger            = $this->getLogger();
    }

    /**
     * @return Logger
     */
    protected function getLogger()
    {
        if ($this->logger === null) {
            $this->logger = GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
        }

        return $this->logger;
    }

    /**
     * Checks if the Indexing is Enabled
     *
     * @param string $context ContextType
     *
     * @return bool
     */
    protected function isIndexingEnabledForContext($context)
    {
        $contextConfig = $this->siteConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.enableFileIndexing.', []);
        return !empty($contextConfig[$context . 'Context']);
    }

    /**
     * @return \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository
     */
    protected function getItemRepository()
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Queue\\ItemRepository');
    }

    /**
     * @return \TYPO3\CMS\Core\Resource\StorageRepository
     */
    protected function getStorageRepository()
    {
        return GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
    }

    /**
     * @return \ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolver
     */
    protected function getFileAttachmentResolver()
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolver');
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * Returns the reference index entry repository
     *
     * @return \ApacheSolrForTypo3\Solrfal\Domain\Repository\ReferenceIndexEntryRepository
     */
    protected function getReferenceIndexEntryRepository()
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solrfal\\Domain\\Repository\\ReferenceIndexEntryRepository');
    }

    /**
     * Returns the indexing queue
     *
     * @return \ApacheSolrForTypo3\Solr\IndexQueue\Queue
     */
    protected function getIndexQueue()
    {
        return GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\IndexQueue\\Queue');
    }

    /**
     * Returns a file object
     *
     * @param integer $fileUid
     *
     * @return \TYPO3\CMS\Core\Resource\File
     */
    protected function getFile($fileUid)
    {
        $file = null;

        try {
            $file = ResourceFactory::getInstance()->getFileObject((int)$fileUid);
        } catch (ResourceDoesNotExistException $e) {
            $this->logger->error('File not found: ' . $fileUid);
        } catch (\Exception $e) {
            $this->logger->error('Unknown exception while loading file: ' . $fileUid);
        }

        return $file;
    }

    /**
     * Returns a file reference object
     *
     * We use this own method since ResourceFactory caches the
     * file reference objects and we cannot be sure that this
     * object is up-to-date
     *
     * @param integer $fileReferenceUid
     *
     * @return \TYPO3\CMS\Core\Resource\FileReference
     */
    protected function getFileReferenceObject($fileReferenceUid)
    {
        $fileReference = null;

        try {
            $fileReferenceData = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('sys_file_reference', $fileReferenceUid);
            $fileReference = ResourceFactory::getInstance()->createFileReferenceObject($fileReferenceData);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $fileReference;
    }

    /**
     * Creates a new queue item
     *
     * @param File $file
     * @param ContextInterface $context
     *
     * @return Item
     */
    protected function createQueueItem(File $file, ContextInterface $context)
    {
        /** @var $item Item */
        $item = GeneralUtility::makeInstance(Item::class, $file, $context);

        // a file should be unique per file, language and site
        $mergeId =  DocumentFactory::SOLR_TYPE . '/' .
                    $file->getUid() . '/' .
                    $context->getLanguage() . '/' .
                    $context->getSite()->getRootPageId();

        $item->setMergeId($mergeId);

        return $item;
    }

    /**
     * Handles new sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordCreated($table, $uid)
    {
        // handle creation of sys_file records, presumably only relevant for storage context
    }

    /**
     * Handles deletions of sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordDeleted($table, $uid)
    {
        // TODO: check if action is required, since file deletion is also taken care of via Slot postFileDelete
        $this->getItemRepository()->removeByFileUid($uid);
    }
}
