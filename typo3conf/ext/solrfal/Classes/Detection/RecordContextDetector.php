<?php
namespace ApacheSolrForTypo3\Solrfal\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solrfal\Context\RecordContext;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ResourceFactory;

/**
 * ClassRecordContextDetector
 */
class RecordContextDetector extends AbstractRecordDetector
{

    /**
     * @param array $initializationStatus
     * @return void
     */
    public function initializeQueue(array $initializationStatus)
    {
        // only readd them if indexing is enabled
        if ($this->isIndexingEnabledForContext('record')) {
            foreach ($initializationStatus as $indexingConfigurationKey => $successfulInitialized) {
                $tableName = $indexingConfigurationKey;
                $indexConfig = $this->siteConfiguration->getObjectByPathOrDefault(
                    'plugin.tx_solr.index.queue.' . $indexingConfigurationKey . '.',
                    []
                );
                if (!empty($indexConfig['table'])) {
                    $tableName = $indexConfig['table'];
                }
                // remove relevant queue entries
                $this->getItemRepository()->removeByIndexingConfigurationInRecordContext($this->site, $indexingConfigurationKey);
                if ($successfulInitialized && $this->isFileExtractionEnabledForIndexingConfiguration($indexingConfigurationKey)) {
                    $this->initializeQueueForConfiguration($indexingConfigurationKey, $tableName);
                }
            }
        } else {
            // if it's disabled, remove everything
            $this->getItemRepository()->removeBySiteAndContext($this->site, 'record');
        }
    }

    /**
     * @param string $indexingConfiguration
     * @param string $tableName
     *
     * @return void
     */
    protected function initializeQueueForConfiguration($indexingConfiguration, $tableName)
    {
        $indexedRecords = array_keys($this->getDatabaseConnection()->exec_SELECTgetRows(
            'item_uid',
            'tx_solr_indexqueue_item',
            'indexing_configuration = ' . $this->getDatabaseConnection()->fullQuoteStr($indexingConfiguration, 'tx_solr_indexqueue_item')
                . ' AND root= ' . (int) $this->site->getRootPageId(),
            '',
            '',
            '',
            'item_uid'
        ));
        array_walk($indexedRecords, 'intval');

        if ($indexedRecords !== array()) {
            $fields = $this->getFieldsToIndex($indexingConfiguration, $tableName);
            $records = $this->getDatabaseConnection()->exec_SELECTgetRows(
                '*',
                $tableName,
                'uid IN (' . implode(',', $indexedRecords) . ')'
            );

            foreach ($records as $record) {
                $this->extractQueueItemsFromRecord($indexingConfiguration, $tableName, $record, $fields);
            }
        }
    }

    /**
     * Calculates the fields which should be searched for files to index
     *
     * @param string $indexingConfiguration
     * @param string $tableName
     *
     * @return array
     */
    protected function getFieldsToIndex($indexingConfiguration, $tableName)
    {
        $attachmentConfiguration = $this->siteConfiguration->getObjectByPathOrDefault(
            'plugin.tx_solr.index.queue.' . $indexingConfiguration . '.attachments.',
            []
        );
        $fields = array_keys($this->getDatabaseConnection()->admin_get_fields($tableName));
        if (is_array($attachmentConfiguration) && array_key_exists('fields', $attachmentConfiguration)) {
            $fieldConfig = trim($attachmentConfiguration['fields']);
            if ($fieldConfig !== '*') {
                $requestedFields = GeneralUtility::trimExplode(',', $fieldConfig);
                $fields = array_intersect($fields, $requestedFields);
            }
        }
        return $fields;
    }

    /**
     * @param string $indexingConfiguration
     * @param string $tableName
     * @param array $record
     * @param array $fieldsToExtract
     * @return void
     */
    protected function extractQueueItemsFromRecord($indexingConfiguration, $tableName, array $record, array $fieldsToExtract)
    {
        $accessRootline = Rootline::getAccessRootlineByPageId($this->site->getRootPageId());
        $languagesToIndex = $this->site->getLanguages();

        /** \TYPO3\CMS\Frontend\Page\PageRepository $page */
        $page = GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
        $page->init(false);

        $this->extractQueueItemsForSingleTranslation(0, $indexingConfiguration, $tableName, $record, $fieldsToExtract, $accessRootline);
        foreach ($languagesToIndex as $language) {
            if ($language == 0) {
                continue;
            }
            $translation = $page->getRecordOverlay(
                $tableName,
                $record,
                (int)$language,
                'hideNonTranslated'
            );
            if ($translation) {
                $this->extractQueueItemsForSingleTranslation($language, $indexingConfiguration, $tableName, $translation, $fieldsToExtract, $accessRootline);
            }
        }
    }

    /**
     * @param string $tableName
     * @return boolean
     */
    protected function isFileExtractionEnabledForTable($tableName)
    {
        $enabled = $this->isFileExtractionEnabledForIndexingConfiguration($tableName);
        $queueConfig = $this->siteConfiguration->getObjectByPathOrDefault(
            'plugin.tx_solr.index.queue.',
            []
        );
        if (!$enabled && !is_array($queueConfig[$tableName . '.'])) {
            foreach ($queueConfig as $configuration) {
                if (is_array($configuration) && isset($configuration['table']) && $configuration['table'] == $tableName) {
                    $enabled = $configuration['attachments'] == 1;
                    break;
                }
            }
        }
        return $enabled;
    }

    /**
     * @param string $configurationKey
     * @return boolean
     */
    protected function isFileExtractionEnabledForIndexingConfiguration($configurationKey)
    {
        $queueConfig = $this->siteConfiguration->getObjectByPathOrDefault(
            'plugin.tx_solr.index.queue.' . $configurationKey . '.',
            []
        );
        return !empty($queueConfig['attachments']);
    }

    /**
     * Check if the file extension is configured to be allowed for indexing
     *
     * @param File $file
     * @param string $indexingConfiguration
     *
     * @return bool
     */
    protected function isAllowedFileExtension(File $file, $indexingConfiguration = '')
    {
        $config = [];

        // get file extensions
        if ($indexingConfiguration !== '') {
            $config = $this->siteConfiguration->getObjectByPathOrDefault(
                'plugin.tx_solr.index.queue.' . $indexingConfiguration . '.attachments.',
                []
            );
        }

        // fileExtensions not set on context level check global index config
        if (!isset($config['fileExtensions'])) {
            $config = $this->siteConfiguration->getObjectByPathOrDefault(
                'plugin.tx_solr.index.enableFileIndexing.recordContext.',
                []
            );
        }

        $extensions = isset($config['fileExtensions']) ? $config['fileExtensions'] : '*';

        // evaluate extension list
        $extensions = strtolower(trim($extensions));
        if ($extensions === '*') {
            $isAllowedFileExtension = true;
        } else {
            $isAllowedFileExtension = GeneralUtility::inList($extensions, $file->getExtension());
        }

        return $isAllowedFileExtension;
    }

    /**
     * Returns the fields to index for given table
     *
     * @param string $tableName
     * @return array
     */
    protected function getIndexingConfigurationsForTable($tableName)
    {
        $indexingConfigurations = array();
        $queueConfig = $this->siteConfiguration->getObjectByPathOrDefault(
            'plugin.tx_solr.index.queue.',
            []
        );
        foreach ($queueConfig as $configurationName => $configuration) {
            if (
                is_array($configuration)
                && ($configurationName == $tableName || (isset($configuration['table']) && $configuration['table'] == $tableName))
                && $configuration['attachments'] == 1
            ) {
                $indexingConfigurations[] = rtrim($configurationName, '.');
            }
        }

        return $indexingConfigurations;
    }

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordCreated($table, $uid)
    {
        if ($this->isIndexingEnabledForContext('record') && $this->isFileExtractionEnabledForTable($table)) {

            // fix record uid if record is a translation
            $originalRecordUid = $uid;
            if (
                isset($GLOBALS['TCA'][$table]['ctrl']['languageField'])
                && ($record = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord($table, $uid))
                && $record[$GLOBALS['TCA'][$table]['ctrl']['languageField']] > 0
            ) {
                $originalRecordUid = (int)$record[$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']];
            }

            $recordInIndexQueue = $this->getDatabaseConnection()->exec_SELECTgetSingleRow(
                'indexing_configuration',
                'tx_solr_indexqueue_item',
                'item_type = ' . $this->getDatabaseConnection()->fullQuoteStr($table, 'tx_solr_indexqueue_item') .
                ' AND item_uid = ' . (int)$originalRecordUid
            );

            if ($recordInIndexQueue) {
                $indexingConfiguration = $recordInIndexQueue['indexing_configuration'];
                $record = $this->getDatabaseConnection()->exec_SELECTgetSingleRow('*', $table, 'uid = ' . (int)$originalRecordUid);
                $this->extractQueueItemsFromRecord(
                    $indexingConfiguration,
                    $table,
                    $record,
                    $this->getFieldsToIndex($indexingConfiguration, $table)
                );
            } else {
                $this->getItemRepository()->removeByTableAndUidInContext('record', $this->site, $table, $uid);
            }
        }
    }

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordUpdated($table, $uid)
    {
        // get referencing record if file reference was updated
        if ($table == 'sys_file_reference') {
            $fileReference = $this->getFileReferenceObject($uid);
            $table = $fileReference->getReferenceProperty('tablenames');
            $uid = $fileReference->getReferenceProperty('uid_foreign');
        }

        $this->recordCreated($table, $uid);
    }

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordDeleted($table, $uid)
    {
        if ($table == 'sys_file_reference') {
            // get reference and try to delete files of referencing records
            // Note that processCmdmap_preProcess must be used to call recordDeleted, otherwise
            // the file reference will not be available

            $fileReference = $this->getFileReferenceObject($uid);
            $this->getItemRepository()->removeByTableAndUidInContext(
                'record',
                $this->site,
                $fileReference->getReferenceProperty('tablenames'),
                $fileReference->getReferenceProperty('uid_foreign'),
                $fileReference->getReferenceProperty('uid_local')
            );
        } else {
            $this->getItemRepository()->removeByTableAndUidInContext('record', $this->site, $table, $uid);
        }
    }

    /**
     * Handles updates of sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordUpdated($table, $uid)
    {
        // skip if record context is not enabled
        if (!$this->isIndexingEnabledForContext('record')) {
            return;
        }

        if ($table == 'sys_file') {
            $indexRecords = $this->getReferencedRecords($uid);
            foreach ($indexRecords as $recordData) {
                $this->recordCreated($recordData['table'], $recordData['uid']);
            }
        }
    }

    /**
     * Returns referenced records that have to be updated
     *
     * @param integer $fileUid
     *
     * @return array
     */
    protected function getReferencedRecords($fileUid)
    {
        $indexRecords = array();
        $referenceIndexRepository = $this->getReferenceIndexEntryRepository();

        // get and process references
        // skip reference in sys_file_metadata, assuming that record context will not be used to indexed this table
        // and pages and pages_language_overlay tables are considered in page context
        $references = $referenceIndexRepository->findByReferenceRecord('sys_file', $fileUid, array('sys_file_metadata', 'pages', 'pages_language_overlay'));
        foreach ($references as $reference) {

            // try to get right reference to index record if reference refers to sys_file_reference
            if ($reference->getTableName() == 'sys_file_reference') {
                // get record reference if this is only a reference to sys_file_reference
                $reference = $referenceIndexRepository->findOneByReferenceIndexEntry($reference, array('sys_file_metadata', 'pages', 'pages_language_overlay'));
                if (!$reference) {
                    continue;
                }
            }

            $indexingConfigurations = $this->getIndexingConfigurationsForTable($reference->getTableName());
            if ($indexingConfigurations) {
                foreach ($indexingConfigurations as $indexingConfiguration) {
                    $fieldsToIndex = $this->getFieldsToIndex($indexingConfiguration, $reference->getTableName());
                    if (in_array($reference->getTableField(), $fieldsToIndex)) {
                        $indexRecords[$reference->getRecordHash()] = array('table' => $reference->getTableName(), 'uid' => $reference->getRecordUid());
                    }
                }
            }
        }

        return $indexRecords;
    }

    /**
     * @param integer $language
     * @param string $indexingConfiguration
     * @param $tableName
     * @param array $record
     * @param array $fieldsToExtract
     * @param \ApacheSolrForTypo3\Solr\Access\Rootline $accessRootline
     * @return void
     */
    protected function extractQueueItemsForSingleTranslation($language, $indexingConfiguration, $tableName, array $record, array $fieldsToExtract, \ApacheSolrForTypo3\Solr\Access\Rootline $accessRootline)
    {
        if (isset($GLOBALS['TCA'][$tableName]['ctrl']['enablecolumns']) && isset($GLOBALS['TCA'][$tableName]['ctrl']['enablecolumns']['fe_group'])) {
            $groupAccess = $record[$GLOBALS['TCA'][$tableName]['ctrl']['enablecolumns']['fe_group']];
        } else {
            $groupAccess = 0;
        }
        $currentRootline = clone $accessRootline;
        $accessRootlineElement = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\Access\\RootlineElement', 'r:' . $groupAccess);
        $currentRootline->push($accessRootlineElement);

        foreach ($fieldsToExtract as $fieldName) {
            $fileUids = $this->getFileAttachmentResolver()->detectFilesInField($tableName, $fieldName, $record);
            // todo: what to do if file exists multiple times in different fields
            $successfulUids = array();

            $context = new \ApacheSolrForTypo3\Solrfal\Context\RecordContext(
                $this->site,
                $currentRootline,
                $tableName,
                $fieldName,
                $record['uid'],
                $indexingConfiguration,
                $language
            );
            foreach ($fileUids as $fileUid) {
                try {
                    $file = ResourceFactory::getInstance()->getFileObject($fileUid);
                    if ($this->isAllowedFileExtension($file, $indexingConfiguration)) {
                        $indexQueueItem = $this->createQueueItem($file, $context);
                        if (!$this->getItemRepository()->exists($indexQueueItem)) {
                            $this->getItemRepository()->add($indexQueueItem);
                        } else {
                            $this->getItemRepository()->update($indexQueueItem);
                        }
                        $successfulUids[] = $fileUid;
                    }
                } catch (\TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException $e) {
                    continue;
                }
            }
            $this->getItemRepository()->removeOldEntriesFromFieldInRecordContext($this->site, $tableName, $record['uid'], $language, $fieldName, $successfulUids);
        }
    }
}
