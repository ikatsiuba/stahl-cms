<?php
namespace ApacheSolrForTypo3\Solrfal\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solrfal\Context\StorageContext;
use ApacheSolrForTypo3\Solrfal\Queue\Item;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class StorageContextDetector
 */
class StorageContextDetector extends AbstractRecordDetector
{

    /**
     * Folders
     *
     * @var array <storageUid> => <array of valid folders>
     */
    protected $folders = array();

    /**
     * Exclude folders
     *
     * @var array <storageUid> => <array of excluded folders>
     */
    protected $excludeFolders = array();

    /**
     * @param array $initializationStatus
     *
     * @return void
     */
    public function initializeQueue(array $initializationStatus)
    {
        if (!empty($initializationStatus)) {
            // only read them if indexing is enabled
            if ($this->isIndexingEnabledForContext('storage')) {
                foreach ($initializationStatus as $indexingConfigurationIndex => $initializationResult) {
                    $config = $this->siteConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.queue.', []);
                    if (
                        isset($config[$indexingConfigurationIndex . '.']) &&
                        is_array($config[$indexingConfigurationIndex . '.']) &&
                        isset($config[$indexingConfigurationIndex . '.']['table']) &&
                        $config[$indexingConfigurationIndex . '.']['table'] === 'sys_file_storage' &&
                        isset($config[$indexingConfigurationIndex . '.']['storageUid'])
                    ) {
                        $fileStorageUid = (int)$config[$indexingConfigurationIndex . '.']['storageUid'];
                        // remove relevant queue entries
                        $this->getLogger()->info('Purging index-queue for storage ' . $fileStorageUid);
                        $this->getItemRepository()->removeByFileStorage($this->site, $fileStorageUid, $indexingConfigurationIndex);

                        $storage = $this->getStorageRepository()->findByUid($fileStorageUid);
                        if ($this->isIndexingEnabledForStorage($fileStorageUid) && $storage !== null) {
                            $this->getLogger()->info('Indexing storage ' . $fileStorageUid . ' is enabled for site ' . $this->site->getSiteHash());
                            $this->initializeQueueForStorage($storage, $indexingConfigurationIndex);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     *
     * @param ResourceStorage $storage
     * @param string $indexingConfigurationIndex
     * @return void
     */
    protected function initializeQueueForStorage(ResourceStorage $storage, $indexingConfigurationIndex = '')
    {
        $this->logger->debug('Starting indexing storage ' . $storage->getUid());
        $contexts = $this->getLanguageContextsForStorage($storage, $indexingConfigurationIndex);
        $fileExtensions = $this->getFileExtensionsToIndex($storage);

        $rootLevelFolder = $storage->getRootLevelFolder();
        if (count($fileExtensions)) {
            /** @var \TYPO3\CMS\Core\Resource\Filter\FileExtensionFilter $filter */
            $filter = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter');
            $filter->setAllowedFileExtensions($fileExtensions);
            $rootLevelFolder->setFileAndFolderNameFilters(array(array($filter, 'filterFileList')));
        }

        $itemRepository = $this->getItemRepository();
        $files = $rootLevelFolder->getFiles(0, 0, Folder::FILTER_MODE_USE_OWN_FILTERS, true);
        foreach ($files as $file) {
            if (($file instanceof ProcessedFile) || !$this->isAllowedFilePath($file)) {
                continue;
            }
            $this->logger->debug('Found file ' . $file->getCombinedIdentifier() . ' to be added to index-queue');
            foreach ($contexts as $context) {
                $itemRepository->add(
                    $this->createQueueItem(
                        $file,
                        $context
                    )
                );
            }
        }
    }

    /**
     * @param int $storageUid
     *
     * @return bool
     */
    protected function isIndexingEnabledForStorage($storageUid)
    {
        $config = $this->siteConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.enableFileIndexing.storageContext.', []);
        return array_key_exists($storageUid . '.', $config);
    }

    /**
     * Determines the indexing configurations for given storage
     *
     * @param ResourceStorage $storage
     *
     * @return string[]
     */
    protected function getIndexingConfigurationsForStorage(ResourceStorage $storage)
    {
        $indexingConfigurations = array();

        if ($this->isIndexingEnabledForStorage($storage->getUid())) {
            $config = $this->siteConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.queue.', []);
            foreach ($config as $configurationName => $configuration) {
                $configurationName = rtrim($configurationName, '.');

                if (
                    (int)$config[$configurationName] === 1
                    && isset($configuration['storageUid']) && $configuration['storageUid'] == $storage->getUid()
                    && isset($configuration['table']) && $configuration['table'] == 'sys_file_storage'
                ) {
                    $indexingConfigurations[] = $configurationName;
                }
            }
        }

        return $indexingConfigurations;
    }

    /**
     * @param ResourceStorage $storage
     *
     * @return array
     */
    protected function getFileExtensionsToIndex(ResourceStorage $storage)
    {
        $configuration = $this->siteConfiguration->getObjectByPathOrDefault(
            'plugin.tx_solr.index.enableFileIndexing.storageContext.' . $storage->getUid() . '.',
            []
        );

        $fileExtensions = array();
        $configuredExtensions = trim($configuration['fileExtensions']);
        if ($configuredExtensions != '' && $configuredExtensions !== '*') {
            $fileExtensions = GeneralUtility::trimExplode(',', $configuredExtensions);
            $this->logger->debug('Indexing of storage ' . $storage->getUid() . ' will be restricted to file-extensions ' . $configuredExtensions);
        } else {
            $this->logger->debug('Indexing of storage ' . $storage->getUid() . ' will not be restricted to special file-extensions ');
        }

        return $fileExtensions;
    }

    /**
     * Check if the file extension is configured to be allowed for indexing
     *
     * @param File $file
     *
     * @return bool
     */
    protected function isAllowedFileExtension(File $file)
    {
        $allowedExtensions = $this->getFileExtensionsToIndex($file->getStorage());
        return ($allowedExtensions === array() || in_array($file->getExtension(), $allowedExtensions));
    }

    /**
     * Check if the file path is allowed
     *
     * @param File $file
     * @return bool
     */
    protected function isAllowedFilePath(File $file)
    {
        // check if file is in one of the configured valid folders
        $validFolders = $this->getValidFoldersForStorage($file->getStorage());
        $isAllowedFilePath = false;
        foreach ($validFolders as $validFolder) {
            $isAllowedFilePath = $file->getStorage()->isWithinFolder($validFolder, $file);
            if ($isAllowedFilePath) {
                break;
            }
        }

        // check if file is within an excluded folder
        if ($isAllowedFilePath) {
            $excludeFolders = $this->getExcludeFoldersForStorage($file->getStorage());
            foreach ($excludeFolders as $excludeFolder) {
                $isWithinExcludeFolder = $file->getStorage()->isWithinFolder($excludeFolder, $file);
                if ($isWithinExcludeFolder) {
                    $isAllowedFilePath = false;
                    break;
                }
            }
        }

        return $isAllowedFilePath;
    }

    /**
     * Returns the valid folders for given storage
     *
     * @param ResourceStorage $storage
     * @return Folder[]
     */
    protected function getValidFoldersForStorage(ResourceStorage $storage)
    {
        if (!isset($this->folders[$storage->getUid()])) {
            $this->folders[$storage->getUid()] = array();

            $configuredFolders = $this->siteConfiguration->getValueByPathOrDefaultValue('plugin.tx_solr.index.enableFileIndexing.storageContext.' . $storage->getUid() . '.folders', '*');
            if ($configuredFolders == '*') {
                $configuredFolders = array('/');
            } else {
                $configuredFolders = GeneralUtility::trimExplode(',', $configuredFolders);
            }

            foreach ($configuredFolders as $folder) {
                try {
                    $this->folders[$storage->getUid()][] = $storage->getFolder($folder);
                } catch (\TYPO3\CMS\Core\Resource\Exception $e) {
                    $this->getLogger()->info('Invalid folder "' . $folder . '" configured for storage ' . $storage->getUid());
                }
            }
        }

        return $this->folders[$storage->getUid()];
    }

    /**
     * Returns the excluded folders for given storage
     *
     * @param ResourceStorage $storage
     * @return Folder[]
     */
    protected function getExcludeFoldersForStorage(ResourceStorage $storage)
    {
        if (!isset($this->excludeFolders[$storage->getUid()])) {
            $this->excludeFolders[$storage->getUid()] = array();

            $configuredExcludeFolders = $this->siteConfiguration->getValueByPathOrDefaultValue('plugin.tx_solr.index.enableFileIndexing.storageContext.' . $storage->getUid() . '.excludeFolders', '');
            if (!empty($configuredExcludeFolders)) {
                $configuredExcludeFolders = GeneralUtility::trimExplode(',', $configuredExcludeFolders);
                foreach ($configuredExcludeFolders as $excludeFolder) {
                    try {
                        $this->excludeFolders[$storage->getUid()][] = $storage->getFolder($excludeFolder);
                    } catch (\TYPO3\CMS\Core\Resource\Exception $e) {
                        $this->getLogger()->info('Invalid exclude folder "' . $excludeFolder . '" configured for storage ' . $storage->getUid());
                    }
                }
            }
        }

        return $this->excludeFolders[$storage->getUid()];
    }

    /**
     * @param ResourceStorage $storage
     * @param string $indexingConfiguration
     * @return StorageContext[]
     */
    protected function getLanguageContextsForStorage(ResourceStorage $storage, $indexingConfiguration = '')
    {
        $this->logger->debug('Creating contexts in which files need to be indexed for storage ' . $storage->getUid());
        $languages = $this->getLanguagesToIndexInStorage($storage);

        /** @var StorageContext[] $contexts * */
        $contexts = array();
        $accessRootline = Rootline::getAccessRootlineByPageId($this->site->getRootPageId());
        $this->logger->debug('Using access rootline of site root page (' . (string)$accessRootline . ') for storage ' . $storage->getUid());
        foreach ($languages as $languageUid) {
            $contexts[$languageUid] = new StorageContext(
                $this->site,
                $accessRootline,
                $languageUid,
                $indexingConfiguration
            );
            $contexts[$languageUid]->setAdditionalDocumentFields(array('fileStorage' => $storage->getUid()));
        }

        return $contexts;
    }

    /**
     * @param ResourceStorage $storage
     * @return int[]
     */
    protected function getLanguagesToIndexInStorage(ResourceStorage $storage)
    {
        $configuration = $this->siteConfiguration->getObjectByPathOrDefault(
            'plugin.tx_solr.index.enableFileIndexing.storageContext.' . $storage->getUid() . '.',
            []
        );
        $languages = array();
        if (isset($configuration['languages'])) {
            $languages = GeneralUtility::intExplode(',', trim($configuration['languages']));
        }
        if (count($languages) == 0) {
            $languages[] = 0;
        }
        $this->logger->debug(sprintf('Storage %d is configured to index languages ', $storage->getUid(), implode(', ', $languages)));

        return $languages;
    }

    /**
     * @param string $table
     * @param integer $uid
     * @return void
     */
    public function recordCreated($table, $uid)
    {
        if ($this->isIndexingEnabledForContext('storage')) {
            switch ($table) {
                case 'sys_file_storage':
                    if ($this->isIndexingEnabledForStorage($uid)) {
                        $storage = $this->getStorageRepository()->findByUid($uid);
                        if ($storage) {
                            $indexingConfigurations = $this->getIndexingConfigurationsForStorage($storage);
                            foreach ($indexingConfigurations as $indexingConfiguration) {
                                $this->initializeQueueForStorage($storage, $indexingConfiguration);
                            }
                        }
                    }
                    break;
                case 'sys_file_metadata':
                    // check if it is another language which has not been present before
                    break;
                case 'sys_file':
                    $file = $this->getFile($uid);
                    if (
                        $file instanceof File
                        && $this->isIndexingEnabledForStorage($file->getStorage()->getUid())
                        && $this->isAllowedFileExtension($file)
                        && $this->isAllowedFilePath($file)
                    ) {
                        $indexingConfigurations = $this->getIndexingConfigurationsForStorage($file->getStorage());
                        foreach ($indexingConfigurations as $indexingConfiguration) {
                            $this->logger->debug('New file created, adding to indexqueue (' . $indexingConfiguration .'): ' . $file->getCombinedIdentifier());

                            $languageContexts = $this->getLanguageContextsForStorage($file->getStorage(), $indexingConfiguration);
                            foreach ($languageContexts as $context) {
                                $this->getItemRepository()->add(
                                    $this->createQueueItem(
                                        $file,
                                        $context
                                    )
                                );
                            }
                        }
                    }
                default:
            }
        }
    }

    /**
     * @param string $table
     * @param integer $uid
     * @return void
     */
    public function recordUpdated($table, $uid)
    {
        if (!$this->isIndexingEnabledForContext('storage')) {
            return;
        }

        $file = null;
        switch ($table) {
            case 'sys_file_storage':
                // do nothing if the storage changed
                // todo: think if it makes sense to reinitialize the search queue
                break;
            case 'sys_file_metadata':
                $fileRecord = $this->getDatabaseConnection()->exec_SELECTgetSingleRow('file', $table, 'uid = ' . (int)$uid);
                $file = $this->getFile($fileRecord['file']);
                $this->logger->info('Metadata for file ' . $fileRecord['file'] . ' updated, marking queue-item for re-indexing');
                break;
            case 'sys_file':
                $file = $this->getFile($uid);
                $this->logger->info('Data of file ' . $uid . ' updated, marking queue-item for re-indexing');
                break;
            default:
        }

        if (
            $file instanceof \TYPO3\CMS\Core\Resource\File
            && $this->isIndexingEnabledForStorage($file->getStorage()->getUid())
            && $this->isAllowedFileExtension($file)
            && $this->isAllowedFilePath($file)
        ) {
            $itemsToUpdate = 0;
            $itemRepository = $this->getItemRepository();
            $indexingConfigurations = $this->getIndexingConfigurationsForStorage($file->getStorage());

            foreach ($indexingConfigurations as $indexingConfiguration) {
                $languageContexts = $this->getLanguageContextsForStorage($file->getStorage(), $indexingConfiguration);
                foreach ($languageContexts as $context) {
                    $queueItem = $this->createQueueItem($file, $context);
                    if ($itemRepository->exists($queueItem)) {
                        $itemsToUpdate++;
                    } else {
                        $this->logger->info('Add missing index queue item (' . $indexingConfiguration . ') for file ' . $file->getUid() . ' and language ' . $context->getLanguage());
                        $itemRepository->add($queueItem);
                    }
                }
            }

            // update existing queue items if any
            if ($itemsToUpdate) {
                $this->getItemRepository()->markFileUpdated($file->getUid(), array('context' => 'storage'));
            }
        }
    }

    /**
     * @param string $table
     * @param integer $uid
     * @return void
     */
    public function recordDeleted($table, $uid)
    {
        switch ($table) {
            case 'sys_file_storage':
                // this really should not happen ever, even though admin may do that - do it, but ugly
                $configuration = $this->siteConfiguration->getObjectByPathOrDefault(
                    'plugin.tx_solr.index.enableFileIndexing.storageContext.',
                    []
                );
                if (array_key_exists($uid . '.', $configuration)) {
                    $this->logger->info('Indexed storage ' . $uid . ' has been removed. Clearing queue and solr index.');

                    $this->getItemRepository()->removeByFileStorage($this->site, $uid);
                    $connectionManager = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\ConnectionManager');
                    $connections = $connectionManager->getConnectionsBySite($this->site);
                    foreach ($connections as $solrService) {
                        $solrService->deleteByQuery('fileStorage:' . (int)$uid);
                    }
                }
                break;
            case 'sys_file_metadata':
                // ignore this, in normal conditions this never happens / should happen
            case 'sys_file':
                // File deletion is taken care of via Slot do deletion signal
                break;
            default:
        }
    }

    /**
     * Handles new sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordCreated($table, $uid)
    {
        $this->recordCreated($table, $uid);
    }

    /**
     * Handles updates on sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordUpdated($table, $uid)
    {
        $this->recordUpdated($table, $uid);
    }
}
