<?php
namespace ApacheSolrForTypo3\Solrfal\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2014 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solrfal\Context\PageContext;
use ApacheSolrForTypo3\Solrfal\Indexing\DocumentFactory;
use ApacheSolrForTypo3\Solrfal\Queue\Item;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Class PageContextDetector
 */
class PageContextDetector extends AbstractRecordDetector
{

    /**
     * @param array $initializationStatus
     *
     * @return void
     */
    public function initializeQueue(array $initializationStatus)
    {
        // files will be detected on frontend indexing - so remove all entries
        if (empty($initializationStatus) || (array_key_exists('pages', $initializationStatus) && $initializationStatus['pages'] == true)) {
            $this->getItemRepository()->removeBySiteAndContext($this->site, 'page');
        }
    }

    /**
     * @param TypoScriptFrontendController $page
     * @param Rootline $pageAccessRootline
     * @param array $allowedFileUids
     * @param array $contentElements
     */
    public function addDetectedFilesToPage(TypoScriptFrontendController $page, Rootline $pageAccessRootline, $allowedFileUids = array(), $contentElements = array())
    {
        if (!$this->isIndexingEnabledForContext('page')) {
            return;
        }
        $contentTypeFieldMapping = $this->getContentElementTypeToFieldMapping();

        $this->logger->info('Adding trigger indexing files for page ' . $page->id, $contentTypeFieldMapping);
        $this->logger->info('Files with calls on "getPublicUrl" ', $allowedFileUids);
        $this->logger->info('Content element count:' . count($contentElements));

        $successfulUids = array();
        foreach ($contentElements as $singleElement) {
            $contentType = $singleElement['CType'];
            if (array_key_exists($contentType, $contentTypeFieldMapping)) {
                $indexableFields = GeneralUtility::trimExplode(',', $contentTypeFieldMapping[$contentType]);

                foreach ($indexableFields as $field) {
                    $this->logger->info('Indexing field ' . $field);
                    $attachedFileUids = $this->getFileAttachmentResolver()->detectFilesInField('tt_content', $field, $singleElement);
                    $this->logger->info('Found-Files: ' . implode(', ', $attachedFileUids));

                    $linkedFiles = array_intersect($attachedFileUids, $allowedFileUids);
                    $this->logger->info('Found files which have been linked: ' . implode(', ', $linkedFiles));
                    if ($linkedFiles !== array()) {
                        #FIXME must not use new
                        $context = new PageContext(
                            $this->site, $pageAccessRootline, $page->id,
                            'tt_content', $field, $singleElement['uid'], $page->sys_language_uid
                        );
                        $this->logger->info('Context created');

                        foreach ($linkedFiles as $fileUid) {
                            if ($this->createIndexQueueEntryForFile($fileUid, $context)) {
                                $this->logger->info('Indexed-Filed: ' . $fileUid);

                                $successfulUids[] = $fileUid;
                            }
                        }
                    }
                }
            }
        }
        $this->getItemRepository()->removeOldEntriesInPageContext($this->site, $page->id, $page->sys_language_uid, $successfulUids);
    }


    /**
     * Create Index Queue entry for a concrete file uid and it's context
     *
     * @param int $fileUid
     * @param PageContext $context
     *
     * @return bool
     */
    protected function createIndexQueueEntryForFile($fileUid, PageContext $context)
    {
        try {
            $file = ResourceFactory::getInstance()->getFileObject((int)$fileUid);
            $this->logger->info('Got File Object for identifier ' . $file->getCombinedIdentifier());
            if ($this->isAllowedFileExtension($file)) {
                $this->logger->info('File extension allowed ' . $file->getExtension());
                $indexQueueItem = $this->createQueueItem($file, $context);
                if (!$this->getItemRepository()->exists($indexQueueItem)) {
                    $this->getItemRepository()->add($indexQueueItem);
                } else {
                    $this->getItemRepository()->update($indexQueueItem);
                }
            } else {
                $this->logger->info('File extension is not allowed: ' . $file->getExtension());
                return false;
            }
        } catch (ResourceDoesNotExistException $e) {
            $this->logger->error('File not found: ' . $fileUid);
            return false;
        } catch (\Exception $e) {
            $this->logger->error('Unknown exception while loading file: ' . $fileUid);
            return false;
        }

        return true;
    }

    /**
     * Get the configured mapping, which fields of which content-element type should be indexed
     *
     * @return array
     */
    protected function getContentElementTypeToFieldMapping()
    {
        return $this->siteConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.enableFileIndexing.pageContext.contentElementTypes.', []);
    }

    /**
     * Check if the content element type is configured to be allowed for indexing
     *
     * @param string $contentType
     * @return bool
     */
    protected function isContentElementTypeAllowed($contentType)
    {
        $contentTypeFieldMapping = $this->getContentElementTypeToFieldMapping();
        return array_key_exists($contentType, $contentTypeFieldMapping);
    }

    /**
     * Check if the file extension is configured to be allowed for indexing
     *
     * @param File $file
     *
     * @return bool
     */
    protected function isAllowedFileExtension(File $file)
    {
        $pageConfig = $this->siteConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.enableFileIndexing.pageContext.', []);
        $extensions = isset($pageConfig['fileExtensions']) ? strtolower(trim($pageConfig['fileExtensions'])) : '';
        if ($extensions === '*') {
            return true;
        } else {
            $allowedFileExtensions = GeneralUtility::trimExplode(',', $extensions);
            return in_array($file->getExtension(), $allowedFileExtensions);
        }
    }

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordCreated($table, $uid)
    {
        // nothing to do since editing page already
        // already triggers re-indexing.
    }

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordUpdated($table, $uid)
    {
        if ($table == 'pages') {
            $page = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('pages', $uid);
            if ($page['hidden']) {
                $this->getItemRepository()->removeOldEntriesInPageContext($this->site, $uid, -1, array());
            }
        } elseif ($table == 'tt_content') {
            $contentElement = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('tt_content', $uid);
            if ($contentElement['hidden'] || !$this->isContentElementTypeAllowed($contentElement['CType'])) {
                $this->getItemRepository()->removeByTableAndUidInContext('page', $this->site, 'tt_content', $uid);
            }
        } elseif ($table == 'sys_file_reference') {
            $fileReference = $this->getFileReferenceObject($uid);
            if ($fileReference->getReferenceProperty('tablenames') == 'tt_content' && $fileReference->getReferenceProperty('hidden')) {
                $this->getItemRepository()->removeByTableAndUidInContext(
                    'page',
                    $this->site,
                    'tt_content',
                    $fileReference->getReferenceProperty('uid_foreign'),
                    $fileReference->getReferenceProperty('uid_local')
                );
            }
        }
    }

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordDeleted($table, $uid)
    {
        if ($table === 'pages') {
            $this->getItemRepository()->removeOldEntriesInPageContext($this->site, $uid, -1, array());
        } elseif ($table === 'tt_content') {
            $this->getItemRepository()->removeByTableAndUidInContext('page', $this->site, 'tt_content', $uid);
        } elseif ($table == 'sys_file_reference') {
            // get reference and try to delete files of referencing records
            // Note that processCmdmap_preProcess must be used to call recordDeleted, otherwise
            // the file reference will not be available

            $fileReference = $this->getFileReferenceObject($uid);
            if ($fileReference->getReferenceProperty('tablenames') == 'tt_content') {
                $this->getItemRepository()->removeByTableAndUidInContext(
                    'page',
                    $this->site,
                    'tt_content',
                    $fileReference->getReferenceProperty('uid_foreign'),
                    $fileReference->getReferenceProperty('uid_local')
                );
            }
        }
    }

    /**
     * Handles updates on sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordUpdated($table, $uid)
    {
        // skip if record context is not enabled or file not allowed
        if (!$this->isIndexingEnabledForContext('page')) {
            return;
        }

        if (
            $table == 'sys_file'
            && ($file = $this->getFile($uid))
            && $this->isAllowedFileExtension($file)
        ) {
            $referencedPages = $this->getReferencedPages($uid);
            if ($referencedPages) {
                $indexQueue = $this->getIndexQueue();
                foreach ($referencedPages as $pageUid) {
                    $indexQueue->updateItem('pages', $pageUid, null, $GLOBALS['EXEC_TIME']);
                }
            }
        }
    }

    /**
     * Returns referenced pages that have to be updated
     *
     * @param integer $fileUid
     *
     * @return int[]
     */
    protected function getReferencedPages($fileUid)
    {
        $referencedPages = array();
        $referenceIndexRepository = $this->getReferenceIndexEntryRepository();

        // get and process references
        // restrict to pages, tt_content and sys_file_reference
        $references = $referenceIndexRepository->findByReferenceRecord('sys_file', $fileUid, array(), array('pages', 'tt_content', 'pages_language_overlay', 'sys_file_reference'));
        foreach ($references as $reference) {

            // try to get right reference to index record if reference refers to sys_file_reference
            if ($reference->getTableName() == 'sys_file_reference') {
                // get record reference if this is only a reference to sys_file_reference
                $reference = $referenceIndexRepository->findOneByReferenceIndexEntry($reference, array(), array('pages', 'tt_content', 'pages_language_overlay', 'sys_file_reference'));
                if (!$reference) {
                    continue;
                }
            }

            switch ($reference->getTableName()) {
                case 'pages':
                    $referencedPages[] = $reference->getRecordUid();
                break;

                case 'tt_content':
                case 'pages_language_overlay':
                    if ($record = $reference->getRecord()) {
                        $referencedPages[] = (int) $record['pid'];
                    }
                break;

                default:

            }
        }

        return array_unique($referencedPages);
    }
}
