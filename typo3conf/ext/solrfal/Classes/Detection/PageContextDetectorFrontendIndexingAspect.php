<?php
namespace ApacheSolrForTypo3\Solrfal\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2014 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solr\PageDocumentPostProcessor;
use ApacheSolrForTypo3\Solr\Site;
use \TYPO3\CMS\Core\Resource;
use TYPO3\CMS\Core\Resource\Driver\DriverInterface;
use TYPO3\CMS\Core\Resource\ResourceInterface;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectPostInitHookInterface;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Class PageContextDetectorFrontendIndexingAspect
 */
class PageContextDetectorFrontendIndexingAspect implements ContentObjectPostInitHookInterface , PageDocumentPostProcessor
{

    /**
     * @var array
     */
    public static $collectedFileUids = array();

    public static $collectedContentElements = array();

    /**
     * Registers file uids
     * Slot to ResourceStorage::preGetPublicUrl
     *
     * @param ResourceStorage $storage
     * @param DriverInterface $driver
     * @param ResourceInterface $resourceObject
     * @param bool $relativeToCurrentScript
     * @param array $urlData
     */
    public function registerGeneratedPublicUrl(ResourceStorage $storage, DriverInterface $driver, ResourceInterface $resourceObject, $relativeToCurrentScript = false, $urlData = array())
    {
        if ($resourceObject instanceof Resource\File) {
            static::$collectedFileUids[] = $resourceObject->getUid();
        } elseif ($resourceObject instanceof Resource\ProcessedFile) {
            static::$collectedFileUids[] = $resourceObject->getOriginalFile()->getUid();
        } elseif ($resourceObject instanceof Resource\FileReference) {
            static::$collectedFileUids[] = $resourceObject->getOriginalFile()->getUid();
        }

        $logger = GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
        $logger->info('getPublicUrl called for file: ' . $resourceObject->getCombinedIdentifier());
    }

    /**
     * Allows Modification of the PageDocument
     * Can be used to trigger actions when all contextual variables of the pageDocument to be indexed are known
     *
     * @param \Apache_Solr_Document $pageDocument the generated page document
     * @param TypoScriptFrontendController $page the page object with information about page id or language
     *
     * @return void
     */
    public function postProcessPageDocument(\Apache_Solr_Document $pageDocument, TypoScriptFrontendController $page)
    {
        $accessField = $pageDocument->getField('access');
#FIXME must not use new
        $pageAccessRootline = new Rootline($accessField['value']);

        $site = Site::getSiteByPageId($page->id);
        $logger = GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
        $logger->info('Adding trigger indexing files for page ' . $page->id  . ' with access rights ' . $pageAccessRootline);
        /** @var PageContextDetector $pageContextDetector */
        $pageContextDetector = GeneralUtility::makeInstance('ApacheSolrForTypo3\Solrfal\Detection\PageContextDetector', $site);
        $pageContextDetector->addDetectedFilesToPage($page, $pageAccessRootline, static::$collectedFileUids, static::$collectedContentElements);
    }

    /**
     * Hook for post processing the initialization of ContentObjectRenderer
     * Passes the record
     *
     * @param ContentObjectRenderer $parentObject Parent content object
     */
    public function postProcessContentObjectInitialization(ContentObjectRenderer &$parentObject)
    {
        list($table, $uid) = explode(':', $parentObject->currentRecord);
        $record = $parentObject->data;
        if (!empty($record) && $table === 'tt_content') {
            $logger = GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
            $logger->info('postInitHook called for record: ' . $parentObject->currentRecord);
            static::$collectedContentElements[$uid] = $record;
        }
    }
}
