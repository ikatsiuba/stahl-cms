<?php
namespace ApacheSolrForTypo3\Solrfal\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\Util;

/**
 * Class StorageContext
 */
class RecordContext extends AbstractContext
{

    /**
     * @var string
     */
    protected $table;

    /**
     * @var string
     */
    protected $field;

    /**
     * @var integer
     */
    protected $uid;

    /**
     * @param Site $site
     * @param Rootline $accessRestrictions
     * @param string $table
     * @param string $field
     * @param string $indexingConfiguration
     * @param integer $uid
     * @param integer $language
     */
    public function __construct(Site $site, Rootline $accessRestrictions, $table, $field, $uid, $indexingConfiguration, $language = 0)
    {
        parent::__construct($site, $accessRestrictions, $language, $indexingConfiguration);
        $this->table = $table;
        $this->field = $field;
        $this->uid = $uid;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            array(
                'context_record_table'                  => $this->getTable(),
                'context_record_uid'                    => $this->getUid(),
                'context_record_field'                  => $this->getField(),
                'context_record_indexing_configuration' => $this->getIndexingConfiguration()
            )
        );
    }

    /**
     * @return string
     */
    public function getContextIdentifier()
    {
        return 'record';
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns an array of context specific field to add to the solr document
     *
     * @return array
     */
    public function getAdditionalStaticDocumentFields()
    {
        return array_merge(
            parent::getAdditionalStaticDocumentFields(),
            array(
                'fileReferenceType' => $this->getTable(),
                'fileReferenceUid' => $this->getUid()
            )
        );
    }

    /**
     * Resolves the field-processing TypoScript configuration which is specific
     * to the current context.
     * Will be merged in the default field-processing configuration and takes
     * precedence over the default configuration.
     *
     * @return array
     */
    public function getSpecificFieldConfigurationTypoScript()
    {
        $fieldConfiguration = parent::getSpecificFieldConfigurationTypoScript();
        return $this->injectReferenceInformation($fieldConfiguration);
    }

    /**
     * Injects configuration so FileReferenceTitle and FileReferenceUrl
     * are set from related record.
     *
     * @param array $fieldConfiguration
     *
     * @return array
     */
    protected function injectReferenceInformation(array $fieldConfiguration = array())
    {
        // if there are no reference title / url configured manually, try to copy configuration form record index-queue
        if (!isset($fieldConfiguration['__RecordContext']) && !is_array($fieldConfiguration['__RecordContext.'])) {
            $indexingConfigurations = Util::getSolrConfigurationFromPageId(
                $this->getSite()->getRootPageId(),
                false,
                $this->getLanguage()
            );
            $fieldConfigurationOfRecord = $indexingConfigurations->getObjectByPathOrDefault('plugin.tx_solr.index.queue.' . $this->getIndexingConfiguration() . '.fields.', []);
            $fieldConfiguration['__RecordContext'] = '_';
            $fieldConfiguration['__RecordContext.'] = array();

            if (isset($fieldConfigurationOfRecord['title'])) {
                $fieldConfiguration['__RecordContext.']['fileReferenceTitle'] = $fieldConfigurationOfRecord['title'];
            }
            if (isset($fieldConfigurationOfRecord['title.']) && is_array($fieldConfigurationOfRecord['title.'])) {
                $fieldConfiguration['__RecordContext.']['fileReferenceTitle.'] = $fieldConfigurationOfRecord['title.'];
            }
            if (isset($fieldConfigurationOfRecord['url'])) {
                $fieldConfiguration['__RecordContext.']['fileReferenceUrl'] = $fieldConfigurationOfRecord['url'];
            }
            if (isset($fieldConfigurationOfRecord['url.']) && is_array($fieldConfigurationOfRecord['url.'])) {
                $fieldConfiguration['__RecordContext.']['fileReferenceUrl.'] = $fieldConfigurationOfRecord['url.'];
            }
        }
        return $fieldConfiguration;
    }

    /**
     * Returns an identifier, which will be used for looking up special
     * configurations in TypoScript like storage uid in storageContext
     * or table name in recordContext
     *
     * @return string
     */
    protected function getIdentifierForItemSpecificFieldConfiguration()
    {
        return $this->getTable();
    }
}
