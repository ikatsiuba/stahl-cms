<?php
namespace ApacheSolrForTypo3\Solrfal\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Site;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ContextFactory
 *
 */
class ContextFactory implements ContextFactoryInterface
{

    protected static $typeMapping = array(
        'page' => array(
            'class'     => 'ApacheSolrForTypo3\\Solrfal\\Context\\PageContext',
            'detection' => 'ApacheSolrForTypo3\\Solrfal\\Detection\\PageContextDetector',
            'factory'   => null
        ),
        'storage' => array(
            'class'     => 'ApacheSolrForTypo3\\Solrfal\\Context\\StorageContext',
            'detection' => 'ApacheSolrForTypo3\\Solrfal\\Detection\\StorageContextDetector',
            'factory'   => null
        ),
        'record' => array(
            'class'     => 'ApacheSolrForTypo3\\Solrfal\\Context\\RecordContext',
            'detection' => 'ApacheSolrForTypo3\\Solrfal\\Detection\\RecordContextDetector',
            'factory'   => null
        )
    );

    /**
     * Factory Method to create a Context based on an entry in
     * tx_solr_indexqueue_file
     *
     * @param array
     *
     * @return ContextInterface
     * @throws \RuntimeException
     */
    public function getByRecord(array $row)
    {
        /** @var AbstractContext $object */
        $object = null;

        $type = $row['context_type'];
        if (!array_key_exists($type, self::$typeMapping)) {
            throw new \RuntimeException('Unknown context type', 1382006080);
        } else {
            $className = self::$typeMapping[$type]['class'];
            $customFactory = self::$typeMapping[$type]['factory'];
            if ($customFactory === null) {
                $accessRootline = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\Access\\Rootline', $row['context_access_restrictions']);
                $site = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\Site', $row['context_site']);
                $language = intval($row['context_language']);
                $additionalFields = json_decode($row['context_additional_fields'], true, 2);
                if (!is_array($additionalFields)) {
                    $additionalFields = array();
                }

                switch ($type) {
                    case 'storage':
                        $object = GeneralUtility::makeInstance($className, $site, $accessRootline, $language);
                        break;
                    case 'record':
                        $table = $row['context_record_table'];
                        $field = $row['context_record_field'];
                        $uid = intval($row['context_record_uid']);
                        $indexingConfiguration =  $row['context_record_indexing_configuration'];
                        $object = GeneralUtility::makeInstance($className, $site, $accessRootline, $table, $field, $uid, $indexingConfiguration, $language);
                        break;
                    case 'page':
                        $pageUid = intval($row['context_record_page']);
                        $table = $row['context_record_table'];
                        $field = $row['context_record_field'];
                        $uid = intval($row['context_record_uid']);
                        $object = GeneralUtility::makeInstance($className, $site, $accessRootline, $pageUid, $table, $field, $uid, $language);
                        break;
                    default:
                        throw new \RuntimeException('You registered a custom Context without providing a Factory', 1382006090);
                }
                $object->setAdditionalDocumentFields($additionalFields);
                return $object;
            } else {
                /** @var ContextFactoryInterface $factory */
                $factory = GeneralUtility::makeInstance($customFactory);
                $object = $factory->getByRecord($row);
            }
        }

        return $object;
    }

    /**
     * Allows to register custom Context-Types for Indexing,
     * or replacing the original implementations.
     *
     * @param string $typeName
     * @param string $implementationClass
     * @param string $detectionClass
     * @param string $customFactory
     *
     * @return void
     * @throws \RuntimeException
     */
    public static function registerType($typeName, $implementationClass, $detectionClass, $customFactory = null)
    {
        if (!is_subclass_of($implementationClass, 'ApacheSolrForTypo3\\Solrfal\\Context\\ContextInterface')) {
            throw new \RuntimeException('Custom Indexing contexts need to implement the ContextInterface', 1382006059);
        }
        if (!is_subclass_of($detectionClass, 'ApacheSolrForTypo3\\Solrfal\\Detection\\RecordDetectionInterface')) {
            throw new \RuntimeException('The detector of a custom indexing context needs to implement the ContextInterface', 1382006071);
        }
        if ($customFactory !== null && !is_subclass_of($customFactory, 'ApacheSolrForTypo3\\Solrfal\\Context\\ContextFactoryInterface')) {
            throw new \RuntimeException('A custom ContextFactory needs to implement the ContextFactoryInterface', 1382006070);
        }
        self::$typeMapping[$typeName] = array(
            'class'     => $implementationClass,
            'factory'   => $customFactory,
            'detection' => $detectionClass
        );
    }

    /**
     * @param Site $site
     *
     * @return \ApacheSolrForTypo3\Solrfal\Detection\RecordDetectionInterface[]
     */
    public static function getContextDetectors(Site $site)
    {
        $detectors = array();
        // Fetch site configuration once and use this for all detectors in loop
        $siteConfiguration = $site->getSolrConfiguration();
        foreach (self::$typeMapping as $configuration) {
            $detectors[] = GeneralUtility::makeInstance($configuration['detection'], $site, $siteConfiguration);
        }
        return $detectors;
    }
}
