<?php
namespace ApacheSolrForTypo3\Solrfal\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Resource\File;

/**
 * Class Context
 */
interface ContextInterface
{

    /**
     * @return string
     */
    public function getContextIdentifier();

    /**
     * Returns the Site
     *
     * @return \ApacheSolrForTypo3\Solr\Site
     */
    public function getSite();

    /**
     * @return integer
     */
    public function getLanguage();

    /**
     * @return \ApacheSolrForTypo3\Solr\Access\Rootline
     */
    public function getAccessRestrictions();

    /**
     * Returns the pageId of this context
     *
     * @return int
     */
    public function getPageId();

    /**
     * Returns an array of context specific field to add to the solr document
     *
     * @return array
     */
    public function getAdditionalStaticDocumentFields();

    /**
     * Returns an array of context specific field to add to the solr document,
     * dynamically calculated from the FILE
     *
     * @param File $file
     * @return array
     */
    public function getAdditionalDynamicDocumentFields(File $file);

    /**
     * @return array
     */
    public function getSpecificFieldConfigurationTypoScript();

    /**
     * Returns the array representation for database storage
     *
     * @return array
     */
    public function toArray();
}
