<?php
namespace ApacheSolrForTypo3\Solrfal\Queue;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\GarbageCollectorPostProcessor;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solrfal\Context\ContextFactory;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Hooks and Slots taking care, the contents of the Queue only refer
 * to existing files and records, as well as purging Solr after
 * index queue entry is removed
 */
class ConsistencyAspect implements GarbageCollectorPostProcessor
{

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Indexing\Indexer
     * @inject
     */
    protected $indexer;

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository
     * @inject
     */
    protected $itemRepository;

    /**
     * Post processing of garbage collector
     *
     * @param string $table The record's table name.
     * @param int $uid The record's uid.
     * @return void
     * @see ApacheSolrForTypo3\Solr\GarbageCollector->collectGarbage()
     */
    public function postProcessGarbageCollector($table, $uid)
    {
    }

    /**
     * If a file is deleted, we can/should remove it from Solr and Indexqueue
     *
     * @param FileInterface $file
     *
     * @return void
     */
    public function removeDeletedFile(FileInterface $file)
    {
        if ($file instanceof File) {
            $this->itemRepository->removeByFile($file);
        }
    }

    /**
     * If a file is marked as missing, we can/should remove it from Solr and Indexqueue
     *
     * @param integer $fileUid
     *
     * @return void
     */
    public function removeMissingFile($fileUid)
    {
        $this->itemRepository->removeByFileUid($fileUid);
    }

    /**
     * @param array $data
     * @return void
     */
    public function fileIndexRecordUpdated(array $data)
    {
        $this->issueCommandOnDetectors('fileIndexRecordUpdated', 'sys_file', $data['uid']);
    }

    /**
     * @param array $data
     * @return void
     */
    public function fileIndexRecordCreated(array $data)
    {
        $this->issueCommandOnDetectors('fileIndexRecordCreated', 'sys_file', $data['uid']);
    }

    /**
     * @param integer $fileUid
     * @return void
     */
    public function fileIndexRecordDeleted($fileUid)
    {
        $this->issueCommandOnDetectors('fileIndexRecordDeleted', 'sys_file', $fileUid);
    }

    /**
     * @param string $command
     * @param string $table
     * @param string $id
     * @param mixed $value
     * @param DataHandler $pObj
     * @return void
     */
    public function processCmdmap_preProcess($command, $table, $id, $value, &$pObj)
    {
        $method = '';
        switch ($command) {
            case 'delete':
                $method = 'recordDeleted';
                break;
            default:
        }
        if ($method !== '') {
            $this->issueCommandOnDetectors($method, $table, $id);
        }
    }

    /**
     * @param $status
     * @param $table
     * @param $id
     * @param array $fieldArray
     * @param DataHandler $dataHandler
     */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, array $fieldArray, DataHandler $dataHandler)
    {
        $method = '';
        switch ($status) {
            case 'update':
                $method = 'recordUpdated';
                break;
            case 'new':
                $method = 'recordCreated';
                if (!MathUtility::canBeInterpretedAsInteger($id)) {
                    $id = $dataHandler->substNEWwithIDs[$id];
                }
            default:
        }
        if ($method !== '') {
            $this->issueCommandOnDetectors($method, $table, $id);
        }
    }

    /**
     * @param string $function
     * @param string $table
     * @param int $uid
     * @return void
     */
    protected function issueCommandOnDetectors($function, $table, $uid)
    {
        $detectors = $this->getDetectorsForRecord($table, $uid);
        foreach ($detectors as $detector) {
            $detector->$function($table, $uid);
        }
    }

    /**
     * @return \ApacheSolrForTypo3\Solrfal\Detection\RecordDetectionInterface[]
     */
    protected function getDetectorsOfAllSites()
    {
        $sites = Site::getAvailableSites();
        $detectors = array();
        foreach ($sites as $site) {
            $detectors = array_merge($detectors, ContextFactory::getContextDetectors($site));
        }
        return $detectors;
    }

    /**
     * This method is used to determine the relevant detectors for a record.
     * For pages and content elements only the detector from the rootPage is relevant.
     *
     * For any other records all detectors will be invoked.
     *
     * @param string $table
     * @param int $uid
     * @return \ApacheSolrForTypo3\Solrfal\Detection\RecordDetectionInterface[]
     */
    protected function getDetectorsForRecord($table, $uid)
    {
        $isPageOrContentElement = in_array($table, ['pages', 'pages_language_overlay', 'tt_content']);
        if ($isPageOrContentElement) {
            return $this->getDetectorsForPageOrContentElement($table, $uid);
        }

        // we have a normal record or a sys_file record. In these cases, we need to check all sites
        return $this->getDetectorsOfAllSites();
    }

    /**
     * This method is used to get all detectors for pages or content elements. Since we know that they
     * belong to one site, we only get the context detectors for this particular site.
     *
     * @param string $table
     * @param int $uid
     * @return array
     */
    protected function getDetectorsForPageOrContentElement($table, $uid)
    {
        $pageId = $this->getRecordPageId($table, $uid);
        $rootPageId = Util::getRootPageId($pageId);

        if (Util::isRootPage($rootPageId)) {
            // when we know that the page is a site root page, we can only get the
            // detectors for this site.
            $site = Site::getSiteByPageId($rootPageId);
            return ContextFactory::getContextDetectors($site);
        }

        return $this->getDetectorsOfAllSites();
    }

    /**
     * This method is used to get the page id that is relevant to build the rootline.
     * If the record is a page the uid is used, if the record is not a page the pid of the record is used.
     *
     * @param string $table
     * @param integer $uid
     * @return integer
     * @throws \Exception
     */
    protected function getRecordPageId($table, $uid)
    {
        // if the record is a pages_language_overlay we also need to check the pid of the overlay record
        $isPageRecord = ($table === 'pages');
        if ($isPageRecord) {
            return (int) $uid;
        }

        $record = BackendUtility::getRecord($table, $uid, 'uid,pid');
        if (!isset($record['pid'])) {
            throw new \Exception('Could not determine pid');
        }

        return (int) $record['pid'];
    }

    /**
     * @param Item $item
     * @return void
     */
    public function removeSolrEntryForItem(Item $item)
    {
        $this->indexer->removeFromIndex($item);
    }

    /**
     * @param array $uids
     */
    public function removeMultipleQueueItemsFromSolr(array $uids)
    {
        $sites = Site::getAvailableSites();
        foreach ($sites as $site) {
            try {
                $this->indexer->removeByQueueEntriesAndSite($uids, $site);
            } catch (\Apache_Solr_HttpTransportException $e) {
                $logger = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Log\\LogManager')->getLogger(__CLASS__);
                $logger->error('Failed to remove multiple queue items from Solr (' . $site->getDomain() . '): ' . print_r($e->getResponse(), 1));
            }
        }
    }
}
