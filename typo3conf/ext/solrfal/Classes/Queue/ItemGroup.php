<?php

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Timo Hund <timo.hund@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace ApacheSolrForTypo3\Solrfal\Queue;

/**
 * Class ItemGroup
 *
 * An ItemGroup is a set of documents that logically belons together. It is used to merge documents into one
 * document that belong together.
 */
class ItemGroup
{

    /**
     * @var string
     */
    protected $groupId;

    /**
     * @var Item[]
     */
    protected $items;

    /**
     * @param Item $item
     */
    public function add(Item $item)
    {
        $this->items[$item->getUid()] = $item;
    }

    /**
     * @param Item $item
     */
    public function remove(Item $item)
    {
        unset($this->items[$item->getUid()]);
    }

    /**
     * Returns the smallest item uid.
     *
     * @return integer
     */
    public function getRootItemUid()
    {
        return min(array_keys($this->items));
    }

    /**
     * Returns the item with the smallest uid.
     *
     * @return Item
     */
    public function getRootItem()
    {
        $minUid = $this->getRootItemUid();
        return isset($this->items[$minUid]) ? $this->items[$minUid] : null;
    }

    /**
     * @param Item $item
     * @return bool
     */
    public function getIsRootItem(Item $item)
    {
        return $this->getRootItemUid() === $item->getUid();
    }

    /**
     * @return boolean
     */
    public function getHasOnlyRootItem()
    {
        return count($this->items) === 1;
    }

    /**
     * @return boolean
     */
    public function getIsEmpty()
    {
        return count($this->items) === 0;
    }

    /**
     * @return string
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }
}
