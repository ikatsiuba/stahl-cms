<?php
namespace ApacheSolrForTypo3\Solrfal\Service;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\IndexQueue\AbstractIndexer;
use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solrfal\Context\ContextInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class FieldProcessingService
 */
class FieldProcessingService extends AbstractIndexer
{
    /**
     * @var FieldProcessingService
     */
    protected static $instance = null;

    /**
     * @param \Apache_Solr_Document $document
     * @param ContextInterface $context
     * @return void
     */
    public static function processFieldInstructions(\Apache_Solr_Document $document, ContextInterface $context)
    {
        $documents = array($document);

        // needs to respect the TS settings of the page the item is on, conditions apply
        $solrConfiguration = Util::getSolrConfigurationFromPageId($context->getSite()->getRootPageId());
        $fieldProcessingInstructions = $solrConfiguration->getObjectByPathOrDefault(
            'plugin.tx_solr.index.fieldProcessingInstructions.',
            []
        );

        // same as in the FE indexer
        if (is_array($fieldProcessingInstructions)) {
            /** @var \ApacheSolrForTypo3\Solr\FieldProcessor\Service $service */
            $service = GeneralUtility::makeInstance('ApacheSolrForTypo3\\Solr\\FieldProcessor\\Service');
            $service->processDocuments(
                $documents,
                $fieldProcessingInstructions
            );
        }
    }

    /**
     * Adds fields to the document as defined in $indexingConfiguration
     *
     * @param ContextInterface $context
     * @param \Apache_Solr_Document $document base document to add fields to
     * @param array $indexingConfiguration Indexing configuration / mapping
     * @param array $data The record Data
     * @return \Apache_Solr_Document Modified document with added fields
     */
    public static function addTypoScriptFieldsToDocument(ContextInterface $context, \Apache_Solr_Document $document, array $indexingConfiguration, array $data)
    {
        if (self::$instance == null) {
            self::$instance = GeneralUtility::makeInstance(__CLASS__);
            self::$instance->type = 'sys_file_metadata';
        }

        $backupTsFe = null;
        if (!empty($GLOBALS['TSFE'])) {
            $backupTsFe = $GLOBALS['TSFE'];
        }

        // Remember original http host value
        $originalHttpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null;

        self::$instance->initializeHttpHost($context);
        Util::initializeTsfe($context->getSite()->getRootPageId(), $context->getLanguage());
        $document = self::$instance->addDocumentFieldsFromTyposcript($document, $indexingConfiguration, $data);

        // restore http host
        if (!is_null($originalHttpHost)) {
            $_SERVER['HTTP_HOST'] = $originalHttpHost;
        } else {
            unset($_SERVER['HTTP_HOST']);
        }

        $GLOBALS['TSFE'] = $backupTsFe;

        return $document;
    }

    /**
     * Initializes the $_SERVER['HTTP_HOST'] environment variable in CLI
     * environments dependent on the Index Queue item's root page.
     *
     * When the Index Queue Worker task is executed by a cron job there is no
     * HTTP_HOST since we are in a CLI environment. RealURL needs the host
     * information to generate a proper URL though. Using the Index Queue item's
     * root page information we can determine the correct host although being
     * in a CLI environment.
     *
     * @param	ContextInterface $itemContext Index Queue item to use to determine the host.
     */
    protected function initializeHttpHost(ContextInterface $itemContext)
    {
        static $hosts = array();

        // relevant for realURL environments, only
        if (ExtensionManagementUtility::isLoaded('realurl')) {
            $rootpageId = $itemContext->getSite()->getRootPageId();
            $hostFound  = !empty($hosts[$rootpageId]);

            if (!$hostFound) {
                $rootline = BackendUtility::BEgetRootLine($rootpageId);
                $host     = BackendUtility::firstDomainRecord($rootline);

                $hosts[$rootpageId] = $host;
            }

            $_SERVER['HTTP_HOST'] = $hosts[$rootpageId];
        }
    }
}
