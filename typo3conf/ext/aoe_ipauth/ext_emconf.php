<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "aoe_ipauth".
 *
 * Auto generated 24-01-2017 13:02
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'IP Authentication',
  'description' => 'Authenticates users based on IP address settings',
  'category' => 'services',
  'version' => '1.0.2',
  'state' => 'stable',
  'uploadfolder' => true,
  'createDirs' => '',
  'clearcacheonload' => true,
  'author' => 'DEV',
  'author_email' => 'dev@aoe.com',
  'author_company' => 'AOE GmbH',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '6.2.1-7.99.99',
//      '' => '',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

