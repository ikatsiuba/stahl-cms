<?php
/*
 * Register necessary class names with autoloader
 *
 * $Id$
 */
$extensionPath = \TYPO3\CMS\Core\Utility\GeneralManagementUtility::extPath('svconnector');
return array(
	'tx_svconnector_base' => $extensionPath . 'class.tx_svconnector_base.php',
	'tx_svconnector_utility' => $extensionPath . 'class.tx_svconnector_utility.php',
);
?>
