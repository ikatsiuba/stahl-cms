<?php
namespace Havasww\DatabaseUtilities\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Navigation
 */
class Navigation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * link
     *
     * @var int
     */
    protected $link = 0;
    
    /**
     * code
     *
     * @var string
     */
    protected $code = '';
    
    /**
     * label
     *
     * @var string
     */
    protected $label = '';
    
    /**
     * navigationmenu
     *
     * @var string
     */
    protected $navigationmenu = '';
    
    /**
     * position
     *
     * @var string
     */
    protected $position = '';
    
    /**
     * Returns the link
     *
     * @return int $link
     */
    public function getLink()
    {
        return $this->link;
    }
    
    /**
     * Sets the link
     *
     * @param int $link
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
    
    /**
     * Returns the code
     *
     * @return string $code
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Sets the code
     *
     * @param string $code
     * @return void
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
    
    /**
     * Returns the label
     *
     * @return string $label
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Sets the label
     *
     * @param string $label
     * @return void
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
    
    /**
     * Returns the navigationmenu
     *
     * @return string $navigationmenu
     */
    public function getNavigationmenu()
    {
        return $this->navigationmenu;
    }
    
    /**
     * Sets the navigationmenu
     *
     * @param string $navigationmenu
     * @return void
     */
    public function setNavigationmenu($navigationmenu)
    {
        $this->navigationmenu = $navigationmenu;
    }
    
    /**
     * Returns the position
     *
     * @return string $position
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    /**
     * Sets the position
     *
     * @param string $position
     * @return void
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

}