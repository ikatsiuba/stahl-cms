<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "autoloader".
 *
 * Auto generated 17-02-2017 17:29
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Autoloader',
  'description' => 'Automatic components loading of ExtBase extensions to get more time for coffee in the company ;) This ext is not a PHP SPL autoloader or class loader - it is better! Loads CommandController, Xclass, Hooks, Aspects, FlexForms, Slots, TypoScript, TypeConverter, BackendLayouts and take care of createing needed templates, TCA configuration or translations at the right location.',
  'version' => '2.2.0',
  'state' => 'stable',
  'clearcacheonload' => true,
  'author' => 'Tim Lochmüller',
  'author_email' => 'tim.lochmueller@hdnet.de',
  'author_company' => 'hdnet.de',
  'constraints' => 
  array (
    'depends' => 
    array (
      'php' => '5.5.0-0.0.0',
      'typo3' => '6.2.0-8.99.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'category' => NULL,
  'uploadfolder' => true,
  'createDirs' => NULL,
);

