<?php
/**
 * $EM_CONF
 *
 * @category Extension
 * @package  AutoloaderBackendlayout
 * @author   Tim Lochmüller
 */

/** @var $_EXTKEY string */
$EM_CONF[$_EXTKEY] = [
    'title'       => 'Autoloader (BackendLayout - New Backend layouts for the backend)',
    'description' => '',
    'constraints' => [
        'depends' => [
            'autoloader' => '2.1.0-9.9.9',
        ],
    ],
];
