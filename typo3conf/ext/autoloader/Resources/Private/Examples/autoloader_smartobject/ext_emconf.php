<?php
/**
 * $EM_CONF
 *
 * @category Extension
 * @package  AutoloaderSmart
 * @author   Tim Lochmüller
 */

/** @var $_EXTKEY string */
$EM_CONF[$_EXTKEY] = [
    'title'       => 'Autoloader (Smart object - You should create test records of an smart object)',
    'description' => '',
    'constraints' => [
        'depends' => [
            'autoloader' => '2.1.0-9.9.9',
        ],
    ],
];
