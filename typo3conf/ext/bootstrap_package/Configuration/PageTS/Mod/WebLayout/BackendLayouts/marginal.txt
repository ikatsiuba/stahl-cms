################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            marginal {
                title = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.marginal
                config {
                    backend_layout {
                        colCount = 8
                        rowCount = 5
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = Header 
                                        colPos = 3
                                        colspan = 6
                                    }
                                }
                            }
							 2 {
                                columns {
                                    1 {
                                        name = Content Top
                                        colPos = 2
                                        colspan = 6
                                    }
                                }
                            }
                            3 {
                                columns {
                                    1 {
                                        name = Content Left
                                        colPos = 1
                                        colspan = 4
                                    }
									2 {
                                        name = Content Right
                                        colPos = 0
                                        colspan = 2
                                    }
                                }
                            }
							 4 {
                                columns {
                                    1 {
                                        name = Content Bottom
                                        colPos = 4
                                        colspan = 6
                                    }
                                }
                            }
                            5 {
                                columns {
                                    1 {
                                        name = Sticky: Top
                                        colPos = 10
                                        colspan = 2
                                    }
                                    2 {
                                        name = Sticky: Center
                                        colPos = 11
                                        colspan = 2
                                    }
                                    3 {
                                        name = Sticky: Bottom
                                        colPos = 12
                                        colspan = 2
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:bootstrap_package/Resources/Public/Images/BackendLayouts/default_marginal.gif
            }
        }
    }
}