<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "solr".
 *
 * Auto generated 10-01-2017 12:48
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Apache Solr for TYPO3 - Enterprise Search',
  'description' => 'Apache Solr for TYPO3 is the enterprise search server you were looking for with special features such as Faceted Search or Synonym Support and incredibly fast response times of results within milliseconds.',
  'version' => '6.0.0',
  'state' => 'stable',
  'category' => 'plugin',
  'author' => 'Ingo Renner, Timo Hund, Markus Friedrich',
  'author_email' => 'ingo@typo3.org',
  'author_company' => 'dkd Internet Service GmbH',
  'uploadfolder' => true,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'constraints' => 
  array (
    'depends' => 
    array (
      'scheduler' => '',
      'typo3' => '7.6.0-8.0.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
      'devlog' => '',
    ),
  ),
  'autoload' => 
  array (
    'classmap' => 
    array (
      0 => 'Resources/Private/Php/',
    ),
    'psr-4' => 
    array (
      'ApacheSolrForTypo3\\Solr\\' => 'Classes/',
      'ApacheSolrForTypo3\\Solr\\Tests\\' => 'Tests/',
    ),
  ),
  'clearcacheonload' => true,
);

