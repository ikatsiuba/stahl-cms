<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "phpmyadmin".
 *
 * Auto generated 17-02-2017 17:29
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'phpMyAdmin',
  'description' => 'Third party \'phpMyAdmin\' administration module. Access to admin-users only.',
  'category' => 'module',
  'version' => '5.1.9',
  'state' => 'stable',
  'uploadfolder' => true,
  'createDirs' => 'uploads/tx_phpmyadmin',
  'clearcacheonload' => false,
  'author' => 'Andreas Beutel - mehrwert',
  'author_email' => 'typo3@mehrwert.de',
  'author_company' => 'mehrwert intermediale kommunikation GmbH',
  'doNotLoadInFE' => 1,
  'constraints' => 
  array (
    'depends' => 
    array (
      'php' => '5.3.7-7.0.99',
      'typo3' => '6.2.0-7.6.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

