<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "calendarize_news".
 *
 * Auto generated 04-01-2017 11:15
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Calendarize for News',
  'description' => 'Add Event options to the news extension',
  'category' => 'fe',
  'version' => '2.0.0',
  'state' => 'stable',
  'clearcacheonload' => true,
  'author' => 'Tim Lochmüller',
  'author_email' => 'tim@fruit-lab.de',
  'constraints' => 
  array (
    'depends' => 
    array (
      'php' => '5.5.0-0.0.0',
      'typo3' => '7.6.0-8.1.99',
      'calendarize' => '2.2.0-0.0.0',
      'news' => '4.0.0-0.0.0',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'uploadfolder' => true,
  'createDirs' => NULL,
  'author_company' => NULL,
);

