<?php
// $GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'].= ',tx_realurl_pathsegment'; // Nicht mehr nÃ¶tig
$TYPO3_CONF_VARS['EXTCONF']['realurl_404_multilingual'] = array(
    '_DEFAULT' => array(
         'errorPage' => 'global/404/',
    ),
);

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = array(
	'_DEFAULT' => array(
		'init' => array(
			'enableCHashCache' => 1,
			'appendMissingSlash' => 'redirect,ifNotFile',
			'enableUrlDecodeCache' => 1,
			'enableUrlEncodeCache' => 1,
			'postVarSet_failureMode' => '',
		),
        'cache' => [
            'banUrlsRegExp' => "/L=[0-9].*ai\[action\]\=list.*\id=6|tx_solr|tx_indexedsearch|tx_kesearch|(?:^|\?|&)q=/",
        ],
		'redirects' => array(),
		'preVars' => array(
			array(
				'GETvar' => 'no_cache',
					'valueMap' => array(
						'nc' => 1,
					),
					'noMatch' => 'bypass',
				),
				array(
					'GETvar' => 'L',
					'valueMap' => array(
						'de' => '0',
						'en' => '1'
					),
					'valueDefault' => 'de',
					'noMatch' => 'bypass',
				),
			),
			'pagePath' => array(
				'type' => 'user',
				'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
				'spaceCharacter' => '-',
				'languageGetVar' => 'L',
				'expireDays' => 7,
				'rootpage_id' => 1,
				'firstHitPathCache' => 1,
			),
			'fixedPostVars' => array(
			  'category'=>array(
                    array(
                    'GETvar' => 'ai[f_catid]',
                            'userFunc' => 'Aimeos\\Aimeosext\\Hooks\\RealUrl\\PathConfig->getCategory',
                            'table' => 'mshop_catalog',
                            'alias_field' => 'label',
                            'id_field' => 'id',
                    ),
			        array (
			                'GETvar' => 'ai[d_parent]',
			                'userFunc' => 'Aimeos\\Aimeosext\\Hooks\\RealUrl\\PathConfig->getProduct',
			                'table' => 'mshop_product',
			                'alias_field' => 'label',
			                'id_field' => 'parent_id',
			        ),        
                    array (
                            'GETvar' => 'ai[d_prodid]',
                            'userFunc' => 'Aimeos\\Aimeosext\\Hooks\\RealUrl\\PathConfig->getProduct',
                            'table' => 'mshop_product',
                            'alias_field' => 'label',
                            'id_field' => 'id',
                    ),
            ),
            //'6'=>'search',  //products
            '21'=>'category',
            '505'=>'category',
            '504'=>'category',
            '503'=>'category',
            '502'=>'category',
            '501'=>'category',
            '500'=>'category',
            '499'=>'category',
            '498'=>'category',
            '497'=>'category',
            '34'=>'category' //ajax
),
			'postVarSets' => array(
				'_DEFAULT' => array(
					// news archive parameters
					'archive' => array(
						array(
							'GETvar' => 'tx_ttnews[year]' ,
						),
						array(
							'GETvar' => 'tx_ttnews[month]' ,
							'valueMap' => array(
							'january' => '01',
							'february' => '02',
							'march' => '03',
							'april' => '04',
							'may' => '05',
							'june' => '06',
							'july' => '07',
							'august' => '08',
							'september' => '09',
							'october' => '10',
							'november' => '11',
							'december' => '12',
						)
					),
				),
				// news pagebrowser
				'browse' => array(
					array(
						'GETvar' => 'tx_ttnews[pointer]',
					),
				),
				// news categories
				'select_category' => array (
					array(
						'GETvar' => 'tx_ttnews[cat]',
					),
				),
				// news articles and searchwords
				'article' => array(
					array(
						'GETvar' => 'tx_ttnews[tt_news]',
						'lookUpTable' => array(
							'table' => 'tt_news',
							'id_field' => 'uid',
							'alias_field' => 'title',
							'addWhereClause' => ' AND NOT deleted',
							'useUniqueCache' => 1,
							'useUniqueCache_conf' => array(
								'strtolower' => 1,
								'spaceCharacter' => '-',
							),
						),
					),
					array(
						'GETvar' => 'tx_ttnews[backPid]',
					),
					array(
						'GETvar' => 'tx_ttnews[swords]',
					),
				),
				        'aimeos' =>
				        array (
				                0 =>
				                array (
				                        'GETvar' => 'ai[controller]',
				                        'noMatch' => 'bypass',
				                ),
				                1 =>
				                array (
				                        'GETvar' => 'ai[action]',
				                        'noMatch' => 'bypass',
				                ),
				        ),
				        'webcode' =>
				        array(
				                0=>array(
				                        'GETvar' => 'ai[s_code]'
				                )
				        ),
				        'materialid' =>
				        array(
				                0=>array(
				                        'GETvar' => 'ai[s_matirial]'
				                )
				        ),
				        'f' =>
				        array (
				                2 =>
				                array (
				                        'GETvar' => 'ai[f_name]',
				                        'valueMap' =>
				                        array (),
				                        'noMatch' => 'bypass',
				                ),
				                3 =>
				                array (
				                        'GETvar' => 'ai[f_sort]',
				                        'valueMap' =>
				                        array (),
				                        'noMatch' => 'bypass',
				                ),
				        ),
				        'fa' =>
				        array (
				                0 =>
				                array (
				                        'GETvar' => 'ai[f_attrid]',
				                ),
				        ),
				        'pin' =>
				        array (
				                0 =>
				                array (
				                        'GETvar' => 'ai[pin_action]',
				                ),
				                1 =>
				                array (
				                        'GETvar' => 'ai[pin_id]',
				                ),
				        ),
				        'fav' =>
				        array (
				                0 =>
				                array (
				                        'GETvar' => 'ai[fav_action]',
				                ),
				                1 =>
				                array (
				                        'GETvar' => 'ai[fav_id]',
				                ),
				        ),
				        'watch' =>
				        array (
				                0 =>
				                array (
				                        'GETvar' => 'ai[wat_action]',
				                ),
				                1 =>
				                array (
				                        'GETvar' => 'ai[wat_id]',
				                ),
				        ),
				        'history' =>
				        array (
				                0 =>
				                array (
				                        'GETvar' => 'ai[his_action]',
				                ),
				                1 =>
				                array (
				                        'GETvar' => 'ai[his_id]',
				                ),
				        ),
				        'bt' =>
				        array (
				                0 =>
				                array (
				                        'GETvar' => 'ai[b_action]',
				                ),
				                1 =>
				                array (
				                        'GETvar' => 'ai[b_position]',
				                ),
				                2 =>
				                array (
				                        'GETvar' => 'ai[b_quantity]',
				                ),
				                3 =>
				                array (
				                        'GETvar' => 'ai[b_coupon]',
				                ),
				        ),
				        'co' =>
				        array (
				                0 =>
				                array (
				                        'GETvar' => 'ai[c_step]',
				                ),
				                1 =>
				                array (
				                        'GETvar' => 'code',
				                ),
				                2 =>
				                array (
				                        'GETvar' => 'orderid',
				                ),
				        ),
				        'json' =>
				        array (
				                'type' => 'single',
				                'keyValues' =>
				                array (
				                        'type' => 191351524,
				                ),
				        ),
				        'plain' =>
				        array (
				                'type' => 'single',
				                'keyValues' =>
				                array (
				                        'type' => 191351525,
				                ),
				        ),
				        
				        
			),
		),
		// configure filenames for different pagetypes
		'fileName' => array(
			'defaultToHTMLsuffixOnPrev' => 0,
			'index' => array(
				'print.html' => array(
					'keyValues' => array(
						'type' => 98,
					),
				),
				'rss.xml' => array(
					'keyValues' => array(
						'type' => 100,
					),
				),
				'rss091.xml' => array(
					'keyValues' => array(
						'type' => 101,
					),
				),
				'rdf.xml' => array(
					'keyValues' => array(
						'type' => 102,
					),
				),
				'atom.xml' => array(
					'keyValues' => array(
						'type' => 103,
					),
				),
			),
		),
	),
);